﻿using System;
using System.Collections.Generic;
using Gimli.JsonObjects;

namespace Gimli.DataService
{
	public class MeasurementContext : IMeasurementContext
	{
		public Dictionary<Guid, Document> LiveDocuments { get; } = new Dictionary<Guid, Document>();
		public List<Guid> AreQueuedForSaving { get; } = new List<Guid>();
	}
}
