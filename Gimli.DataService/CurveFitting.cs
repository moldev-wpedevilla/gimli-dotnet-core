﻿using System;
using System.Diagnostics; 

namespace Gimli.DataService
{
    internal class CurveFitting 
    {
        #region CurveBase

        internal abstract class CurveBase
        {
            protected double[] Y; 
            protected double[] X;
            protected double[] Cy;
            protected double[] Cx;
            private double mExtraYMin;
            private double mExtraYMax;
            private double mExtraXMin;
            private double mExtraXMax;
            private double mExtra;
            private readonly int mDecPlaces = 3;
            protected bool SortY;

            public abstract void CalculateCurve(double[] y, double[] x, int count, double extrapolation);

            protected abstract double CalcYFromX(double x);

            public double GetYFromX(double x)
            {
                double dret;

                try
                {
                    dret = CalcYFromX(x);
                }
                catch
                {
                    return double.NaN;
                }

                if (x < ExtrapolationXMin)
                {
                    return dret;
                }

                if (x > ExtrapolationXMax)
                {
                    return dret;
                }

                return SetValue(dret);
            }

            public double Extrapolation
            {
                get { return mExtra; }
                set { mExtra = SetValue(value); }
            }

            protected double ExtrapolationXMin
            {
                get { return mExtraXMin; }
                set { mExtraXMin = Math.Round(value, 4); }
            }

            protected double ExtrapolationYMin
            {
                get { return mExtraYMin; }
                set { mExtraYMin = Math.Round(value, 4); }
            }

            protected double ExtrapolationXMax
            {
                get { return mExtraXMax; }
                set { mExtraXMax = Math.Round(value, 4); }
            }

            protected double ExtrapolationYMax
            {
                get { return mExtraYMax; }
                set { mExtraYMax = Math.Round(value, 4); }
            }

            protected void SetParameters(double[] y, double[] x, int count, double extrapolation)
            {
                Y = y;
                X = x;

                for (var i = 0; i < y.Length; i++)
                {
                    Y[i] = SetValue(Y[i], 5);
                    X[i] = SetValue(X[i], 5);
                }

                Count = count;
                Extrapolation = extrapolation;

                Cx = new double[Count];
                Cy = new double[Count];

                SetCalcValues();
                SetExtrapolation();
            }

            private double GetMaxY
            {
                get
                {
                    if (Y[0] > Y[Count - 1])
                        return Y[0];
                    return Y[Count - 1];
                }
            }

            private double GetMinY
            {
                get
                {
                    if (Y[0] < Y[Count - 1])
                        return Y[0];
                    return Y[Count - 1];
                }
            }

            protected void SetExtrapolation()
            {
                var xadd = (ConvertLogX(Math.Abs(X[0] - X[Count - 1])))*mExtra/100;
                ExtrapolationXMin = ConvertPowX(ConvertLogX(X[X[0] < X[Count - 1] ? 0 : Count - 1]) - xadd);
                ExtrapolationXMax = ConvertPowX(ConvertLogX(X[X[0] < X[Count - 1] ? Count - 1 : 0]) + xadd);
                
                xadd = (ConvertLogY(GetMaxY) - ConvertLogY(GetMinY))*mExtra/100;
                //if (Y[0] < Y[Count - 1])
                //{
                ExtrapolationYMin = GetMinY - ConvertPowY(xadd);
                ExtrapolationYMax = GetMaxY + ConvertPowY(xadd);
                //}
                //else
                //{
                //    ExtrapolationYMin = GetMinY - ConvertPowY(xadd);
                //    ExtrapolationYMax = GetMaxY + ConvertPowY(xadd);
                //}
            }

            protected double SetValue(double d)
            {
                return SetValue(d, mDecPlaces);
            }

            protected double SetValue(double d, int decplaces)
            {
                if (double.IsNaN(d))
                    return 0;
                return Math.Round(d, decplaces);
            }

            protected int Count { get; set; }

            public virtual bool CheckMonotony { get; set; } = true;

            protected void SetCalcValues()
            {
                for (var i = 0; i < Count; i++)
                {
                    Cx[i] = X[i];
                    Cy[i] = Y[i];

                    Cx[i] = ConvertLogX(X[i]);
                    Cy[i] = ConvertLogY(Y[i]);
                }
            }

            public bool IsIncreasing { get; set; } = true;

            private bool CheckInDecreasing(string valueType, double[] v)
            {
                var increasing = !(v[0] > v[Count - 1]);

                var dTemp = v[0];

                for (var i = 1; i < Count; i++)
                {
                    if (increasing)
                    {
                        if (dTemp >= v[i])
                            throw new ArgumentException("StandardCurve" + valueType + "ValueNotMonotonIncreasing");
                    }
                    else
                    {
                        if (dTemp <= v[i])
                            throw new ArgumentException("StandardCurve" + valueType + "ValueNotMonotonDecreasing");
                    }
                    dTemp = v[i];
                }
                return increasing;
            }

            protected void MonotonyCheck()
            {
                CheckInDecreasing("X", X);
            }

            protected virtual void SortData(ref double[] yy, ref double[] xx)
            {
                for (var ix = 0; ix < xx.Length; ix++)
                {
                    for (var i = 0; i < xx.Length - 1; i++)
                    {
                        //if (xx[i] > xx[i + 1])
                        if ((SortY && yy[i] > yy[i + 1]) || (!SortY && xx[i] > xx[i + 1]))
                        {
                            var tempX = xx[i];
                            var tempY = yy[i];
                            xx[i] = xx[i + 1];
                            yy[i] = yy[i + 1];
                            xx[i + 1] = tempX;
                            yy[i + 1] = tempY;
                        }
                    }
                }
            }

            public double ConvertPowY(double y)
            {
                return y;
            }

            public double ConvertPowX(double x)
            {
                return x;
            }

            public double ConvertLogY(double y)
            {
                return y;
            }

            public double ConvertLogX(double x)
            {
                return x;
            }
        }
        #endregion

        #region PolynomialClass

        public class Polynomial : CurveBase
        {
            private double[] mParams;
            private int mOrder = 3;

            public Polynomial()
            {
                mParams = new double[27];
            }

            public int Order
            {
                get
                {
                    return mOrder - 1;
                }
                set
                {
                    mOrder = value + 1;
                    if (mOrder > 26)
                        mOrder = 27;
                    if (mOrder < 3)
                        mOrder = 3;
                }
            }

            public override void CalculateCurve(double[] y, double[] x, int count, double extrapolation)
            {
                if (y == null) throw new ArgumentNullException(nameof(y), Properties.Resources.Polynomial_CalculateCurve_Values_for_Y_Axis_are_null);
                if (x == null) throw new ArgumentNullException(nameof(x), Properties.Resources.Polynomial_CalculateCurve_Values_for_X_Axis_are_null);

                SortY = false;
                SortData(ref y, ref x);
                SetParameters(y, x, count, extrapolation);
                MonotonyCheck();
                CalcCurve(Cy, x);
            }

            protected override double CalcYFromX(double x)
            {
                double d = 0;
                for (var xx = mOrder; xx > 0; xx--)
                {
                    d += mParams[xx] * (Math.Pow(x, xx - 1));
                }

                return ConvertPowY(d);
            }

            private void CalcCurve(double[] y, double[] x)
            {
                try
                {
                    var good = LlSqFit(Count, x, y);
                    if (!good)
                        throw new ArgumentException("StandardCurvePolynomialCalcError");
                    mParams[0] = mParams[mOrder];
                }
                catch (Exception e)
                {
                    Trace.WriteLine("Polynomial: CalcCurve: " + e.Message);
                }
            }

            private void GetLVals(double x, double[] pFuncVal, long[] func, bool[] inv, double[] power)
            {
                //Get values for Linear Least Squares
                double v = 0;
                for (var i = 1; i <= mOrder; i++)
                {
                    switch (func[i])
                    {
                        case 0:
                            v = 1;
                            break;
                        case 1:
                            v = Math.Pow(x, power[i]);
                            break;
                    }
                    if (inv[i])
                    {
                        if (Math.Abs(v) < double.Epsilon)
                            pFuncVal[i] = 0;
                        else //'NOT V...
                            pFuncVal[i] = 1/v;
                    }
                    else //'INV(I) = FALSE
                        pFuncVal[i] = v;

                }
            }

            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body")]
            private bool LlSqFit(int count, double[] x, double[] y)
            {
                var beta = new double[Count + 1];
                var coVar = new double[mOrder + 1, mOrder + 1];
                var pFuncVal = new double[mOrder + 1];
                var func = new long[mOrder + 1];
                var inv = new bool[mOrder + 1];
                var power = new double[mOrder + 1];
                mParams = new double[27];

                for (var i = 1; i <= mOrder; i++)
                {
                    func[i] = 1;
                    power[i] = i - 1;
                }

                for (var i = 1; i <= count; i++)
                {
                    var xv = x[i - 1];
                    GetLVals(xv, pFuncVal, func, inv, power);

                    for (var l = 1; l <= mOrder; l++)
                    {
                        for (var m = 1; m <= l; m++)
                        {
                            coVar[l, m] = coVar[l, m] + pFuncVal[l]*pFuncVal[m];
                        }
                        beta[l] = beta[l] + y[i - 1]*pFuncVal[l];
                    }
                }
                for (var j = 2; j <= mOrder; j++)
                {
                    for (var k = 1; k < j; k++)
                    {
                        coVar[k, j] = coVar[j, k];
                    }
                }
                if (GaussJordan(coVar, mOrder + 1, beta))
                {
                    //MsgBox "There was an error in the Linear Least Squares fit"
                    for (var l = 1; l <= mOrder; l++)
                    {
                        mParams[l] = 0;
                    }
                    return false;
                }
                else //BAD = FALSE
                {
                    for (var l = 1; l <= mOrder; l++)
                    {
                        mParams[l] = beta[l];
                    }
                }

                return true;
            }

            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#")]
            private bool GaussJordan(double[,] a, int n, double[] b)
            {
                //GaussJordan elimination for LLSq and LM solving
                var indxc = new long[n];
                var indxr = new long[n];
                var ipiv = new long[n];
                long icol = 0;
                long irow = 0;
                double dum;

                for (var i = 1; i < n; i++)
                {
                    double big = 0;
                    for (var j = 1; j < n; j++)
                    {
                        if (ipiv[j] != 1)
                        {
                            for (var k = 1; k < n; k++)
                            {
                                if (ipiv[k] == 0)
                                {
                                    if (Math.Abs(a[j, k]) >= big)
                                    {
                                        big = Math.Abs(a[j, k]);
                                        irow = j;
                                        icol = k;
                                    }
                                }
                            }
                        }
                    }
                    ipiv[icol] = ipiv[icol] + 1;
                    if (irow != icol)
                    {
                        for (var l = 1; l < n; l++)
                        {
                            dum = a[irow, l];
                            a[irow, l] = a[icol, l];
                            a[icol, l] = dum;
                        }
                        dum = b[irow];
                        b[irow] = b[icol];
                        b[icol] = dum;
                    }
                    indxr[i] = irow;
                    indxc[i] = icol;
                    if (Math.Abs(a[icol, icol]) < Double.Epsilon)
                    {
                        //MsgBox "Sorry, the matrix was singular"
                        return true;
                    }
                    var pivInv = 1/a[icol, icol];
                    a[icol, icol] = 1;
                    for (var l = 1; l < n; l++)
                    {
                        a[icol, l] = a[icol, l]*pivInv;
                    }
                    b[icol] = b[icol]*pivInv;
                    for (var ll = 1; ll < n; ll++)
                    {
                        if (ll != icol)
                        {
                            dum = a[ll, icol];
                            a[ll, icol] = 0;
                            for (var l = 1; l < n; l++)
                            {
                                a[ll, l] = a[ll, l] - a[icol, l]*dum;
                            }
                            b[ll] = b[ll] - b[icol]*dum;
                        }
                    }
                }
                for (var l = n - 1; l > 0; l--)
                {
                    if (indxr[l] != indxc[l])
                    {
                        for (var k = 1; k < n; k++)
                        {
                            dum = a[k, indxr[l]];
                            a[k, indxr[l]] = a[k, indxc[l]];
                            a[k, indxc[l]] = dum;
                        }
                    }
                }
                return false;

            }
        }

        #endregion
    }
}

