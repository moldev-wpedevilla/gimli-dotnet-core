﻿using Anthos.Analysis;
using Gimli.JsonObjects.Enumarations.Status;

namespace Gimli.DataService
{
	public static class AnalysisStatusExtensions
	{
		public static DataPointStatus GetDataStatus(this AnalysisStatus analysisStatus)
		{
			switch (analysisStatus)
			{
				default:
				case AnalysisStatus.OK:
					return DataPointStatus.OK;
				case AnalysisStatus.Error:
					return DataPointStatus.Error;
				case AnalysisStatus.Overflow:
					return DataPointStatus.Overflow;
				case AnalysisStatus.Rejected:
					return DataPointStatus.Rejected;
				case AnalysisStatus.Underflow:
					return DataPointStatus.Underflow;
				case AnalysisStatus.Unused:
					return DataPointStatus.Unused;
				case AnalysisStatus.Extrapolated:
					return DataPointStatus.Extrapolated;
				case AnalysisStatus.NotEvaluated:
					return DataPointStatus.NotEvaluated;
			}
		}
	}
}
