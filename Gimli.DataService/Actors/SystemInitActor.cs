﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;

namespace Gimli.DataService.Actors
{
	public class SystemInitActor : SystemInitActorBase
	{
		public SystemInitActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
		}

		protected override bool L3_StandardImplementation(IActorsystemOverallState systemState)
		{
			// AppManager is notified by Commandsequencer
			ActorService.Get<DataServiceRoutingActor>().Tell(MessageExtensions.CreateRequestMessage(new InitRoutingActorMessage()));
			// Create invisible import user
			DocumentHelperMethods.CreateImportUserFolder(ActorService);
			return true;
		}

		protected override bool InitProxies(IActorService actorService)
		{
			var success = IsProxyConnected<WebServiceProxyActor>(actorService, GetGuaranteedAnswer);
			success = InitInstrumentCoreNetworkProxies(actorService, success) && success;

			return success;
		}
	}
}
