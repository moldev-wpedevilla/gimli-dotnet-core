﻿using Gimli.JsonObjects;
using Gimli.JsonObjects.Serialize;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.DataService.Actors
{
	public class ExportRemoteActor : GimliActorBase
	{

        public ExportRemoteActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			Receive<RequestMessage<SendDocumentToRemoteMessage>>(message =>
			{
				var response = (DocumentResponseMessage)ActorService.GetDocument(message.Model.DocumentId);
				actorService.Get<NetworkCommunicationServiceProxyActor>().Forward(MessageExtensions.CreateRequestMessage(new StoreDocumentMessage
				{
					SerializedDocument = DocumentAndSerializedWells.SerializeDocument(response.Document).JsonSerializeToCamelCase(),
					Strategy = StorageStrategy.PublishToAll,
					AutoExported = message.Model.AutoExported,
					NumberOfDocuments = message.Model.NumberOfDocuments,
					ExportCallIdentifier = message.Model.ExportCallIdentifier
				}));
			});
        }
	}
}
