﻿namespace Gimli.DataService.Actors
{
	public interface IAnthosFunctions
	{
		void SetSystemValue(string key, object value);
		object GetSystemValue(string key, object defaultValue);
	}
}