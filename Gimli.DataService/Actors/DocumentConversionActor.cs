﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Akka.Actor;
using Gimli.DataService.DataConversion.XmlUtils;
using Gimli.DataService.Properties;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.ExportImport;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.DataService.Actors
{
    class DocumentConversionActor : GimliActorBase
    {
        private IActorService mActorService { get; set; }

        public DocumentConversionActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
        {
            mActorService = actorService;

            Receive<RequestMessage<GetDocumentAsMessage>>(message =>
            {
                List<FileInfo> fileInfos = new List<FileInfo>();

                switch (message.Model.SaveAsFiletype)
                {
                    case SaveAsFiletype.Csv:
                        break;
                    case SaveAsFiletype.Excel:
                        if (message.Model.Document == null)
                        {
                            fileInfos = ExportXml(message.Model.UserFull, message.Model.documentIds);
                            Sender.Tell(new GetDocumentAsResponseMessage { TempDirectory = fileInfos?.FirstOrDefault()?.Directory, Size = GetSize(fileInfos), FileInfos = fileInfos });
                        }
                        else
                        {
                            
                        }
                        break;
                }
                
                
            });
        }

        private List<FileInfo> ExportXml(UserFull userFull, List<Guid> documentIds)
        {
            List<FileInfo> fileInfos = new List<FileInfo>();

            if (!Convert.ToBoolean(AppConfigurationManager.GetSettingsValue("ExportExcel")))
            {
                return new List<FileInfo>();
            }

            if (userFull == null)
            {
                var taskUser = ActorService.Get<DataServiceRoutingActor>().
                    Ask<GetCurrentUserResponseMessage>(
                        MessageExtensions.CreateRequestMessage(new GetCurrentUserMessage()));
                
                taskUser.Wait();
                userFull = taskUser.Result.UserFull;
            }

            var excelExportTempDirectory = AppConfigurationManager.GetSettingsValue("ExportExcelTempDirectory");

            var taskDirectory =  mActorService.Get<CoreServiceProxyActor>().Ask<GimliResponse>(MessageExtensions.CreateRequestMessage(new PrepareDirectoryMessage { DirectoryName = excelExportTempDirectory, DeleteIfExists = true }));
            taskDirectory.Wait(25000);

            int countErrors = 0;

            foreach (var documentId in documentIds)
            {
                var taskDocument = mActorService.Get<DataServiceRoutingActor>()
                    .Ask<GimliResponse>(
                        MessageExtensions.CreateRequestMessage(new ProtocolResultMessage
                        {
                            Id = documentId,
                            UserFull = userFull,
                            Type = ProtocolResultMessageType.GetDocument
                        }));

                taskDocument.Wait();

	            var typedResponse = taskDocument.Result as DocumentResponseMessage;
	            if (typedResponse == null) return new List<FileInfo>();

				var document = typedResponse.Document;
				fileInfos.Add(new FileInfo(document.ProtocolName));

                var gimliXmlTool = new GimliXmlTool(document, userFull, Log);
                try
                {
                    gimliXmlTool.Export(excelExportTempDirectory); //targetDirectoryInfo.FullName);    
                    var xmlFileInfo = gimliXmlTool.GetXmlFileInfo();
                    fileInfos.Add(xmlFileInfo);
                }
                catch (Exception e)
                {
                    countErrors++;
                    Log.Info(document.DocumentName + " could not be exported due to: " + e.InnerException);   
                }
            }

            if (countErrors > 0)
            {
                mActorService.Get<WebServiceProxyActor>()
                    .Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
                    {
                        Parameters = new MessageParameters
                        {
                            Message = string.Format(Resources.Exporting_Files_Conversion_Errors, countErrors),
                            MessageType = MessageType.Info,
                            DisplayType = MessageDisplayType.Toast,
                            MessageCode = MessageStatusCodes.DummyMessage
                        }
                    }));
            }

            return fileInfos;
        }
        

        private long GetSize(List<FileInfo> fileInfos)
        {
            long size = 0;

            foreach (var fileInfo in fileInfos)
            {
                size += fileInfo.Length;
            }

            return size;
        }
    }
}
