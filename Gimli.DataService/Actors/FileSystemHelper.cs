﻿using System;
using System.IO;
using Ionic.Zip;
using System.Collections.Generic;

namespace Gimli.DataService.Actors
{
	internal class FileSystemHelper : IFileSystemFunctions
	{
		public ulong GetFileSize(string filename)
		{
			return (ulong)new FileInfo(filename).Length;
		}

        public ulong GetFileSize(DirectoryInfo directoryInfo)
        {
            var fileInfos = GetFilesInDirectory(directoryInfo);
            ulong size = 0;

            foreach (var fileInfo in fileInfos)
            {
                size += GetFileSize(new FileInfo(fileInfo).FullName);
            }

            return size;
        }

        public ulong GetFileSize(string[] filenames)
        {
            ulong size = 0;

            foreach (var filename in filenames)
            {
                size += GetFileSize(filename);
            }

            return size;
        }

	    public string[] GetFilesInDirectory(DirectoryInfo directoryInfo)
	    {
	        FileInfo[] fileInfos = directoryInfo.GetFiles("*.*", SearchOption.AllDirectories);
            List<string> fileList = new List<string>();
            foreach (var fileInfo in fileInfos)
            {
                fileList.Add(fileInfo.FullName);
            }
            return fileList.ToArray();
        }

	    public void DeleteFiles(string[] files)
	    {
	        foreach (string file in files)
	        {
	            try
	            {
	                new FileInfo(file).Delete();
	            }
                catch (Exception)
	            {
	                
	            }
	        }
	    }

        public void ZipFiles(string[] filenames, DirectoryInfo directoryInfoSource, string fileNameTarget)
        {
            var fileNames = new List<string>();

            using (var zipFile = new ZipFile())
            {
                foreach (var file in filenames)
                {
                    var fileName = Path.GetFileName(file);

                    if (!fileNames.Contains(fileName))
                    {
                        fileNames.Add(fileName);
                        zipFile.AddFile(file, "");
                    }
                }
                zipFile.Save(Path.Combine(directoryInfoSource.FullName, fileNameTarget));
            }
        }

        public bool CopyFiles(DirectoryInfo source, DirectoryInfo target, string[] filenames, bool overwrite = true)
        {
            if (filenames == null)
            {
                filenames = GetFilesInDirectory(source);
            }

            foreach (var filename in filenames)
            {
                try
                {
                    File.Copy(Path.Combine(source.FullName, filename), Path.Combine(target.FullName, filename), overwrite);
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

		public bool DoesDirectoryExist(string filename)
		{
			return Directory.Exists(filename);
		}

		public bool DoesFileExist(string filename)
		{
			return File.Exists(filename);
		}

		public void DeleteDirectory(string path, bool recursive)
		{
			Directory.Delete(path, recursive);
		}
	}
}