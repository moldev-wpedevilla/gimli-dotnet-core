﻿using System.Linq;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.DataService.Actors
{
	public class LiveDocumentRoutingActor : GimliActorBase
	{
		private readonly string StoreDataActorName = nameof(WriteDataActor);
		private readonly IMeasurementContext mMeasurementContext;

		public LiveDocumentRoutingActor(IActorService actorService, IMeasurementContext measurementContext, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			mMeasurementContext = measurementContext;

			Receive<RequestMessage<UpdateDocumentWithPlateMessage>>(message =>
			{
				GetGuaranteedAnswer(UpdatePlate, message);
				Parent.Tell(message.Model.Id);
			});
		}

		protected virtual IActor Parent => ParentRef;

		private GimliResponse UpdatePlate(RequestMessage<UpdateDocumentWithPlateMessage> message)
		{
			Log.Debug("Received live-data message for deserialize data.");
			if (message.Model.Plate == null)
			{
				ActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(message.Model.FrontendMessage));
				return new GimliResponse();
			}

			// get document
			var updatePlateDocumentResponse = ActorService.GetDocument(message.Model.DocumentId);
			if (updatePlateDocumentResponse.ResponseStatus != ResultStatusEnum.Success)
			{
				return updatePlateDocumentResponse;
			}

			var updatePlateDocument = ((DocumentResponseMessage)updatePlateDocumentResponse).Document;
			var requestedPlates =
				updatePlateDocument.Experiments.SelectMany(e => e.Plates.Where(p => p.Id == message.Model.Plate.Id)).ToList();

			if (requestedPlates.Count != 1)
			{
				throw new ActorException(
					$"Number of plates with the given id {message.Model.Plate.Id} in the document with id {message.Model.DocumentId} is {requestedPlates.Count}.");
			}

			var wells = JsonObjects.Deserialize.RawBinaryReader.DeserializeWells(message.Model.WellSeriailization,
				message.Model.WellSeriailizationSize);

			// do primary data reduction
			foreach (var well in wells)
			{
				well.ReduceWellData(message.Model.Plate.SnapShot[0]);
			}

			message.Model.Plate.SnapShot[0].Wells = wells;

			var plate = requestedPlates[0];
			var parentExperiment = updatePlateDocument.Experiments.First(e => e.Plates.Contains(plate));
			var index = parentExperiment.Plates.IndexOf(plate);
			parentExperiment.Plates.Remove(plate);
			parentExperiment.Plates.Insert(index, message.Model.Plate);

			// Start of atomic operation, don't do anything else in here or you'll get race conditions
			mMeasurementContext.LiveDocuments[updatePlateDocument.Id] = updatePlateDocument;
			if (!mMeasurementContext.AreQueuedForSaving.Contains(updatePlateDocument.Id))
			{
				mMeasurementContext.AreQueuedForSaving.Add(updatePlateDocument.Id);
			}
			// End of atomic operation

			GetChild<StoreLatestLiveDataActor>(StoreDataActorName).Tell(updatePlateDocument.Id);

			ActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(message.Model.FrontendMessage));

			return new GimliResponse();
		}

		protected override void PreStart()
		{
			GetChild<StoreLatestLiveDataActor>(StoreDataActorName);

			base.PreStart();
		}
	}
}
