﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gimli.JsonObjects;
using Gimli.JsonObjects.DummyData;
using Gimli.JsonObjects.Enumarations.Status;

namespace Gimli.DataService.Actors
{
    static class ResultFilter
	{
	    internal static void RestrictDocumentToPlates(this Document emptyDocument, List<Guid> plateIds)
	    {
		    foreach (var experiment in emptyDocument.Experiments.ToList())
		    {
			    var toDelete = experiment.Plates.Where(plate => !plateIds.Contains(plate.Id)).ToList();

			    foreach (var plate in toDelete)
			    {
				    experiment.Plates.Remove(plate);
			    }

			    if (experiment.Plates.Count < 1)
			    {
				    emptyDocument.Experiments.Remove(experiment);
			    }
		    }
	    }


	    internal static void RestrictDataToWellGroup(this Document protocolContent, PlateWellBase wellGroup)
	    {
            GimliTrace.Start("Filter");
	        if (protocolContent == null) throw new ArgumentNullException(nameof(protocolContent));
	        if (protocolContent.Experiments == null) throw new ArgumentNullException(nameof(protocolContent.Experiments));

	        foreach (var plate in protocolContent.Experiments.SelectMany(experiment => experiment.Plates))
	        {
	            if (wellGroup != null && plate.Id != wellGroup.PlateID)
	            {
	                plate.SnapShot = null;
	                continue;
	            }

                plate.RestrictDataOfSnapshot(wellGroup);
	        }
	        GimliTrace.Stop("Filter");
        }

	    private static void RestrictDataOfSnapshot(this Plate plate, PlateWellBase wellGroup)
	    {
	        foreach (var snapShot in plate.SnapShot)
	        {
                snapShot.RestrictDataOfWell(wellGroup);
            }
		}

		private static Func<Well, bool> IsNotInside(PlateWellBase wellGroup)
		{
			var wellArray = wellGroup as PlateWellArray;
			var wellRange = wellGroup as PlateWellRange;

			if (wellArray != null)
			{
				return w => w.IsNotInArray(wellArray);
			}

			return w => w.IsNotInRange(wellRange);
		}

		private static void RestrictDataOfWell(this Snapshot snapShot, PlateWellBase wellGroup)
        {
			var isNotInside = IsNotInside(wellGroup);

	        foreach (var well in snapShot.Wells.ToList())
	        {
	            if (well.IfNotInRangeDeleteData(wellGroup)) continue;

	            if (isNotInside(well))  //well is not in range or array, requested from frontend
	            {
	                snapShot.Wells.Remove(well);
	                continue;
	            }

	            if (snapShot.Settings.MeasurementType.UseMaxPoints())
	            {
	                // This is inside the range -> OK
	                well.RoundValues(snapShot.Settings.MeasurementMode == Mode.Abs ? 3 : 0);
	                continue;
	            }

                well.LimitDataPoints(wellGroup, snapShot.Settings.MeasurementType, snapShot.Settings.MeasurementMode == Mode.Abs ? 3 : 0, snapShot);

				well.UpdateMinMeanMax();
	        }

			snapShot.UpdateMinMax();
	    }

        internal static void LimitDataPoints(this Well well, PlateWellBase wellGroup, MeasurementType measurementType, int decimalPlaces, Snapshot snapShot)
	    {
			if (well.Points == null) return;
			if (wellGroup.MaxPoints.X == 0 && wellGroup.MaxPoints.Y == 0) return;
            //create new well object and fill it with data
            var desiredWl = well.Points.GetLength(0);
	        var desiredR = measurementType.IsAreaScan()
                ? Math.Min(wellGroup.MaxPoints.Y, well.Points.GetLength(1))
	            : well.Points.GetLength(1);
	        var desiredC = Math.Min(wellGroup.MaxPoints.X, well.Points.GetLength(2));

            //nothing to do
            if (desiredR == well.Points.GetLength(1) && desiredC == well.Points.GetLength(2)) return;

	        var newPointValues = new double[desiredWl, desiredR, desiredC];
	        var newStateValues = new DataPointStatus[desiredWl, desiredR, desiredC];

            for (var wl = 0; wl < desiredWl; wl++)
	        {
				var reducedPointsList = new List<List<double>>();
				var reducedStatesList = new List<List<DataPointStatus>>();
				for (var r = 0; r < well.Points.GetLength(1); r++)
	            {
	                if (measurementType.IsKinetic() || measurementType.IsSpectralScan())
	                {
                        var dataYOut = new List<double>();
                        var dataSOut = new List<DataPointStatus>();
						var actualPoints = InitDataLists(wl, r, well, dataYOut, dataSOut);

	                    while (actualPoints > desiredC)
	                    {
	                        var dataYIn = new double[dataYOut.Count];
	                        dataYOut.CopyTo(dataYIn);
                            var dataSIn = new DataPointStatus[dataSOut.Count];
                            dataSOut.CopyTo(dataSIn);
                            actualPoints = CreateReducedData(wl, r, dataYIn, dataSIn, dataYOut, dataSOut);
	                    }

						reducedPointsList.Add(dataYOut);
						reducedStatesList.Add(dataSOut);
		                if (r == well.Points.GetLength(1) - 1)
		                {
			                ReCopyData(wl, well, reducedPointsList, reducedStatesList, snapShot);
		                }
	                }
	                else
	                {
	                    for (var c = 0; c < well.Points.GetLength(2); c++)
	                    {
	                        if ((measurementType.IsAreaScan() && r < wellGroup.MaxPoints.Y && c < wellGroup.MaxPoints.X) ||
	                            (measurementType.IsGraphView() && c < wellGroup.MaxPoints.X))
	                        {
	                            newPointValues[wl, r, c] = Math.Round(well.Points[wl, r, c], decimalPlaces);
	                            newStateValues[wl, r, c] = well.DataPointsState[wl, r, c];
	                        }
	                    }
                        well.DataPointsState = newStateValues;
                        well.Points = newPointValues;
                    }
	            }
	        }

            
	    }

        private static void ReCopyData(int wl, Well well, List<List<double>> dataY, List<List<DataPointStatus>> dataS, Snapshot snapShot)
        {
            var kineticXValueList = new List<double>();
            well.Points = new double[well.Points.GetLength(0), well.Points.GetLength(1), dataY[0].Count];
            well.DataPointsState = new DataPointStatus[well.Points.GetLength(0), well.Points.GetLength(1), dataS[0].Count];

	        for (var wavelen = 0; wavelen < dataY.Count; wavelen++)
	        {

		        for (var i = 0; i < dataY[wavelen].Count; i++)
		        {
			        well.Points[wl, wavelen, i] = dataY[wavelen][i];

			        if (wavelen == 0) kineticXValueList.Add(i);
		        }
		        for (var i = 0; i < dataS[wavelen].Count; i++)
			        well.DataPointsState[wl, wavelen, i] = dataS[wavelen][i];

		        //var min = snapShot.DataDimensionInfo[2].DataPointLabel.First();
		        //var max = snapShot.DataDimensionInfo[2].DataPointLabel.Last();
		        //var increment = (max - min) / (dataY.Count - 1);
		        //var actXValue = min;
		        //var kineticXValueList = new List<double>();
		        //for (var c = 0; c < dataY.Count; c++)
		        //{
		        //    kineticXValueList.Add(Math.Round(actXValue, 3));
		        //    actXValue += increment;
		        //}

		        snapShot.GraphXValueList = kineticXValueList;
	        }
        }

        internal static int InitDataLists(int wl, int r, Well well, List<double> newPointValues, List<DataPointStatus> newStateValues)
        {
            newPointValues.Clear();
            newStateValues.Clear();

            for (var c = 0; c < well.Points.GetLength(2); c++)
            {
                newPointValues.Add(well.Points[wl,r,c]);
                newStateValues.Add(well.DataPointsState[wl, r, c]);
            }

            return newPointValues.Count;
        }

        internal static int CreateReducedData(int wl, int r, double[] inputX, DataPointStatus[] inputS,
            List<double> newPointValues, List<DataPointStatus> newStateValues)
        {
            newPointValues.Clear();
            newStateValues.Clear();
            for (var c = 0; c < inputX.Length; c++)
            {
	            if (c + 2 == inputX.Length)
	            {
		            newPointValues.Add(inputX[c + 1]);
                    newStateValues.Add(inputS[c + 1]);
					break;
				}

				if (c + 2 > inputX.Length) break;
                if (inputS[c] != DataPointStatus.OK)
                {
                    newPointValues.Add(inputX[c]);
                    newStateValues.Add(inputS[c]);
                }

                else if (inputS[c + 1] != DataPointStatus.OK)
                {
                    newPointValues.Add(inputX[c + 1]);
                    newStateValues.Add(inputS[c + 1]);
                }
                else
                {
                    newPointValues.Add((inputX[c] + inputX[c + 1]) / 2);
                    newStateValues.Add(inputS[c]);
                }
                c++;
            }
            return newPointValues.Count;
        }

		private static DataDimension[] AdjustDimensionsInfo(IReadOnlyList<DataDimension> oldValues, DataDimension[] newValues)
        {
            if (oldValues == null) return newValues;

            var dimensionDiffers = false;
            for (var i = 0; i < 3; i++)
            {
                if (oldValues[i].DataPointLabel.Count != newValues[i].DataPointLabel.Count)
                    dimensionDiffers = true;
            }
            if (!dimensionDiffers) return newValues;

            for (var i = 0; i < 3; i++)
            {
                newValues[i] = AdjustDimensionInfo(oldValues[i], newValues[i]);
            }
            return newValues;
        }

        private static DataDimension AdjustDimensionInfo(DataDimension oldValues, DataDimension newValues)
        {
            if (oldValues.DataPointLabel.Count == newValues.DataPointLabel.Count) return newValues;

            var minValue = oldValues.DataPointLabel[0];
            var maxValue = oldValues.DataPointLabel[oldValues.DataPointLabel.Count - 1];
            var increment = (maxValue - minValue) / (newValues.DataPointLabel.Count - 1);
            var actValue = oldValues.DataPointLabel[0];
            var count = newValues.DataPointLabel.Count;
            newValues.DataPointLabel.Clear();

            for (var i = 0; i < count; i++)
            {
                newValues.DataPointLabel.Add(actValue);
                actValue += increment;
            }
            return newValues;
        }
	}
}
