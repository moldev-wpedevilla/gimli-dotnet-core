﻿using System;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages.Errors;

namespace Gimli.DataService.Actors
{
	public class StoreLatestLiveDataActor : GimliActorBase
	{
		private readonly IMeasurementContext mMeasurementContext;

		public StoreLatestLiveDataActor(IActorService actorService, IMeasurementContext measurementContext, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			mMeasurementContext = measurementContext;

			Receive<Guid>(message =>
			{
				SaveAndUpdateContext(message);
			});
		}

		private void SaveAndUpdateContext(Guid message)
		{
			if (!mMeasurementContext.LiveDocuments.ContainsKey(message))
			{
				return;
			}

			Log.Debug("Received live-data message for storage.");
			mMeasurementContext.AreQueuedForSaving.RemoveAll(d => d == message);
			var storageMessage = MessageExtensions.CreateRequestMessage(new StoreDocumentMessage
			{
				Document = mMeasurementContext.LiveDocuments[message].Clone(),
				RemoteHostname = HelperMethods.GetLocalHostName(),
				Strategy = StorageStrategy.Local
			}); 

			var task = ActorService.Get<DataServiceRoutingActor>()
				.Ask<GimliResponse>(storageMessage);
			task.Wait();

			if (task.Result.ResponseStatus == ResultStatusEnum.Success && !mMeasurementContext.AreQueuedForSaving.Contains(message))
			{
				mMeasurementContext.LiveDocuments.Remove(message);
				// do not set IsQueuedForSaving false here because the thought is that parent 
				// sets it true when new update comes in but saving data to filesystem/db takes 
				// longer and document needs to be saved again after this call here has succeeded
			}
		}
	}
}
