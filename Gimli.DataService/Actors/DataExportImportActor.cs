﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Akka.Actor;
using Castle.Core.Internal;
using Gimli.DataService.Properties;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Enums;
using Gimli.JsonObjects.Utils;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.ExportImport;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.DataService.Actors
{
	public class DataExportImportActor : GimliActorBase
	{
		private readonly IAnthosFunctions mAnthosFunctions;
		private readonly IFileSystemFunctions mFileSystemFunctions;
		private const string GuidRegex = "([0-9A-Fa-f]{8}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{12})";
		private const string ListOfItemsName = "ListOfItems.xml";

		private enum ExportMessage
		{
			Success,
			Error,
			ErrorNoDrive,
			ExportToNet,
			ExportToNetPartial
		}

		public DataExportImportActor(IActorService actorService, IActorsystemOverallState systemState,
			IAnthosFunctions anthosFunctions, IFileSystemFunctions fileSystemFunctions) : base(actorService, systemState)
		{
			mAnthosFunctions = anthosFunctions;
			mFileSystemFunctions = fileSystemFunctions;

			Receive<RequestMessage<ExportFilesMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(ExportFiles, message));
			});

			Receive<RequestMessage<ToggleExportExcelMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(ToggleExportFiles, message));
			});

			Receive<RequestMessage<GetGlobalConfigurationMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(GetToggleExportFiles, message));
			});

			Receive<RequestMessage<CopyFilesMessage>>(message =>
			{
				bool success = mFileSystemFunctions.CopyFiles(message.Model.Source, message.Model.Target, message.Model.Filenames);
				Sender.Tell(new CopyFilesResponseMessage { Success = success });
			});

			Receive<RequestMessage<ImportFilesMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(ImportFiles, message));
			});

			Receive<RequestMessage<UsbDrivePluggedInMessage>>(message =>
			{
				// Create invisible import user
				DocumentHelperMethods.CreateImportUserFolder(ActorService).Wait(1000);

				var drives = GetAllDriveLetters();
				var filesFound = false;

				foreach (var drive in drives)
				{
					var targetDir = Path.Combine(drive.Item1, GlobalConstants.UsbMdDocumentStore);
					if (!mFileSystemFunctions.DoesDirectoryExist(targetDir)) continue;
					var files = mFileSystemFunctions.GetFilesInDirectory(new DirectoryInfo(targetDir)).Where(f => f != null && Regex.IsMatch(Path.GetFileName(f), GuidRegex) && f.EndsWith(".xml")).ToList();
					foreach (var file in files)
					{
						var fileDir = Path.GetDirectoryName(file);
						if (file == null || fileDir.IsNullOrEmpty()) continue;
						mFileSystemFunctions.CopyFiles(new DirectoryInfo(fileDir), new DirectoryInfo(Path.Combine(DataSetHelper.RootPath, GlobalConstants.ImportGuid.ToString())), new[] { Path.GetFileName(file) });

						filesFound = true;
					}
				}
				DocumentHelperMethods.NotifyUserAboutImportableData(actorService);

				if (!filesFound || message.Model.InitialCheck)
				{
					actorService.Get<AppManagerProxyActor>().Forward(message);
				}
			});

			Receive<RequestMessage<UsbDrivePluggedOutMessage>>(message =>
			{
				var userDir = Path.Combine(DataSetHelper.RootPath, GlobalConstants.ImportGuid.ToString());
				if (mFileSystemFunctions.DoesDirectoryExist(userDir)) mFileSystemFunctions.DeleteDirectory(userDir, true);
			});

			Receive<RequestMessage<DeleteDocumentsMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(DelteFilesOnUsb, message));
			});
		}

		private GimliResponse DelteFilesOnUsb(RequestMessage<DeleteDocumentsMessage> message)
		{
			using (SystemState.DisableShutdown(ShutdownForbiddenReason.WritingToFilesystem))
			{
				var successMessage = MessageExtensions.CreateRequestMessage(new DeleteDocumentsMessage { Documents = new List<Guid>() });
				var drives = GetAllDriveLetters();
				foreach (var drive in drives)
				{
					var targetDir = Path.Combine(drive.Item1, GlobalConstants.UsbMdDocumentStore);
					if (!mFileSystemFunctions.DoesDirectoryExist(targetDir)) continue;
					var dataStoreDir = Path.Combine(DataSetHelper.RootPath, GlobalConstants.ImportGuid.ToString());
					var usbDir = Path.Combine(drive.Item1, GlobalConstants.UsbMdDocumentStore);
					foreach (var document in message.Model.Documents)
					{
						try
						{
							var dataStoreFileName = Path.Combine(dataStoreDir, document + ".xml");
							var usbFileName = Path.Combine(usbDir, document + ".xml");
							if (mFileSystemFunctions.DoesFileExist(usbFileName)) mFileSystemFunctions.DeleteFiles(new[] { usbFileName });
							if (mFileSystemFunctions.DoesFileExist(dataStoreFileName)) mFileSystemFunctions.DeleteFiles(new[] { dataStoreFileName });
							successMessage.Model.Documents.Add(document);
						}
						catch (Exception)
						{
							Log.Error("Unable to delete file: " + document);
						}
					}

					var dataStoreListName = Path.Combine(dataStoreDir, ListOfItemsName);
					if (mFileSystemFunctions.DoesFileExist(dataStoreListName)) mFileSystemFunctions.DeleteFiles(new[] { dataStoreListName });
				}

				ActorService.Get<WebServiceProxyActor>().Ask<GimliResponse>(successMessage);
			}

			return new GimliResponse();
		}


		private GimliResponse ImportFiles(RequestMessage<ImportFilesMessage> message)
		{
			using (var spinnerController = new SpinnerController(() => ActorService.Get<WebServiceProxyActor>(), Resources.Importing_Files))
			{
				spinnerController.DisplaySpinner();
				using (SystemState.DisableShutdown(ShutdownForbiddenReason.WritingToFilesystem))
				{
					var result = ActorService.Get<DataServiceRoutingActor>()
						.Ask<GetCurrentUserResponseMessage>(MessageExtensions.CreateRequestMessage(new GetCurrentUserMessage()), 10000);
					result.Wait();

					foreach (var document in message.Model.Files)
					{
						var documentResponse = ActorService.GetDocument(document, new UserFull { Id = GlobalConstants.ImportGuid });
						if (documentResponse.ResponseStatus != ResultStatusEnum.Success)
						{
							continue;
						}
						var documentToImport = ((DocumentResponseMessage)documentResponse).Document;
						documentToImport.Id = Guid.NewGuid();
						documentToImport.IsProtected = false;
						documentToImport.IsPinned = false;
						documentToImport.Timestamp = DateTime.Now;
						documentToImport.Category = documentToImport.HasData ? ItemType.Result.ToString() : ItemType.Protocol.ToString();
						foreach (var experiment in documentToImport.Experiments)
						{
							experiment.Id = Guid.NewGuid();
							foreach (var plate in experiment.Plates)
							{
								plate.Id = Guid.NewGuid();
								foreach (var snapshot in plate.SnapShot)
								{
									snapshot.Id = Guid.NewGuid();
								}
							}
						}
						ActorService.SaveDocument(documentToImport);
					}
				}
				spinnerController.HiddeSpinner(new GenericFrontendPushMessage
				{
					Parameters = new MessageParameters
					{
						Message = "Import finished.",
						MessageCode = MessageStatusCodes.ImportFinishedMessage,
						DisplayType = MessageDisplayType.Toast,
						MessageType = MessageType.Info,
					}
				});
			}

			return new GimliResponse();
		}

		private GimliResponse GetToggleExportFiles(RequestMessage<GetGlobalConfigurationMessage> message)
		{
			var exportExcelFlag = string.Equals(mAnthosFunctions.GetSystemValue("GimliExportExcel", false).ToString(),
							bool.TrueString, StringComparison.CurrentCultureIgnoreCase);

			return new GetGlobalConfigurationResponseMessage { Preferences = new ReadPreferences { ExportExcel = exportExcelFlag } };
		}

		private GimliResponse ToggleExportFiles(RequestMessage<ToggleExportExcelMessage> message)
		{
			mAnthosFunctions.SetSystemValue("GimliExportExcel", message.Model.Active);

			return new GimliResponse();
		}

		private GimliResponse ExportFiles(RequestMessage<ExportFilesMessage> message)
		{
			if (message.Model.AutoExported && !bool.Parse(mAnthosFunctions.GetSystemValue("GimliExportExcel", false).ToString()))
			{
				return new GimliResponse();
			}

			using (var spinnerController = new SpinnerController(() => ActorService.Get<WebServiceProxyActor>(), Resources.ResourceManager.GetString("Exporting_Files")))
			{
				spinnerController.DisplaySpinner();

				if (message.Model.SendProtocolsToUsb)
				{
					var filesToExport = GetPathsOfProtocols(message.Model.Filenames, message.Model.UserFull);

					var rawSuccess = ExportRawDataToUsb(filesToExport);
					var exportMessage = rawSuccess;

					spinnerController.HiddeSpinner(exportMessage);
					return new GimliResponse();
				}
				else
				{
					var filesToExport = message.Model.Logs
						? new[] { PrepareLogsForExport() }
						: PrepareDataForExportDocument(message.Model.Filenames, message.Model.UserFull);

					var endResponse = new GimliResponse();
					GenericFrontendPushMessage exportMessage;

					var rawFiles = filesToExport.Where(f => Regex.IsMatch(f, GuidRegex)).ToArray();
					var excelOrLogs = filesToExport.Where(f => !rawFiles.Contains(f)).ToArray();

					var exportedToUsb = ExportToUsb(excelOrLogs, out exportMessage);
					ExportRawDataToUsb(rawFiles);

					if (!message.Model.Logs)
					{
						if (exportedToUsb)
						{
							ExportToWeb(excelOrLogs);
						}
						else
						{
							endResponse = ExportToNet(message.Model.Filenames, message.Model.AutoExported, out exportMessage);
						}
					}

					mFileSystemFunctions.DeleteFiles(excelOrLogs);
					spinnerController.HiddeSpinner(exportMessage);
					return endResponse;
				}
			}
		}

		private string[] GetPathsOfProtocols(string[] documentIds, UserFull userFull)
		{
			var fileInfos = new List<string>();
			foreach (var documentId in documentIds)
			{
				var taskDocument = ActorService.Get<DataServiceRoutingActor>()
					.Ask<GimliResponse>(
						MessageExtensions.CreateRequestMessage(new ProtocolResultMessage
						{
							Id = Guid.Parse(documentId.Split('.')[0]),
							UserFull = userFull,
							Type = ProtocolResultMessageType.GetDocument
						}));

				taskDocument.Wait();

				var typedResponse = taskDocument.Result as DocumentResponseMessage;
				if (typedResponse == null) return new string[0];

				var document = typedResponse.Document;
				fileInfos.Add(document.ProtocolName);
			}

			return fileInfos.ToArray();
		}

		private string PrepareLogsForExport()
		{
			var filenameZip = $"{Environment.MachineName}_logs_{DateTime.Now.ToString("yyMMdd_HHmmss")}.zip";
			DirectoryInfo directoryInfoLogs = new DirectoryInfo(DataSetHelper.RootPath + @"logs" + Path.DirectorySeparatorChar);
			string filenameTarget = Path.Combine(directoryInfoLogs + filenameZip);

			mFileSystemFunctions.ZipFiles(mFileSystemFunctions.GetFilesInDirectory(directoryInfoLogs), directoryInfoLogs, filenameTarget);

			return filenameTarget;
		}

		private string[] PrepareDataForExportDocument(string[] filenames, UserFull userFull)
		{
			List<Guid> guids = filenames.Select(f => new Guid(f.Split('.')[0])).ToList();

			var task = ActorService.Get<DataServiceRoutingActor>()
				.Ask<GetDocumentAsResponseMessage>(
					MessageExtensions.CreateRequestMessage(new GetDocumentAsMessage
					{
						documentIds = guids,
						SaveAsFiletype = SaveAsFiletype.Excel,
						UserFull = userFull
					}), 0);
			task.Wait();

			return task.Result.FileInfos.Select(f => f.FullName).ToArray();
		}

		private GimliResponse ExportToNet(string[] files, bool autoExport, out GenericFrontendPushMessage message)
		{
			var minNumberOfSucceededClients = int.MaxValue;
			var succeededDocuments = new List<string>();
			var exportCallIdentifier = Guid.NewGuid();

			foreach (var file in files)
			{
				var documentId = Guid.Parse(Path.GetFileNameWithoutExtension(file));
				var result = GetGuaranteedAnswer(m =>
				{
					var task = ActorService.Get<DataServiceRoutingActor>()
						.Ask<GimliResponse>(
							MessageExtensions.CreateRequestMessage(new SendDocumentToRemoteMessage
							{
								DocumentId = documentId,
								NumberOfDocuments = files.Length,
								ExportCallIdentifier = exportCallIdentifier,
								AutoExported = autoExport
							}), 25000);
					task.Wait();
					return task.Result;
				}, (RequestMessage<ExportFilesMessage>)null);

				if (result.ResponseStatus == ResultStatusEnum.Success)
				{
					var typedResult = result as RemotingSuccessResponseMessage;
					if (typedResult != null && typedResult.NumberOfSuccesses < minNumberOfSucceededClients)
					{
						minNumberOfSucceededClients = typedResult.NumberOfSuccesses;
					}
					succeededDocuments.Add(file);
				}
			}

			if (succeededDocuments.Any())
			{
				if (succeededDocuments.Count == files.Length)
				{
					message = CreateFrontendMessage(ExportMessage.ExportToNet, minNumberOfSucceededClients == 1
										? string.Format(Resources.Export_Success_Network1)
										: string.Format(Resources.Export_Success_Network, minNumberOfSucceededClients));

					return new GimliResponse();
				}

				message = CreateFrontendMessage(ExportMessage.ExportToNetPartial);

				return new GimliResponse(new ErrorInfo { Message = "Files where not exported to all clients." }, ResultStatusEnum.Error, Log);
			}

			const string error = "Files where not exported to any client.";
			message = CreateFrontendMessage(ExportMessage.Error, error);
			return new GimliResponse(new ErrorInfo { Message = error }, ResultStatusEnum.Error, Log);
		}

		private GenericFrontendPushMessage ExportRawDataToUsb(string[] files)
		{
			GenericFrontendPushMessage message;
			var letterToSave = GetLetterToSave(mFileSystemFunctions.GetFileSize(files), out message);
			if (letterToSave == null)
			{
				return message;
			}

			try
			{
				var targetDir = Path.Combine(letterToSave, GlobalConstants.UsbMdDocumentStore);
				var importUserPath = Path.Combine(DataSetHelper.RootPath, GlobalConstants.ImportGuid.ToString());
				if (!mFileSystemFunctions.DoesDirectoryExist(targetDir)) Directory.CreateDirectory(targetDir);
				foreach (var file in files)
				{
					var fileInfo = new FileInfo(file);
					mFileSystemFunctions.CopyFiles(fileInfo.Directory, new DirectoryInfo(targetDir), new[] { fileInfo.Name });
					mFileSystemFunctions.CopyFiles(fileInfo.Directory, new DirectoryInfo(importUserPath), new[] { fileInfo.Name });
				}

				var importUserListFile = Path.Combine(importUserPath, ListOfItemsName);
				if (mFileSystemFunctions.DoesFileExist(importUserListFile))
				{
					try
					{
						mFileSystemFunctions.DeleteFiles(new[] { importUserListFile });
					}
					catch (Exception)
					{
						Log.Error("Error deleting user list file of import user.");
					}
				}
			}
			catch (Exception)
			{
				return CreateFrontendMessage(ExportMessage.Error);
			}

			return CreateFrontendMessage(ExportMessage.Success);
		}

		private bool ExportToUsb(string[] files, out GenericFrontendPushMessage message)
		{
			var letterToSave = GetLetterToSave(mFileSystemFunctions.GetFileSize(files), out message);

			if (letterToSave == null)
			{
				return false;
			}

			var copied = mFileSystemFunctions.CopyFiles(new FileInfo(files[0]).Directory, new DirectoryInfo(letterToSave), files.Select(f => new FileInfo(f).Name).ToArray());

			if (copied)
			{
				message = CreateFrontendMessage(ExportMessage.Success);
			}

			return copied;
		}

		private void ExportToWeb(string[] files)
		{
			if (!Convert.ToBoolean(AppConfigurationManager.GetSettingsValue("GenerateForWeb")))
			{
				return;
			}

			var exportWebDirectory = AppConfigurationManager.GetSettingsValue("ExportWebDirectory");

			var taskWebDirectory = ActorService.Get<CoreServiceProxyActor>()
				.Ask<GimliResponse>(MessageExtensions.CreateRequestMessage(
					new PrepareDirectoryMessage
					{
						DirectoryName = exportWebDirectory,
						DeleteIfExists = false
					}));
			taskWebDirectory.Wait(25000);

			foreach (var file in files)
			{
				FileInfo fileInfo = new FileInfo(file);

				mFileSystemFunctions.CopyFiles(fileInfo.Directory, new DirectoryInfo(exportWebDirectory),
					new[] { fileInfo.Name });
			}
		}

		private Tuple<string, DriveInformation>[] GetAllDriveLetters()
		{
			var task = ActorService.Get<CoreServiceProxyActor>()
				.Ask<GetPluggedInUsbDrivesResponseMessage>(MessageExtensions.CreateRequestMessage(
					new GetPluggedInUsbDrivesMessage()));
			try
			{
				task.Wait();
			}
			catch
			{
				return null;
			}

			var drives = new List<Tuple<string, DriveInformation>>();

			foreach (var drive in task.Result.DriveLetters)
			{
				var taskInfo = ActorService.Get<CoreServiceProxyActor>()
					.Ask<GetDriveInformationResponseMessage>(MessageExtensions.CreateRequestMessage(
						new GetDriveInformationMessage
						{
							DriveLetter = drive
						}));
				try
				{
					taskInfo.Wait();
				}
				catch (Exception)
				{

				}

				drives.Add(new Tuple<string, DriveInformation>(drive, taskInfo.Result.DriveInformation));
			}

			return drives.ToArray();
		}

		private string GetLetterToSaveHelper(ulong fileSize = 0)
		{
			var drives = GetAllDriveLetters();
			if (drives == null) return null;

			foreach (var drive in drives)
			{
				if (drive.Item2.FileSystemCompatible && drive.Item2.Writeable && drive.Item2.SpaceFree >= fileSize)
				{
					return drive.Item1;
				}
			}

			return null;
		}


		private string GetLetterToSave(ulong fileSize, out GenericFrontendPushMessage message)
		{
			var letterToSave = GetLetterToSaveHelper(fileSize);

			message = letterToSave == null ? CreateFrontendMessage(ExportMessage.ErrorNoDrive) : null;

			return letterToSave;
		}

		private GenericFrontendPushMessage CreateFrontendMessage(ExportMessage exportMessage, string messageText = "")
		{
			int messageStatusCode = MessageStatusCodes.ExportDataMessage;

			switch (exportMessage)
			{
				case ExportMessage.Success:
					messageText = string.Format(Resources.Export_Success_Generic);
					break;
				case ExportMessage.Error:
					messageText = string.Format(Resources.Export_Error_Generic);
					break;
				case ExportMessage.ErrorNoDrive:
					messageText = string.Format(Resources.Export_Error_NoDrive);
					break;
				case ExportMessage.ExportToNet:
					messageStatusCode = MessageStatusCodes.ExportDataMessage;
					break;
				case ExportMessage.ExportToNetPartial:
					messageText = string.Format(Resources.Files_Not_Exported_To_All);
					messageStatusCode = MessageStatusCodes.ExportedSomeResults;
					break;
			}

			return new GenericFrontendPushMessage
			{
				Parameters = new MessageParameters
				{
					Message = messageText,
					MessageType = MessageType.Info,
					DisplayType = MessageDisplayType.Toast,
					MessageCode = messageStatusCode
				}
			};
		}
	}
}
