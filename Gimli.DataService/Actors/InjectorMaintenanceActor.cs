﻿using System;
using System.Collections.Generic;
using Gimli.DataService.Messages;
using Gimli.JsonObjects.InjectorMaintenance;
using Gimli.JsonObjects.Settings;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages.InjectorMaintenance;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using Akka.Actor;
using Gimli.JsonObjects.Interfaces;

namespace Gimli.DataService.Actors
{
	public class InjectorMaintenanceActor : GimliActorBase
	{
	    private readonly ISystemDataStore mSystemDataStore;
	    private SettingsSession mSession = new SettingsSession();

		public InjectorMaintenanceActor(IActorService actorService, IActorsystemOverallState systemState, ISystemDataStore systemDataStore) : base(actorService, systemState)
		{
		    mSystemDataStore = systemDataStore;
		    Receive<RequestMessage<InjectorActionMessage>>(message =>
		    {
		        switch (message.Model.Action.Mode)
		        {
		            case InjectionMode.Prime:
		                break;
		            case InjectionMode.Rinse:
		                break;
		            case InjectionMode.RevPrime:
		                break;
		            case InjectionMode.Wash:
                        //TODO: save washsettings (good place to save as well)
                        actorService.Get<InstrumentServiceProxyActor>().Forward(message);
		                break;
		            case InjectionMode.LoadSettings:
		                break;
		            case InjectionMode.SaveSettings:
                        mSession.Settings = ((SaveSettings)message.Model.Action.InjectionAction).Settings;
                        actorService.Get<DataServiceRoutingActor>().Tell(SaveInjectorSettingsMessage.Generate(mSession.Settings.FluidVolume));
		                break;
		            case InjectionMode.LoadTimestamp:
		                break;
		            case InjectionMode.SaveTimestamp:
		                break;
		            case InjectionMode.ResetVolumeCounter:
		                break;
		            //case InjectionMode.DoCalibrationTubing:
		            //    break;
		            //case InjectionMode.SetCalibrationWeight:
		            //    break;
		            default:
		                throw new ArgumentOutOfRangeException();
		        }

		    });

            Receive<RequestMessage<GetInjectorSettingsMessage>>(message =>
            {
                Sender.Tell(GetGuaranteedAnswer(GetAnswerForSettingsRequest, message.Model));
            });
        }

	    private GimliResponse GetAnswerForSettingsRequest(GetInjectorSettingsMessage message)
	    {
	        return new GetInjectorSettingsResponseMessage { Volume = ReadWashVolumes() };
	    }

	    private class SettingsSession
	    {
	        internal InjectorMaintenanceSettings Settings { get; set; }
	    }

	    private List<int> ReadWashVolumes()
	    {
	        var volList = new List<int>();
	        var strVolumes = mSystemDataStore.LoadConfigFile("ConfigSettings.txt").Split(':');
            foreach (var strVolume in strVolumes)
            {
                int vol;
                int.TryParse(strVolume, out vol);
                volList.Add(vol);
            }
	        return volList;
	    } 
	}
}
