﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.DI.Core;
using Akka.Routing;
using Gimli.DataService.Messages;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Utils;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.ExportImport;
using Gimli.Shared.Actors.Messages.InjectorMaintenance;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using Gimli.Shared.Actors.Messages.Outbound;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.DataService.Actors
{
	public class DataServiceRoutingActor : ToplevelRoutingActorBase
	{
		protected const string DocumentFetcherName = nameof(ProtocolResultFetchActor) + "Router";
		private const string StoreDataRoutingActorName = nameof(StoreDataRoutingActor);
		private const string EditDocumentActorName = nameof(EditDocumentActor);
	    private const string UserActorName = nameof(UserActor);
	    private const string DataExportImportActorName = nameof(DataExportImportActor);
	    private const string DocumentConversionActorName = nameof(DocumentConversionActor);
	    private const string OutboundConnectionActorName = nameof(OutboundConnectionActor);
	    private const string InjectorMaintenanceActorName = nameof(InjectorMaintenanceActor);
		const string SystemInitActorName = nameof(SystemInitActor);

		public DataServiceRoutingActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			Receive<RequestMessage<InitRoutingActorMessage>>(message =>
			{
				WriteDummydata(systemState);
			});

			Receive<RequestMessage<ProtocolResultMessage>>(message =>
			{
				GetDocumentFetcher().Forward(message);
			});

			#region EditDocumentActor

			Receive<RequestMessage<SetQuickreadSlotMessage>>(message =>
			{
				GetChild<EditDocumentActor>(EditDocumentActorName).Forward(message);
			});

			Receive<RequestMessage<RenameDocumentMessage>>(message =>
			{
				GetChild<EditDocumentActor>(EditDocumentActorName).Forward(message);
			});

			Receive<RequestMessage<CreateNewProtocolMessage>>(message =>
			{
				GetChild<EditDocumentActor>(EditDocumentActorName).Forward(message);
			});

			Receive<RequestMessage<GetInputForReadMessage>>(message =>
			{
				GetChild<EditDocumentActor>(EditDocumentActorName).Forward(message);
			});

			Receive<RequestMessage<CopyPlateMessage>>(message =>
			{
				GetChild<EditDocumentActor>(EditDocumentActorName).Forward(message);
			});

			Receive<RequestMessage<DeletePlateMessage>>(message =>
			{
				GetChild<EditDocumentActor>(EditDocumentActorName).Forward(message);
			});

			Receive<RequestMessage<RenamePlateMessage>>(message =>
			{
				GetChild<EditDocumentActor>(EditDocumentActorName).Forward(message);
			});

			Receive<RequestMessage<UpdateDocumentWithSettingsMessage>>(message =>
			{
				GetChild<EditDocumentActor>(EditDocumentActorName).Forward(message);
			});

			Receive<RequestMessage<CreateProtocolFromDocumentMessage>>(message =>
			{
				GetChild<EditDocumentActor>(EditDocumentActorName).Forward(message);
			});

			Receive<RequestMessage<UpdateDocumentWithReductionSettingsMessage>>(message =>
			{
				GetChild<EditDocumentActor>(EditDocumentActorName).Forward(message);
			});

			#endregion EditDocumentActor

			#region UserActor

			Receive<RequestMessage<FrontendViewChangeMessage>>(message =>
		    {
                GetChild<UserActor>(UserActorName).Forward(message);
		    });

			Receive<RequestMessage<SetCurrentUserMessage>>(message =>
			{
				GetChild<UserActor>(UserActorName).Forward(message);
			});

			Receive<RequestMessage<LoginUserMessage>>(message =>
			{
				GetChild<UserActor>(UserActorName).Forward(message);
			});

            Receive<RequestMessage<LogoutUserMessage>>(message =>
            {
                GetChild<UserActor>(UserActorName).Forward(message);
            });

            Receive<RequestMessage<NfcPairingResultNotificationMessage>>(message =>
			{
				GetChild<UserActor>(UserActorName).Forward(message);
			});
            
		    Receive<RequestMessage<LockUnlockMessage>>(message =>
		    {
                GetChild<UserActor>(UserActorName).Forward(message);
            });

		    Receive<RequestMessage<IsLockedMessage>>(message =>
		    {
                GetChild<UserActor>(UserActorName).Forward(message);
            });

			#endregion UserActor

			#region StoreDataRoutingActor

			Receive<RequestMessage<GetCurrentUserMessage>>(message =>
            {
                GetChild<UserActor>(UserActorName).Forward(message);
            });

            Receive<RequestMessage<ValidateUserFullMessage>>(message =>
            {
                GetChild<UserActor>(UserActorName).Forward(message);
            });

            Receive<RequestMessage<AddUserMessage>>(message =>
            {
                GetChild<StoreDataRoutingActor>(StoreDataRoutingActorName).Forward(message);
            });

            Receive<RequestMessage<ChangeUserMessage>>(message =>
            {
                GetChild<StoreDataRoutingActor>(StoreDataRoutingActorName).Forward(message);
            });

            Receive<RequestMessage<DeleteUserMessage>>(message =>
            {
				GetChild<StoreDataRoutingActor>(StoreDataRoutingActorName).Forward(message);
            });

            Receive<RequestMessage<GetUsersMessage>>(message =>
            {
                GetChild<StoreDataRoutingActor>(StoreDataRoutingActorName).Forward(message);
            });

            Receive<RequestMessage<StoreDocumentMessage>>(message =>
			{
				GetChild<StoreDataRoutingActor>(StoreDataRoutingActorName).Forward(message);
			});

			Receive<RequestMessage<UpdateDocumentWithPlateMessage>>(message =>
			{
				GetChild<StoreDataRoutingActor>(StoreDataRoutingActorName).Forward(message);
			});

			Receive<RequestMessage<RemoteRegistrationMessage>>(message =>
			{
				GetChild<StoreDataRoutingActor>(StoreDataRoutingActorName).Forward(message);
			});

			Receive<RequestMessage<SendDocumentToRemoteMessage>>(message =>
			{
				GetChild<StoreDataRoutingActor>(StoreDataRoutingActorName).Forward(message);
			});

			Receive<ThreadsafeGetListOfItemsMessage>(message =>
			{
				GetChild<StoreDataRoutingActor>(StoreDataRoutingActorName).Forward(message);
			});

            Receive<RequestMessage<CheckPinMessage>>(message =>
            {
                GetChild<StoreDataRoutingActor>(UserActorName).Forward(message);
            });

			#endregion StoreDataRoutingActor

			Receive<RequestMessage<DeleteDocumentsMessage>>(message =>
			{
				if (message.Model.OnUsb) GetChild<DataExportImportActor>(DataExportImportActorName).Forward(message);
				else GetChild<StoreDataRoutingActor>(StoreDataRoutingActorName).Forward(message);
			});

			#region DataExportImportActor

			Receive<RequestMessage<ExportFilesMessage>>(message =>
		    {
				GetChild<DataExportImportActor>(DataExportImportActorName).Forward(message);
            });

			Receive<RequestMessage<ToggleExportExcelMessage>>(message =>
			{
				GetChild<DataExportImportActor>(DataExportImportActorName).Forward(message);
			});

			Receive<RequestMessage<GetGlobalConfigurationMessage>>(message =>
			{
				GetChild<DataExportImportActor>(DataExportImportActorName).Forward(message);
			});

            Receive<RequestMessage<CopyFilesMessage>>(message =>
            {
                GetChild<DataExportImportActor>(DataExportImportActorName).Forward(message);
            });

			Receive<RequestMessage<ImportFilesMessage>>(message =>
			{
				GetChild<DataExportImportActor>(DataExportImportActorName).Forward(message);
			});

			Receive<RequestMessage<UsbDrivePluggedInMessage>>(message =>
			{
				GetChild<DataExportImportActor>(DataExportImportActorName).Forward(message);
			});

			Receive<RequestMessage<UsbDrivePluggedOutMessage>>(message =>
			{
				GetChild<DataExportImportActor>(DataExportImportActorName).Forward(message);
			});

			#endregion DataExportImportActor

			Receive<RequestMessage<GetDocumentAsMessage>>(message =>
		    {
                GetChild<DocumentConversionActor>(DocumentConversionActorName).Forward(message);
		    });

			#region OutboundConnectionActor

			Receive<RequestMessage<GxpLoginMessage>>(message =>
			{
				GetChild<OutboundConnectionActor>(OutboundConnectionActorName).Forward(message);
			});
			Receive<RequestMessage<GxpLogoutMessage>>(message =>
			{
				GetChild<OutboundConnectionActor>(OutboundConnectionActorName).Forward(message);
			});
			Receive<RequestMessage<GxpModeMessage>>(message =>
			{
				GetChild<OutboundConnectionActor>(OutboundConnectionActorName).Forward(message);
			});
			Receive<RequestMessage<OutboundDocumentInfoMessage>>(message =>
			{
				GetChild<OutboundConnectionActor>(OutboundConnectionActorName).Forward(message);
			});
			Receive<RequestMessage<OutboundDataMessage>>(message =>
			{
				GetChild<OutboundConnectionActor>(OutboundConnectionActorName).Forward(message);
			});
			Receive<RequestMessage<OutboundControlledMessage>>(message =>
			{
				GetChild<OutboundConnectionActor>(OutboundConnectionActorName).Forward(message);
			});
			Receive<RequestMessage<GetOutboundSessionMessage>>(message =>
			{
				GetChild<OutboundConnectionActor>(OutboundConnectionActorName).Forward(message);
			});

			#endregion OutboundConnectionActor

			Receive<RequestMessage<GetInjectorSettingsMessage>>(message =>
			{
				GetChild<InjectorMaintenanceActor>(InjectorMaintenanceActorName).Forward(message);
			});
		}

		/// <summary>
		/// Made protected for testing.
		/// </summary>
		/// <returns></returns>
		protected virtual IActorRef GetDocumentFetcher()
		{
			return Context.Child(DocumentFetcherName);
		}

		protected override IActor GetSysInitActor()
		{
			return GetChild<SystemInitActor>(SystemInitActorName);
		}

		protected override void PreStart()
		{
			// init actor pools
			CreateDocumentFetcherPool();

			GetChild<StoreDataRoutingActor>(StoreDataRoutingActorName);
			GetChild<EditDocumentActor>(EditDocumentActorName);
		    GetChild<UserActor>(UserActorName);
		    GetChild<DataExportImportActor>(DataExportImportActorName);
		    GetChild<DocumentConversionActor>(DocumentConversionActorName);
		    GetChild<OutboundConnectionActor>(OutboundConnectionActorName);
			GetChild<InjectorMaintenanceActor>(InjectorMaintenanceActorName);

			base.PreStart();
		}

		protected virtual void CreateDocumentFetcherPool()
		{
			if (!Context.Child(DocumentFetcherName).IsNobody()) return;

			int parallelRequests;
			var success = int.TryParse(AppConfigurationManager.GetSettingsValue("NumberOfParallelDocumentRequests"), out parallelRequests);
			if (!success) parallelRequests = 2;

			ActorService.Register<ProtocolResultFetchActor>();
			Context.ActorOf(
				Context.DI().Props(typeof (ProtocolResultFetchActor)).WithRouter(new RoundRobinPool(parallelRequests)),
				DocumentFetcherName);
		}

	    private void CopyProtocolsFromDepot(string typeDirectory)
	    {
            var rewriteQuick = new DirectoryInfo(DataSetHelper.RootPath + typeDirectory);
            if (!rewriteQuick.Exists || rewriteQuick.GetFiles("*.xml").Length < 2)
            {
                var px = new Process();
                var pxInfo =
                    new ProcessStartInfo(Environment.GetFolderPath(Environment.SpecialFolder.System) + Path.DirectorySeparatorChar + @"xcopy.exe",
                        "/D /f /s /r /y /C /I " + (char) 34 + Environment.CurrentDirectory +
                        @"/Additionals/ProgramData/Molecular Devices/GimliData/" + typeDirectory + Path.DirectorySeparatorChar + @"*.*" + (char)34);
				var destDirectory = (char)34 + DataSetHelper.RootPath + typeDirectory + Path.DirectorySeparatorChar;
                pxInfo.Arguments += " " + destDirectory + (char)34;
                pxInfo.WindowStyle = ProcessWindowStyle.Hidden;
                
                try
                {
                    //So that copied protocols are added to index file
                    if (File.Exists(destDirectory + "ListOfItems.xml" + (char)34))
                        File.Delete(destDirectory + "ListOfItems.xml" + (char)34);
                    px.StartInfo = pxInfo;
                    px.Start();

                }
                catch (Exception exc)
                {
                    Trace.WriteLine(exc.Message);
                }
            }
        }
		private void WriteDummydata(IActorsystemOverallState systemState)
		{
			systemState.InitState = InitFinishedState.Initializing;
			Task.Run(() =>
			{
				try
				{
					PrepareForDatastore();
				}
				catch (Exception e)
				{
					Log.Error("Exception thrown while preparing data for datastore (copy stuff). " + e);
					SystemState.InitState = InitFinishedState.FinishedWithError;
					// TODO: what now??? Restart service?
				}

				systemState.InitLevel = InitLevel.L3_Operational;

				TellAppManagerInitStateLevel(systemState.InitLevel, systemState.InitState);
			});
		}

		private void PrepareForDatastore()
		{
			// sets Public as default user
			GetChild<UserActor>(UserActorName)
				.Ask<GimliResponse>(MessageExtensions.CreateRequestMessage(new GetCurrentUserMessage()))
				.Wait();

			CopyProtocolsFromDepot("00000000-0000-0000-0000-000000000001");
			CopyProtocolsFromDepot("00000000-0000-0000-0000-000000000002");

			SystemState.InitState = InitFinishedState.FinishedWithSuccess;
		}
	}
}
