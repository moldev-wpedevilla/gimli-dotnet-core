﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Gimli.DataService.Properties;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.DataService.Actors
{
	public class UserActor : GimliActorBase
	{
	    private UserFull mCurrentUser;
	    private bool mLocked;

		public UserActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
            Receive<RequestMessage<SetCurrentUserMessage>>(message =>
		    {
                var sender = Sender;
                sender.Tell(GetGuaranteedAnswer(SetCurrentUser, message));
            });

            Receive<RequestMessage<GetCurrentUserMessage>>(message =>
            {
                var sender = Sender;
                sender.Tell(GetGuaranteedAnswer(GetCurrentUser, message));
            });

		    Receive<RequestMessage<ValidateUserFullMessage>>(message =>
		    {
                var sender = Sender;
                sender.Tell(GetGuaranteedAnswer(ValidateUserFull, message));
            });

			Receive<RequestMessage<NfcPairingResultNotificationMessage>>(message =>
			{
				var task = ActorService.Get<DataServiceRoutingActor>().Ask<GetUsersResponseMessage>(MessageExtensions.CreateRequestMessage(new GetUsersMessage()));
				task.Wait();
				if (task.Result.Users.Where(u => u.HasNfc).Select(u => u.NfcId).Contains(message.Model.Result.User.NfcId))
				{
					message.Model.WasSuccessful = false;
					message.Model.Result.ErrorMessage = string.Format(Resources.ResourceManager.GetString("Validate_NfcId_Error_Exists"), task.Result.Users.First(u => u.NfcId == message.Model.Result.User.NfcId).UserName);
					ActorService.Get<WebServiceProxyActor>().Tell(message);
				}
				else
				{
					ActorService.Get<WebServiceProxyActor>().Forward(message);
				}
			});

			Receive<RequestMessage<LoginUserMessage>>(message =>
			{
				if (message.Model.Id == Guid.Empty) return;

				var task = ActorService.Get<DataServiceRoutingActor>().Ask<GetUsersResponseMessage>(MessageExtensions.CreateRequestMessage(new GetUsersMessage()));
				task.Wait();
				UserFull newUser = null;
				switch (message.Model.Type)
				{
					case LoginIdType.None:
						break;
					case LoginIdType.Nfc:
						newUser = task.Result.Users.FirstOrDefault(u => u.HasNfc && u.NfcId == message.Model.Id);
                        break;
					case LoginIdType.User:
						newUser = task.Result.Users.FirstOrDefault(u => u.Id == message.Model.Id);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				if (newUser == null)
				{
					actorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
					{
						Parameters = new MessageParameters
						{
							Message = Resources.ResourceManager.GetString("Nfc_Tag_Not_Assigned"),
							MessageType = MessageType.Info,
							DisplayType = MessageDisplayType.Toast,
							MessageCode = MessageStatusCodes.NfcTagNotAssigned,
						}
					}));
					return;
				}
				mCurrentUser = newUser;

				ActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new NotifyUserLogedinMessage {User = mCurrentUser}));
			});

		    Receive<RequestMessage<LogoutUserMessage>>(message =>
		    {
		        var sender = Sender;
                sender.Tell(GetGuaranteedAnswer(LogoutUser, message));
            });

			Receive<RequestMessage<FrontendViewChangeMessage>>(message =>
			{

			});

		    Receive<RequestMessage<LockUnlockMessage>>(message =>
		    {
                var sender = Sender;
                sender.Tell(GetGuaranteedAnswer(LockUnlock, message));
            });

		    Receive<RequestMessage<IsLockedMessage>>(message =>
		    {
                var sender = Sender;
                sender.Tell(GetGuaranteedAnswer(GetIsLocked, message));
            });

		}

		private GimliResponse SetCurrentUser(RequestMessage<SetCurrentUserMessage> message)
		{
			var task = GetUserList();

			if (message.Model.Username != null && task.Result.Users.Any(x => x.UserName == message.Model.Username))
			{
				mCurrentUser = task.Result.Users.FirstOrDefault(x => x.UserName == message.Model.Username);
				return new GimliResponse();
			}
			if (message.Model.Id != Guid.Empty && task.Result.Users.Any(x => x.Id == message.Model.Id))
			{
				mCurrentUser = task.Result.Users.FirstOrDefault(x => x.Id == message.Model.Id);
				return new GimliResponse();
			}

			return new GimliResponse(new ErrorInfo {Message = Resources.Validate_Username_Error_Exists_Not}, ResultStatusEnum.Error, Log);
		}

		private Task<GetUsersResponseMessage> GetUserList()
		{
			var task =
				ActorService.Get<DataServiceRoutingActor>()
					.Ask<GetUsersResponseMessage>(MessageExtensions.CreateRequestMessage(new GetUsersMessage()));
			task.Wait();
			return task;
		}

	    private GimliResponse LogoutUser(RequestMessage<LogoutUserMessage> message)
	    {
            mCurrentUser = null;

            if (mLocked)
            {
                mLocked = false;
            }

            return new GimliResponse();
        }

	    private GimliResponse LockUnlock(RequestMessage<LockUnlockMessage> message)
	    {
            mLocked = message.Model.Lock;
            return new GimliResponse();
        }

	    private IsLockedResponseMessage GetIsLocked(RequestMessage<IsLockedMessage> message)
	    {
	        return new IsLockedResponseMessage {Locked = mLocked};
	    }

		private GimliResponse GetCurrentUser(RequestMessage<GetCurrentUserMessage> message)
		{
			return new GetCurrentUserResponseMessage
			{
				UserFull = mCurrentUser
			};
		}

		private GimliResponse ValidateUserFull(RequestMessage<ValidateUserFullMessage> message)
		{
			if (message.Model.UserFull.UserName.Trim() == string.Empty)
			{
				return new GimliResponse(new ErrorInfo {Message = Resources.Validate_Username_Error_Empty}, ResultStatusEnum.Error, Log);
			}

			var task = ActorService.Get<DataServiceRoutingActor>().Ask<GetUsersResponseMessage>(MessageExtensions.CreateRequestMessage(new GetUsersMessage()));
			task.Wait();

			if (task.Result.Users.Any(x => x.UserName == message.Model.UserFull.UserName))
			{
				return new GimliResponse(new ErrorInfo {Message = Resources.Validate_Username_Error_Exists}, ResultStatusEnum.Error, Log);
			}

			UserFull user;

			if (message.Model.UserFull.NfcId != Guid.Empty && (user = task.Result.Users.First(x => x.NfcId == message.Model.UserFull.NfcId)) != null)
			{
				return new GimliResponse(new ErrorInfo {Message = string.Format(Resources.Validate_NfcId_Error_Exists, user.UserName)}, ResultStatusEnum.Error, Log);
			}

			return new GimliResponse();
		}
	}
}
