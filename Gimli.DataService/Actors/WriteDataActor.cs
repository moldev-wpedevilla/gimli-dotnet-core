﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Akka.Actor;
using Gimli.DataService.Messages;
using Gimli.DataService.Properties;
using Gimli.DataStoreFileBased;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Enums;
using Gimli.JsonObjects.Interfaces;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.DataService.Actors
{
	public class WriteDataActor : GimliActorBase
	{
        private static DateTime StartTime = DateTime.Now;  //excel-abuse
        private readonly IDataStore mStore;
	    private readonly ISystemDataStore mSysStore;
	    
		public WriteDataActor(IActorService actorService, IDataStore store, IActorsystemOverallState systemState, ISystemDataStore sysStore/* = null*/) : base(actorService, systemState)
		{
            mStore = store;
		    mSysStore = sysStore;

			Receive<RequestMessage<StoreDocumentMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(StoreDocument, message));
			});

			Receive<RequestMessage<DeleteDocumentsMessage>>(message =>
			{
			    var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(DeleteDocuments, message));
			});

		    Receive<RequestMessage<GetUsersMessage>>(message =>
		    {
				// Is in WriteDataActor because implicitely creates a new user if none exists 
		        var sender = Sender;
                sender.Tell(GetGuaranteedAnswer(GetUsers, message));
		    });

		    Receive<RequestMessage<AddUserMessage>>(message =>
		    {
                var sender = Sender;
                sender.Tell(GetGuaranteedAnswer(AddUser, message));
            });

            Receive<RequestMessage<ChangeUserMessage>>(message =>
            {
                var sender = Sender;
                sender.Tell(GetGuaranteedAnswer(ChangeUser, message));
            });

            Receive<RequestMessage<DeleteUserMessage>>(message =>
            {
                var sender = Sender;
                sender.Tell(GetGuaranteedAnswer(DeleteUser, message));
            });

			Receive<ThreadsafeGetListOfItemsMessage>(message =>
			{
				// Is in WriteDataActor because implicitely creates a list of items if none exists
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(m => new ThreadsafeGetListOfItemsResponseMessage
				{
					ProtocolListItems = mStore.GetListOfItems(m.User, m.DocumentType, m.IncludePublic)
				}, message));
			});

			Receive</*RequestMessage<*/SaveInjectorSettingsMessage/*>*/>(message =>
            {
                var sender = Sender;
                sender.Tell(GetGuaranteedAnswer(SaveWashSettings, message));
            });


            Receive<RequestMessage<CheckPinMessage>>(message =>
            {
                Sender.Tell(GetGuaranteedAnswer(m =>
                    CheckPin(message.Model.UserId, message.Model.Pin)
                , message));
            });
        }

        protected GimliResponse StoreDocument(RequestMessage<StoreDocumentMessage> message)
		{
			bool success;
			using (SystemState.DisableShutdown(ShutdownForbiddenReason.WritingToFilesystem))
			{
				success = mStore.SaveItem(message.Model.Document);
			}
			return success
				? new GimliResponse()
				: new GimliResponse(
					new ErrorInfo {Details = string.Format(Resources.Failed_saving_document, message.Model.Document.DisplayName)},
					ResultStatusEnum.Error, Log);
		}

		protected GimliResponse DeleteDocuments(RequestMessage<DeleteDocumentsMessage> message)
		{
		    using (SystemState.DisableShutdown(ShutdownForbiddenReason.WritingToFilesystem))
		    {
		        var successMessage =
		        MessageExtensions.CreateRequestMessage(new DeleteDocumentsMessage {Documents = new List<Guid>()});
				var result =
				    ActorService.Get<DataServiceRoutingActor>()
					    .Ask<GetCurrentUserResponseMessage>(MessageExtensions.CreateRequestMessage(new GetCurrentUserMessage()), 10000);
				result.Wait();

				var editableDocuments =
				    mStore.GetListOfItems(result.Result.UserFull, ItemType.Protocol | ItemType.Result, false).Select(d => d.Id);
				var documentsToDelete = message.Model.Documents.Where(d => editableDocuments.Contains(d));
				foreach (var document in documentsToDelete)
				{
				    var success = mStore.DeleteItem(null, document.ToString());
				    if (success)
				    {
					    successMessage.Model.Documents.Add(document);
				    }
				}

			    ActorService.Get<WebServiceProxyActor>().Ask<GimliResponse>(successMessage);
		    }

            return new GimliResponse();
		}

        protected GimliResponse DeleteUser(RequestMessage<DeleteUserMessage> message)
        {
            using (SystemState.DisableShutdown(ShutdownForbiddenReason.WritingToFilesystem))
	        {
		        var success = mStore.DeleteUser(message.Model.UserFull);

                if (!success)
                {
                    return new GimliResponse(new ErrorInfo { Message = "Cannot delete user" }, ResultStatusEnum.Error, Log);
                }
                return new GimliResponse();
	        }
        }

	    protected GetUsersResponseMessage GetUsers(RequestMessage<GetUsersMessage> message)
	    {
	        if (message.Model.Username != null)
            {
                return new GetUsersResponseMessage { Users = mStore.GetAllUserCredentials().Where(x => x.UserName == message.Model.Username).ToList().ConvertAll(UserFeToUserFull).ToList() };
            }
            if (message.Model.Id != Guid.Empty)
            {
                return new GetUsersResponseMessage { Users = mStore.GetAllUserCredentials().Where(x => x.Id == message.Model.Id).ToList().ConvertAll(UserFeToUserFull).ToList() };
            }

            return new GetUsersResponseMessage { Users = mStore.GetAllUserCredentials().ToList().ConvertAll(UserFeToUserFull).ToList() };
	    }

	    protected GimliResponse AddUser(RequestMessage<AddUserMessage> message)
	    {
	        var success = mStore.SaveUserCredentials(new UserFullExtended(message.Model.UserFull), message.Model.Pin);

	        if (!success)
	        {
                return new GimliResponse(new ErrorInfo { Message = "Cannot add user" }, ResultStatusEnum.Error, Log);
	        }
            return new GimliResponse();
	    }

	    protected GimliResponse ChangeUser(RequestMessage<ChangeUserMessage> message)
	    {
	        var userFe = new UserFullExtended(message.Model.UserFullNew)
	        {
	            Pin = message.Model.PinNew
	        };

	        if (message.Model.UserFullNew.Id == GlobalConstants.AdminGuid)
	        {
	            if (message.Model.ResetAdminPin)
	            {
                    userFe.Pin = GlobalConstants.AdminPin;
                }
            }

	        var success = mStore.SaveUserCredentials(userFe, userFe.Pin);

	        return !success
	            ? new GimliResponse(new ErrorInfo {Message = "Cannot change user"}, ResultStatusEnum.Error, Log)
	            : new GimliResponse();
	    }

        protected GimliResponse SaveWashSettings(/*RequestMessage<*/SaveInjectorSettingsMessage/*>*/ message)
        {
            var strBuilder = new StringBuilder();
            for (var volIndex = 0; volIndex < 4; volIndex++)
            {

                strBuilder.Append(message.mVolume[volIndex].ToString());
                if (volIndex < 3)
                {
                    strBuilder.Append(':');
                }
            }
            var content = strBuilder.ToString();

            var success = ((SystemDataStore) mSysStore).SaveConfigFile("ConfigSettings.txt", /*"tEsT.txt"*/content);

            if (!success)
            {
                return new GimliResponse(new ErrorInfo { Message = "Cannot write config" }, ResultStatusEnum.Error, Log);
            }
            return new GimliResponse();
        }

	    protected GimliResponse CheckPin(Guid userId, string pin)
	    {
	        if (mStore.CheckPin(userId, pin))
	        {
                return new GimliResponse();
            }

            return new GimliResponse(new ErrorInfo { Message = "Wrong pin" }, ResultStatusEnum.Error, Log);
        }

	    private static UserFull UserFeToUserFull(UserFull userFe)
	    {
	        return new UserFull
	        {
                Id = userFe.Id,
                UserName = userFe.UserName,
                HasNfc = userFe.HasNfc,
                HasPin = userFe.HasPin,
                IsDefaultPin = userFe.IsDefaultPin,
                NfcId = userFe.NfcId
	        };
	    }
    }
}
