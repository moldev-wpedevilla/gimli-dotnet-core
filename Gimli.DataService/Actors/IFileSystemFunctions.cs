﻿using System.IO;

namespace Gimli.DataService.Actors
{
	public interface IFileSystemFunctions
	{
		ulong GetFileSize(string fileName);
	    ulong GetFileSize(string[] fileNames);
	    ulong GetFileSize(DirectoryInfo directoryInfo);
		string[] GetFilesInDirectory(DirectoryInfo directory);
	    void DeleteFiles(string[] files);
        void ZipFiles(string[] filenames, DirectoryInfo directoryInfoSource, string fileNameTarget);
        bool CopyFiles(DirectoryInfo source, DirectoryInfo target, string[] filenames, bool overwrite = true);
		bool DoesDirectoryExist(string filename);
		bool DoesFileExist(string filename);
		void DeleteDirectory(string path, bool recursive);
	}
}
