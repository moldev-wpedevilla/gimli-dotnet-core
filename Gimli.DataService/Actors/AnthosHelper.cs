﻿using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;

namespace Gimli.DataService.Actors
{
	internal class AnthosHelper : IAnthosFunctions
	{
		private readonly IActorService mActorService;

		public AnthosHelper(IActorService actorService)
		{
			mActorService = actorService;
		}

		public void SetSystemValue(string key, object value)
		{
			mActorService.Get<InstrumentServiceProxyActor>().Tell(
				MessageExtensions.CreateRequestMessage(new SaveInApexConfigMessage
				{
					ConfigKey = key,
					Value = value,
				}));
		}
		
		public object GetSystemValue(string key, object defaultValue)
		{
			var task = mActorService.Get<InstrumentServiceProxyActor>()
				.Ask<ReadFromApexConfigMessage>(
					MessageExtensions.CreateRequestMessage(new ReadFromApexConfigMessage
					{
						ConfigKey = key,
						Value = defaultValue,
					}));
			task.Wait();
			return task.Result.Value;
		}
	}
}