﻿using Gimli.DataService.Messages;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.DataService.Actors
{
	public class StoreDataRoutingActor : GimliActorBase
	{
		private readonly string StoreDataActorName = nameof(WriteDataActor);
		private readonly string UpdateLiveDataActorName = nameof(LiveDataQueueProcessorActor);
		private readonly string ExportRemoteActorName = nameof(ExportRemoteActor);

        public StoreDataRoutingActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
	        Receive<RequestMessage<RemoteRegistrationMessage>>(message =>
	        {
		        GetChild<ExportRemoteActor>(ExportRemoteActorName).Forward(message);
	        });

			Receive<RequestMessage<StoreDocumentMessage>>(message =>
			{
				var child = message.Model.Strategy == StorageStrategy.Local
					? GetChild<WriteDataActor>(StoreDataActorName)
					: GetChild<ExportRemoteActor>(ExportRemoteActorName);
				child.Forward(message);
			});

			Receive<RequestMessage<SendDocumentToRemoteMessage>>(message =>
			{
				GetChild<ExportRemoteActor>(ExportRemoteActorName).Forward(message);
			});

			Receive<RequestMessage<UpdateDocumentWithPlateMessage>>(message =>
			{
				GetChild<LiveDataQueueProcessorActor>(UpdateLiveDataActorName).Forward(message);
			});

			Receive<RequestMessage<DeleteDocumentsMessage>>(message =>
			{
				GetChild<WriteDataActor>(StoreDataActorName).Forward(message);
			});

		    Receive<RequestMessage<GetUsersMessage>>(message =>
		    {
                GetChild<WriteDataActor>(StoreDataActorName).Forward(message);
            });

            Receive<RequestMessage<AddUserMessage>>(message =>
            {
                GetChild<WriteDataActor>(StoreDataActorName).Forward(message);
            });

            Receive<RequestMessage<ChangeUserMessage>>(message =>
            {
                GetChild<WriteDataActor>(StoreDataActorName).Forward(message);
            });
            
            Receive<RequestMessage<DeleteUserMessage>>(message =>
            {
                GetChild<WriteDataActor>(StoreDataActorName).Forward(message);
            });

            Receive<SaveInjectorSettingsMessage>(message =>
            {
                GetChild<WriteDataActor>(StoreDataActorName).Forward(message);
            });

			Receive<ThreadsafeGetListOfItemsMessage>(message =>
			{
				GetChild<WriteDataActor>(StoreDataActorName).Forward(message);
			});

            Receive<RequestMessage<CheckPinMessage>>(message =>
            {
                GetChild<WriteDataActor>(StoreDataActorName).Forward(message);
            });
        }

		protected override void PreStart()
		{
			GetChild<ExportRemoteActor>(ExportRemoteActorName);
			GetChild<LiveDataQueueProcessorActor>(UpdateLiveDataActorName);
			GetChild<WriteDataActor>(StoreDataActorName);

			base.PreStart();
		}
	}
}
