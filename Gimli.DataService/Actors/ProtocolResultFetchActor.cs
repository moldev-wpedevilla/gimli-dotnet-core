﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Akka.Actor;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Enums;
using Gimli.JsonObjects.Interfaces;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.ExportImport;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.DataService.Actors
{
	public class ProtocolResultFetchActor : GimliActorBase
	{
		private static readonly Dictionary<Guid, ProtocolListItem> Document2ListItemMap = new Dictionary<Guid, ProtocolListItem>();
        private const int NumberOfQrSlots = 4;
		private readonly IDataStore mStore;
		private readonly IMeasurementContext mMeasurementContext;

		public ProtocolResultFetchActor(IActorService actorService, IDataStore store, IMeasurementContext measurementContext, IActorsystemOverallState systemState ) : base(actorService, systemState)
		{
			mStore = store;
			mMeasurementContext = measurementContext;

			Receive<RequestMessage<ProtocolResultMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(GetAnswerForDocumentRequest, message));
			});
		}

		protected GimliResponse GetAnswerForDocumentRequest(RequestMessage<ProtocolResultMessage> message)
		{
			GimliResponse errorResponse = null;
			string validResponse = null;
			switch (message.Model.Type)
			{
				case ProtocolResultMessageType.GetProtocol:
				case ProtocolResultMessageType.GetResult:

					var resultContent = GetSingleDocument(message.Model.Id, message.Model.UserFull);
					if (resultContent != null)
					{
						if (message.Model.Type == ProtocolResultMessageType.GetResult)
						{
							resultContent.RestrictDataToWellGroup(message.Model.WellGroup);
						}
						if (message.Model.WellGroup == null)
						{
							var task = ActorService.Get<InstrumentServiceProxyActor>()
								.Ask<GimliResponse>(
									MessageExtensions.CreateRequestMessage(new UpdateDocumentWithInstrumentSettingsMessage
									{
										Document = resultContent
									}), 10000);

							task.Wait();
							if (task.Result.ResponseStatus != ResultStatusEnum.Success)
							{
								return task.Result;
							}

							var typedResponse = (UpdateDocumentWithInstrumentSettingsResponseMessage)task.Result;
							resultContent = typedResponse.Document;
						}

						validResponse = resultContent.JsonSerializeToCamelCase(errorText => Log.Error(errorText));
						break;
					}

					errorResponse = CreateErrorResponse(message);
					break;
				case ProtocolResultMessageType.GetQuickProtocol:
					validResponse = GetPinnedSlotList(message.Model.UserFull).JsonSerializeToCamelCase();
					break;
				case ProtocolResultMessageType.GetLastUsedProtocolList:
					var lastDocuments = ActorService.GetListOfItems(message.Model.UserFull, ItemType.LastUpdated, false);
					var last = new LastUsedDocuments
					{
						Results = lastDocuments.Where(d => d.HasData).Take(message.Model.NumberOfItemsRequested).ToList(),
						Protocols = lastDocuments.Where(d => !d.HasData).Take(message.Model.NumberOfItemsRequested).ToList(),
						MixedDocuments = lastDocuments.ToList()
					};

					validResponse = last.JsonSerializeToCamelCase();
					break;
				case ProtocolResultMessageType.GetStandardProtocolList:
					var stdProtocols = ActorService.GetListOfItems(message.Model.UserFull, ItemType.Standard, false).ToList();
					validResponse = message.Model.Category != null && message.Model.Category != GlobalConstants.CategoriesAllDocuments
						? GetProtocolsForCategory(message, stdProtocols).JsonSerializeToCamelCase()
						: stdProtocols.JsonSerializeToCamelCase();
					break;
                case ProtocolResultMessageType.GetProtocolList:
					var userProtocols = ActorService.GetListOfItems(message.Model.UserFull, ItemType.Protocol, false).ToList();
					validResponse = message.Model.Category != null
						? GetProtocolsForCategory(message, userProtocols).JsonSerializeToCamelCase()
						: userProtocols.JsonSerializeToCamelCase();
					break;
				case ProtocolResultMessageType.GetResultList:
					validResponse = ActorService.GetListOfItems(message.Model.UserFull, ItemType.Result, false)
						.ToList().JsonSerializeToCamelCase();
					break;
				case ProtocolResultMessageType.GetProtocolsForImportList:
					validResponse = ActorService.GetListOfItems(new UserFull { Id = GlobalConstants.ImportGuid }, ItemType.Protocol, false)
						.ToList().JsonSerializeToCamelCase();
					break;

				case ProtocolResultMessageType.GetResultsForImportList:
					validResponse = ActorService.GetListOfItems(new UserFull { Id = GlobalConstants.ImportGuid }, ItemType.Result, false)
						.ToList().JsonSerializeToCamelCase();
					break;
				case ProtocolResultMessageType.GetDocument:
				case ProtocolResultMessageType.GetEmptyDocument:
					var document = GetSingleDocument(message.Model.Id, message.Model.UserFull);
					if (document != null && message.Model.Type == ProtocolResultMessageType.GetEmptyDocument)
					{
						document.RestrictDataToWellGroup(null);
					}

					return document != null
						? new DocumentResponseMessage { Document = document }
						: CreateErrorResponse(message);
				case ProtocolResultMessageType.GetCategoriesListMy:
				case ProtocolResultMessageType.GetCategoriesListStandard:
			        var categories = mStore.GetListOfCategories(message.Model.UserFull.Id).OrderBy(i => i.Label);
			        
			        validResponse = categories.JsonSerializeToCamelCase();
					break;
				case ProtocolResultMessageType.GetCountsForImportDocuments:
					var response = ActorService.GetListOfItems(new UserFull {Id = GlobalConstants.ImportGuid}, ItemType.Protocol | ItemType.Result,
							false).ToList();
					return new DocumentsForImportMessage
					{
						Documents = new DocumentsForImport { NumberOfProtocols = response.Count(d => !d.HasData), NumberOfResults = response.Count(d => d.HasData) }
					};
				default:
					throw new ActorException("Unrecognized ProtocolResultRequest.");
			}

			return errorResponse ?? new ProtocolResultResponseMessage
			{
				JsonFormattedDocument = validResponse
			};
		}

		private static List<ProtocolListItem> GetProtocolsForCategory(RequestMessage<ProtocolResultMessage> message, List<ProtocolListItem> protocols)
		{
            var regExp = new Regex("[ ]{2,}", RegexOptions.None);
            var itemsInCategory = new List<ProtocolListItem>();

            switch (message.Model.Category)
		    {
                case GlobalConstants.CategoriesAllDocuments:
		            itemsInCategory = protocols;
		            break;
                default:
                    var protFromMessageCondensed = regExp.Replace(message.Model.Category.Replace("+", ""), " ");
		            
                    foreach (var protocol in protocols)
                    {
                        var protCondensed = regExp.Replace(protocol.Categories.Replace("+", ""), " ");
                        
                        if (protCondensed.Split('|').Contains(protFromMessageCondensed))
		                {
		                    itemsInCategory.Add(protocol);
		                }
		            }
                    break;
		    }
            return itemsInCategory;
        }

		private List<QuickReadSlot> GetPinnedSlotList(UserFull user)
		{
			var quickAll = ActorService.GetListOfItems(user, ItemType.Quick, false).ToList();
			var slotList = new List<QuickReadSlot>();
			for (var i = 0; i < NumberOfQrSlots; i++)
			{
				slotList.Add(QuickReadSlot.CreateEmptySlot());
				var protocolForSlot = quickAll.FirstOrDefault(q => q.PinSlot == i);
				if (protocolForSlot != null)
				{
					slotList[i].ProtocolId = protocolForSlot.Id.ToString();
					slotList[i].Name = protocolForSlot.PinName;
				}
			}

			return slotList;
		}

		private Document GetSingleDocument(Guid docuemntId, UserFull userFull)
		{
			if (mMeasurementContext.LiveDocuments.ContainsKey(docuemntId))
			{
				return mMeasurementContext.LiveDocuments[docuemntId].Clone();
			}

			var result = GetSpecificListItem(docuemntId, userFull, true);

		    if (userFull ?.Id != null && userFull.Id == GlobalConstants.ImportGuid)
		    {
		        result.UserNameGuid = GlobalConstants.ImportGuid.ToString();
		        var protocolName = result.ProtocolName.Split('\\');
		        if (protocolName.Length >= 5)
		        {
                    protocolName[4] = GlobalConstants.ImportGuid.ToString();
                    result.ProtocolName = string.Join("\\", protocolName);
                }
		    }

			var resultContent = mStore.GetItem(result);

			return resultContent;
		}

		private GimliResponse CreateErrorResponse(RequestMessage<ProtocolResultMessage> message)
		{
			return new GimliResponse(new ErrorInfo { Message = $"Not possible to load file {message.Model.Id}." }, ResultStatusEnum.Error, Log);
		}

		private ProtocolListItem GetSpecificListItem(Guid id, UserFull userFull, bool includePublic)
		{
			if (Document2ListItemMap.ContainsKey(id) && Document2ListItemMap[id] != null)
			{
				return Document2ListItemMap[id];
			}

			var item = ActorService.GetListOfItems(userFull, ItemType.Protocol | ItemType.Result | ItemType.Standard, includePublic).ToList().Find(p => p.Id == id);
			Document2ListItemMap[id] = item;

			return item;
		}
	}
}
