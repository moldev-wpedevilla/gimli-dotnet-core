﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.DataService.Actors
{
	class LiveDataQueueProcessorActor: GimliActorBase
	{
		private readonly string LiveDocumentRouter = nameof(LiveDocumentRoutingActor);

		public LiveDataQueueProcessorActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			var lastReceivedData = new List<RunningUpdateMessage>();

			Receive<Guid>(message =>
			{
				var finishedMessage = lastReceivedData.FirstOrDefault(m => m.Message.Model.Id == message);
				if (finishedMessage == null) throw new ActorException("No such update message is still running.");
				lastReceivedData.Remove(finishedMessage);

				ForwardAllMessages(lastReceivedData);
			});

			Receive<RequestMessage<UpdateDocumentWithPlateMessage>>(message =>
			{
				Log.Debug("Received live-data message in queue processor.");

				// update not running
				if (!lastReceivedData.Any(m => m.Message.Model.DocumentId == message.Model.DocumentId && m.Message.Model.Plate.Id == message.Model.Plate.Id && m.IsRunning))
				{
					lastReceivedData.Add(new RunningUpdateMessage {Message = message});
					ForwardAllMessages(lastReceivedData);
					return;
				}

				// find message that is not being processed and which can be updated
				var toUpdate = lastReceivedData.FirstOrDefault(m => m.Message.Model.DocumentId == message.Model.DocumentId && m.Message.Model.Plate.Id == message.Model.Plate.Id && !m.IsRunning);
				if (toUpdate == null)
				{
					toUpdate = new RunningUpdateMessage {Message = message};
					lastReceivedData.Add(toUpdate);
					return;
				}

				// update wells in plate
				toUpdate.Message.Model.Plate = message.Model.Plate;
				toUpdate.Message.Model.WellSeriailization = message.Model.WellSeriailization;
				toUpdate.Message.Model.WellSeriailizationSize = message.Model.WellSeriailizationSize;

				// update lately measured wellIds in frontend message
				var wells = toUpdate.Message.Model.FrontendMessage.Info.Wells.ToList();
				wells.AddRange(message.Model.FrontendMessage.Info.Wells.Where(point => !toUpdate.Message.Model.FrontendMessage.Info.Wells.Any(w => w.X == point.X && w.Y == point.Y)));
				toUpdate.Message.Model.FrontendMessage = message.Model.FrontendMessage;
				toUpdate.Message.Model.FrontendMessage.Info.Wells = wells.ToArray();
			});
		}

		private void ForwardAllMessages(IEnumerable<RunningUpdateMessage> lastReceivedData)
		{
			foreach (var runningUpdateMessage in lastReceivedData.Where(m => !m.IsRunning))
			{
				runningUpdateMessage.IsRunning = true;
				runningUpdateMessage.Message.Model.Id = Guid.NewGuid();
				GetChild<LiveDocumentRoutingActor>(LiveDocumentRouter).Forward(runningUpdateMessage.Message);
			}
		}

		protected override void PreStart()
		{
			GetChild<LiveDocumentRoutingActor>(LiveDocumentRouter);

			base.PreStart();
		}
	}

	class RunningUpdateMessage
	{
		internal RequestMessage<UpdateDocumentWithPlateMessage> Message { get; set; }
		internal bool IsRunning { get; set; }
	}
}
