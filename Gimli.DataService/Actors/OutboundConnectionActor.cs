﻿using System.Collections.Generic;
using Akka.Actor;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Outbound;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.Outbound;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.DataService.Actors
{
	public class OutboundConnectionActor : GimliActorBase
	{
		private readonly OutboundSession mSession = new OutboundSession();
		private string mGxPFileName = string.Empty;

		public OutboundConnectionActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			const string selfInitMessage = "DoSelfInit";

			Receive<string>(message =>
			{
				if (message == selfInitMessage)
				{
					mGxPFileName = GetAnthosDir() + "GxPModeIsOn.txt";
					InitSession();
				}
			});

			Receive<RequestMessage<GxpLoginMessage>>(message =>
			{
				mSession.User = message.Model.GxpUser;
				if (message.Model.GxpUser?.Connection != null)
				{
					mSession.IpAddressOfClient = message.Model.GxpUser.Connection.IP;
					mSession.SmpHostname = message.Model.GxpUser.Connection.HostName;
				}
				actorService.Get<WebServiceProxyActor>().Forward(message);
				SendNewOutboundControlledMessage(actorService);
			});
			Receive<RequestMessage<GxpLogoutMessage>>(message =>
			{
				mSession.User = null;
				actorService.Get<WebServiceProxyActor>().Forward(message);
				SendNewOutboundControlledMessage(actorService);
			});
			Receive<RequestMessage<GxpModeMessage>>(message =>
			{
				mSession.GxpMode = SaveGxPModeState(message.Model.Active);
				actorService.Get<WebServiceProxyActor>().Forward(message);

				actorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
				{
					Parameters =
						new MessageParameters
						{
							DisplayType = MessageDisplayType.Toast,
							Message = message.Model.Active ? Properties.Resources.Reader_Is_Locked : Properties.Resources.Reader_Is_Released,
							MessageCode = MessageStatusCodes.GxpLockChanged,
							MessageType = MessageType.Info
						}
				}));
				SendNewOutboundControlledMessage(actorService);
			});
			Receive<RequestMessage<OutboundDocumentInfoMessage>>(message =>
			{
				mSession.Document = message.Model.DocumentUpdateInfo;
				actorService.Get<WebServiceProxyActor>().Forward(message);
			});
			Receive<RequestMessage<OutboundDataMessage>>(message =>
			{
				var finishedStates = new List<MeasurementStatus> {MeasurementStatus.Finished, MeasurementStatus.ErrorOccurred };
				if (finishedStates.Contains(message.Model.Data.Status))
				{
					mSession.MeasurementProgress = null;
					actorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
					{
						Parameters =
							new MessageParameters
							{
								DisplayType = MessageDisplayType.Toast,
								Message = Properties.Resources.Read_Finished,
								MessageCode = MessageStatusCodes.ReadFinished,
								MessageType = MessageType.Info
							}
					}));

					message.Model.Data.User = null;
					message.Model.Data.DocumentName = null;
					message.Model.Data.ExperimentName = null;
					message.Model.Data.PlateName = null;
				}
				else
				{
					mSession.MeasurementProgress = message.Model.Data;
				}

				actorService.Get<WebServiceProxyActor>().Forward(message);
			});
			Receive<RequestMessage<OutboundControlledMessage>>(message =>
			{
				mSession.IsClientConnected = message.Model.IsOutboundControlled;
				if (message.Model.ClientHostname != null) mSession.SmpHostname = message.Model.ClientHostname;
				if (message.Model.IpAddressOfClient != null) mSession.IpAddressOfClient = message.Model.IpAddressOfClient;

				if (!message.Model.IsOutboundControlled)
				{

					actorService.Get<WebServiceProxyActor>().Tell(
						MessageExtensions.CreateRequestMessage(new OutboundDataMessage { Data = new MeasurementDataInfo
						{
							User = new GxpUser { User = null },
							Status = MeasurementStatus.Finished
						}}));
					InitSession();
				}

				//actorService.Get<WebServiceProxyActor>().Forward(message);
				SendNewOutboundControlledMessage(actorService);
			});
			Receive<RequestMessage<GetOutboundSessionMessage>>(message =>
			{
				Sender.Tell(new GetOutboundSessionResponseMessage {Session = mSession});
			});

			Self.Tell(selfInitMessage);
		}

	    protected virtual bool SaveGxPModeState(bool active)
	    {
			// CHBE: To avoid: The process cannot access the file because it is being used by another process.
			System.GC.Collect();
			System.GC.WaitForPendingFinalizers();

			if (active)
	        {
				if (!System.IO.File.Exists(mGxPFileName))
					System.IO.File.Create(mGxPFileName);
	            return active;
	        }
            if (System.IO.File.Exists(mGxPFileName))
                System.IO.File.Delete(mGxPFileName);
	        return false;
	    }

		protected virtual bool IsGxpModeOn()
		{
			return System.IO.File.Exists(mGxPFileName);
		}

		protected virtual string GetAnthosDir()
		{
			return Anthos.ResourceReader.DirectoryLookup.GetTemplateDir;
		}

		private void SendNewOutboundControlledMessage(IActorService actorService)
		{
			actorService.Get<WebServiceProxyActor>().Tell(
				MessageExtensions.CreateRequestMessage(new OutboundControlledAndMoreInfoMessage
				{
					ClientHostname = mSession.SmpHostname,
					GxpMode = mSession.GxpMode,
					Username = mSession.User?.User,
					IpAddressOfClient = mSession.IpAddressOfClient,
					IsOutboundControlled = mSession.IsClientConnected
				}));
		}

		private void InitSession()
		{
			mSession.GxpMode = IsGxpModeOn();
			mSession.IpAddressOfClient = null;
			mSession.SmpHostname = null;
			mSession.User = null;
			mSession.Document = null;
			mSession.DocumentReadInfo = null;
			mSession.MeasurementProgress = null;
		}
	}
}
