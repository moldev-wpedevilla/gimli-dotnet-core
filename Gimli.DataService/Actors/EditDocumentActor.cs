﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Akka.Actor;
using Castle.Core.Internal;
using Gimli.DataService.Properties;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Enums;
using Gimli.JsonObjects.Settings;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.DataService.Actors
{
	public class EditDocumentActor : GimliActorBase
	{
		private const string GimliDefaultGFactorKey = "GimliDefaultGFactor";
		private readonly IAnthosFunctions mAnthosFunctions;

		public EditDocumentActor(IActorService actorService, IActorsystemOverallState systemState, IAnthosFunctions anthosFunctions) : base(actorService, systemState)
		{
			mAnthosFunctions = anthosFunctions;

			Receive<RequestMessage<CopyPlateMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(CopyPlate, message));
			});

			Receive<RequestMessage<DeletePlateMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(DeletePlate, message));
			});

			Receive<RequestMessage<RenamePlateMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(RenamePlate, message));
			});

			Receive<RequestMessage<UpdateDocumentWithSettingsMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(UpdateSettings, message));
			});

			Receive<RequestMessage<UpdateDocumentWithReductionSettingsMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(UpdateReductionSettings, message));
			});

			Receive<RequestMessage<CreateProtocolFromDocumentMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(CreateProtocol, message));
			});

			Receive<RequestMessage<GetInputForReadMessage>>(message =>
			{
				var sender = Sender;
				sender.Tell(GetGuaranteedAnswer(GetStartReadInput, message));
			});

			Receive<RequestMessage<CreateNewProtocolMessage>>(message =>
			{
				Sender.Tell(GetGuaranteedAnswer(CreateNewProtocol, message));
			});

			Receive<RequestMessage<RenameDocumentMessage>>(message =>
			{
				Sender.Tell(GetGuaranteedAnswer(RenameDocument, message));
			});

			Receive<RequestMessage<SetQuickreadSlotMessage>>(message =>
			{
				Sender.Tell(GetGuaranteedAnswer(SetQuickreadSlot, message));
			});
		}

		private GimliResponse SetQuickreadSlot(RequestMessage<SetQuickreadSlotMessage> message)
		{
			// First unpin all documents for given slot
			var quickAll = ActorService.GetListOfItems(message.Model.User, ItemType.Quick, false).ToList();
			foreach (var protocol in quickAll.Where(q => q.PinSlot == message.Model.SlotId))
			{
				var pinned = ActorService.GetDocument(protocol.Id);
				var typedPinned = pinned as DocumentResponseMessage;
				if (typedPinned == null) return pinned;

				typedPinned.Document.IsPinned = false;
				typedPinned.Document.PinSlot = -1;
				typedPinned.Document.PinName = null;

				ActorService.SaveDocument(typedPinned.Document);
			}

			// unpin done: return
			if (message.Model.Slot == null) return new GimliResponse();

			// else: pin new item
			var response = ActorService.GetDocument(Guid.Parse(message.Model.Slot.ProtocolId));
			var typedResponse = response as DocumentResponseMessage;
			if (typedResponse == null) return response;

			typedResponse.Document.IsPinned = true;
			typedResponse.Document.PinSlot = message.Model.SlotId;
			typedResponse.Document.PinName = message.Model.Slot.Name;

			return ActorService.SaveDocument(typedResponse.Document);
		}

		private GimliResponse RenameDocument(RequestMessage<RenameDocumentMessage> message)
		{
			// get document
			var documentResponse = ActorService.GetDocument(message.Model.DocumentId);
			if (documentResponse.ResponseStatus != ResultStatusEnum.Success)
			{
				return documentResponse;
			}

			var documentToRename = ((DocumentResponseMessage)documentResponse).Document;
			documentToRename.DisplayName = message.Model.Name;
		    documentToRename.IsProtected = false;

			return ActorService.SaveDocument(documentToRename);
		}

		private GimliResponse CreateNewProtocol(RequestMessage<CreateNewProtocolMessage> message)
		{
			var name = message.Model.Name ?? string.Format(CultureInfo.InvariantCulture, Resources.New_Mode_Protocol, JsonObjects.Resources.GetResource.GetString($"ReadMode_{message.Model.ReadMode}"));
			var listItem = new ProtocolListItem(Guid.NewGuid(), GlobalConstants.PublicGuidString, name, name, DateTime.Now, false, false) { Category = ItemType.Protocol.ToString() };
			var document = JsonObjects.DummyData.DummyDataGenerator.CreateProtocol(listItem, 8, 12, 1, 1, 4 , true, Mode.Abs, MeasurementType.Endpoint);
			document.Experiments[0].Plates[0].Settings = PlateSettings.GenerateDefaultSettings();

			var measurementSettingsTask = ActorService.Get<InstrumentServiceProxyActor>().Ask<GimliResponse>(
				MessageExtensions.CreateRequestMessage(new UpdateSettingsWithReadModeTypeMessage
				{
					SnapshotSettings = MeasurementSettings.Generate(message.Model.ReadMode, MeasurementType.Endpoint),
					DesiredReadMode = message.Model.ReadMode,
					DesiredReadType = MeasurementType.Endpoint
				}));
			measurementSettingsTask.Wait(5000);
			var settings = ((UpdateSettingsWithReadModeTypeResponseMessage)measurementSettingsTask.Result).SnapshotSettings;
			document.Experiments[0].Plates[0].SnapShot[0].Settings = settings;

			var task = ActorService.Get<InstrumentServiceProxyActor>().Ask<GimliResponse>(MessageExtensions.CreateRequestMessage(new GetReductionParametersMessage()));
			task.Wait(5000);
			var reductionSettings = ((GetReductionParametersResponseMessage)task.Result).Settings;

			reductionSettings.ParametersDouble.First(
				p => p.Name == JsonObjects.Resources.GetResource.GetString(DataReductionSettings.ParameterGfactor)).Value =
					double.Parse(mAnthosFunctions.GetSystemValue(GimliDefaultGFactorKey, 0.5).ToString());

			document.Experiments[0].Plates[0].SnapShot[0].ReductionSettings = reductionSettings;

			var saveResponse = ActorService.SaveDocument(document);
			return saveResponse.ResponseStatus != ResultStatusEnum.Success
				? saveResponse
				: new CreateProtocolResponseMessage { DocumentId = document.Id };
		}
        
		private GimliResponse GetStartReadInput(RequestMessage<GetInputForReadMessage> message)
		{
			// get document
			var documentResponse = ActorService.GetDocument(message.Model.DocumentId);
			if (documentResponse.ResponseStatus != ResultStatusEnum.Success)
			{
				return documentResponse;
			}

			var documentToSave = ((DocumentResponseMessage)documentResponse).Document;
			documentToSave.Timestamp = DateTime.Now;
			Dictionary<Guid, Guid> plateIdMap = null;

			// if Protocol
			if (!documentToSave.HasData)
			{
				plateIdMap = new Dictionary<Guid, Guid>();

				// create new result file
				documentToSave.Id = Guid.NewGuid();
				documentToSave.HasData = true;
				documentToSave.IsProtected = false;
			    documentToSave.IsPinned = false;
			    documentToSave.Category = ItemType.Result.ToString();

                if (message.Model.Name != null)
				{
					documentToSave.DisplayName = message.Model.Name;
				}
				foreach (var experiment in documentToSave.Experiments)
				{
					experiment.Id = Guid.NewGuid();
					foreach (var plate in experiment.Plates)
					{
						var newId = Guid.NewGuid();
						plateIdMap[plate.Id] = newId;
						plate.Id = newId;
						foreach (var snapshot in plate.SnapShot)
						{
							snapshot.Id = Guid.NewGuid();
                            //snapshot.CreateDefaultDimensionInfo();
							foreach (var well in snapshot.Wells)
							{
								well.DeleteAllWellData();
							}
                        }
					}
				}
			}
			else
			{
				var plates = documentToSave.Experiments.SelectMany(e => e.Plates.Where(p => message.Model.Plates.Contains(p.Id))).ToList();
				if (!message.Model.Overwrite && plates.Any(p => p.HasData))
				{
                    var ret = new GimliResponse(new ErrorInfo { Message = Resources.Data_found_use_overwrite }, ResultStatusEnum.Error, Log);

				    return ret;
				}


				// delete data
				foreach (var well in plates.SelectMany(plate => plate.SnapShot.SelectMany(snapshot => snapshot.Wells)))
				{
					well.DeleteAllWellData();
				}
				documentToSave.Category = ItemType.Protocol.ToString();
			}

			var docWithoutData = documentToSave.CloneToEmpty();
			var validationMessage = MessageExtensions.CreateRequestMessage(new ValidateDocumentMessage { Document = docWithoutData});
			var task = ActorService.Get<InstrumentServiceProxyActor>().Ask<GimliResponse>(validationMessage);
			task.Wait();
			if (task.Result.ResponseStatus != ResultStatusEnum.Success)
			{
				return task.Result;
			}

			var typedResponse = task.Result as ValidateDocumentResponseMessage;
			if (typedResponse != null)
			{
				foreach (var snapshot in from experiment in documentToSave.Experiments
					from plate in experiment.Plates
					from snapshot in plate.SnapShot
					select snapshot)
				{
					var settings = snapshot.Settings;
					// != spectrumscan because spectrumscan can not be 
					settings.IsModuleUsed = typedResponse.ModuleInfo != null &&
											settings.MeasurementMode == Mode.Trf &&
					                        settings.MeasurementType != MeasurementType.SpectrumScan &&
					                        settings.Wavelengths.Excitation.All(e => !e.UseMonocromator) &&
					                        settings.Wavelengths.Emission.All(e => !e.UseMonocromator);
					settings.ModuleInformation = settings.IsModuleUsed ? typedResponse.ModuleInfo : null;
				}
			}

			var saveResponse = ActorService.SaveDocument(documentToSave);
			if (saveResponse.ResponseStatus != ResultStatusEnum.Success)
			{
				return saveResponse;
			}

			// create one document for each plate
			var emptyDocument = documentToSave.Clone();
			var platesToBeMeasured = plateIdMap != null
				? message.Model.Plates.Select(plate => plateIdMap[plate]).ToList()
				: message.Model.Plates;
			emptyDocument.RestrictDocumentToPlates(platesToBeMeasured);

			var documentsToStart = new List<Document>();
			foreach (var experiment in emptyDocument.Experiments)
			{
				foreach (var plate in experiment.Plates)
				{
					var newDocument = emptyDocument.Clone();
					newDocument.RestrictDocumentToPlates(new List<Guid> { plate.Id });
					documentsToStart.Add(newDocument);
				}
			}

			return new GetInputForReadResponseMessage
			{
				DocumentsToStart = documentsToStart
			};
		}

        //private void AddDimensionInfo(Snapshot snapshot)
        //{
        //    var nrOfFirstItems = 1;
        //    var nrOfSecondItems = snapshot.MaxYPos == 0 ? 1 : snapshot.MaxYPos;
        //    var nrOfThirdItems = snapshot.MaxXPos == 0 ? 1 : snapshot.MaxXPos;

        //    snapshot.DataDimensionInfo[0].DataPointLabel = new List<double>();
        //    snapshot.DataDimensionInfo[1].DataPointLabel = new List<double>();
        //    snapshot.DataDimensionInfo[2].DataPointLabel = new List<double>();

        //    for (var i = 0; i < nrOfFirstItems; i++)
        //        snapshot.DataDimensionInfo[0].DataPointLabel.Add(i+1);
        //    for (var i = 0; i < nrOfSecondItems; i++)
        //        snapshot.DataDimensionInfo[1].DataPointLabel.Add(i+1);
        //    for (var i = 0; i < nrOfThirdItems; i++)
        //        snapshot.DataDimensionInfo[2].DataPointLabel.Add(i+1);
        //}
        //private void SetDataDimensionInfo(Snapshot snapshot)
        //{
        //    try
        //    {
        //        var isMultiWavelength = snapshot.Settings.Wavelengths.NrOfWavelengths.ToInt() > 1;
        //        var thirdDim = snapshot.Settings.MeasurementMode == Mode.Abs ? "OD" : snapshot.Settings.MeasurementMode == Mode.Lum ? "RLU" : "RFU";
        //        snapshot.DataDimensionInfo = new DataDimension[3];
        //        snapshot.DataDimensionInfo[0] = new DataDimension("Data", 1);
        //        snapshot.DataDimensionInfo[1] = new DataDimension(isMultiWavelength ? "Wavelength" : "Data", 2);
        //        snapshot.DataDimensionInfo[2] = new DataDimension(thirdDim, 3);

        //        AddDimensionInfo(snapshot);
        //    }
        //    catch (Exception e)
        //    {
        //        Trace.WriteLine("Error DataDimensionInfo: " + e.Message);
        //    }
        //}
        private GimliResponse CreateProtocol(RequestMessage<CreateProtocolFromDocumentMessage> message)
		{
			// get document
			var createDocumentResponse = ActorService.GetDocument(message.Model.DocumentId);
			if (createDocumentResponse.ResponseStatus != ResultStatusEnum.Success)
			{
				return createDocumentResponse;
			}

			var createDocument = ((DocumentResponseMessage) createDocumentResponse).Document;
			createDocument.Id = Guid.NewGuid();
			createDocument.IsSynchronized = false;
			createDocument.HasData = false;
			createDocument.Timestamp = DateTime.Now;
			createDocument.DisplayName = message.Model.Name;
	        createDocument.IsProtected = false;
	        createDocument.IsPinned = false;
			createDocument.Category = ItemType.Protocol.ToString();
			foreach (var experiment in createDocument.Experiments)
			{
				experiment.Id = Guid.NewGuid();
				foreach (var plate in experiment.Plates)
				{
					plate.Id = Guid.NewGuid();
					foreach (var snapshot in plate.SnapShot)
					{
						snapshot.Id = Guid.NewGuid();
						foreach (var well in snapshot.Wells)
						{
							well.DeleteAllWellData();
						}
					}
				}
			}

			var saveResponse = ActorService.SaveDocument(createDocument);
			return saveResponse.ResponseStatus != ResultStatusEnum.Success
				? saveResponse
				: new CreateProtocolResponseMessage {DocumentId = createDocument.Id};
		}

		private GimliResponse UpdateReductionSettings(RequestMessage<UpdateDocumentWithReductionSettingsMessage> message)
		{
			// get document
			var reductionDocumentResponse = ActorService.GetDocument(message.Model.Settings.DocumentId);
			if (reductionDocumentResponse.ResponseStatus != ResultStatusEnum.Success)
			{
				return reductionDocumentResponse;
			}

			// update settings
			var reductionSettingsDocument = ((DocumentResponseMessage)reductionDocumentResponse).Document;
			foreach (var snapshotId in message.Model.Settings.ReductionSettings.Keys)
			{
				var snapshot = reductionSettingsDocument.Experiments[0].Plates.First(p => p.Id == message.Model.Settings.PlateId)
					.SnapShot.First(s => s.Id == snapshotId);
				snapshot.ReductionSettings = message.Model.Settings.ReductionSettings[snapshotId];

				mAnthosFunctions.SetSystemValue(GimliDefaultGFactorKey, snapshot.ReductionSettings.ParametersDouble.First(
					p => p.Name == JsonObjects.Resources.GetResource.GetString(DataReductionSettings.ParameterGfactor)).ToDouble());

				// do primary data reduction
				foreach (var well in snapshot.Wells)
				{
					well.ReduceWellData(snapshot);
				}
			}

			return ActorService.SaveDocument(reductionSettingsDocument);
		}

		private GimliResponse UpdateSettings(RequestMessage<UpdateDocumentWithSettingsMessage> message)
		{
			// get document
			var settingsDocumentResponse = ActorService.GetDocument(message.Model.Settings.DocumentId);
			if (settingsDocumentResponse.ResponseStatus != ResultStatusEnum.Success)
			{
				return settingsDocumentResponse;
			}

			var settingsDocument = ((DocumentResponseMessage) settingsDocumentResponse).Document;

			var requestedPlates =
				settingsDocument.Experiments.SelectMany(experiment => experiment.Plates.Where(p => p.Id == message.Model.Settings.PlateId)).ToList();
			if (requestedPlates.Count != 1)
			{
				throw new ActorException(
					$"Number of plates with the given id {message.Model.Settings.PlateId} in the document with id {message.Model.Settings.DocumentId} is {requestedPlates.Count}.");
			}

			var plate = requestedPlates[0];
			plate.Settings = message.Model.Settings.PlateSettings;
			var snapshots = plate.SnapShot.Where(s => message.Model.Settings.SnapshotSettings.ContainsKey(s.Id)).ToList();
			if (snapshots.Count != message.Model.Settings.SnapshotSettings.Count)
			{
				throw new ActorException(
					$"Number of snapshots with the given plate id {message.Model.Settings.PlateId} in the document with id {message.Model.Settings.DocumentId} is {snapshots.Count} but should be {message.Model.Settings.SnapshotSettings.Count}.");
			}

			foreach (var snapshot in snapshots)
			{
				snapshot.Settings = message.Model.Settings.SnapshotSettings[snapshot.Id];
				if (snapshot.ReductionSettings == null) snapshot.ReductionSettings = DataReductionSettings.Generate(DataReductionSettings.MehtodPolarization);
				snapshot.Wells = null;
				snapshot.InitializeWells(1, plate.Rows, plate.Columns, false);
			}

            //document comes from _Standard_ folder, should be saved in MyProtocols or Public folder
		    if (settingsDocument.IsProtected)
		    {
		        settingsDocument.IsProtected = false;
		        settingsDocument.Category = ItemType.Protocol.ToString();
                settingsDocument.UserNameGuid = GlobalConstants.PublicGuidString;
		    }
			return ActorService.SaveDocument(settingsDocument);
		}

		private GimliResponse RenamePlate(RequestMessage<RenamePlateMessage> message)
		{
			if (message.Model.Name.Trim().IsNullOrEmpty())
			{
				return new GimliResponse(new ErrorInfo {Message = Resources.String_Empty_not_allowed_as_plate_name }, ResultStatusEnum.Error, Log);
			}

			// get document
			var renameDocumentResponse = ActorService.GetDocument(message.Model.DocumentId);
			if (renameDocumentResponse.ResponseStatus != ResultStatusEnum.Success)
			{
				return renameDocumentResponse;
			}

			var renameDocument = ((DocumentResponseMessage)renameDocumentResponse).Document;

			var existingNames = (
				from exp in renameDocument.Experiments
				from p in exp.Plates
				select p.PlateName).ToList();

			if (existingNames.Contains(message.Model.Name))
			{
				return new GimliResponse(new ErrorInfo { Message = Resources.Name_already_exists_in_this_document}, ResultStatusEnum.Error, Log);
			}

			// rename
			Experiment experiment;
			var plate = GetPlate(renameDocument, message.Model.PlateId, out experiment);
			plate.PlateName = message.Model.Name;

			// save
			return ActorService.SaveDocument(renameDocument);
		}

		private GimliResponse DeletePlate(RequestMessage<DeletePlateMessage> message)
		{
			// get document
			var deleteDocumentResponse = ActorService.GetDocument(message.Model.DocumentId);
			if (deleteDocumentResponse.ResponseStatus != ResultStatusEnum.Success)
			{
				return deleteDocumentResponse;
			}

			var deleteDocument = ((DocumentResponseMessage)deleteDocumentResponse).Document;

			// delete plate
			Experiment experiment;
			var plate = GetPlate(deleteDocument, message.Model.PlateId, out experiment);
			if (experiment.Plates.Count < 2)
			{
				return new GimliResponse(new ErrorInfo {Message = Resources.The_last_plate_in_the_list_can_not_be_deleted}, ResultStatusEnum.Error, Log);
			}
			experiment.Plates.Remove(plate);

			return ActorService.SaveDocument(deleteDocument);
		}

		protected GimliResponse CopyPlate(RequestMessage<CopyPlateMessage> message)
		{
			// get document
			var copyDocumentResponse = ActorService.GetDocument(message.Model.DocumentId);
			if (copyDocumentResponse.ResponseStatus != ResultStatusEnum.Success)
			{
				return copyDocumentResponse;
			}

			var copyDocument = ((DocumentResponseMessage)copyDocumentResponse).Document;

			// copy plate
			Experiment experiment;
			var plate = GetPlate(copyDocument, message.Model.PlateId, out experiment);
			var newPlate = plate.Clone();
			newPlate.Id = Guid.NewGuid();
			newPlate.PlateName = message.Model.Name ?? "Copy of " + plate.PlateName;
			foreach (var snapshot in newPlate.SnapShot)
			{
				snapshot.Id = Guid.NewGuid();
				foreach (var well in snapshot.Wells)
				{
					well.DeleteAllWellData();
				}
			}

			var originalPlateIndex = experiment.Plates.IndexOf(plate);
			experiment.Plates.Insert(originalPlateIndex +1, newPlate);

			// save
			var saveResponse = ActorService.SaveDocument(copyDocument);
			return saveResponse.ResponseStatus != ResultStatusEnum.Success 
				? saveResponse 
				: new CopyPlateResponseMessage {DocumentId = message.Model.DocumentId, PlateId = newPlate.Id, Name = newPlate.PlateName};
		}

		private Plate GetPlate(Document document, Guid plateId, out Experiment experiment)
		{
			experiment = null;
			Plate plate = null;
			foreach (var exp in document.Experiments)
			{
				foreach (var plt in exp.Plates)
				{
					if (plt.Id == plateId)
					{
						experiment = exp;
						plate = plt;
						break;
					}
				}

				if (plate != null) break;
			}

			if (plate == null)
			{
				throw new ActorException("No such plate id in the given document.");
			}
			return plate;
		}
	}
}
