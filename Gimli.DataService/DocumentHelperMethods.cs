﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Gimli.DataService.Actors;
using Gimli.DataService.Messages;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Enums;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.ExportImport;
using MolDev.Common.Akka.Interfaces;

namespace Gimli.DataService
{
	internal static class DocumentHelperMethods
	{
		internal static GimliResponse SaveDocument(this IActorService actorService, Document document, StorageStrategy strategy = StorageStrategy.Local)
		{
            if (!document.IsPinned && !document.IsProtected)
		        document.UserNameGuid = GetCurrentUserId(actorService);
            var task =
				actorService.Get<DataServiceRoutingActor>()
					.Ask<GimliResponse>(
						MessageExtensions.CreateRequestMessage(new StoreDocumentMessage
						{
							Document = document,
							RemoteHostname = HelperMethods.GetLocalHostName(),
							Strategy = strategy
						}), 15000);
			task.Wait();
			return task.Result;
		}

		internal static GimliResponse GetDocument(this IActorService actorService, Guid documentId, UserFull user = null)
		{
			var task = actorService.Get<DataServiceRoutingActor>()
				.Ask<GimliResponse>(MessageExtensions.CreateRequestMessage(new ProtocolResultMessage
				{
					Id = documentId,
					Type = ProtocolResultMessageType.GetDocument,
					UserFull = user
				}), 25000);
			task.Wait();
			return task.Result;
		}

		internal static ICollection<ProtocolListItem> GetListOfItems(this IActorService actorService, UserFull user, ItemType protocolType, bool includePublic)
		{
			var task = actorService.Get<DataServiceRoutingActor>()
				.Ask<ThreadsafeGetListOfItemsResponseMessage>(new ThreadsafeGetListOfItemsMessage
				{
					DocumentType = protocolType,
					User = user,
					IncludePublic = includePublic
				});
			task.Wait();
			return task.Result.ProtocolListItems;
		}
        
        private static GimliResponse GetCurrentUser(IActorService actorService)
        {
            var result =
                    actorService.Get<DataServiceRoutingActor>()
                        .Ask<GetCurrentUserResponseMessage>(MessageExtensions.CreateRequestMessage(new GetCurrentUserMessage()), 10000);
            result.Wait();

            return result.Result;
        }

        private static string GetCurrentUserId(IActorService actorService)
        {
            var userFullResponse = GetCurrentUser(actorService);
            var userId = GlobalConstants.PublicGuid;
            if(userFullResponse != null)
                userId = ((GetCurrentUserResponseMessage)userFullResponse).UserFull.Id;

            return userId.ToString();
        }

		internal static Task<GimliResponse> CreateImportUserFolder(IActorService actorService)
		{
			return actorService.Get<DataServiceRoutingActor>().Ask<GimliResponse>(
				MessageExtensions.CreateRequestMessage(new ProtocolResultMessage
				{
					UserFull = new UserFull { Id = GlobalConstants.ImportGuid },
					Type = ProtocolResultMessageType.GetProtocolList
				}));
		}

		internal static void NotifyUserAboutImportableData(IActorService actorService)
		{
			var task = actorService.Get<DataServiceRoutingActor>()
				.Ask<DocumentsForImportMessage>(
					MessageExtensions.CreateRequestMessage(new ProtocolResultMessage
					{
						Type = ProtocolResultMessageType.GetCountsForImportDocuments
					}));
			task.Wait();

			actorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(task.Result));
		}
	}
}
