﻿using Gimli.JsonObjects;
using Gimli.JsonObjects.Enums;

namespace Gimli.DataService.Messages
{
	class ThreadsafeGetListOfItemsMessage
	{
		public UserFull User { get; set; }
		public ItemType DocumentType { get; set; }
		public bool IncludePublic { get; set; }

	}
}
