﻿using System;
using System.Collections.Generic;
using System.Threading;
using Gimli.JsonObjects.Parameter;

namespace Gimli.DataService.Messages
{
    public class SaveInjectorSettingsMessage
    {
        public static SaveInjectorSettingsMessage Generate(List<int> vol)
        {
            return new SaveInjectorSettingsMessage(vol);
        }

        public static SaveInjectorSettingsMessage Generate(List<GimliParameterInt> vol)
        {
            return new SaveInjectorSettingsMessage(vol);
        }

        private SaveInjectorSettingsMessage(List<int> volume)
        {
            mVolume = new List<int>();
            foreach (var vol in volume)
            {
                mVolume.Add(vol);
            }
        }

        private SaveInjectorSettingsMessage(List<GimliParameterInt> volume)
        {
            mVolume = new List<int>();
            foreach (var item in volume)
            {
                mVolume.Add(Convert.ToInt32(item.Value));
            }
        }

        public List<int> mVolume { get; set; }
    }
}
