﻿using System.Collections.Generic;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;

namespace Gimli.DataService.Messages
{
	class ThreadsafeGetListOfItemsResponseMessage : GimliResponse
	{
		public ICollection<ProtocolListItem> ProtocolListItems { get; set; }
	}
}
