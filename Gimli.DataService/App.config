﻿<configuration>
  <configSections>
    <section name="castle" type="Castle.Windsor.Configuration.AppDomain.CastleSectionHandler, Castle.Windsor" />
    <section name="akka" type="Akka.Configuration.Hocon.AkkaConfigurationSection, Akka" />
    <section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog" />
    <section name="ShutdownSettings" type="System.Configuration.NameValueSectionHandler" />
  </configSections>
  <startup>
    <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.6.1" />
  </startup>
  <appSettings>
    <!--remember to update RemoteActorConfiguration as well-->
    <add key="moldevhost:ActorSystemName" value="data" />
    <add key="moldevhost:ActorAskTimeOutMilliseconds" value="5000" />
    <add key="LoadTimerIntervalMs" value="3000" />
    <add key="NumberOfParallelDocumentRequests" value="4" />
    <add key="RewriteDocuments" value="false" />
    <add key="ClientSettingsProvider.ServiceUri" value="" />
    <add key="UseSqlDataStore" value="false" />
    <add key="ExportExcel" value="true" />
    <add key="ExportExcelTempDirectory" value="C:\ProgramData\Molecular Devices\working\export_excel\" />
    <add key="GenerateForWeb" value="true" />
    <add key="ExportWebDirectory" value="C:\ProgramData\Molecular Devices\GimliData\Excel\" />
    <add key="SelfInitL2Delay" value="2" />
    <add key="IsInGxPMode" value="true" />
  </appSettings>
  <akka>
    <hocon>
        akka {
          # Options: OFF, ERROR, WARNING, INFO, DEBUG
          stdout-loglevel = DEBUG
          loglevel = DEBUG
          log-config-on-start = off
          loggers = ["MolDev.ActorHost.InternalActors.NLogLogger, MolDev.ActorHost"]

          actor {
            provider = "Akka.Remote.RemoteActorRefProvider, Akka.Remote"
            serializers {
              json = "Akka.Serialization.NewtonSoftJsonSerializer, Akka"
              #binary = "MolDev.ActorHost.MessagesSerializer, MolDev.ActorHost"
            }
            serialization-bindings {
              "Akka.Actor.Identify" = json
              "Akka.Actor.ActorIdentity" = json
              "Akka.Remote.RemoteWatcher+Heartbeat, Akka.Remote" = json
              "Akka.Remote.RemoteWatcher+HeartbeatRsp, Akka.Remote" = json
              "Akka.Dispatch.SysMsg.DeathWatchNotification" = json
              "Akka.Dispatch.SysMsg.Unwatch" = json
              "Akka.Dispatch.SysMsg.Terminate" = json
              "Akka.Dispatch.SysMsg.Watch" = json
              "MolDev.ActorHost.InternalMessages.WatchForMessage, MolDev.ActorHost" = json
              "System.Object" = json
            }
            deployment {
              /DataServiceRoutingActor {
                create-local = "Gimli.DataService.Actors.DataServiceRoutingActor, Gimli.DataService"
              }
              /WebServiceProxyActor {
                create-local = "Gimli.Shared.Actors.Actors.WebServiceProxyActor, Gimli.Shared.Actors"
              }
              /InstrumentServiceProxyActor {
                create-local = "Gimli.Shared.Actors.Actors.InstrumentServiceProxyActor, Gimli.Shared.Actors"
              }
              /CoreServiceProxyActor {
                create-local = "Gimli.Shared.Actors.Actors.CoreServiceProxyActor, Gimli.Shared.Actors"
              }
              /NetworkCommunicationServiceProxyActor {
                create-local = "Gimli.Shared.Actors.Actors.NetworkCommunicationServiceProxyActor, Gimli.Shared.Actors"
              }
              /AppManagerProxyActor {
                create-local = "Gimli.Shared.Actors.Actors.AppManagerProxyActor, Gimli.Shared.Actors"
              }              
            }
            debug {
              receive = on
              autoreceive = off
              lifecycle = on
              event-stream = on
              unhandled = on
            }
          }
          remote {
            enabled-transports = ["akka.remote.dot-netty.tcp"]
            dot-netty.tcp {
#              transport-class = "akka.remote.transport.dotnetty.dotNettyTransport"
#              applied-adapters = []
#              transport-protocol = tcp
#
              port = ==replaceMeWithPort==
              hostname = "0.0.0.0"
              public-hostname = "==replaceMeWithHostname=="
              maximum-frame-size = 256000000b
              connection-timeout = 1s
              send-buffer-size = 4096000b
              receive-buffer-size = 4096000b
            }
          log-remote-lifecycle-events = on
        }
    </hocon>
  </akka>
  <castle>
    <components>
      <!--<component id="IUserRepository" service="MolDev.Shared.Interfaces.User.IUserRepository, MolDev.Shared" type="MolDev.Repository.User.UserRepositoryMock, MolDev.Repository" lifestyle="singleton" />-->
    </components>
  </castle>
  <nlog autoReload="true" throwExceptions="false">
    <targets async="true">
      <!--     Writing events to the a file with the date in the filename.     -->
      <target name="file" type="File" fileName="${specialfolder:folder=CommonApplicationData}/Molecular Devices/GimliData/logs/data.log" layout="${longdate}|${level}|${message}|${exception:format=tostring}" maxArchiveFiles="10" archiveNumbering="Sequence" archiveAboveSize="5242880" archiveFileName="${specialfolder:folder=CommonApplicationData}/Molecular Devices/GimliData/logs/data{#######}.a" />
      <target name="console" type="ColoredConsole" layout="${date:format=HH\:mm\:ss}|${level}|${message}|${exception:format=tostring}" />
      <!--<target name="TcpOutlet" type="NLogViewer" address="tcp4://localhost:8204"/>-->
    </targets>
    <rules>
       <logger name="*" minlevel="Debug" writeTo="file,console" /> 
      <!--<logger name="MolDev.Web.Logger" minlevel="Trace" writeTo="file,console,TcpOutlet" />
      <logger name="MolDev.Akka.Logger" minlevel="Trace" writeTo="file,console,TcpOutlet" />
      <logger name="MolDev.Test.Logger" minlevel="Trace" writeTo="file,console,TcpOutlet" />-->
    </rules>
  </nlog>
  <ShutdownSettings>
    <add key="doShutdown" value="false" />
  </ShutdownSettings>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="Google.ProtocolBuffers" publicKeyToken="55f7125234beb589" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-2.4.1.555" newVersion="2.4.1.555" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="MD.LoggingInterface" publicKeyToken="213c5af171ea924e" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-1.0.0.0" newVersion="1.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-9.0.0.0" newVersion="9.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Topshelf" publicKeyToken="b800c4cfcdeea87b" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.3.154.0" newVersion="3.3.154.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Anthos" publicKeyToken="cc7704c1cb081f26" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-7.0.3.0" newVersion="7.0.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="MD.PluginsFrameworkManager" publicKeyToken="213c5af171ea924e" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-2.0.7.40" newVersion="2.0.7.40" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Windows.Interactivity" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.5.0.0" newVersion="4.5.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.0.1.0" newVersion="3.0.1.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.web>
    <membership defaultProvider="ClientAuthenticationMembershipProvider">
      <providers>
        <add name="ClientAuthenticationMembershipProvider" type="System.Web.ClientServices.Providers.ClientFormsAuthenticationMembershipProvider, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" serviceUri="" />
      </providers>
    </membership>
    <roleManager defaultProvider="ClientRoleProvider" enabled="true">
      <providers>
        <add name="ClientRoleProvider" type="System.Web.ClientServices.Providers.ClientRoleProvider, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" serviceUri="" cacheTimeout="86400" />
      </providers>
    </roleManager>
  </system.web>
</configuration>