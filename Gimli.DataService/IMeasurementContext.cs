﻿using System;
using System.Collections.Generic;
using Gimli.JsonObjects;

namespace Gimli.DataService
{
	public interface IMeasurementContext
	{
		Dictionary<Guid,Document> LiveDocuments { get; }
		List<Guid> AreQueuedForSaving { get; }
	}
}