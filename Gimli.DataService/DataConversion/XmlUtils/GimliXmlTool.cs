﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using Akka.Event;
using Gimli.DataService.Properties;
using Gimli.JsonObjects;
using Gimli.JsonObjects.DummyData;
using Gimli.JsonObjects.Enumarations.Status;
using Gimli.JsonObjects.Parameter;

namespace Gimli.DataService.DataConversion.XmlUtils
{
    public class GimliXmlTool
    {
        private readonly ILoggingAdapter mLoggingAdapter;
        private readonly Document mDocument;
        private readonly UserFull mUserFull;
        private readonly string mSystemName;
        private XmlTextWriter mXmlTextWriter;

        private string mXmlFullPath;

        private readonly CultureInfo mCultureGlobal = CultureInfo.InvariantCulture;

        private int mOffsetRow = 1;
        private int mOffsetCol = 1;

        public GimliXmlTool(Document document, UserFull userFull, ILoggingAdapter loggingAdapter)
        {
            mDocument = document;
            mUserFull = userFull;
            mLoggingAdapter = loggingAdapter;
            mSystemName = HelperMethods.GetLocalHostName();
        }
        public GimliXmlTool(Document document, string systemName)
        {
            mDocument = document;
            mUserFull = new UserFull {UserName = "DemoUser"};
            mSystemName = systemName;
            
        }

        public string Export(string targetDirectory)
        {
            GimliTrace.Start("ExportComplete");

            string filename = GenerateFilename();

            mXmlFullPath = Path.Combine(targetDirectory, filename);

            CreateXmlDocument(mXmlFullPath);
            
            WriteToSheet();

            SaveXmlDocument();
            
            mLoggingAdapter?.Info($"Export Complete => {GimliTrace.Stop("ExportComplete")} ms");

            return filename;
        }

        public FileInfo GetXmlFileInfo()
        {
            return new FileInfo(mXmlFullPath);
        }
        
        private void WriteToSheet()
        {
            GimliTrace.Start("WriteGeneralData");
            
            var serial = "";

            if (mSystemName.Contains("-"))
            {
                serial = mSystemName.Split('-')[1];
            }
            else if (mSystemName.Contains("_"))
            {
                serial = mSystemName.Split('_')[1];
            }

            var openElements = WriteWorkSheet("General");
            ResizeColumns();

            WriteGeneralRow("String", Resources.Excel_Xml_UserName, "String", mUserFull.UserName);
            WriteGeneralRow("String", Resources.Excel_Xml_DocumentName, "String", mDocument.DocumentName);
            WriteGeneralRow("String", Resources.Excel_Xml_DateOfExport, "String", DateTime.Now.ToString(mCultureGlobal));
            WriteGeneralRow("String", Resources.Excel_Xml_DateOfLastChange, "String", mDocument.TimeStamp.ToString(mCultureGlobal));
            WriteGeneralRow("String", Resources.Excel_Xml_DateOfLastUse, "String", mDocument.LastUsedDate.ToString(mCultureGlobal));
            WriteGeneralRow("String", Resources.Excel_Xml_Version_Software, "String", Assembly.GetExecutingAssembly().GetName().Version.ToString());
            WriteGeneralRow("String", Resources.Excel_Xml_InstrumentName, "String", mSystemName);
            WriteGeneralRow("String", Resources.Excel_Xml_SerialNumber, "String", serial);
            WriteGeneralRow("String", Resources.Excel_Xml_Notes, "String", mDocument.Notes);

            mOffsetRow++;
            WriteExperimentDataToSheet();

            WriteEndElement(openElements);

            mLoggingAdapter?.Info($"Write General Data => {GimliTrace.Stop("WriteGeneralData")} ms");
        }
        private void WriteExperimentDataToSheet()
        {
            var experimentIndex = 1;
            foreach (var experiment in mDocument.Experiments)
            {
                GimliTrace.Start("WriteExperimentData");

                WriteGeneralRow("String", string.Format(Resources.Excel_Xml_Experiment_Header, experimentIndex++, mDocument.Experiments.Count), null, null, 1);
                WriteGeneralRow("String", Resources.Excel_Xml_Experiment_Name, "String", experiment.ExpName);

                mLoggingAdapter?.Info("Write Experiment Data => " + GimliTrace.Stop("WriteExperimentData") + " ms");

                mOffsetRow++;
                WritePlateDataToSheet(experiment);
            }
        }
        private void WritePlateDataToSheet(Experiment experiment)
        {
            var plateIndex = 1;
            foreach (var plate in experiment.Plates)
            {
                GimliTrace.Start("WritePlateData");
                
                WriteGeneralRow("String", string.Format(Resources.Excel_Xml_Plate_Header, plateIndex++, experiment.Plates.Count), null, null, 1);
                WriteGeneralRow("String", Resources.Excel_Xml_Plate_Name, "String", plate.PlateName);
                WriteGeneralRow("String", Resources.Excel_Xml_Plate_Barcode, "String", plate.Settings.Barcode);
                WriteGeneralRow("String", Resources.Excel_Xml_Plate_Labware, "String", plate.Settings.Plate.MicroplateName);
                WriteGeneralRow("String", Resources.Excel_Xml_Plate_Rows, "String", plate.Settings.Plate.PlateSpecification.NumberOfRows.ToString());
                WriteGeneralRow("String", Resources.Excel_Xml_Plate_Columns, "String", plate.Settings.Plate.PlateSpecification.NumberOfColumns.ToString());
                WriteGeneralRow("String", Resources.Excel_Xml_Plate_WellCount, "String", plate.Settings.WellCount);
                WriteGeneralRow("String", Resources.Excel_Xml_Plate_WellShape, "String", plate.Settings.Plate.PlateSpecification.WellShape);
                WriteGeneralRow("String", Resources.Excel_Xml_Plate_BottomShape, "String", plate.Settings.Plate.PlateSpecification.WellBottomShape);
                WriteGeneralRow("String", Resources.Excel_Xml_Plate_Orientation, "String", plate.Settings.Orientation.Value.ToString());
                WriteGeneralRow("String", Resources.Excel_Xml_Plate_IsLidded, "String", plate.Settings.IsLidded.Value.ToString());
                WriteGeneralRow("String", Resources.Excel_Xml_Plate_MeasureDate, "String", plate.Settings.MeasureDate.ToString(mCultureGlobal));
                
                mLoggingAdapter?.Info($"Write Plate Data => {GimliTrace.Stop("WritePlateData")} ms");
                
                WriteSnapshotDataToSheet(plate);
            }
        }
        private void WriteSnapshotDataToSheet(Plate plate)
        {
            foreach (var snapshot in plate.SnapShot)
            {
                GimliTrace.Start("WriteSnapshotData");

                WriteSettingsReductionToSheet(snapshot);
                WriteSettingsDataToSheet(snapshot);
                WriteWellDataToSheet(snapshot, plate);

                mOffsetRow += 2;

                mLoggingAdapter?.Info($"Write Snapshot Data => {GimliTrace.Stop("WriteSnapshotData")} ms");
            }
        }
        private void WriteSettingsReductionToSheet(Snapshot snapshot)
        {
            if (mDocument.Version == null || mDocument.Version.Major < 2 || snapshot.ReductionSettings == null || !snapshot.ReductionSettings.IsUsed)
            {
                return;
            }

            //Data Reduction is preset during creation, but is not shown nor used in onboard => hide in export
            if (snapshot.Settings?.MeasurementType == MeasurementType.Kinetic &&
                snapshot.Settings?.MeasurementMode == Mode.Fp)
            {
                return;
            }

            GimliTrace.Start("WriteReductionSettingsData");

            mOffsetRow++;
            WriteGeneralRow("String", Resources.Excel_Xml_Reduction_Header, null, null, 1);
            WriteParameterRow(snapshot.ReductionSettings.Method, "String", Resources.Excel_Xml_Reduction_Method);
            WriteParameterRow(snapshot.ReductionSettings.ParametersDouble[0], "String", Resources.Excel_Xml_Reduction_Parameter_1);
            
            mLoggingAdapter?.Info($"Write Reduction Settings Data => {GimliTrace.Stop("WriteReductionSettingsData")} ms");
        }
        private void WriteSettingsDataToSheet(Snapshot snapshot)
        {
            GimliTrace.Start("WriteSettingsData");

            mOffsetRow++;
            WriteGeneralRow("String", Resources.Excel_Xml_Settings_Header, null, null, 1);
            WriteGeneralRow("String", Resources.Excel_Xml_Settings_Mode, "String", snapshot.Settings?.MeasurementMode.ToString());
            WriteGeneralRow("String", Resources.Excel_Xml_Settings_Type, "String", snapshot.Settings?.MeasurementType.ToString());

            mLoggingAdapter?.Info($"Write Settings Data => {GimliTrace.Stop("WriteSettingsData")} ms");
            
            WriteSettingsWavelengthDataToSheet(snapshot);
            WriteSettingsDetectionDataToSheet(snapshot);
            WriteSettingsKineticDataToSheet(snapshot);
            WriteSettingsShakeDataToSheet(snapshot);
            WriteSettingsReadAreaDataToSheet(snapshot);
            WriteSettingsWellScanDataToSheet(snapshot);
        }
        private void WriteSettingsWavelengthDataToSheet(Snapshot snapshot)
        {
            if (!snapshot.Settings.Wavelengths.IsUsed)
            {
                return;
            }

            GimliTrace.Start("WriteSettingsWavelengtData");

            mOffsetRow++;
            WriteGeneralRow("String", Resources.Excel_Xml_Wavelength_Header, null, null, 1);
            WriteParameterRow(snapshot.Settings.Wavelengths.Attenuation, "String", Resources.Excel_Xml_Wavelength_Attenuation);
            WriteParameterRow(snapshot.Settings.Wavelengths.ExcitationStart, "String", Resources.Excel_Xml_Wavelength_Excitation_Start);
            WriteParameterRow(snapshot.Settings.Wavelengths.ExcitationEnd, "String", Resources.Excel_Xml_Wavelength_Excitation_End);
            WriteParameterRow(snapshot.Settings.Wavelengths.ExcitationStep, "String", Resources.Excel_Xml_Wavelength_Excitation_Step);
            WriteParameterRow(snapshot.Settings.Wavelengths.EmissionStart, "String", Resources.Excel_Xml_Wavelength_Emission_Start);
            WriteParameterRow(snapshot.Settings.Wavelengths.EmissionEnd, "String", Resources.Excel_Xml_Wavelength_Emission_End);
            WriteParameterRow(snapshot.Settings.Wavelengths.EmissionStep, "String", Resources.Excel_Xml_Wavelength_Emission_Step);
            WriteParameterRow(snapshot.Settings.Wavelengths.ExcitationSweep, "String", Resources.Excel_Xml_Wavelength_Excitation_Sweep);
            WriteParameterRow(snapshot.Settings.Wavelengths.PathCheck, "String", Resources.Excel_Xml_Wavelength_PathCheck);

            JsonObjects.Settings.ItemClass settingsItemMethod = null;

            try
            {
                settingsItemMethod =
                    snapshot.Settings.GetSummary?.Where(i => i.Header == "Wavelength").ToArray()[0]?.Items?.Where(
                        j => j.Title == "Method").ToArray()[0];
            }
            catch (Exception e)
            {
                mLoggingAdapter?.Info($"WriteSettings (Wavelength) Method (Get)| Exception ({e.Message})");
            }

            if (settingsItemMethod != null)
            {
                WriteGeneralRow("String", Resources.Excel_Xml_Detection_Wavelength_Method, "String", settingsItemMethod.ItemValue);
            }

            if (snapshot.Settings.Wavelengths.NrOfWavelengths.IsUsed && snapshot.Settings.Wavelengths.NrOfWavelengths.Value != null)
            {
                for (var iWl = 0; iWl < snapshot.Settings.Wavelengths.NrOfWavelengths.ToInt(); iWl++)
                {
                    WriteGeneralRow("String", iWl == 0 ? Resources.Excel_Xml_Wavelength_WLList_Header : null, "String",
                        (snapshot.Settings.Wavelengths.Excitation.Count > iWl
                            ? (snapshot.Settings.Wavelengths.Excitation[iWl].Value + " " + snapshot.Settings.Wavelengths.Excitation[iWl].Units)
                            : "-") + "/" +
                        (snapshot.Settings.Wavelengths.Emission.Count > iWl
                            ? (snapshot.Settings.Wavelengths.Emission[iWl].Value + " " + snapshot.Settings.Wavelengths.Emission[iWl].Units)
                            : "-"));
                }
            }
            
            mLoggingAdapter?.Info($"Write Settings (Wavelength) Data => {GimliTrace.Stop("WriteSettingsWavelengthData")} ms");
        }
        private void WriteSettingsDetectionDataToSheet(Snapshot snapshot)
        {
            if (!snapshot.Settings.DetectionSetting.IsUsed)
            {
                return;
            }

            GimliTrace.Start("WriteSettingsDetectionData");

            mOffsetRow++;
            WriteGeneralRow("String", Resources.Excel_Xml_Detection_Header, null, null, 1);
            WriteParameterRow(snapshot.Settings.DetectionSetting.IntegrationTime, "String", Resources.Excel_Xml_Detection_IntegrationTime);
            WriteParameterRow(snapshot.Settings.DetectionSetting.PmtSensitivity, "String", Resources.Excel_Xml_Detection_PmtSensitivity);
	        if (snapshot.Settings.DetectionSetting.AttenuationValue != null)
	        {
				WriteParameterRow(snapshot.Settings.DetectionSetting.AttenuationValue, "String", Resources.Excel_Xml_Detection_Attenuation);
			}
			WriteParameterRow(snapshot.Settings.DetectionSetting.SpeedRead, "String", Resources.Excel_Xml_Detection_SpeedRead);
            WriteParameterRow(snapshot.Settings.DetectionSetting.Normalization, "String", Resources.Excel_Xml_Detection_Normalization);
            WriteParameterRow(snapshot.Settings.DetectionSetting.ReadFromBottom, "String", Resources.Excel_Xml_Detection_BottomRead);
            WriteParameterRow(snapshot.Settings.DetectionSetting.ExcitationTime, "String", Resources.Excel_Xml_Detection_ExcitationTime);
            WriteParameterRow(snapshot.Settings.DetectionSetting.MeasurementDelay, "String", Resources.Excel_Xml_Detection_MeasurementDelay);
	        if (snapshot.Settings.DetectionSetting.NumberOfPulses != null)
	        {
		        WriteParameterRow(snapshot.Settings.DetectionSetting.NumberOfPulses, "String", Resources.Excel_Xml_Detection_NumberOfPulses);
	        }
			WriteParameterRow(snapshot.Settings.DetectionSetting.ReadHeight, "String", Resources.Excel_Xml_Detection_ReadHeight);

			mLoggingAdapter?.Info($"Write Settings (Detection) Data => {GimliTrace.Stop("WriteSettingsDetectionData")} ms");
        }
        private void WriteSettingsKineticDataToSheet(Snapshot snapshot)
        {
            if (!snapshot.Settings.Kinetic.IsUsed)
            {
                return;
            }

            GimliTrace.Start("WriteSettingsKineticData");

            mOffsetRow++;
            WriteGeneralRow("String", Resources.Excel_Xml_Kinetic_Header, null, null, 1);
            WriteParameterRow(snapshot.Settings.Kinetic.TotalRunTime, "String", Resources.Excel_Xml_Kinetic_TotalRunTime);
            WriteParameterRow(snapshot.Settings.Kinetic.Cycles, "String", Resources.Excel_Xml_Kinetic_Cycles);
            WriteParameterRow(snapshot.Settings.Kinetic.Intervall, "String", Resources.Excel_Xml_Kinetic_Interval);

            mLoggingAdapter?.Info($"Write Settings (Kinetic) Data => {GimliTrace.Stop("WriteSettingsKineticData")} ms");
            
            WriteSettingsShakeDataToSheet(snapshot, true);
        }
        private void WriteSettingsShakeDataToSheet(Snapshot snapshot, bool kinetic = false)
        {
            var shakeSettingRelevant = kinetic ? snapshot.Settings.Kinetic.IntervallShake : snapshot.Settings.Shake;
            
            if (!shakeSettingRelevant.IsUsed)
            {
                return;
            }

            GimliTrace.Start("WriteSettingsShakeData" + kinetic);

            mOffsetRow++;
            WriteGeneralRow("String", kinetic ? Resources.Excel_Xml_Shake_Header_Kinetic : Resources.Excel_Xml_Shake_Header, null, null, 1);
            WriteParameterRow(shakeSettingRelevant.DoShake, "String", Resources.Excel_Xml_Shake_DoShake);
            WriteParameterRow(shakeSettingRelevant.Duration, "String", Resources.Excel_Xml_Shake_Duration);
            WriteParameterRow(shakeSettingRelevant.Mode, "String", Resources.Excel_Xml_Shake_Mode);
            WriteParameterRow(shakeSettingRelevant.Intensity, "String", Resources.Excel_Xml_Shake_Intensity);

            mLoggingAdapter?.Info($"Write Settings (Shake({kinetic})) Data => {GimliTrace.Stop("WriteSettingsShakeData" + kinetic)} ms");
        }
        private void WriteSettingsReadAreaDataToSheet(Snapshot snapshot)
        {
            if (!snapshot.Settings.ReadArea.IsUsed)
            {
                return;
            }

            GimliTrace.Start("WriteSettingsReadAreaData");

            mOffsetRow++;
            WriteGeneralRow("String", Resources.Excel_Xml_ReadArea_Header, null, null, 1);
            WriteParameterRow(snapshot.Settings.ReadArea.InterlacedReading, "String", Resources.Excel_Xml_ReadArea_Interlaced);
            WriteParameterRow(snapshot.Settings.ReadArea.ReadOrder, "String", Resources.Excel_Xml_ReadArea_ReadOrder);

            mLoggingAdapter?.Info($"Write Settings (ReadArea) Data => {GimliTrace.Stop("WriteSettingsReadAreaData")} ms");
        }
        private void WriteSettingsWellScanDataToSheet(Snapshot snapshot)
        {
            if (!snapshot.Settings.WellScan.IsUsed)
            {
                return;
            }

            GimliTrace.Start("WriteSettingsWellScanData");

            mOffsetRow++;
            WriteGeneralRow("String", Resources.Excel_Xml_WellScan_Header, null, null, 1);
            WriteParameterRow(snapshot.Settings.WellScan.ScanMode, "String", Resources.Excel_Xml_WellScan_ScanMode);
            WriteParameterRow(snapshot.Settings.WellScan.X_Spacing, "String", Resources.Excel_Xml_WellScan_X_Spacing);
            WriteParameterRow(snapshot.Settings.WellScan.Y_Spacing, "String", Resources.Excel_Xml_WellScan_Y_Spacing);
            WriteParameterRow(snapshot.Settings.WellScan.X_Points, "String", Resources.Excel_Xml_WellScan_X_Points);
            WriteParameterRow(snapshot.Settings.WellScan.Y_Points, "String", Resources.Excel_Xml_WellScan_Y_Points);
            WriteParameterRow(snapshot.Settings.WellScan.X_Center, "String", Resources.Excel_Xml_WellScan_X_Center);
            WriteParameterRow(snapshot.Settings.WellScan.Y_Center, "String", Resources.Excel_Xml_WellScan_Y_Center);

            mLoggingAdapter?.Info($"Write Settings (WellScan) Data => {GimliTrace.Stop("WriteSettingsWellScanData")} ms");
        }

        private void WriteWellDataToSheet(Snapshot snapshot, Plate plate)
        {
            if (!snapshot.HasData || !CheckHasData(snapshot.Wells[0].Points))
            {
                return;
            }

            GimliTrace.Start("WriteWellData");

            mOffsetRow++;
            WriteGeneralRow("String", Resources.Excel_Xml_WellData_Header, null, null, 1); //TBD!!
            switch (snapshot.Settings.MeasurementType)
            {
                case MeasurementType.Endpoint:
                    WriteWellEndpointDataToSheet(snapshot, plate);
                    break;
                case MeasurementType.Kinetic:
                    WriteWellKineticDataToSheet(snapshot, plate);
                    break;
                case MeasurementType.SpectrumScan:
                    WriteWellSpectrumScanDataToSheet(snapshot, plate);
                    break;
                case MeasurementType.None:
                case MeasurementType.AreaScan:
                    break;
            }

            mLoggingAdapter?.Info($"Write Well Data => {GimliTrace.Stop("WriteWellData")} ms");
        }
        private void WriteWellEndpointDataToSheet(Snapshot snapshot, Plate plate)
        {
            switch (snapshot.Settings.MeasurementMode)
            {
                case Mode.Fp:
                    WriteWellEndpointDataFp(snapshot, plate);
                    break;
                case Mode.Abs:
                case Mode.Fl:
                case Mode.Lum:
                case Mode.Trf:
                    WriteWellEndpointDataAllWl(snapshot, plate);
                    break;
                case Mode.None:
                    break;
            }
        }
        private void WriteWellEndpointDataFp(Snapshot snapshot, Plate plate)
        {
            GimliTrace.Start("WriteWellDataFp");

            var noForOutput = 3;

            var statusV = new DataPointStatus[plate.Rows, plate.Columns];
            var outputV = new double[plate.Rows, plate.Columns];
            var statusH = new DataPointStatus[plate.Rows, plate.Columns];
            var outputH = new double[plate.Rows, plate.Columns];
            var redsta = new DataPointStatus[plate.Rows, plate.Columns];
            var redval = new double[plate.Rows, plate.Columns];

            foreach (var well in snapshot.Wells)
            {
                outputV[well.YPos, well.XPos] = well.Points[0, 0, 0];
                statusV[well.YPos, well.XPos] = well.DataPointsState[0, 0, 0];
                outputH[well.YPos, well.XPos] = well.Points[0, 1, 0];
                statusH[well.YPos, well.XPos] = well.DataPointsState[0, 1, 0];
                redsta[well.YPos, well.XPos] = well.ReductionState;
                redval[well.YPos, well.XPos] = well.ReductionValue;
            }

            WriteHeaderFp(snapshot);

            mOffsetCol = 2;

            for (int i = 0; i < noForOutput; i++)
            {
                WriteCell("NRB", "String", GetFpDesc(i), mOffsetCol);
                mOffsetCol++;
                WriteHeaderColEndpoint(plate);
                mOffsetCol++;
            }
            mXmlTextWriter.WriteEndElement();

            mOffsetRow++;
            mOffsetCol = 2;

            for (var iRow = 0; iRow < outputV.GetLength(0); iRow++)
            {
                WriteRow(15, mOffsetRow);

                if (mOffsetCol == 2)
                {
                    for (int i = 0; i < noForOutput; i++)
                    {
                        var offsetCol = 2 + 2*i + (plate.Columns)*i;

                        WriteCell("NRB", "String", GetColumnName(iRow + 1), offsetCol);
                        offsetCol++;

                        DataPointStatus[,] relStatus = new DataPointStatus[0,0];
                        double[,] relOutput = new double[0,0];

                        switch (i)
                        {
                            case 0:
                                relStatus = statusV;
                                relOutput = outputV;
                                break;
                            case 1:
                                relStatus = statusH;
                                relOutput = outputH;
                                break;
                            case 2:
                                relStatus = redsta;
                                relOutput = redval;
                                break;
                        }

                        for (var iCol = 0; iCol < relOutput.GetLength(1); iCol++)
                        {
                            WriteDataCell(relOutput[iRow, iCol], relStatus[iRow, iCol], ref offsetCol);
                        }
                    }
                }

                mXmlTextWriter.WriteEndElement();
                mOffsetRow++;
            }
            mOffsetRow++;

            mLoggingAdapter?.Info($"Write Well Data FP => {GimliTrace.Stop("WriteWellDataFp")} ms");
        }
        private void WriteWellEndpointDataAllWl(Snapshot snapshot, Plate plate)
        {
            for (var iWl = 0; iWl < snapshot.Settings.Wavelengths.NrOfWavelengths.ToInt(); iWl++)
            {
                WriteWellEndpointDataPerWl(snapshot, plate, iWl);
            }
        }
        private void WriteWellEndpointDataPerWl(Snapshot snapshot, Plate plate, int wl)
        {
            GimliTrace.Start("WriteWellData" + wl);

            var status = new DataPointStatus[plate.Rows, plate.Columns];
            var output = new double[plate.Rows, plate.Columns];

            foreach (var well in snapshot.Wells)
            {
                output[well.YPos, well.XPos] = well.Points[0, wl, 0];
                status[well.YPos, well.XPos] = well.DataPointsState[0, wl, 0];
            }

            WriteHeaderWl(snapshot, wl);
            WriteHeaderColEndpoint(plate);
            mXmlTextWriter.WriteEndElement();

            mOffsetRow++;
            mOffsetCol = 2;

            for (var iRow = 0; iRow < output.GetLength(0); iRow++)
            {
                WriteRow(15, mOffsetRow);

                if (mOffsetCol == 2)
                {
                    WriteCell("NRB", "String", GetColumnName(iRow + 1), mOffsetCol);
                }

                var offsetCol = mOffsetCol + 1;

                for (var iCol = 0; iCol < output.GetLength(1); iCol++)
                {
                    WriteDataCell(output[iRow, iCol], status[iRow, iCol], ref offsetCol);
                }

                mXmlTextWriter.WriteEndElement();
                mOffsetRow++;
            }

            mOffsetRow++;

            mLoggingAdapter?.Info($"Write Well Data (WL: {wl}) => {GimliTrace.Stop("WriteWellData" + wl)} ms");
        }

        private void WriteWellKineticDataToSheet(Snapshot snapshot, Plate plate)
        {
            switch (snapshot.Settings.MeasurementMode)
            {
                case Mode.Fp:
                    WriteWellKineticDataFp(snapshot, plate);
                    break;
                case Mode.Abs:
                case Mode.Fl:
                case Mode.Lum:
                case Mode.Trf:
                    WriteWellKineticDataAllWl(snapshot, plate);
                    break;
                case Mode.None:
                    break;
            }
        }
        private void WriteWellKineticDataFp(Snapshot snapshot, Plate plate)
        {
            GimliTrace.Start("WriteWellDataFp");

            var noForOutput = 2;

            var statusH = GetPointStatusArray(snapshot, plate);
            var outputH = GetOutputArray(snapshot, plate);
            var statusV = GetPointStatusArray(snapshot, plate);
            var outputV = GetOutputArray(snapshot, plate);

            foreach (var well in snapshot.Wells)
            {
                for (var iCyc = 0; iCyc < well.Points.GetLength(2); iCyc++)
                {
                    statusH[iCyc, well.YPos, well.XPos] = well.DataPointsState[0, 0, iCyc];
                    outputH[iCyc, well.YPos, well.XPos] = well.Points[0, 0, iCyc];
                    statusV[iCyc, well.YPos, well.XPos] = well.DataPointsState[0, 1, iCyc];
                    outputV[iCyc, well.YPos, well.XPos] = well.Points[0, 1, iCyc];
                }
            }
            
            for (int i = 0; i < noForOutput; i++)
            {
                mOffsetCol = 2;
                WriteHeaderFp(snapshot);
                WriteColumnNames3D(outputH, GetFpDesc(i));

                var relStatus = GetPointStatusArray(snapshot, plate);
                var relOutput = GetOutputArray(snapshot, plate);

                switch (i)
                {
                    case 0:
                        relStatus = statusV;
                        relOutput = outputV;
                        break;
                    case 1:
                        relStatus = statusH;
                        relOutput = outputH;
                        break;
                }

                for (var iCyc = 0; iCyc < relOutput.GetLength(0); iCyc++)
                {
                    var offsetCol = WriteKineticCycInfo(snapshot.DataDimensionInfo[2].DataPointLabel[iCyc], mOffsetCol + 1);

                    for (var iRow = 0; iRow < relOutput.GetLength(1); iRow++)
                    {
                        for (var iCol = 0; iCol < relOutput.GetLength(2); iCol++)
                        {
                            WriteDataCell(relOutput[iCyc, iRow, iCol], relStatus[iCyc, iRow, iCol], ref offsetCol);
                        }
                    }
                    mXmlTextWriter.WriteEndElement();
                    mOffsetRow++;
                }

                mOffsetRow++;
            }
            
            
            mLoggingAdapter?.Info($"Write Well Data FP => {GimliTrace.Stop("WriteWellDataFp")} ms");
        }
        private void WriteWellKineticDataAllWl(Snapshot snapshot, Plate plate)
        {
            for (var iWl = 0; iWl < snapshot.Settings.Wavelengths.NrOfWavelengths.ToInt(); iWl++)
            {
                WriteWellKineticDataPerWl(snapshot, plate, iWl);
            }
        }
        private void WriteWellKineticDataPerWl(Snapshot snapshot, Plate plate, int wl)
        {
            GimliTrace.Start("WriteWellData" + wl);

            var status = GetPointStatusArray(snapshot, plate);
            var output = GetOutputArray(snapshot, plate);

            foreach (var well in snapshot.Wells)
            {
                for (var iCyc = 0; iCyc < well.Points.GetLength(2); iCyc++)
                {
                    status[iCyc, well.YPos, well.XPos] = well.DataPointsState[0, wl, iCyc];
                    output[iCyc, well.YPos, well.XPos] = well.Points[0, wl, iCyc];
                }
            }

            WriteHeaderWl(snapshot, wl);
            WriteCell("NLB", "String", string.Format(Resources.Excel_Xml_WellData_Kinetic_TAB_Header, snapshot.DataDimensionInfo[2].DimensionName), mOffsetCol);

            WriteColumnNames3D(output);

            for (var iCyc = 0; iCyc < output.GetLength(0); iCyc++)
            {
                var offsetCol = WriteKineticCycInfo(snapshot.DataDimensionInfo[2].DataPointLabel[iCyc], mOffsetCol + 1);

                for (var iRow = 0; iRow < output.GetLength(1); iRow++)
                {
                    for (var iCol = 0; iCol < output.GetLength(2); iCol++)
                    {
                        WriteDataCell(output[iCyc, iRow, iCol], status[iCyc, iRow, iCol], ref offsetCol);
                    }
                }

                mXmlTextWriter.WriteEndElement();
                mOffsetRow++;
            }

            mOffsetRow++;

            mLoggingAdapter?.Info($"Write Well Data (WL: {wl}) => {GimliTrace.Stop("WriteWellData" + wl)} ms");
        }
        
	    private static double[,,] GetOutputArray(Snapshot snapshot, Plate plate)
	    {
		    return new double[snapshot.Wells[0].Points.GetLength(2), plate.Rows, plate.Columns];
	    }

	    private static DataPointStatus[,,] GetPointStatusArray(Snapshot snapshot, Plate plate)
	    {
		    return new DataPointStatus[snapshot.Wells[0].Points.GetLength(2), plate.Rows, plate.Columns];
	    }

	    private void WriteWellSpectrumScanDataToSheet(Snapshot snapshot, Plate plate)
        {
            GimliTrace.Start("WriteWellData");

			var spectrumStatus = GetPointStatusArray(snapshot, plate);
			var spectrumOutput = GetOutputArray(snapshot, plate);

			foreach (var well in snapshot.Wells)
            {
                for (var iWl = 0; iWl < well.Points.GetLength(2); iWl++)
                {
                    spectrumStatus[iWl, well.YPos, well.XPos] = well.DataPointsState[0, 0, iWl];
                    spectrumOutput[iWl, well.YPos, well.XPos] = well.Points[0, 0, iWl];
                }
            }

            mOffsetCol = 3;
            WriteRow(15, mOffsetRow);
            WriteCell("NLB", "String", Resources.Excel_Xml_WellData_Spectrum_TAB_Header, mOffsetCol);

            WriteColumnNames3D(spectrumOutput);
            
            for (var iWl = 0; iWl < spectrumOutput.GetLength(0); iWl++)
            {
                WriteRow(15, mOffsetRow);

                var offsetCol = mOffsetCol + 1;
                var wlstring = "";

                if (snapshot.Settings.Wavelengths.ExcitationStart.IsUsed)
                {
                    var wl = snapshot.Settings.Wavelengths.ExcitationStart.ToInt() +
                             iWl * snapshot.Settings.Wavelengths.ExcitationStep.ToInt();
                    wlstring += wl;
                }

                WriteCell("NRB", "Number", wlstring, offsetCol);
                offsetCol++;

                for (var iRow = 0; iRow < spectrumOutput.GetLength(1); iRow++)
                {
                    for (var iCol = 0; iCol < spectrumOutput.GetLength(2); iCol++)
                    {
                        WriteDataCell(spectrumOutput[iWl, iRow, iCol], spectrumStatus[iWl, iRow, iCol], ref offsetCol);
                    }
                }

                mXmlTextWriter.WriteEndElement();
                mOffsetRow++;
            }

            mOffsetRow++;

            mLoggingAdapter?.Info($"Write Well Data => {GimliTrace.Stop("WriteWellData")} ms");
        }
        
        private void WriteParameterRow(IParameter parameter, string type, string description)
        {
            if (parameter.IsUsed)
            {
                WriteGeneralRow(type, description, type, parameter.Value?.ToString());
            }
        }
        private void WriteDataCell(double value, DataPointStatus status, ref int offsetCol)
        {
            switch (status)
            {
                case DataPointStatus.Unused:
                    break;
                case DataPointStatus.OK:
                    WriteCell(status + "NRB", "Number", Math.Round(value, 3).ToString(mCultureGlobal), offsetCol);
                    break;
                case DataPointStatus.Overflow:
                    WriteCell(status + "NRB", "String", "#SAT", offsetCol);
                    break;
                case DataPointStatus.Error:
                case DataPointStatus.Incorrect:
                case DataPointStatus.Rejected:
                case DataPointStatus.Underflow:
                case DataPointStatus.Extrapolated:
                case DataPointStatus.NotEvaluated:
                    WriteCell(status + "NRB", "String", status.ToString(), offsetCol);
                    break;
                default:
                    WriteCell(status + "NRB", "String", status.ToString(), offsetCol);
                    break;
            }
            offsetCol++;
        }
        private void WriteHeaderFp(Snapshot snapshot)
        {
            WriteRow(15, mOffsetRow);    
            WriteCell("NLB", "String", $"{snapshot.Settings.MeasurementMode.ToString().ToUpper()} ({snapshot.DataDimensionInfo[1].Name})", mOffsetCol);
            mOffsetCol++;
        }
        private void WriteHeaderWl(Snapshot snapshot, int wlCount)
        {
            var ex = snapshot.Settings.Wavelengths.Excitation.Count > wlCount ? (snapshot.Settings.Wavelengths.Excitation[wlCount].Value + " " + snapshot.Settings.Wavelengths.Excitation[wlCount].Units) : "-";
            var em = snapshot.Settings.Wavelengths.Emission.Count > wlCount ? (snapshot.Settings.Wavelengths.Emission[wlCount].Value + " " + snapshot.Settings.Wavelengths.Emission[wlCount].Units) : "-";

            mOffsetCol = 1;
            WriteRow(15, mOffsetRow);
            WriteCell("NLB", "String", Resources.Excel_Xml_WellData_WL_Header, mOffsetCol);
            mOffsetCol++;
            WriteCell("NLB", "String", $"{ex}/{em}", mOffsetCol);
            mOffsetCol++;
        }
        private void WriteColumnNames3D(double[,,] output, string fptext = null)
        {
            if (fptext != null)
            {
                WriteCell("NRB", "String", fptext, mOffsetCol);
            }
            mOffsetCol++;

            for (var iRow = 0; iRow < output.GetLength(1); iRow++)
            {
                for (var iCol = 0; iCol < output.GetLength(2); iCol++)
                {
                    WriteCell("NLB", "String", GetColumnName(iRow + 1) + (iCol + 1), mOffsetCol);
                    mOffsetCol++;
                }
            }
            mXmlTextWriter.WriteEndElement();

            mOffsetRow++;
            mOffsetCol = 2;
        }
        private bool CheckHasData(double[,,] points)
        {
            if (points != null)
            {
                return true;
            }

            WriteGeneralRow("String", "No Data!", null, null, 1); //TBD!!
            mOffsetRow++;
            return false;
        }

        private void WriteIntro()
        {
            mXmlTextWriter.WriteStartElement("Workbook");
            mXmlTextWriter.WriteAttributeString("xmlns", null, null, "urn:schemas-microsoft-com:office:spreadsheet");
            mXmlTextWriter.WriteAttributeString("xmlns", "o", null, "urn:schemas-microsoft-com:office:office");
            mXmlTextWriter.WriteAttributeString("xmlns", "x", null, "urn:schemas-microsoft-com:office:excel");
            mXmlTextWriter.WriteAttributeString("xmlns", "ss", null, "urn:schemas-microsoft-com:office:spreadsheet");
            mXmlTextWriter.WriteAttributeString("xmlns", "html", null, "http://www.w3.org/TR/REC-html40");

            mXmlTextWriter.WriteStartElement("DocumentProperties");
            mXmlTextWriter.WriteAttributeString("xmlns", null, null, "urn:schemas-microsoft-com:office:office");
            mXmlTextWriter.WriteElementString("LastAuthor", "DTX-Software");
            mXmlTextWriter.WriteElementString("Created", DateTime.Now.ToShortDateString());
            mXmlTextWriter.WriteElementString("Version", "1.0");
            mXmlTextWriter.WriteEndElement();

            mXmlTextWriter.WriteStartElement("ExcelWorkbook");
            mXmlTextWriter.WriteAttributeString("xmlns", null, null, "urn:schemas-microsoft-com:office:excel");
            mXmlTextWriter.WriteElementString("ProtectedStructure", "False");
            mXmlTextWriter.WriteElementString("ProtectedWindows", "False");
            mXmlTextWriter.WriteEndElement();

            WriteStyles();
        }

        private int WriteWorkSheet(string name)
        {
            mXmlTextWriter.WriteStartElement("Worksheet");
            mXmlTextWriter.WriteAttributeString("ss", "Name", null, CheckOfIllegalChars(name));
            mXmlTextWriter.WriteStartElement("Table");
            mXmlTextWriter.WriteAttributeString("x", "FullColumns", null, "1");
            mXmlTextWriter.WriteAttributeString("x", "FullRows", null, "1");

            mOffsetRow = 1;

            return 2;
        }
        private void ResizeColumns()
        {
            WriteColumnWidth(150, 1);
            WriteColumnWidth(150, 2);
        }
        private void WriteColumnWidth(int width, int index, bool autofit = false)
        {
            mXmlTextWriter.WriteStartElement("Column");

            mXmlTextWriter.WriteAttributeString("ss", "AutoFitWidth", null, autofit ? "1" : "0");
            mXmlTextWriter.WriteAttributeString("ss", "Width", null, width.ToString());

            if (index > 0)
            {
                mXmlTextWriter.WriteAttributeString("ss", "Index", null, index.ToString());
            }

            mXmlTextWriter.WriteEndElement();
        }
        private void WriteRow(int height, int index)
        {
            mXmlTextWriter.WriteStartElement("Row");
            mXmlTextWriter.WriteAttributeString("ss", "Height", null, height.ToString());
            if (index != 0)
                mXmlTextWriter.WriteAttributeString("ss", "Index", null, index.ToString());
        }
        private void WriteGeneralRow(string type1, string name, string type2, string strvalue, int cellsToMerge = 0, bool noClose = false)
        {
            
            WriteRow(15, noClose ? mOffsetRow : mOffsetRow++);

            WriteCell("BLB", type1, name, 1, cellsToMerge);

            switch (type2)
            {
                case null:
                    break;
                case "DateTime":
                    WriteCell("DTRB", type2, strvalue, 2);
                    break;
                default:
                    WriteCell("NLB", type2, strvalue, 2);
                    break;
            }

            if (!noClose)
            {
                mXmlTextWriter.WriteEndElement();
            }
        }
        private void WriteCell(string style, string type, string strvalue, int index, int cellsToMerge = 0)
        {
            mXmlTextWriter.WriteStartElement("Cell");

            if (cellsToMerge > 0)
            {
                mXmlTextWriter.WriteAttributeString("ss", "MergeAcross", null, cellsToMerge.ToString());
            }

            if (type == "Formula")
            {
                mXmlTextWriter.WriteAttributeString("ss", "Index", null, index.ToString());
                mXmlTextWriter.WriteAttributeString("ss", "Formula", null, strvalue);
            }
            else
            {
                mXmlTextWriter.WriteAttributeString("ss", "StyleID", null, style);
                if (index != 0)
                    mXmlTextWriter.WriteAttributeString("ss", "Index", null, index.ToString());
                mXmlTextWriter.WriteStartElement("Data");
                mXmlTextWriter.WriteAttributeString("ss", "Type", null, type);
                mXmlTextWriter.WriteString(strvalue);
                mXmlTextWriter.WriteEndElement();
            }

            mXmlTextWriter.WriteEndElement();
        }
        private void WriteEndElement(int count)
        {
            for (var i = 0; i < count; i++)
            {
                mXmlTextWriter.WriteEndElement();
            }
        }
        private void WriteStyles()
        {
            mXmlTextWriter.WriteStartElement("Styles");
            WriteStyle("Default", null, "Bottom", null, "Normal", null, null, null);
            WriteStyle("BCC", "Center", "Center", "1", null, "1", null, null);
            WriteStyle("BLB", "Left", "Bottom", "1", null, "1", null, null);
            WriteStyle("NRB", "Right", "Bottom", "1", null, null, null, null);
            WriteStyle("NLB", "Left", "Bottom", "1", null, null, null, null);
            WriteStyle("DTRB", "Right", "Bottom", "1", null, null, "General Date", null);
            WriteStyle(DataPointStatus.Error + "NRB", "Right", "Bottom", "1", null, null, null, "#FF9900");
            WriteStyle(DataPointStatus.Unused + "NRB", "Right", "Bottom", "1", null, null, null, "#C0C0C0");
            WriteStyle(DataPointStatus.Incorrect + "NRB", "Right", "Bottom", "1", null, null, null, "#33CCCC");
            WriteStyle(DataPointStatus.Overflow + "NRB", "Right", "Bottom", "1", null, null, null, "#FF0000");
            WriteStyle(DataPointStatus.Underflow + "NRB", "Right", "Bottom", "1", null, null, null, "#FF0000");
            WriteStyle(DataPointStatus.OK + "NRB", "Right", "Bottom", "1", null, null, null, null);
            WriteStyle(DataPointStatus.Rejected + "NRB", "Right", "Bottom", "1", null, null, null, "#33CC33");
            
            mXmlTextWriter.WriteEndElement();
        }
        private void WriteStyle(string id, string horAlign, string verAlign, string wrapText, string name, string fontBold, string format, string color)
        {
            mXmlTextWriter.WriteStartElement("Style");
            mXmlTextWriter.WriteAttributeString("ss", "ID", null, id);
            if (name != null)
                mXmlTextWriter.WriteAttributeString("ss", "Name", null, name);

            WriteAlignment(id, horAlign, verAlign, wrapText, name);
            WriteFont("Arial Unicode MS", "Swiss", fontBold, color);

            if (format != null)
                WriteNumberFormat(format);

            mXmlTextWriter.WriteEndElement();
        }
        private void WriteAlignment(string id, string horAlign, string verAlign, string wrapText, string name)
        {
            mXmlTextWriter.WriteStartElement("Alignment");
            if (verAlign != null)
                mXmlTextWriter.WriteAttributeString("ss", "Vertical", null, verAlign);
            if (horAlign != null)
                mXmlTextWriter.WriteAttributeString("ss", "Horizontal", null, horAlign);
            if (wrapText != null)
                mXmlTextWriter.WriteAttributeString("ss", "WrapText", null, wrapText);
            mXmlTextWriter.WriteEndElement();
        }
        private void WriteFont(string fontName, string family, string fontBold, string color)
        {
            mXmlTextWriter.WriteStartElement("Font");
            if (fontName != null)
                mXmlTextWriter.WriteAttributeString("ss", "FontName", null, fontName);
            if (family != null)
                mXmlTextWriter.WriteAttributeString("ss", "Family", null, family);
            if (fontBold != null)
                mXmlTextWriter.WriteAttributeString("ss", "Bold", null, fontBold);
            if (color != null)
                mXmlTextWriter.WriteAttributeString("ss", "Color", null, color);
            mXmlTextWriter.WriteEndElement();
        }
        private void WriteNumberFormat(string format)
        {
            mXmlTextWriter.WriteStartElement("NumberFormat");
            if (format != null)
                mXmlTextWriter.WriteAttributeString("ss", "Format", null, format);
            mXmlTextWriter.WriteEndElement();
        }
        private static string CheckOfIllegalChars(string name)
        {
            if (string.IsNullOrEmpty(name))
                return name;

            name = name.Replace(@"\", " ");
            name = name.Replace('/', ' ');
            name = name.Replace(':', ' ');
            name = name.Replace('?', ' ');
            name = name.Replace('*', ' ');
            name = name.Replace('[', ' ');
            name = name.Replace(']', ' ');

            return name;
        }
        private static string GetColumnName(int column)
        {
            var pref = 0;
            while (column > 26)
            {
                column -= 26;
                pref++;
            }
            
            return string.Format("{0}{1}", pref == 0 ? "" : string.Format("{0}", (char)(64 + pref)), (char)(64 + column));
        }
        private void CreateXmlDocument(string documentName)
        {
            mXmlTextWriter = new XmlTextWriter(documentName, null);
            mXmlTextWriter.WriteStartDocument();
            mXmlTextWriter.Formatting = Formatting.Indented;

            WriteIntro();
        }
        private void SaveXmlDocument()
        {
            mXmlTextWriter.WriteEndDocument();
            mXmlTextWriter.Flush();
            mXmlTextWriter.Close();
        }
        private string GenerateFilename()
        {
            var documentName = mUserFull.UserName + "_" + mDocument.DocumentName + DateTime.Now + "_" + mDocument.TimeStamp;
            documentName = documentName.Replace(":", "_");
            documentName = documentName.Replace("/", "_");

            return documentName + ".xml";
        }

        private string GetFpDesc(int dim)
        {
            switch (dim)
            {
                case 0:
                    return Resources.Excel_Xml_FP_Perp_Raw;
                case 1:
                    return Resources.Excel_Xml_FP_Para_Raw;
                case 2:
                    return Resources.Excel_Xml_FP_Res_Pre;
                default:
                    return "";
            }
        }

        private void WriteHeaderColEndpoint(Plate plate)
        {
            for (var iCol = 0; iCol < plate.Columns; iCol++)
            {
                WriteCell("NRB", "Number", (iCol + 1).ToString(), mOffsetCol);
                mOffsetCol++;
            }
        }
        private int WriteKineticCycInfo(double value, int offsetCol)
        {
            WriteRow(15, mOffsetRow);
            WriteCell("NRB", "Number", value.ToString(mCultureGlobal), offsetCol);
            return offsetCol + 1;
        }
    }
}
 