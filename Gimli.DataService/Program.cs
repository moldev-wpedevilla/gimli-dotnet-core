﻿using System;
using System.Configuration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Gimli.DataService.Actors;
using Gimli.DataStoreFileBased;
using Gimli.JsonObjects.Interfaces;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using System.Reflection;
using Gimli.JsonObjects;

namespace Gimli.DataService
{


	class Program
	{
		static void Main()
		{
			var thisAssemblyName = Assembly.GetEntryAssembly().GetName().Name;
			var configFilePath = thisAssemblyName + ".dll.config";
			AppConfigurationManager.Build(configFilePath);

			bool useSql;
            bool.TryParse(AppConfigurationManager.GetSettingsValue("UseSqlDataStore"), out useSql);

			var factory = new GimliActorHostServiceFactory();

			factory.Run(
				RemoteActorConfiguration.Instance.ServiceConfigurations[ActorServiceKey.Data],
				"Gimli.DataService",
				"Gimli DataService",
				"Gimli DataService with TopShelf",
				configFilePath,
				container =>
				{
					RegisterComponents(container, factory);
				});
		}

		internal static void RegisterComponents(IWindsorContainer container, GimliActorHostServiceFactory factory)
		{
			container.Register(Component.For<IDataStoreFactory>().ImplementedBy<DataStoreFactory>());   // 1) DataStoreFactory for FileBased or 2) DataStoreSqlFactory for SqlLite
			container.Register(Component.For<IDataStore>()
				.UsingFactory((IDataStoreFactory d) => d.Generate())
				.LifestyleSingleton());

			container.Register(Component.For<ISystemDataStoreFactory>().ImplementedBy<SystemDataStoreFactory>());   // 1) DataStoreFactory for FileBased or 2) DataStoreSqlFactory for SqlLite
			container.Register(Component.For<ISystemDataStore>()
				.UsingFactory((ISystemDataStoreFactory d) => d.Generate())
				.LifestyleSingleton());

			container.Register(Component
				.For<IMeasurementContext>()
				.ImplementedBy<MeasurementContext>()
				.LifeStyle.Singleton);
			container.Register(Component
				.For<IActorsystemOverallState>()
				.Instance(new ActorsystemOverallState { ServiceId = ActorServiceKey.Data })
				.LifestyleSingleton());
			container.Register(Component
				.For<IAnthosFunctions>()
				.ImplementedBy<AnthosHelper>()
				.LifestyleSingleton());
			container.Register(Component
				.For<IFileSystemFunctions>()
				.ImplementedBy<FileSystemHelper>()
				.LifestyleSingleton());
			container.Register(Component.For<Func<GimliActorHost>>().Instance(() => factory.Host));
		
		}
	}
}
