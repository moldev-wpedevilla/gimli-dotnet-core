﻿using System;
using System.Collections.Generic;
using System.Linq;
using Anthos.Analysis;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Enumarations.Status;
using Gimli.JsonObjects.Settings;

namespace Gimli.DataService
{
	public static class WellExtensions
	{
		delegate double FpReduction(double parallel, double perpendicular, double gFactor, out AnalysisStatus status);
		public static void ReduceWellData(this Well well, Snapshot snapshot)
		{
			if (snapshot.Settings.MeasurementMode != Mode.Fp) return;
			if (well.Points == null) return;
			if (snapshot.ReductionSettings == null) return;
			if (snapshot.Settings.MeasurementType != MeasurementType.Endpoint) return;

			// prepare well data
			var wlAmount = well.Points.GetLength(0);
			var polarizeLevelAmount = well.Points.GetLength(1);
			var dataAmount = well.Points.GetLength(2);
			var parallelPerpendiculars = new List<List<double>>();
			var parallelPerpendicularsStates = new List<List<DataPointStatus>>();
			for (var wl = 0; wl < wlAmount; wl++)
			{
				var valuesForWavelength = new List<double>();
				var statesForWavelength = new List<DataPointStatus>();

				for (var vH = 0; vH < polarizeLevelAmount; vH++)
				{
					for (var singlePoint = 0; singlePoint < dataAmount; singlePoint++)
					{
						valuesForWavelength.Add(well.Points[wl, vH, singlePoint]);
						statesForWavelength.Add(well.DataPointsState[wl, vH, singlePoint]);
					}
				}

				parallelPerpendiculars.Add(valuesForWavelength);
				parallelPerpendicularsStates.Add(statesForWavelength);
			}

			switch (snapshot.ReductionSettings.Method.Value.ToString())
			{
				case DataReductionSettings.MehtodPolarization:
					CalculateReductionValues(MathAnth.mP, well, snapshot.ReductionSettings, parallelPerpendiculars, parallelPerpendicularsStates);
					break;
				case DataReductionSettings.MehtodAnisotropy:
					CalculateReductionValues(MathAnth.PolrizationAnisotropy, well, snapshot.ReductionSettings, parallelPerpendiculars, parallelPerpendicularsStates);
					break;
				default:
					throw new NotImplementedException("no implementation for this datareduction method");
			}
		}

		private static void CalculateReductionValues(FpReduction reductionMethod, Well well, DataReductionSettings reductionSettings,
			List<List<double>> parallelPerpendiculars, List<List<DataPointStatus>> parallelPerpendicularsStates)
		{
			var gFactor = reductionSettings.ParametersDouble.First(
				p => p.Name == JsonObjects.Resources.GetResource.GetString(DataReductionSettings.ParameterGfactor));
			AnalysisStatus analysisStatus;
			// currently only single wavelength -> take first:
			var firstWavelengthValues = parallelPerpendiculars.First();
			var firstWavelengthStates = parallelPerpendicularsStates.First();

			if (firstWavelengthStates.Any(s => s == DataPointStatus.Unused))
			{
				well.ReductionState = DataPointStatus.Unused;
				return;
			}

			if (firstWavelengthStates.Any(s => s != DataPointStatus.OK))
			{
				well.ReductionState = DataPointStatus.Error;
				return;
			}

			well.ReductionValue = reductionMethod(firstWavelengthValues[0], firstWavelengthValues[1], gFactor.ToDouble(),
				out analysisStatus);
			well.ReductionState = analysisStatus.GetDataStatus();
		}
	}
}
