﻿using System.Globalization;

namespace MolDev.Resources.UI
{
  public class UserMessagesEx : UserMessages
  {
    public static string GetString(string name, CultureInfo resourceCulture)
    {
      if (name == null) return null;
      return ResourceManager.GetString(name, resourceCulture);
    }
  }
}
