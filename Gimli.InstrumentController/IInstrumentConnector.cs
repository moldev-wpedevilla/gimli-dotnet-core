﻿using System;
using Apex.Device;
using Gimli.Instrument;

namespace Gimli.InstrumentController
{
    public interface IInstrumentConnector
    {
        bool Connected { get; }
        FluoroGimli GetReaderObject();
        void Refresh();
        DeviceInitState IsReady();

        void Acknowledge();

        event EventHandler<FilterTagEventArgs> FilterPos;
        event EventHandler<SlideTagEventArgs> SlidePos;
        event EventHandler<PosEventArgs> Pos;

        void OnFilterPos(InstrumentRequestSlide slide, int filter);
        void OnSlidePos(InstrumentRequestSlide slide);
        void OnPos(int pos);
    }
}
