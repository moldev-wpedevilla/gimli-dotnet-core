﻿using System;

namespace Gimli.InstrumentController
{
    public interface ICmdController
    {
        void StartCommand(string cmd, int noOfResp, int timeOut);
        string Command { get; set; }
        int NoOfResp { get; set; }
        int TimeOut { get; set; }
        event EventHandler<CmdRespEventArgs> CommandResponse;
    }

    public class GimliCmdController : ICmdController
    {
        public void StartCommand(string cmd, int noOfResp, int timeOut)
        {
            throw new NotImplementedException();
        }

        public string Command { get; set; }
        public int NoOfResp { get; set; }
        public int TimeOut { get; set; }
        public event EventHandler<CmdRespEventArgs> CommandResponse;

        public static ICmdController Generate()
        {
            return new GimliCmdController();
        }

        public static ICmdController Generate(string cmd)
        {
            if (cmd == null)
                throw new ArgumentException(nameof(cmd));
            return new GimliCmdController(cmd);
        }

        public static ICmdController Generate(string cmd, int noOfResp = 1, int timeOut = 30)
        {
            if (null == cmd)
                throw new ArgumentException(nameof(cmd));
            return new GimliCmdController(cmd, noOfResp, timeOut);
        }

        protected GimliCmdController()
        {
        }

        protected GimliCmdController(string cmd)
        {
            Command = cmd;
        }

        protected GimliCmdController(string cmd, int noOfResp, int timeOut)
        {
            Command = cmd;
            NoOfResp = noOfResp;
            TimeOut = timeOut;
        }

        public void OnCommandResponse(string response)
        {
            CommandResponse?.Invoke(this, new CmdRespEventArgs(response));
        }
    }

    public class CmdRespEventArgs : EventArgs
    {
        public CmdRespEventArgs(string response)
        {
            Response = response;
        }

        public string Response { get; private set; }
    }
}
