﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Apex.Device;
using Gimli.InstrumentController.GimliDevice;
using MolDev.Common.Logger;

namespace Gimli.InstrumentController
{
    public class DirectCmdMngr
    {
        private ILogService mLogger;
        private DateTime mStartTime;
        public ManualResetEventSlim CmdDoneEvent;
        private IAdvancedDirectCommunication mDirectComm;
        public CancellationToken cancellationToken { get; }

        public DirectCmdMngr(ILogService logger)
        {
            CmdDoneEvent = new ManualResetEventSlim();
            cancellationToken = new CancellationToken();
            mLogger = logger;
        }

        public void PrepareDirectCmd(Func<IAdvancedDirectCommunication> getDirectComm, EventHandler<ResponseReceivedArgs> handleResponseReceived)
        {
            mStartTime = DateTime.Now;
            mDirectComm = getDirectComm();
            mDirectComm.ResponseReceived += handleResponseReceived;
        }

        public void ExecuteDirectCmd(string cmd, int noOfResp, int timeoutInSecs)
        {
            mDirectComm.SendCommand(cmd, noOfResp, timeoutInSecs);
            Task.Run(() => CheckExeFwTimeout(timeoutInSecs, cancellationToken), cancellationToken);
            CmdDoneEvent.Wait(cancellationToken);
        }

        private void CheckExeFwTimeout(int timeoutInSecs, CancellationToken cancellationToken)
        {
            GimliDeviceHelper.CheckExeFwTimeout(mDirectComm, timeoutInSecs, cancellationToken, mStartTime, CmdDoneEvent, mLogger);
        }
    }
}
