﻿using Apex.Extensibility.Data;
using Apex.Extensibility.Interfaces;
using Gimli.JsonObjects;

namespace Gimli.InstrumentController.DataHandling
{
    internal class KinetictDataCharger : DataChargerBase
    {
        private readonly int mCycles;

        public static IDataCharger Generate(int cycles, Snapshot snapshot, FillOrder fillOrder) { return new KinetictDataCharger(cycles, snapshot, fillOrder); }
        protected KinetictDataCharger(int cycles, Snapshot snapshot, FillOrder fillOrder)
        {
            FillOrder = fillOrder;
            mCycles = cycles;
			if (snapshot.Settings.MeasurementMode == Mode.Fp)
			{
				InitDataPoints(snapshot, snapshot.Settings.Wavelengths.NrOfWavelengths.ToInt(), 2, mCycles);
			}
			else
			{
				InitDataPoints(snapshot, 1, snapshot.Settings.Wavelengths.NrOfWavelengths.ToInt(), mCycles);
			}
        }

        public override Point[] Fill(RawDataContainer source, Snapshot destination, RunMethodEventArgs args)
        {
            switch (FillOrder)
            {
                default:
                case FillOrder.RowWise:
                    return RowWiseFill(source, destination, args);
                case FillOrder.ColumnWise:
                    return ColumnWiseFill(source, destination, args);
                case FillOrder.WellWise:
                    return WellWiseFill(source, destination, args);
            }
        }

        private Point[] RowWiseFill(RawDataContainer source, Snapshot destination, RunMethodEventArgs args)
        {
            for (var y = args.StartColumn - 1; y < args.EndColumn; y++)
            {
                var x = args.StartRow - 1;
                AddValueForWell(source, destination, args.StartCycle - 1, x, y, args.XPoints);
            }
            return base.Fill(source, destination, args);
        }

        private Point[] ColumnWiseFill(RawDataContainer source, Snapshot destination, RunMethodEventArgs args)
        {
            for (int x = args.StartRow - 1; x < args.EndRow; x++)
            {
                var y = args.StartColumn - 1;
                AddValueForWell(source, destination, args.StartCycle - 1, x, y, args.XPoints);
            }
            return base.Fill(source, destination, args);
        }

        private Point[] WellWiseFill(RawDataContainer source, Snapshot destination, RunMethodEventArgs args)
        {
            for (var z = args.StartCycle - 1; z < args.EndCycle; z++)
            {
                var x = args.StartRow - 1;
                var y = args.StartColumn - 1;
                AddValueForWell(source, destination, z, x, y, args.XPoints);
            }
            return base.Fill(source, destination, args);
        }
    }
}
