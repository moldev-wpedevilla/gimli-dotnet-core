﻿using System;
using System.Diagnostics;
using Apex.Extensibility.Data;
using Apex.Extensibility.Interfaces;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Enumarations.Status;
using Gimli.JsonObjects.Labware;
using Gimli.JsonObjects.Settings;

namespace Gimli.InstrumentController.DataHandling
{
    internal class AreaScanDataCharger : DataChargerBase
    {
	    private readonly Microplate mPlate;
	    public static IDataCharger Generate(int scanRows, int scanCols, Snapshot snapshot, Microplate plate, FillOrder fillOrder) { return new AreaScanDataCharger(scanRows, scanCols, snapshot, plate, fillOrder); }
        private AreaScanDataCharger(int scanRows, int scanCols, Snapshot snapshot, Microplate plate, FillOrder fillOrder)
        {
	        FillOrder = fillOrder;
	        mPlate = plate;
	        InitDataPoints(snapshot, 1, scanRows, scanCols);
        }
        public override Point[] Fill(RawDataContainer source, Snapshot destination, RunMethodEventArgs args)
        {
            for (var j = FillOrder == FillOrder.ColumnWise ? args.StartRow - 1 : args.StartColumn - 1; j < (FillOrder == FillOrder.ColumnWise ? args.EndRow : args.EndColumn); j++)
            {
                var i = FillOrder == FillOrder.ColumnWise ? args.StartColumn - 1 : args.StartRow - 1;
                var wellPosRow = FillOrder == FillOrder.ColumnWise ? j : i;
                var wellPosCol = FillOrder == FillOrder.ColumnWise ? i : j;
	            if (!source.CycleDataArray[args.StartCycle - 1].WellArray[wellPosRow, wellPosCol].HasData) continue;

	            var well = destination.Wells.GetWell(wellPosRow, wellPosCol);
	            Trace.WriteLine("Adding well: " + well.WellName);
	            // InitDataPoints(destination, well, mScanRows, mScanCols);

                var dataWidth = FillOrder == FillOrder.ColumnWise ? DataRows : DataCols;
                var currentDataHeight = FillOrder == FillOrder.ColumnWise ? args.XPoints - 1 : args.YPoints - 1;

                for (var c = 0; c < dataWidth; c++)
                {
                    var dataRow = FillOrder == FillOrder.ColumnWise ? c : currentDataHeight;
                    var dataCol = FillOrder == FillOrder.ColumnWise ? currentDataHeight : c;
                    var val = source.CycleDataArray[args.StartCycle - 1].WellArray[wellPosRow, wellPosCol].get(dataRow, dataCol);
                    var stat = IsPointWithinArea(currentDataHeight, c, mPlate, destination) ? DataPointStatus.OK : DataPointStatus.Unused;
                    well.AddValue(0, dataRow, dataCol, val, stat);
	            }
            }

            return base.Fill(source, destination, args);
        }

		private static bool IsPointWithinArea(int row, int col, Microplate plate, Snapshot snapshot)
		{
			if (plate == null || plate.PlateSpecification.WellShape == "Square")
				return true;

			if (snapshot.Settings.WellScan.ScanMode.Value.ToString() == WellScanSettings.ScanModeHorizontal)
				return true;

			var density = snapshot.Settings.WellScan.X_Points.ToInt();
			var spacing = snapshot.Settings.WellScan.X_Spacing.ToDouble();
			var wellDiameter = plate.PlateSpecification.WellDiameter;
			var xyOffset = (density - 1d) / 2d;
			var minDia = spacing * (density);
			var wellDia = minDia < wellDiameter ? minDia : wellDiameter;
			var maxRadius = wellDia / 2;

			var t = Math.Pow(((col - xyOffset) * spacing), 2) + Math.Pow(((row - xyOffset) * spacing), 2);
			var s = Math.Pow(maxRadius, 2);
			return t <= s;
		}

	}
}
