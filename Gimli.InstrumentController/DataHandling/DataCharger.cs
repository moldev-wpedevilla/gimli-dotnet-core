﻿using System.Diagnostics;
using Apex.Extensibility.Data;
using Apex.Extensibility.Interfaces;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Enumarations.Status;

namespace Gimli.InstrumentController.DataHandling
{
    public interface IDataCharger
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="args"></param>
        /// <returns>Array will wells that were filled with this call</returns>
        Point[] Fill(RawDataContainer source, Snapshot destination, RunMethodEventArgs args);
        int DataRows { get; }
        int DataCols { get; }
        FillOrder FillOrder { get; }
    }

    public enum FillOrder
    {
        RowWise,
        ColumnWise,
        WellWise
    }

    internal abstract class DataChargerBase : IDataCharger
    {
        public virtual Point[] Fill(RawDataContainer source, Snapshot destination, RunMethodEventArgs args)
        {
            return FillOrder == FillOrder.ColumnWise ? GetColumnwiseMeasuredWells(args) : GetRowwiseMeasuredWells(args);
        }

        public int DataRows { get; private set; }
        public int DataCols { get; private set; }

        public FillOrder FillOrder { get; protected set; }

        protected void InitDataPoints(Snapshot snapshot)
        {
            InitDataPoints(snapshot, 1, 1, 1);
        }

        protected void InitDataPoints(Snapshot snapshot, int firstDim, int dataRows, int dataCols)
        {
            DataRows = dataRows;
            DataCols = dataCols;
            snapshot.InitializeWells(firstDim, dataRows, dataCols);
        }

        private Point[] GetRowwiseMeasuredWells(RunMethodEventArgs args)
        {
            var ret = new Point[args.EndColumn - args.StartColumn + 1];
            for (int col = args.StartColumn, cnt = 0; col <= args.EndColumn; col++, cnt++)
            {
                ret[cnt] = new Point() { X = col, Y = args.StartRow };
            }
            return ret;
        }

        private Point[] GetColumnwiseMeasuredWells(RunMethodEventArgs args)
        {
            var ret = new Point[args.EndRow - args.StartRow + 1];
            for (int row = args.StartRow, cnt = 0; row <= args.EndRow; row++, cnt++)
            {
                ret[cnt] = new Point() { X = args.StartColumn, Y = row };
            }
            return ret;
        }

        protected DataPointStatus GetDataStatus(WellStatus wellStatus)
        {
            switch (wellStatus)
            {
                default:
                case WellStatus.OK:
                    return DataPointStatus.OK;
                case WellStatus.Error:
                    return DataPointStatus.Error;
                case WellStatus.Overflow:
                    return DataPointStatus.Overflow;
                case WellStatus.Incorrect:
                    return DataPointStatus.Incorrect;
                case WellStatus.Rejected:
                    return DataPointStatus.Rejected;
                case WellStatus.Underflow:
                    return DataPointStatus.Underflow;
                case WellStatus.Unused:
                    return DataPointStatus.Unused;
            }
        }

        protected void AddValueForWell(RawDataContainer source, Snapshot destination, int cycle, int x, int y, int xPoints)
        {
            if (source.CycleDataArray[cycle].WellArray[x, y].HasData)
            {
                var well = destination.Wells.GetWell(x, y);
                Trace.WriteLine("Adding well: " + well.WellName);
                for (var pt = 0; pt < xPoints; pt++)
                {
                    var val = source.CycleDataArray[cycle].WellArray[x, y].get(0, pt);
                    var stat = GetDataStatus(source.CycleDataArray[cycle].WellArray[x, y].getStatus(0, pt));
                    if (val < 0)
                    {
                        stat = DataPointStatus.Error;
                        val = double.NaN;
                    }
                    well.AddValue(0, pt, cycle, val, stat, source.CycleDataArray[cycle].CycleTime);
                }
            }
        }
    }
}
