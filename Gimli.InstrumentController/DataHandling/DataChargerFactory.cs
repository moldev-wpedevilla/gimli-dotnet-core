﻿using Gimli.JsonObjects;
using Gimli.JsonObjects.Labware;
using Gimli.JsonObjects.Settings;

namespace Gimli.InstrumentController.DataHandling
{
    internal class DataChargerFactory
    {
        public static IDataCharger GenerateAppropriateCharger(Snapshot snapshot, Microplate plate)
        {
            var type = snapshot.Settings.MeasurementType;

            switch (type)
            {
                case MeasurementType.Kinetic:
                    return KinetictDataCharger.Generate(snapshot.Settings.Kinetic.Cycles.ToInt(), snapshot, GetFillOrder(snapshot.Settings.ReadArea));
                case MeasurementType.AreaScan:
                    return AreaScanDataCharger.Generate(snapshot.Settings.WellScan.Y_Points.ToInt(),
                        snapshot.Settings.WellScan.X_Points.ToInt(), snapshot, plate,
                        GetFillOrder(snapshot.Settings.ReadArea));
                case MeasurementType.SpectrumScan:
                    bool emSweep;
                    var nrOfPts = GetNumberOfSpectrumPoints(snapshot, out emSweep);
                    return SpectrumScanDataCharger.Generate(nrOfPts, emSweep, snapshot);
                default:
                    //case MeasurementType.Endpoint:
                    return EndpointDataCharger.Generate(snapshot, GetFillOrder(snapshot.Settings.ReadArea));
            }
        }

        private static FillOrder GetFillOrder(ReadAreaSettings readArea)
        {
            if(readArea.ReadOrder.Value.Equals("Column"))
                return FillOrder.ColumnWise;
            return readArea.ReadOrder.Value.Equals("Well") ? FillOrder.WellWise : FillOrder.RowWise;
        }

        private static int GetNumberOfSpectrumPoints(Snapshot snapshot, out bool emSweep)
        {
            var mode = snapshot.Settings.MeasurementMode;
            var wav = snapshot.Settings.Wavelengths;
            if (mode == Mode.Abs || (mode.IsModeInFluoroGroup() && snapshot.Settings.Wavelengths.ExcitationSweep.ToBool()))
            {
                emSweep = false;
                return (wav.ExcitationEnd.ToInt() - wav.ExcitationStart.ToInt()) / wav.ExcitationStep.ToInt() + 1;
            }
            //else if (mode == Mode.Lum )//|| (mode == Mode.Fl && snapshot.Settings.Wavelengths.ExcitationSweep.ToBool()))
            {
                emSweep = true;
                return (wav.EmissionEnd.ToInt() - wav.EmissionStart.ToInt()) / wav.EmissionStep.ToInt() + 1;
            }
        }
    }
}
