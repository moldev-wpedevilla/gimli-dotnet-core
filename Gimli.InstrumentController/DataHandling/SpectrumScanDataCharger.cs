﻿using System.Diagnostics;
using Apex.Extensibility.Data;
using Apex.Extensibility.Interfaces;
using Gimli.JsonObjects;

namespace Gimli.InstrumentController.DataHandling
{
    internal class SpectrumScanDataCharger : DataChargerBase
    {
        private readonly int mValues;
        private readonly bool mEmSweep;

        public static IDataCharger Generate(int values, bool emSweep, Snapshot snapshot) { return new SpectrumScanDataCharger(values, emSweep, snapshot); }
        private SpectrumScanDataCharger(int values, bool emSweep, Snapshot snapshot)
        {
            FillOrder = FillOrder.RowWise;
            mValues = values;
            mEmSweep = emSweep;
            InitDataPoints(snapshot, 1, 1, mValues);
        }
        public override Point[] Fill(RawDataContainer source, Snapshot destination, RunMethodEventArgs args)
        {
            var row = args.StartRow - 1;
            var col = args.StartColumn - 1;
            if (source.CycleDataArray[args.StartCycle - 1].WellArray[row, col].HasData)
            {
                var well = destination.Wells.GetWell(row, col);
                Trace.WriteLine("Adding well: " + well.WellName);
                // InitDataPoints(destination, well, 1, mValues);
                for (int i = 0; i < args.XPoints; i++)
                {
                    well.AddValue(0, 0, i, source.CycleDataArray[args.StartCycle - 1].WellArray[row, col].get(0, i),
                        GetDataStatus(source.CycleDataArray[args.StartCycle - 1].WellArray[row, col].getStatus(0, i)));
                }
                //excitation sweep - all data points of one well received at once
                if (!mEmSweep)
                    return base.Fill(source, destination, args); 
                //emission sweep - each data point received separate so we wait for last one to mark well as received
                if(args.EventNr%args.XPoints == 0 )
                    return base.Fill(source, destination, args); 
            }
            return new Point[] {};
        }
    }
}
