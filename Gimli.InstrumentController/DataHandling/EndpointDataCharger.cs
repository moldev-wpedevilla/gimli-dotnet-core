﻿using Gimli.JsonObjects;

namespace Gimli.InstrumentController.DataHandling
{
    internal class EndpointDataCharger : KinetictDataCharger
    {
        public static IDataCharger Generate(Snapshot snapshot, FillOrder fillOrder) { return new EndpointDataCharger(snapshot, fillOrder); }

        private EndpointDataCharger(Snapshot snapshot, FillOrder fillOrder) : base(1, snapshot, fillOrder)
        {
            FillOrder = fillOrder;
	        if (snapshot.Settings.MeasurementMode == Mode.Fp)
	        {
		        InitDataPoints(snapshot, snapshot.Settings.Wavelengths.NrOfWavelengths.ToInt(), 2, 1);
	        }
	        else
	        {
				InitDataPoints(snapshot, 1, snapshot.Settings.Wavelengths.NrOfWavelengths.ToInt(), 1);
			}
		}

        //public override Point[] Fill(RawDataContainer source, Snapshot destination, RunMethodEventArgs args)
        //{
        //    return FillOrder == FillOrder.RowWise
        //        ? RowWiseFill(source, destination, args)
        //        : ColumnWiseFill(source, destination, args);
        //}

        //private Point[] RowWiseFill(RawDataContainer source, Snapshot destination, RunMethodEventArgs args)
        //{
        //    for (int y = args.StartColumn - 1; y < args.EndColumn; y++)
        //    {
        //        var x = args.StartRow - 1;
        //        AddValueForWell(source, destination, args.StartCycle - 1, x, y, args.XPoints);
        //    }
        //    return base.Fill(source, destination, args);
        //}

        //private Point[] ColumnWiseFill(RawDataContainer source, Snapshot destination, RunMethodEventArgs args)
        //{
        //    for (int x = args.StartRow - 1; x < args.EndRow; x++)
        //    {
        //        var y = args.StartColumn - 1;
        //        AddValueForWell(source, destination, args.StartCycle - 1, x, y, args.XPoints);
        //    }
        //    return base.Fill(source, destination, args);
        //}
    }
}
