﻿using Gimli.JsonObjects.Settings;
using Gimli.InstrumentServer;
using Gimli.InstrumentServer.Validation;
using MD.CommonComponents.PluginFramework;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;
using IGimliDevice = Gimli.InstrumentServer.GimliDevice.IGimliDevice;

namespace Gimli.InstrumentController
{
    [Plugin(Category = "MD.InstrumentController")]
	public class InstrumentController : IInstrumentController
	{
	    private InstrumentController()
	    {
			
		}

		[PluginFactory(Metadata = "MD.InstrumentController")]
		public static IInstrumentController Generate()
		{
			if (Instance != null)
			{
				return Instance;
			}

			return Instance = new InstrumentController();
		}

	    public static IInstrumentController Instance { get; set; }


	    public IGimliDevice CreateGimliDevice(ILogService logger, IActorService actorService, IInstrumentContext instrumentContext, ISettingsValidatorFactory settingsValidatorFactory)
	    {
            return GimliDevice.GimliDevice.Generate(logger, actorService, instrumentContext, settingsValidatorFactory);
            
        }

	    public ISettingsToMethodTranslator CreateTranslator(PlateSettings plateSettings, MeasurementSettings settings)
	    {
		    return SettingsToMethodTranslator.Generate(plateSettings, settings);
	    }
	}
}
