﻿using System;
using Apex.Extensibility.Interfaces;
using Apex.Extensibility.Layouts;
using Apex.LabwareLib;
using Gimli.JsonObjects.Labware;

namespace Gimli.InstrumentController.Multimode
{
    internal class MicroplateConverter
    {
        private static readonly double MaximalPlateHeight = 22000d;

        private readonly object mLocker = new object();
        internal static ILabware GenerateLabwareFromMicroplate(Microplate plate)
        {
            if (plate == null) return null;
            var converter = new MicroplateConverter();
            return converter.LabwareFromMicroplate(plate);
        }

        private ILabware LabwareFromMicroplate(Microplate plate)
        {
            lock (mLocker)
            {
                var techColl = AddMeasTechCollection();

                //Create Labware. Defintion of microplate (eg.: Height, Width, Well Distance, Well Diameter...)
                ILabware labware = new Apex.LabwareLib.Labware(plate.PlateSpecification.NumberOfWells, techColl, this);

                try
                {
                    labware.Name = plate.MicroplateName;
                    labware.Wells_X = plate.PlateSpecification.NumberOfColumns;
                    labware.Wells_Y = plate.PlateSpecification.NumberOfRows;

                    if (plate.PlateSpecification.PlateHeight == 0)
                        plate.PlateSpecification.PlateHeight = 14500d;
                    labware.PlateHeight_Z = plate.PlateSpecification.PlateHeight/10000d;

                    //if (plate.Microplate.PlateSpecification.PlateHeightWithLid == 0)
                    //plate.PlateSpecification.PlateHeightWithLid =
                    //    plate.PlateSpecification.PlateHeight + 3000d;
                    //if (plate.PlateSpecification.PlateHeightWithLid > MaximalPlateHeight)
                    //    plate.PlateSpecification.PlateHeightWithLid = MaximalPlateHeight;
                    labware.PlateHeightWithLid_Z = plate.PlateSpecification.PlateHeightWithLid/10000d;

                    if (plate.PlateSpecification.ReadingHeight <= 0)
                        plate.PlateSpecification.ReadingHeight =
                            GetDefaultReadingHeightInHundredthOfMm(plate.PlateSpecification.NumberOfWells);
                    labware.DefaultReadingHeight_Z = plate.PlateSpecification.ReadingHeight/10000d;

                    //MISSING:
                    if (plate.PlateSpecification.PlateLength_X == 0)
                        plate.PlateSpecification.PlateLength_X = 127700d;
                    labware.PlateLength_X = plate.PlateSpecification.PlateLength_X/10000d;
                    if (plate.PlateSpecification.PlateLength_Y == 0)
                        plate.PlateSpecification.PlateLength_Y = 85700d;
                    labware.PlateLength_Y = plate.PlateSpecification.PlateLength_Y/10000d;

                    if (string.IsNullOrEmpty(plate.PlateSpecification.Label))
                        plate.PlateSpecification.Label = PlateLabel.Alpha.ToString();
                    labware.LabelType =
                        (PlateLabel) Enum.Parse(typeof (PlateLabel), plate.PlateSpecification.Label);

                    //create labware lot. labware can hold more than one lot.
                    ILabwareLot lot = new Apex.LabwareLib.LabwareLot(null);

                    //Position Well A1
                    lot.Well1_X =
                        lot.Well2_X =
                            lot.Well3_X =
                                lot.Well4_X =
                                    (plate.PlateSpecification.CenterOfFirstWellX)/10000d;
                    lot.Well1_Y =
                        lot.Well2_Y =
                            lot.Well3_Y =
                                lot.Well4_Y =
                                    (plate.PlateSpecification.CenterOfFirstWellY)/10000d;

                    //System.Diagnostics.Trace.WriteLine(string.Format("Plate Well A1: {0} - {1} - {2}", plate.Microplate.MicroplateName, lot.Well1_X.ToString("0.0000"), lot.Well1_Y.ToString("0.0000")));

                    //Diameter and Distances
                    lot.WellDistance_X = plate.PlateSpecification.DistanceBetweenWellsX/10000d;
                    lot.WellDistance_Y = plate.PlateSpecification.DistanceBetweenWellsY/10000d;

                    if (plate.PlateSpecification.WellWidth.Equals(0.0))
                        plate.PlateSpecification.WellWidth = plate.PlateSpecification.WellDiameter;
                    lot.WellLength_X = plate.PlateSpecification.WellWidth/10000d;

                    if (plate.PlateSpecification.WellLength.Equals(0.0))
                        plate.PlateSpecification.WellLength =
                            plate.PlateSpecification.WellDiameter;
                    lot.WellWidth_Y = plate.PlateSpecification.WellLength/10000d;

                    //Depth and Volume
                    if (plate.PlateSpecification.WellDepth > 0)
                        lot.WellDepth_Z = plate.PlateSpecification.WellDepth/10000d;
                    else
                        lot.WellDepth_Z = 0;
                    
                    if (lot.WellDepth_Z > labware.PlateHeight_Z)
                        lot.WellDepth_Z = labware.PlateHeight_Z - 0.01d;

                    labware.WellVolume = (int) plate.PlateSpecification.WellVolume;

                    if (string.IsNullOrEmpty(plate.PlateSpecification.WellBottomShape))
                        plate.PlateSpecification.WellBottomShape = eWellBottomShape.Flat.ToString();
                    labware.WellBottomShape =
                        (eWellBottomShape)
                            Enum.Parse(typeof (eWellBottomShape), plate.PlateSpecification.WellBottomShape);

                    labware.WellBottomThickness = (int) plate.PlateSpecification.WellBottomThickness;

                    if (string.IsNullOrEmpty(plate.PlateSpecification.WellShape))
                        plate.PlateSpecification.WellShape = eWellShape.Round.ToString();
                    labware.WellShape =
                        (eWellShape) Enum.Parse(typeof (eWellShape), plate.PlateSpecification.WellShape);

                    if (lot.WellLength_X >= lot.WellDistance_X)
                        lot.WellLength_X = lot.WellDistance_X - 0.001;
                    if (lot.WellWidth_Y >= lot.WellDistance_Y)
                        lot.WellWidth_Y = lot.WellDistance_Y - 0.001;

                    //Instrument Firmware accepts only WidthY == LengthX if WellShape is round
                    if (labware.WellShape == eWellShape.Round)
                    {
                        if (lot.WellLength_X < lot.WellWidth_Y)
                            lot.WellWidth_Y = lot.WellLength_X;
                        else
                            lot.WellLength_X = lot.WellWidth_Y;
                    }

                    lot.OptimizationTechnic = plate.PlateSpecification.OptimizationTechnique;
                    lot.Orientation = plate.PlateSpecification.PlateOrientation;
                    lot.DateOptimized = plate.PlateSpecification.DateOptimized;

                    lot.Name = plate.MicroplateName;

                    labware.addLabwareLot(lot, true);
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Trace.WriteLine("Converting Microplate -> Labware: " + exc.Message);
                }
                return labware;
            }
        }

        private static MeasTechCollection AddMeasTechCollection()
        {
            //Add all measurement techniques to labware
            var techColl = new Apex.LabwareLib.MeasTechCollection();

            foreach (MeasureTech tech in Enum.GetValues(typeof (MeasureTech)))
            {
                techColl.Add(tech);
            }
            return techColl;
        }

        private double GetDefaultReadingHeightInHundredthOfMm(int numberOfWells)
        {
            switch (numberOfWells)
            {
                default:
                case 96:
                    return 100;
                case 384:
                    return 300;
                case 1536:
                    return 500;
            }
        }

        public static Microplate UpdateMicroplateFromOptimizedLabware(ILabware optimizedLabware, Microplate plate)
        {
            if (plate == null) return null;
            if (optimizedLabware == null) return plate;
            
            var updater = new MicroplateConverter();
            return updater.UpdateMicroplate(optimizedLabware, plate);
        }
        private Microplate UpdateMicroplate(ILabware optimizedLabware, Microplate plate)
        {
            lock (mLocker)
            {
                //Position Well A1
                plate.PlateSpecification.CenterOfFirstWellX = optimizedLabware.CurrentLabwareLot.Well1_X * 10000d;
                plate.PlateSpecification.CenterOfFirstWellY = optimizedLabware.CurrentLabwareLot.Well1_Y * 10000d;

                //Diameter and Distances
                plate.PlateSpecification.DistanceBetweenWellsX = optimizedLabware.CurrentLabwareLot.WellDistance_X * 10000d;
                plate.PlateSpecification.DistanceBetweenWellsY = optimizedLabware.CurrentLabwareLot.WellDistance_Y * 10000d;

                plate.PlateSpecification.DateOptimized = DateTime.Now;
                plate.PlateSpecification.OptimizationTechnique = optimizedLabware.CurrentLabwareLot.OptimizationTechnic;
                plate.PlateSpecification.PlateOrientation = optimizedLabware.CurrentLabwareLot.Orientation;

                plate.MicroplateName = optimizedLabware.Name;

                return plate;
            }
        }
    }
}
