﻿using System.Collections.Generic;
using Apex.Extensibility.Interfaces;
using Apex.Extensibility.Layouts;
using Gimli.JsonObjects.Parameter;
using Gimli.JsonObjects.Settings;

namespace Gimli.InstrumentController.Multimode
{
    internal class ReadAreaConverter
    {
        public static ILayout GenerateLayoutFromReadArea(ReadAreaSettings readArea, List<IParameter> parameters )
        {
            if (readArea == null) return null;

            var converter = new ReadAreaConverter();
            return converter.CreateLayout(readArea, parameters);
        }

        private ILayout CreateLayout(ReadAreaSettings readArea, List<IParameter> parameters)
        {
            var numberColumns = readArea.Wells.GetLength(1);
            var numberRows = readArea.Wells.GetLength(0);

            ILayout layout = new Layout(numberRows, numberColumns, 0, 0, "Test");
            layout.Wells = new WellIdentifierMap(numberRows, numberColumns);

            int id = 1;
            for (var c = 0; c < numberColumns; c++)
            {
                for (var r = 0; r < numberRows; r++)
                {
                    if (!readArea.Wells[r, c].Selected) continue;
                    layout.Wells.Add(r + 1, c + 1, "S", id++);
                }
            }
            layout.SetMinMax();
            //layout.Orientation = (string)GetParameter("Orientation", parameters).Value == "Landscape" ? PlateOrientation.Landscape : PlateOrientation.OppositeLandscape;
            layout.Lidded = (bool)GetParameter("IsLidded", parameters).Value;
            var readOrder = readArea.ReadOrder.Value.ToString().Contains("Row") ? ProcModes.Row : ProcModes.Column;
            layout.MeasurementDirection = readArea.ReadOrder.Value.ToString().Contains("Well") ? ProcModes.Well : readOrder; 

            return layout;
        }

        private IParameter GetParameter(string name, List<IParameter> parameters)
        {
            foreach (var parameter in parameters)
            {
                if (parameter.Name == name)
                    return parameter;
            }
            return null;
        }
    }
}
