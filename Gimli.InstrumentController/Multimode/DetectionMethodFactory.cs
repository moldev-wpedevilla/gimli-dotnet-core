﻿using Apex.Device;
using Apex.Device.Cartridges;
using Apex.Extensibility.Interfaces;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Settings;

namespace Gimli.InstrumentController.Multimode
{
    public interface IDetectionMethodFactory
    {
        IDetectMethod Generate(MeasurementSettings sett, FluoroGimli reader);
    }
    
    public class DetectionMethodFactory : IDetectionMethodFactory
    {
        private static IDetectionMethodFactory _instance;
        public static IDetectionMethodFactory Instance
        {
            get
            {
                if(_instance == null)
                    _instance = new DetectionMethodFactory();
                return _instance;
            }
        }

        internal static void SetInstance(IDetectionMethodFactory instance)
        {
            _instance = instance;
        }

        private DetectionMethodFactory() { }

        public IDetectMethod Generate(MeasurementSettings sett, FluoroGimli reader)
        {
            var measureTech = ConvertMeasureTechnique(sett.MeasurementMode, sett.DetectionSetting);
            var methodProvider = GetMethodProvider(measureTech, reader);
            return methodProvider.GetMethod(string.Empty, reader, "Selectable Wavelength");
        }

        private static MeasureTech ConvertMeasureTechnique(Mode mode, DetectionSettings det)
        {
            switch (mode)
            {
                case Mode.Abs:
                    return MeasureTech.Absorbance;
                case Mode.Lum:
                    return MeasureTech.Luminescence;
                case Mode.Trf:
                    return MeasureTech.TimeResolvedFluorescence;
                case Mode.Fp:
                    return MeasureTech.FluorescencePolarization;
                case Mode.Fl:
					return det.ReadFromBottom.ToBool()
                        ? MeasureTech.FluorescenceIntensityBottom
                        : MeasureTech.FluorescenceIntensityTop;
                default:
                    return MeasureTech.FluorescenceIntensityTop;
            }
        }

        protected static MethodProvider GetMethodProvider(MeasureTech measTech, FluoroGimli reader)
        {
            var mps = reader.DetectionMethodProviders;
            var cart = ((LmbConfig)reader.Config).TopMono;
            cart.SetPosition(measTech == MeasureTech.FluorescenceIntensityBottom ? 7 : 1);
            //use only methods from selected cartridge
            foreach (var mp in mps)
            {
                if (mp.IsUsing(cart) && mp.Technique == measTech)
                    return mp;
            }
            return null;
        }
    }
}
