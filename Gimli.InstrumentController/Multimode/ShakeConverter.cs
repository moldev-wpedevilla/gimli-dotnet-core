﻿using Apex.Extensibility.Interfaces;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Parameter;
using System;
using ShakeIntensity = Gimli.JsonObjects.ShakeIntensity;

namespace Gimli.InstrumentController.Multimode
{
    internal class ShakeConverter
    {
        internal static Apex.Extensibility.Interfaces.ShakeIntensity GetShakeIntensity(ShakeIntensity intensity)
        {
            switch (intensity)
            {
                default:
                case ShakeIntensity.Low:
                    return Apex.Extensibility.Interfaces.ShakeIntensity.Low;
                case ShakeIntensity.Medium:
                    return Apex.Extensibility.Interfaces.ShakeIntensity.Medium;
                case ShakeIntensity.High:
                    return Apex.Extensibility.Interfaces.ShakeIntensity.High;
            }
        }

        internal static Apex.Extensibility.Interfaces.ShakeIntensity GetShakeIntensity(GimliParameterString intensity)
        {
            return GetShakeIntensity( (ShakeIntensity) Enum.Parse(typeof(ShakeIntensity), intensity.Value.ToString()));
        }

        internal static ShakeTypes GetShakeType(ShakeMode mode)
        {
            switch (mode)
            {
                default:
                case ShakeMode.Linear:
                    return ShakeTypes.Linear;
                case ShakeMode.Orbital:
                    return ShakeTypes.Orbital;
                case ShakeMode.DoubleOrbital:
                    return ShakeTypes.DoubleOrbital;
            }
        }

        internal static ShakeTypes GetShakeType(GimliParameterString mode)
        {
            if (mode?.Value == null || string.IsNullOrEmpty(mode.Value.ToString())) return ShakeTypes.Orbital;
            return GetShakeType(mode.Value.ToString().ConvertFromString());
        }
    }
}
