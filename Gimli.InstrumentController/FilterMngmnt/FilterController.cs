﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Apex.Device;
using Gimli.InstrumentController.GimliDevice;
using Gimli.InstrumentServer;
using Gimli.InstrumentServer.GimliDevice;
using Gimli.JsonObjects;
using Gimli.JsonObjects.FiltersMngmnt;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MD.Nfc.Basic.StrTagConverting;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;

namespace Gimli.InstrumentController.FilterMngmnt
{
    public class FilterController : IFilterController
    {
        private const double CmdTimeoutFactor = 2.0;
        private const int PlateTransportLagInSecs = 5;
        private const int VolumeRoundUpInSecs = 1;

        private readonly Func<IAdvancedDirectCommunication> mGetDirectComm;
        private readonly ILogService mLogger;
        private IAdvancedDirectCommunication mDirectComm;
        public ManualResetEventSlim FiltersMngmntCmdDoneEvent;
        private DateTime mStartTime;
        private readonly IActorService mActorService;
	    private readonly IGimliDeviceHelper mHelper;

        private bool mResult;

	    public static IFilterController Generate(ILogService logger, Func<IAdvancedDirectCommunication> getDirectComm, IActorService actorService, IGimliDeviceHelper helper)
        {
            return new FilterController(logger, getDirectComm, actorService, helper);
        }

        protected FilterController(ILogService logger, Func<IAdvancedDirectCommunication> getDirectComm, IActorService actorService, IGimliDeviceHelper helper)
        {
            mLogger = logger;
            mGetDirectComm = getDirectComm;
            mActorService = actorService;
	        mHelper = helper;
        }

        private string ComposeCmd(SlideSel slide, string cmd, bool isSpace)
        {
            var command = string.Empty;
            if (SlideSel.SlideA == slide || SlideSel.SlideB == slide)
            {
                command = $"{cmd}{(isSpace ? " " : "")}{FiltersMngrAction.MapFilterSlotId((int)slide)}";
            }
            return command;
        }

        private string ComposeLoadSlideCommand(SlideSel slide)
        {
            return ComposeCmd(slide, "L", true);
        }

        private string ComposeEjectSlideCommand(SlideSel slide)
        {
            return ComposeCmd(slide, "E", true);
        }

        private string ComposeToggleSlideCommand(SlideSel slide)
        {
            return ComposeCmd(slide, "T", true);
        }

        private string ComposeIsSliderLoadedCommand(SlideSel slide)
        {
            return ComposeCmd(slide, "E", false);
        }


        private void PrepareDirectCmd()
        {
            mStartTime = DateTime.Now;
            FiltersMngmntCmdDoneEvent = new ManualResetEventSlim();
            mDirectComm = mGetDirectComm();
            mDirectComm.ResponseReceived += DirectCommunicationResponseReceivedHandler;
        }

        private bool SendFiltersCmdWithFiltersParam(string funName, string cmd, int timeoutInSecs, CancellationToken cancellationToken)
        {
            try
            {
                mLogger.Info(LogCategoryEnum.Host, $"Starting {funName}().");
                PrepareDirectCmd();

                //Trace.WriteLine($"FiltersCmdWithCmdParam() Before SendCommand: FilterSlidesProcessingFinished.IsSet: {mHelper.FilterSlidersProcessingHasBeenFinished.IsSet} FilterSlideInserted.IsSet: {mHelper.FilterSliderHasBeenInserted.IsSet}");

                mDirectComm.SendCommand(cmd, 1, timeoutInSecs);

                //Trace.WriteLine($"FiltersCmdWithCmdParam() After SendCommand: FilterSlidesProcessingFinished.IsSet: {mHelper.FilterSlidersProcessingHasBeenFinished.IsSet} FilterSlideInserted.IsSet: {mHelper.FilterSliderHasBeenInserted.IsSet}");

                Task.Run(() => CheckExeFwTimeout(timeoutInSecs, cancellationToken), cancellationToken);
                FiltersMngmntCmdDoneEvent.Wait(cancellationToken);

                //Trace.WriteLine($"FiltersCmdWithCmdParam() End of try: FilterSlidesProcessingFinished.IsSet: {mHelper.FilterSlidersProcessingHasBeenFinished.IsSet} FilterSlideInserted.IsSet: {mHelper.FilterSliderHasBeenInserted.IsSet}");
                return mResult;
            }
            catch (Exception exc)
            {
                mLogger.Error(LogCategoryEnum.Host, $"Failed due to {exc.Message} - {exc.InnerException?.Message}");
                throw;
            }
            finally
            {
                mLogger.Info(LogCategoryEnum.Host, $"{funName}() finished.");
            }
        }

        private bool ComposeAndSendFiltersCmdWithFiltersParam(string funcName, Func<SlideSel, string> composeCommand, SlideSel slide, int timeoutInSecs, CancellationToken cancellationToken)
        {
            var cmd = composeCommand(slide);
            return SendFiltersCmdWithFiltersParam(funcName, cmd, timeoutInSecs, cancellationToken);
        }

        private int GetSlideMngmntCmdTimoutInSecs(int volumeInUl)
        {
            return (int)(((volumeInUl * CmdTimeoutFactor) / 100) + VolumeRoundUpInSecs + PlateTransportLagInSecs);
        }

        private int GetEjectSlideCmdTimoutInSecs(SlideSel slide)
        {
            return GetSlideMngmntCmdTimoutInSecs(10);
        }

        private int GetLoadSlideCmdTimoutInSecs(SlideSel slide)
        {
            return GetSlideMngmntCmdTimoutInSecs(10);
        }

        private int GetToggleSlideCmdTimoutInSecs(SlideSel slide)
        {
            return GetSlideMngmntCmdTimoutInSecs(10);
        }

        #region interface
        public void EjectSlider(SlideSel slide, CancellationToken cancellationToken)
        {
            ComposeAndSendFiltersCmdWithFiltersParam("EjectSlider", ComposeEjectSlideCommand, slide, GetEjectSlideCmdTimoutInSecs(slide), cancellationToken);
        }

        public void LoadSlider(SlideSel slide, CancellationToken cancellationToken)
        {
            ComposeAndSendFiltersCmdWithFiltersParam("LoadSlider", ComposeLoadSlideCommand, slide, GetLoadSlideCmdTimoutInSecs(slide), cancellationToken);
        }

        public void ToggleSlider(SlideSel slide, CancellationToken cancellationToken)
        {
            ComposeAndSendFiltersCmdWithFiltersParam("ToggleSlider", ComposeToggleSlideCommand, slide, GetToggleSlideCmdTimoutInSecs(slide), cancellationToken);
        }

        public bool IsSliderLoaded(SlideSel slide, CancellationToken cancellationToken)
        {
            return ComposeAndSendFiltersCmdWithFiltersParam("IsSliderLoaded", ComposeIsSliderLoadedCommand, slide, GetToggleSlideCmdTimoutInSecs(slide), cancellationToken);
        }

        #endregion
        // --------------------------------------------------------------------

        private void CheckExeFwTimeout(int timeoutInSecs, CancellationToken cancellationToken)
        {
            GimliDeviceHelper.CheckExeFwTimeout(mDirectComm, timeoutInSecs, cancellationToken, mStartTime, FiltersMngmntCmdDoneEvent, mLogger);
        }

        private void DirectCommunicationResponseReceivedHandler(object sender, ResponseReceivedArgs args)
        {
            try
            {
                mLogger.Debug(LogCategoryEnum.Host, "Handling filter event.");
                if (args.ResponseMessage.StartsWith("-"))
                {
                    if (args.ResponseMessage.Contains("Communication Error: Time out"))
                    {
                        // TODO BRANE: check if ActOnSlideLoading necessary
                        mLogger.Debug(LogCategoryEnum.Uncategorized, $"Handling filter timeout event {args.ResponseMessage}", "");
                    }
                    else 
                    {
                        Trace.WriteLine($"FilterController::ResponseEvent(): Entering else of -- if (args.ResponseMessage.Contains(\"Communication Error: Time out \")) args.ResponseMessage {args.ResponseMessage}");
                        HandleNonTimeoutDirectResponse(sender);
                    }
                    FiltersMngmntCmdDoneEvent.Set();
                }
                else if (args.ResponseMessage.StartsWith("+"))
                {
                    // TODO BRANE: check if ActOnSlideLoading necessary

                    if (args.ResponseMessage.Contains("+ 0"))
                    {
                        mResult = true;
                    }
                    else 
                    {
                        mResult = false;
                    }
                    
                    mLogger.Debug(LogCategoryEnum.Host, "Handling filter success event.");
                    FiltersMngmntCmdDoneEvent.Set();
                }
            }
            catch (Exception exc)
            {
                HandleExceptionInResponseHandling(args, exc);
            }
        }

        private void HandleExceptionInResponseHandling(ResponseReceivedArgs args, Exception except)
        {
            mLogger.Error(LogCategoryEnum.Host, $"HandleExceptionInResponseHandling() except: {except}");
            // Temporary solution for developer enabling for the device to stay responsive avoiding the need to be restarted.
            try
            {
                mLogger.Error(LogCategoryEnum.Host, "=== SLIDER LOADING... HandleExceptionInResponseHandling(): There was an exception in response (FiltersMngmntCmdDoneEvent, FilterSlidersProcessingHasBeenFinished, FilterSliderHasBeenInserted, FilterSliderLoadingIsInProgress are set)");
                FiltersMngmntCmdDoneEvent.Set();
                mHelper.FilterSlidersProcessingHasBeenFinished.Set();

                // TODO Brane How to detect which Slider !?
                mHelper.FilterSliderLoadingIsInProgress.Reset();     //mHelper.FilterSliderALoadingIsInProgress.Set();

                //mLogger.Debug(LogCategoryEnum.Uncategorized, $"HandleExceptionInResponseHandling(): FiltersMngmntCmdDoneEvent, FilterSlidesProcessingFinished and FilterSlideInserted were all set.", "");
                Trace.WriteLine($"HandleExceptionInResponseHandling(): FiltersMngmntCmdDoneEvent, FilterSlidesProcessingFinished and FilterSlideInserted were all set.");
                // here an error is expected if no filters are inserted -> do not show error to the user while filter slide loading on init
                mActorService.Get<WebServiceProxyActor>()
                    .Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
                    {
                        Parameters = new MessageParameters
                        {
                            Message = args.ResponseMessage,
                            DisplayType = MessageDisplayType.DialogOk,
                            MessageCode = MessageStatusCodes.FiltersMngmntError,
                            MessageType = MessageType.Error
                        }
                    }));
            }
            catch (Exception e)
            {
                mLogger.Error(LogCategoryEnum.Host, "Exception while handling filter slide response error handling: " + e);
            }
        }

        private void HandleNonTimeoutDirectResponse(object sender)
        {
            var slideSel =
                ((IAdvancedDirectCommunication) sender).GetCommandConnector().CurrentCommand.Commands[0]
                    .Contains(" A")
                    ? "A"
                    : "B";
            var filter = new[]
            {
                new FilterTagData(), new FilterTagData(), new FilterTagData(), new FilterTagData(),
                new FilterTagData(), new FilterTagData()
            };
            var slide = new SliderTagData();
            slide.IsValid = false;
            mHelper.ActOnSliderLoaded(slideSel, null, filter);
        }
    }
}
