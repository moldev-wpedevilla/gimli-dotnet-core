﻿using System.Collections;
using System.Collections.Generic;
using Gimli.InstrumentServer.GimliDevice;
using Gimli.InstrumentServer.Validation;
using Gimli.JsonObjects;
using Gimli.JsonObjects.FiltersMngmnt;
using Gimli.JsonObjects.Settings;

namespace Gimli.InstrumentController
{
    internal class GimliSettingsDefaults : ISettingsDefaults
    {
        private const int MinAbs = 230;
        private const int MaxAbs = 1000;
        private const int MinLum = 300;
        private const int MaxLum = 850;
        private const int MinFlEx = 250;
        private const int MaxFlEx = 830;
        private const int MinFlEm = 270;
        private const int MaxFlEm = 850;
	    private readonly IGimliDeviceHelper mHelper;

		public static ISettingsDefaults Generate(IGimliDeviceHelper helper)
        {
            return new GimliSettingsDefaults(helper);
        }

        private GimliSettingsDefaults(IGimliDeviceHelper helper)
        {
	        mHelper = helper;
        }

        public RangeAndDefaults GetNonSpectrumRangeAndDefaults(Mode readMode, bool isFlEx)
        {
            switch (readMode)
            {
                case Mode.Abs:
                    return new RangeAndDefaults(MinAbs, MaxAbs, 405);
                case Mode.Lum:
                    return new RangeAndDefaults(MinLum, MaxLum, 578);
                default:
                case Mode.Fl:
                case Mode.Fp:
                case Mode.Trf:
                    return isFlEx
                        ? new RangeAndDefaults(MinFlEx, MaxFlEx, 485)
                        : new RangeAndDefaults(MinFlEm, MaxFlEm, 535);
            }
        }

        public SpectrumRangeAndDefaults GetSpectrumRangeAndDefaults(Mode readMode, bool exFix = true)
        {
            switch (readMode)
            {
                case Mode.Abs:
                    return new SpectrumRangeAndDefaults(new RangeAndDefaults(MinAbs, MaxAbs, MinAbs),
                        new RangeAndDefaults(MinAbs, MaxAbs, MaxAbs), new RangeAndDefaults(1, 100, 10),
                        new RangeAndDefaults(0, 0, 0));
                case Mode.Lum:
                    return new SpectrumRangeAndDefaults(new RangeAndDefaults(MinLum, MaxLum, 400),
                        new RangeAndDefaults(MinLum, MaxLum, 750), new RangeAndDefaults(1, 100, 10),
                        new RangeAndDefaults(0, 0, 0));
                default:
				case Mode.Fl:
				case Mode.Fp:
                case Mode.Trf:
                    return exFix
                        ? new SpectrumRangeAndDefaults(new RangeAndDefaults(MinFlEm, MaxFlEm, 400),
                            new RangeAndDefaults(MinFlEm, MaxFlEm, 750), new RangeAndDefaults(1, 100, 10),
                            new RangeAndDefaults(MinFlEx, MaxFlEx, 360))
                        : new SpectrumRangeAndDefaults(new RangeAndDefaults(MinFlEx, MaxFlEx, 360),
                            new RangeAndDefaults(MinFlEx, MaxFlEx, 750), new RangeAndDefaults(1, 100, 10),
                            new RangeAndDefaults(770, MaxFlEm, 800));
            }
        }

        public int ExEmMinimumSpread => 20;

        public IList<ReadModeTypeCombination> GetAllReadModeTypeCombinations()
        {
            // TODO: has to come from MDL
            var basic = new List<ReadModeTypeCombination>
            {
                new ReadModeTypeCombination
                {
                    Name = Mode.Abs,
                    Types = new List<MeasurementType>
                    {
                        MeasurementType.Endpoint,
                        MeasurementType.Kinetic,
                        MeasurementType.AreaScan,
                        MeasurementType.SpectrumScan,
					}
                },
                new ReadModeTypeCombination
                {
                    Name = Mode.Fl,
                    Types = new List<MeasurementType>
                    {
                        MeasurementType.Endpoint,
                        MeasurementType.Kinetic,
                        MeasurementType.AreaScan,
                        MeasurementType.SpectrumScan,
					}
                },
                new ReadModeTypeCombination
                {
                    Name = Mode.Lum,
                    Types = new List<MeasurementType>
                    {
                        MeasurementType.Endpoint,
                        MeasurementType.Kinetic,
                        MeasurementType.AreaScan,
                        MeasurementType.SpectrumScan,
					}
                }
			};

            if(mHelper.IsFilterReady)
                basic.AddRange(GetAdvancedCombinations());

            return basic;
        }

        private IEnumerable<ReadModeTypeCombination> GetAdvancedCombinations()
        {
            return new List<ReadModeTypeCombination>
            {
                new ReadModeTypeCombination
                {
                    Name = Mode.Fp,
                    Types = new List<MeasurementType>
                    {
                        MeasurementType.Endpoint,
                        MeasurementType.Kinetic,
                    }
                },
                new ReadModeTypeCombination
                {
                    Name = Mode.Trf,
                    Types = new List<MeasurementType>
                    {
                        MeasurementType.Endpoint,
                        MeasurementType.Kinetic,
                        MeasurementType.AreaScan,
                        MeasurementType.SpectrumScan,
                    }
                }
            };
        }

        public IList<int> SupportedPlateFormats => new List<int> {6, 12, 24, 48, 96, 384};

        public RangeAndDefaults GetIntegrationTime(int wellNumber, Mode readMode)
        {
	        RangeAndDefaults timeRange;

			switch (readMode)
	        {
		        case Mode.Fp:
		        case Mode.Fl:
					timeRange = new RangeAndDefaults(10, 10000, wellNumber > 96 ? 100 : 400);
					break;
		        case Mode.Lum:
					timeRange = new RangeAndDefaults(10, 10000, wellNumber > 96 ? 400 : 1000);
					break;
				case Mode.Trf:
					// these are microseconds
					timeRange = new RangeAndDefaults(200, 10000, 1890);
					break;
				default:
					timeRange = wellNumber > 96 ? new RangeAndDefaults(10, 10000, 400) : new RangeAndDefaults(10, 10000, 1000);
					break;
	        }

	        return timeRange;
        }

	    public RangeAndDefaults GetReadingHeight(int wellNumber)
	    {
		    return wellNumber > 96 ? new RangeAndDefaults(50, 1039, 300) : new RangeAndDefaults(50, 1039, 100);
	    }

	    public RangeAndDefaults GetNumberOfWavelengthsRangeAndDefaults(Mode readMode)
	    {
		    switch (readMode)
		    {
			    case Mode.Abs:
				    return new RangeAndDefaults(1, 6, 1);
			    case Mode.Fp:
			    case Mode.Trf:
				    return new RangeAndDefaults(1, 1, 1);
			    default:
				    return new RangeAndDefaults(1, 4, 1);
		    }
	    }

	    public ArrayList GetAttenuationValues()
	    {
		    var filters = new List<double> {0, 1};
		    if (mHelper.IsFilterReady)
		    {
			    filters.Add(2);
			    filters.Add(3);
		    }

		    return new ArrayList(filters);
	    }

	    public FilterModuleConfig GetCurrentFilters()
	    {
		    return mHelper.GetFilterConfiguration();
	    }

	    public bool IsLumBottomReady()
	    {
		    return mHelper.IsLumBottomReady;
	    }

	    public string GetReadOrderDefaults(MeasurementType type)
	    {
		    return type == MeasurementType.SpectrumScan ? ReadAreaSettings.ReadOrderWell : ReadAreaSettings.ReadOrderRow;
	    }
    }
}
