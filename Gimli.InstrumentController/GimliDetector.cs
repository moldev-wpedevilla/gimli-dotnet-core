﻿using System;
using System.Collections.Generic;

namespace Gimli.InstrumentController
{
    public interface IInstrumentDetector
    {
        bool IsConnected(out List<string> port);
        object GetReaderObject(String communication, int version);
        string GetModelName();
    }

    //not used at the moment
    //[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    //public static class GimliDetectorFactory
    //{
    //    static GimliDetectorFactory()
    //    {
    //        _defaultCommServer = GimliDetector.DefaultCommServerConnection;
    //    }

    //    private static ICommServer _defaultCommServer;
    //    public static ICommServer DefaultCommServer
    //    {
    //        get { return _defaultCommServer; }
    //        set { _defaultCommServer = value; }
    //    }

    //    public static IInstrumentDetector GenerateDefaultInstrumentDetector()
    //    {
    //        return GimliDetector.GenerateInstrumentDetector(DefaultCommServer);
    //    }
    //}
    //    public class GimliDetector : IInstrumentDetector
    //{
    //    private FluoroLmb mReader;
    //    private ICommServer mCommServer;
    //    private CommClientSink mSink;

    //    public static IInstrumentDetector GenerateInstrumentDetector(ICommServer commServer)
    //    {
    //        if (commServer == null)
    //            throw new ArgumentNullException();
    //        return new GimliDetector(commServer);
    //    }

    //    protected GimliDetector(ICommServer commServer)
    //    {
    //        mCommServer = commServer;
    //    }

    //    public static ICommServer DefaultCommServerConnection
    //    {
    //        get
    //        {
    //            bool local;
    //            return ServerConfig.ConfigureAndConnectToServer(GimlliReaderModel.SkinPath + @"templates\Client.exe.config", out local);
    //        }
    //    }

    //    protected void ServerJoin()
    //    {
    //        mSink = new CommClientSink(false);
    //        mCommServer.Join(mSink.AttendeeID, mSink);
    //    }

    //    private static DateTime LastRefreshTime;
    //    private const int RefreshTimeMinumInterval = 20;

    //    protected void ServerRefresh()
    //    {
    //        if ((DateTime.Now - LastRefreshTime).TotalSeconds > RefreshTimeMinumInterval)
    //        {
    //            Trace.WriteLine("###############CommServer refresh");
    //            mCommServer.Refresh();
    //            LastRefreshTime = DateTime.Now;
    //        }
    //    }

    //    protected void ServerExit()
    //    {
    //        mCommServer.Exit(mSink.AttendeeID);
    //    }

    //    public bool IsConnected(out List<string> port)
    //    {
    //        GetPortsForConnectedReaders(out port, false);

    //        return port.Count > 0;
    //    }

    //    protected List<InstrumentVersion> GetPortsForConnectedReaders(out List<string> port, bool checkInstrumentVersion)
    //    {
    //        port = new List<string>();
    //        var versions = new List<InstrumentVersion>();
    //        try
    //        {
    //            ServerJoin();
    //            ServerRefresh();
    //            foreach (var device in mCommServer.Devices)
    //            {
    //                var name = device.Type.ToLower();
    //                Trace.WriteLine(string.Format("Instrument {0} on {2} with Serial#: {3} is {1}", name,
    //                                                                 device.State.ToString(), device.Port, device.SerialNr));
    //                if (name.Contains(InstrumentRecognitionString))
    //                {
    //                    if (device.State == DeviceStates.Online)
    //                    {
    //                        if (checkInstrumentVersion)
    //                        {
    //                            versions.Add(GetInstrumentVersionFromDevice(device));
    //                        }
    //                        port.Add(device.Port);
    //                    }
    //                }
    //            }
    //            ServerExit();
    //        }
    //        catch (Exception e)
    //        {
    //            Trace.WriteLine("ParadigmDetector::IsConnected: " + e.Message);
    //        }
    //        return versions;
    //    }

    //    private InstrumentVersion GetInstrumentVersionFromDevice(ICommDevice device)
    //    {
    //        Fluoro lmb = CreateOnlineReader();
    //        lmb.CommDevice = device;
    //        lmb.Connect(true);
    //        var ret = lmb.FluoroConfig.InstrumentIdentity.Version;
    //        lmb.Disconnect();
    //        return ret;
    //    }

    //    public object GetReaderObject(String communication, int version)
    //    {
    //        if (string.IsNullOrEmpty(communication) || communication.ToLower().Contains(Properties.Resources.Offline))
    //            return CreateOfflineReader(version);

    //        //checks if desired version on port is still online
    //        List<string> detectedPorts;
    //        var bConnected = false;
    //        IsConnected(out detectedPorts);
    //        foreach (string port in detectedPorts)
    //        {
    //            if (port == communication)
    //                bConnected = true;
    //        }
    //        if (!bConnected)
    //            return CreateOfflineReader(version);

    //        try
    //        {
    //            ServerJoin();
    //            foreach (var device in mCommServer.Devices)
    //            {
    //                var name = device.Type.ToLower();
    //                if (name.Contains(InstrumentRecognitionString) && device.State == DeviceStates.Online && device.Port == communication)
    //                {
    //                    mReader = CreateOnlineReader();
    //                    mReader.CommDevice = device;
    //                    break;
    //                }
    //            }
    //            ServerExit();
    //        }
    //        catch (Exception e)
    //        {
    //            Trace.WriteLine(e);
    //        }

    //        return mReader;
    //    }

    //    public FluoroLmb CreateOnlineReader()
    //    {
    //        return new FluoroLmb(InstrumentCreator.GetSkinWithName(GetModelName()), null);
    //    }

    //    public FluoroLmb CreateOfflineReader(int version)
    //    {
    //        return FluoroLmb.CreateReaderObjectWithAllCartridges(InstrumentCreator.GetSkinWithName(GetModelName()));
    //    }

    //    public string GetModelName()
    //    {
    //        return "SpectraMax i3x";
    //    }

    //    public static bool CanAccessPort(string portName)
    //    {
    //        try
    //        {
    //            var port = new System.IO.Ports.SerialPort(portName);
    //            port.Open();
    //            port.Close();
    //            return true;
    //        }
    //        catch (Exception e)
    //        {
    //            Trace.WriteLine("Can not access the port: " + portName + " due to exception " + e.Message);
    //            return false;
    //        }
    //    }

    //    protected InstrumentCreator InstrumentCreator
    //    {
    //        get { return new InstrumentCreator(GimlliReaderModel.SkinPath); }
    //    }

    //    private static string GetMultimodePath()
    //    {
    //        return DirectoryLookup.GetAllUsersBaseDir;
    //    }

    //    protected string MRecognitionString = "i3x";
    //    protected virtual string InstrumentRecognitionString
    //    {
    //        get { return MRecognitionString; }
    //    }

    //    protected ICommServer CommServer { get { return mCommServer; } }
    //}
}

