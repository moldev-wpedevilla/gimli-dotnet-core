﻿using System;

namespace Gimli.InstrumentController
{
    public enum ReaderStatus
    {
        Idle = 0,
        Initializing,
        Busy,
        Stopping,
        Error,
        Timeout,
        Offline
    }

    //not used at the moment
    //[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    //public class GimlliReaderModel
    //{
    //    public static string SkinPath => Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
    //                                       @"\Molecular Devices\GimliData\_Skins_\";
    //    private IInstrumentDetector mDetector;
    //    private GimliReader mReader;
    //    private GimliRead mCurrentRead;
    //    private bool mStopClicked;

    //    public GimlliReaderModel()
    //    {
            
    //    }
    //    public GimliReader Reader
    //    {
    //        get
    //        {
    //            if (mDetector == null)
    //            {
    //                mDetector = GimliDetectorFactory.GenerateDefaultInstrumentDetector();
    //                mReader = GimliReader.CreateReader(mDetector);
    //            }
    //            return mReader;
    //        } 
    //    }
    //    public ReaderStatus Status { get; set; }
    //    private bool StartRead()//IReadArgsBase readArgs)
    //    {
    //        string readInfo = null;
    //        try
    //        {
    //            Status = ReaderStatus.Initializing;

    //            mCurrentRead = GimliRead.SetupNewRead(null, 0, mReader.Reader);
    //            mCurrentRead.ReadStatusChanged += MCurrentRead_ReadStatusChanged;
    //            mCurrentRead.TemperatureChanged += MCurrentRead_TemperatureChanged;
    //            mCurrentRead.ConfirmMessage += MCurrentRead_ConfirmMessage;
    //            mStopClicked = false;

    //            if (!mCurrentRead.StartMethod(new CancellationToken()))//readArgs))
    //            {
    //                Status = ReaderStatus.Idle;
    //                return false;
    //            }
    //        }
    //        catch (Exception exc)
    //        {
    //            System.Diagnostics.Trace.WriteLine("Reading failed: " + exc.Message);
    //            Status = ReaderStatus.Idle;
    //            return false;
    //        }

    //        //StartReadEventWithInfoArgs startReadEventWithInfoArgs = ReadHelper.GetStartReadEventArgs(mCurrentReaderSettings, mCurrentRead, Paradigm);
    //        //OnReaderStarted(startReadEventWithInfoArgs);

    //        return true;//new CommandResponse() { Result = CommandResult.Successful };
    //    }

    //    private void MCurrentRead_ConfirmMessage(object sender, ReaderMessageEventArgs e)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    private void MCurrentRead_TemperatureChanged(object sender, TemperatureChangedEventArgs e)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    private void MCurrentRead_ReadStatusChanged(ReaderStatus status, string message)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    public class ReaderMessageEventArgs : EventArgs
    {
        public ReaderMessageEventArgs(object message)
        {
            Message = message;
        }

        public object Message { get; set; }
    }
}
