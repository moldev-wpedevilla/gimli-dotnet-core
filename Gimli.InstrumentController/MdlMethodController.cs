﻿using System.Collections;
using System.Net.Sockets;
using Apex.Device;
using Apex.Device.Cartridges;
using Apex.Extensibility.Interfaces;
using Gimli.JsonObjects;
using Gimli.InstrumentController.Multimode;

namespace Gimli.InstrumentController
{
    public class MdlMethodController
    {
        public static IMethodController GenerateMethodController(GimliReader gimli, Plate plate, int snapshotNumber = 0)
        {
            var converter = new MdlMethodController();
            return converter.CreateController(gimli, plate, snapshotNumber);
        }

        private IMethodController CreateController(GimliReader gimli, Plate plate, int snapshotNumber)
        {
            var labware = MicroplateConverter.GenerateLabwareFromMicroplate(plate.Settings.Plate);
            var readArea = ReadAreaConverter.GenerateLayoutFromReadArea(plate.Settings.ReadArea);

            var measureTech = ConvertMeasureTechnique(plate.SnapShot[snapshotNumber].Mode);

            var methodProvider = GetMethodProvider(gimli, measureTech);
            var method = methodProvider.GetMethod(string.Empty, gimli.Reader, "Selectable Wavelength");
            var methodController = method.GetController(gimli.Reader , readArea);

            
            //HookEvents();
            //if (mSettings.ReadingHeight > 0.0)
            //    layout.StartingReadingHeight = mSettings.ReadingHeight;
            //mRunPlateInfo = readArgs.PlateInfo;
            ////Measurement start
            //mMethodController.Start(readLabware, true);
            return methodController;
        }

        private MeasureTech ConvertMeasureTechnique(Mode mode)
        {
            switch (mode)
            {
                case Mode.Abs:
                    return MeasureTech.Absorbance;
                case Mode.Lum:
                    return MeasureTech.Luminescence;
                case Mode.Trf:
                    return MeasureTech.TimeResolvedFluorescence;
                default:
                    return MeasureTech.FluorescenceIntensityTop;
            }
        }

        protected MethodProvider GetMethodProvider(GimliReader reader, MeasureTech measTech)
        {
            var mps =reader.Reader.DetectionMethodProviders;
            var cart = ((LmbConfig) reader.Config).TopMono;
            //use only methods from selected cartridge
            foreach (var mp in mps)
            {
                if (mp.IsUsing(cart) && mp.Technique == measTech)
                    return mp;
            }
            return null;
        }
    }
}
