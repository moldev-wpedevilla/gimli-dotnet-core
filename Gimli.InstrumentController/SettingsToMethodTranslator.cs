﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Apex.Device;
using Apex.Device.Methods;
using Apex.Extensibility.Device;
using Apex.Extensibility.Interfaces;
using Apex.Extensibility.Layouts;
using Gimli.InstrumentController.Multimode;
using Gimli.InstrumentController.Properties;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Parameter;
using Gimli.JsonObjects.Settings;
using Gimli.InstrumentServer;

namespace Gimli.InstrumentController
{
    public class SettingsToMethodTranslator : ISettingsToMethodTranslator
    {
        private readonly PlateSettings mPlateSettings;
        private readonly MeasurementSettings mMeasurementSettings;
        private const double FlashConversionFactor = 0.0078125;
        private SettingsToMethodTranslator(PlateSettings plateSettings, MeasurementSettings measSettings)
        {
            mPlateSettings = plateSettings;
            mMeasurementSettings = measSettings;
            Labware = MicroplateConverter.GenerateLabwareFromMicroplate(mPlateSettings.Plate);
            Layout = TranslateLayout(measSettings.ReadArea, plateSettings, measSettings);
        }

        public static ISettingsToMethodTranslator Generate(PlateSettings plateSettings, MeasurementSettings measSettings)
        {
            if (plateSettings == null)
                throw new ArgumentNullException(nameof(plateSettings));
            if (measSettings == null)
                throw new ArgumentNullException(nameof(measSettings));

            return new SettingsToMethodTranslator(plateSettings, measSettings);
        }

        public IDetectMethod Translate(FluoroGimli reader)
        {
            return Translate(reader, false);
        }

        public IDetectMethod Translate(FluoroGimli reader, bool ignoreKineticInterval)
        {
            var method = DetectionMethodFactory.Instance.Generate(mMeasurementSettings, reader);
            method.MeasType = method.AllTypes.HasFlag(MeasureTypes.Monochromatic) ? MeasureTypes.Monochromatic : method.MeasType;
            //make sure we pass the labware

            var labCons = method as ILabwareConsumer;
            labCons?.OnLabwareChanged(Labware);
            //make sure we pass the read area 
            var layCons = method as ILayoutConsumer;
            layCons?.OnLayoutChanged(Layout);

            SetWavelengthsParameters(method);
            SetDetectionParameters(method);
            //needed for the refresh of the min kin time
            layCons?.OnLayoutChanged(Layout);
            SetReadTypeSpecificParameters(method, ignoreKineticInterval);
            return method;
        }

        public ILayout Layout { get; private set; }
        public ILabware Labware { get; private set; }

        private void SetDetectionParameters(IDetectMethod method)
        {
            if(mMeasurementSettings.MeasurementMode == Mode.Abs)
                method.Parameters.GetNamedParameter("FlyMode").Value = mMeasurementSettings.DetectionSetting.SpeedRead.ToBool();
            switch (mMeasurementSettings.MeasurementMode)
            {
                case Mode.Lum:
                    SetIntegrationTime(method);
                    SetBottomReading(method);
                    break;
                case Mode.Fp:
                case Mode.Fl:
                    SetIntegrationTime(method);
                    //if (mMeasurementSettings.DetectionSetting.SpeedRead.ToBool())
                    //    method.Parameters.GetNamedParameter("NrOfPulses").Value = mMeasurementSettings.DetectionSetting.IntegrationTime.Minimum;
                    //else
                    //    method.Parameters.GetNamedParameter("NrOfPulses").Value = mMeasurementSettings.DetectionSetting.IntegrationTime.ToInt();//Math.Round((mMeasurementSettings.DetectionSetting.IntegrationTime.ToDouble() / 1000.0) / FlashConversionFactor, 0);
                    SetPmtGain(method);
                    break;
                case Mode.Trf:
                    SetIntegrationTime(method);
                    SetBottomReading(method);
                    SetPmtGain(method);
                    SetTrfParameters(method);
                    break;
            }
        }

        private void SetBottomReading(IDetectMethod method)
        {
            var br = method as IBottomReading;
            if(br == null)
                return;
            if (mMeasurementSettings.DetectionSetting.ReadFromBottom.ToBool())
                br.IsBottomReadingSelected = true;
        }

        private void SetTrfParameters(IDetectMethod method)
        {
            method.Parameters.GetNamedParameter("NrOfPulses").Value = mMeasurementSettings.DetectionSetting.NumberOfPulses.ToInt();
            method.Parameters.GetNamedParameter("Deley2MeasTime").Value = mMeasurementSettings.DetectionSetting.MeasurementDelay.ToDouble() / 1000000.0;
        }

        private void SetPmtGain(IDetectMethod method)
        {
            if (method.PmtGain != null && method.MeasTech != MeasureTech.TimeResolvedFluorescence)//omit setting some default value (Auto) for TRF as it only support Digital for now
            {
                method.PmtGain.GainType = GetPmtGainParameter(mMeasurementSettings.DetectionSetting);
                if (mMeasurementSettings.DetectionSetting.AttenuationValue != null && mMeasurementSettings.DetectionSetting.AttenuationValue.ToInt() > 0)
                {
                    method.PmtGain.Attenuation.IsSelected = true;
                    method.PmtGain.Attenuation.SelectedAttenuationLevel.Value =
                        mMeasurementSettings.DetectionSetting.AttenuationValue.ToInt();
                }
            }
        }

        private void SetIntegrationTime(IDetectMethod method)
        {
            //if (mMeasurementSettings.DetectionSetting.SpeedRead.ToBool())
            //    method.Parameters.GetNamedParameter("IntegrationTime").Value = mMeasurementSettings.DetectionSetting.IntegrationTime.Minimum / 1000.0;
            //else
				method.Parameters.GetNamedParameter("IntegrationTime").Value = mMeasurementSettings.DetectionSetting.IntegrationTime.ToDouble() / 
                (mMeasurementSettings.DetectionSetting.IntegrationTime.Units ==  "µs" ? 1000000.0 : 1000.0);
        }

        private PmtGainType GetPmtGainParameter(DetectionSettings detectionSetting)
        {
            if (detectionSetting.PmtSensitivity.Value.Equals(JsonObjects.Resources.GetResource.GetString("PmtSensitivityHigh")))
                return PmtGainType.High;
            if (detectionSetting.PmtSensitivity.Value.Equals(JsonObjects.Resources.GetResource.GetString("PmtSensitivityMedium")))
                return PmtGainType.Medium;
            if (detectionSetting.PmtSensitivity.Value.Equals(JsonObjects.Resources.GetResource.GetString("PmtSensitivityLow")))
                return PmtGainType.Low;
            //if (detectionSetting.PmtSensitivity.ToString().Equals(JsonObjects.Resources.GetResource.GetString("PmtSensitivityAuto")))
                return PmtGainType.Auto;
        }

        private static ILayout TranslateLayout(ReadAreaSettings readArea, PlateSettings plateSetting, MeasurementSettings measSetting)
        {
            var layout = ReadAreaConverter.GenerateLayoutFromReadArea(readArea, new List<IParameter> { plateSetting.IsLidded, plateSetting.Orientation });
            if (measSetting.MeasurementType == MeasurementType.SpectrumScan)
                layout.MeasurementDirection = ProcModes.Well;
            double res;
            //TODO: remove this after DoubleParameter is fixed
            if(double.TryParse(measSetting.DetectionSetting.ReadHeight.Value.ToString(), out res))
                layout.StartingReadingHeight = res;
            return layout;
        }


        private void SetWavelengthsParameters(IDetectMethod method)
        {
            var type = mMeasurementSettings.MeasurementType;

            switch (type)
            {
                case MeasurementType.Endpoint:
                case MeasurementType.Kinetic:
                case MeasurementType.AreaScan:
                    SetNonSpectrumWavelengths(method);
                    break;
                case MeasurementType.SpectrumScan:
                    method.MeasType = MeasureTypes.WavelengthScan;
                    method.ProcessMode = ProcModes.Well;
                    SetSpectrumWavelengths(method);
                    break;
            }
        }

        private void SetReadTypeSpecificParameters(IDetectMethod method, bool ignoreKineticInterval)
        {
            var type = mMeasurementSettings.MeasurementType;

            switch (type)
            {
                case MeasurementType.Kinetic:
                    method.Kinetic.NrCycles.Value = mMeasurementSettings.Kinetic.Cycles.ToInt();
                    if (mMeasurementSettings.Kinetic.IntervallShake.DoShake.ToBool())
                    {
                        method.Shaking.Duration.Value = mMeasurementSettings.Kinetic.IntervallShake.Duration.ToInt();
                        method.Shaking.Intensity.Value = ShakeConverter.GetShakeIntensity(mMeasurementSettings.Kinetic.IntervallShake.Intensity);
                        method.Shaking.Type = ShakeConverter.GetShakeType(mMeasurementSettings.Kinetic.IntervallShake.Mode);
                    }
                    if (!ignoreKineticInterval)
                        method.Kinetic.Interval.Value = mMeasurementSettings.Kinetic.Intervall.ToDouble();
                    break;
                case MeasurementType.AreaScan:
                    //line pattern
                    if (mMeasurementSettings.WellScan.ScanMode.Value.Equals("Horizontal") || mMeasurementSettings.WellScan.ScanMode.Value.Equals("Vertical"))
                    {
                        if (mMeasurementSettings.ReadArea.ReadOrder.Value.Equals("Column"))
                        {
                            method.DetectionArea.X.Value = 1;
                            method.DetectionArea.Y.Value = mMeasurementSettings.WellScan.Y_Points.Value;
                        }
                        else //row, well
                        {
                            method.DetectionArea.X.Value = mMeasurementSettings.WellScan.X_Points.Value;
                            method.DetectionArea.Y.Value = 1;
                        }
                    }
                    else // fill pattern
                    {
                        method.DetectionArea.X.Value = mMeasurementSettings.WellScan.X_Points.Value;
                        method.DetectionArea.Y.Value = mMeasurementSettings.WellScan.Y_Points.Value;
                    }

                    method.DetectionArea.XRes.Value = mMeasurementSettings.WellScan.X_Spacing.Value;
                    method.DetectionArea.YRes.Value = mMeasurementSettings.WellScan.Y_Spacing.Value;
                    break;
            }
        }

        private void SetSpectrumWavelengths(IDetectMethod method)
        {
            if (mMeasurementSettings.MeasurementMode.IsModeInFluoroGroup())
            {
                method.Parameters.GetNamedParameter("ExStartWL").Value = GetWavelenghtValue(mMeasurementSettings.Wavelengths.ExcitationStart);
                method.Parameters.GetNamedParameter("EmStartWL").Value = GetWavelenghtValue(mMeasurementSettings.Wavelengths.EmissionStart);
                if (mMeasurementSettings.Wavelengths.ExcitationSweep.ToBool())
                {
                    method.Parameters.GetNamedParameter("ExEndWL").Value = mMeasurementSettings.Wavelengths.ExcitationEnd.ToInt();
                    method.Parameters.GetNamedParameter("EmEndWL").Value = mMeasurementSettings.Wavelengths.EmissionStart.ToInt();
                    method.Parameters.GetNamedParameter("ExWLIncrement").Value = mMeasurementSettings.Wavelengths.ExcitationStep.ToInt();
                }
                else
                {
                    method.Parameters.GetNamedParameter("ExEndWL").Value = mMeasurementSettings.Wavelengths.ExcitationStart.ToInt();
                    SetEmissionEndAndIncrement(method, mMeasurementSettings.Wavelengths);
                }
            }
            else if(mMeasurementSettings.MeasurementMode == Mode.Abs)
            {
                method.Parameters.GetNamedParameter("StartWL").Value = mMeasurementSettings.Wavelengths.ExcitationStart.ToInt();
                method.Parameters.GetNamedParameter("EndWL").Value = mMeasurementSettings.Wavelengths.ExcitationEnd.ToInt();
                method.Parameters.GetNamedParameter("WLStep").Value = mMeasurementSettings.Wavelengths.ExcitationStep.ToInt();
            }
            else if (mMeasurementSettings.MeasurementMode == Mode.Lum)
            {
                method.Parameters.GetNamedParameter("EmStartWL").Value = mMeasurementSettings.Wavelengths.EmissionStart.ToInt();
                SetEmissionEndAndIncrement(method, mMeasurementSettings.Wavelengths);
            }
        }

        private static object GetWavelenghtValue(GimliParameterWavelength gimliParameterWavelength)
        {
            return gimliParameterWavelength.UseMonocromator ? gimliParameterWavelength.ToInt() : gimliParameterWavelength.Value;
        }

        private static void SetEmissionEndAndIncrement(IDetectMethod method, WavelengthSettings wavelengths)
        {
            method.Parameters.GetNamedParameter("EmEndWL").Value = wavelengths.EmissionEnd.ToInt();
            method.Parameters.GetNamedParameter("EmWLIncrement").Value = wavelengths.EmissionStep.ToInt();
        }

        private void SetNonSpectrumWavelengths(IDetectMethod method)
        {
            if (mMeasurementSettings.Wavelengths.NrOfWavelengths.ToInt() > 1 && mMeasurementSettings.MeasurementMode != Mode.Fp)
            {
                method.MeasType = MeasureTypes.Multichromatic;
                method.Parameters.GetNamedParameter("MethodNrOfWavelengths", "NrOfWavelengthPairs").Value = mMeasurementSettings.Wavelengths.NrOfWavelengths.ToInt();
            }
            if (mMeasurementSettings.MeasurementMode == Mode.Lum && mMeasurementSettings.Wavelengths.AllWavelengths.ToBool())
            {
                method.Parameters.GetNamedParameter("MethodEmissionWL", "EmissionFilter").Value = 0;
                return;
            }

            SetWavelengthSource(method as IAdvancedWavelengthSelection);
            
            for (int i = 0; i < mMeasurementSettings.Wavelengths.NrOfWavelengths.ToInt(); i++)
            {
                if(mMeasurementSettings.MeasurementMode == Mode.Abs || mMeasurementSettings.MeasurementMode.IsModeInFluoroGroup())
                    CheckSetWlValue(method, mMeasurementSettings.Wavelengths.Excitation[i].Value, !mMeasurementSettings.Wavelengths.Excitation[i].UseMonocromator, 
                        $"MethodExcitationWL{GetWavelenghtIndex(i)}", $"ExcitationFilter{GetWavelenghtIndex(i)}");

                if (mMeasurementSettings.MeasurementMode == Mode.Lum || mMeasurementSettings.MeasurementMode.IsModeInFluoroGroup())
                    CheckSetWlValue(method, mMeasurementSettings.Wavelengths.Emission[i].Value, !mMeasurementSettings.Wavelengths.Emission[i].UseMonocromator,
                        $"MethodEmissionWL{GetWavelenghtIndex(i)}", $"EmissionFilter{GetWavelenghtIndex(i)}");
                if (mMeasurementSettings.MeasurementMode == Mode.Fp)
                    break;
            }

            if (mMeasurementSettings.MeasurementMode == Mode.Abs && mMeasurementSettings.MeasurementType == MeasurementType.Endpoint)
                method.Parameters.GetNamedParameter("PathCheck").Value = mMeasurementSettings.Wavelengths.PathCheck.ToBool();
        }

        private static void CheckSetWlValue(IDetectMethod method, object value2Set, bool useFilter, params string[] paramNames)
        {
            try
            {
                method.Parameters.GetNamedParameter(paramNames).Value = value2Set;
            }
            catch (ArgumentOutOfRangeException)
            {
                if (useFilter)
                    throw new Exception(string.Format(Resources.FilterNotInsertedError, value2Set));
                throw;
            }
        }

        private void SetWavelengthSource(IAdvancedWavelengthSelection selection)
        {
            var aws = selection?.GetSelector();
            if (aws == null) return;
            if (mMeasurementSettings.Wavelengths.Excitation.Count > 0)
                aws.ChangeExcitationSource(!mMeasurementSettings.Wavelengths.Excitation[0].UseMonocromator
                    ? WavelengthSource.Filter
                    : WavelengthSource.Mono);
            if (mMeasurementSettings.Wavelengths.Emission.Count > 0)
                aws.ChangeEmissionSource(!mMeasurementSettings.Wavelengths.Emission[0].UseMonocromator
                    ? WavelengthSource.Filter
                    : WavelengthSource.Mono);
        }

        private static string GetWavelenghtIndex(int index)
        {
            return index == 0 ? string.Empty : (index + 1).ToString(CultureInfo.InvariantCulture);
        }
    }

    internal static class ParameterExtenstions
    {
        internal static MethodParameter GetNamedParameter(this MethodParameter[] parameters, params string[] names)
        {
            foreach (var parameter in parameters.Where(parameter => names.Any(name => parameter.Name.Equals(name))))
            {
                return parameter;
            }
            throw new ArgumentException($"Settings contain some illegal values: {names[0]}");
        }
    }

}
