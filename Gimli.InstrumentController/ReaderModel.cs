﻿using System;
using System.Diagnostics;
using Apex.Device;
using Apex.Remoting;

namespace Gimli.InstrumentController
{
    public class ReaderModel
    {
        public static string SkinPath => Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                                           @"\Molecular Devices\GimliData\_Skins_\";
        private FluoroLmb mReader;
       public FluoroLmb Reader
        {
            get
            {
                if (mReader == null)
                {
                    var instrSkin = new InstrumentCreator(SkinPath).GetSkinWithName("SpectraMax jx");
                    mReader = FluoroLmb.CreateReaderObjectWithAllCartridges(instrSkin);
                }
                try
                {
                    if (!mReader.Connected)
                        mReader.Connect(true);
                }
                catch (Exception exc)
                {
                    Trace.WriteLine("Error connecting Gimli: " + exc.Message);
                }
                
                return mReader;
            } 
        }
    }
}
