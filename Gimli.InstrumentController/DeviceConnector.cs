﻿using System;
using System.Threading;
using Apex.Device;
using Apex.Remoting;
using Gimli.Instrument;
using MolDev.Common.Logger;

namespace Gimli.InstrumentController
{
    public static class DeviceConnectorFactory
    {
        static DeviceConnectorFactory()
        {
            SupportSimualtion = false;
        }
        public static bool SupportSimualtion { get; set; }

        public static IInstrumentConnector GenerateInstrumentConnector(ICommServer commServer, ILogService logger)
        {
            return DeviceConnector.Generate(commServer, SupportSimualtion, logger);
        }

        public static IInstrumentConnector GenerateDefaultInstrumentConnector(ILogService logger)
        {
            return DeviceConnector.Generate(ConnectionHelper.DefaultCommServerConnection, SupportSimualtion, logger);
        }
	}
	public enum DeviceInitState
	{
		None,
		Ready,
		Measuring,
		Connecting,
		Failed
	}

	public class DeviceConnector : CommClientSink, IInstrumentConnector
    {


        private readonly ICommServer mCommServer;
        private readonly bool mDoSimulation;
        private readonly ManualResetEventSlim mRespReceived = new ManualResetEventSlim();
        private string mResponse;
        private ICommDevice mCommDevice;
        private readonly ILogService mLogger;

        private const string GoToNextTagPosCmd = "+";

        public static IInstrumentConnector Generate(ICommServer commServer, bool offlineReader, ILogService logger)
        {
            if (commServer == null)
                throw new ArgumentNullException();
            return new DeviceConnector(commServer, offlineReader, logger);
        }

        protected DeviceConnector(ICommServer commServer, bool offlineReader, ILogService logger) : base(false)
        {
            mLogger = logger;
            mCommServer = commServer;
            Connected = false;
            mDoSimulation = offlineReader;
            EstablishConnection();
        }

        public bool Connected { get; private set; }

        public void Refresh()
        {
            try
            {
                Connected = false;

                mLogger.Info(LogCategoryEnum.Host, "Before server refresh.");
                ServerRefresh();
				foreach (var device in mCommServer.Devices)
                {
                    var name = device.Type.ToLower();
                    mLogger.Info(LogCategoryEnum.Host, $"Instrument {name} on {device.Port} with Serial#: {device.SerialNr} is {device.State}");

                    if (!name.ToLower().Contains(RecognitionString) || device.Port != "USB")
                        continue;

                    if (device.State != DeviceStates.Online) continue;
                    DefaultReaderProductName = device.Type;
                    mCommServer.GainControl(AttendeeID, device, DeviceStates.Online);
                    Connected = true;
                    mCommDevice = device;
                    break;
                }
            }
            catch (Exception e)
            {
                //ExceptionLogger.LogException(e);
                mLogger.Error(LogCategoryEnum.Host, "DeviceDetector::EstablishConnection: ", e);
            }
        }

        public DeviceInitState IsReady()
        {
            if (mCommServer == null)
                return DeviceInitState.Connecting;
            mResponse = string.Empty;
            mRespReceived.Reset();
            mCommServer.SendCommand(AttendeeID, "STAT", 1, 2);
            //return WaitForDoneAndGetResponse(2).ToUpper().Contains("READY");
            var resultStr = WaitForDoneAndGetResponse(2).ToUpper();
			if (resultStr.Contains("READY")) return DeviceInitState.Ready;
	        return resultStr == string.Empty || resultStr.Contains("MEASURING") || resultStr.EndsWith("TIME OUT") ? DeviceInitState.Measuring : DeviceInitState.Failed;
        }

        public void Shutdown()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Acknowledges reading filter (slide) tag on current NFC antenna position.
        /// </summary>
        public void Acknowledge()
        {
            try
            {
                mCommServer.SendCommand(AttendeeID, GoToNextTagPosCmd, 1, 5);
            }
            catch (Exception exc)
            {
                System.Diagnostics.Trace.WriteLine($"Acknowledging filter/slide tag reading: {exc.Message}");
            }
        }

        public void ReleaseDevice()
        {
            throw new NotImplementedException();
        }

        public event EventHandler<FilterTagEventArgs> FilterPos;
        public event EventHandler<SlideTagEventArgs> SlidePos;
        public event EventHandler<PosEventArgs> Pos;

        public void OnFilterPos(InstrumentRequestSlide slide, int filter)
        {
            FilterPos?.BeginInvoke(this, new FilterTagEventArgs(slide, filter), ar => { }, new object());
        }

        public void OnSlidePos(InstrumentRequestSlide slide)
        {
            SlidePos?.BeginInvoke(this, new SlideTagEventArgs(slide), ar => { }, new object());
        }

        public void OnPos(int pos)
        {
            Pos?.BeginInvoke(this, new PosEventArgs(pos), ar => { }, new object());
        }

        private string WaitForDoneAndGetResponse(int timeout)
        {
            var start = DateTime.Now;
            while (!mRespReceived.Wait(100))
            {
                if (DateTime.Now.Subtract(start).TotalSeconds > timeout)
                    break;
            }
            return mResponse;
        }

        protected void ServerJoin()
        {
            mCommServer.Join(AttendeeID, this);
        }

        protected void ServerRefresh()
        {
            mCommServer.Refresh(DeviceListTypes.ExcludeNET | DeviceListTypes.Connected);
        }

        protected void ServerExit()
        {
            mCommServer.Exit(AttendeeID);
        }

        private void EstablishConnection()
        {
            try
            {

                mLogger.Info(LogCategoryEnum.Host, "Before server join");
                ServerJoin();
				if (mDoSimulation)
                {
                    return;
                }
                Refresh();
            }
            catch (Exception e)
            {
                //ExceptionLogger.LogException(e);
                mLogger.Error(LogCategoryEnum.Host, "DeviceDetector::EstablishConnection: ", e);
            }
        }

        internal const string Id5 = "SpectraMax iD5";
        internal static string DefaultReaderProductName = Id5; //id5 is the default so that the simulator will always be id5


        public FluoroGimli GetReaderObject()
        {
            if (mDoSimulation) return CreateSimualationObject();

            mCommServer.ReleaseControl(AttendeeID, false);
            var rdr = new FluoroGimli(ConnectionHelper.InstrumentCreator.GetSkinWithName(DefaultReaderProductName), null);
            rdr.CommDevice = mCommDevice;
            rdr.Connect(true);
            Connected = false;
            return rdr;
        }

        public static FluoroGimli CreateSimualationObject()
        {
            return FluoroGimli.CreateReaderObjectWithAllCartridges(ConnectionHelper.InstrumentCreator.GetSkinWithName(DefaultReaderProductName));
        }

        protected const string RecognitionString = "spectramax";

        private int ExtractPosition(string wholeCmd, string param)
        {
            int pos = int.Parse(param);
            return pos;
        }

        private InstrumentRequestSlide ExtractSlideForReadReq(string wholeCmd, string param)
        {
            InstrumentRequestSlide slide;
            switch (param)
            {
                case "A":
                    slide = InstrumentRequestSlide.A;
                    break;
                case "B":
                    slide = InstrumentRequestSlide.B;
                    break;
                default:
                    string errText = $"Error extracting slide param of FP/SP req: {wholeCmd}";
                    System.Diagnostics.Trace.WriteLine(errText);
                    throw new ArgumentException(errText);
            }
            return slide;
        }

        private int ExtractFilterForReadReq(string wholeCmd, string param)
        {
            int filter;
            if (!int.TryParse(param, out filter))
            {
                string errText = $"Error extracting filter param of FP/SP req: {wholeCmd}";
                System.Diagnostics.Trace.WriteLine(errText);
                throw new ArgumentException(errText);
            }
            if (filter < 1 || filter > 6)
            {
                string errText = $"Invalid extracting filter param of FP/SP req: {wholeCmd}";
                System.Diagnostics.Trace.WriteLine(errText);
                throw new ArgumentException(errText);
            }
            return filter;
        }

        protected override void OnMessageReceived(string member, ICommDevice dev, string msg, int n, string s1, string s2)
        {
            System.Diagnostics.Trace.WriteLine("DeviceConnector::OnMessageReceived(): " + msg);
            //if (msg.Equals(ShutdownReq))
            //    OnShutdownRequested();

            mResponse = msg;
            mRespReceived.Set();
        }
    }

    internal class ConnectionHelper
    {
        internal static ICommServer DefaultCommServerConnection
        {
            get
            {
                bool local;
                return ServerConfig.ConfigureAndConnectToServer(GetMultimodePath() + @"templates\Client.exe.config", out local);
            }
        }

        internal static InstrumentCreator InstrumentCreator
        {
            get { return new InstrumentCreator(GetMultimodePath() + @"\skins"); }
        }

        private static string GetMultimodePath()
        {
            return Anthos.ResourceReader.DirectoryLookup.GetAllUsersBaseDir;
        }
    }
}
