﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using Akka.IO;
using Anthos.Controls.Properties;
using Apex.Device;
using Apex.Extensibility.Interfaces;
using Apex.Extensibility.Layouts;
using Apex.LabwareLib;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;

namespace Gimli.InstrumentController.InjectorMaintenance
{
    public class DispenseController
    {
        private IMethodController mMethodController;
        private ManualResetEventSlim mReadDoneEvent;
        private readonly IActorService mActorService;
		private bool mLastCommandSucceeded;

		protected DispenseController(IActorService actorService)
        {
            mActorService = actorService;
        }

        public static DispenseController Generate(IActorService actorService)
        {
            return new DispenseController(actorService);
        }

        private void MethodControllerOnError(object sender, RunMethodErrorEventArgs runMethodErrorEventArgs)
        {
            Trace.WriteLine("MethodControllerOnError: " + runMethodErrorEventArgs.Error.Message);
            mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
            {
                Parameters = new MessageParameters
                {
                    Message = runMethodErrorEventArgs.Error.Message,
                    DisplayType = MessageDisplayType.DialogOk,
                    MessageCode = MessageStatusCodes.InjectionError,
                    MessageType = MessageType.Error
                }
            }));

	        mLastCommandSucceeded = false;
            mReadDoneEvent.Set();
        }

        private void MethodControllerOnDataReceived(object sender, RunMethodEventArgs args)
        {
            Trace.WriteLine("MethodControllerOnDataReceived : row " + args.StartRow);
	        if (9 != args.EventNr) return;

	        mLastCommandSucceeded = true;
	        mReadDoneEvent.Set();
        }

        private void HookEvents()
        {
            if (mMethodController == null)
                return;
            mMethodController.DataReceived += MethodControllerOnDataReceived;
            mMethodController.Error += MethodControllerOnError;
        }

        private void UnHookEvents()
        {
            if (mMethodController == null)
                return;
            mMethodController.DataReceived -= MethodControllerOnDataReceived;
            mMethodController.Error -= MethodControllerOnError;

            Thread.Sleep(1000);
            mMethodController = null;
        }

        private ILabware CreateNewLabware()
        {
            var labw = new Labware(96, new MeasTechCollection() { MeasureTech.Absorbance, MeasureTech.Luminescence, MeasureTech.FluorescenceIntensityTop, MeasureTech.FluorescenceIntensityBottom, MeasureTech.Injecting }, null);

            var lot = new LabwareLot(labw);
            lot.WellDistance_X = 0.9;
            lot.WellDistance_Y = 0.9;
            lot.WellLength_X = 0.6782;
            lot.WellWidth_Y = 0.6782;
            lot.WellDepth_Z = 1;

            labw.WellVolume = 300;
            labw.addLabwareLot(lot, true);
            labw.PlateHeight_Z = 1.5;

            return labw;
        }

        public bool DispensePlate(IInstrument reader, int injector)
        {
            if (!CheckIfFirstWellExists(reader))
            {
                NoPlateOnCalibrationDespenseError();
                return false;
            }

            mReadDoneEvent = new ManualResetEventSlim();
            UnHookEvents();

            var mth = reader.GetDetectMethod(MeasureTech.Injecting);
            mth.Parameters[0].Value = 0;
            var fi = mth as IFluidInjection;
            fi.SequencesInUse = 1;
            fi.GetParametersForSequence(1).ElementAt(0).Value = injector;
            fi.GetParametersForSequence(1).ElementAt(1).Value = 50;

            var lay = new Layout(8, 12, 96, (int)ProcModes.Column, "S") { MeasurementDirection = ProcModes.Row };

            mth.ProcessMode = ProcModes.Row;
            ((ILayoutConsumer)mth).OnLayoutChanged(lay);

            mMethodController = reader.GetMethodController(mth, lay);
            HookEvents();
            var lab = CreateNewLabware();
            mMethodController.Start(lab, true);

            mReadDoneEvent.Wait();
	        return mLastCommandSucceeded;
        }

        private bool CheckIfFirstWellExists(IInstrument reader)
        {
            var gimli = reader as FluoroGimli;
            if (null == gimli)
                return false;

            var transportController = gimli.GeTransportController();
            if (null == transportController)
                return false;

            if (transportController.CheckIfFirstWellExists())
                return true;
            return false;
        }

        private void NoPlateOnCalibrationDespenseError()
        {
            Trace.WriteLine("NoPlateOnDispenseError(): ");

            mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
            {
                Parameters = new MessageParameters
                {
                    Message = Properties.Resources.NoPlateInsertedOnCalibrationDispensing,
                    DisplayType = MessageDisplayType.DialogOk,
                    MessageCode = MessageStatusCodes.InjectionError,
                    MessageType = MessageType.Error
                }
            }));

            mLastCommandSucceeded = false;
        }
    }
}
