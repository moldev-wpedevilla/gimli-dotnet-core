﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Apex.Device;
using Gimli.InstrumentController.GimliDevice;
using Gimli.InstrumentServer;
using Gimli.JsonObjects;
using Gimli.JsonObjects.InjectorMaintenance;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;
using Gimli.InstrumentServer.GimliDevice;

namespace Gimli.InstrumentController.InjectorMaintenance
{
    public class InjectorMaintenance : IInjectorMaintenanceController
    {
        private const double CmdTimeoutFactor = 2.0;
        private const int PlateTransportLagInSecs = 5;
        private const int VolumeRoundUpInSecs = 1;
        private const int VolumeCounterOperationTime = 2;

        private readonly Func<IAdvancedDirectCommunication> mGetDirectComm;
		private readonly ILogService mLogger;
		private IAdvancedDirectCommunication mDirectComm;
        public ManualResetEventSlim InjectorCmdDoneEvent;
        private DateTime mStartTime;
        private readonly IActorService mActorService;

        private readonly IGimliDeviceHelper mHelper;

        private int mVolume1;
        private int mVolume2;

	    private bool mLastCommandSucceeded;

        public static IInjectorMaintenanceController Generate(ILogService logger, Func<IAdvancedDirectCommunication> getDirectComm, IActorService actorService, IGimliDeviceHelper helper)
        {
            return new InjectorMaintenance(logger, getDirectComm, actorService, helper);
        }

        protected InjectorMaintenance(ILogService logger, Func<IAdvancedDirectCommunication> getDirectComm, IActorService actorService, IGimliDeviceHelper helper)
        {
            mLogger = logger;           
            mGetDirectComm = getDirectComm;
            mActorService = actorService;
            mHelper = helper;
        }

        private string ComposeCmd(InjectorSel injector, string cmd)
        {
            var command = string.Empty;
            if (InjectorSel.Injector1 == injector || InjectorSel.Injector2 == injector)
            {
                command = $"{cmd} {(int)injector}";
            }
            return command;
        }

        private string ComposePrimeCommand(InjectorSel injector)
        {
            return ComposeCmd(injector, "P");
        }

        private string ComposeRevPrimeCommand(InjectorSel injector)
        {
            return ComposeCmd(injector, "REVP");
        }

        private string ComposeRinseCommand(InjectorSel injector)
        {
            return ComposeCmd(injector, "R");
        }

        private string GetCombinedInjectorSel(InjectorSel injector)
        {
            var injectorSel = string.Empty;
            switch (injector)
            {
                case InjectorSel.Injector1:
                    injectorSel = "1";
                    break;
                case InjectorSel.Injector2:
                    injectorSel = "2";
                    break;
                case InjectorSel.None:
                    break;
                default:
                    if (injector.HasFlag(InjectorSel.Injector1) && injector.HasFlag(InjectorSel.Injector2)) injectorSel = "3";
                    break;
            }
            return injectorSel;
        }

        private string ComposeSetCalibCommand(int injector, double deviation)
        {
            string command = $"PUMPRESO {injector} {deviation:F2}";
            return command;
        }

        private string ComposeWashCommand(InjectorSel injector, int volumeInUs, bool isReversed)
        {
            var direction = isReversed ? "1" : "0";
            var injectorSel = GetCombinedInjectorSel(injector);
            string command = $"W {injectorSel} {volumeInUs} {direction}";
            return command;
        }

        private string ComposeWashForCalibCommand(InjectorSel injector, int volume)
        {
            var injectorSel = GetCombinedInjectorSel(injector);
            string command = $"W {injectorSel} {volume} {"0"}";
            return command;
        }

        private string ComposeResetVolCounterCommand(InjectorSel injector)
        {
            var injectorSel = GetCombinedInjectorSel(injector);
            string command = $"RESCOUNT {injectorSel}";
            return command;
        }

        private void PrepareDirectCmd()
        {
            mStartTime = DateTime.Now;
            InjectorCmdDoneEvent = new ManualResetEventSlim();
            mDirectComm = mGetDirectComm();
            mDirectComm.ResponseReceived += (s, e) => ResponseEvent(s, e);
        }

        private void InjectorCmdWithCmdParam(string funName, string cmd, int timeoutInSecs, CancellationToken cancellationToken)
        {
            try
            {
                mLogger.Info(LogCategoryEnum.Host, $"Starting {funName}() cmd: {cmd} timeoutInSecs {timeoutInSecs}.");
                PrepareDirectCmd();
                mDirectComm.SendCommand(cmd, 1, timeoutInSecs);
                Task.Run(() => CheckExeFwTimeout(timeoutInSecs, cancellationToken), cancellationToken);
                InjectorCmdDoneEvent.Wait(cancellationToken);
            }
            catch (Exception exc)
            {
                mLogger.Error(LogCategoryEnum.Host, $"Failed due to {exc.Message} - {exc.InnerException?.Message}");
                throw;
            }
            finally
            {
                mLogger.Info(LogCategoryEnum.Host, $"{funName}() finished.");
            }
        }

        private void InjectorCmdWithInjectorParam(string funcName, Func<InjectorSel, string> composeCommand, InjectorSel injector, int timeoutInSecs, CancellationToken cancellationToken)
        {
            var cmd = composeCommand(injector);
            InjectorCmdWithCmdParam(funcName, cmd, timeoutInSecs, cancellationToken);
        }

        private int GetInjectorMaintenanceCmdTimoutInSecs(int volumeInUl)
        {
            return (int)(((volumeInUl * CmdTimeoutFactor) / 100) + VolumeRoundUpInSecs + PlateTransportLagInSecs);
        }

        private int GetPrimeCmdTimoutInSecs(InjectorSel injector, List<InjectorConfiguration> config)
        {
            return GetInjectorMaintenanceCmdTimoutInSecs(config[(int) (injector - 1)].PrimeVolume);
        }

        public void Prime(InjectorSel injector, List<InjectorConfiguration> config, CancellationToken cancellationToken)
        {
            var funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            InjectorCmdWithInjectorParam(funcName, ComposePrimeCommand, injector, GetPrimeCmdTimoutInSecs(injector, config), cancellationToken);
        }

        public void RevPrime(InjectorSel injector, List<InjectorConfiguration> config, CancellationToken cancellationToken)
        {
            var funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            InjectorCmdWithInjectorParam(funcName, ComposeRevPrimeCommand, injector, GetPrimeCmdTimoutInSecs(injector, config), cancellationToken);
        }

        public void Rinse(InjectorSel injector, List<InjectorConfiguration> config, CancellationToken cancellationToken)
        {
            var funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            InjectorCmdWithInjectorParam(funcName, ComposeRinseCommand, injector, GetPrimeCmdTimoutInSecs(injector, config), cancellationToken);
        }

        private static int GetVolumeCounterCmdTimeoutInSecs()
        {
            return VolumeCounterOperationTime + VolumeRoundUpInSecs + PlateTransportLagInSecs;
        }

        public void ResetVolumeCounter(InjectorSel injector, CancellationToken cancellationToken)
        {
            var funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            InjectorCmdWithInjectorParam(funcName, ComposeResetVolCounterCommand, injector, GetVolumeCounterCmdTimeoutInSecs(), cancellationToken);
        }

        public void Wash(InjectorSel injector, int volume, bool isReversed, CancellationToken cancellationToken)
        {
            var funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var cmd = ComposeWashCommand(injector, volume, isReversed);
            var timeoutInSecs = GetInjectorMaintenanceCmdTimoutInSecs(volume);
            InjectorCmdWithCmdParam(funcName, cmd, timeoutInSecs, cancellationToken);
        }

        public List<double> GetVolumeCounter()
        {
            var funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            InjectorCmdWithCmdParam(funcName, "GETCOUNT", GetVolumeCounterCmdTimeoutInSecs(), new CancellationToken());
            return new List<double>() { mVolume1, mVolume2 };
        }

        public bool WashForCalibration(InjectorSel injector, CancellationToken cancellationToken)
        {
            var funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var volume = 10000;
            var cmd = ComposeWashForCalibCommand(injector, volume);
            var timeoutInSecs = GetInjectorMaintenanceCmdTimoutInSecs(volume);
            InjectorCmdWithCmdParam(funcName, cmd, timeoutInSecs, cancellationToken);
	        return mLastCommandSucceeded;
        }

        public bool DispensePlate(InjectorSel injector, CancellationToken cancellationToken)
        {
            var dispenser = DispenseController.Generate(mActorService);
            var succeeded = dispenser.DispensePlate(mHelper.GimliReader, (int)injector);
	        return succeeded;
        }

        public void SetCalibrationFactor(InjectorSel injector, double weight1, double weight2, CancellationToken cancellationToken)
        {
            int intWeight1 = (int) (weight1 * 1000);
            int intWeight2 = (int) (weight2 * 1000);
            var funcName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var factor = 4800.0 / ((intWeight1 + intWeight2)/2.0);

            var cmd = ComposeSetCalibCommand((int)injector, (float)factor);
            var timeoutInSecs = 2;
            InjectorCmdWithCmdParam(funcName, cmd, timeoutInSecs, cancellationToken);
        }


        // --------------------------------------------------------------------


        private void CheckExeFwTimeout(int timeoutInSecs, CancellationToken cancellationToken)
        {
            GimliDeviceHelper.CheckExeFwTimeout(mDirectComm, timeoutInSecs, cancellationToken, mStartTime, InjectorCmdDoneEvent, mLogger);
        }

        public void ResponseEvent(object sender, ResponseReceivedArgs args)
        {
            if (args.ResponseMessage.StartsWith("-"))
            {
                mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
                {
                    Parameters = new MessageParameters
                    {
                        Message = args.ResponseMessage, DisplayType = MessageDisplayType.DialogOk, MessageCode = MessageStatusCodes.InjectionError, MessageType = MessageType.Error
                    }
                }));
	            mLastCommandSucceeded = false;
                InjectorCmdDoneEvent.Set();
			}
            else if (args.ResponseMessage.StartsWith("+"))
            {
                var parts = args.ResponseMessage.Split(' ');
                mVolume1 = 0;
                mVolume2 = 0;
                if (3 == parts.Length) // GETCOUNT response
                {
                    mVolume1 = int.Parse(parts[1]);
                    mVolume2 = int.Parse(parts[2]);
                }
	            mLastCommandSucceeded = true;
                InjectorCmdDoneEvent.Set();
			}
        }
    }
}
