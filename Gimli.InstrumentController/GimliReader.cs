﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Anthos.Controls;
using Apex.Device;
using Apex.Extensibility.Interfaces;
using Apex.Device.Cartridges;
using Apex.Remoting;

namespace Gimli.InstrumentController
{
    //not used at the moment
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    public static class ParadigmReaderFactory
    {
        public static GimliReader GenerateParadigmReader(IInstrumentDetector detector)
        {
            return GimliReader.CreateReader(detector);
        }

        public static GimliReader GenerateAutomatedParadigmReader(IInstrumentDetector detector)
        {
            return GimliReader.CreateReader(detector);
        }
    }

    //not used at the moment
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    public class GimliReader
    {

        public static GimliReader CreateReader(IInstrumentDetector detector)
        {
            if (detector == null)
                throw new ArgumentNullException();
            return new GimliReader(detector);
        }
        protected readonly IInstrumentDetector mGimliDetector;
        protected readonly IInteractionServices.IInteractionService mInteractionService;
        private FluoroGimli mReader;

        private GimliReader(IInstrumentDetector detector) 
        {
            mGimliDetector = detector;

            DependencyManager.SetDependency<IInteractionServices.IInteractionService>(InteractionServices.InteractionService.Generate());
            mInteractionService = DependencyManager.GetDependency<IInteractionServices.IInteractionService>();

            Initialize("USB", 1);
        }

        public FluoroGimli Reader => mReader;
        public void Uninitialize()
        {
            Trace.WriteLine("ParadigmReader::Uninitialize().");
            SilentDisconnect();
        }
        public void SilentDisconnect()
        {
            if (mReader != null)
                mReader.Disconnect(true);
        }
        public bool IsTopDrawerCartridgeInstalled(Cartridge cartridge)
        {
            if (Config == null || Config.TopKeeper == null)
                return false;

            return Config.TopKeeper.PositionOf(cartridge) > -1;
        }

        public string GetInstrumentSerialNumber()
        {
            if (Config != null)
                return Config.SerialNr;
            return string.Empty;
        }

        public bool IsMeasTechSupported(string cartridgeName, MeasureTech measTech)
        {
            var detM = GetDetectMethod(cartridgeName, measTech);
            return detM != null;
        }
        
        public Cartridge[] GetInstalledCartridgePool()
        {
            if (mReader == null)
                return null;

            var unfilteredPool = new List<Cartridge>();
            var filteredPool = new List<Cartridge>();

            if (!mReader.Connected)
                unfilteredPool.AddRange(mReader.V2Config.CartridgePool);
            else
            {
                unfilteredPool.AddRange(mReader.V2Config.TopKeeper.Installed);
                unfilteredPool.AddRange(mReader.V2Config.BottomKeeper.Installed);
            }

            foreach (var cart in unfilteredPool)
            {
                if (cart.Recognized)
                    filteredPool.Add(cart);
            }

            return filteredPool.ToArray();
        }

        public Cartridge[] GetVisibleCartridgePool()
        {
            var recognizedCartridgePool = GetInstalledCartridgePool();
            var filteredPool = new List<Cartridge>();
            foreach (var cart in recognizedCartridgePool)
            {
                if (ShouldCartBeVisible(cart, recognizedCartridgePool))
                    filteredPool.Add(cart);
            }

            return filteredPool.ToArray();
        }

        protected bool ShouldCartBeVisible(Cartridge cart, Cartridge[] pool)
        {
            if (cart.IsHidden)
                return false;
            if (cart.Type != CartridgeType.AbsFlash) return true;
            //ignore abs cartridges that are not paired (same s/n)
            foreach (Cartridge innerCart in pool)
            {
                if (innerCart.Type == CartridgeType.AbsDet && cart.SerialNr == innerCart.SerialNr)
                    return true;
            }
            return false;
        }

     
        public event EventHandler ReaderWentOffline;

        public MethodProvider[] GetMethodsFromSelectedCartridge(string cartridge)
        {
            var mps = mReader.DetectionMethodProviders;
            var cartridgeMethod = new List<MethodProvider>();

            //Get selected cartridge (cboCartridges)
            Cartridge cart = FindCartrige(cartridge);
            if (cart != null)
            {
                //use only methods from selected cartridge
                foreach (MethodProvider mp in mps)
                {
                    if (mp.IsUsing(cart))
                        cartridgeMethod.Add(mp);
                }
            }

            return cartridgeMethod.ToArray();
        }

        public IDetectMethod GetDetectMethod(string cartridge, MeasureTech measTech)
        {
            //Get selected cartridge (cboCartridges)
            var cart = FindCartrige(cartridge);
            if (cart == null)
                return null;

            if (cart.Level == CartridgeLevel.Bottom && measTech == MeasureTech.FluorescenceIntensityTop)
                measTech = MeasureTech.FluorescenceIntensityBottom;
            else if (cart.Level == CartridgeLevel.Top && measTech == MeasureTech.FluorescenceIntensityBottom)
                measTech = MeasureTech.FluorescenceIntensityTop;

            MethodProvider selectedTechniqueProvider = GetMethodProvider(cart, measTech);
            if (selectedTechniqueProvider != null)
            {
                //Abs_DET / ABS-MONO doesn´t have a FilterSets (eg.: Ex340/Em616|Em665). It has one filter wavelength (eg.: 340)
                if (selectedTechniqueProvider.FilterSets.Length == 0)
                    return selectedTechniqueProvider.GetMethod("", mReader, "");
                if (cartridge == "TUNE")
                    return selectedTechniqueProvider.GetMethod("", mReader, "Selectable Wavelength");
                var meths = selectedTechniqueProvider.FilterSets.Select(fset => selectedTechniqueProvider.GetMethod("", mReader, fset)).ToList();

                return MergeFilters(meths);

            }
            return null;
        }

        public static IDetectMethod MergeFilters(List<IDetectMethod> methods)
        {
            if (methods.Count == 0)
                return null;

            if (methods.Count == 1)
                return methods[0];

            for (int im = 1; im < methods.Count; im++)
            {
                for (int i = 0; i < methods[im].Parameters.Length; i++)
                {
                    if (methods[im].Parameters[i].Name.ToLower().IndexOf("filter") > 1)
                    {
                        var objs = new List<object>(methods[0].Parameters[i].LegalValues.ToArray());
                        foreach (object obj in methods[im].Parameters[i].LegalValues.ToArray())
                        {
                            if (!objs.Contains(obj))
                                objs.Add(obj);
                        }
                        methods[0].Parameters[i].RefreshRange(objs.ToArray());
                    }
                }
            }

            return methods[0];
        }

        private IDetectMethod GetMethodFromProviderUsingFilterSets(MethodProvider mp)
        {
            var meths = mp.FilterSets.Select(fset => mp.GetMethod("", mReader, fset)).ToList();
            return MergeFilters(meths);
        }
     
        protected MethodProvider GetMethodProvider(Cartridge cart, MeasureTech measTech)
        {
            var mps = mReader.DetectionMethodProviders;
            //use only methods from selected cartridge
            foreach (var mp in mps)
            {
                if (mp.IsUsing(cart) && mp.Technique == measTech)
                    return mp;
            }
            return null;
        }

        protected Cartridge FindCartrige(string cartridge)
        {
            Cartridge cart = null;
            Cartridge[] cartridgePool = GetVisibleCartridgePool();
            if (cartridgePool != null)
            {
                foreach (Cartridge cartInstr in cartridgePool)
                {
                    if (cartridge == cartInstr.Alias)
                        cart = cartInstr;
                }
            }
            return cart;
        }

        public void OpenDrawer()
        {
            mReader.Eject(-1, -1, false);
        }

        public Cartridge GetTopKeeperCartridge(int index)
        {
            if (Config == null)
                return null;
            return Config.TopKeeper[index];
        }

        public Cartridge GetBottomKeeperCartridge(int index)
        {
            if (Config == null)
                return null;
            return Config.BottomKeeper[index];
        }

        public void OpenDrawer(int xPosition, int yPosition, bool locked)
        {
            mReader.Eject(xPosition, yPosition, locked);
        }

        public void CloseDrawer()
        {
            mReader.Load();
        }

        public V2Config Config { get { return ((FluoroNG)mReader).V2Config; } }
        public IDetectMethod GetMethodForOptimization(IDetectMethod method, string cartridge)
        {
            var ooptMethod = CopyOfDetectionMethod(method, cartridge);

            switch (ooptMethod.MeasTech)
            {
                case MeasureTech.Absorbance:
                    if (ooptMethod.IsWavelengthScan || (ooptMethod.MeasType & MeasureTypes.Multichromatic) != 0)
                    {
                        ooptMethod.MeasType = MeasureTypes.Monochromatic;
                        ooptMethod.Parameters[0].Value = "400";
                    }
                    if (ooptMethod is Apex.Device.Methods.IPathCheck && ((Apex.Device.Methods.IPathCheck)ooptMethod).PathCheck != null)
                        ((Apex.Device.Methods.IPathCheck)ooptMethod).PathCheck.Value = false;
                    break;
                case MeasureTech.BioLuminescenceRET:
                case MeasureTech.LuminescenceGlow:
                case MeasureTech.LuminescenceFlash:
                case MeasureTech.Luminescence:
                    if (ooptMethod.IsWavelengthScan)
                    {
                        ooptMethod.MeasType = MeasureTypes.Monochromatic;
                        ooptMethod.Parameters[0].Value = "1";
                    }
                    break;
                case MeasureTech.FluorescenceIntensityBottom:
                case MeasureTech.FluorescenceIntensityTop:
                    if (ooptMethod.MeasType == MeasureTypes.Multichromatic)
                    {
                        double ex1 = ooptMethod.Parameters[0];
                        double em1 = ooptMethod.Parameters[1];
                        ooptMethod.MeasType = MeasureTypes.Monochromatic;
                        ooptMethod.Parameters[0].Value = ex1;
                        ooptMethod.Parameters[1].Value = em1;
                    }
                    break;
            }
            return ooptMethod;
        }

        public bool IsConnected(out List<string> port)
        {
            return mGimliDetector.IsConnected(out port);
        }

        public ReaderStatus Initialize(String communicationName, int version)
        {
            try
            {
                Trace.WriteLine("ParadigmReader::Initialize().");

                if (mReader == null || mReader.CommDevice == null || !mReader.Connected || mReader.CommDevice.Port != communicationName)
                {
                    if (mReader != null)
                    {
                        mReader.StatusChanged -= ReaderOnStatusChanged;
                        mReader.Disconnect();
                    }
                    mReader = mGimliDetector.GetReaderObject(communicationName, version) as FluoroGimli;
                }

                if (mReader != null && mReader.CommDevice != null && mReader.CommDevice.State == DeviceStates.Online)
                {
                    mReader.Connect();
                    mReader.StatusChanged += ReaderOnStatusChanged;
                   
                    return ReaderStatus.Idle;
                }
            }
            catch (Apex.Extensibility.Device.DeviceException devExc)
            {
                Trace.WriteLine("Error on Connect: " + devExc.Message);

                if (devExc.Message == PropertieClass.GetText("InstrumentLockedError"))
                {
                    //Try to unlock instrument
                    ((ISupportTransportLocks)mReader).StartUnlockProcedure();

                    var locked = true;
                    try
                    {
                        locked = ((FluoroNG)mReader).TransportLocked;
                    }
                    catch (Exception e)
                    {
                    }

                    //return locked ? ReaderModel.ReaderStatus.Offline : CheckUpdateFW_PIC("SystemUpdater");
                    return locked ? ReaderStatus.Offline : ReaderStatus.Idle;
                }
            }
            return ReaderStatus.Offline;
        }

        private void ReaderOnStatusChanged(InstrumentStatusArgs args)
        {
            if (args.Status == ConnectionStatus.Offline)
                OnReaderWentOffline();
        }

        protected void OnReaderWentOffline()
        {
            if (ReaderWentOffline != null)
                ReaderWentOffline(this, new EventArgs());
        }

        public IDetectMethod CopyOfDetectionMethod(IDetectMethod original, string cartridge)
        {
            var possibleMethod = GetDetectMethod(cartridge, original.MeasTech);
            try
            {
                InitializeMethod(original, possibleMethod, true);
                return possibleMethod;
            }
            catch (Exception) { }
            return null;
        }

        private void InitializeMethod(IDetectMethod source, IDetectMethod destination, bool forceParameterAssignment)
        {
            destination.MeasType = source.MeasType;
            destination.DateCreated = source.DateCreated;
            destination.DateEdited = source.DateEdited;
            destination.UserCreated = source.UserCreated;
            destination.UserEdited = source.UserEdited;

            for (var i = 0; i < source.Parameters.Length; i++)
            {
                if (forceParameterAssignment)
                    destination.Parameters[i].ForceAssignment(source.Parameters[i].Value);
                else
                    destination.Parameters[i].Value = source.Parameters[i].Value;
            }
        }

        protected Shaker Shaker
        {
            get { return mReader.Shaker; }
        }

        public void AddCustomCartridges(bool isSimulation)
        {

        }
    }
}
