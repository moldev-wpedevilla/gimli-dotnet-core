﻿using System;
using System.Diagnostics;
using Apex.Extensibility.Interfaces;
using Gimli.InstrumentServer;
using MD.Nfc.Operation;
using MolDev.Common.Logger;

namespace Gimli.InstrumentController
{
    public class GimliTemperatureCtrl : ITemperatureController
    {
        private readonly ILogService mLogger;
        private const double MinTemp = 20;
        private const double MaxTemp = 80;
        private readonly ITemperizer mTemperizer;
        private double mTargetTemp = 24.0;
        private readonly object mSync = new object();
        private const int TemperaturePollingPeriodMillisec = 60000;
        private const int TemperatureFirstTimePollMillisec = 10000;
	    private readonly BackgroundWorker mTemperatureWorker;

        public static ITemperatureController Generate(ITemperizer temp, ILogService logger)
        {
            if(temp == null)
                throw new ArgumentException(nameof(temp));
            return new GimliTemperatureCtrl(temp, logger);
        }

        private GimliTemperatureCtrl(ITemperizer temp, ILogService logger)
        {
            mTemperizer = temp;
            mLogger = logger;
            MinValue = MinTemp;
            MaxValue = MaxTemp;
			mTemperatureWorker = new BackgroundWorker(CheckCurrentTemperatureTimerCallback, TemperatureFirstTimePollMillisec, true, MD.CommonComponents.Logging.LoggerFactory.Logger);
			mTemperatureWorker.Start();
        }

        public void SetTemperature(double targetTemperature)
        {
            lock (mSync)
            {
                Trace.WriteLine("SetTemperature(): Entering \"lock (mSync)\"");
                if (targetTemperature < MinValue || targetTemperature > MaxValue)
                    throw new ArgumentOutOfRangeException(nameof(targetTemperature));
                mTargetTemp = targetTemperature;
                try
                {
                    mTemperizer.SetTemp = (float)targetTemperature;
                    IsHeating = true;
                    //notify subsribe right away
                    OnCurrentValueUpdated(CurrentValue);     
                }
                catch (Exception e)
                {
                    mLogger.Error(LogCategoryEnum.Host, "SetTemperature failed due to: " + e.Message);
                    throw;
                }
                Trace.WriteLine("SetTemperature(): Leaving \"lock (mSync)\"");
            }
        }

        public double MinValue { get; }
        public double MaxValue { get; }
        public bool IsHeating { get; private set; }

        public void TurnOff()
        {
            lock (mSync)
            {
                Trace.WriteLine("TurnOff(): Entering \"lock (mSync)\"");
                try
                {
                    mTemperizer.SetTemp = 0;
                    IsHeating = false;
                    //notify subsribe right away
                    OnCurrentValueUpdated(CurrentValue);    
                }
                catch (Exception e)
                {
                    mLogger.Error(LogCategoryEnum.Host, "TurnTemperatureOff failed due to: " + e.Message);
                    throw;
                }
                Trace.WriteLine("TurnOff(): Leaving \"lock (mSync)\"");
            }
        }
        public double TargetValue => mTargetTemp;

        public double CurrentValue => mTemperizer.Current;

        public event EventHandler<TemperatureChangedEventArgs> CurrentValueUpdated;
        public void SetPollingStatus(bool pollingOn)
        {
            lock (mSync)
            {
                Trace.WriteLine("SetPollingStatus(): Entering \"lock (mSync)\"");
	            if (pollingOn)
	            {
		            mTemperatureWorker.Start();
	            }
	            else
	            {
                    mLogger.Debug(LogCategoryEnum.Host, "Stopping temperature polling.");
		            mTemperatureWorker.Stop();
	            }
                Trace.WriteLine("SetPollingStatus(): Leaving \"lock (mSync)\"");
            }
		}

        protected void OnCurrentValueUpdated(double newVal)
        {
            CurrentValueUpdated?.Invoke(this, new TemperatureChangedEventArgs(newVal, IsHeating));    
        }

        private int CheckCurrentTemperatureTimerCallback()
        {
            lock (mSync)
            {
                Trace.WriteLine("CheckCurrentTemperatureTimerCallback(): Entering \"lock (mSync)\"");
                var currentVal = TryGetTemperatureValues();
                Trace.WriteLine("CheckCurrentTemperatureTimerCallback(): Step 1");
                if (!double.IsNegativeInfinity(currentVal))
                {
                    Trace.WriteLine("CheckCurrentTemperatureTimerCallback(): Step 2");
                    OnCurrentValueUpdated(currentVal);
                }
                    
                Trace.WriteLine("CheckCurrentTemperatureTimerCallback(): Leaving \"lock (mSync)\"");
            }

	        return TemperaturePollingPeriodMillisec;
        }

        private double TryGetTemperatureValues()
        {
            try
            {
                //we must check if reader is heating - could be that SMP set the target from outside
                Trace.WriteLine("TryGetTemperatureValues(): Step 0");
                var readerTarget = mTemperizer.SetTemp;
                Trace.WriteLine("TryGetTemperatureValues(): Step 1");
                if (readerTarget > 0)
                {
                    Trace.WriteLine("TryGetTemperatureValues(): Step 2");
                    IsHeating = true;
                    mTargetTemp = readerTarget;
                }
                else
                {
                    Trace.WriteLine("TryGetTemperatureValues(): Step 3");
                    IsHeating = false;
                }

                Trace.WriteLine("TryGetTemperatureValues(): Step 4");
                return CurrentValue;
            }
            catch (ApplicationException e)//reader busy - ignore this update
            {
                Trace.WriteLine("TryGetTemperatureValues(): Step 5");
                mLogger.Error(LogCategoryEnum.Host, "CheckTemperature failed due to: " + e.Message);
                return double.NegativeInfinity;
            }
            catch (Exception e) //something wrong
            {
                Trace.WriteLine("TryGetTemperatureValues(): Step 6");
                mLogger.Error(LogCategoryEnum.Host, $"CheckTemperature failed due to: {e.Message}. Reporting 0.0°C");
                return 0.0;
            }
        }
    }
}
