﻿using System;
using System.Collections.Generic;
using System.Linq;
using Apex.Device;
using Apex.Extensibility.Interfaces;
using Gimli.InstrumentController.Multimode;
using Gimli.InstrumentServer;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Labware;
using MolDev.Common.Logger;
using ShakeIntensity = Gimli.JsonObjects.ShakeIntensity;

namespace Gimli.InstrumentController
{
    public class GimliOutsideShakeCtrl : IOutsideShakeController
    {
        private readonly IShaker mShaker;
        private readonly ILogService mLogger;
        private readonly IAdvancedTransportController mTransportController;

        public static IOutsideShakeController Generate(IShaker shaker, IAdvancedTransportController transportController, ILogService logger)
        {
            return new GimliOutsideShakeCtrl(shaker, transportController, logger);  
        }

        private GimliOutsideShakeCtrl(IShaker shaker, IAdvancedTransportController transportController, ILogService logger)
        {
            mShaker = shaker;
            mTransportController = transportController;
            mLogger = logger;
            ShakeModes = GetSupportedShakeModes(shaker.AllTypes);
            ShakeState = ShakeStates.Idle;
        }

        //public bool IsShaking => mShaker.IsShaking;

        public ShakeStates ShakeState { get; private set; }

        public List<ShakeIntensity> ShakeIntensities => new List<ShakeIntensity>() { ShakeIntensity.Low, ShakeIntensity.Medium, ShakeIntensity.High };

        public List<ShakeMode> ShakeModes { get; private set; } //new List<ShakeMode>() {ShakeMode.Linear, ShakeMode.Orbital};

        public void PauseShake()
        {
            StopShake(ShakeStopOptions.KeepPlateLocked);
            ShakeState = ShakeStates.Paused;
        }

        public void Resume(ShakeMode mode, ShakeIntensity intensity)
        {
            StartShake(string.Empty, mode, intensity);
        }

        public void StartShake(string plateFormat, ShakeMode mode, ShakeIntensity intensity)
        {
            try
            {
                mLogger.Info(LogCategoryEnum.Host, "Starting the shake operation");
                if (ShakeState == ShakeStates.Idle)
                {
                    mTransportController.LoadPlate(false);
                    mTransportController.TogglePlateDrawer(true);
                }
                mShaker.Type = ShakeConverter.GetShakeType(mode);
                mShaker.Intensity = ShakeConverter.GetShakeIntensity(intensity);
                mShaker.Duration = 360;
                mShaker.OutsideShaking = true;
                SetLabwareFromGivenFormat(plateFormat);
                mShaker.Shake(true);
                mShaker.OutsideShaking = false;
                ShakeState = ShakeStates.Shaking;
            }
            catch (Exception e)
            {
                mLogger.Error(LogCategoryEnum.Host, "StartShake() failed due to: " + e.Message);
                throw;
            }
        }

        private void SetLabwareFromGivenFormat(string plateFormat)
        {
            int format;
            if(!int.TryParse(GetWellNumber(plateFormat), out format))
                return;
            var plate = PlateLibrary.MicroplateLibrary.GetPlates(format).FirstOrDefault();
            if(plate == null)
                return;
            var lab = MicroplateConverter.GenerateLabwareFromMicroplate(plate);
            if(lab != null)
                mShaker.OnLabwareChanged(lab);
        }

        private string GetWellNumber(string plateFormat)
        {
            var textBegin = plateFormat.IndexOf(' ');
            return textBegin != -1 ? plateFormat.Substring(0, textBegin) : string.Empty;
        }

        private List<ShakeMode> GetSupportedShakeModes(ShakeTypes allTypes)
        {
            return new List<ShakeMode>() {ShakeMode.Linear, ShakeMode.Orbital, ShakeMode.DoubleOrbital};
            //return (from ShakeTypes value in Enum.GetValues(typeof (ShakeTypes)) where (value & allTypes) != 0 select GetShakeMode(value)).ToList();
        }

        public void StopShake(ShakeStopOptions lockOption)
        {
            try
            {
                mLogger.Info(LogCategoryEnum.Host, "Stopping the shake operation");
                mShaker.StopShaking();
                //TODO: temp solution until new fw command is implemented
                if (lockOption == ShakeStopOptions.CloseDrawer || lockOption == ShakeStopOptions.ReleasePlate)
                {
                    mTransportController.LoadPlate(false);    
                }
                if (lockOption == ShakeStopOptions.ReleasePlate)
                {
                    mTransportController.EjectUnLockedPlate();
                }
                ShakeState = ShakeStates.Idle;
            }
            catch (Exception e)
            {
                mLogger.Error(LogCategoryEnum.Host, "StopShake() failed due to: " + e.Message);
                throw;
            }
        }
    }
}
