﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using Apex.Device;
using Apex.Extensibility.Device;
using Gimli.Instrument.Common;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.Instrument;
using Gimli.InstrumentServer.GimliDevice;
using Gimli.JsonObjects;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;
using MolDev.Common.Messages;

namespace Gimli.InstrumentController.GimliDevice
{
    internal class GimliDeviceOutboundController : IGimliDeviceOutboundController
    {
        private readonly ILogService mLogger;
        private readonly IActorService mActorService;
        private readonly IGimliDeviceHelper mHelper;

        private int mNoOfResponsesSoFar;
        private int mNoOfResponses;
        private DateTime mStartTime;
        private int mTimeOut;
        private ManualResetEventSlim mReadDonEvent;
        private IAdvancedDirectCommunication mDirectComm;
        
        public static GimliDeviceOutboundController Generate(ILogService logger, IActorService actorService, IGimliDeviceHelper helper)
        {
            return new GimliDeviceOutboundController(logger, actorService, helper);
        }
        protected GimliDeviceOutboundController(ILogService logger, IActorService actorService, IGimliDeviceHelper helper)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            mLogger = logger;
            mActorService = actorService;
            mHelper = helper;
        }

        private bool IsPlateClosingWithHeightMeasuring()
        {
            bool result;
            try
            {
	            result = !mHelper.GimliReader.GeTransportController().CameraFunctions.CameraLedIsOff.IsSet;
            }
            catch (Exception exc)
            {
                mLogger.Debug(LogCategoryEnum.Uncategorized, $"{exc.Message} - {exc.InnerException?.Message}", "");
                throw;
            }           
            return result;
        }

        private bool IsSlideLoading()
        {
            bool isSlideLoading = false;

            if (mHelper.IsFilterReady)
            {
                // 1st phase
                isSlideLoading = !mHelper.FilterSlidersProcessingHasBeenFinished.IsSet;
                mLogger.Debug(LogCategoryEnum.Uncategorized, $"IsSlideLoading(): Phase 1 -- isSlideLoading: {isSlideLoading}", "");

                // 2nd phase (if necessary)
                if (!isSlideLoading)
                {
                    // Check if one piece is loading pushed manually
                    isSlideLoading = mHelper.FilterSliderLoadingIsInProgress.IsSet ;   // TODO Brane To be replaced with "FilterSliderLoading"
                    //isSlideLoading = mHelper.FilterSliderLoadingIsInProgress.IsSet;         
                    mLogger.Debug(LogCategoryEnum.Uncategorized, $"IsSlideLoading(): Phase 2 -- isSlideLoading: {isSlideLoading} )", "");
                }
            }
            return isSlideLoading;
        }

        #region interface
        public void ExecuteFirmwareCommand(int requestId, string command, int noOfResp, int timeout,
            CancellationToken cancellationToken)
        {
            mLogger.Info(LogCategoryEnum.Host, $"ExecuteFirmwareCommand: enter (command {command}).");
            Trace.WriteLine($"GimliDeviceOutboundController::ExecuteFirmwareCommand() command {command}");
            var isPlateClosing = IsPlateClosingWithHeightMeasuring();
            var isSlideLoading = IsSlideLoading();

            if (isPlateClosing || isSlideLoading)
            {
                ActAsBusyOnPlateClosingOrSlideLoading(isPlateClosing, isSlideLoading);
            }

            try
            {
                mHelper.DisableTempPollingAndNfc();

                if (command.StartsWith("REMOTE_"))
                {
                    ExecuteDeviceMethod(requestId, command, noOfResp, timeout, cancellationToken);
                    return;
                }

                mNoOfResponsesSoFar = 0;
                mNoOfResponses = noOfResp;
                mStartTime = DateTime.Now;
                mTimeOut = timeout;
                mReadDonEvent = new ManualResetEventSlim();
                mDirectComm = mHelper.GimliReader.GetDirectCommunication();
                mDirectComm.ResponseReceived += (s, e) => ResponseEvent(s, e, requestId);
                //if the current command is just to get the INFO string (read done) and we already received the last response
                if (command == string.Empty && mDirectComm.UndeliveredResponse.Contains("INFO:"))
                {
                    mLogger.Info(LogCategoryEnum.Host, "ExecuteFirmwareCommand: INFO response is already there");
                    //no need to send - just 
                    Task.Run(
                        () => ResponseEvent(this, new ResponseReceivedArgs(mDirectComm.UndeliveredResponse), requestId));
                }
                else
                {
                    mDirectComm.SendCommand(command, noOfResp, timeout);
                    Task.Run(() => GimliDeviceHelper.CheckExeFwTimeout(mDirectComm, mTimeOut, cancellationToken, mStartTime, mReadDonEvent, mLogger));
                }
                mReadDonEvent.Wait();
            }
            finally
            {
                Trace.WriteLine($"ExecuteFirmwareCommand(): Before calling EnableTempPollingAndNfc() command {command} requestId {requestId} noOfResp {noOfResp} timeout {timeout}");
                mHelper.EnableTempPollingAndNfc();
            }

            mLogger.Info(LogCategoryEnum.Host, "ExecuteFirmwareCommand: leave");
        }

        private void ActAsBusyOnPlateClosingOrSlideLoading(bool isPlateClosing, bool isSlideLoading)
        {
            if (isPlateClosing)
            {
                mLogger.Debug(LogCategoryEnum.Uncategorized, "ActAsBusyOnPlateClosingOrSlideLoading(): Exiting as PlataClosingComplex operation has been being performed...  ", "");
                throw new DeviceException(Properties.Resources.ResourceManager.GetString("Busy_PlateClosing"), string.Empty, "144", 0);
            }

            if (isSlideLoading)
            {
                mLogger.Debug(LogCategoryEnum.Uncategorized, "ActAsBusyOnPlateClosingOrSlideLoading(): Exiting as SlideLoading operation has been being performed...  ", "");
                throw new DeviceException(Properties.Resources.ResourceManager.GetString("Busy_SlideLoading"), string.Empty, "144", 0);
            }
        }

        public void ExecuteCommand<T>(T message) where T : RequestMessageBase
        {
            mActorService.Get<DataServiceProxyActor>().Tell(message);
        }
        #endregion

        public void ResponseEvent(object sender, ResponseReceivedArgs args, int requestId)
        {
            mNoOfResponsesSoFar++;

            mActorService.Get<NetworkCommunicationServiceProxyActor>()
                .Tell(MessageExtensions.CreateRequestMessage(new DataeventMessage { Payload = args.ResponseMessage }));

            if (args.ResponseMessage.StartsWith("-"))
            {
                mReadDonEvent.Set();

                // TODO Calling/Signaling action like in the case of HOOD_OPENED device request.
                //if (args.ResponseMessage.Contains("E56"))
                //{
                //    mHelper.ActOnHoodOpenDeviceRequest();
                //}
            }
            else if (mNoOfResponsesSoFar == mNoOfResponses)
            {
                mReadDonEvent.Set();
            }
        }

        private void ExecuteDeviceMethod(int requestId, string command, int noOfResp, int timeout, CancellationToken cancellationToken)
        {
            mLogger.Info(LogCategoryEnum.Host, "ExecuteDeviceMethod: enter");
            mNoOfResponsesSoFar = 0;
            mNoOfResponses = noOfResp;
            mStartTime = DateTime.Now;
            mTimeOut = timeout;
            mReadDonEvent = new ManualResetEventSlim();
            var response = string.Empty;
            command = command.Replace("REMOTE_", "");

            var tpController = mHelper.GimliReader.GeTransportController();
            if (tpController != null)
            {
                if (command.StartsWith("LOADPLATECOMMAND"))
                {
                    var readPhAndBc = command.Split(' ').Length > 1 &&
                                      command.Split(' ')[1].ToLowerInvariant().StartsWith("true");
                    tpController.LoadPlate(readPhAndBc);
                }
                else if (command.StartsWith("TOGGLEPLATELOCKEDCOMMAND"))
                {
                    tpController.TogglePlateDrawer(true);
                }
                else if (command.StartsWith("TOGGLEPLATEUNLOCKEDCOMMAND"))
                {
                    tpController.TogglePlateDrawer(false);
                }
                //else if (command.StartsWith("READPLATEHEIGHT"))
                //{
                //    response = tpController.CameraFunctions.ReadPlateHeight().ToString(CultureInfo.InvariantCulture);
                //}
                else if (command.StartsWith("GETPLATEHEIGHT"))
                {
                    response = tpController.CameraFunctions.PlateHeight().ToString(CultureInfo.InvariantCulture);
                }
                else if (command.StartsWith("GETBARCODE"))
                {
                    response = tpController.CameraFunctions.Barcode();
                }
                else if (command.StartsWith("CANREADBARCODE"))
                {
                    response = tpController.CameraFunctions.CanReadBarcode.ToString();
                }
                else if (command.StartsWith("SETCANREADBARCODE"))
                {
                    tpController.CameraFunctions.CanReadBarcode = command.Contains("TRUE");
                }
                else if (command.StartsWith("CANREADPLATEHEIGHT"))
                {
                    response = tpController.CameraFunctions.CanReadPlateHeight.ToString();
                }
                else if (command.StartsWith("SETCANREADPLATEHEIGHT"))
                {
                    tpController.CameraFunctions.CanReadPlateHeight = command.Contains("TRUE");
                }
                else if (command.StartsWith("GETSLIDESCONFIG"))
                {
                    var slideConfig = mHelper.GetFilterConfiguration().StoredFilterInfo.CurrentSlides;
                    response = slideConfig.SlideEx.JsonSerializeToCamelCase() + SpecialMeaningsStrings.Delimiter + slideConfig.SlideEm.JsonSerializeToCamelCase();
                }
            }

            var gxpController = mHelper.GimliReader.GetGxPController();
            if (gxpController != null)
            {
                if (command.StartsWith("GXP_ISLOCKED"))
                {
                    response = gxpController.IsLocked.ToString();
                }
            }

            mDirectComm = mHelper.GimliReader.GetDirectCommunication();
            mDirectComm.ResponseReceived += (s, e) => ResponseEvent(s, e, requestId);

            ResponseEvent(this, new ResponseReceivedArgs("+ " + response), requestId);

            Task.Run(() => GimliDeviceHelper.CheckExeFwTimeout(mDirectComm, mTimeOut, cancellationToken, mStartTime, mReadDonEvent, mLogger));
            mReadDonEvent.Wait();

            mLogger.Info(LogCategoryEnum.Host, "ExecuteDeviceMethod: leave");
        }
	}
}
