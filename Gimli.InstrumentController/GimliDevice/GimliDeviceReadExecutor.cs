﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Gimli.InstrumentServer;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Serialize;
using Gimli.Shared.Actors.Messages.Instrument;
using Gimli.InstrumentServer.Actors;
using Gimli.InstrumentServer.GimliDevice;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;

namespace Gimli.InstrumentController.GimliDevice
{
    internal class GimliDeviceReadExecutor : IGimliDeviceReadExecutor
    {
        private readonly ILogService mLogger;
        private readonly IActorService mActorService;
        private IGimliDeviceHelper mHelper;

        private ManualResetEventSlim mReadDonEvent;
        private ManualResetEventSlim mIgnorePlateheightError = new ManualResetEventSlim();
        private Document mDocument;
        private int mRequestId;
        private GimliRead mRead;

        public static GimliDeviceReadExecutor Generate(ILogService logger, IActorService actorService, IGimliDeviceHelper helper)
        {
            return new GimliDeviceReadExecutor(logger, actorService, helper);
        }

        protected GimliDeviceReadExecutor(ILogService logger, IActorService actorService, IGimliDeviceHelper helper)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            mLogger = logger;
            mActorService = actorService;
            mHelper = helper;
        }

        #region interface
        public void TogglePlateHeightMeasurement(bool active)
        {
            if (mHelper.GimliReader.GeTransportController() != null)
                mHelper.GimliReader.GeTransportController().CameraFunctions.CanReadPlateHeight = active;
        }

        public void ToggleBarcodeMeasurement(bool active)
        {
            if (mHelper.GimliReader.GeTransportController() != null)
                mHelper.GimliReader.GeTransportController().CameraFunctions.CanReadBarcode = active;
        }

        public void ExecuteRead(int requestId, Document document, CancellationToken cancellationToken)
        {
            mLogger.Info(LogCategoryEnum.Host, "ExecuteRead: enter");
            mReadDonEvent = new ManualResetEventSlim();
            mDocument = document;
            mRequestId = requestId;
	        mIgnorePlateheightError = new ManualResetEventSlim();

			Task.Run(() => StartRead(cancellationToken));

            mReadDonEvent.Wait();
            mLogger.Info(LogCategoryEnum.Host, "ExecuteRead: leave");
        }

	    public void IgnorePlateheightError()
	    {
		    mIgnorePlateheightError.Set();
	    }

	    #endregion

        private void StartRead(CancellationToken cancellationToken)
        {
            try
            {
                Trace.WriteLine($"GimliDeviceReadExecutor::StartRead(): Before calling DisableTempPollingAndNfc()");
                mHelper.DisableTempPollingAndNfc();

                var plt = mDocument.Experiments.First().Plates.First();
                mRead = GimliRead.SetupNewRead(plt, 0, mHelper.GimliReader, mLogger, mActorService);
                mRead.ReadStatusChanged += ReadOnReadStatusChanged;
                mRead.TemperatureChanged += ReadOnTemperatureChanged;
                mRead.SetDataReceiver(mRequestId, mDocument);
                mRead.StartMethod(cancellationToken, mIgnorePlateheightError);
            }
            catch (Exception e)
            {
                HandleOnError(e.Message);
            }
		}

        private void HandleOnError(string message)
        {
            Trace.WriteLine("StartRead: " + message);
            mLogger.Error(LogCategoryEnum.Host, "Read error: " + message);

            var serialization = DocumentAndSerializedWells.SerializeDocument(mDocument);
            var firstSnapshotKey = serialization.GetDictionaryKeyOfFirstSnapshot();

            var payload = new RuntimeData
            {
                Document = serialization.Document,
                Status = MeasurementStatus.ErrorOccurred,
                RemainingTime = 0.0,
                MeasuredWells = new Point[0],
                ErrorMessage = message,
                WellSeriailization = !serialization.WellSeriailization.ContainsKey(firstSnapshotKey) ? null : serialization.WellSeriailization[firstSnapshotKey],
                WellSeriailizationSize = !serialization.WellSeriailizationSize.ContainsKey(firstSnapshotKey) ? 0 : serialization.WellSeriailizationSize[firstSnapshotKey],
            };

            mActorService.Get<InstrumentServerRoutingActor>().Tell(new DataeventMessage { Payload = payload });

            Thread.Sleep(2000);

            FinalizeRead();
        }

	    private void FinalizeRead()
	    {
		    mReadDonEvent.Set();

		    mHelper.EnableTempPollingAndNfc();

		    mActorService.Get<InstrumentServerRoutingActor>().Tell(new DataeventMessage {IsFinalEvent = true});
	    }

	    private void ReadOnReadStatusChanged(ReaderStatus status, string message)
        {
            switch (status)
            {
                case ReaderStatus.Idle:
                    FinalizeRead();
                    break;
                case ReaderStatus.Error:
                    HandleOnError(message);
                    break;
            }
        }

        private void ReadOnTemperatureChanged(object sender, TemperatureChangedEventArgs temperatureChangedEventArgs)
        {
            mHelper.TemperatureControllerOnCurrentValueUpdated(this, new TemperatureChangedEventArgs(temperatureChangedEventArgs.Current, mHelper.TemperatureController?.IsHeating ?? false));
        }
    }
}
