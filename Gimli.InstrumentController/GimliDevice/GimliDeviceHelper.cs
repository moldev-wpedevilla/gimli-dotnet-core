﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Apex.Device;
using Gimli.InstrumentServer;
using Gimli.InstrumentServer.GimliDevice;
using Gimli.JsonObjects;
using Gimli.JsonObjects.FiltersMngmnt;
using Gimli.JsonObjects.InjectorMaintenance;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.FilterMngmnt;
using Gimli.Shared.Actors.Messages.Instrument;
using MD.Nfc.Basic.StrTagConverting;
using MD.Nfc.Common.StrTagManipulation;
using MD.Nfc.Common.TagConnecting;
using MD.Nfc.Operation.FilterPosMngmnt;
using MD.Nfc.Operation.NfcTerminal;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;
using Newtonsoft.Json;

namespace Gimli.InstrumentController.GimliDevice
{
    public class GimliDeviceHelper : IGimliDeviceHelper
    {
		private readonly ILogService mLogger;
        private readonly IActorService mActorService;
        private IInstrumentContext mInstrumentContext;
	    private bool mIsFilterReady;
	    private bool mIsLumBottomReady;


        public GimliDeviceHelper(ILogService logger, IActorService actorService, IInstrumentContext instrumentContext)
        {
            mLogger = logger;
            mActorService = actorService;
            mInstrumentContext = instrumentContext;

            FilterSlidersProcessingHasBeenFinished = new ManualResetEventSlim();
            FilterSliderLoadingIsInProgress = new ManualResetEventSlim();
            mLogger.Debug(LogCategoryEnum.Host, "=== SLIDER LOADING... GimliDeviceHelper(): FilterSliderHasBeenInserted, FilterSlidersProcessingHasBeenFinished, FilterSliderLoadingIsInProgress created and set");

            NfcPairingRunsWaiter = new ManualResetEventSlim();
            IsNfcTagReadWaiter = new ManualResetEventSlim();
        }

		public Guid TagGuid { get; set; }
        public SpinnerController SpinnercontrolerForFilterMovement { get; set; }
        public ManualResetEventSlim NfcPairingRunsWaiter { get; set; }
        public ManualResetEventSlim IsNfcTagReadWaiter { get; set; }
        public ManualResetEventSlim FilterSlidersProcessingHasBeenFinished { get; }
        public ManualResetEventSlim FilterSliderLoadingIsInProgress { get; }
        public IOutsideShakeController OutsideShakeController { get; set; }
		public IInjectorMaintenanceController InjectorMaintenance { get; set; }
		public IFilterController FilterController { get; set; }
	    public ITemperatureController TemperatureController { get; set; }
	    public IStrTagManipulator StrTagManipulator { get; set; }
        public ITagConnector TagConnector { get; set; }         // Brane temp
		public INfcTerminalMngr NfcTerminalMngr { get; set; }
		public IFilterPosMngr FilterPosMngr { get; set; }
		public FluoroGimli GimliReader { get; set; }
		public bool IsSimulated { get; set; }
	    public bool IsInitialized { get; set; }
	    public string ReaderResponse { get; set; }
        public static FilterSlidesCubeMemory FilterSlidesCubeMemory { get; set; }

	    public bool IsFilterReady
	    {
		    get { return mIsFilterReady; }
		    set
		    {
			    mIsFilterReady = value;
				mActorService.Get<WebServiceProxyActor>().Tell(
					MessageExtensions.CreateRequestMessage(new FilterConfigurationChangedMessage
					{
						IsFilterReady = value
					}));
			}
	    }

		public bool IsLumBottomReady
		{
			get { return mIsLumBottomReady; }
			set
			{
				mIsLumBottomReady = value;
			}
        }

        private void SetSignalsAfterSliderLoaded()
        {
            FilterSliderLoadingIsInProgress.Reset();
        }

        private void SetSignalsBeforeSliderLoaded()
        {
            FilterSliderLoadingIsInProgress.Set();
        }

        public void LoadFilterSlide(SlideSel slider, CancellationToken token)
	    {
		    if (IsSimulated)
		    {
		        SetSignalsAfterSliderLoaded();
                mLogger.Debug(LogCategoryEnum.Host, $"=== SLIDER LOADING... LoadFilterSlide(): (SIMULATION MODE) Signalling END of slider {(SlideSel.SlideA == slider ? "A" : "B")} loading... FilterSliderLoadingIsInProgress reset");
                return;
		    }

            DisableTempPollingAndNfc();
            SetSignalsBeforeSliderLoaded();
            mLogger.Debug(LogCategoryEnum.Host, $"=== SLIDER LOADING... LoadFilterSlide(): Signalling START of slider {(SlideSel.SlideA == slider ? "A" : "B")} loading (FilterSliderLoadingIsInProgress set)");

            GimliReader?.GeTransportController()?.CameraFunctions?.CameraLedIsOff.Wait();
            FilterController.LoadSlider(slider, token);
	    }

        public void ToggleFilterSlide(SlideSel slide, CancellationToken token)
        {
            //if (IsSimulated)
            //{
            //    FilterSlideInserted.Set();
            //    mLogger.Debug(LogCategoryEnum.Uncategorized, $"ToggleFilterSlide(): FilterSlideInserted should be set within if (IsSimulated): FilterSlideInserted.IsSet {FilterSlideInserted.IsSet}", "");
            //    return;
            //}

            //FilterSlideInserted.Reset();
            //mLogger.Debug(LogCategoryEnum.Uncategorized, $"LoadFilterSlide(): FilterSlideInserted should be reset before .LoadSlide(): FilterSlideInserted.IsSet {FilterSlideInserted.IsSet}", "");
            //FilterController.ToggleSlide(slide, token);
        }
        
        public Dictionary<InjectorSel, DoubleValueWithMaximum> GetTubingStore()
        {
            var filename = GetInjectorTubingStoreName();
            if (!File.Exists(filename))
            {
                File.WriteAllText(filename, new Dictionary<InjectorSel, DoubleValueWithMaximum>
                {
                    {InjectorSel.Injector1, new DoubleValueWithMaximum {Value = 0, Maximum = 100000}},
                    {InjectorSel.Injector2, new DoubleValueWithMaximum {Value = 0, Maximum = 100000}}
                }.JsonSerializeToCamelCase());
            }

            return JsonConvert.DeserializeObject<Dictionary<InjectorSel, DoubleValueWithMaximum>>(File.ReadAllText(filename));
        }

        public string GetInjectorTubingStoreName()
        {
            return Anthos.ResourceReader.DirectoryLookup.GetTemplateDir + "SimulatorTubingVoumes.txt";
        }

        public List<InjectorConfiguration> GetInjectorConfiguration()
        {
            if (IsSimulated)
            {
                return new List<InjectorConfiguration>
                    {
                        GetStandardInjectorConfig(InjectorSel.Injector1),
                        GetStandardInjectorConfig(InjectorSel.Injector2)
                    };
            }
            if (GimliReader.V2Config.Features.HasFlag(FluoroFeatures.Dispenser))
            {
                return new List<InjectorConfiguration>
                {
                    GetStandardInjectorConfig(InjectorSel.Injector1),
                    GetStandardInjectorConfig(InjectorSel.Injector2)
                };
            }
            return new List<InjectorConfiguration>();
        }

        private InjectorConfiguration GetStandardInjectorConfig(InjectorSel name)
        {
	        const double expectedCalibrationWeight = 4.8;

	        return new InjectorConfiguration
	        {
		        Name = name,
		        ManualRange = new DoubleValueWithRange {Value = 300, Minimum = 1, Maximum = 2000, Step = 1},
		        CalibrationDispenseRange =
			        new DoubleValueWithRange
			        {
						// setting invalid default value so user is forced to enter a different value to be able to proceed to the next calibration step
				        Value = 0.0,
						// this means +/- 20%
						Minimum = expectedCalibrationWeight - (expectedCalibrationWeight * 0.2),
				        Maximum = expectedCalibrationWeight + (expectedCalibrationWeight * 0.2),
				        Step = 0.001,
			        },
		        CalibrationVerifyRange =
			        new DoubleValueWithRange
			        {
						// setting invalid default value so user is forced to enter a different value to be able to proceed to the next calibration step
				        Value = 0.0,
						// this means +/- 5%
						Minimum = expectedCalibrationWeight - (expectedCalibrationWeight * 0.05),
				        Maximum = expectedCalibrationWeight + (expectedCalibrationWeight * 0.05),
				        Step = 0.001
					},
		        PrimeVolume = 300,
		        WashVolume = 900,
		        VoidVolume = 300,
		        Unit = "µl",
				WeightUnit = "g"
	        };
        }

        public void PrepareInjectorMaintenance()
        {
            if (null == InjectorMaintenance && !IsSimulated)
            {
                InjectorMaintenance = Gimli.InstrumentController.InjectorMaintenance.InjectorMaintenance.Generate(mLogger, GimliReader.GetDirectCommunication, mActorService, this);
            }

            DisableTempPollingAndNfc();
        }

        public void PrepareFiltersMngmnt()
        {
            if (null == FilterController && !IsSimulated)
            {
                FilterController = FilterMngmnt.FilterController.Generate(mLogger, GimliReader.GetDirectCommunication, mActorService, this);
            }
            //DisableTempPollingAndNfc();
        }

        public void EnableTempPollingAndNfc()
        {
            InitTemperatureController();
            TemperatureController?.SetPollingStatus(true);
            GimliReader.GetNfcAccessor().NfcTerminalMngr?.Start();
        }

        public void DisableTempPollingAndNfc()
        {
            TemperatureController?.SetPollingStatus(false);
            GimliReader.GetNfcAccessor().NfcTerminalMngr?.Stop();
        }

        public bool IsDisabled_TempPollingAndNfc()
        {
            bool isDisabled = !GimliReader.GetNfcAccessor().NfcTerminalMngr.IsNfcPollingRunning;
            return isDisabled;
        }

        public void TemperatureControllerOnCurrentValueUpdated(object sender, TemperatureChangedEventArgs temperatureChangedEventArgs)
        {
            var settings = new TemperatureNotification
            {
                Current = temperatureChangedEventArgs.Current,
                IsControllerRunning = temperatureChangedEventArgs.IsHeating
            };

            mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new TemperatureChangedMessage { Settings = settings }));
        }


		public void InitTemperatureController()
		{
			if (TemperatureController != null) return;

			TemperatureController = GimliTemperatureCtrl.Generate(GimliReader.TempControl, mLogger);
			TemperatureController.CurrentValueUpdated += TemperatureControllerOnCurrentValueUpdated;
		}

        public FilterModuleConfig GetFilterConfiguration()
        {
			return new FilterModuleConfig {IsFilterReady = IsFilterReady, StoredFilterInfo = FilterSlidesCubeMemory};
		}

		public static void CheckExeFwTimeout(IAdvancedDirectCommunication directComm, int timeoutInSecs, CancellationToken cancellationToken, DateTime startTime, ManualResetEventSlim commandDone, ILogService logger)
        {
            do
            {
	            try
	            {
					Thread.Sleep(100);
					if (commandDone.IsSet) break;
					if (!cancellationToken.IsCancellationRequested) continue;
					directComm.Cancel();
					break;
				}
	            catch (Exception e)
	            {
					logger.Error(LogCategoryEnum.Host, "Exception while aborting instrument command: " + e);
		            break;
	            }
            } while (DateTime.Now - startTime < TimeSpan.FromSeconds(timeoutInSecs));
        }

		internal static string GetFilterConfigPath(bool isSimulated)
		{
			const string filterConfigPath = @"C:\ProgramData\Multimode\Detection Software\FilterConfig.xml";
			const string simulatedFilterConfigPath = @"C:\ProgramData\Multimode\Detection Software\SimFilterConfig.xml";

			return isSimulated ? simulatedFilterConfigPath : filterConfigPath;
		}

		internal static FilterSlidesCubeMemory ReadFilterInfoFromFileSystem(Action<string> errorLogger, bool isSimulated)
		{
			var serializer = new XmlSerializer(typeof(FilterSlidesCubeMemory));
			var filterConfigPath = GetFilterConfigPath(isSimulated);

			try
			{
				FilterSlidesCubeMemory storedInfo;
				using (var textReader = new StreamReader(new FileStream(filterConfigPath, FileMode.OpenOrCreate)))
				{
					storedInfo = (FilterSlidesCubeMemory)serializer.Deserialize(textReader);
				}

				FilterSlidesCubeMemory = storedInfo;

				return storedInfo;
			}
			catch (Exception e)
			{
				errorLogger("Exception caught opening filter config " + e);
			}

			// Deserialization failed -> delete old file and write a new one
			File.Delete(filterConfigPath);
			var newData = new FilterSlidesCubeMemory
			{
				CubeMemory = new List<FilterTagData>(),
				CurrentSlides = new CurrentFilterSlides()
			};

			WriteObjectToXml(newData, isSimulated);

			FilterSlidesCubeMemory = newData;

			return newData;
		}

		private static void WriteObjectToXml(FilterSlidesCubeMemory storedInfo, bool isSimulated)
		{
			using (var textWriter = new XmlTextWriter(GetFilterConfigPath(isSimulated), Encoding.Unicode))
			{
				new XmlSerializer(typeof(FilterSlidesCubeMemory)).Serialize(textWriter, storedInfo);
			}
		}

		internal static void StoreFilterData(string slide, SliderTagData slideTag, bool isSimulated, Action<string> errorLogger)
		{
			var storedInfo = ReadFilterInfoFromFileSystem(errorLogger, isSimulated);
			if (slide == "A")
			{
				storedInfo.CurrentSlides.SlideEx = slideTag;
			}
			else
			{
				storedInfo.CurrentSlides.SlideEm = slideTag;
			}

			///////////////////////////////////////////////////
			// DO NOT REMOVE: maybe usefull for later release
			///////////////////////////////////////////////////
			//if (slideTag != null)
			//{
			//	foreach (var filterTagData in slideTag.Filter)
			//	{
			//		var filterWithSameName = storedInfo.CubeMemory.FirstOrDefault(c => c.Name == filterTagData.Name);
			//		if (filterWithSameName != null)
			//		{
			//			storedInfo.CubeMemory.RemoveAt(storedInfo.CubeMemory.IndexOf(filterWithSameName));
			//		}

			//		storedInfo.CubeMemory.Add(filterTagData);
			//	}
			//}

			WriteObjectToXml(storedInfo, isSimulated);

			FilterSlidesCubeMemory = storedInfo;
		}

        public void TagReadHandler(object source, Guid guid)
        {
            var nfcPairingRunsInitialTrial = NfcPairingRunsWaiter.IsSet;
            TagGuid = guid;
            IsNfcTagReadWaiter.Set();

            mLogger.Info(LogCategoryEnum.Uncategorized, $"UnknownTagReadEventHandler: tagGuid ... {TagGuid}, mNfcPairingRuns ... {NfcPairingRunsWaiter.IsSet}");
            if (!nfcPairingRunsInitialTrial && !NfcPairingRunsWaiter.IsSet)
            {
                mActorService.Get<DataServiceProxyActor>()
                    .Tell(
                        MessageExtensions.CreateRequestMessage(new LoginUserMessage
                        {
                            Id = TagGuid,
                            Type = LoginIdType.Nfc
                        }));
            }
        }

        public async void UnknownTagReadEventHandler(object source, UnknownTagReadEventArgs ev)
        {
            await Task.Run(() =>
            {
                TagReadHandler(source, ev.ChipGuid);
            });
        }

        public async void FilterTagReadEventHandler(object source, FilterTagReadEventArgs ev)
        {
            await Task.Run(() =>
            {
                mLogger.Debug(LogCategoryEnum.Host, "Filter cube detected.");
                mActorService.Get<WebServiceProxyActor>()
                    .Tell(
                        MessageExtensions.CreateRequestMessage(new FrontNfcFilterCubeMessage
                        {
                            Cube = ev.FilterData
                        }));
            });
        }

        public async void SlideTagReadEventHandler(object source, SlideTagReadEventArgs ev)
        {
            // For this release!!! We present only filter tag with Front NFC detection. Passing Slide tag from filter carrier works not always because of distance.

            //await Task.Run(() =>
            //{
            //    mActorService.Get<WebServiceProxyActor>()
            //        .Tell(
            //            MessageExtensions.CreateRequestMessage(new FrontNfcSlideMessage
            //            {
            //                Slide = ev.SlideData
            //            }));
            //});
        }

        public async void CardTagReadEventHandler(object source, CardTagReadEventArgs ev)
        {
            await Task.Run(() =>
            {
                TagReadHandler(source, ev.ChipGuid);
            });
        }

        public async void LinkTagReadEventHandler(object source, LinkTagReadEventArgs ev)
        {
            await Task.Run(() =>
            {
                TagReadHandler(source, ev.ChipGuid);
            });
        }

        public async void PhoneTagReadEventHandler(object source, PhoneTagReadEventArgs ev)
        {
            await Task.Run(() =>
            {
                TagReadHandler(source, ev.ChipGuid);
            });
        }

        public async void ProtocolTagReadEventHandler(object source, ProtocolTagReadEventArgs ev)
        {
            await Task.Run(() =>
            {
                TagReadHandler(source, ev.ChipGuid);
            });
        }

        public void ToggleNfcTagReadWaiter()
        {
            if (IsNfcTagReadWaiter.IsSet)
            {
                IsNfcTagReadWaiter.Reset();
            }
            else
            {
                IsNfcTagReadWaiter.Set();
            }
        }

        public void ActOnSliderLoaded(string slideSel, SliderTagData slide, FilterTagData[] filter)
        {
            Thread.Sleep(5000);
            mInstrumentContext.State.State = DeviceState.Idle;
            if (SpinnercontrolerForFilterMovement != null)
            {
                SpinnercontrolerForFilterMovement.HiddeSpinner();
                SpinnercontrolerForFilterMovement.Dispose();
                SpinnercontrolerForFilterMovement = null;
            }

            StoreAndNotify(slideSel, slide, filter);
            SetSignalsAfterSliderLoaded();
            mLogger.Debug(LogCategoryEnum.Host, $"=== SLIDER LOADING... ActOnSliderLoaded(): Slider {slideSel} has been loaded (FilterSlider{slideSel}HasBeenInserted) (FilterSliderHasBeenInserted is null? : A or B)");

            if ("B" == slideSel /* && A was already loaded!!! i.e. it is HOOD_CLOSED reaction */)           // 
            {
                FilterSlidersProcessingHasBeenFinished.Set();
                mLogger.Debug(LogCategoryEnum.Host, $"=== SLIDER LOADING... ActOnSliderLoaded(): As it was Slider B (emission), both slider loading completed is flagged (HOOD_CLOSED is assumed) (FilterSlidersProcessingHasBeenFinished).");
            }

            if (!FilterSliderLoadingIsInProgress.IsSet)
            {
                EnableTempPollingAndNfc();   // How to call it only when there is no HOOD_CLOSED plus slide push before that or after some time if it is free (e.g. after 50 secs).
            }
            
            mLogger.Debug(LogCategoryEnum.Host, $"=== SLIDER LOADING... ActOnSliderLoaded(): Flagging Slider {slideSel} loading process is over (FilterSlider{slideSel}LoadingIsInProgress reset)");
            mLogger.Debug(LogCategoryEnum.Host, "ActOnSliderLoaded Temp. polling and NFC enabled.");
        }

        public void WaitForPossibleManuallyInsertedSliderLoadingToBeFinished()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            while (FilterSliderLoadingIsInProgress.IsSet && stopwatch.ElapsedMilliseconds < 26000)
            {
                Thread.Sleep(1000);
            }
            FilterSliderLoadingIsInProgress.Reset();
        }

        public void StoreAndNotify(string slideSel, SliderTagData slide, FilterTagData[] filter)
        {
            var slideWithActualFilter = GetSlideWithActualFilters(slide, filter);
            StoreFilterData(slideSel, slide, IsSimulated, s => mLogger.Error(LogCategoryEnum.Host, s));

            if (!IsInitialized || null == slide) return;

            mActorService.Get<WebServiceProxyActor>().Tell(
                MessageExtensions.CreateRequestMessage(new SlideLoadedMessage
                {
                    SlideSel = slideSel,
                    Slide = slideWithActualFilter
                }));
        }

        private SliderTagData GetSlideWithActualFilters(SliderTagData slider, FilterTagData[] filter)
        {
            if (null == slider) return slider;
            if (slider.IsValid)
            {
                MD.Nfc.Operation.FilterPosMngmnt.FilterPosMngr.CopyAllFilterTagData(filter, slider.Filter);
            }
            else
            {
                slider = null;
            }

            return slider;
        }

        public void ActOnHoodOpenDeviceRequest()
        {
            using (var mHoodSpinnerController = new SpinnerController(mActorService.Get<WebServiceProxyActor>, Properties.Resources.ResourceManager.GetString("Filter_Configuration")))
            {
                mHoodSpinnerController.DisplaySpinner();
                mLogger.Debug(LogCategoryEnum.Host, "ActOnHoodOpenDeviceRequest(): After Display Spinner");
                ResetBothSlideConfigurationsAndNotifyThem();
                mLogger.Debug(LogCategoryEnum.Host, "ActOnHoodOpenDeviceRequest(): Before Hide Spinner");
                mHoodSpinnerController.HiddeSpinner();
            }

            mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new HoodStateChangedMessage { IsOpen = true }));
        }

        private void ResetBothSlideConfigurationsAndNotifyThem()
        {
            foreach (var slideSel in new[] { "A", "B" })
            {
                ResetSlideConfigAndNotifyIt(slideSel);
            }
        }

        private void ResetSlideConfigAndNotifyIt(string slideSel)
        {
            var filter = new[] { new FilterTagData(), new FilterTagData(), new FilterTagData(), new FilterTagData(), new FilterTagData(), new FilterTagData() };
            StoreAndNotify(slideSel, null, filter);
            GimliReader.UpdateFilterConfiguration(slideSel, null, filter);
        }
    }
}
