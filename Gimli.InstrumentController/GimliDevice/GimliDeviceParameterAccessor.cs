﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using Apex.Device;
using Gimli.JsonObjects;
using Gimli.JsonObjects.InjectorMaintenance;
using Gimli.InstrumentServer.GimliDevice;
using Gimli.JsonObjects.FiltersMngmnt;
using MD.Nfc.Basic.StrTagConverting;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;

namespace Gimli.InstrumentController.GimliDevice
{
	internal class GimliDeviceParameterAccessor : IGimliDeviceParameterAccessor
	{
		private readonly ILogService mLogger;
		private readonly IGimliDeviceHelper mHelper;

		internal bool IsSimulated { get; set; }

		public static GimliDeviceParameterAccessor Generate(ILogService logger, IActorService actorService,
            IGimliDeviceHelper helper)
		{
			return new GimliDeviceParameterAccessor(logger, helper);
		}

		protected GimliDeviceParameterAccessor(ILogService logger, IGimliDeviceHelper helper)
		{
			if (logger == null) throw new ArgumentNullException(nameof(logger));
			mLogger = logger;
			mHelper = helper;
		}

		#region interface

		public InstrumentParameters GetConfiguration()
		{
			if (!mHelper.IsInitialized)
			{
				mLogger.Warning(LogCategoryEnum.Host, "Get configuration can not be executed while instrument is not initialized.", null);
				return new InstrumentParameters
				{
					ReaderConfiguration = new ReaderConfiguration {DeviceType = DeviceConnector.DefaultReaderProductName == DeviceConnector.Id5 ? DeviceConnector.DefaultReaderProductName : null},
					Preferences = new ReadPreferences(),
					Shake = new ShakeSettings(),
					Temperature = new TemperatureInstrumentParameters()
				};
			}

			var temperature = new TemperatureInstrumentParameters();
			if (mHelper.TemperatureController != null)
			{
				temperature.Target = new DoubleValueWithRange
				{
					Value = mHelper.TemperatureController.TargetValue,
					Maximum = mHelper.TemperatureController.MaxValue,
					Minimum = mHelper.TemperatureController.MinValue,
					Step = 0.5
				};

				temperature.ControllerRunning = mHelper.TemperatureController.IsHeating;
			}

			var shake = new ShakeSettings
			{
				Mode = new ListValueWithLegalValues<ShakeMode>
				{
					Value = ShakeMode.Orbital,
					LegalValues = mHelper.OutsideShakeController?.ShakeModes
				},
				Format = new ListValueWithLegalValues<string>
				{
					Value = GetLocalizedPlateFormat(96),
					LegalValues =
						new List<string>
						{
							GetLocalizedPlateFormat(6),
							GetLocalizedPlateFormat(12),
							GetLocalizedPlateFormat(24),
							GetLocalizedPlateFormat(48),
							GetLocalizedPlateFormat(96),
							GetLocalizedPlateFormat(384)
						}
				},
				Intensity = new ListValueWithLegalValues<ShakeIntensity>
				{
					Value = ShakeIntensity.Medium,
					LegalValues = mHelper.OutsideShakeController?.ShakeIntensities
				},
				AutoPauseTimeoutSeconds = 300
			};

			var serial = mHelper.GimliReader.Config.SerialNr;
			while (serial.Length < 4) serial = "0" + serial;

		    string moduleName = string.Empty, moduleSerial = string.Empty;
		    GetModuleInformation(mHelper.GimliReader.GimliConfig, ref moduleName, ref moduleSerial);

			var reader = new ReaderConfiguration
			{
				DeviceNumber = mHelper.GimliReader.Config.DeviceType.ToString(),
				FirmwareEEPROM = "",
				FirmwareVersion = IsSimulated ? ConfigurationManager.AppSettings["SimulatorFwVersion"] : mHelper.GimliReader.Config.FWVersion,
				PICEEPROM = "",
				PICVersion = ((V2Config) mHelper.GimliReader.Config).PICVersion,
				SerialNumber = serial,
				DeviceType = mHelper.IsFilterReady ? GlobalConstants.DeviceId5 : GlobalConstants.DeviceId3,
				ReadBarcodeOnLoad =
					mHelper.GimliReader.GeTransportController() != null &&
					mHelper.GimliReader.GeTransportController().CameraFunctions.CanReadBarcode,
                ModuleName = moduleName,
                ModuleSerialNumber = moduleSerial
			};

			var read = new ReadPreferences
			{
				AutoEjectPlate = mHelper.GimliReader.EjectPlateAfterRead,
				ReadPlateheightOnLoad =
					mHelper.GimliReader.GeTransportController() != null &&
					mHelper.GimliReader.GeTransportController().CameraFunctions.CanReadPlateHeight
			};

			var injector = mHelper.GetInjectorConfiguration();
			var filters = mHelper.GetFilterConfiguration();

			return new InstrumentParameters
			{
				Temperature = temperature,
				Shake = shake,
				ReaderConfiguration = reader,
				Preferences = read,
				InjectorConfiguration = new InjectorsSetup {Injectors = injector},

				FilterConfiguration = filters
			};
		}

		public InstrumentSettings GetParameters(string typeName, CancellationToken cancellationToken)
		{
			var volumes = new List<double>();
			var maxVolume = 3000000;
			if (mHelper.IsSimulated)
			{
				var simulatedStore = mHelper.GetTubingStore();
				AddCounterToList(simulatedStore, volumes, InjectorSel.Injector1);
				AddCounterToList(simulatedStore, volumes, InjectorSel.Injector2);
			}
			else
			{
				mHelper.PrepareInjectorMaintenance();

				volumes = mHelper.InjectorMaintenance.GetVolumeCounter();
				var injectorReader = mHelper.GimliReader.Config as IInjectorSupport;
				if (injectorReader != null)
				{
					var maint = injectorReader.GetInjectorMaintenance();
					maxVolume = maint.TubingReplacementErrorLimit;
				}

                Trace.WriteLine($"GetParameters(): Before calling EnableTempPollingAndNfc() typeName {typeName}");
                mHelper.EnableTempPollingAndNfc();
			}

			return new InstrumentSettings
			{
				InjectorsCounters =
					new List<InjectorCounters>
					{
						GetStandardInjectorCounter(InjectorSel.Injector1, volumes[0], maxVolume),
						GetStandardInjectorCounter(InjectorSel.Injector2, volumes[1], maxVolume)
					}
			};
		}

		public string GetDeviceIdentification()
		{
			return mHelper.IsSimulated
				? "+\rMolecular Devices SpectraMax Gimli Anthos\rV1.0 b17 12.10.2016\r145\r777\rDevice Code: 8575\rPlate Control: FALSE\rPIC Firmware: +V1.3 b13 22.04.2016\rDisplay Firmware: +V0.0 b0\rNew MainPCB: Yes\rCartridge: Pos Type Version SerienNr\r1 26 10 1\r2 0 0 0\r3 8 10 1\r4 0 0 0\r5 0 0 0\r6 0 0 0\r7 26 10 1\r8 0 0 0\r9 8 10 1\r10 0 0 0\r11 0 0 0\r12 0 0 0\rExScanFirst"
				: mHelper.ReaderResponse;
		}

		public void DoFilterMngmnt(FiltersMngrAction action, int noOfResp, int timeout, CancellationToken cancellationToken)
		{
			mLogger.Info(LogCategoryEnum.Host, "DoFilterMngmnt(): enter");

			mHelper.PrepareFiltersMngmnt();

			switch (action.Mode)
			{
				case FiltersMngmntMode.EjectSlide:
				{
					var ejectAction = action.FiltersMngmntAction as EjectSlideAction;
					if (ejectAction == null) break;

                    mHelper.DisableTempPollingAndNfc();
				    if (!mHelper.IsSimulated)
				    {
				        mHelper.FilterController.EjectSlider(ejectAction.Slide, cancellationToken);
				    }

				    GimliDeviceHelper.StoreFilterData(FiltersMngrAction.MapFilterSlotId((int)ejectAction.Slide), null, mHelper.IsSimulated, s => mLogger.Error(LogCategoryEnum.Host, s));

                    var filter = new[] { new FilterTagData(), new FilterTagData(), new FilterTagData(), new FilterTagData(), new FilterTagData(), new FilterTagData() };
                    mHelper.GimliReader.UpdateFilterConfiguration(SlideSel.SlideA == ejectAction.Slide ? "A" : "B", null, filter);

                    // enable here because eject is finished here while load slide is not (still NFC events coming from FW)
                    mHelper.EnableTempPollingAndNfc(); 

                    break;
				}
				case FiltersMngmntMode.LoadSlide:
				{
					if (mHelper.IsSimulated) break;

					var loadAction = action.FiltersMngmntAction as LoadSlideAction;
					if (loadAction == null) break;

					mHelper.LoadFilterSlide(loadAction.Slide, cancellationToken);

                    mHelper.WaitForPossibleManuallyInsertedSliderLoadingToBeFinished();
                        
                    mLogger.Debug(LogCategoryEnum.Uncategorized, $"DoFilterMngmnt(): var result = mHelper.FilterSlideInserted.Wait(26000).");
                    // do not EnableTempPollingAndNfc because load slide is not finished (still NFC events coming from FW)
                    break;
                    }
				case FiltersMngmntMode.DummyInsertManually:
				{
						var dummyaction = action.FiltersMngmntAction as DummyInsertManuallyAction;
						if (dummyaction == null) break;

						GimliDeviceHelper.StoreFilterData(FiltersMngrAction.MapFilterSlotId((int) dummyaction.Slide), dummyaction.Info, true, s => mLogger.Error(LogCategoryEnum.Host, s));
						mHelper.GimliReader.UpdateFilterConfiguration(SlideSel.SlideA == dummyaction.Slide ? "A" : "B", dummyaction.Info, dummyaction.Info.Filter);
                        mHelper.EnableTempPollingAndNfc(); 
						break;
                    }
			}

            // do not EnableTempPollingAndNfc because load slide is not finished (still NFC events coming from FW)
            mLogger.Info(LogCategoryEnum.Host, "DoFilterMngmnt: leave");
		}

		public string GetModuleInformation()
		{
			var module = mHelper.GimliReader.GimliConfig.Module;
			return module == null ? null : $"{module.Alias}(sn{module.SerialNr})";
		}

		#endregion

		private InjectorCounters GetStandardInjectorCounter(InjectorSel inj, double volume, double maxVolume)
		{
			return new InjectorCounters
			{
				Name = inj,
				Unit = "ml",
				TubingVolumeCounter =
					new DoubleValueWithMaximum {Value = volume/1000.0, Maximum = maxVolume}
			};
		}

		private static void AddCounterToList(Dictionary<InjectorSel, DoubleValueWithMaximum> simulatedStore,
			List<double> volumes, InjectorSel inj)
		{
			if (simulatedStore.ContainsKey(inj)) volumes.Add(simulatedStore[inj].Value);
		}

		private static string GetLocalizedPlateFormat(int numberOfWells)
		{
			return string.Format(JsonObjects.Resources.GetResource.GetString("Plate_Format_Mask"), numberOfWells);
		}

        private static bool GetModuleInformation(GimliConfig config, ref string moduleName, ref string moduleSerial)
        {
            if (config.Module == null)
                return false;

            moduleName = config.Module.Alias;
            moduleSerial = config.Module.SerialNr;

            return true;
        }
    }
}
