﻿using System;
using System.Threading;
using Gimli.InstrumentServer;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.InstrumentServer.GimliDevice;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;

namespace Gimli.InstrumentController.GimliDevice
{
    internal class GimliDevicePlateManipulator : IGimliDevicePlateManipulator
    {
        private readonly ILogService mLogger;
        private readonly IActorService mActorService;
        private readonly IGimliDeviceHelper mHelper;

        public static GimliDevicePlateManipulator Generate(ILogService logger, IActorService actorService, IGimliDeviceHelper helper)
        {
            return new GimliDevicePlateManipulator(logger, actorService, helper);
        }

        protected GimliDevicePlateManipulator(ILogService logger, IActorService actorService, IGimliDeviceHelper helper)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            mLogger = logger;
            mActorService = actorService;
            mHelper = helper;
        }

        #region interface
        public void LockUnlockReader()
        {
            try
            {
                mHelper.TemperatureController?.SetPollingStatus(false);
                mHelper.GimliReader.SendSimpleCmd("TLOCK", 20, 60);
                SendPushMessage(Properties.Resources.Reader_Locked_Success, MessageType.Info, MessageStatusCodes.DeviceLocked, MessageDisplayType.Toast);
            }
            catch (Exception e)
            {
                mLogger.Error(LogCategoryEnum.Host, "Lock reader failed", e);
                SendPushMessage(string.Format(Properties.Resources.Reader_Locked_Fail, e.Message), MessageType.Error, MessageStatusCodes.InstrumentErrorMessage, MessageDisplayType.DialogOk);
                throw;
            }
        }

        public void OpenCloseDrawer(PlateDrawerSettings settings)
        {
            ExecuteWithTemperatureSync(() =>
            {
                try
                {
                    switch (settings.Behavior)
                    {
                        case ToggleBehavior.Toggle:
                            mHelper.GimliReader.GeTransportController().TogglePlateDrawer(false);
                            break;
                        case ToggleBehavior.Open:
                            if (!mHelper.GimliReader.GeTransportController().IsPlateDrawerOpen)
                                mHelper.GimliReader.GeTransportController().TogglePlateDrawer(false);
                            break;
                        case ToggleBehavior.Close:
                            if (mHelper.GimliReader.GeTransportController().IsPlateDrawerOpen)
                                mHelper.GimliReader.GeTransportController().LoadPlate(false);
                            break;
                    }

                }
                catch (Exception e)
                {
                    mLogger.Error(LogCategoryEnum.Host, "OpenCloseDrawer failed", e);
                    throw;
                }
            });
        }

        public void ToggleAutoEjectPlate(bool active)
        {
            mHelper.GimliReader.EjectPlateAfterRead = active;
        }

        public void ShakeOutside(ShakeSettingsSetInfo settings, CancellationToken token)
        {
            ExecuteWithTemperatureSync(() =>
            {
                mHelper.OutsideShakeController.StartShake(settings.Format, settings.Mode, settings.Intensity);
                while (!token.IsCancellationRequested)
                {
                    Thread.Sleep(500);
                }
                mHelper.OutsideShakeController.PauseShake();
            });
        }

        public void InsertPlateAfterShake()
        {
            ExecuteWithTemperatureSync(() => mHelper.OutsideShakeController.StopShake(ShakeStopOptions.CloseDrawer));
        }

        public void ReleasePlate()
        {
            ExecuteWithTemperatureSync(() => mHelper.OutsideShakeController.StopShake(ShakeStopOptions.ReleasePlate));
        }
#endregion

        private void ExecuteWithTemperatureSync(Action action)
        {
            try
            {
                mHelper.TemperatureController?.SetPollingStatus(false);
                action();
            }
            finally
            {
                mHelper.TemperatureController?.SetPollingStatus(true);
            }
        }

        private void SendPushMessage(string msg, MessageType type, int msgCode, MessageDisplayType displayType)
        {
            mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
            {
                Parameters = new MessageParameters
                {
                    Message = msg,
                    DisplayType = displayType,
                    MessageType = type,
                    MessageCode = msgCode
                }
            }));
        }

        public void InitOutShakeController()
        {
            mHelper.OutsideShakeController = GimliOutsideShakeCtrl.Generate(mHelper.GimliReader.Shaker, mHelper.GimliReader.GeTransportController(), mLogger);
        }
    }
}
