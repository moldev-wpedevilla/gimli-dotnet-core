﻿using System;
using System.Security.Cryptography;
using Gimli.InstrumentServer;
using Gimli.InstrumentServer.GimliDevice;
using Gimli.InstrumentServer.Validation;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;
using IGimliDevice = Gimli.InstrumentServer.GimliDevice.IGimliDevice;

namespace Gimli.InstrumentController.GimliDevice
{
    public class GimliDevice : IGimliDevice
    {
        #region interface
        public IGimliDeviceLifeCycleManager LifeCycleMngr { get; }
        public IGimliDeviceMaintenanceController MaintenanceController { get; }
        public IGimliDeviceOutboundController OutboundController { get; }
        public IGimliDeviceParameterAccessor ParameterAccessor { get; }
        public IGimliDevicePlateManipulator PlateManipulator { get; }
        public IGimliDeviceReadExecutor ReadExecutor { get; }
	    public IGimliDeviceHelper Helper { get; }
	    public ISettingsDefaults DefaultSettings { get; }

	    #endregion

        public static GimliDevice Generate(ILogService logger, IActorService actorService, IInstrumentContext instrumentContext, ISettingsValidatorFactory settingsValidatorFactory)
        {
            return new GimliDevice(logger, actorService, instrumentContext, settingsValidatorFactory);
        }

        protected GimliDevice(ILogService logger, IActorService actorService, IInstrumentContext instrumentContext, ISettingsValidatorFactory settingsValidatorFactory)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            var helper = new GimliDeviceHelper(logger, actorService, instrumentContext);
            
	        Helper = helper;
			MaintenanceController = GimliDeviceMaintenanceController.Generate(logger, actorService, helper);
            OutboundController = GimliDeviceOutboundController.Generate(logger, actorService, helper);
            ParameterAccessor = GimliDeviceParameterAccessor.Generate(logger, actorService, helper);
            PlateManipulator = GimliDevicePlateManipulator.Generate(logger, actorService, helper);
            ReadExecutor = GimliDeviceReadExecutor.Generate(logger, actorService, helper);
            LifeCycleMngr = GimliDeviceLifeCycleManager.Generate(logger, actorService, instrumentContext, helper, PlateManipulator, settingsValidatorFactory);
			DefaultSettings = GimliSettingsDefaults.Generate(helper);
        }
    }
}
