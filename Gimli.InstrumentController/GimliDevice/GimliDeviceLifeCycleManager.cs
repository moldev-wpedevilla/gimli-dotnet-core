﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Apex.Device;
using Apex.Device.RequestManaging;
using Apex.Remoting;
using Gimli.InstrumentServer;
using Gimli.InstrumentServer.Actors;
using Gimli.InstrumentServer.GimliDevice;
using Gimli.InstrumentServer.Validation;
using Gimli.JsonObjects;
using Gimli.JsonObjects.FiltersMngmnt;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.Instrument;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;
using MessageType = Gimli.JsonObjects.MessageType;

namespace Gimli.InstrumentController.GimliDevice
{
    internal class GimliDeviceLifeCycleManager : IGimliDeviceLifeCycleManager
    {
	    private readonly ILogService mLogger;
        private readonly IActorService mActorService;
	    private readonly IInstrumentContext mInstrumentContext;
	    private readonly IGimliDeviceHelper mHelper;

        private readonly IGimliDevicePlateManipulator mPlateManipulator;
		private readonly ISettingsValidatorFactory mSettingsValidatorFactory;
        private ManualResetEventSlim mReadDoneEvent;
	    private IInstrumentConnector mInstrumentConnector;

        public static GimliDeviceLifeCycleManager Generate(ILogService logger, IActorService actorService, IInstrumentContext instrumentContext, IGimliDeviceHelper helper, 
             IGimliDevicePlateManipulator plateManipulator, ISettingsValidatorFactory settingsValidatorFactory)
        {
            return new GimliDeviceLifeCycleManager(logger, actorService, instrumentContext, helper, plateManipulator, settingsValidatorFactory);
        }

        protected GimliDeviceLifeCycleManager(ILogService logger, IActorService actorService, IInstrumentContext instrumentContext, IGimliDeviceHelper helper, 
            IGimliDevicePlateManipulator plateManipulator, ISettingsValidatorFactory settingsValidatorFactory)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            mLogger = logger;
            mActorService = actorService;
	        mInstrumentContext = instrumentContext;
	        mHelper = helper;

            mPlateManipulator = plateManipulator;
	        mSettingsValidatorFactory = settingsValidatorFactory;

	        SendStayAliveMsgAndRetakeControl(GetStayAliveInterval());
        }

        private static int GetStayAliveInterval()
        {
            var stayAliveIntervalString = ConfigurationManager.AppSettings["StayAliveInterval"];
            int stayAliveInterval;
            var parseSuccess = int.TryParse(stayAliveIntervalString, out stayAliveInterval);
            if (!parseSuccess)
            {
                stayAliveInterval = 30000;
            }
            return stayAliveInterval;
        }

        private void SendStayAliveMsgAndRetakeControl(int stayAliveInterval)
        {
            Task.Run(() =>
            {
                while (true)
                {
	                SendStayAliveMsg(stayAliveInterval);
                }
            });
        }

        private void SendStayAliveMsg(int stayAliveInterval)
        {
            Thread.Sleep(stayAliveInterval);
            mLogger.Debug(LogCategoryEnum.Host, "Sending stay alive message.");

            mActorService.Get<NetworkCommunicationServiceProxyActor>().Tell(
                MessageExtensions.CreateRequestMessage(new HeartbeatMessage()));
        }
        
        #region interface implementation

        public void ConnectToInstrument(bool isSimulated)
        {
            mLogger.Info(LogCategoryEnum.Host, "GimliDeviceLifeCycleManager ConnectToInstrument(): Entering ...");
            DeviceConnectorFactory.SupportSimualtion = mHelper.IsSimulated = isSimulated;
            //if !isSimulated try to connect to the instrument
	        if (mInstrumentConnector != null)
	        {
				mLogger.Info(LogCategoryEnum.Host, "Already connected.");
		        return;
	        }

			mLogger.Info(LogCategoryEnum.Host, "Generating DefaultInstrumentConnector.");
	        mInstrumentConnector = DeviceConnectorFactory.GenerateDefaultInstrumentConnector(mLogger);
            //CreateSimualtionObject first so we can allow protocol editing
			mLogger.Info(LogCategoryEnum.Host, "Creating simulation object.");
            mHelper.GimliReader = DeviceConnector.CreateSimualationObject();
			InitializeConnection();
            if (mHelper.IsSimulated)
                InitializeTempShakeAndNfc();
        }

	    public void InitializeInstrument()
	    {
		    InitializeInstrumentInternal();
            mHelper.IsInitialized = true;
            if (!mHelper.IsFilterReady)
            {
                mHelper.StoreAndNotify("A", null, null);
                mHelper.StoreAndNotify("B", null, null);
            }
            mActorService.Get<WebServiceProxyActor>()
                .Tell(MessageExtensions.CreateRequestMessage(new DeviceInitializingMessage { IsInitializing = false }));
        }

        public void InitializeInstrumentInternal()
		{
			GimliDeviceHelper.ReadFilterInfoFromFileSystem(s => mLogger.Error(LogCategoryEnum.Host, s), mHelper.IsSimulated);

			if (mHelper.IsSimulated)
			{
				mHelper.IsLumBottomReady = bool.Parse(ConfigurationManager.AppSettings["SimulatorIsLumBottomReady"]);
				mHelper.IsFilterReady = bool.Parse(ConfigurationManager.AppSettings["SimulatorFilterReady"] ?? "true");
				Thread.Sleep(int.Parse(ConfigurationManager.AppSettings["SimulatorInitDuration"] ?? "0"));
				return;
            }
            if (mHelper.GimliReader != null && mHelper.GimliReader.Connected)
            {
                InitializeTempShakeAndNfc();
	            return;
            }
            //if not simulating check if we are connected - if not wait until the reader is there
            try
            {
                mLogger.Info(LogCategoryEnum.Host, "GimliDeviceLifeCycleManager::InitializeInstrument - checking if connected");
                while (!mInstrumentConnector.Connected)
                {
                    Thread.Sleep(6000);
                    mLogger.Info(LogCategoryEnum.Host, "GimliDeviceLifeCycleManager::InitializeInstrument() - not connected (refresh)");
                    ((DeviceConnector)mInstrumentConnector).Refresh();
                }

                //if connected check if reader is ready (could be still initializing)
	            var deviceInitFailed = false;
	            var numberOfFailedResponses = 0;
	            while (true)
	            {
		            DeviceInitState deviceInitState;
					try
					{
						deviceInitState = ((DeviceConnector)mInstrumentConnector).IsReady();
					}
					catch (Exception e)
					{
						mLogger.Error(LogCategoryEnum.Host, "Catched exception gettting device state -> trying again : ", e);
						Thread.Sleep(5000);
						continue;
					}

		            var initDone = false;
		            switch (deviceInitState)
		            {
			            case DeviceInitState.None:
				            break;
			            case DeviceInitState.Ready:
				            initDone = true;
				            break;
			            case DeviceInitState.Connecting:
			            case DeviceInitState.Measuring:
							mLogger.Info(LogCategoryEnum.Host, "INIT: not ready (sleeping)");
							Thread.Sleep(5000);
				            break;
			            case DeviceInitState.Failed:
							mLogger.Info(LogCategoryEnum.Host, "INIT: returned failed state (sleeping)");
							Thread.Sleep(5000);
							numberOfFailedResponses++;
				            if (numberOfFailedResponses > 30)
				            {
								initDone = true;
					            deviceInitFailed = true;
				            }
				            break;
			            default:
				            throw new ArgumentOutOfRangeException();
		            }
		            if (initDone)
		            {
			            break;
		            }
	            }

	            while (true)
	            {
		            try
		            {
			            mHelper.GimliReader = ((DeviceConnector) mInstrumentConnector).GetReaderObject();
			            break;
		            }
		            catch (Exception e)
		            {
						mLogger.Error(LogCategoryEnum.Host, "Catched exception creating reader object -> trying again : ", e);
						Thread.Sleep(5000);
					}
				}
	            //if (IsFirmwareUpdateNeeded())
	            //{
	            //    SendPushMessage(Properties.Resources.Firmware_Updated_Needed_Message, MessageType.Info, MessageStatusCodes.FirmwareUpdate, MessageDisplayType.DialogDecision);
	            //}
	            InitializeConnection();
	            if (deviceInitFailed)
	            {
					mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
					{
						Parameters = new MessageParameters
						{
							Message = Properties.Resources.Reader_Init_Failed,
							DisplayType = MessageDisplayType.DialogOk,
							MessageCode = MessageStatusCodes.ShutdownAfterInitFailureMessage,
							MessageType = MessageType.Error
						}
					}));
					return;
	            }

				// here temperature polling is enabled for iD3
	            InitializeTempShakeAndNfc();

	            mHelper.IsLumBottomReady = mHelper.GimliReader.GimliConfig.Features.HasFlag(FluoroFeatures.LumiBottom);

				// in case of iD5 temperature polling is disabled for slide loading and enabled afterwards again
	            LoadSlidersIfFeatureExists(true);
            }
            catch (Exception e)
            {
	            mLogger.Error(LogCategoryEnum.Host, "GimliDeviceLifeCycleManager::InitializeInstrument - Exception!!!!", e);
	            ((DeviceConnector) mInstrumentConnector).Refresh();
	            Thread.Sleep(1000);
	            throw;
            }
		}

        private void LoadSlidersIfFeatureExists(bool isInit)
	    {
		    try
		    {
                mHelper.IsFilterReady = mHelper.GimliReader?.Config.InstrumentIdentity.Version == InstrumentVersion.V2;
			    if (mHelper.IsFilterReady)
			    {
                    mLogger.Debug(LogCategoryEnum.Host, $"=== SLIDER LOADING... LoadSlidersIfFeatureExists(): Beginning ... Both sliders are going to be loaded {(isInit ? "on Init stage" : "on HOOD_CLOSED")}).");
                    mHelper.PrepareFiltersMngmnt();
                    mHelper.WaitForPossibleManuallyInsertedSliderLoadingToBeFinished();
                    mHelper.FilterSlidersProcessingHasBeenFinished.Reset();
                    //mLogger.Debug(LogCategoryEnum.Host, $"=== SLIDER LOADING... LoadSlidersIfFeatureExists(): FilterSlidersProcessingFinished has been reset at the beginning of both sliders loading");

                    foreach (var slide in new[] {SlideSel.SlideA, SlideSel.SlideB})
				    {
                        //mHelper.PrepareFiltersMngmnt();
                        LoadSlideIfNotAlreadyLoaded(slide, isInit);
				    }

				    var result = mHelper.FilterSlidersProcessingHasBeenFinished.Wait(52000);
                    mLogger.Debug(LogCategoryEnum.Host, $"=== SLIDER LOADING... LoadSlidersIfFeatureExists(): Ending ... after waiting for FilterSlidersProcessingHasBeenFinished (result: {result}).");
                    // ----------------------------------------------------------------
                }
		    }
		    catch (Exception e)
		    {
			    mLogger.Error(LogCategoryEnum.Host, "Exception caught while (taking a decision on) loading filter slides: " + e);
		    }
		    finally
		    {
			    Trace.WriteLine($"LoadSlidersIfFeatureExists() Exiting ... ");
		    }
	    }



        private void LoadSlideIfNotAlreadyLoaded(SlideSel slide, bool isInit)
	    {
		    if (SlideIsAlreadyLoaded(slide, isInit))
		    {
			    if (SlideSel.SlideB == slide)
			    {
				    mHelper.FilterSlidersProcessingHasBeenFinished.Set();
                    mLogger.Debug(LogCategoryEnum.Host, $"=== SLIDER LOADING... LoadSlideIfNotAlreadyLoaded(): Both sliders loading has been finished.");
                }
			    return;
		    }
            mHelper.LoadFilterSlide(slide, CancellationToken.None);
            mHelper.WaitForPossibleManuallyInsertedSliderLoadingToBeFinished();
            mLogger.Debug(LogCategoryEnum.Host, $"=== SLIDER LOADING... LoadSlideIfNotAlreadyLoaded(): Ending ... [Slide {(SlideSel.SlideA == slide ? "A" : "B")}]");
        }

        private bool SlideIsAlreadyLoaded(SlideSel slide, bool isInit)
	    {
	        if (isInit)
	        {
	            return false;
	        }

            var isSliderAlreadyLoaded = mHelper.FilterController.IsSliderLoaded(slide, new CancellationToken());
            mLogger.Debug(LogCategoryEnum.Host, $"Getting slider loaded status... IsSliderAlreadyLoaded(): Slider {(SlideSel.SlideA == slide ? "A" : "B")} {(isSliderAlreadyLoaded ? "was already loaded" : "was not loaded yet")}");
            return isSliderAlreadyLoaded;
	    }

        /// <summary>
        /// Sends a shutdown confirmation request to user if shutdown is allowed.
        /// </summary>
        public void RequestShutdown()
	    {
		    mActorService.Get<InstrumentServerRoutingActor>().Tell(MessageExtensions.CreateRequestMessage(new ShutdownMessage {Type = ShutdownMessageType.Request, WantsAnswer = false}));
	    }

	    /// <summary>
	    /// Sends the shutdown command to the instrument if possible.
	    /// </summary>
	    public void DoShutdown()
	    {
		    mHelper.DisableTempPollingAndNfc();
		    mHelper.GimliReader.InitiateShutdown(30000);
	    }

	    #endregion

	    private void InitializeConnection()
	    {
		    mLogger.Info(LogCategoryEnum.Host, "GimliDeviceLifeCycleManager InitializeConnection(): Entering ...");
		    if (!mHelper.IsSimulated && mHelper.GimliReader.SupportRequests)
		    {
			    mLogger.Info(LogCategoryEnum.Host, "Getting RequestsController");
			    var req = mHelper.GimliReader.GetRequestsController();
			    if (req != null)
			    {
				    ((RequestsController) req).InstrumentRequestProcessingStarted += HandleBeforeInitiatingRequest;
				    ((RequestsController) req).InstrumentRequestHandled += HandleInstrumentRequestHandled;
				    RegisterEvents();
			    }
		    }

		    mLogger.Info(LogCategoryEnum.Host, "Setting reader");
		    mSettingsValidatorFactory.SetReader(mHelper.GimliReader);
		    mHelper.ReaderResponse = "+\r" + mHelper.GimliReader.SendSimpleCmd("?", 5, 5);
	    }

	    private void RegisterEvents()
	    {
		    if (!mHelper.IsSimulated && mHelper.GimliReader.SupportRequests)
		    {
			    var req = mHelper.GimliReader.GetRequestsController();
			    if (req != null)
			    {
				    req.FilterTagRead += mHelper.FilterTagReadEventHandler;
				    req.SlideTagRead += mHelper.SlideTagReadEventHandler;

				    req.UnknownTagRead += mHelper.UnknownTagReadEventHandler;
				    req.CardTagRead += mHelper.CardTagReadEventHandler;
				    req.LinkTagRead += mHelper.LinkTagReadEventHandler;
				    req.PhoneTagRead += mHelper.PhoneTagReadEventHandler;
				    req.ProtocolTagRead += mHelper.ProtocolTagReadEventHandler;
			    }
		    }
	    }

	    private void ActOnFilterPositioningFirstPositionDeviceRequest(SlideSel slide)
	    {
            mHelper.FilterSliderLoadingIsInProgress.Set();
            mLogger.Debug(LogCategoryEnum.Host, $"=== SLIDER LOADING... ActOnFilterPositioningDeviceRequest(): Slider {(SlideSel.SlideA == slide ? "A" : "B")} loading in progress ...");

            // --------------------------------------------------------------------------------------------

            mHelper.DisableTempPollingAndNfc();
	    }

	    private void ActOnHoodClosedDeviceRequest()
	    {
		    Task.Run(() =>
		    {
			    LoadSlidersIfFeatureExists(false);
			    Thread.Sleep(1000);
			    RefreshDevice();

			    mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new HoodStateChangedMessage {IsOpen = false}));
		    });
	    }

	    private void RefreshDevice()
	    {
		    using (var controller = new SpinnerController(mActorService.Get<WebServiceProxyActor>, Properties.Resources.ResourceManager.GetString("Get_module_data")))
		    {
			    controller.DisplaySpinner();
			    mHelper.GimliReader.Refresh();
			    controller.HiddeSpinner();
		    }
	    }

        private void HandleBeforeInitiatingRequest(object source, InstrumentRequestProcessingStartedArgs args)
	    {
		    switch (args.InstrumentRequest)
		    {
			    case InstrumentRequestEnum.SystemShutdown:
				    RequestShutdown();
				    break;
			    case InstrumentRequestEnum.ReadFilterTagOnSlideLoading:
				    if (args.Param.StartsWith("START"))
				    {
                        mInstrumentContext.State.State = DeviceState.Busy;
                        if (mHelper.IsInitialized)
                        {
                            mHelper.SpinnercontrolerForFilterMovement = new SpinnerController(mActorService.Get<WebServiceProxyActor>, Properties.Resources.Filter_Configuration);
                            mHelper.SpinnercontrolerForFilterMovement.DisplaySpinner(25000);
                        }
                        ActOnFilterPositioningFirstPositionDeviceRequest(args.Param.Contains("START A") ? SlideSel.SlideA : SlideSel.SlideB);
                    }
				    break;
                case InstrumentRequestEnum.HoodOpen:
		            mHelper.ActOnHoodOpenDeviceRequest();
                    break;
                case InstrumentRequestEnum.HoodClosed:
                    ActOnHoodClosedDeviceRequest();
                    break;
                case InstrumentRequestEnum.ReadSlideTagOnSlideLoading:
				    break;
			    // TODO for other cases
		    }
	    }

	    private void HandleInstrumentRequestHandled(object source, InstrumentRequestHandledArgs args)
	    {
            Trace.WriteLine($"InstrumentRequestHandledHandler(): Entering ...");
            mHelper.ActOnSliderLoaded(args.SlideSel, args.Slide, args.Filter);
	    }

	    private void InitializeTempShakeAndNfc()
	    {
		    try
		    {
			    mHelper.InitTemperatureController();
			    ((GimliDevicePlateManipulator) mPlateManipulator).InitOutShakeController();
		    }
		    catch (Exception exc)
		    {
			    mLogger.Error(LogCategoryEnum.Host, $"GimliDeviceLifeCycleManager InitializeTempShakeAndNfc(): Initialization Error: {exc.Message} - {exc.InnerException?.Message}");
		    }
	    }
    }
}
