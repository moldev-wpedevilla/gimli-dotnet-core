﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using Anthos.Controls;
using Apex.Device.Modules;
using Gimli.JsonObjects;
using Gimli.JsonObjects.InjectorMaintenance;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.InjectorMaintenance;
using Gimli.InstrumentServer;
using Gimli.InstrumentServer.GimliDevice;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;

namespace Gimli.InstrumentController.GimliDevice
{
    internal class GimliDeviceMaintenanceController : IGimliDeviceMaintenanceController, IBackgroundTaskCallback
    {
        private readonly ILogService mLogger;
        private readonly IActorService mActorService;
		private readonly IGimliDeviceHelper mHelper;

        private ManualResetEventSlim mNfcWaiter;
		private Action<Dictionary<InjectorSel, DoubleValueWithMaximum>> mTubingStoreWriter;
		private Action<int> mInjectionTimeSimulator;
	    private bool mSimulatedInjectionSucceeds;

		public static GimliDeviceMaintenanceController Generate(ILogService logger, IActorService actorService, IGimliDeviceHelper helper)
        {
            return new GimliDeviceMaintenanceController(logger, actorService, helper);
        }

        protected GimliDeviceMaintenanceController(ILogService logger, IActorService actorService, IGimliDeviceHelper helper)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            mLogger = logger;
            mActorService = actorService;
            mHelper = helper;
        }

	    public Action<Dictionary<InjectorSel, DoubleValueWithMaximum>> TubingStoreWriter
	    {
		    get {
			    return mTubingStoreWriter ??
			           (mTubingStoreWriter =
				           store => File.WriteAllText(mHelper.GetInjectorTubingStoreName(), store.JsonSerializeToCamelCase()));
		    }
		    set { mTubingStoreWriter = value; }
	    }

		public Action<int> InjectionTimeSimulator
		{
			get { return mInjectionTimeSimulator ?? (mInjectionTimeSimulator = volume => Thread.Sleep(volume*10)); }
			set { mInjectionTimeSimulator = value; }
		}

		#region interface
		public void DoInjectorMaintenance(InjectorAction action, int noOfResp,
            int timeout, CancellationToken cancellationToken)
        {
            mLogger.Info(LogCategoryEnum.Host, "DoInjectorMaintenanceOperation(): enter");

            mHelper.PrepareInjectorMaintenance();

            var injectorConfig = mHelper.GetInjectorConfiguration();

            switch (action.Mode)
            {
                case InjectionMode.Prime:
                    {
                        var typedAction = action.InjectionAction as PrimeAction;
						mSimulatedInjectionSucceeds = true;
                        ExecuteInjectionAction(() =>
						{
	                        mHelper.InjectorMaintenance.Prime(typedAction.Injector, injectorConfig, cancellationToken);
	                        return true;
                        },
                            typedAction, injectorConfig, injectorConfig.First().PrimeVolume);
                        break;
                    }
                case InjectionMode.Rinse:
                    {
                        var typedAction = action.InjectionAction as RinseAction;
						mSimulatedInjectionSucceeds = true;
                        ExecuteInjectionAction(() =>
						{
	                        mHelper.InjectorMaintenance.Rinse(typedAction.Injector, injectorConfig, cancellationToken);
	                        return true;
                        },
                            typedAction, injectorConfig, 900);
                        break;
                    }
                case InjectionMode.RevPrime:
                    {
                        var typedAction = action.InjectionAction as RevPrimeAction;
						mSimulatedInjectionSucceeds = true;
                        ExecuteInjectionAction(() =>
						{
	                        mHelper.InjectorMaintenance.RevPrime(typedAction.Injector, injectorConfig, cancellationToken);
	                        return true;
                        },
                            typedAction, injectorConfig, injectorConfig.First().PrimeVolume);
                        break;
                    }
                case InjectionMode.Wash:
                    {
                        var typedAction = action.InjectionAction as WashAction;
						mSimulatedInjectionSucceeds = true;
                        ExecuteInjectionAction(() =>
						{
	                        mHelper.InjectorMaintenance.Wash(typedAction.Injector, typedAction.Volume, typedAction.Reverse,
		                        cancellationToken);
	                        return true;
                        },
                            typedAction, injectorConfig, typedAction.Volume);
                        break;
                    }
                case InjectionMode.ResetVolumeCounter:
                    {
                        var resetAction = action.InjectionAction as ResetVolumeCounterAction;
                        if (mHelper.IsSimulated)
                        {
                            SetTubingVolume(resetAction.Injector, 0, false);
                        }
                        else
                        {
                            mHelper.InjectorMaintenance.ResetVolumeCounter(resetAction.Injector, cancellationToken);
                        }

                        SendInjectorUpdate(resetAction, injectorConfig.Select(i => i.Name).ToList(), InjectorStatus.Finished, mActorService);
                    }
                    break;
                    
                case InjectionMode.CalibWash:
                    {
                        var calibWashAction = action.InjectionAction as CalibWashAction;
						mSimulatedInjectionSucceeds = !mSimulatedInjectionSucceeds;

						ExecuteInjectionAction(
		                    () =>
		                    {
		                        var succeeded = mHelper.InjectorMaintenance.WashForCalibration(calibWashAction.Injector, cancellationToken);
                                mHelper.GimliReader?.GeTransportController().EjectUnLockedPlate();
			                    return succeeded;
		                    },
		                    calibWashAction, injectorConfig, 1000);
                    }
                    break;

                case InjectionMode.CalibDispensePlate:
                    {
                        var dispensePlateAction = action.InjectionAction as DispensePlateAction;
						mSimulatedInjectionSucceeds = !mSimulatedInjectionSucceeds;
	                    ExecuteInjectionAction(
							() =>
	                        {
                                mHelper.GimliReader?.GeTransportController().LoadPlate(false);
                                var succeeded = mHelper.InjectorMaintenance.DispensePlate(dispensePlateAction.Injector, cancellationToken);
                                mHelper.GimliReader?.GeTransportController().EjectUnLockedPlate();
		                        return succeeded;
	                        },
                            dispensePlateAction, injectorConfig, 1500);
                    }
                    break;
                case InjectionMode.SetCalibFactor:
                    {
                        var setCalibWeightAction = action.InjectionAction as SetCalibrationFactorAction;
                        if (mHelper.IsSimulated)
                        {
                            // nothing to do
                        }
                        else
                        {
                            mHelper.InjectorMaintenance.SetCalibrationFactor(setCalibWeightAction.Injector, setCalibWeightAction.Weight1, setCalibWeightAction.Weight2, cancellationToken);
                        }

                        SendInjectorUpdate(setCalibWeightAction, injectorConfig.Select(i => i.Name).ToList(), InjectorStatus.Finished, mActorService);
					}
					break;
            }

            Trace.WriteLine($"DoInjectorMaintenance(): Before calling EnableTempPollingAndNfc() action.Mode {action.Mode}");
            mHelper.EnableTempPollingAndNfc();
            mLogger.Info(LogCategoryEnum.Host, "DoInjectorMaintenanceOperation: leave");
        }

        public void SetTemperature(TemperatureSettings settings)
        {
            if (!settings.ControllerRunning)
                mHelper.TemperatureController?.TurnOff();
            else
                mHelper.TemperatureController?.SetTemperature(settings.Target);
        }

        public void PairNfc(string username, CancellationToken token)
        {
            mHelper.NfcPairingRunsWaiter.Set();
            using (new DisposableExecutor(() => mHelper.NfcPairingRunsWaiter.Reset()))
            {
                int nfcPairingTime;
                var isCancelled = false;
                if (mHelper.IsSimulated)
                {
                    nfcPairingTime = mHelper.IsNfcTagReadWaiter.IsSet ? 1500 : 5000;
                    mNfcWaiter = new ManualResetEventSlim();
                    mNfcWaiter.Wait(nfcPairingTime, token);
                    mHelper.ToggleNfcTagReadWaiter();
                    mHelper.TagGuid = Guid.NewGuid();
                }
                else
                {
                    nfcPairingTime = 5000;
                    mHelper.IsNfcTagReadWaiter.Reset();

                    try
                    {
                        mHelper.IsNfcTagReadWaiter.Wait(nfcPairingTime, token);
                    }
                    catch (OperationCanceledException exc)
                    {
                        mHelper.IsNfcTagReadWaiter.Reset();
                        isCancelled = true;
                    }
                }

                if (!isCancelled)
                {
                    if (mHelper.IsNfcTagReadWaiter.IsSet)
                    {
                        mLogger.Info(LogCategoryEnum.Host, $"PairNfc(): Pairing succeeded (tagGuid ... {mHelper.TagGuid}).");
                        mActorService.Get<DataServiceProxyActor>()
                            .Tell(
                                MessageExtensions.CreateRequestMessage(new NfcPairingResultNotificationMessage
                                {
                                    Result =
                                        new NfcPairingResult
                                        {
                                            User = new UserFull { UserName = username, NfcId = mHelper.TagGuid, HasNfc = true }
                                        },
                                    WasSuccessful = true
                                }));
                    }
                    else
                    {
                        mLogger.Error(LogCategoryEnum.Host, "PairNfc(): Pairing failed.");
                        mActorService.Get<WebServiceProxyActor>()
                            .Tell(MessageExtensions.CreateRequestMessage(new NfcPairingResultNotificationMessage
                            {
                                Result = new NfcPairingResult
                                {
                                    User = new UserFull { UserName = username, NfcId = Guid.Empty, HasNfc = false },
                                    ErrorMessage = string.Format(Properties.Resources.ResourceManager.GetString("No_NFC_Tag_Found"), TimeSpan.FromMilliseconds(nfcPairingTime).TotalSeconds)
                                },
                                WasSuccessful = false
                            }));
                    }
                }
            }
        }

        public void UpdateFirmware(string updateFolderPath)
        {
            Trace.WriteLine($"GimliDeviceMaintenanceController::UpdateFirmware(): Starting ... Before Calling DisableTempPollingAndNfc() updateFolderPath {updateFolderPath}.");
            mHelper.DisableTempPollingAndNfc();
            var mup = DeviceModulesUpdater.Generate(mHelper.GimliReader, mHelper.GimliReader.LmbConfig);
            var filePath = Path.Combine(updateFolderPath, "module.update");
            if (!File.Exists(filePath) || mup.UpdateNeeded((int)mHelper.GimliReader.LmbConfig.DeviceType, filePath) == 0)
            {
                Trace.WriteLine($"UpdateFirmware(): Before calling EnableTempPollingandNfc()");
                mHelper.EnableTempPollingAndNfc();
                mLogger.Info(LogCategoryEnum.Host, Properties.Resources.Firmware_Update_Not_Found_Error);
                ReportProgressOrError(100, FirmwareUpdateStatus.Status.Done, null);
                return;
            }

            UpdateFirmwareAndReportProgress(mup, new DeviceModuleUpdaterArgs(filePath, (int)mHelper.GimliReader.LmbConfig.DeviceType));
        }

        #endregion

        // ----------------------------------------------------------------------------------------------------------
        //                                         Injector Maintenance
        // ----------------------------------------------------------------------------------------------------------

        private void ExecuteInjectionAction(Func<bool> injectionAction, IInjectorSpecificAction typedAction, IEnumerable<InjectorConfiguration> injectorConfig, int volume)
        {
            var availableInjectors = injectorConfig.Select(i => i.Name).ToList();

            SendInjectorUpdate(typedAction, availableInjectors, InjectorStatus.Running, mActorService);

            bool injectionSucceeded = false;

            try
            {
                if (mHelper.IsSimulated)
                {
                    injectionSucceeded = mSimulatedInjectionSucceeds;
                    if (mSimulatedInjectionSucceeds)
                    {
                        InjectionTimeSimulator(volume);
                        SetTubingVolume(typedAction.Injector, volume, true);
                    }
                    else
                    {
                        Thread.Sleep(1000);
                        mActorService.Get<WebServiceProxyActor>()
                            .Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
                            {
                                Parameters = new MessageParameters
                                {
                                    Message = Properties.Resources.ErrorExecutingInjection,
                                    DisplayType = MessageDisplayType.DialogOk,
                                    MessageCode = MessageStatusCodes.InjectionError,
                                    MessageType = MessageType.Error
                                }
                            }));
                    }
                }
                else
                {
                    injectionSucceeded = injectionAction();
                }
            }
            catch (Exception exc)
            {

            }
            finally
            {
                SendInjectorUpdate(typedAction, availableInjectors, injectionSucceeded ? InjectorStatus.Finished : InjectorStatus.FinishedWithError, mActorService);
            }
        }

        private static void SendInjectorUpdate(IInjectorSpecificAction typedAction, List<InjectorSel> availableInjectors, InjectorStatus targetState, IActorService actorService)
        {
            var injectorUpdate = new InjectorsUpdate();
            if (typedAction.Injector.HasFlag(InjectorSel.Injector1) && typedAction.Injector.HasFlag(InjectorSel.Injector2))
            {
                injectorUpdate.Injector1 = targetState;
                injectorUpdate.Injector2 = targetState;
            }
            if (typedAction.Injector == InjectorSel.Injector1)
            {
                injectorUpdate.Injector1 = targetState;
                injectorUpdate.Injector2 = availableInjectors.Contains(InjectorSel.Injector2)
                    ? InjectorStatus.Idle
                    : InjectorStatus.NotConnected;
            }
            if (typedAction.Injector == InjectorSel.Injector2)
            {
                injectorUpdate.Injector1 = availableInjectors.Contains(InjectorSel.Injector1)
                    ? InjectorStatus.Idle
                    : InjectorStatus.NotConnected;
                injectorUpdate.Injector2 = targetState;
            }
            actorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new InjectorStatusChangedMessage { Injectors = injectorUpdate }));
        }

        private void SetTubingVolume(InjectorSel injectors, double volume, bool addToOld)
        {
            var flags = GetUniqueFlags<InjectorSel>(injectors);
            var injectorsVolumes = new Dictionary<InjectorSel, double>();
            var simulatedStore = mHelper.GetTubingStore();
            foreach (var inj in flags)
            {
                if (addToOld)
                {
                    var oldVolume = simulatedStore[inj].Value;
                    injectorsVolumes[inj] = volume + oldVolume;
                }
                else
                {
                    injectorsVolumes[inj] = volume;
                }
            }
            SetDispensedVolume(injectorsVolumes);
        }

        public static IEnumerable<T> GetUniqueFlags<T>(Enum flags)
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException("The generic type parameter must be an Enum.");

            if (flags.GetType() != typeof(T))
                throw new ArgumentException("The generic type parameter does not match the target type.");

            ulong flag = 1;
            foreach (var value in Enum.GetValues(flags.GetType()).Cast<T>())
            {
                ulong bits = Convert.ToUInt64(value);
                while (flag < bits)
                {
                    flag <<= 1;
                }

                if (flag == bits && flags.HasFlag(value as Enum))
                {
                    yield return value;
                }
            }
        }

        private void SetDispensedVolume(Dictionary<InjectorSel, double> values)
        {
            var store = mHelper.GetTubingStore();

            foreach (var injectorSel in values.Keys)
            {
                store[injectorSel].Value = values[injectorSel];
            }

	        TubingStoreWriter(store);
        }

        private void UpdateFirmwareAndReportProgress(IDeviceModulesUpdater mup, DeviceModuleUpdaterArgs args)
        {
            mLogger.Info(LogCategoryEnum.Host, "GimliDeviceMaintenanceController::UpdateFirmware() - update firmware started");
            try
            {
                var err = mup.UpdateAllModules(this, args);
                if (!string.IsNullOrEmpty(err))
                    throw new Exception(err);
                //for (var i = 0; i < 100; i++)
                //{
                //    Thread.Sleep(500);
                //    ReportProgress(i + 1);
                //    if(i == 20)
                //        throw new Exception("error");
                //}
                mLogger.Info(LogCategoryEnum.Host, "GimliDeviceMaintenanceController::UpdateFirmware() - update firmware finished");
                ReportProgressOrError(100, FirmwareUpdateStatus.Status.Done, null);
            }
            catch (Exception e)
            {
                mLogger.Error(LogCategoryEnum.Host, "GimliDeviceMaintenanceController::UpdateFirmware() - update firmware failed", e);
                ReportProgressOrError(100, FirmwareUpdateStatus.Status.Error, e.Message);
            }
            finally
            {
                mHelper.GimliReader.Disconnect();
            }
            //make sure that the instrument controller performs Init so the reader is properly detected once rebooted
            //throw new Exception("Reader not initialized");
        }

        private void ReportProgressOrError(int percentProgress, FirmwareUpdateStatus.Status status, string statusText)
        {
            mLogger.Info(LogCategoryEnum.Host, $"GimliDeviceMaintenanceController::UpdateFirmware() - ReportProgress {percentProgress} % done");

            var actor = mActorService.Get<AppManagerProxyActor>();

            mLogger.Info(LogCategoryEnum.Host, $"GimliDeviceMaintenanceController::AppManagerProxy reachable {actor != null}");

            var msg = new SetFirmwareUpdateStatusMessage
            {
                UpdateStatusPercentage = percentProgress,
                Status = status,
                UpdateStatusText = statusText
            };

            actor?.Tell(MessageExtensions.CreateRequestMessage(msg));
        }

        public void ReportProgress(int percentProgress)
        {
            mLogger.Info(LogCategoryEnum.Host, $"GimliDeviceMaintenanceController::UpdateFirmware() - ReportProgress {percentProgress} % done");

            ReportProgressOrError(percentProgress, FirmwareUpdateStatus.Status.Ok, null);
        }
    }
}
