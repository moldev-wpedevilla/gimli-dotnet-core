﻿using System;
using System.Globalization;
using System.Threading;
using Apex.Device;
using Apex.Extensibility.Device;
using Apex.Extensibility.Interfaces;
using Gimli.InstrumentController.DataHandling;
using Gimli.InstrumentController.Multimode;
using Gimli.InstrumentController.Properties;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Settings;
using System.Threading.Tasks;
using Gimli.InstrumentServer;
using Gimli.JsonObjects.Serialize;
using Gimli.Shared.Actors.Messages.Instrument;
using Gimli.InstrumentServer.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;

namespace Gimli.InstrumentController
{
    public class GimliRead
    {
        private IMethodController mMethodController;
        private readonly FluoroGimli mReader;
        private readonly Plate mPlate;
        private readonly int mSnapshotNumber;
        private int mId;
        private Document mDocument;
        private IDataCharger mDataCharger;
        private DateTime mStartTime;
        private float mEstimTime;
        private readonly ILogService mLogger;
	    private readonly IActorService mActorService;

        public static GimliRead SetupNewRead(Plate plate, int snapshotNumber, FluoroGimli reader, ILogService logger, IActorService actorService)
        {
            return new GimliRead(plate, snapshotNumber, reader, logger, actorService);
        }

        private GimliRead(Plate plate, int snapshotNumber, FluoroGimli reader, ILogService logger, IActorService actorService)
        {
            mReader = reader;
            mPlate = plate;
            mSnapshotNumber = snapshotNumber;
            mLogger = logger;
	        mActorService = actorService;
        }

        public bool StartMethod(CancellationToken cancellationToken, ManualResetEventSlim ignorePlateheightError)
        {
            var measSett = mPlate.SnapShot[mSnapshotNumber].Settings;

            //var labware = MicroplateConverter.GenerateLabwareFromMicroplate(mPlate.Settings.Plate);

            try
            {
                //let GUI reset the progress bar
                NotifyWithMeasuringInformation(0, 0, new Point[0]);
                //var method = DetectionMethodFactory.Generate(measSett, mReader);
                var transl = SettingsToMethodTranslator.Generate(mPlate.Settings, measSett);
                var method = transl.Translate(mReader);
                mDataCharger = DataChargerFactory.GenerateAppropriateCharger(mPlate.SnapShot[mSnapshotNumber], mPlate.Settings.Plate);
                if (mReader.GeTransportController() != null)// && mReader.GeTransportController().CameraFunctions.CanReadPlateHeight)
                {
                    CloseDrawer();
                    if (method.MeasTech != MeasureTech.Absorbance && !measSett.DetectionSetting.ReadFromBottom.ToBool() && !mPlate.PlateName.Contains("Cuvette")) { 
                        var definedPlateHeight = mPlate.Settings.IsLidded.ToBool()?mPlate.Settings.Plate.PlateSpecification.PlateHeightWithLid: mPlate.Settings
                            .Plate.PlateSpecification.PlateHeight;
                        var plateheightError = CheckPlateHeight(definedPlateHeight);
	                    if (!string.IsNullOrEmpty(plateheightError))
	                    {
		                    mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
		                    {
			                    Parameters = new MessageParameters
			                    {
				                    Message = plateheightError + "\n\r" +  Resources.Do_You_Want_To_Proceed_With_The_Read,
									MessageCode = MessageStatusCodes.PlateHeightError,
									DisplayType = MessageDisplayType.DialogDecision,
									MessageType = MessageType.Error
			                    }
		                    }));

		                    try
		                    {
								ignorePlateheightError.Wait(cancellationToken);
							}
							catch (OperationCanceledException)
		                    {
		                    }
	                    }

                        //mPlate.Settings.Barcode = GetBarcode();
                    }
                }
                mMethodController = method.GetController(mReader, transl.Layout);// transl.TranslateLayout(measSett.ReadArea, mPlate.Settings, measSett));
                HookEvents();
                mEstimTime = mMethodController.RealEstimTime + GetBeforeShakingEstimate(measSett);
	            mPlate.Settings.MeasureDate = mStartTime = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
				mPlate.Settings.InstrumentName = mReader.AliasName;
                mPlate.Settings.InstrumentSerial = mReader.SerialNr;
                mPlate.Settings.UserName = mDocument?.UserNameGuid;

                if (!cancellationToken.IsCancellationRequested) NotifyWithMeasuringInformation(mEstimTime, 0, new Point[0]);
                DoBeforeShaking(measSett, transl.Labware, cancellationToken);
                //user cancelled the read already
                if (cancellationToken.IsCancellationRequested)
                {
                    Task.Run(() =>
                    {
                        TryEjectPlateIfNeeded();
                        OnReadStatusChanged(ReaderStatus.Idle, string.Empty);
                    });
                    UnHookEvents();
                    return true;
                }
                mMethodController.Start(transl.Labware, true);
                cancellationToken.Register(StopRead);
            }
            catch (Exception e)
            {
                OnReadStatusChanged(ReaderStatus.Error, e.Message);
                return false;
            }
            OnReadStatusChanged(ReaderStatus.Busy, string.Empty);
            return true;
        }

        private string GetBarcode()
        {
            if (!mReader.Connected) return string.Empty;
            if (!mReader.GeTransportController().CameraFunctions.CanReadBarcode) return string.Empty;
            return mReader.GeTransportController().CameraFunctions.Barcode();
        }

        private string CheckPlateHeight(double definedHeight)
        {
            if (!mReader.Connected) return null;
            if (!mReader.GeTransportController().CameraFunctions.CanReadPlateHeight) return null;

            var measuredHeight = mReader.GeTransportController().CameraFunctions.PlateHeight();
            if (measuredHeight < 0.1)
                throw new Exception(Resources.NoPlateInserted);
            if (measuredHeight < (definedHeight - 2000)/1000)
                return 
                    $"{Resources.PlateHeightTooLow} ({Resources.PlateMeasuredHeight} {measuredHeight.ToString("0.00")} - {Resources.PlateDefinedHeight} {(definedHeight/1000).ToString("0.00")})";
            if (measuredHeight > (definedHeight + 500)/1000)
                return 
                    $"{Resources.PlateHeightTooHigh} ({Resources.PlateMeasuredHeight} {measuredHeight.ToString("0.00")} - {Resources.PlateDefinedHeight} {(definedHeight/1000).ToString("0.00")})";

	        return null;
        }

        private void CloseDrawer()
        {
            if (!mReader.Connected) return;
            if (mReader.GeTransportController().IsPlateDrawerOpen)
                mReader.GeTransportController().LoadPlate(true);
            mLogger.Info(LogCategoryEnum.Host, "Gimli::CloseDrawer: PlateHeight -> " + mReader.GeTransportController().CameraFunctions.PlateHeight());
        }

        private void DoBeforeShaking(MeasurementSettings measSett, ILabware labware, CancellationToken cancellationToken)
        {
            if (GetBeforeShakingEstimate(measSett) > 0)
            {
                mReader.Shaker.OnLabwareChanged(labware);
                mReader.Shaker.Shake(true);
                do
                {
                    Thread.Sleep(200);
                    if (cancellationToken.IsCancellationRequested)
                    {
                        mReader.Shaker.StopShaking();
                    }
                } while (mReader.Shaker.IsShaking);
            }
        }

        private float GetBeforeShakingEstimate(MeasurementSettings measSett)
        {
            if (!measSett.Shake.DoShake.ToBool())
                return 0;
            mReader.Shaker.Duration = measSett.Shake.Duration.ToInt();
            mReader.Shaker.Intensity = ShakeConverter.GetShakeIntensity(measSett.Shake.Intensity);
            mReader.Shaker.Type = ShakeConverter.GetShakeType(measSett.Shake.Mode);
            return mReader.Shaker.EstimTime;
        }

        public void SetDataReceiver(int id, Document doc)
        {
            mId = id;
            mDocument = doc;
        }
       
        private void HookEvents()
        {
            if (mMethodController == null)
                return;
            mMethodController.DataReceived += MethodControllerDataReceived;
            mMethodController.Error += MethodControllerError;
            mMethodController.MethodPending += MethodControllerMethodPending;
        }
        private void UnHookEvents()
        {
            if (mMethodController == null)
                return;
            mMethodController.DataReceived -= MethodControllerDataReceived;
            mMethodController.Error -= MethodControllerError;
            mMethodController.MethodPending -= MethodControllerMethodPending;
        }

        private void MethodControllerMethodPending(object sender, MethodPendingEventArgs args)
        {
            mLogger.Info(LogCategoryEnum.Host, "Gimli::MethodPending: " + args.Reason);
            args.Resuming = true;
            if (args.Reason == MethodPendingReason.Tempering)
            {
                double temp;
                if(double.TryParse(args.Info.ToString(), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out temp))
                    OnTemperatureChanged(temp);
            }
        }

        private const string CanceledByUserDeviceCode = "140";

        private void MethodControllerError(object sender, RunMethodErrorEventArgs arg)
        {
            var devEx = arg.Error as DeviceException;
            if (devEx != null)
            {
                if (devEx.DeviceCode == CanceledByUserDeviceCode)
                {
                    Task.Run(() =>
                    {
                        TryEjectPlateIfNeeded();
                        OnReadStatusChanged(ReaderStatus.Idle, string.Empty);
                    });
                }
                else
                    OnReadStatusChanged(ReaderStatus.Error, mReader.FormatErrorText(devEx));
            }
            else
                OnReadStatusChanged(ReaderStatus.Error, arg.Error.Message);

            UnHookEvents();
        }

        private void MethodControllerDataReceived(object sender, RunMethodEventArgs args)
        {
            mLogger.Info(LogCategoryEnum.Host, "Gimli::DataReceived: " + args.EventNr);
            if (args.StartRow == 0)
            {
                Task.Run(() =>
                {
                    TryEjectPlateIfNeeded();
                    OnReadStatusChanged(ReaderStatus.Idle, string.Empty);
                });
                UnHookEvents();
                return;
            }

            var measWells = mDataCharger.Fill(mMethodController.Data, mPlate.SnapShot[mSnapshotNumber], args);
            if (measWells.Length == 0)
                return;
            
            double percDone, remainTime;
            GetProgressInformation(args, out percDone, out remainTime);
            NotifyWithMeasuringInformation(remainTime, percDone, measWells);
        }

        private void TryEjectPlateIfNeeded()
        {
            try
            {
                if (mReader.EjectPlateAfterRead)
                {
                    mLogger.Info(LogCategoryEnum.Host, "Trying to eject the plate after read");
                    Thread.Sleep(500);
                    if(mReader.WaitForInstrumentIsReadyForNextCommand(10, 500))
                        mReader.GeTransportController().EjectUnLockedPlate();
                }
            }
            catch (Exception e)
            {
                mLogger.Error(LogCategoryEnum.Host, "Eject Plate After Read failed.",  e);
            }
        }

        private void NotifyWithMeasuringInformation(double timeLeft, double percDone, Point[] measuredWells)
        {
            //if (mDataEvent == null) return;
            // use raw data serializer to reduce system load on deserialization on client side

	        var serialization = DocumentAndSerializedWells.SerializeDocument(mDocument);
	        var firstSnapshotKey = serialization.GetDictionaryKeyOfFirstSnapshot();

			var payload = new RuntimeData
			{
				Document = serialization.Document,
				Status = MeasurementStatus.Measuring,
				RemainingTime = timeLeft,
				PercentDone = (int)(percDone * 100),
				MeasuredWells = measuredWells,
				WellSeriailization = !serialization.WellSeriailization.ContainsKey(firstSnapshotKey) ? null : serialization.WellSeriailization[firstSnapshotKey],
				WellSeriailizationSize = !serialization.WellSeriailizationSize.ContainsKey(firstSnapshotKey) ? 0 : serialization.WellSeriailizationSize[firstSnapshotKey],
			};

			mActorService.Get<InstrumentServerRoutingActor>().Tell(new DataeventMessage {Payload = payload});
        }

	    private void GetProgressInformation(RunMethodEventArgs args, out double percDone, out double remainingTime)
        {
            var elapsedTime = DateTime.Now - mStartTime;
            //get remaining time first according to start and estimates
            remainingTime = Math.Max(mEstimTime - elapsedTime.TotalSeconds, 0);
            percDone = args.EventNr/(double)mMethodController.EventCount;
            //to avoid big jumps at the beginning we don't correct for the very first two events
            if(args.EventNr < 3)
                return;
            //if we are doing kinetic we dont correct until a third is already done
            if (mPlate.SnapShot[mSnapshotNumber].Settings.MeasurementType == MeasurementType.Kinetic && percDone < 33)
                return;
            remainingTime = elapsedTime.TotalSeconds/percDone*(1 - percDone);
        }

        public event ReadStatusChangedHandler ReadStatusChanged;
        public event EventHandler<TemperatureChangedEventArgs> TemperatureChanged;
        public event EventHandler<ReaderMessageEventArgs> ConfirmMessage;
        public delegate void ReadStatusChangedHandler(ReaderStatus status, string message);

        protected void OnConfirmMessage(object sender, ReaderMessageEventArgs e)
        {
            ConfirmMessage?.Invoke(sender, e);
        }
        protected void OnTemperatureChanged(double temperature)
        {
            TemperatureChanged?.Invoke(this, new TemperatureChangedEventArgs(temperature, false));
        }

        protected void OnReadStatusChanged(ReaderStatus readStatus, string message)
        {
            ReadStatusChanged?.Invoke(readStatus, message);
        }

        private void StopRead()
        {
            mMethodController?.Stop();
        }
    }
}
