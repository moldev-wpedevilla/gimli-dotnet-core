﻿using Akka.Actor;
using Akka.Actor.Internal;
using Akka.Util;
using System;
using System.Collections.Generic;

namespace MolDev.ActorHost
{
  /*class OurDeadlineFailureDetector: DeadlineFailureDetector
  {

  }
  class OurRemoteActorRefProvider : RemoteActorRefProvider
  {
    public OurProvider(string systemName, Settings settings, EventStream eventStream):base(systemName, settings, eventStream)
    {

    }
  }*/
  class ResumeStrategy : SupervisorStrategyConfigurator
  {
    public override SupervisorStrategy Create()
    {
      return new ResumeStrategyInstance();
    }
  }
  class ResumeStrategyInstance : SupervisorStrategy
  {
    public override IDecider Decider
    {
      get
      {
        return DefaultDecider;
      }
    }

    public override void HandleChildTerminated(IActorContext actorContext, IActorRef child, IEnumerable<IInternalActorRef> children)
    {
    }

    public override ISurrogate ToSurrogate(ActorSystem system)
    {
      throw new NotImplementedException();
    }

    protected override Directive Handle(IActorRef child, Exception x)
    {
      return Directive.Resume;
    }

    [Obsolete]
    protected override void ProcessFailure(IActorContext context, bool restart, Exception cause, ChildRestartStats failedChildStats, IReadOnlyCollection<ChildRestartStats> allChildren)
    {
    }

    protected override void ProcessFailure(IActorContext context, bool restart, IActorRef child, Exception cause, ChildRestartStats stats, IReadOnlyCollection<ChildRestartStats> children)
    {
    }
  }
}
