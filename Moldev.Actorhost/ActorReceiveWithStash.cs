﻿using Akka.Actor;
using MolDev.Common.Akka.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MolDev.ActorHost
{
  public class ActorReceiveWithStash : ActorReceiveBase, IWithUnboundedStash
  {
    private class MessageInfo
    {
      public object Message { get; set; }
      public IActorRef Sender { get; set; }
    }

    private Queue<MessageInfo> _lowPriorityStash = new Queue<MessageInfo>();

    public ActorReceiveWithStash(IActorService actorService) : base(actorService) { }

    private IStash _stash;

    IStash IActorStash.Stash
    {
      get { return _stash; }
      set { _stash = value; }
    }

    public void RecieveLater(Func<object, bool> lowPriority = null)
    {
      ReceiveAny(m =>
      {
        if (lowPriority != null && lowPriority(m))
        {
          _lowPriorityStash.Enqueue(new MessageInfo { Message = m, Sender = Sender });
        }
        else
        {
          _stash.Stash();
        }
      });
    }

    public void Stash()
    {
      _stash.Stash();
    }

    public void ResetBehavior()
    {
      _stash.UnstashAll();
      UnbecomeStacked();
      while (_lowPriorityStash.Count > 0)
      {
        var msg = _lowPriorityStash.Dequeue();
        Task.Run(() => msg.Message).PipeTo(Self, msg.Sender);
      }
    }
  }
}