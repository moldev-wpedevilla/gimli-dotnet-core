﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;
using System;

namespace MolDev.ActorHost
{
  internal class WindsorInstaller : IWindsorInstaller
  {
    private Action<IWindsorContainer> additionalInstall;
    private string _actorServiceConfigFileName = null;

    public WindsorInstaller(Action<IWindsorContainer> additionalInstall, string akkaConfigFile)
    {
      this.additionalInstall = additionalInstall;
      _actorServiceConfigFileName = akkaConfigFile;
    }

    public void Install(IWindsorContainer container, IConfigurationStore store)
    {
      container.Register(Component.For<IWindsorContainer>().Instance(container));

      container.Register(Component.For<ILogService>().ImplementedBy<LogService>()
                                                     .DependsOn(new
                                                     {
                                                       loggerName = Consts.MolDevWebLogger
                                                     })
                                                     .LifeStyle.Singleton);

      container.Register(Component.For<IActorService>().ImplementedBy<ActorService>()
                                                       .DependsOn(new
                                                       {
                                                         container = container,
                                                         actorServiceConfigFileName = _actorServiceConfigFileName
                                                       })
                                                       .LifeStyle.Singleton);

      if (additionalInstall != null) additionalInstall(container);
    }
  }
}
