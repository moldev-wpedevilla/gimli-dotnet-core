﻿using MolDev.Common.Akka.Interfaces;
using System;

namespace MolDev.ActorHost.InternalMessages
{
  internal class WatchForMessage
  {
    public IActor Actor { get; set; }
  }
}
