﻿using MolDev.Common.Akka.Interfaces;
using System;

namespace MolDev.ActorHost.InternalMessages
{
  internal class TryGetActor
  {
    public string ActorPath { get; set; }
  }

  internal class TryCheckActor
  {
    public IActor ActorRef { get; set; }
    public string ActorPath { get; set; }
  }
}
