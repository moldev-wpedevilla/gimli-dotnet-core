﻿using MolDev.Common.Akka.Interfaces;
using System;

namespace MolDev.ActorHost.InternalMessages
{
  internal class ActorWasFoundInternal
  {
    public IActor Actor { get; set; }
    public string ActorPath { get; set; }
    public Type ActorType { get; set; }
  }
}
