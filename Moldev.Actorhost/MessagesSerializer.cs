﻿using Akka.Actor;
using Akka.Serialization;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace MolDev.ActorHost
{
  class MessagesSerializer : Serializer
  {
    private BinaryFormatter _bf;

    public MessagesSerializer(ExtendedActorSystem system) : base(system)
		{
      _bf = new BinaryFormatter(null, new StreamingContext(StreamingContextStates.All, system));
    }

    public override int Identifier
    {
      get
      {
        return 100;
      }
    }

    public override bool IncludeManifest
    {
      get
      {
        return false;
      }
    }

    public override byte[] ToBinary(object obj)
    {
      byte[] array = null;
      using (MemoryStream ms = new MemoryStream())
      {
        _bf.Serialize(ms, obj);
        array = ms.ToArray();
      }
      return array;
    }

    public override object FromBinary(byte[] bytes, Type type)
    {
      object res = null;
      using (MemoryStream ms = new MemoryStream(bytes))
      {
        res = _bf.Deserialize(ms);
      }
      return res;
    }
  }
}
