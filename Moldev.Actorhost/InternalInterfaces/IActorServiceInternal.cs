﻿using System.Collections.Generic;

namespace MolDev.ActorHost.InternalInterfaces
{
  internal interface IActorServiceInternal
  {
    IEnumerable<string> GetAllRemoteAddresses();

    void RemoveActorsFromDictionaryByPath(string fullPath);
  }
}
