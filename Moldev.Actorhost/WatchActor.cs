﻿using MolDev.ActorHost.InternalMessages;
using MolDev.Common.Akka.Interfaces;
using System;

namespace MolDev.ActorHost
{
  public class WatchActor<TActor> : ActorReceiveBase where TActor : class
  {
    private readonly int _timerInterval;
    private readonly int _maxTryNumber;
    private int _tryNumber = 0;
    private IActor _actorRef;
    private string _actorPath;

    public WatchActor(IActorService actorService) : base(actorService)
    {
      _timerInterval = 5000;
      _maxTryNumber = 1;
      Receive<TryCheckActor>(msg => OnTryCheckActor(msg));
      Receive<TryFindActor>(msg => TimerMethod());
    }
    private void OnTryCheckActor(TryCheckActor msg)
    {
      _actorRef = msg.ActorRef;
      _actorPath = msg.ActorPath;
      StartScheduler();
    }
    private void StartScheduler()
    {
      ScheduleTellOnce(TimeSpan.FromMilliseconds(_timerInterval), new TryFindActor());
    }
    private void TimerMethod()
    {
      try
      {
        var result = _actorRef.Ask<CheckActorResult>(new CheckActor(), 60000).Result;
        if (result == null) throw new NullReferenceException();
      }
      catch
      {
        _tryNumber++;
      }

      if (_tryNumber >= _maxTryNumber)
      {
        ParentRef.Tell(new ActorTerminatedInternal() { Actor = _actorRef, ActorPath = _actorPath, ActorType = GetType() });
        StopSelf();
      }
      else
      {
        StartScheduler();
      }
    }
  }
}
