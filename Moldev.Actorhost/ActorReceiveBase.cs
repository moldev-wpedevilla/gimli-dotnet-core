﻿using Akka.Actor;
using Akka.DI.Core;
using Akka.Event;
using MolDev.ActorHost.InternalMessages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Akka.Messages;
using MolDev.Common.Helpers;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;
using MolDev.Resources.UI;
using MolDev.Resources.System;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;

namespace MolDev.ActorHost
{
  public class ActorReceiveBase : ReceiveActor//, IActorReceiveBase
  {
    private Dictionary<Type, Dictionary<string, IActor>> _childActors;

    protected ILoggingAdapter Log { get; private set; }
    protected IActorService ActorService { get; private set; }
    protected IActor SenderRef { get { return new ActorRef(Sender); } }
    protected IActor ParentRef { get; private set; }
    protected IActor SelfRef { get; private set; }
    public ActorReceiveBase(IActorService actorService)
    {
      Log = Context.GetLogger();
      ActorService = actorService;
      ParentRef = new ActorRef(Context.Parent);
      SelfRef = new ActorRef(Self);

      Receive<CheckActor>(msg => SenderRef.Tell(new CheckActorResult()));
      Receive<ActorWasFoundInternal>(msg => OnActorWasFoundInternal(msg));
      Receive<ActorTerminatedInternal>(msg => OnActorTerminatedInternal(msg));
      Receive<PoisonPillInternal>(msg => StopSelf());
      //Receive<GetLog>(request => OnGetLog(request));
      //Receive<StopService>(request => OnStopService(request));
      //Receive<EnvironmentExit>(request => StopService());
    }
    protected enum ActorType
    {
      NullActor,
      SenderRef,
      ParentRef,
      SelfRef
    }
    protected void TryReceive<RequestClass, ResponseClass>(Func<RequestClass, ResponseClass> method, ActorType actorType = ActorType.NullActor, bool forward = false, bool begEndTextOn = false)
                                                          where ResponseClass : ResponseMessageBase, new()
    {
      Receive<RequestClass>(request =>
      {
        IActor actor = null;
        switch (actorType)
        {
          case ActorType.ParentRef: { actor = ParentRef; break; }
          case ActorType.SenderRef: { actor = SenderRef; break; }
          case ActorType.SelfRef: { actor = SelfRef; break; }
        }

        try
        {
          string msgType = request.GetType().ToString();
          if (begEndTextOn)
          {
            Log.Info(SystemMessages.ProcessingStarted, msgType);
          }

          var res = method(request);

          if (res != null && actor != null)
          {
            if (!forward)
            {
              actor.Tell(res);
            }
            else
            {
              actor.Forward(res);
            }
          }

          if (begEndTextOn)
          {
            Log.Info(SystemMessages.ProcessingFinished, msgType);
          }
        }
        catch (ValidationException vex)
        {
          Log.Error(vex, vex.Message);
          if (actor != null)
          {
            var message = UserMessagesEx.GetString(vex.Message, request.GetCulture());
            var msg = new ResponseClass()
            {
              Status = ResultStatusEnum.ValidationError,
              Error = new ErrorInfo
              {
                Message = message ?? vex.Message,
              }
            };
            if (!forward)
            {
              actor.Tell(msg);
            }
            else
            {
              actor.Forward(msg);
            }
          }
        }
        catch (Exception ex)
        {
          Log.Error(ex, string.Empty);
          if (actor != null)
          {
            var msg = new ResponseClass() { Status = ResultStatusEnum.Error, Error = new ErrorInfo { Message = ex.Message, Details = ex.StackTrace } };
            if (!forward)
            {
              actor.Tell(msg);
            }
            else
            {
              actor.Forward(msg);
            }
          }
        }
      });
    }

    protected void TryReceive<RequestClass>(Action<RequestClass> method, ActorType actorType = ActorType.NullActor, bool forward = false, bool begEndTextOn = false)
    {
      TryReceive<RequestClass, ResponseMessageBase>(m => { method(m); return null; }, actorType, forward, begEndTextOn);
    }
    protected void TryReceive<RequestClass, ResponseClass>(Action<RequestClass> method, ActorType actorType = ActorType.NullActor, bool forward = false, bool begEndTextOn = false)
                                            where ResponseClass : ResponseMessageBase, new()
    {
      TryReceive<RequestClass, ResponseClass>(m => { method(m); return null; }, actorType, forward, begEndTextOn);
    }

    //private void OnGetLog(GetLog request)
    //{
    //  try
    //  {
    //    if (AdminMessageHandler(request))
    //    {
    //      var response = MakeGetLogResponse(request);
    //      SenderRef.Tell(response);
    //    }
    //  }
    //  catch (Exception ex)
    //  {
    //    SenderRef.Tell(new ResponseMessage<GetLogResponse>(ResultStatusEnum.ValidationError, new ErrorInfo() { Message = ex.Message }));
    //  }
    //}
    //protected virtual bool AdminMessageHandler(RequestMessage<AdminMessage> request)
    //{
    //  bool ret = string.IsNullOrEmpty(request.Model.Id);
    //  if (!ret)
    //  {
    //    request.Model.Id = string.Empty;
    //    SelfRef.Forward(request);
    //  }
    //  return ret;
    //}
    //private void OnStopService(StopService request)
    //{
    //  try
    //  {
    //    if (AdminMessageHandler(request))
    //    {
    //      SenderRef.Tell(new ResponseMessage<AdminMessage>(request.Model, request.ExecutionContext));
    //      ScheduleTellOnce(TimeSpan.FromSeconds(Consts.EnviromentExitMessageDelay), new EnvironmentExit());//sb dbg
    //    }
    //  }
    //  catch (Exception ex)
    //  {
    //    SenderRef.Tell(new ResponseMessage<AdminMessage>(ResultStatusEnum.ValidationError, new ErrorInfo() { Message = ex.Message }));
    //  }
    //}
    ////sb dbg
    //private void StopService()
    //{
    //  Environment.Exit(0);
    //}
    ////sb dbg
    //private GetLogResponse MakeGetLogResponse(GetLog request)
    //{
    //  var ret = new GetLogResponse(new AdminMessage() { Type = request.Model.Type, Id = request.Model.Id }, request.ExecutionContext) { ZippedData = new byte[0] };
    //  var logConf = LogManager.Configuration;
    //  var vars = logConf.Variables[Consts.LogDirEnv];
    //  var source = vars.Text;
    //  bool b = false;
    //  if (vars != null)
    //  {
    //    var s = source.Replace(Consts.EnviromentVarPref, Consts.Percent).Replace("}", Consts.Percent);
    //    source = Environment.ExpandEnvironmentVariables(s);
    //    b = Directory.Exists(source);
    //    if (b)
    //    {
    //      var result = source.Substring(0, source.LastIndexOf(@"/") + 1) + Guid.NewGuid().ToString() + Consts.ZipFileExt;
    //      using (var zipHelper = new ZipDirectoryHelper(source, result))
    //      {
    //        ret.ZippedData = zipHelper.MakeZipFile().GetZippedData();
    //      }
    //    }
    //  }
    //  return ret;
    //}

    protected void Discover<TActor>(string path) where TActor : class
    {
      GetChild<DiscoveryActor<TActor>>(path.ToLower(), true, new TryGetActor() { ActorPath = path });
    }
    protected void Undiscover<TActor>(string path) where TActor : class
    {
      StopChild<WatchActor<TActor>>(path.ToLower());
    }
    protected void Watch<TActor>(IActor actor, string id) where TActor : class
    {
      GetChild<WatchActor<TActor>>(actor.PathStr.ToLower(), true, new TryCheckActor() { ActorRef = actor, ActorPath = id });
    }
    protected void Unwatch<TActor>(IActor actor) where TActor : class
    {
      StopChild<WatchActor<TActor>>(actor.PathStr.ToLower());
    }
    private void OnActorTerminatedInternal(ActorTerminatedInternal msg)
    {
      if (_childActors.ContainsKey(msg.ActorType)) _childActors[msg.ActorType].Remove(msg.Actor.PathStr.ToLower());
      SelfRef.Tell(new ActorTerminated() { Actor = msg.Actor, ActorPath = msg.ActorPath });
    }
    private void OnActorWasFoundInternal(ActorWasFoundInternal msg)
    {
      if (_childActors.ContainsKey(msg.ActorType)) _childActors[msg.ActorType].Remove(msg.ActorPath.ToLower());
      SelfRef.Tell(new ActorWasFound() { Actor = msg.Actor, ActorPath = msg.ActorPath });
    }
    public IActor GetChild<TActor>(string name, bool createIfMissing = true, object createMessage = null, bool forward = false, bool useOnes = false) where TActor : class
    {
      IActor child;
      if (!useOnes)
      {
        if (_childActors == null)
        {
          _childActors = new Dictionary<Type, Dictionary<string, IActor>>();
        }
        if (!_childActors.ContainsKey(typeof(TActor)))
        {
          _childActors[typeof(TActor)] = new Dictionary<string, IActor>();
        }

        if (_childActors[typeof(TActor)].TryGetValue(name, out child))
        {
          return child;
        }
      }

      if (!createIfMissing)
      {
        return null;
      }

      ActorService.Register<TActor>();
      Guid guid;
      var actor = Context.ActorOf(Context.DI().Props(typeof(TActor)), Guid.TryParse(name, out guid) ? name : Guid.NewGuid().ToString());
      if (actor == null)
      {
        return null;
      }

      child = new ActorRef(actor);
      if (!useOnes) _childActors[typeof(TActor)][name] = child;
      if (createMessage != null)
      {
        if (forward) child.Forward(createMessage);
        else child.Tell(createMessage);
      }
      return child;
    }

    public bool StopChild<TActor>(string name)
    {
      if (_childActors == null || !_childActors.ContainsKey(typeof(TActor)))
      {
        return false;
      }
      IActor child;
      if (!_childActors[typeof(TActor)].TryGetValue(name, out child))
      {
        return false;
      }
      _childActors[typeof(TActor)].Remove(name);
      ////sb 09/26/17      Context.Stop((child as ActorRef)?.Self);
      (child as ActorRef)?.Self.Tell(new PoisonPillInternal());//sb 09/26/17
      return true;
    }

    protected void StopSelf()
    {
      BeforeStopActor();
      Context.Stop(Self);
    }

    protected void TellChildren<TActor>(object message, bool forward = false)
    {
      if (_childActors == null || !_childActors.ContainsKey(typeof(TActor)))
      {
        return;
      }
      _childActors[typeof(TActor)].ToList().ForEach(p =>
      {
        if (forward) p.Value.Forward(message);
        else p.Value.Tell(message);
      });
    }

    protected int CountChild<TActor>()
    {
      if (_childActors == null || !_childActors.ContainsKey(typeof(TActor)))
      {
        return 0;
      }
      return _childActors[typeof(TActor)].Count;
    }

    protected override SupervisorStrategy SupervisorStrategy()
    {
      return new OneForOneStrategy(
          Decider.From(x =>
          {
              return Directive.Resume;
          }));
    }

    protected void ReceiveTimeout(Action<ReceiveTimeout> handler)
    {
      Receive(handler);
    }

    protected void ScheduleTellOnce(TimeSpan delay, object message)
    {
      ITellScheduler scheduler = Context.System.Scheduler as ITellScheduler;
      if (scheduler != null)
      {
        scheduler.ScheduleTellOnce(delay, Self, message, Self);
      }
    }

    protected ICancelable ScheduleTellRepeatedly(TimeSpan delay, TimeSpan interval, object message)
    {
      ITellScheduler scheduler = Context.System.Scheduler as ITellScheduler;
      if (scheduler != null)
      {
        var cancelable = new Cancelable(scheduler as IScheduler);
        scheduler.ScheduleTellRepeatedly(delay, interval, Self, message, Self, cancelable);
        return cancelable;
      }
      return null;
    }

    protected virtual void BeforeStopActor()
    {
      Log.Info("Actor {0} is stopped", this.ToString());
    }
  }
}