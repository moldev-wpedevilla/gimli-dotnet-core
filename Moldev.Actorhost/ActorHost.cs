﻿using Akka.Actor;
using Akka.Remote.Transport;
using Castle.Windsor;
using Castle.Windsor.Installer;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;
using System;
using System.Threading.Tasks;

namespace MolDev.ActorHost
{
  internal class ActorHost
  {
    private IWindsorContainer container = null;
    private IActorService actorService = null;

    private Action<IWindsorContainer> additionalInstall;
    private ILogService logger;
    public ActorHost(Action<IWindsorContainer> additionalInstall = null)
    {
      this.additionalInstall = additionalInstall;
    }

    public void Start()
    {
      container = new WindsorContainer().Install(new WindsorInstaller(additionalInstall, string.Empty));

      logger = container.Resolve<ILogService>();
      actorService = container.Resolve<IActorService>();
    }

    public void Stop()
    {
      actorService.ShutDown();
    }
  }

  public class CustomAdapter : ITransportAdapterProvider
  {
    public CustomAdapter() { }
    public Transport Create(Transport wrappedTransport, ExtendedActorSystem system)
    {
      return new TransportAdapter(wrappedTransport);
    }
  }

  /*public class MyAssociationHandle : AssociationHandle
  {
    public MyAssociationHandle(Address localAddress, Address remoteAddress, TaskCompletionSource<IHandleEventListener> tcs) :base(localAddress,remoteAddress)
    {
      this.ReadHandlerSource = tcs;
    }
    public override void Disassociate()
    {
    }

    public override bool Write(ByteString payload)
    {
      return true;
    }
  }*/
  public class TransportAdapter : Transport//, IAssociationEventListener//AbstractTransportAdapter
  {
    //private IAssociationEventListener _upstreamListener;
    public static Transport _wrappedTransport;

    public TransportAdapter(Transport wrappedTransport)// :base(wrappedTransport)
    {
      _wrappedTransport = wrappedTransport;
    }

    public override long MaximumPayloadBytes
    {
      get
      {
        return 1000000;
      }

      protected set
      {
      }
    }

    public override string SchemeIdentifier
    {
      get
      {
        return "tcp";
      }

      protected set
      {
      }
    }

    public override Task<AssociationHandle> Associate(Address remoteAddress)
    {
      return _wrappedTransport.Associate(remoteAddress);
    }

    public override bool IsResponsibleFor(Address remote)
    {
      return true;// _wrappedTransport.IsResponsibleFor(remote);
    }

    public override Task<Tuple<Address, TaskCompletionSource<IAssociationEventListener>>> Listen()
    {
      return _wrappedTransport.Listen();
    }

    public override Task<bool> Shutdown()
    {
      return _wrappedTransport.Shutdown();
    }

    /*protected override SchemeAugmenter SchemeAugmenter
    {
      get
      {
        return new SchemeAugmenter("TransportAdapter2");
      }
    }

    public override bool IsResponsibleFor(Address remote)
    {
      return true;
    }

    public void Notify(IAssociationEvent ev)
    {
      InboundAssociation aaa = (InboundAssociation)ev;
      var ia = new InboundAssociation(new MyAssociationHandle(aaa.Association.LocalAddress, aaa.Association.RemoteAddress, aaa.Association.ReadHandlerSource));//Address.Parse("akka.TransportAdapter2.tcp://EPRUPETW6498:12345")
    _upstreamListener.Notify(ia);
    }

    protected override void InterceptAssociate(Address remoteAddress, TaskCompletionSource<AssociationHandle> statusPromise)
    {
      statusPromise.SetResult(null);
    }

    protected override Task<IAssociationEventListener> InterceptListen(Address listenAddress, Task<IAssociationEventListener> listenerTask)
    {
      //_log.Warning("FailureInjectorTransport is active on this system. Gremlins might munch your packets.");
      listenerTask.ContinueWith(tr =>
      {
        // Side effecting: As this class is not an actor, the only way to safely modify state is through volatile vars.
        // Listen is called only during the initialization of the stack, and upstreamListener is not read before this
        // finishes.
        _upstreamListener = tr.Result;
      }, TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.OnlyOnRanToCompletion);
      return Task.FromResult((IAssociationEventListener)this);
    }*/
  }
}

