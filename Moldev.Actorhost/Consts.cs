﻿namespace MolDev.ActorHost
{
  public static class Consts
  {
    public const string DefaultAkkaConfigFileName = "app.config";
    public const string ActorAskTimeOutConfigKey = "moldevhost:ActorAskTimeOutMilliseconds";
    public const string ActorSystemNameConfigKey = "moldevhost:ActorSystemName";
    public const int ActorAskTimeOut = 60000;
    public const string MolDevWebLogger = "MolDev.ActorHost.Logger";
    public const string MolDevAkkaLogger = "MolDev.Akka.Logger";
    public const string AkkaLogerStarted = "NLog Logger for Akka started";
    public const string ConsistentHashingPool = "consistent-hashing-pool";
    public const string RoundRobinPool = "round-robin-pool";
    public const string AkkaActorDeployment = "akka.actor.deployment{0}";
    public const string AkkaActorDeploymentActorName = "./{0}";
    public const string CreateLocalParam = "create-local";
    public const string RouterParam = "router";
    public const string RemoteParam = "remote";
    public const string Akka = "akka";
    public const string O_user_1 = "{0}/user/{1}";
    public const string LoadTimerIntervalMs = "moldevhost:LoadTimerIntervalMs";
    public const string MaxTryNumber = "moldevhost:MaxTryNumber";
    public const string AkkaRemoteHeliosTcpPort = "akka.remote.netty.tcp.port";
    public const string AkkaRemoteHeliosTcpPublicHostName = "akka.remote.netty.tcp.public-hostname";
    public const string Percent = "%";
    public const string ZipFileExt = ".zip";
    public const string EnviromentVarPref = "${environment:";
    public const string LogDirEnv = "LogDirEnv";
    public const int EnviromentExitMessageDelay = 3;
  }
}
