﻿using Akka.Actor;
using Akka.Configuration;
using Akka.Configuration.Hocon;
using Akka.DI.CastleWindsor;
using Akka.DI.Core;
using Akka.Routing;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using MolDev.ActorHost.InternalActors;
using MolDev.ActorHost.InternalInterfaces;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Helpers;
using MolDev.Common.Messages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;

namespace MolDev.ActorHost
{
  public class ActorService : IActorService, IActorServiceInternal
  {
    private ActorSystem _actorSystem;
    private readonly IWindsorContainer _container;
    private readonly ConcurrentDictionary<string, IActor> _actorRefs = new ConcurrentDictionary<string, IActor>();
    private readonly ConcurrentDictionary<string, IActor> _actorRefPaths = new ConcurrentDictionary<string, IActor>();
    private readonly object _syncObj = new object();
    private int _actorAskTimeOut = Consts.ActorAskTimeOut;
    private Config _config;
    private readonly string _actorServiceConfigFileName;
    private WindsorDependencyResolver _resolver;
    private IActorRef _infoActor;

    public ActorService(IWindsorContainer container, string actorServiceConfigFileName)
    {
      _container = container;
      _actorServiceConfigFileName = actorServiceConfigFileName;

      _actorAskTimeOut = SettingsHelper.ReadSettingInt(Consts.ActorAskTimeOutConfigKey, Consts.ActorAskTimeOut, _actorServiceConfigFileName);
			var configFileContent = System.IO.File.ReadAllLines(actorServiceConfigFileName);
			var hoconPart = string.Empty;
			var inHoconArea = false;
			foreach (var line in configFileContent)
			{
				if (line.Contains("<hocon>"))
				{
					inHoconArea = true;
					continue;
				}
				if (line.Contains("</hocon>"))
				{
					break;
				}

				if (inHoconArea) hoconPart += line + "\r\n";
			}
			var x = Environment.ExpandEnvironmentVariables(hoconPart);
	  _config = ConfigurationFactory.ParseString(x);

   //   var localPort = SettingsHelper.ReadSetting("moldevhost:ServiceAkkaPort", _actorServiceConfigFileName);
			//var portConfig = ConfigurationFactory.ParseString($"{Consts.AkkaRemoteHeliosTcpPort}={localPort}");
   //   _config = portConfig.WithFallback(_config);

      /*var nis = NetworkInterface.GetAllNetworkInterfaces();
      var ni = nis?.FirstOrDefault(n => n.NetworkInterfaceType == NetworkInterfaceType.Ethernet) ?? nis.FirstOrDefault();
      _localIP = ni?.GetIPProperties()?.UnicastAddresses?.FirstOrDefault(a => !a.Address.IsIPv6LinkLocal && !a.Address.IsIPv6Multicast && !a.Address.IsIPv6SiteLocal && !a.Address.IsIPv6Teredo)?.Address?.ToString() ?? _localIP;
      var ipConfig = ConfigurationFactory.ParseString($"{Consts.AkkaRemoteHeliosTcpPublicHostName}=\"{_localIP}\"");
      if (config.GetString(Consts.AkkaRemoteHeliosTcpPublicHostName) == Consts.DefaultLocalIP) _config = ipConfig.WithFallback(_config);*/
      //dns-use-ipv6
      _actorSystem = ActorSystem.Create(SettingsHelper.ReadSetting(Consts.ActorSystemNameConfigKey, _actorServiceConfigFileName), _config);

      _resolver = new WindsorDependencyResolver(_container, _actorSystem);

      CreateActors(false);
      _infoActor = _actorSystem.ActorOf(Props.Create(typeof(InfoActor), this), typeof(InfoActor).Name);
    }

    public IActor Get<TActor>(int? timeout) where TActor : class
    {
      return Get(typeof(TActor).Name, timeout);
    }

    public IActor Get(string name, int? timeout)
    {
      if (_actorRefs.ContainsKey(name))
      {
        return _actorRefs[name];
      }
      var akkaConfig = GetConfig(name);
      string remotePath = GetConfigString(akkaConfig, Consts.RemoteParam);
      lock (_syncObj)
      {
        if (_actorRefs.ContainsKey(name))
        {
          return _actorRefs[name];
        }
        string path = string.Format(Consts.O_user_1, remotePath ?? string.Empty, name);
        IActorRef actor = GetRef(name, remotePath ?? string.Empty, timeout);
        if (actor == null) return null;
        _actorRefs[name] = new ActorRef(actor);

        return _actorRefs[name];
      }
    }

    public IActor Get<TActor>(string path) where TActor : class
    {
      if (_actorRefPaths.ContainsKey(path))
      {
        return _actorRefPaths[path];
      }
      lock (_syncObj)
      {
        if (_actorRefs.ContainsKey(path))
        {
          return _actorRefs[path];
        }
        List<string> addresses = new List<string>();
        try
        {
          if (typeof(TActor) != typeof(InfoActor))
          {
            var hostEntry = Dns.GetHostEntry(Address.Parse(path).Host);
            addresses.AddRange(hostEntry.AddressList.Select(a => a.ToString().Replace("::1", "127.0.0.1")).ToList());
            addresses.Add(hostEntry.HostName);
            addresses.AddRange(hostEntry.Aliases);
            if (hostEntry.HostName.IndexOf('.') != -1) addresses.Add(hostEntry.HostName.Substring(0, hostEntry.HostName.IndexOf('.')));
          }
          addresses.Add(Address.Parse(path).Host);
        }
        catch { }
        foreach (var address in addresses)
        {
          try
          {
            string tryPath = Address.Parse(path).WithHost(address).ToString();
            var actor = GetRef(typeof(TActor).Name, tryPath, 2000);
            if (actor != null) return new ActorRef(actor);
          }
          catch { }
        }
        return null;
      }
    }

    private IActorRef GetRef(string name, string path, int? timeout = null)
    {
      path = string.Format(Consts.O_user_1, path, name);
      ActorSelection selection = _actorSystem.ActorSelection(path);
      IActorRef actor = null;
      try
      {
        actor = selection.Ask<ActorIdentity>(new Identify(Guid.NewGuid()), TimeSpan.FromMilliseconds(timeout ?? _actorAskTimeOut)).Result.Subject;
      }
      catch (Exception exc)
      {
        Console.WriteLine(path + " : " + exc.Message + ((exc.InnerException != null) ? " " + exc.InnerException.Message : ""));
      }

      if (actor == null)
      {
        Console.WriteLine(path + " : actor == null");
        return null;
      }
      Console.WriteLine(path + " : actor OK");
      return actor;
    }

    public void Register<TActor>() where TActor : class
    {
      string actorName = typeof(TActor).Name;
      if (_actorRefs.ContainsKey(actorName))
      {
        return;
      }
      lock (_syncObj)
      {
        if (_actorRefs.ContainsKey(actorName))
        {
          return;
        }
        _container.Register(Component.For(typeof(TActor)).Named(actorName).LifeStyle.Transient);
        _actorRefs[actorName] = null;
      }
    }

    public IActor Create<TActor>(string name) where TActor : class
    {
      var akkaConfig = GetConfig(name);
      string remotePath = GetConfigString(akkaConfig, Consts.RemoteParam);
      string path = string.Format(Consts.O_user_1, remotePath ?? string.Empty, name);

      _container.Register(Component.For<TActor>().Named(name).LifeStyle.Transient);
      string routerType = GetConfigString(akkaConfig, Consts.RouterParam);
      Props props = null;
      if (routerType == Consts.ConsistentHashingPool)
      {
        props = _actorSystem.DI().Props(typeof(TActor)).WithRouter(new ConsistentHashingPool(akkaConfig).WithHashMapping(m => HashMapping(m)));
      }
      else if (routerType == Consts.RoundRobinPool)
      {
        var roundRobin = new RoundRobinPool(akkaConfig);
        props = _actorSystem.DI().Props(typeof(TActor)).WithRouter(roundRobin);
      }
      else
      {
        props = _actorSystem.DI().Props(typeof(TActor));
      }
      return new ActorRef(_actorSystem.ActorOf(props, name));
    }

    public void ShutDown()
    {
      _actorSystem.Terminate();
      _actorSystem.WhenTerminated.Wait();
    }

    private void CreateActors(bool restart)
    {
      Config akkaConfig = GetConfig();
      foreach (HoconObject val in akkaConfig.Root.Values)
      {
        var items = val.Items;
        foreach (var item in items)
        {
          string actorName = item.Key.Substring(1);
          foreach (HoconObject v in item.Value.Values)
          {
            var t = v.GetKey(Consts.CreateLocalParam);
            if (t != null)
            {
              string str = t.GetString();
              Type actorType = Type.GetType(str);
              CreateActor(actorName, actorType, restart);
            }
          }
        }
      }
    }

    private IActor CreateActor(string name, Type actorType, bool restart)
    {
      if (_actorRefs.ContainsKey(name))
      {
        return _actorRefs[name];
      }
      IActorRef actor;
      lock (_syncObj)
      {
        if (_actorRefs.ContainsKey(name))
        {
          return _actorRefs[name];
        }
        var akkaConfig = GetConfig(name);
        string remotePath = GetConfigString(akkaConfig, Consts.RemoteParam);
        string path = string.Format(Consts.O_user_1, remotePath ?? string.Empty, name);

        if (!restart) _container.Register(Component.For(actorType).Named(name).LifeStyle.Transient);
        string routerType = GetConfigString(akkaConfig, Consts.RouterParam);
        Props props = null;
        if (routerType == Consts.ConsistentHashingPool)
        {
          props = _actorSystem.DI().Props(actorType).WithRouter(new ConsistentHashingPool(akkaConfig).WithHashMapping(m => HashMapping(m)));
        }
        else if (routerType == Consts.RoundRobinPool)
        {
          var roundRobin = new RoundRobinPool(akkaConfig);
          props = _actorSystem.DI().Props(actorType).WithRouter(roundRobin);
        }
        else
        {
          props = _actorSystem.DI().Props(actorType);
        }

        actor = _actorSystem.ActorOf(props, name);
      }
      _actorRefs[name] = new ActorRef(actor);
      return _actorRefs[name];
    }

    private Config GetConfig(string actorName = null)
    {
      if (!string.IsNullOrWhiteSpace(actorName))
      {
        actorName = string.Format(Consts.AkkaActorDeploymentActorName, actorName);
      }
      var deploymentStr = string.Format(Consts.AkkaActorDeployment, actorName);
      var deploymentConfig = _config.GetConfig(deploymentStr);
      return deploymentConfig;
    }

    private string GetConfigString(Config deploymentConfig, string propertyName)
    {
      if (deploymentConfig != null)
      {
        return deploymentConfig.GetString(propertyName);
      }
      return null;
    }

    private object HashMapping(object m)
    {
      var request = m as RequestMessageBase;
      if (request != null) return request.Key;
      var response = m as ResponseMessageBase;
      if (response != null) return response.Key;
      return null;
    }

    public void RemoveActorsFromDictionaryByPath(string fullPath)
    {
      IActor actor;
      foreach (var elem in _actorRefs)
      {
        if (elem.Value != null && elem.Value.PathStr.Contains(fullPath))
          _actorRefs.TryRemove(elem.Key, out actor);
      }
    }

    public IEnumerable<string> GetAllRemoteAddresses()
    {
      Dictionary<string, int> addresses = new Dictionary<string, int>();
      Config akkaConfig = GetConfig();
      foreach (HoconObject val in akkaConfig.Root.Values)
      {
        var items = val.Items;
        foreach (var item in items)
        {
          string actorName = item.Key.Substring(1);
          foreach (HoconObject v in item.Value.Values)
          {
            var t = v.GetKey(Consts.RemoteParam);
            if (t != null)
            {
              string str = t.GetString();
              addresses[str] = 0;
            }
          }
        }
      }
      var ret = addresses.Keys.ToList();
      return ret;
    }
  }
}