﻿using Castle.Windsor;
using System;
using Topshelf;

namespace MolDev.ActorHost
{
  public class ActorHostServiceFactory
  {
    public static void Run(string serviceName,
                           string serviceDisplayName,
                           string serviceDescription,
                           Action<IWindsorContainer> additionalInstall = null)
    {
      HostFactory.Run(x =>
      {
        x.Service<ActorHost>(s =>
        {
          s.ConstructUsing(settings =>
          {
            return new ActorHost(additionalInstall);
          });
          s.WhenStarted(tc => tc.Start());
          s.WhenStopped(tc => tc.Stop());
        });

        x.RunAsLocalSystem();
        x.SetDescription(serviceDescription);
        x.SetDisplayName(serviceDisplayName);
        x.EnableServiceRecovery(s =>
        {
          s.RestartService(0);
          s.RestartService(0);
          s.RestartService(0);
        });
        x.SetServiceName(serviceName);
        x.SetStartTimeout(TimeSpan.FromSeconds(30));
        x.SetStopTimeout(TimeSpan.FromSeconds(60));
      });
    }
  }
}
