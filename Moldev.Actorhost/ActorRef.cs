﻿using Akka.Actor;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Exceptions;
using MolDev.Common.Helpers;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;
using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace MolDev.ActorHost
{
  public static class ActorRefPipeTo
  {
    public static Task PipeTo<T>(this Task<T> taskToPipe, IActor recipient, IActor sender = null)
    {
      return taskToPipe.PipeTo((recipient as ActorRef)?.Self, (sender as ActorRef)?.Self);
      /*return taskToPipe.ContinueWith(t =>
      {
        if (t.IsCanceled || t.IsFaulted)
        {
          recipient.Tell(new Status.Failure(t.Exception), sender);
        }
        else
        {
          recipient.Tell(t.Result, sender);
        }
      }, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);*/
    }
  }

  public class ActorRef : IActor, ISerializable//, ICanTell
  {
    [JsonProperty("IActorRef", Order = 0)]
    private IActorRef IActorRef { get; set; }
    private int AskTimeOut { get; set; }

    public ActorRef()
    {
      AskTimeOut = SettingsHelper.ReadSettingInt(Consts.ActorAskTimeOutConfigKey, Consts.ActorAskTimeOut);
    }

    public ActorRef(ActorRef actor)
    {
      IActorRef = actor.IActorRef;
    }

    public ActorRef(IActorRef actorRef) : this()
    {
      IActorRef = actorRef;
    }

    public ActorRef(SerializationInfo info, StreamingContext context) : base()
    {
      ActorSystem system = context.Context as ActorSystem;
      if (system == null)
      {
        IActorRef = (IActorRef)info.GetValue("IActorRef", typeof(IActorRef));
        return;
      }
      byte[] data = (byte[])info.GetValue("IActorRef", typeof(byte[]));
      IActorRef = (IActorRef)system.Serialization.FindSerializerForType(typeof(ActorIdentity)).FromBinary(data, typeof(byte[]));
    }

    public string PathStr
    {
      get
      {
        if (IActorRef != null) return IActorRef.Path.ToString();
        else return "";
      }
    }

    public IActorRef Self
    {
      get
      {
        return IActorRef;
      }
    }

    public async Task<TResult> Ask<TResult>(object message, int? timeout) where TResult : ResponseMessageBase
    {
      var res = await IActorRef.Ask<TResult>(message, TimeSpan.FromMilliseconds(timeout ?? AskTimeOut));

      if (res.Status != ResultStatusEnum.Success)
      {
        throw new MolDevException(res.Error.Message, res.Status, res.Error);
      }
      return res;
    }

    public void Tell(object message, IActor sender = null)
    {
      if (!(sender is ActorRef))
      {
        IActorRef.Tell(message);
      }
      else
      {
        IActorRef.Tell(message, (sender as ActorRef)?.Self);
      }
    }

    public void Forward(object message)
    {
      IActorRef.Forward(message);
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      ActorSystem system = context.Context as ActorSystem;
      if (system == null)
      {
        info.AddValue("IActorRef", IActorRef, typeof(IActorRef));
        return;
      }
      byte[] data = system.Serialization.FindSerializerForType(typeof(ActorIdentity)).ToBinary(IActorRef);
      info.AddValue("IActorRef", data, typeof(byte[]));
    }
  }
}
