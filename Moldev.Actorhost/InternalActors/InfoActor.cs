﻿using Akka.Actor;
using MolDev.ActorHost.InternalInterfaces;
using MolDev.ActorHost.InternalMessages;
using MolDev.Common.Akka.Interfaces;
using System;
using System.Collections.Generic;

namespace MolDev.ActorHost.InternalActors
{
  internal class InfoActor : ReceiveActor
  {
    private IEnumerable<string> _paths;
    private ActorPath _myPath;
    private string _mySystemName;
    private IActorServiceInternal _actorService;

    public InfoActor(IActorService actorService)
    {
      _actorService = actorService as IActorServiceInternal;
      _mySystemName = Context.System.Name;
      _myPath = Self.Path;

      Receive<Terminated>(msg => OnTerminated(msg));
      Receive<WatchForMessage>(msg =>
      {
        var actor = msg.Actor as ActorRef;
        if (actor != null) Context.Watch(actor.Self);
      });
      Receive<ActorWasFoundInternal>(msg => OnActorWasFound(msg));
    }

    private void OnActorWasFound(ActorWasFoundInternal msg)
    {
      msg.Actor.Tell(new WatchForMessage() { Actor = new ActorRef(Self) });
      Context.Watch(((ActorRef)msg.Actor).Self);
    }

    private void OnTerminated(object msg)
    {
      var m = msg as Terminated;
      if (m != null)
      {
        var path = m.ActorRef.Path.Parent.ToString();
        var systemName = path.Split('/')[2].Split('@')[0];

        //string message = string.Format(SystemMessages.InfoActorOnTerminated, _mySystemName, systemName, m.ActorRef.Path.Name, Sender.Path.Parent.ToString());
        //Console.WriteLine(message);

        //bool b = ActorService.RemoveActorFromDictionary(m.ActorRef.Path.Name);
        //if (b) Context.Unwatch(m.ActorRef);

        _actorService.RemoveActorsFromDictionaryByPath(m.ActorRef.Path.Root.ToString());
        StartTryGetActor(Sender.Path.Parent.ToString().Replace("/user", ""));
      }
    }

    protected override void PreStart()
    {
      _paths = _actorService.GetAllRemoteAddresses();
      foreach (var path in _paths)
      {
        var systemName = path.Split('/')[2].Split('@')[0];
        if (_mySystemName != systemName)
        {
          StartTryGetActor(path);
        }
      }
    }

    private void StartTryGetActor(string path)
    {
      var discoveryActor = Context.ActorOf(Props.Create(typeof(DiscoveryActor<InfoActor>), _actorService), Guid.NewGuid().ToString());
      if (discoveryActor != null) discoveryActor.Tell(new TryGetActor() { ActorPath = path });
    }
  }
}