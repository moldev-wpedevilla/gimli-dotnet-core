﻿using Akka.Actor;
using Akka.Event;
using NLog;
using System;
using NLogger = global::NLog.Logger;

namespace MolDev.ActorHost.InternalActors
{
  internal class NLogLogger : ReceiveActor
  {
    private readonly ILoggingAdapter log = Context.GetLogger();

    private static void Log(LogEvent logEvent, Action<NLogger> logStatement)
    {
      var logger = LogManager.GetLogger(Consts.MolDevAkkaLogger);
      logStatement(logger);
    }

    public NLogLogger()
    {
      Receive<Error>(m => Log(m, logger => logger.Error(m.Cause, m.ToString())));
      Receive<Warning>(m => Log(m, logger => logger.Warn(m.ToString())));
      Receive<Info>(m =>
      {
        Log(m, logger => logger.Info(m.ToString()));
      });

      Receive<Debug>(m => Log(m, logger => logger.Debug(m.ToString())));

      Receive<InitializeLogger>(m =>
      {
        Sender.Tell(new LoggerInitialized());
        log.Info(Consts.AkkaLogerStarted);
      });
    }
  }
}