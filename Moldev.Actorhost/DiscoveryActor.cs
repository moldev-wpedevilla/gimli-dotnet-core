﻿using MolDev.ActorHost.InternalMessages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Helpers;
using System;

namespace MolDev.ActorHost
{
  public class DiscoveryActor<TActor> : ActorReceiveBase where TActor : class
  {
    private readonly int _timerInterval;
    private readonly int _maxTryNumber;
    private int _tryNumber = 0;
    private string _actorPath;

    public DiscoveryActor(IActorService actorService) : base(actorService)
    {
      _timerInterval = SettingsHelper.ReadSettingInt(Consts.LoadTimerIntervalMs, 60000);
      _maxTryNumber = SettingsHelper.ReadSettingInt(Consts.MaxTryNumber, int.MaxValue);
      Receive<TryGetActor>(msg => OnTryGetActor(msg));
      Receive<TryFindActor>(msg => TimerMethod());
    }
    private void OnTryGetActor(TryGetActor msg)
    {
      _actorPath = msg.ActorPath;
      StartScheduler();
    }
    private void StartScheduler()
    {
      ScheduleTellOnce(TimeSpan.FromMilliseconds(_timerInterval), new TryFindActor());
    }
    private void TimerMethod()
    {
      if (++_tryNumber <= _maxTryNumber)
      {
        try
        {
          IActor actor = ActorService.Get<TActor>(_actorPath);
          if (actor == null) throw new NullReferenceException();
          ParentRef.Tell(new ActorWasFoundInternal() { Actor = actor, ActorPath = _actorPath, ActorType = GetType() });
          StopSelf();
        }
        catch
        {
          StartScheduler();
        }
      }
      else
      {
        _tryNumber = 0;
        StopSelf();
      }
    }
  }
}
