﻿namespace MolDev.Shared
{
  public static class Consts
  {
    public const string NotAvailable = "N/A";
    public const string SoloModeColor = null;//"#FFFFFF";
    public const int PyramidTileSize = 256;
    public static readonly decimal[] PyramidLevelFactors = new decimal[] { 1m, 2m, 4m, 8m, 16m, 32m, 64m, 128m, 256m, 512m, 1024m };
    public const string RelatedLabwareTypesKey = @"RelatedLabwareTypes";
    public const string UserCultureContextKey = "userCulture";

    public const string Unknown = "unknown";
    public const string Untyped = "untyped";

    public const string White = "WHITE";
    public const string WhiteColor = "#FFFFFF";
    public const string TL = "TL";
    public const char CsvSeparator = ',';

    public const float Epsilon = 0.000001f;//0.5;
    public const decimal Pi = 3.14m;
    public const long NmKoefficient = 1000000;

    public const string Running = "running";
    public const string Completed = "completed";
    public const string Failed = "failed";
    public const string Cancelled = "cancelled";
    public const string Unavailable = "unavailable";
    public const double LogarithBase = 2.0;
    public const string GFormatDigits = "5";
    public const string GFormatDigitsStr = "moldevhost:GFormatDigits";
    public const long ImageQuality = 90L;
    public const string ImageFormat = "image/jpeg";
    public const string AvatarSizePath = "moldevhost:AvatarSize";
    public const int DefaultAvatarSize = 128;
    public const string PlanetaryImageSizePath = "moldevhost:PlanetaryImageSize";
    public const int DefaultPlanetaryImageSize = 512;
    public const float DecreaseZoneValue = 0;//0.0015f; //before Adjustment In order not to increase exact zones 
    public const double AcceptAreaCoefficient = 0.0001; //Part of well Area to be accepted during adding random sites
    public const int MinForSafeWells = 96;    // Minimum numbers of well on the plate for Safe wells testing
  }

  public enum MonitoringIntStatus
  {
    Running = 0,
    Completed = 1,
    Failed = 2,
    Unavailable = 3,
    Cancelled = 4
  };
  public enum TimeSeriesElementIntStatus
  {
    InProgress = 1,
    Complete = 2,
    InComplete = 0,
  };

  public static class SortElements
  {
    public const string Name = "name";
    public const string Created = "created";
    public const string Modified = "modified";
    public const string Accessed = "accessed";
    public const string Stich = "stich";
    public const string Type = "type";
    public const string Section = "sections";
    public const string Favarite = "favorite";
    public const string ExpName = "expname";
    public const string ExpCreated = "expcreated";
    public const string ExpModified = "expmodified";
  }

  public static class SortOrder
  {
    public const string Desc = "desc";
    public const string Asc = "asc";
  }

  public static class TimeTypes
  {
    public const string Created = "created";
    public const string Modified = "modified";
    public const string Accessed = "accessed";
    public const string ExperimentCreated = "expcreated";
    public const string ExperimentModified = "expmodified";
    public const string ExperimentAccessed = "expaccessed";
  }

  public static class ImageFileConsts
  {
    // For virtual snap image. ToDo may be move to shared consts
    public const string ImageFileFilter = "*.tif";
    public const string HtdFileFilter = "*.HTD";
    public const string HtdFileXSitesTag = @"XSites";
    public const string HtdFileYSitesTag = @"YSites";
    public const string HtdFileXWellsTag = @"XWells";
    public const string HtdFileYWellsTag = @"YWells";
    public const string ThumbFileSymptom = "_Thumb.TIF";
    public const char ImageFileNameSeparator = '_';
    public const string SilaParametersFile = "ParametersRead.params";
    public const string SilaFileMapFile = "ImageFileMap.csv";
  }

  public static class AnnotationFileFormat
  {
    public const string Column = "column";
    public const string Plate = "plate";
  }
  public static class AnnotationMatchParameter
  {
    public const string Replace = "replace";
    public const string Skip = "skip";
  }

  public static class ConstsDefaultAddressPart
  {
    public const string NetTcp = "net.tcp";
    public const string AkkaTcp = "akka.tcp";
    public const string DevicePath = "MD.SiLAService.Host";
    public const string DevicePort = "8091";
    public const string AnalysisServicePort = "12325";
    public const string AnalysisServiceActorSystemName = "MLDIDV5";
    public const string StorageServicePort = "12324";
    public const string StorageServiceActorSystemName = "MLDIDV4";

  }
  public static class ConfigKeys
  {
    public const string DeviceSchemehKey = "moldevhost:DeviceSchemehKey";//"net.tcp";
    public const string DevicePathKey = "moldevhost:DevicePathKey";//"MD.SiLAService.Host";
    public const string DevicePortKey = "moldevhost:DevicePortKey";//"8091";

    public const string AnalysisServiceSchemeKey = "moldevhost:AnalysisServiceSchemeKey";//"akka.tcp";
    public const string AnalysisServicePortKey = "moldevhost:AnalysisServicePortKey";//"12325";
    public const string AnalysisServiceActorSystemName = "moldevhost:AnalysisServiceActorSystemName";//"MLDIDV5";

    public const string StorageServiceSchemeKey = "moldevhost:StorageServiceSchemeKey";//"akka.tcp";
    public const string StorageServicePortKey = "moldevhost:StorageServicePortKey";//"12324";
    public const string StorageServiceActorSystemName = "moldevhost:StorageServiceActorSystemName";//"MLDIDV4";
  }

}
