﻿using System;

namespace MolDev.Shared.Messages.Experiment
{
  [Serializable]
  public enum ServiceType
  {
    Unknown,
    Analysis,
    Core,
    Web,
    Location,
    Data,
    Device
  }

  [Serializable]
  public class StopService
  {
    public ServiceType Type { get; set; }
    public string Id { get; set; }
  }

  [Serializable]
  public class GetServices
  {

  }
}
