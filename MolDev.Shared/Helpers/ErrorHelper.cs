﻿using MolDev.Common.Messages.Errors;
using MolDev.Resources.UI;
using System.Collections.Generic;
using System.Globalization;

namespace MolDev.Shared.Helpers
{
  public static class ErrorHelper
  {
    public static ErrorInfo AddValidationResult(ErrorInfo errorInfo, List<MoldevValidationResult> validationResult, CultureInfo culture)
    {
      if (errorInfo == null)  // Create new error
      {
        errorInfo = new ErrorInfo { Message = UserMessagesEx.GetString("ValidationFailed",culture) };
      }
      if (errorInfo.Validation == null) errorInfo.Validation = new List<MoldevValidationResult>();

      ((List<MoldevValidationResult>)errorInfo.Validation).AddRange(validationResult);
      return errorInfo;
    }
  }
}
