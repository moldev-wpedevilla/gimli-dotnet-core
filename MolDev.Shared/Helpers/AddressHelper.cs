﻿using MolDev.Common.Helpers;
using System;
using System.Net;

namespace MolDev.Shared.Helpers
{
  public static class DefaultAddressPart
  {
    public static string DeviceScheme { get; set; }
    public static string DevicePath { get; set; }
    public static string DevicePort { get; set; }
    public static string AnalysisServiceScheme { get; set; }
    public static string AnalysisServicePort { get; set; }
    public static string AnalysisServiceActorSystemName { get; set; }
    public static string StorageServiceScheme { get; set; }
    public static string StorageServicePort { get; set; }
    public static string StorageServiceActorSystemName { get; set; }


    //static DefaultAddressPart()
    //{
    //  DeviceScheme = SettingsHelper.ReadSettingString(ConfigKeys.DeviceSchemehKey, ConstsDefaultAddressPart.NetTcp);
    //  DevicePath = SettingsHelper.ReadSettingString(ConfigKeys.DevicePathKey, ConstsDefaultAddressPart.DevicePath);
    //  DevicePort = SettingsHelper.ReadSettingString(ConfigKeys.DevicePortKey, ConstsDefaultAddressPart.DevicePort);

    //  AnalysisServiceScheme = SettingsHelper.ReadSettingString(ConfigKeys.AnalysisServiceSchemeKey, ConstsDefaultAddressPart.AkkaTcp);
    //  AnalysisServicePort = SettingsHelper.ReadSettingString(ConfigKeys.AnalysisServicePortKey, ConstsDefaultAddressPart.AnalysisServicePort);
    //  AnalysisServiceActorSystemName = SettingsHelper.ReadSettingString(ConfigKeys.AnalysisServiceActorSystemName, ConstsDefaultAddressPart.AnalysisServiceActorSystemName);

    //  StorageServiceScheme = SettingsHelper.ReadSettingString(ConfigKeys.StorageServiceSchemeKey, ConstsDefaultAddressPart.AkkaTcp);
    //  StorageServicePort = SettingsHelper.ReadSettingString(ConfigKeys.StorageServicePortKey, ConstsDefaultAddressPart.StorageServicePort);
    //  StorageServiceActorSystemName = SettingsHelper.ReadSettingString(ConfigKeys.StorageServiceActorSystemName, ConstsDefaultAddressPart.StorageServiceActorSystemName);
    //}
  }

  public enum AddressType
  {
    Device,
    AnalysisService,
    StorageService,
  }
  public class AddressHelper
  {
    //empress.ggasoftware.com  = 88.201.200.131
    public AddressType Type { get; set; }
    public string Host { get; set; }
    public string Port { get; set; }
    public string Path { get; set; }
    public string Scheme { get; set; }
    public string User { get; set; }
    public string HostName { get { return GetHostName(Host); } }
    public bool IsDefaultPort
    {
      get
      {
        switch (Type)
        {
          case AddressType.Device:
            {
              return Port == DefaultAddressPart.DevicePort;
            }
          case AddressType.AnalysisService:
            {
              return Port == DefaultAddressPart.AnalysisServicePort;
            }
          case AddressType.StorageService:
            {
              return Port == DefaultAddressPart.StorageServicePort;
            }
        }
        return false;
      }
    }

    public AddressHelper(string address, AddressType type)
    {
      Parse(address, type);
    }

    public string CreateFullAddress()
    {
      var address = string.Empty;
      switch (Type)
      {
        case AddressType.Device:
          {
            // test if all properties not empty ???
            //address = string.Format(@"{0}://{1}:{2}/{3}", Scheme, Host, Port, Path);
            address = string.Format(@"{0}://{1}:{2}/{3}", Scheme, Host, Port, Path);
            break;
          }
        case AddressType.AnalysisService:
        case AddressType.StorageService:
          {
            // test if all properties not empty ???
            address = string.Format(@"{0}://{1}@{2}:{3}", Scheme, User, Host, Port);
            //address = string.Format(@"{0}://{1}@{2}:{3}", Scheme, GetHostName(User), Host, Port);
            break;
          }
      }
      return address;
    }
    private void Parse(string address, AddressType type)
    {
      Type = type;
      address = address.Trim();
      switch (type)
      {
        case AddressType.Device:
          {
            //net.tcp://eprupetw6498:8091/MD.SiLAService.Host
            int idx = address.IndexOf(@"://");
            if (idx > 0)
            {
              Scheme = address.Substring(0, idx);
              address = address.Substring(idx + 3);
            }
            else { Scheme = DefaultAddressPart.DeviceScheme; } // "net.tcp"
            //  eprupetw6498:8091/MD.SiLAService.Host
            idx = address.IndexOf(@"/");
            if (idx > 0)
            {
              Path = address.Substring(idx + 1);
              address = address.Substring(0, idx);

            }
            else { Path = DefaultAddressPart.DevicePath; } // "MD.SiLAService.Host"
            //eprupetw6498:8091
            idx = address.IndexOf(@":");
            if (idx > 0)
            {
              Host = address.Substring(0, idx);
              Port = address.Substring(idx + 1);
            }
            else
            {
              Host = address;
              Port = DefaultAddressPart.DevicePort;// "8091";
            }
            break;
          }
        case AddressType.AnalysisService:
          {
            ParseServiceAddress(address, DefaultAddressPart.AnalysisServiceScheme, DefaultAddressPart.AnalysisServiceActorSystemName, DefaultAddressPart.AnalysisServicePort);
            break;
          }
        case AddressType.StorageService:
          {
            ParseServiceAddress(address, DefaultAddressPart.StorageServiceScheme, DefaultAddressPart.StorageServiceActorSystemName, DefaultAddressPart.StorageServicePort);
            break;
          }
        default:
          throw new Exception("Invalid address type");
      }
    }

    private void ParseServiceAddress(string address, string dfltScheme, string dfltActorSystemName, string dfltServicePort)
    {
      int idx = address.IndexOf(@"://");
      if (idx > 0)
      {
        Scheme = address.Substring(0, idx);
        address = address.Substring(idx + 3);
      }
      else { Scheme = dfltScheme; }  // "akka.tcp"
      idx = address.IndexOf(@"@");
      if (idx < 0)
      {
        User = dfltActorSystemName;
        //Host = address;
        //Port = dfltServicePort;// "12325";
      }
      else
      {
        User = address.Substring(0, idx);
        address = address.Substring(idx + 1);
        /*idx = address.IndexOf(@":");
        if (idx > 0)
        {
          Host = address.Substring(0, idx);
          Port = address.Substring(idx + 1);
          idx = Port.IndexOf(@"/");
          if (idx > 0) Port = Port.Substring(0, idx);
        }
        else
        {
          Host = address;
          Port = dfltServicePort;// "12325";
        }*/
      }

      idx = address.IndexOf(@":");
      string internalAddress;
      if (idx > 0)
      {
        internalAddress = address.Substring(0, idx).ToUpper();
        Port = address.Substring(idx + 1);
        idx = Port.IndexOf(@"/");
        if (idx > 0) Port = Port.Substring(0, idx);
      }
      else
      {
        internalAddress = address.ToUpper();
        Port = dfltServicePort;// "12325";
      }
      Host = internalAddress;
    }

    public string GetHostName(string address)
    {
      try
      {
        IPAddress ipAddress;
        if (IPAddress.TryParse(address, out ipAddress))
        {
          IPHostEntry entry = Dns.GetHostEntry(address);
          if (entry != null)
          {
            return entry.HostName;
          }
        }
      }
      catch { }
      return address;
    }
  }
}
