﻿using MolDev.Resources.UI;
using System.Globalization;
using System.Linq;
using System.Text;

namespace MolDev.Shared.Helpers
{
  public static class MonitoringHelper
  {
    public static string DetailsDecoding(string dbDetails, CultureInfo culture)
    {
      if (string.IsNullOrEmpty(dbDetails)) return dbDetails;
      StringBuilder ret = new StringBuilder();
      try
      {
        var strArr = dbDetails.Split('|');
        for (int i = 0; i < strArr.Count(); i++)
        {
          var pars = strArr[i].Split('&');
          var s = UserMessagesEx.GetString(pars[0], culture);
          if (string.IsNullOrEmpty(s)) s = strArr[i];
          var pCnt = pars.Count();
          if (pCnt > 1)
          {
            object[] args = new object[pCnt - 1];
            for (int j = 1; j < pCnt; j++)
            {
              args[j - 1] = pars[j];
            }
            s = string.Format(s, args);
          }
          if (ret.Length > 0) ret.Append("; ");
          ret.Append(s);
        }
      }
      catch { }
      return ret.ToString();
    }
  }
}
