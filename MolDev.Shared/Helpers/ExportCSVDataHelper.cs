﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MolDev.Shared.Helpers
{
  public static class ExportCSVDataHelper
  {
    public static Dictionary<string, string> GetHeaders(string filename)
    {
      return new Dictionary<string, string> { { "Content-Disposition", "attachment; filename=\"" + filename + "\"" } };
    }

    public static string GetCsvFormatRow(IEnumerable<string> stringRowData)
    {
      var formatRowData = stringRowData.Select(GetCsvFormatString);
      return string.Join(Consts.CsvSeparator.ToString(), formatRowData) + Environment.NewLine;
    }

    public static string GetCsvFormatString(string rawString)
    {
      return $"\"{rawString.Replace("\"", "\"\"")}\"";
    }
  }
}
