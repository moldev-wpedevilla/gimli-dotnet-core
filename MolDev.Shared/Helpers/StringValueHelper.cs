﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MolDev.Shared.Helpers
{
  public class StringValueItem
  {
    public string Value { get; set; }
    public int Position { get; set; }
  }
  public class StringValueHelperData
  {
    public List<StringValueItem> Values { get; set; } = new List<StringValueItem>();
    public Dictionary<string, int> ValuesDictionary { get; set; } = new Dictionary<string, int>();
  }
  public class StringValueHelper
  {
    public static readonly string PositiveStr = "positive";
    public static readonly string NegativeStr = "negative";
    public static readonly string NeutralStr = "neutral";
    public static readonly string SampleStr = "sample";
    public static readonly string TestStr = "test";
    public static readonly string OtherStr = "other";
    public static readonly string GroupStr = "group";

    private static Dictionary<string, StringValueHelperData> _dictionary;
    private static int _otherIdx = 2;
    private static void InitHelper()
    {
      if(_dictionary == null)
      {
        _dictionary = new Dictionary<string, StringValueHelperData>();
        var data = new StringValueHelperData()
        {
          Values = new List<StringValueItem>()
          {
            new StringValueItem {Value = NegativeStr, Position = 0},
            new StringValueItem {Value = TestStr, Position = 1},
            new StringValueItem {Value = OtherStr, Position = 2},
            new StringValueItem {Value = PositiveStr, Position = 3}
          },
          ValuesDictionary = new Dictionary<string, int>()
          {
            { NegativeStr.ToLower(), 0},
            { NeutralStr.ToLower(), 1},
            { SampleStr.ToLower(), 1},
            { TestStr.ToLower(), 1},
            { PositiveStr.ToLower(), 3}
          }
        };
        _dictionary.Add(GroupStr.ToLower(), data);
        _otherIdx = 2; //position of "other" in data.Values
      }
    }
    public static void RenameKey(string oldKey, string newKey)
    {
      InitHelper();
      var oldKeyUpper = oldKey.ToLower();
      var newKeyUpper = newKey.ToLower();
      if (_dictionary.ContainsKey(newKeyUpper))
      {
        throw new Exception(string.Format("newKey = {0} allready exists", newKey));
      }
      if (!_dictionary.ContainsKey(oldKeyUpper))
      {
        throw new Exception(string.Format("oldKey = {0} doesn't exist", oldKey));
      }
      var data = _dictionary[oldKeyUpper];
      _dictionary.Add(newKeyUpper, data);
      _dictionary.Remove(oldKeyUpper);
    }
    public static void UpdateInsert(string key, StringValueHelperData data)
    {
      InitHelper();
      var keyUpper = key.ToLower();
      if (_dictionary.ContainsKey(keyUpper))
      {
        _dictionary.Remove(key);
      }
      _dictionary.Add(keyUpper, data);
    }
    public static bool IsDataExists(string value)
    {
      InitHelper();
      return _dictionary.ContainsKey(value.ToLower());
    }
    public static StringValueItem GetValueItem(string key, string stringValue)
    {
      InitHelper();
      var keyUpper = key.ToLower();
      if (_dictionary.ContainsKey(keyUpper))
      {
        var el = _dictionary[keyUpper];
        int idx = _otherIdx;
        var stringValueUpp = stringValue.ToLower();
        if (el.ValuesDictionary.ContainsKey(stringValueUpp))
        {
          idx = el.ValuesDictionary[stringValueUpp];
        }
        return el.Values[idx];
      }
      return null;
    }
    public static int GetItemIndex(string key, string stringValue)
    {
      InitHelper();
      var keyUpper = key.ToLower();
      if (_dictionary.ContainsKey(keyUpper))
      {
        var el = _dictionary[keyUpper];
        int idx = _otherIdx;
        var stringValueUpp = stringValue.ToLower();
        if (el.ValuesDictionary.ContainsKey(stringValueUpp))
        {
          idx = el.ValuesDictionary[stringValueUpp];
        }
        return idx;
      }
      return -1;
    }

    public static int GetValuesCount(string key)
    {
      InitHelper();
      var keyUpper = key.ToLower();
      if (_dictionary.ContainsKey(keyUpper))
      {
        return _dictionary[keyUpper].Values.Count();
      }
      return -1;
    }
  }
}
