﻿using System;

namespace MolDev.Shared.Helpers
{
  public static class DateTimeHelper
  {
    public static long ToUnixTimestamp(this DateTime value)
    {
      return (long)Math.Truncate((value.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
    }
  }
}
