﻿using System.Collections.Generic;
using System.Linq;

namespace MolDev.Shared.Helpers
{
  public static class UtilityHelper
  {
    public static IEnumerable<T> StartCount<T>(int? start, int? count, IEnumerable<T> ret)
    {
      if (start != null)
      {
        if (ret.Count() > start) ret = ret.Where((p, i) => i >= start);
        else ret = ret.Where(p => false);
      }

      if (count != null)
      {
        if (count < ret.Count()) ret = ret.Take(count ?? ret.Count());
      }
      return ret;
    }
  }
}
