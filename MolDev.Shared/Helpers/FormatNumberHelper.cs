﻿using System;
using System.Configuration;

namespace MolDev.Shared.Helpers
{
  public static class FormatNumberHelper
  {
    //    private static double _min = 0.001;
    //    private static double _max = 1000;
    private static string _format = string.Format("g1");
    public static string GetLabelString(string value, int? precision)
    {
      /*
            var ret = value;
            double d;
            if (double.TryParse(ret, out d))
            {
              if (Math.Abs(d) > _min && Math.Abs(d) < _max)
              {
                return TruncateFloatingPoint(d).ToString();
              }
              else
              {
                double num; int exp; bool isNegative;
                GetExpView(out num, out exp, out isNegative, d);
                ret = (isNegative ? "-" : string.Empty) + ((Math.Abs(exp) > 0) ? num.ToString() + "e" + exp.ToString() : num.ToString());
              }
            }
      */
      double d;
      string format = _format;
      if(precision != null)
      {
        format = string.Format("g{0}", precision);
      }
      var ret = value;
      if (double.TryParse(ret, out d))
      {
        ret = d.ToString(format);
      }
      return ret;
    }
    private static double TruncateFloatingPoint(double d, int precision = 2)
    {
      return Math.Round(d, precision);
    }
    private static void GetExpView(out double num, out int exp, out bool isNegative, double dPar, double min = 0.1, double max = 9)
    {
      num = 0; exp = 0; isNegative = false;
      double d = dPar;
      if (d != 0)
      {
        isNegative = d < 0;
        d = Math.Abs(d);
        bool lessThenNormalized = (d < 0.1);
        if (lessThenNormalized)
        {
          while (d < min)
          {
            d *= 10;
            exp--;
          }
        }
        else
        {
          while (d > max)
          {
            d /= 10;
            exp++;
          }

          if (exp % 3 != 0)
          {
            bool del;
            int newExp, multiplier;
            GetClosestDelimeterBy3(out newExp, out multiplier, out del, exp);
            exp = newExp;
            var multDev = Math.Pow(10, multiplier);
            d = del ? d / multDev : d * multDev;
          }
          num = Math.Round(d, 2);
        }
      }
    }
    private static void GetClosestDelimeterBy3(out int newExp, out int multiplier, out bool del, int exp)
    {
      int leftTemp = exp;
      int rightTemp = exp;
      int leftSteps = 0;
      int rightSteps = 0;

      while (leftTemp % 3 != 0)
      {
        leftTemp -= 1;
        leftSteps++;
      }

      while (rightTemp % 3 != 0)
      {
        rightTemp += 1;
        rightSteps++;
      }

      if (leftSteps < rightSteps)
      {
        newExp = leftTemp;
        multiplier = leftSteps;
        del = false;
      }
      else
      {
        del = true;
        newExp = rightTemp;
        multiplier = rightSteps;
      }
    }
    public static string GetLabelString(double value, int? precision)
    {
      //      var ret = value.ToString();
      //      ret = GetLabelString(ret);
      var format = _format;
      if(precision != null)
      {
        format = string.Format("g{0}", precision);
      }
      var ret = (!double.IsNaN(value) && !float.IsNaN((float)value))? value.ToString(format) : string.Empty;
      return ret;
    }
    public static float GetLabelFloat(double value, int? precision)
    {
      /*
      var ret = value.ToString();
      ret = GetLabelString(ret);
      */
      if (double.IsNaN(value)) return float.NaN;
      var format = _format;
      if (precision != null)
      {
        format = string.Format("g{0}", precision);
      }
      var ret = value.ToString(format);
      return float.Parse(ret);
    }

    public static bool FloatLower(float? x, float? y)
    {
      if (!x.HasValue || !y.HasValue)
      {
        return false;
      }
      if (float.IsNaN(x.Value) || float.IsNaN(y.Value))
      {
        if (float.IsNaN(x.Value) && float.IsNaN(y.Value)) return false;
        return true;
      }
      return x.Value < y.Value && !FloatEq(x.Value, y.Value);
    }

    public static bool FloatEq(float x, float y)
    {
      if (float.IsNaN(x) || float.IsNaN(y))
      {
        if (float.IsNaN(x) && float.IsNaN(y)) return true;
        return false;
      }

      bool ret = (x == y) || (Math.Abs((x - y) * 2 / (x + y)) < Consts.Epsilon);
      return ret;
    }
  }
}
