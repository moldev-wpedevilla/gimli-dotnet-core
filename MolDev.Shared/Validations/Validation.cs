﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MolDev.Shared.Validations
{
  /// <summary>
  /// Base class for MolDev models validation rules
  /// </summary>
  public abstract class MolDevValidationAttribute : ValidationAttribute
  {
    private readonly string _memberName;

    /// <summary>
    /// ctor
    /// </summary>
    protected MolDevValidationAttribute()
    {
    }

    /// <summary>
    /// ctor
    /// </summary>
    /// <param name="memberName"></param>
    protected MolDevValidationAttribute(string memberName)
    {
      if(string.IsNullOrEmpty(memberName)) throw new ArgumentException("memberName");

      _memberName = memberName;
    }

    /// <summary>
    /// Gets member names
    /// </summary>
    protected ValidationResult GetValidationError(ValidationContext validationContext)
    {
      return new ValidationResult(string.Empty, 
        new[] { string.IsNullOrEmpty(_memberName) ? validationContext.MemberName : _memberName });
    }
  }
}
