﻿using MolDev.Resources.UI;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace MolDev.Shared.Validations
{
  /// <summary>
  /// Checks that number is greater than 0
  /// </summary>
  [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
  internal sealed class PositiveNumberAttribute : MolDevValidationAttribute
  {
    private readonly bool _couldBeEqualToZero;
    private string _errorMessage;
    /// <summary>
    /// ctor
    /// </summary>
    public PositiveNumberAttribute(string memberName, bool couldBeEqualToZero = false) : base(memberName)
    {
      _couldBeEqualToZero = couldBeEqualToZero;
    }

    /// <summary>
    /// Returns error message
    /// </summary>
    public override string FormatErrorMessage(string name)
    {
      return _errorMessage;
    }

    /// <summary>
    /// Validates the specified value with respect to the current validation attribute
    /// </summary>
    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
      decimal size;
      decimal.TryParse(Convert.ToString(value), out size);
      if (size < 0 || !_couldBeEqualToZero && size == 0)
      {
        var culture = validationContext.Items[Consts.UserCultureContextKey] as CultureInfo;
        _errorMessage = UserMessagesEx.GetString("SizeValidationError", culture);
        return GetValidationError(validationContext);
      }
      return ValidationResult.Success;
    }
  }
}
