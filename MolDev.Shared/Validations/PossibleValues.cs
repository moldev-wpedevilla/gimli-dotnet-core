﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using MolDev.Resources.UI;
using System.Globalization;

namespace MolDev.Shared.Validations
{
  /// <summary>
  /// Contains rules for possible values
  /// </summary>
  [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
  public sealed class PossibleValuesAttribute : MolDevValidationAttribute
  {
    private static ConcurrentDictionary<string, ICollection<string>> possibleValues = new ConcurrentDictionary<string, ICollection<string>>();
    private readonly string memberName;
    private readonly string dictionaryName;
    private string _errorMessage;

    public static void AddValidator(string name, ICollection<string> values)
    {
      possibleValues[name] = values;
    }

    /// <summary>
    /// ctor
    /// </summary>
    public PossibleValuesAttribute(string dictionaryName)
    {
      this.dictionaryName = dictionaryName;
    }

    /// <summary>
    /// ctor
    /// </summary>
    /// <param name="memberName"></param>
    public PossibleValuesAttribute(string memberName, string dictionaryName)
    {
      this.memberName = memberName;
      this.dictionaryName = dictionaryName;
    }

    /// <summary>
    /// Returns error message
    /// </summary>
    public override string FormatErrorMessage(string name)
    {
      return _errorMessage;
    }

    /// <summary>
    /// Validates the specified value with respect to the current validation attribute
    /// </summary>
    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
      var memberNames = new[] { string.IsNullOrEmpty(memberName) ? validationContext.MemberName : memberName };
      if (value == null)
      {
        return ValidationResult.Success;
      }
      if (dictionaryName == "Languages")
      {
        var culture = CultureInfo.GetCultureInfo(value as string);
        _errorMessage = UserMessagesEx.GetString("PossibleValuesValidationError", culture);
      }
      else
      {
        _errorMessage = UserMessages.PossibleValuesValidationError;
      }
      return !possibleValues.ContainsKey(dictionaryName) || !possibleValues[dictionaryName].Contains(Convert.ToString(value)) ? new ValidationResult(string.Empty, memberNames) : ValidationResult.Success;
    }
  }
}
