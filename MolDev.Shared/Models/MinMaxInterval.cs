﻿using System;

namespace MolDev.Shared.Models
{
  [Serializable]
  public class MinMaxInterval
  {
    public float Min { get; set; }
    public float Max { get; set; }
  } 
}
