﻿using System;

namespace MolDev.Shared.Models
{
  [Serializable]
  public class Rectangle
  {
    public static readonly Rectangle Empty = new Rectangle();

    public Rectangle() { }
    public Rectangle(int left, int top, int width, int height)
    {
      X = left;
      Y = top;
      SizeX = width;
      SizeY = height;
    }

    public int X { get; set; }
    public int Y { get; set; }
    public int SizeX { get; set; }
    public int SizeY { get; set; }
    public Position Coord { get { return new Position(X, Y); } }
    public Size Size { get { return new Size(SizeX, SizeY); } }

    public static Rectangle Intersect(Rectangle a, Rectangle b)
    {
      int x1 = Math.Max(a.X, b.X);
      int x2 = Math.Min(a.X + a.SizeX, b.X + b.SizeX);
      int y1 = Math.Max(a.Y, b.Y);
      int y2 = Math.Min(a.Y + a.SizeY, b.Y + b.SizeY);

      if (x2 >= x1 && y2 >= y1)
      {
        return new Rectangle(x1, y1, x2 - x1, y2 - y1);
      }
      return Rectangle.Empty;
    }
  }
}
