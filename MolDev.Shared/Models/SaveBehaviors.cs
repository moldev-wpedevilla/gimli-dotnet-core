﻿using System;

namespace MolDev.Shared.Models
{
  /// <summary>
  /// Gets available model save behaviors 
  /// </summary>
  [Serializable]
  public enum SaveBehaviors
  {
    UpdateOrCreateNew,
    CreateNew,
    UpdateThrowExcpetionIfNotExists
  }
}
