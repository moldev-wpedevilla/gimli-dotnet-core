﻿using System;

namespace MolDev.Shared.Models.Options
{
  [Serializable]
  public class OptionsModel
  {
    public string Copyright { get; set; }
    //Version
    public int Build { get; set; }
    public int Major { get; set; }
    public int Minor { get; set; }
    public int Revision { get; set; }
    public DateTime Date { get; set; }
  }
}
