﻿using System;

namespace MolDev.Shared.Models
{
  [Serializable]
  public class Range
  {
    public Range() { }
    public Range(int left, int right) { Left = left; Right = right; }

    public int Left { get; set; }
    public int Right { get; set; }
  }
}
