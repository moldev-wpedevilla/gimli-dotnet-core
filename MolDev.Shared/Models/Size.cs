﻿using System;

namespace MolDev.Shared.Models
{
  [Serializable]
  public class Size
  {
    public Size() { }
    public Size(int x, int y) { X = x; Y = y; }

    public int X { get; set; }
    public int Y { get; set; }
  }
}
