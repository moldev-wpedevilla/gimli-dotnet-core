﻿using System;

namespace MolDev.Shared.Models
{
  [Serializable]
  public class Position
  {
    public Position() { }
    public Position(int x, int y) { X = x; Y = y; }

    public int X { get; set; }
    public int Y { get; set; }

    public bool Equals(Position obj)
    {
      return X == obj.X && Y == obj.Y;
    }
  }
}
