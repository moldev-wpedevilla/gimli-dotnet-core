﻿using System.ServiceModel;

namespace Gimli.Instrument.Common
{
	//[ServiceContract]
	public interface IInstrumentService
	{
		/// <summary>
		/// A servicecontract has to have at least one member
		/// </summary>
		//[OperationContract]
		void Dummy();
	}
}
