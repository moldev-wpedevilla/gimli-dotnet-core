﻿using System.CodeDom;

namespace Gimli.Instrument.Common
{
	public static class SpecialMeaningsStrings
	{
		public const string GimliMasterLockId = "51AF2968-1867-4B9D-B840-601E8B224907";
		public const string GimliOnboardLockId = "360A98AD-F413-4747-8C7E-ACE53D824166";
		public const string GimliOffboardLockId = "B55B5AEA-5051-407A-8C0A-EB2F0B450728";

		public const string DiscoverableServicePort = "8091";
		public const string DiscoverableServiceName = "SpectraMax_iDx";
		public const string DiscoverableServiceIdMask = "net.tcp://{0}:{1}/{2}";

		public const string ReleasePlateAfterShakeMethod = "7DE4CCB4-490E-40D7-9B85-E035EAF59314_RELEASE_PLATE_AFTER_SHAKE";
		public const string InsertPlateAfterShakeMethod = "C4F33F52-63FC-4FEC-ADF6-75CB18F114A9_INSERT_PLATE_AFTER_SHAKE";
		public const string DoOutsideShakeMethod = "6A4867F1-7963-4A4C-9637-2D1844378291_SHAKE_OUTSIDE";
		public const string SetTemperatureMethod = "6481C473-2D20-47DF-96CD-C01A2D18A74B_SET_TEMPERATURE";
		public const string OpenCloseDrawerMethod = "E81D6C3B-B83D-4223-AAFC-FEF0C4812FDD_OPEN/CLOSE_DRAWER";
		public const string TogglePlateHeightMeasurementMethod = "4B9E4175-416D-4E1E-8BFF-0575FDCEFC60_PLATEHEIGHT_MEASUREMENT";
	    public const string ToggleBarcodeMeasurementMethod = "5B9E4175-416D-4E1E-8BFF-0575FDCEFC61_BARCODE_MEASUREMENT";
        public const string ToggleAutoEjectPlateMethod = "645FE957-2E9F-4DFC-9FEA-6836559F6541_AUTO_EJECT_PLATE";
		public const string LockUnlockReaderMethod = "78F3263F-9B1C-4E50-BC35-EA18AFB3A460_LOCK/UNLOCK_READER";
		public const string ShutdownMethod = "766F1F79-F726-4148-9BDD-3531EC4BAD9A_SHUTDOWN";
		public const string ReadMethod = "EDA400DC-3B02-4B07-9911-FC4CEA83A1B1_READ";
		public const string ExecuteFirmwareCommand = "906F7BA2-EB4A-4CF5-A078-DBE62EAD2701_FWCMD";
		public const string ExecuteCommand = "12E52F8F-8C34-41C2-A46B-C7CAD8D3F88D_GeneralCMD";
		public const string PairNfcCommand = "E33959D1-EB23-4C26-8D0B-341AC7394BED_PAIR_NFC";
		public const string Delimiter = "==payload==";
	    public const string SubDelimiter = "==param==";
        public const string UpdateFirmwareCommand = "58A07BA2-EB4A-4CF5-A078-DBE62EAD2701_FWUPDATE";
        public const string GetDevId = "4C80A428-ECBC-4204-B77C-A17FCF00FABF_GET_DEVICEID";
        public const string InjectorMaintenance = "983047B6-B634-4273-BE6C-04245D8BDE1B_INJECTOR_MAINTENANCE";
        public const string ResetCommand = "1589ED2A-BDBC-493A-8B1A-253158966D17_RESET";
        public const string InitCommand = "D9A846E7-011E-4258-89DC-B209AEA7DDDD_INIT";
        public const string FiltersMngmnt = "4EFDE2F0-B8E6-46CC-8A11-564486261449_FILTERS_MNGMNT";
    }
}
