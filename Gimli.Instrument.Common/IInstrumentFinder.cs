﻿using System;
using System.Collections.ObjectModel;

namespace Gimli.Instrument.Common
{
	public interface IInstrumentFinder
	{
		ObservableCollection<string> Discover(Action<string> reporter);
		ObservableCollection<string> DiscoverOutOfDateReader(Action<string> reporter);
	}
}
