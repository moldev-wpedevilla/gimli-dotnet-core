using System.IO;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Messages.OperatingSystem;

namespace Gimli.CoreService.OSController
{
    public interface IOperatingSystemController
    {
        DriveInformation GetDriveInformation(string drive);
        bool PrepareDirectory(string directoryPath, bool deleteIfExists, bool directoryHidden);
        DoubleValueWithRange GetVolume();
        void SetVolume(double volumePercentage);
        DoubleValueWithRange GetBrightness();
        void SetBrightness(double brigthnessPercentage);
        ListValueWithLegalValues<string> GetDateFormat();
        void SetDateFormat(string format);
        ListValueWithLegalValues<string> GetTimeFormat();
        void SetTimeFormat(string format);
        void SetDateTime(double dateTime, bool isDate);
        void StartUsbWatcher();
        BatteryStatus GetBatteryStatus();
        void DismissBatteryStatusInfo();
    }
}