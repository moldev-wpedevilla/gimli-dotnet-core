﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using Castle.Core.Internal;
using Gimli.CoreService.OSController.GraphicsManager;
using Gimli.CoreService.OSController.SoundManager;
using Gimli.CoreService.OSController.USBManager;
using Gimli.CoreService.OSController.WindowsSettings;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using MolDev.Common.Akka.Interfaces;
using NLog;

namespace Gimli.CoreService.OSController
{
    public class OperatingSystemController : IOperatingSystemController
    {
        private enum BatteryDateType
        {
            Changed,
            Check,
            Scheduled,
            Notification
        }

        private readonly IUsbWatcher mIUsbWatcher;
	    private AnthosHelper mAnthosHelper;
        public Logger Log { get; set; } = LogManager.GetCurrentClassLogger();

        public OperatingSystemController(IActorService actorService, IUsbWatcher iUsbWatcher) 
        {
            if (actorService == null) throw new ArgumentNullException(nameof(actorService));

			mAnthosHelper = new AnthosHelper(actorService);
            mIUsbWatcher = iUsbWatcher;
        }

        public DriveInformation GetDriveInformation(string drive)
        {
            var usbDriveInfo = mIUsbWatcher.Drives.FirstOrDefault(x => x.DriveLetter == drive);

            return new DriveInformation
            {
                SpaceTotal = usbDriveInfo?.SpaceTotal ?? 0,
                SpaceFree = usbDriveInfo?.SpaceFree ?? 0,
                FileSystem = usbDriveInfo?.FileSystem ?? "",
                FileSystemCompatible = usbDriveInfo?.FileSystemCompatible ?? false,
                Writeable = usbDriveInfo?.Writeable ?? false
            };
        }
        
        public bool PrepareDirectory(string directoryPath, bool deleteIfExists, bool directoryHidden)
        {
            Log.Debug("OperatingSystemController|PrepareDirectory: Start");
            Log.Debug("OperatingSystemController|PrepareDirectory: " + directoryPath + "|" + deleteIfExists + "|" + directoryHidden);

            if (!Directory.Exists(directoryPath))
            {
                try
                {
                    Directory.CreateDirectory(directoryPath);
                }
                catch
                {
                    return false;
                }
            }

            if (directoryHidden)
            {
                if (!HideDirectory(directoryPath))
                {
                    return false;
                }
            }

            if (deleteIfExists)
            {
                if (!DeleteDirectory(directoryPath))
                {
                    return false;
                }
            }

            Log.Debug("OperatingSystemController|PrepareDirectory: Stop");

            return true;
        }
        private bool HideDirectory(string directoryPath)
        {
            DirectoryInfo directoryInfo;

            try
            {
                directoryInfo = new DirectoryInfo(directoryPath)
                {
                    Attributes = FileAttributes.Directory | FileAttributes.Hidden
                };
            }
            catch
            {
                return false;
            }

            if (directoryInfo.Parent != null && directoryInfo.Parent.ToString().Length <= 4)
            {
                return HideDirectory(directoryInfo.Parent.FullName);
            }
            return true;
        }
        private bool DeleteDirectory(string path, bool deleteRoot = false)
        {
            Log.Debug("OperatingSystemController|DeleteDirectory: Start");
            Log.Debug("OperatingSystemController|DeleteDirectory: " + path + "|" + deleteRoot);

            DirectoryInfo directoryInfo;
            
            try
            {
                 directoryInfo = new DirectoryInfo(path);
            }
            catch
            {
                return false;
            }

            foreach (FileInfo fileInfo in directoryInfo.GetFiles())
            {
                try
                {
                    fileInfo.Delete();
                }
                catch
                {
                    return false;
                }
                
            }
            foreach (DirectoryInfo directoryInfoSub in directoryInfo.GetDirectories())
            {
                try
                {
                    directoryInfoSub.Delete(true);
                }
                catch
                {
                    return false;
                }
            }

            if (deleteRoot)
            {
                try
                {
                    directoryInfo.Delete(true);
                }
                catch
                {
                    return false;
                }
            }
            Log.Debug("OperatingSystemController|DeleteDirectory: Stop");

            return true;
        }

        public DoubleValueWithRange GetVolume()
        {
            double volume = VolumeController.GetVolume();

            Log.Debug("OperatingSystemController|GetVolume: " + volume);

            return new DoubleValueWithRange
            {
                Value = volume,
                Step = 10,
                Minimum = 0,
                Maximum = 100
            };
        }
        public void SetVolume(double volumePercentage)
        {
            Log.Debug("OperatingSystemController|SetVolume: " + volumePercentage);

            VolumeController.SetVolume(volumePercentage);
        }

        public DoubleValueWithRange GetBrightness()
        {
            double brightness = BrightnessController.GetBrightness();

            Log.Debug("OperatingSystemController|GetBrightness: " + brightness);

            return new DoubleValueWithRange
            {
                Value = brightness,
                Step = 10,
                Minimum = 20,
                Maximum = 100
            };

        }
        public void SetBrightness(double brightnessPercentage)
        {
            Log.Debug("OperatingSystemController|SetBrightness: " + brightnessPercentage);

            BrightnessController.SetBrightness(brightnessPercentage, mAnthosHelper);
        }

        public ListValueWithLegalValues<string> GetDateFormat()
        {
            return new ListValueWithLegalValues<string>
            {
                Value = mAnthosHelper.GetSystemValue("GimliDateFormat", "dd.MM.yyyy").ToString(),
                LegalValues = new List<string> {"yyyy-MM-dd", "dd.MM.yyyy", "MM/dd/yyyy"}
            };
        }
        public void SetDateFormat(string format)
        {
			mAnthosHelper.SetSystemValue("GimliDateFormat", format);
        }

        public ListValueWithLegalValues<string> GetTimeFormat()
        {
            return new ListValueWithLegalValues<string>
            {
                Value = mAnthosHelper.GetSystemValue("GimliTimeFormat", "HH:mm:ss").ToString(),
                LegalValues = new List<string> {"hh:mm:ss", "HH:mm:ss"}
            };
        }
        public void SetTimeFormat(string format)
        {
			mAnthosHelper.SetSystemValue("GimliTimeFormat", format);
        }

        public void SetDateTime(double dateTime, bool isDate)
        {
            DateTimeController.SetDateTime(dateTime, isDate);
        }

        public void StartUsbWatcher()
        {
            Log.Debug("OperatingSystemController|StartUsbWatcher: Start");
            mIUsbWatcher.StartUsbWatcher();
        }

        public BatteryStatus GetBatteryStatus()
        {
            Log.Debug("OperatingSystemController|GetBatteryStatus: Start");
            RunBatteryCheck();
            
            DateTime dateTimeCurrent = DateTime.Today;
            DateTime dateTimeChanged = GetBatteryDate(BatteryDateType.Changed);
            DateTime dateTimeCheck = GetBatteryDate(BatteryDateType.Check);
            DateTime dateTimeScheduled = GetBatteryDate(BatteryDateType.Scheduled);
            DateTime dateTimeNotification = GetBatteryDate(BatteryDateType.Notification);

            Log.Debug("OperatingSystemController|GetBatteryStatus:");
            Log.Debug($"  => dateTimeCurrent: {dateTimeCurrent}");
            Log.Debug($"  => dateTimeChanged: {dateTimeChanged}");
            Log.Debug($"  => dateTimeCheck: {dateTimeCheck}");
            Log.Debug($"  => dateTimeScheduled: {dateTimeScheduled}");
            Log.Debug($"  => dateTimeNotification: {dateTimeNotification}");
            Log.Debug($"  => DisplayNotification: {GetDisplayBatteryNotification()}");
            Log.Debug($"  => SchedNote: {(dateTimeScheduled != new DateTime() && dateTimeNotification < dateTimeCurrent && GetDisplayBatteryNotification())}");
            Log.Debug($"  => BattNote: {(dateTimeCheck != new DateTime() && dateTimeCheck > dateTimeCurrent)}");
            
            Log.Debug("OperatingSystemController|GetBatteryStatus: End");

            return new BatteryStatus
            {
                DisplayScheduleNotification = dateTimeScheduled != new DateTime() && dateTimeNotification < dateTimeCurrent && GetDisplayBatteryNotification(),
                BatteryDead = dateTimeCheck != new DateTime() && dateTimeCheck > dateTimeCurrent,
                DateTimeChanged = dateTimeChanged,
                DateTimeScheduled = dateTimeScheduled
            };
        }

        public void DismissBatteryStatusInfo()
        {
            SetDisplayBatteryNotification(true);
        }

        private DateTime GetBatteryDate(BatteryDateType batteryDateType)
        {
            var bufferSize = 128;
            string date;
            
            string directory = ConfigurationManager.AppSettings["PathToBatteryStatusDirectory"];
            string filename = "";

            switch (batteryDateType)
            {
                case BatteryDateType.Changed:
                    filename = ConfigurationManager.AppSettings["FilenameChangeLast"];
                    break;
                case BatteryDateType.Check:
                    filename = ConfigurationManager.AppSettings["FilenameCheckStart"];
                    break;
                case BatteryDateType.Scheduled:
                    filename = ConfigurationManager.AppSettings["FilenameChangeLast"];
                    break;
                case BatteryDateType.Notification:
                    filename = ConfigurationManager.AppSettings["FilenameChangeLast"];
                    break;
            }

            if (!File.Exists(Path.Combine(directory, filename)))
            {
                return new DateTime();
            }
            
            using (var fileStream = File.OpenRead(Path.Combine(directory, filename)))
            {
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, bufferSize))
                {
                    date = streamReader.ReadLine();
                }
            }

            if (date.IsNullOrEmpty())
            {
                return new DateTime();
            }

            string[] dateSplit = date?.Split('-');

            if (dateSplit == null || dateSplit.Length < 3)
            {
                return new DateTime();
            }

            DateTime returnDateTime = new DateTime(int.Parse(dateSplit[0]), int.Parse(dateSplit[1]),int.Parse(dateSplit[2]));

            if (batteryDateType == BatteryDateType.Check)
            {
                returnDateTime = ModificateNotificationDateTime(returnDateTime);
            }
            
            if (batteryDateType == BatteryDateType.Scheduled || batteryDateType == BatteryDateType.Notification)
            {
                //Evaluation of computer name to retrieve serial number
                int[] serialNumberSplit = Array.ConvertAll(ConfigurationManager.AppSettings["SerialNumbersForShortLifetime"].Split(','), int.Parse);
                string[] computerNameSplit = Environment.MachineName.Split('-');
                int serialNumber = 0;
                bool isSerialNumber = false;
                if (computerNameSplit.Length > 1)
                {
                    isSerialNumber = int.TryParse(computerNameSplit[1], out serialNumber);
                }
                
                //Serial numbers in the given intervall are using short lifetime, others are using long lifetime
                if (isSerialNumber && (serialNumber >= serialNumberSplit[0] && serialNumber <= serialNumberSplit[1]) ||
                    Array.IndexOf(serialNumberSplit, serialNumber) > -1)
                {
                    returnDateTime = ModificateLifetimeDateTime(returnDateTime, true);
                }
                else
                {
                    returnDateTime = ModificateLifetimeDateTime(returnDateTime);
                }
            }

            if (batteryDateType == BatteryDateType.Notification)
            {
                returnDateTime = ModificateNotificationDateTime(returnDateTime);
            }

            return returnDateTime;
        }

        private DateTime ModificateNotificationDateTime(DateTime returnDateTime)
        {
            return returnDateTime.AddDays(int.Parse(ConfigurationManager.AppSettings["NotifcationBeforeScheduledDateInDays"]) * -1)
                                 .AddMonths(int.Parse(ConfigurationManager.AppSettings["NotifcationBeforeScheduledDateInMonths"]) * -1)
                                 .AddYears(int.Parse(ConfigurationManager.AppSettings["NotifcationBeforeScheduledDateInYears"]) * -1);
        }

        private DateTime ModificateLifetimeDateTime(DateTime returnDateTime, bool shortLifetime = false)
        {
            return returnDateTime.AddDays(shortLifetime ? int.Parse(ConfigurationManager.AppSettings["BatteryLifetimeShortInDays"])
                                            : int.Parse(ConfigurationManager.AppSettings["BatteryLifetimeInDays"]))
                                 .AddMonths(shortLifetime ? int.Parse(ConfigurationManager.AppSettings["BatteryLifetimeShortInMonths"])
                                              : int.Parse(ConfigurationManager.AppSettings["BatteryLifetimeInMonths"]))
                                 .AddYears(shortLifetime ? int.Parse(ConfigurationManager.AppSettings["BatteryLifetimeShortInYears"])
                                             : int.Parse(ConfigurationManager.AppSettings["BatteryLifetimeInYears"]));
        }

        private bool GetDisplayBatteryNotification()
        {
            var bufferSize = 128;
            string data;

            string directory = ConfigurationManager.AppSettings["PathToBatteryStatusDirectory"];
            string filename = ConfigurationManager.AppSettings["FilenameHideBatteryNotification"];

            if (!File.Exists(Path.Combine(directory, filename)))
            {
                return true;
            }

            using (var fileStream = File.OpenRead(Path.Combine(directory, filename)))
            {
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, bufferSize))
                {
                    data = streamReader.ReadLine();
                }
            }

            return string.IsNullOrWhiteSpace(data);
        }

        private void SetDisplayBatteryNotification(bool hide)
        {
            if (!hide)
            {
                return;
            }
            string directory = ConfigurationManager.AppSettings["PathToBatteryStatusDirectory"];
            string filename = ConfigurationManager.AppSettings["FilenameHideBatteryNotification"];

            using (StreamWriter writer = new StreamWriter(Path.Combine(directory, filename)))
            {
                writer.Write("X");
            }
            Log.Debug($"OperatingSystemController|SetDisplayBatteryNotification: Write ({filename}): (X)");

            directory = ConfigurationManager.AppSettings["PathToBatteryStatusDirectory"];
            filename = ConfigurationManager.AppSettings["FilenameBatteryStatus"];

            using (StreamWriter writer = new StreamWriter(Path.Combine(directory, filename), false))
            {
                writer.Write("Schedule - Replace!");
            }
            Log.Debug($"OperatingSystemController|SetDisplayBatteryNotification: Write ({filename}): (Schedule - Replace!)");
        }

        private void RunBatteryCheck()
        {
            Log.Debug("OperatingSystemController|RunBatteryCheck: Start");

            string directory = ConfigurationManager.AppSettings["PathToBatteryStatusDirectory"];

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);

                string filename = ConfigurationManager.AppSettings["FilenameChangeLast"];

                using (StreamWriter writer = new StreamWriter(Path.Combine(directory, filename)))
                {
                    writer.Write(ConfigurationManager.AppSettings["BatteryInitialChangeLastDate"]);
                }
                Log.Debug($"OperatingSystemController|RunBatteryCheck: Write ({filename}): ({ConfigurationManager.AppSettings["BatteryInitialChangeLastDate"]})");
            }

            if (GetBatteryDate(BatteryDateType.Check) == new DateTime() || GetBatteryDate(BatteryDateType.Check) < DateTime.Today)
            {
                string filename = ConfigurationManager.AppSettings["FilenameCheckStart"];

                using (StreamWriter writer = new StreamWriter(Path.Combine(directory, filename)))
                {
                    writer.Write(DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day);
                }
                Log.Debug($"OperatingSystemController|RunBatteryCheck: Write ({filename}): ({DateTime.Now.Year} - {DateTime.Now.Month} - {DateTime.Now.Day})");
            }
            else
            {
                string filename = ConfigurationManager.AppSettings["FilenameBatteryStatus"];

                using (StreamWriter writer = new StreamWriter(Path.Combine(directory, filename), false))
                {
                    writer.Write("Empty - Replace!");
                }
                Log.Debug($"OperatingSystemController|RunBatteryCheck: Write ({filename}): (Empty - Replace!)");
            }

            Log.Debug("OperatingSystemController|RunBatteryCheck: Stop");
        }
    }

	public class AnthosHelper
	{
		private readonly IActorService mActorService;

		public AnthosHelper(IActorService actorService)
		{
			mActorService = actorService;
		}

		public void SetSystemValue(string key, object value)
		{
			mActorService.Get<InstrumentServiceProxyActor>().Tell(
				MessageExtensions.CreateRequestMessage(new SaveInApexConfigMessage
				{
					ConfigKey = key,
					Value = value,
				}));
		}

		public object GetSystemValue(string key, object defaultValue)
		{
			var task = mActorService.Get<InstrumentServiceProxyActor>()
				.Ask<ReadFromApexConfigMessage>(
					MessageExtensions.CreateRequestMessage(new ReadFromApexConfigMessage
					{
						ConfigKey = key,
						Value = defaultValue,
					}));
			task.Wait();
			return task.Result.Value;
		}
	}
}