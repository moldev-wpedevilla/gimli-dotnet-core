﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Management;
using NLog;

namespace Gimli.CoreService.OSController.USBManager
{
    public class UsbDriveInfo : IEqualityComparer<UsbDriveInfo>
    {
        public Logger Log { get; set; } = LogManager.GetCurrentClassLogger();

        public enum CompatibleFileSystemTypes
        {
            FAT,        //check
            FAT32,
            NTFS,
            exFAT,      //check 
            ReFS
        };

        private readonly ManagementObject mRoot;
        private readonly ManagementObject mDevice;

        public string Name => (string)mDevice.GetPropertyValue("Name");
        public string DriveLetter => (string)mDevice.GetPropertyValue("Name") + @"\";
        public ulong SpaceTotal => (ulong)mDevice.GetPropertyValue("Size");
        public ulong SpaceFree => (ulong)mDevice.GetPropertyValue("FreeSpace");
        public string FileSystem => (string)mDevice.GetPropertyValue("FileSystem");
        public bool FileSystemCompatible => Enum.IsDefined(typeof(CompatibleFileSystemTypes), FileSystem);
        public bool Writeable => CheckWriteable();
        public bool GimliDriveChecked { get; set; }
        public bool GimliDrive { get; private set; }
        public string GimliDataPath { get; private set; }
        public UsbDriveInfo(ManagementObject device, ManagementObject root)
        {
            mDevice = device;
            mRoot = root;
        }
        public void IsGimliDrive(bool isRelevant)
        {
            GimliDrive = isRelevant;
        }
        public void SetGimliDataPath(string path)
        {
            IsGimliDrive(path.Length > 0);
            GimliDataPath = path;
        }
        public int GetHashCode(UsbDriveInfo usbDriveInfo)
        {
            if (usbDriveInfo == null)
            {
                return 0;
            }

            return Name.GetHashCode();
        }
        public bool Equals(UsbDriveInfo x, UsbDriveInfo y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }
            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
            {
                return false;
            }

            return compareProperty(x, y, "Name") && compareProperty(x, y, "DeviceID") && compareProperty(x, y, "DeviceID", true);
        }
        private bool compareProperty(UsbDriveInfo x, UsbDriveInfo y, string property, bool rootProperty = false)
        {
            if (rootProperty)
            {
                return x.mRoot.GetPropertyValue(property) == y.mRoot.GetPropertyValue(property);
            }

            return x.mDevice.GetPropertyValue(property) == y.mDevice.GetPropertyValue(property);
        }

        private bool CheckWriteable()
        {
            Log.Debug($"usbDriveInfo: Start ({@DriveLetter})");

            int errorCountMax = 5;
            int errorCount = 0;
            bool writeable = false;
            StreamWriter sw = null;

            do
            {
                try
                {
                    sw = new StreamWriter(@DriveLetter + @"\a.a");
                }
                catch (Exception e)
                {
                    Log.Debug($"UsbDriveInfo|CheckWriteable|Write|Error: {e}");
                    errorCount++;
                    continue;
                }

                try
                {
                    sw?.Dispose();
                }
                catch (Exception e)
                {
                    Log.Debug($"UsbDriveInfo|CheckWriteable|Write|Dispose: {e}");
                    errorCount++;
                    continue;
                }

                try
                {
                    File.Delete(@DriveLetter + @"\a.a");
                }
                catch (Exception e)
                {
                    Log.Debug($"UsbDriveInfo|CheckWriteable|Write|Delete: {e}");
                    errorCount++;
                    continue;
                }

                writeable = true;

            } while (!writeable && errorCount < errorCountMax);
            
            Log.Debug($"UsbDriveInfo|CheckWriteable|AdditionalInfo (Filesystem): {FileSystem}");

            return writeable;
        }
    }
}