﻿using System.Collections.Generic;
using System.Management;

namespace Gimli.CoreService.OSController.USBManager
{
    public interface IUsbWatcher
    {
        List<UsbDriveInfo> Drives { get; set; }
        List<string> DriveLetters { get; }
        List<string> LastAddedDriveLetters { get;  }
        void StartUsbWatcher();
        void CheckPlugin(object sender, EventArrivedEventArgs e);
    }
}