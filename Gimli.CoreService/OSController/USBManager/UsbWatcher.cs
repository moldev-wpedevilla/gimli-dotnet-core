﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Threading.Tasks;
using Gimli.CoreService.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using MolDev.Common.Akka.Interfaces;
using NLog;

namespace Gimli.CoreService.OSController.USBManager
{
    public class UsbWatcher : IUsbWatcher
    {
        private UsbWatcher instance;
        private bool started = false; 

        private readonly IActorService mActorService;
        private bool mDrivesAdded;
        private bool mValidDrivesAdded;

        public List<UsbDriveInfo> Drives { get; set; }
        public List<UsbDriveInfo> LastAddedDrives { get; set; }
        public Logger Log { get; set; } = LogManager.GetCurrentClassLogger();

        public List<string> DriveLetters
        {
            get
            {
                List<string> driveLetters = new List<string>();

                foreach (var drive in Drives)
                {
                    driveLetters.Add(drive.DriveLetter);
                }

                return driveLetters;
            }
        }

        public List<string> LastAddedDriveLetters
        {
            get
            {
                List<string> lastDriveLetters = new List<string>();

                foreach (var drive in LastAddedDrives)
                {
                    lastDriveLetters.Add(drive.DriveLetter);
                }

                return lastDriveLetters;
            }
        }

        void DeviceInsertedEvent(object sender, EventArrivedEventArgs e)
        {
            Log.Debug("UsbWatcher|DeviceInsertedEvent: " + e);
            Task.Run(() => CheckPlugin(sender, e));
        }

        public void CheckPlugin(object sender, EventArrivedEventArgs e)
        {
            UpdateDrives(GetUsbDrives(), true);
            NotifyPluggedInDrives();
        }

        void DeviceRemovedEvent(object sender, EventArrivedEventArgs e)
        {
            UpdateDrives(GetUsbDrives());
            NotifyPluggedOutDrives();
        }
        
        public UsbWatcher(IActorService actorService)
        {
            Log.Debug($"UsbWatcher|Constructor ({actorService != null})");

            if (actorService == null) throw new ArgumentNullException(nameof(actorService));

            mActorService = actorService;
            instance = this;
        }

        public void StartUsbWatcher()
        {
            if (!started)
            {
                Drives = GetUsbDrives();
                LastAddedDrives = Drives;
                NotifyPluggedInDrives(true);

                InitInsertWatcher();
                InitRemoveWatcher();
                started = true;
            }
        }

        private List<UsbDriveInfo> GetUsbDrives()
        {
            List<UsbDriveInfo> drives = new List<UsbDriveInfo>();

            foreach (var device in new ManagementObjectSearcher("select * from Win32_DiskDrive where InterfaceType='USB'").Get())
            {
                drives.AddRange(ScanUsbDeviceForDrives((string)device["DeviceID"], (ManagementObject)device));
            }

            Log.Debug("UsbWatcher|GetUsbDrives: " + drives);

            return drives;
        }

        private List<UsbDriveInfo> ScanUsbDeviceForDrives(string deviceId, ManagementObject rootDevice)
        {
            List<UsbDriveInfo> drives = new List<UsbDriveInfo>();

            foreach (ManagementObject partition in new ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskDrive.DeviceID='" + deviceId +"'} WHERE AssocClass = Win32_DiskDriveToDiskPartition").Get())
            {
                foreach (ManagementObject drive in new ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskPartition.DeviceID='" + partition["DeviceID"] +"'} WHERE AssocClass = Win32_LogicalDiskToPartition").Get())
                {
                    drives.Add(new UsbDriveInfo(drive, rootDevice));
                }
            }

            Log.Debug("UsbWatcher|ScanUsbDeviceForDrives: " + deviceId + "|" + rootDevice + "|" + drives);

            return drives;
        }

        private void UpdateDrives(List<UsbDriveInfo> actualDrives, bool plugin = false)
        {
            if (plugin)
            {
                LastAddedDrives.Clear();

                mDrivesAdded = actualDrives.Count > Drives.Count;

                LastAddedDrives = actualDrives.Where(x => !Drives.Contains(x)).ToList();
                mValidDrivesAdded = LastAddedDrives.Any(x => x.FileSystemCompatible);
            }

            Log.Debug("UsbWatcher|UpdateDrives: " + actualDrives + "|" + plugin + "|" + Drives);

            Drives = actualDrives;
        }

        private void InitInsertWatcher()
        {
            WqlEventQuery insertQuery = new WqlEventQuery("SELECT * FROM __InstanceCreationEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_USBHub'");
            ManagementEventWatcher insertWatcher = new ManagementEventWatcher(insertQuery);
            insertWatcher.EventArrived += DeviceInsertedEvent;
            insertWatcher.Start();
            Log.Debug("UsbWatcher|InitInsertWatcher: Start");
        }

        private void InitRemoveWatcher()
        {
            WqlEventQuery removeQuery = new WqlEventQuery("SELECT * FROM __InstanceDeletionEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_USBHub'");
            ManagementEventWatcher removeWatcher = new ManagementEventWatcher(removeQuery);
            removeWatcher.EventArrived += DeviceRemovedEvent;
            removeWatcher.Start();
            Log.Debug("UsbWatcher|InitRemoveWatcher: Stop");
        }

        private void NotifyPluggedInDrives(bool initialCheck = false)
        {
            Log.Debug("UsbWatcher|NotifyPluggedInDrives: Plug in (StartUp Enumeration: " + initialCheck + ")");
            if (Drives.Count > 0)
            {
                mActorService.Get<CoreServiceRoutingActor>().Tell(MessageExtensions.CreateRequestMessage(new UsbDrivePluggedInMessage { InitialCheck = initialCheck, DrivesAdded = mDrivesAdded, ValidDrive = mValidDrivesAdded }));
            }
        }

        private void NotifyPluggedOutDrives()
        {
            Log.Debug("UsbWatcher|NotifyPluggedOutDrives: Plug out");
            mActorService.Get<CoreServiceRoutingActor>().Tell(MessageExtensions.CreateRequestMessage(new UsbDrivePluggedOutMessage()));
        }
    }
}
