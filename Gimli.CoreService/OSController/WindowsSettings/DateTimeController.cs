﻿using System;
using System.Runtime.InteropServices;

namespace Gimli.CoreService.OSController.WindowsSettings
{
    class DateTimeController
    {
		// TODO someone: do something to reenable this feature
        [DllImport("kernel32.dll", SetLastError = true)]
		public static extern bool SetSystemTime(ref Systemtime st);

        [StructLayout(LayoutKind.Sequential)]
        public struct Systemtime
        {
            public short wYear;
            public short wMonth;
            public short wDayOfWeek;
            public short wDay;
            public short wHour;
            public short wMinute;
            public short wSecond;
            public short wMilliseconds;
        }

        public static void SetDateTime(double timestamp, bool isDate)
        {
            Systemtime systemtime = new Systemtime();

            DateTimeOffset dateTimeOffset = isDate ? DateTimeOffset.FromUnixTimeMilliseconds((long)timestamp) : DateTimeOffset.FromUnixTimeMilliseconds((long) timestamp*1000);
            systemtime.wYear = isDate ? (short)dateTimeOffset.Year : (short)DateTime.Now.Year;
            systemtime.wMonth = isDate ? (short)dateTimeOffset.Month : (short)DateTime.Now.Month;
            systemtime.wDay = isDate ? (short)dateTimeOffset.Day : (short)DateTime.Now.Day;
            systemtime.wHour = isDate ? (short)DateTime.Now.Hour : (short)dateTimeOffset.Hour;
            systemtime.wMinute = isDate ? (short)DateTime.Now.Minute : (short)dateTimeOffset.Minute;
            systemtime.wSecond = isDate ? (short)DateTime.Now.Second : (short)dateTimeOffset.Second;
            
            SetSystemTime(ref systemtime);
        }
    }
}
