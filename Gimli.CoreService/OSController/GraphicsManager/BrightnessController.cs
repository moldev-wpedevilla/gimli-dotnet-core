﻿using System;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Drawing;

namespace Gimli.CoreService.OSController.GraphicsManager
{
	// TODO someone: do something to reenable this feature
	public class BrightnessController
    {
        //[DllImport("gdi32.dll")]
        //private static extern unsafe int GetDeviceGammaRamp(IntPtr hDC, ref ramp ramp);
        //[DllImport("gdi32.dll")]
        //private static extern unsafe bool SetDeviceGammaRamp(IntPtr hDC, void* ramp);

        private struct ramp
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)] public UInt16[] Red;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)] public UInt16[] Green;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)] public UInt16[] Blue;
        }

        public static double GetBrightness()
        {
            ramp ramp = new ramp();
            //GetDeviceGammaRamp(Graphics.FromHwnd(IntPtr.Zero).GetHdc(), ref ramp);
            var first =  ramp.Red[1] - 128;

            if (first < 23) return 20f;
            if (first > 23 && first <= 43) return 30f;
            if (first > 43 && first <= 64) return 40f;
            if (first > 64 && first <= 84) return 50f;
            if (first > 84 && first <= 105) return 60f;
            if (first > 105 && first <= 125) return 70f;
            if (first > 125 && first <= 146) return 80f;
            if (first > 146 && first <= 166) return 90f;
            return 100f;
        }

        public static void SetBrightness(double? targetBrightness = null, AnthosHelper anthosHelper = null)
        {
            double brightness = 0f;

         //   if (targetBrightness == null)
         //   {
         //       if (Convert.ToBoolean(ConfigurationManager.AppSettings["ResetBrightness"]))
         //       {
	        //        if (anthosHelper == null)
	        //        {
		       //         Anthos.Utilities.AppSetting.All.System[
			      //          "GimliBrightness", ConfigurationManager.AppSettings["DefaultBrightness"]] =
			      //          ConfigurationManager.AppSettings["DefaultBrightness"];
		       //         Anthos.Utilities.AppSetting.Save();
	        //        }
	        //        else
	        //        {
		       //         anthosHelper.SetSystemValue("GimliBrightness", ConfigurationManager.AppSettings["DefaultBrightness"]);
	        //        }
         //       }

	        //    var gimliBrightness =
		       //     Convert.ToDouble(anthosHelper == null
			      //      ? Anthos.Utilities.AppSetting.All.System[
				     //       "GimliBrightness", ConfigurationManager.AppSettings["DefaultBrightness"]]
			      //      : anthosHelper.GetSystemValue("GimliBrightness",
				     //       ConfigurationManager.AppSettings["DefaultBrightness"]));
         //       targetBrightness = gimliBrightness;
         //   }

	        //if (anthosHelper == null)
	        //{
		       // Anthos.Utilities.AppSetting.All.System["GimliBrightness"] = targetBrightness;
		       // Anthos.Utilities.AppSetting.Save();
	        //}
	        //else
	        //{
		       // anthosHelper.SetSystemValue("GimliBrightness", targetBrightness.ToString());
	        //}

	        if (targetBrightness <= 25) brightness = 55f;
            else if (targetBrightness > 25 && targetBrightness <= 35) brightness = 63f;
            else if (targetBrightness > 35 && targetBrightness <= 45) brightness = 71f;
            else if (targetBrightness > 45 && targetBrightness <= 55) brightness = 79f;
            else if (targetBrightness > 55 && targetBrightness <= 65) brightness = 87f;
            else if (targetBrightness > 65 && targetBrightness <= 75) brightness = 95f;
            else if (targetBrightness > 75 && targetBrightness <= 85) brightness = 103f;
            else if (targetBrightness > 85 && targetBrightness <= 95) brightness = 111f;
            else if (targetBrightness > 95) brightness = 119f;

            brightness = brightness/100f;
            
            IntPtr hdc = Graphics.FromHwnd(IntPtr.Zero).GetHdc();
            double[] gammas = { 1, 1, 1 };

            //ushort* gArray = stackalloc ushort[3 * 256];
            //ushort* idx = gArray;

            //for (int j = 0; j < 3; j++)
            //{
            //    for (int i = 0; i < 256; i++)
            //    {
            //        double arrayVal = (Math.Pow(i / 256.0, 1.0 / gammas[j]) * 65535) + 0.5;
            //        arrayVal *= brightness;

            //        if (arrayVal > 65535)
            //            arrayVal = 65535;
            //        if (arrayVal < 0)
            //            arrayVal = 0;

            //        *idx = (ushort)arrayVal;
            //        idx++;
            //    }
            //}

            //SetDeviceGammaRamp(hdc, gArray);
        }
    }
}
