﻿using System.Configuration;
using Gimli.CoreService.OSController;
using Gimli.CoreService.Properties;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.CoreService.Actors
{
	public class SystemInitActor : SystemInitActorBase
	{
        private readonly IActorService mActorService;
        private readonly IOperatingSystemController mOperatingSystemController;

        public SystemInitActor(IActorService actorService, IOperatingSystemController operatingSystemController, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
            mActorService = actorService;
            mOperatingSystemController = operatingSystemController;

            Receive<RequestMessage<RunAutoStartMessage>>(message =>
            {
                AutoStartUsbHandling();
                AutoStartCheckBattery();
            });
        }

		protected override bool InitProxies(IActorService actorService)
		{
			var success = IsProxyConnected<WebServiceProxyActor>(actorService, GetGuaranteedAnswer);
			success = IsProxyConnected<DataServiceProxyActor>(actorService, GetGuaranteedAnswer) && success;
			success = IsProxyConnected<InstrumentServiceProxyActor>(actorService, GetGuaranteedAnswer) && success;
			return success;
		}

	    private void AutoStartUsbHandling()
	    {
	        mOperatingSystemController.StartUsbWatcher();
            mActorService.Get<AppManagerProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new StartUsbWatcherMessage()));
	    }

        private void AutoStartCheckBattery()
        {
            if (bool.Parse(ConfigurationManager.AppSettings["CheckBatteryStatus"]))
            {
                BatteryStatus batteryStatus = mOperatingSystemController.GetBatteryStatus();

                //As windows 8.1 changes its date to a recent one if it detects a dead battery, this check has to be temporarily disabled
                /*if (batteryStatus.BatteryDead)
                {
                    mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
                    {
                        Parameters = new MessageParameters
                        {
                            Message = Resources.Battery_Dead,
                            MessageType = MessageType.Warning,
                            DisplayType = MessageDisplayType.DialogOk,
                            MessageCode = MessageStatusCodes.DismissBatteryDeadMessage
                        }
                    }));
                }
                else*/ if(batteryStatus.DisplayScheduleNotification)
                {
                    mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
                    {
                        Parameters = new MessageParameters
                        {
                            Message = string.Format(Resources.Battery_Maintenance, batteryStatus.DateTimeScheduled.ToString("dd/MM/yyyy")),
                            MessageType = MessageType.Warning,
                            DisplayType = MessageDisplayType.DialogOk,
                            MessageCode = MessageStatusCodes.DismissBatteryScheduleMessage
                        }
                    }));
                }
            }
        }
	}
}
