﻿using System;
using Akka.Actor;
using Gimli.CoreService.OSController.USBManager;
using Gimli.CoreService.Properties;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.CoreService.Actors.OSController
{
    class UsbWatcherActor : GimliActorBase
    {
        private readonly IActorService mActorService;

        private readonly IUsbWatcher mIUsbWatcher;

        public UsbWatcherActor(IActorService actorService, IUsbWatcher iUsbWatcher, IActorsystemOverallState systemState) : base(actorService, systemState)
        {
            if (actorService == null) throw new ArgumentNullException(nameof(actorService));

            mActorService = actorService;
            mIUsbWatcher = iUsbWatcher;

            Receive<RequestMessage<UsbDrivePluggedInMessage>>(message =>
            {
                //mActorService.Get<AppManagerProxyActor>().Forward(message);
                

				if (message.Model.InitialCheck)
                {
                    Log.Info("UsbWatcherActor|<UsbDrivePluggedInMessage>: Plug in at InitialCheck");

                    mActorService.Get<DataServiceProxyActor>().Tell(message);

                    return;
				}

                if (message.Model.DrivesAdded && message.Model.ValidDrive)
                {
                    Log.Info("UsbWatcherActor|<UsbDrivePluggedInMessage>: Message to Frontend (USB_drive_valid)");

                    mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
                    {
                        Parameters = new MessageParameters
                        {
                            Message = string.Format(Resources.USB_drive_valid),
                            MessageType = MessageType.Info,
                            DisplayType = MessageDisplayType.Toast,
                            MessageCode = MessageStatusCodes.UsbDrivePluginValid
                        }
                    }));

                    mActorService.Get<DataServiceProxyActor>().Tell(message);
                }
                else
                {
                    Log.Info("UsbWatcherActor|<UsbDrivePluggedInMessage>: Message to Frontend (USB_drive_invalid)");
                    
                    mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
                    {
                        Parameters = new MessageParameters
                        {
                            Message = string.Format(Resources.USB_drive_invalid),
                            MessageType = MessageType.Info,
                            DisplayType = MessageDisplayType.Toast,
                            MessageCode = MessageStatusCodes.UsbDrivePluginInvalid
                        }
                    }));
                }
            });

            Receive<RequestMessage<UsbDrivePluggedOutMessage>>(message =>
            {
                mActorService.Get<AppManagerProxyActor>().Forward(message);
                mActorService.Get<DataServiceProxyActor>().Tell(message);
                mActorService.Get<WebServiceProxyActor>().Tell(message);
			});

			Receive<RequestMessage<GetPluggedInUsbDrivesMessage>>(message =>
            {
                Sender.Tell(new GetPluggedInUsbDrivesResponseMessage { DriveLetters = mIUsbWatcher.LastAddedDriveLetters, LastDriveLetters = mIUsbWatcher.LastAddedDriveLetters });
            });
        }
    }
}
