﻿using System;
using Akka.Actor;
using Gimli.CoreService.Actors.OSController;
using Gimli.CoreService.OSController;
using Gimli.CoreService.Properties;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.CoreService.Actors
{
    public class OperatingSystemActor : GimliActorBase
    {
        const string UsbWatcherActorName = nameof(UsbWatcherActor);

        private readonly IOperatingSystemController mOperatingSystemController;

        public OperatingSystemActor(IActorService actorService, IOperatingSystemController operatingSystemController,
            IActorsystemOverallState systemState) : base(actorService, systemState)
        {
            if (actorService == null) throw new ArgumentNullException(nameof(actorService));
            if (operatingSystemController == null) throw new ArgumentNullException(nameof(operatingSystemController));

            mOperatingSystemController = operatingSystemController;

            Receive<RequestMessage<UsbDrivePluggedInMessage>>(message =>
            {
                var usbWatcherActor = GetChild<UsbWatcherActor>(UsbWatcherActorName);
                usbWatcherActor.Forward(message);
            });

            Receive<RequestMessage<UsbDrivePluggedOutMessage>>(message =>
            {
                var usbWatcherActor = GetChild<UsbWatcherActor>(UsbWatcherActorName);
                usbWatcherActor.Forward(message);
            });

            Receive<RequestMessage<GetPluggedInUsbDrivesMessage>>(message =>
            {
                var usbWatcherActor = GetChild<UsbWatcherActor>(UsbWatcherActorName);
                usbWatcherActor.Forward(message);
            });

			Receive<RequestMessage<GetDriveInformationMessage>>(message =>
            {
                DriveInformation driveInformation = mOperatingSystemController.GetDriveInformation(message.Model.DriveLetter);

                Sender.Tell(new GetDriveInformationResponseMessage
                {
                    DriveInformation = driveInformation
                });
            });

            Receive<RequestMessage<PrepareDirectoryMessage>>(message =>
            {
                bool success = mOperatingSystemController.PrepareDirectory(message.Model.DirectoryName,
                    message.Model.DeleteIfExists, message.Model.DirectoryHidden);
                if (success)
                {
                    Sender.Tell(new GimliResponse());
                }
                else
                {
                    Sender.Tell(new GimliResponse(new ErrorInfo {Message = Resources.Error_Prepare_Directory},
                        ResultStatusEnum.Error, Log));
                }
            });

            Receive<RequestMessage<GetGlobalConfigurationMessage>>(message =>
            {
                Sender.Tell(GetGuaranteedAnswer(m =>
                {
                    switch (m.Model.Configuration)
                    {
                        case ConfigurationPart.System:
                            return new GetGlobalConfigurationResponseMessage
                            {
                                SystemConfiguration = new SystemConfiguration
                                {
                                    Volume = mOperatingSystemController.GetVolume(),
                                    Brightness = mOperatingSystemController.GetBrightness(),
                                    DateFormat = mOperatingSystemController.GetDateFormat(),
                                    TimeFormat = mOperatingSystemController.GetTimeFormat()
                                }
                            };
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }, message));
            });

            Receive<RequestMessage<SetVolumeMessage>>(message =>
            {
                Sender.Tell(GetGuaranteedAnswer(m =>
                {
                    mOperatingSystemController.SetVolume(message.Model.Value);
                    return new GimliResponse();
                }, message));
            });
            
            Receive<RequestMessage<SetBrightnessMessage>>(message =>
            {
                Sender.Tell(GetGuaranteedAnswer(m =>
                {
                    mOperatingSystemController.SetBrightness(message.Model.Value);
                    return new GimliResponse();
                }, message));
            });

            Receive<RequestMessage<SetDateTimeFormatMessage>>(message =>
            {
                Sender.Tell(GetGuaranteedAnswer(m =>
                {
                    if (m.Model.IsDate)
                    {
                        mOperatingSystemController.SetDateFormat(m.Model.Value);
                    }
                    else
                    {
                        mOperatingSystemController.SetTimeFormat(m.Model.Value);
                    }
                    return new GimliResponse();
                }, message));
            });

			Receive<RequestMessage<SetDateTimeMessage>>(message =>
			{
				Sender.Tell(GetGuaranteedAnswer(m =>
				{
				    mOperatingSystemController.SetDateTime(m.Model.Value, m.Model.IsDate);
					return new GimliResponse();
				}, message));
			});

            Receive<RequestMessage<CheckBatteryStatusMessage>>(message =>
            {
                Sender.Tell(GetGuaranteedAnswer(m =>
                {
                    mOperatingSystemController.GetBatteryStatus();
                    return new GimliResponse();
                }, message));
            });

            Receive<RequestMessage<CheckBatteryDismissMessage>>(message =>
            {
                mOperatingSystemController.DismissBatteryStatusInfo();
            });
        }

        protected override void PreStart()
        {
            GetChild<UsbWatcherActor>(UsbWatcherActorName);
        }
    }
}
