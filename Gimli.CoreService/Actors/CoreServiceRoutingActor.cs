﻿using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.CoreService.Actors
{
	public class CoreServiceRoutingActor : ToplevelRoutingActorBase
	{
	    const string OperatingSystemActorName = nameof(OperatingSystemActor);
	    const string SystemInitActorName = nameof(SystemInitActor);

		public CoreServiceRoutingActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			Receive<RequestMessage<UsbDrivePluggedInMessage>>(message =>
            {
                GetChild<OperatingSystemActor>(OperatingSystemActorName).Forward(message);
            });

            Receive<RequestMessage<UsbDrivePluggedOutMessage>>(message =>
            {
                GetChild<OperatingSystemActor>(OperatingSystemActorName).Forward(message);
            });

            Receive<RequestMessage<GetPluggedInUsbDrivesMessage>>(message =>
		    {
                GetChild<OperatingSystemActor>(OperatingSystemActorName).Forward(message);
            });

		    Receive<RequestMessage<GetDriveInformationMessage>>(message =>
		    {
                GetChild<OperatingSystemActor>(OperatingSystemActorName).Forward(message);
            });

		    Receive<RequestMessage<PrepareDirectoryMessage>>(message =>
		    {
                GetChild<OperatingSystemActor>(OperatingSystemActorName).Forward(message);
            });

		    /*Receive<RequestMessage<CopyFilesMessage>>(message =>
		    {
                GetChild<OperatingSystemActor>(OperatingSystemActorName).Forward(message);
            });*/

		    Receive<RequestMessage<SetVolumeMessage>>(message =>
		    {
                GetChild<OperatingSystemActor>(OperatingSystemActorName).Forward(message);
            });

		    Receive<RequestMessage<SetBrightnessMessage>>(message =>
		    {
                GetChild<OperatingSystemActor>(OperatingSystemActorName).Forward(message);
            });

            Receive<RequestMessage<SetDateTimeFormatMessage>>(message =>
            {
                GetChild<OperatingSystemActor>(OperatingSystemActorName).Forward(message);
            });

			Receive<RequestMessage<SetDateTimeMessage>>(message =>
			{
                GetChild<OperatingSystemActor>(OperatingSystemActorName).Forward(message);
            });

            Receive<RequestMessage<CheckBatteryStatusMessage>>(message =>
            {
                GetChild<OperatingSystemActor>(OperatingSystemActorName).Forward(message);
            });

            Receive<RequestMessage<CheckBatteryDismissMessage>>(message =>
            {
                GetChild<OperatingSystemActor>(OperatingSystemActorName).Forward(message);
            });

			Receive<RequestMessage<RunAutoStartMessage>>(message =>
			{
                GetSysInitActor().Forward(message);
            });

			Receive<RequestMessage<GetGlobalConfigurationMessage>>(message =>
			{
				GetChild<OperatingSystemActor>(OperatingSystemActorName).Forward(message);
			});
        }
        
		protected override IActor GetSysInitActor()
		{
			return GetChild<SystemInitActor>(SystemInitActorName);
		}

		protected override void PreStart()
		{
		    GetChild<OperatingSystemActor>(OperatingSystemActorName);

			base.PreStart();
		}
	}
}
