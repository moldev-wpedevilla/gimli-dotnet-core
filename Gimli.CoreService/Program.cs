﻿using System;
using System.Reflection;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Gimli.CoreService.OSController;
using Gimli.CoreService.OSController.USBManager;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;

namespace Gimli.CoreService
{
	class Program
	{
	    static void Main()
	    {
			var thisAssemblyName = Assembly.GetEntryAssembly().GetName().Name;
			var configFilePath = thisAssemblyName + ".dll.config";
			AppConfigurationManager.Build(configFilePath);

			var factory = new GimliActorHostServiceFactory();
			factory.Run(
				RemoteActorConfiguration.Instance.ServiceConfigurations[ActorServiceKey.Core],
				"Gimli.CoreService",
				"Gimli CoreService",
				"Gimli CoreService with TopShelf",
				configFilePath,
				container =>
				{
					RegisterComponents(container, factory);
				});
		}

		internal static void RegisterComponents(IWindsorContainer container, GimliActorHostServiceFactory factory)
		{
			container.Register(Component
				.For<IActorsystemOverallState>()
				.Instance(new ActorsystemOverallState { ServiceId = ActorServiceKey.Core })
				.LifestyleSingleton());
			container.Register(Component
				.For<IUsbWatcher>()
				.ImplementedBy<UsbWatcher>()
				.LifestyleSingleton());
			container.Register(Component
				.For<IOperatingSystemController>()
				.ImplementedBy<OperatingSystemController>()
				.LifestyleSingleton());
			container.Register(Component.For<Func<GimliActorHost>>().Instance(() => factory.Host));
		}
	}
}
