﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Gimli.AppStarter.Manifest;
using Gimli.JsonObjects;
using Gimli.JsonObjects.DummyData;
//using Ionic.Zlib;
using NLog;

namespace Gimli.AppStarter.PackageManager
{
    public class PackageManager
    {
        
        public static Logger Log { get; set; } = LogManager.GetCurrentClassLogger();

        private static readonly Random Random = new Random((int)DateTime.Now.Ticks);

        public static FileInfo CopyFile(FileInfo fileInfoSource, string updateCopyLocation)
        {
            GimliTrace.Start("CopyFile");
            Log.Debug($"PackageManager|CopyFile ({fileInfoSource}) ({updateCopyLocation}): Start");

            FileInfo fileInfoTarget = new FileInfo(Path.Combine(updateCopyLocation, fileInfoSource.Name));
            try
            { 
                PrepareDirectory(fileInfoTarget.DirectoryName);
                File.Copy(fileInfoSource.FullName, fileInfoTarget.FullName, true);
            }
            catch (Exception e)
            {
                Log.Debug($"PackageManager|CopyFile ({fileInfoSource}) ({updateCopyLocation}): Exception ({e.Message})");
                fileInfoTarget = null;
            }

            var elapsed = GimliTrace.Stop("CopyFile");
            Log.Debug($"PackageManager|CopyFile ({fileInfoSource}) ({updateCopyLocation}): Stop ({elapsed}ms)");
            return fileInfoTarget;
        }

        public static void CopyDirectory(string source, string destination)
        {
            GimliTrace.Start("CopyDirectory");
            Log.Debug($"PackageManager|CopyDirectory ({source}) ({destination}): Start");

            foreach (string dirPath in Directory.GetDirectories(source, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(source, destination));
            }

            foreach (string newPath in Directory.GetFiles(source, "*.*", SearchOption.AllDirectories))
            {
                File.Copy(newPath, newPath.Replace(source, destination), true);
            }

            var elapsed = GimliTrace.Stop("CopyDirectory");
            Log.Debug($"PackageManager|CopyDirectory ({source}) ({destination}): Stop ({elapsed}ms)");
        }

        public static FileInfo Peek(string pathSource, string pathTemp, string fileName, bool delete = true)
        {
            GimliTrace.Start("Peek");
            Log.Debug($"PackageManager|Peek ({pathSource}): Start");

            string randomDirectoryName = GetRandomDirectoryName();
            randomDirectoryName = Path.Combine(pathTemp, randomDirectoryName);
            PrepareDirectory(randomDirectoryName);

            FileInfo fileInfo = Extract(pathSource, randomDirectoryName, fileName);

            if (delete)
            {
                DeleteDirectory(randomDirectoryName, true);
            }

            var elapsed = GimliTrace.Stop("Peek");
            Log.Debug($"PackageManager|Peek ({pathSource}): Stop ({elapsed}ms)");

            return fileInfo;
        }

        public static UpdateManifest PeekManifest(string pathSource, string pathTemp, string fileName, out string version)
        {
            GimliTrace.Start("PeekManifest");
            Log.Debug($"PackageManager|PeekManifest ({pathSource}): Start");

            FileInfo fileInfo = Peek(pathSource, pathTemp, fileName, false);

            UpdateManifest updateManifest = new UpdateManifestReader(fileInfo.FullName).Get();

            DeleteDirectory(pathTemp);

            version = updateManifest.Version;

            var elapsed = GimliTrace.Stop("PeekManifest");
            Log.Debug($"PackageManager|PeekManifest ({pathSource}): Stop ({elapsed}ms)");

            return updateManifest;
        }

        public static bool CheckChecksum(string pathSource)
        {
            Log.Debug("PackageManager|CheckChecksum: Start");
            string checksum = GenerateChecksum(pathSource, new [] { AppConfigurationManager.GetSettingsValue("updateManifest") });

            UpdateManifest updateManifest = new UpdateManifestReader(Path.Combine(pathSource, AppConfigurationManager.GetSettingsValue("updateManifest"))).Get();

            Log.Debug($"PackageManager|CheckChecksum|ManifestChecksum: {updateManifest.Checksum}|PendingChecksum: {checksum}");
            return updateManifest.Checksum == checksum;
        }

        public static bool Extract(string pathSource, string pathTarget, bool deleteSource = false)
        {
            GimliTrace.Start("Extract");
            Log.Debug($"PackageManager|Extract ({pathSource}) ({pathTarget}): Start");

			// TODO someone: do something to reenable this feature
			//using (ZipFile zipFile = ZipFile.Read(pathSource))
			//{
			//    foreach (ZipEntry zipEntry in zipFile)
			//    {
			//        bool success = false;
			//        int loopCount = 0;
			//        int errorCount = 0;

			//        while (loopCount <= 3 && !success)
			//        {
			//            loopCount++;

			//            try
			//            {
			//                zipEntry.Extract(pathTarget, ExtractExistingFileAction.OverwriteSilently);
			//                success = true;
			//            }
			//            catch (Exception e)
			//            {
			//                Log.Debug($"PackageManager|Extract ({pathSource}) ({pathTarget}): Exception ({e.Message})");
			//                errorCount++;
			//            }
			//        }

			//        if (errorCount == 3)
			//        {
			//            Log.Debug($"PackageManager|Extract ({pathSource}) ({pathTarget}): Cancel (3)");
			//            return false;
			//        }
			//    }
			//}

			File.Delete(pathSource);

            var elapsed = GimliTrace.Stop("Extract");
            Log.Debug($"PackageManager|Extract ({pathSource}) ({pathTarget}): Stop ({elapsed}ms)");

            return true;
        }

        public static FileInfo Extract(string pathSource, string pathTarget, string fileName)
        {
            GimliTrace.Start("Extract");
            Log.Debug($"PackageManager|Extract ({pathSource}) ({pathTarget}) ({fileName})");

			// TODO someone: do something to reenable this feature
			//using (ZipFile zipFile = ZipFile.Read(pathSource))
			//{
			//    foreach (ZipEntry zipEntry in zipFile)
			//    {
			//        if (zipEntry.FileName == fileName)
			//        {
			//            zipEntry.Extract(pathTarget, ExtractExistingFileAction.OverwriteSilently);
			//            return new FileInfo(Path.Combine(pathTarget, fileName));
			//        }
			//    }
			//}

			var elapsed = GimliTrace.Stop("Extract");
            Log.Debug($"PackageManager|Extract ({pathSource}) ({pathTarget}) ({fileName}): Stop ({elapsed}ms)");

            return null;
        }

        public static void CreateDirectory(string path)
        {
            Log.Debug($"PackageManager|CreateDirectory ({path})");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static void PrepareDirectory(string path)
        {
            Log.Debug($"PackageManager|PrepareDirectory ({path})");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                return;
            }

            DeleteDirectory(path);
        }

        public static void DeleteDirectory(string path, bool deleteRoot = false)
        {
            Log.Debug($"PackageManager|DeleteDirectory ({path}) ({deleteRoot})");

            DirectoryInfo directoryInfo = new DirectoryInfo(path);

            foreach (FileInfo fileInfo in directoryInfo.GetFiles())
            {
                fileInfo.Delete();
            }
            foreach (DirectoryInfo directoryInfoSub in directoryInfo.GetDirectories())
            {
                directoryInfoSub.Delete(true);
            }

            if (deleteRoot)
            {
                directoryInfo.Delete(true);
            }
        }

        private static string GetRandomDirectoryName(int size = 10)
        {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < size; i++)
            {
                var ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26*Random.NextDouble() + 65)));
                stringBuilder.Append(ch);
            }

            return stringBuilder.ToString();
        }

        private static string GenerateChecksum(string path, string[] excludedFiles = null)
        {
            GimliTrace.Start("GenerateChecksum");
            Log.Debug("PackageManager|GenerateChecksum: Start");

            var files = (Directory.GetFiles(path, "*.*", SearchOption.AllDirectories).OrderBy(p => p).ToList());
            if (excludedFiles != null)
                files.RemoveAll(x => excludedFiles.ToList().Select(c => { c = Path.Combine(path, c); return c; }).Contains(x));

            MD5 md5 = MD5.Create();

            for (int i = 0; i < files.Count; i++)
            {
                string file = files[i];

                string relativePath = file.Substring(path.Length + (path[path.Length - 1] == '\\' ? 0 : 1));
                byte[] pathBytes = Encoding.UTF8.GetBytes(relativePath.ToLower());
                md5.TransformBlock(pathBytes, 0, pathBytes.Length, pathBytes, 0);

                byte[] contentBytes = File.ReadAllBytes(file);
                if (i == files.Count - 1)
                    md5.TransformFinalBlock(contentBytes, 0, contentBytes.Length);
                else
                    md5.TransformBlock(contentBytes, 0, contentBytes.Length, contentBytes, 0);
            }

            var elapsed = GimliTrace.Stop("GenerateChecksum");
            Log.Debug($"PackageManager|GenerateChecksum: Stop ({elapsed}ms)");

            return BitConverter.ToString(md5.Hash).Replace("-", "").ToLower();
        }
    }
}
