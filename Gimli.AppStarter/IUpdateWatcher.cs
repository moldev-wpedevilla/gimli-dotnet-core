﻿namespace Gimli.AppStarter
{
    public interface IUpdateWatcher
    {
        void ProcessUpdate();
    }
}