﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Gimli.AppStarter;
using Gimli.AppStarter.Managed;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.AppStarter;
using System;
using System.IO;
using System.Reflection;

namespace Gimli.AppStarter
{
	public class MainWindow : Window
	{
        private MainViewModel mViewModel;
		public MainWindow()
		{
			InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}


		public MainViewModel ViewModel
		{
			get
			{
				return mViewModel;
			}
			set
			{
				mViewModel = value;
				DataContext = value;
			}
		}

		private void ShowDialog_Checked(object sender, RoutedEventArgs e)
		{
			try
			{
				// TODO someone: do something to reenable this feature
				//BringIntoView();
				Show();
				Activate();
			}
			catch (Exception ex)
			{

			}

			Topmost = true;
			WindowState = WindowState.Maximized;
		}

		private void ShowDialog_Unchecked(object sender, RoutedEventArgs e)
		{
			Topmost = false;
			Hide();
			//WindowState = WindowState.Minimized;
		}

		private void ButtonClose_Click(object sender, RoutedEventArgs e)
		{
			AppManager.Instance.Shutdown();
		}
	}
}
