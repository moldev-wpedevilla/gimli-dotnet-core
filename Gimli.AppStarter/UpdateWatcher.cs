﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using Gimli.AppStarter.Actors;
using Gimli.JsonObjects.DummyData;
using NLog;
using Gimli.AppStarter.Managed;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using MolDev.Common.Akka.Interfaces;
using Gimli.JsonObjects;

namespace Gimli.AppStarter
{
    public class UpdateWatcher : IUpdateWatcher
    {
        public Logger Log { get; set; } = LogManager.GetCurrentClassLogger();

        private UpdateChecker mUpdateChecker;
        private readonly IActorService mActorService;

        public UpdateWatcher(IActorService actorService)
        {
            Log.Debug($"UpdateWatcher|Constructor ({actorService != null})");

            mActorService = actorService;
        }

        private static void ConfirmUpdate(string version = "")
        {
            WaitForUpdatePossible();

            WatchdogUpdateActor.Instance.SendUpdateRequest(version);
        }

        private static void WaitForUpdatePossible()
        {
            while (!WatchdogShutdownActor.Instance.IsShutdownAllowed())
            {
                Thread.Sleep(int.Parse(AppConfigurationManager.GetSettingsValue("UpdateRequestIntervalInMs")));
            }
        }

        public void ProcessUpdate()
        {
            GimliTrace.Start("ProcessUpdate");
            Log.Debug("UpdateWatcher|ProcessUpdate: Start");

            if (mUpdateChecker == null)
                mUpdateChecker = new UpdateChecker(false);

            using (var spinnerController = new SpinnerController(() => mActorService.Get<WebServiceProxyActor>(), Properties.Resources.ResourceManager.GetString("Update_Preparing")))
            {
                spinnerController.DisplaySpinner(180000);

                var updateFile = mUpdateChecker.ExtractPending();
                var checksumInvalid = !mUpdateChecker.IsPendingChecksumValid(updateFile);

                if (checksumInvalid || updateFile == null)
                {
                    Log.Debug($"UpdateWatcher|ProcessUpdate (Error): {checksumInvalid}|{updateFile == null}");
                    spinnerController.HiddeSpinner();
                    WatchdogUpdateActor.Instance.SendUpdateInfo(WatchdogUpdateActor.SoftwareUpdateInfo.Corrupted);
                }
                else
                {
                    Log.Debug("UpdateWatcher|ProcessUpdate (Success)");

                    AppManager.Instance.SetStatus(AppManager.Status.Updating);
                    
                    var update = new Update.Update(updateFile);
                    AppManager.Instance.CurrentUpdate = update;
                    update.ProcessUpdate();
                }
            }

            var elapsed = GimliTrace.Stop("ProcessUpdate");
            Log.Debug($"UpdateWatcher|ProcessUpdate: Stop ({elapsed}ms)");
        }

        public void CheckDrives(List<string> driveLetters)
        {
            foreach (var driveLetter in driveLetters)
            {
                CheckDrive(driveLetter);
            }
        }

        private void CheckDrive(string driveLetter)
        {
            GimliTrace.Start("CheckDrive");
            Log.Debug($"UpdateWatcher|CheckDrive ({driveLetter}): Start");

            int relevantUpdateVersion = 0;
            FileInfo relevantUpdateFile = null;

            using (var spinnerController = new SpinnerController(() => mActorService.Get<WebServiceProxyActor>(), Properties.Resources.ResourceManager.GetString("Update_Checking")))
            {
                spinnerController.DisplaySpinner(15000);
                
                var directoryInfo = new DirectoryInfo(driveLetter);
                var updateFiles = GetFiles(directoryInfo, AppConfigurationManager.GetSettingsValue("UpdateFileType"));
                
                if (updateFiles.Length > 0)
                {
                    if (mUpdateChecker == null)
                        mUpdateChecker = new UpdateChecker();

                    var systemVersion = UpdateChecker.GetVersionFromSystem();
                    var updateVersion = 0;

                    foreach (var updateFile in updateFiles)
                    {
                        var actUpdateVersion = mUpdateChecker.GetVersionFromUpdate(updateFile);

                        if (actUpdateVersion > systemVersion && actUpdateVersion > updateVersion)
                        {
                            updateVersion = actUpdateVersion;
                            relevantUpdateVersion = actUpdateVersion;
                            relevantUpdateFile = updateFile;
                        }
                    }
                }

                if (relevantUpdateFile != null)
                {
                    PackageManager.PackageManager.CopyFile(relevantUpdateFile, UpdateChecker.DirectoryUpdatePending);
                }
            
                spinnerController.HiddeSpinner();
            }

            if (relevantUpdateFile != null)
            {
                ConfirmUpdate(relevantUpdateVersion.ToString());
            }
            
            var elapsed = GimliTrace.Stop("CheckDrive");
            Log.Debug($"UpdateWatcher|CheckDrive: Stop ({elapsed}ms)");
        }

        private FileInfo[] GetFiles(DirectoryInfo directoryInfo, string filter)
        {
            GimliTrace.Start("GetFiles");
            Log.Debug($"UpdateWatcher|GetFiles ({directoryInfo}): Start");

            IList<string> files = new List<string>();
            AddFiles(directoryInfo.FullName, files);
            IList<FileInfo> fileInfos = new List<FileInfo>();
            foreach (var file in files)
            {
                if (file.Substring(file.Length - 5, 5) == filter)
                {
                    fileInfos.Add(new FileInfo(file));
                }
            }

            var elapsed = GimliTrace.Stop("GetFiles");
            Log.Debug($"UpdateWatcher|GetFiles ({directoryInfo}): Stop ({elapsed}ms)");

            return fileInfos.ToArray();
        }

        private static void AddFiles(string path, IList<string> files)
        {
            try
            {
                Directory.GetFiles(path).ToList().ForEach(files.Add);
                Directory.GetDirectories(path).ToList().ForEach(s => AddFiles(s, files));
            }
            catch (UnauthorizedAccessException)
            {
                
            }
        }
    }
}
