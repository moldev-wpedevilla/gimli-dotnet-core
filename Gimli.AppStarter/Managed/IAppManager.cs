using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.AppStarter;
using Gimli.Shared.Actors.Messages;
using NLog;

namespace Gimli.AppStarter.Managed
{
    public interface IAppManager
    {
        GimliActorHost ActorHost { get; }
        Logger Log { get; set; }
        UpdateWatcher UpdateWatcher { get; set; }
        Update.Update CurrentUpdate { get; set; }
        void Init(StartupConfigurationHandler startupConfigurationHandler, MainViewModel mainViewModel, bool restart = false);
        void AddManaged(StartupElement startupElement, bool frontendManual = false);
        void StartFrontend();
        void StopFrontend();
        void RaiseInitLevel(string sender, InitLevel initLevel);
        void StopAll();
        void Shutdown();
        void Update();
        void Restart();
        void SetStatus(AppManager.Status status);
        void SetUpdateStatus(FirmwareUpdateStatus.Status status, int percentage = 0, string text = "");
        void RestartApplication();
        void ShutdownSystem();
    }
}