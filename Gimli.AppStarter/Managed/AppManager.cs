﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Castle.Core.Internal;
using Gimli.AppStarter.Actors;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.AppStarter;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using NLog;

namespace Gimli.AppStarter.Managed
{
    public class AppManager : IAppManager
    {
		// TODO someone: do something to reenable this feature
		[DllImport("USER32.DLL", CharSet = CharSet.Unicode)]
        public static extern IntPtr FindWindow(String lpClassName, String lpWindowName);

		// TODO someone: do something to reenable this feature
		[DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        private ManagedProgram mManagedProgramFrontend;

        public enum Status
        {
            Startup,
            Running,
            Shutdown,
            Restoring,
            Updating,
            UpdatingFirmware,
            Restarting
        };

        public GimliActorHost ActorHost { get; private set; }

        private StartupConfigurationHandler mStartupConfigurationHandler;

        private MainViewModel mMainViewModel;
        public List<ManagedProgram> ManagedPrograms { get; private set; }

        public Logger Log { get; set; } = LogManager.GetCurrentClassLogger();

        public static AppManager Instance { get; } = new AppManager();
        
        public UpdateWatcher UpdateWatcher { get; set; }

        public Update.Update CurrentUpdate { get; set; }

        private StartupElement mStartupElementFrontend;

        public bool RestartFrontendMode = false;
        public bool FrontendStarted;
        private AppManager() { }

        private List<InitHandle> mInitHandle = new List<InitHandle>();

        private int mCountInitAs;
        
        internal class InitHandle
        {
            public StartupElement StartupElement { get; set; }
            public InitLevel InitLevel { get; set; }
            public InitFinishedState InitFinishedState { get; set; }
            
            public InitHandle(StartupElement startupElement)
            {
                StartupElement = startupElement;
                InitLevel = InitLevel.L0_Starting;
            }
        }

        public void Init(StartupConfigurationHandler startupConfigurationHandler, MainViewModel mainViewModel, bool restart = false)
        {
            Log.Info($"AppStarter starting for software version {UpdateChecker.GetVersionFromSystem()}");

            if (mainViewModel == null)
            {
                throw new ArgumentNullException(nameof(mainViewModel));
            }
            if (startupConfigurationHandler == null)
            {
                throw new ArgumentException("Argument invalid", "StartupConfigurationHandler");
            }

            InitActorHost();

            mCountInitAs = 0;
            mInitHandle = new List<InitHandle>();
            ManagedPrograms = new List<ManagedProgram>();
            
            if (restart)
            {
                Log.Info("AppStarter Restart");

                SetStatus(Status.Restarting);
            }
            else
            {
                Log.Info("AppStarter Startup");

                mMainViewModel = mainViewModel;
                SetButtonCloseHidden(!bool.Parse(AppConfigurationManager.GetSettingsValue("SplashscreenButtonClose")));

                mStartupConfigurationHandler = startupConfigurationHandler;

                SetStatus(Status.Startup);

                PreStart();
            }

            foreach (StartupElement startupConfiguration in startupConfigurationHandler.Startups)
            {
                if (startupConfiguration.InitAS)
                { 
                    mCountInitAs++;
                }
            }


            foreach (StartupElement startupConfiguration in startupConfigurationHandler.Startups)
            {
                Instance.AddManaged(startupConfiguration);
            }
        }

        public void AddManaged(StartupElement startupElement, bool frontendManual = false)
        {
            if (startupElement.StartupDelay != 0)
            {
                Thread.Sleep(TimeSpan.FromSeconds(startupElement.StartupDelay));
            }

            if (startupElement.Name == "Frontend" && !frontendManual)
            {
                mStartupElementFrontend = startupElement;
                return;
            }

            Log.Info("AddManaged: " + startupElement.Name + (startupElement.Program.IsNullOrEmpty() ? "" : " (" + startupElement.Program + ")") + (startupElement.RequiresWatch ? " - Watched" : "") + (startupElement.InitAS ? " - InitAS" : ""));

            if (startupElement.InitAS)
            {
                AddInitHandler(startupElement);
            }

            mManagedProgramFrontend = new ManagedProgram(startupElement);

            ManagedPrograms.Add(mManagedProgramFrontend);
        }

        public void StartFrontend()
        {
            if (!FrontendStarted)
            {
                AddManaged(mStartupElementFrontend, true);
                FrontendStarted = true;
                //SetStatus(Status.Running);
            }
        }

        public void CheckUpdateSuccess()
        {
            if (!File.Exists(AppConfigurationManager.GetSettingsValue("UpdateInfoFile")))
            {
                return;
            }

            var updateResult = File.ReadAllText(AppConfigurationManager.GetSettingsValue("UpdateInfoFile")).Split('|');

            if (updateResult.Length == 3 && updateResult[0] == "UPDATE")
            {
                WatchdogUpdateActor.Instance.UpdateVersion = updateResult[1];

                if (updateResult[2] == "FAIL")
                {
                    WatchdogUpdateActor.Instance.UpdateSuccess = false;
                }
                else if (updateResult[2] == "OK")
                {
                    WatchdogUpdateActor.Instance.UpdateSuccess = true;
                    CleanupOldInstallations(updateResult[1]);
                }
            }

            File.Delete(AppConfigurationManager.GetSettingsValue("UpdateInfoFile"));
        }

        public void CleanupOldInstallations(string version)
        {
            Log.Info("AppManager|CleanupOldInstallations: Start");

            string[] files;
            string[] installations;
            
            try
            {
                files = Directory.GetFiles(AppConfigurationManager.GetSettingsValue("ProgramBaseDirectory"), "*.*", SearchOption.TopDirectoryOnly).ToArray();
                installations = Directory.GetDirectories(AppConfigurationManager.GetSettingsValue("ProgramBaseDirectory"), "*.*", SearchOption.TopDirectoryOnly).Where(i => i != Path.Combine(AppConfigurationManager.GetSettingsValue("ProgramBaseDirectory"), version)).ToArray();
            }
            catch (Exception e)
            {
                Log.Info($"AppManager|CleanupOldInstallations|Exception ({e.Message})");
                return;
            }
            
            files.Where(i => File.Exists(i)).ToList().ForEach(i => { try { File.Delete(i); } catch(Exception e) { Log.Info($"AppManager|CleanupOldInstallations|Exception (File {i}) e.Message"); } });
            installations.Where(i => Directory.Exists(i)).ToList().ForEach(i => PackageManager.PackageManager.DeleteDirectory(i, true));
            
            if (Directory.Exists(AppConfigurationManager.GetSettingsValue("GimliUpdateLocation_11")))
            {
                PackageManager.PackageManager.DeleteDirectory(AppConfigurationManager.GetSettingsValue("GimliUpdateLocation_11"), true);
            }
        }

        private void AddInitHandler(StartupElement startupElement)
        {
            mInitHandle.Add(new InitHandle(startupElement));
        }

        public void RaiseInitLevel(string sender, InitLevel initLevel)
        {
            Log.Info("Sender: " + sender + " reached: " + initLevel);

            var handle = mInitHandle.Find(x => x.StartupElement.ActorClass == sender);

            if (handle != null)
            {
                handle.InitLevel = initLevel;
                CheckInitLevels(handle, initLevel);
            }
        }

        private void CheckInitLevels(InitHandle changedHandle, InitLevel initLevel)
        {
            if (mInitHandle.Count < mCountInitAs)
            {
                return;
            }

            InitLevel newLevel = InitLevel.L0_Starting;

            switch (initLevel)
            {
                case InitLevel.L0_Starting:
                    newLevel = InitLevel.L1_SystemStarted;
                    break;
                case InitLevel.L1_SystemStarted:
                    newLevel = InitLevel.L2_ProxysLive;
                    break;
                case InitLevel.L2_ProxysLive:
                    newLevel = InitLevel.L3_Operational;
                    break;
            }

            bool allLevels = true;

            List<string> proxies = new List<string>();

            foreach (var handle in mInitHandle)
            {
                proxies.Add(handle.StartupElement.ActorClass.Replace("Routing", "Proxy"));

                if (handle.InitLevel != initLevel)
                {
                    allLevels = false;
                }
            }

            if (allLevels && mInitHandle[0].InitLevel != InitLevel.L3_Operational)
            {
                Log.Info("All services reached level: " + initLevel + " publishing new level: " + newLevel);

	            if (newLevel == InitLevel.L2_ProxysLive)
	            {
		            try
		            {
						var actorService = ActorHost.Container.Resolve<IActorService>();
						var task = actorService.Get<AppManagerRoutingActor>()
							.Ask<GimliResponse>(
								MessageExtensions.CreateRequestMessage(new InitSystemMessage
								{
									TargetLevels = new[] { InitLevel.L2_ProxysLive }
								}));
						task.Wait(30000);
					}
		            catch (Exception e)
		            {
			            Log.Error("Error communicating with services: "+ e);
		            }
	            }

                //SetStatus(Status.Running);
                WatchdogShutdownActor.Instance.PublishInitLevel(proxies, newLevel);
                return;
            }

	        if (allLevels && mInitHandle[0].InitLevel == InitLevel.L3_Operational)
	        {
				Log.Info("----------------------> starting frontend");
				StartFrontend();
	            CheckUpdateSuccess();
		        return;
	        }

            bool otherLevels = true;
            
            foreach (var handle in mInitHandle.Where(x => x.StartupElement.ActorClass != changedHandle.StartupElement.ActorClass))
            {
                switch (initLevel)
                {
                    case (InitLevel.L1_SystemStarted):
                        if (handle.InitLevel == InitLevel.L0_Starting || handle.InitLevel == InitLevel.L1_SystemStarted)
                        {
                            otherLevels = false;
                        }
                        break;
                    case (InitLevel.L2_ProxysLive):
                        if (handle.InitLevel == InitLevel.L0_Starting || handle.InitLevel == InitLevel.L1_SystemStarted || handle.InitLevel == InitLevel.L2_ProxysLive)
                        {
                            otherLevels = false;
                        }
                        break;
                }
            }

            if (otherLevels)
            {
                proxies.Clear();
                proxies.Add(changedHandle.StartupElement.ActorClass.Replace("Routing", "Proxy"));
                WatchdogShutdownActor.Instance.PublishInitLevel(proxies, newLevel);
            }
        }

        public void StopAll()
        {
            StopAllManagedPrograms();
        }

        private void StopAllManagedPrograms()
        {
            mManagedProgramFrontend?.Stop();

            foreach (var managedProgram in ManagedPrograms)
            {
                managedProgram?.Stop();
            }
            ManagedPrograms.Clear();
        }

        public void Shutdown()
        {
            SetStatus(Status.Shutdown);
            StopAll();

            ShutdownSystem();
        }

        public void Update()
        {
            SetStatus(Status.Updating);
            StopAll();

            Environment.Exit(0);
        }

        public void Restart()
        {
            SetStatus(Status.Updating);
            StopAll();

            var processStartInfo = new ProcessStartInfo("shutdown.exe", "/r /t 0")
            {
                CreateNoWindow = true,
                UseShellExecute = false
            };

            Process.Start(processStartInfo);
        }

        public void SetStatus(Status status)
        {
            Log.Info($"AppManager|SetStatus {((Status)status).ToString()}");

            switch (status)
            {
                case Status.Startup:
                    SetProgressBarStartupHidden(false);
                    SetProgressBarUpdateHidden(true);
                    SetSplashscreenStatusText(Properties.Resources.Splashscreen_Status_Text_Startup);
                    ShowSplashscreen();
                    break;
                case Status.Running:
                    SetSplashscreenStatusText(Properties.Resources.Splashscreen_Status_Text_Running);
                    SetProgressBarStartupHidden(true);
                    SetProgressBarUpdateHidden(true);
                    BringChromeWindowToFront();
                    HideSplashscreen();
                    BringChromeWindowToFront();
                    break;
                case Status.Shutdown:
                    SetSplashscreenStatusText(Properties.Resources.Splashscreen_Status_Text_Shutdown);
                    ShowSplashscreen();
                    break;
                case Status.Restoring:
                    SetSplashscreenStatusText(Properties.Resources.Splashscreen_Status_Text_Restoring);
                    ShowSplashscreen();
                    break;
                case Status.Updating:
                    SetProgressBarStartupHidden(false);
                    SetProgressBarUpdateHidden(true);
                    SetSplashscreenStatusText(Properties.Resources.Splashscreen_Status_Text_Updating);
                    ShowSplashscreen();
                    break;
                case Status.UpdatingFirmware:
                    SetProgressBarStartupHidden(true);
                    SetProgressBarUpdateHidden(false);
                    SetSplashscreenStatusText(Properties.Resources.Splashscreen_Status_Text_Updating);
                    ShowSplashscreen();
                    break;
                case Status.Restarting:
                    SetProgressBarStartupHidden(false);
                    SetProgressBarUpdateHidden(true);
                    SetSplashscreenStatusText(Properties.Resources.Splashscreen_Status_Text_Restarting);
                    ShowSplashscreen();
                    break;
            }
        }

        public void SetUpdateStatus(FirmwareUpdateStatus.Status status, int percentage = 0, string text = "")
        {
            Log.Info("SetUpdateStatus: " + status + " - %: " + percentage + " - Text: " + text + " - MvM avail: " +
                     (mMainViewModel != null));

            switch (status)
            {
                case FirmwareUpdateStatus.Status.Ok:
                    if (percentage > 0)
                    {
                        if (mMainViewModel != null)
                        {
                            mMainViewModel.CurrentUpdateProgress = percentage;
                        }
                    }

                    SetSplashscreenStatusText(text.IsNullOrEmpty()
                        ? string.Format(Properties.Resources.Splashscreen_Status_Text_Updating_TextPer, percentage)
                        : string.Format(Properties.Resources.Splashscreen_Status_Text_Updating_Text, text));
                    break;
                case FirmwareUpdateStatus.Status.Done:
                    CurrentUpdate.UpdateGeneral(true, true);
                    break;
                case FirmwareUpdateStatus.Status.Error:
                    CurrentUpdate.UpdateGeneral(true, false, text);
                    break;
            }
        }
        
        private void SetProgressBarStartupHidden(bool hidden)
        {
            mMainViewModel.ProgressBarStartupVisibility = !hidden;
        }

        private void SetProgressBarUpdateHidden(bool hidden)
        {
            mMainViewModel.ProgressBarUpdateVisibility = !hidden;
        }

        private void SetButtonCloseHidden(bool hidden)
        {
            mMainViewModel.ButtonCloseVisibility = !hidden;
        }

        private void SetSplashscreenStatusText(string splashscreenStatusText)
        {
            if (mMainViewModel == null)
            {
                return;
            }

            mMainViewModel.LabelSplashscreenStatusText = splashscreenStatusText;
        }

        private void ShowSplashscreen()
        {
            if (Convert.ToBoolean(AppConfigurationManager.GetSettingsValue("PersistentSplashscreen")))
            {
                mMainViewModel.CheckBoxShowHelper = true;
                BringAppStarterWindowToFront();
            }
            else
            {
                mMainViewModel.CheckBoxShowHelper = false;
            }
        }

        private void HideSplashscreen()
        {
            mMainViewModel.CheckBoxShowHelper = false;
        }

        private static void BringChromeWindowToFront()
        {
            var handle = FindWindow(null, "SpectraMax iD - Chromium");

            if (handle != IntPtr.Zero)
            {
                SetForegroundWindow(handle);
            }
        }

        private void BringAppStarterWindowToFront()
        {
            var handle = FindWindow(null, "Gimli.AppStarter");

            if (handle == IntPtr.Zero)
            {
                return;
            }

            SetForegroundWindow(handle);
        }

        public void RestartApplication()
        {
            SetStatus(Status.Restarting);

            FrontendStarted = false;
			//var actorService = ActorHost.Container.Resolve<IActorService>();
			//actorService.Get<InstrumentServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new RestartServicesMessage()));

			StopAllManagedPrograms();

            Task.Run(() => {
                ActorHost.Stop();
                Init(mStartupConfigurationHandler, mMainViewModel, true);
            });
        }

        public void ShutdownSystem()
        {
            var shutdownSystem = bool.Parse(AppConfigurationManager.GetSettingsValue("ShutdownSystem"));

            if (shutdownSystem)
            {
                var processStartInfo = new ProcessStartInfo("shutdown", "/s /f /t 0")
                {
                    CreateNoWindow = true,
                    UseShellExecute = false
                };

                Process.Start(processStartInfo);
            }
            else
            {
                Environment.Exit(0);
            }
        }

        public void StopFrontend()
        {
            mManagedProgramFrontend.Stop();
        }

        private void InitActorHost()
        {
			var thisAssemblyName = Assembly.GetEntryAssembly().GetName().Name;
			var configFilePath = thisAssemblyName + ".dll.config";

			ActorHost = new GimliActorHost(container =>
            {
                container.Register(Castle.MicroKernel.Registration.Component
                    .For<IActorsystemOverallState>()
                    .Instance(new ActorsystemOverallState { ServiceId = ActorServiceKey.AppManager })
                    .LifestyleSingleton());
                container.Register(Castle.MicroKernel.Registration.Component.For<Func<GimliActorHost>>().Instance(() => null));
            }, configFilePath);

            ActorHost.Start(RemoteActorConfiguration.Instance.ServiceConfigurations[ActorServiceKey.AppManager], false);
        }

        private static void PreStart()
        {
			/*var startupDelayMilliseconds = int.Parse(AppConfigurationManager.GetSettingsValue("StartupDelaySeconds")) * 1000;

            var task = Task.Delay(startupDelayMilliseconds);
            task.Wait();*/
		}
	}
}
