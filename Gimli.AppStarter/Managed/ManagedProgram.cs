﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Gimli.JsonObjects;
//using System.Windows.Automation;
using Gimli.JsonObjects.DummyData;
using Gimli.Shared.Actors.AppStarter;
using NLog;

namespace Gimli.AppStarter.Managed
{
    public class ManagedProgram
    {
        private readonly int mWatchdogTimerMilliseconds;
        private readonly int mWatchdogInitialDelayMilliseconds;
        private readonly int mProcessShutdownTimeAllowedMilliseconds;

        public StartupElement StartupElement { get; }

        private readonly Logger mLog;
        
        private CancellationTokenSource mCancellationTokenSource;
        
        internal Process Process;

        private bool mIsWatching;
        
        public ManagedProgram(StartupElement startupElement, bool startImmediately = true)
        {
            mWatchdogTimerMilliseconds = int.Parse(AppConfigurationManager.GetSettingsValue("WatchdogTimerMilliseconds"));
            mWatchdogInitialDelayMilliseconds = int.Parse(AppConfigurationManager.GetSettingsValue("WatchdogInitialDelayMilliseconds"));
            mProcessShutdownTimeAllowedMilliseconds = int.Parse(AppConfigurationManager.GetSettingsValue("ProcessShutdownTimeAllowedMilliseconds"));

            StartupElement = startupElement;
            mLog = AppManager.Instance.Log;

            if (startImmediately)
            {
                Start();
            }
        }

        public void Start()
        {
            if (StartupElement.Name == "Frontend")
            {
                StartFrontend();
            }
            else
            {
                StartProgram();
            }
        }

        public void Stop()
        {
            if (StartupElement.Name == "Frontend")
            {
                StopFrontend();
            }
            else
            {
                StopProgram();
            }
        }

        private void StartProgram()
        {
            if (StartupElement.GetStartInfo != null)
            {
                StartProcess();

                if (StartupElement.Watch)
                {
                    StartWatch();
                }
            }
        }

        private void StopProgram()
        {
            if (Process != null && !Process.HasExited)
            {
                if (StartupElement.Watch)
                {
                    StopWatch();
                }

                StopProcess();
            }
        }

        private void StartFrontend()
        {
            var frontendDelayMilliseconds = int.Parse(AppConfigurationManager.GetSettingsValue("FrontendDelaySeconds")) * 1000;

            string swVersion = null;
            var client = new WebClient();
            client.DownloadStringCompleted += (sender, args) =>
            {
                if (!args.Cancelled && args.Error == null)
                {
                    swVersion = args.Result;
                }
            };

            while (swVersion == null)
            {
                if (!client.IsBusy)
                {
                    client.DownloadStringAsync(new Uri("http://localhost:8002/reader/swversion&rnd=seconds_sine_epoch"));
                }
                if (swVersion == null || client.IsBusy)
                {
                    Thread.Sleep(1000);
                }
            }

            var task = Task.Delay(frontendDelayMilliseconds);
            task.Wait();
            
            try
            {
                Process = Process.Start(StartupElement.GetStartInfo);
            }
            catch (Win32Exception)
            {

            }
        }

        private static void StopFrontend()
        {
            var shutdownBrowser = bool.Parse(AppConfigurationManager.GetSettingsValue("ShutdownBrowser"));

            if (!shutdownBrowser)
            {
                return;
            }

            foreach (var process in Process.GetProcessesByName("chrome"))
            {
                if (process.MainWindowHandle == IntPtr.Zero || process.MainWindowTitle != "SpectraMax iD - Chromium")
                {
                    continue;
                }

				// TODO someone: do something to reenable this feature
                //AutomationElement element = AutomationElement.FromHandle(process.MainWindowHandle);
                //((WindowPattern) element?.GetCurrentPattern(WindowPattern.Pattern))?.Close();
            }
        }

        private async Task StartWatch()
        {
            mLog.Info($"Start Watch - {StartupElement.Program} / {Process.Id}");

            mIsWatching = true;
            mCancellationTokenSource = new CancellationTokenSource();

            await Task.Delay(mWatchdogInitialDelayMilliseconds);

            while (mIsWatching && !mCancellationTokenSource.IsCancellationRequested)
            {
                if (!CheckProcess())
                {
                    StartProcess(true);
                    Thread.Sleep(mWatchdogInitialDelayMilliseconds);
                }

                await Task.Delay(mWatchdogTimerMilliseconds, mCancellationTokenSource.Token).ContinueWith(task => { });
            }
        }

        private void StopWatch()
        {
            mLog.Info($"StopWatch: {StartupElement.Program}");

            mIsWatching = false;
            mCancellationTokenSource.Cancel();
        }

        private void StartProcess(bool restart = false)
        {
            if (restart)
            {
                AppManager.Instance?.SetStatus(AppManager.Status.Restoring);
            }

            Process = Process.Start(StartupElement.GetStartInfo);

            if (restart)
            {
                AppManager.Instance?.SetStatus(AppManager.Status.Running);
            }

            mLog.Info("StartProcess: " + StartupElement.Program + " Id: " + Process?.Id + (restart ? " (Restart)" : ""));
        }

        private void StopProcess()
        {
            Process?.CloseMainWindow();
            Process?.WaitForExit(mProcessShutdownTimeAllowedMilliseconds);
            if (Process != null && !Process.HasExited)
            {
                GimliTrace.Start("StopProcess");

                try
                {
                    Process?.Kill();
                    Process?.WaitForExit();
                    
                    var elapsed = GimliTrace.Stop("StopProcess");
                    mLog.Info($"StopProcess: {StartupElement.Program} Id: {Process?.Id} (Kill) Time: {elapsed} ms");
                }
                catch (Exception e)
                {
                    var elapsed = GimliTrace.Stop("StopProcess");
                    mLog.Info($"StopProcess: {StartupElement.Program} Id: {Process?.Id} (Kill) Exception: {e.Message} Time: {elapsed} ms");
                }
            }
            else
            {
                var elapsed = GimliTrace.Stop("StopProcess");
                mLog.Info($"StopProcess: {StartupElement.Program} Id: {Process?.Id} (Clean) Time: {elapsed} ms");
            }
        }

        private bool CheckProcess()
        {
            Process.Refresh();
            return IsRunning();
        }

        public bool IsRunning()
        {
            if (Process == null)
            {
                mLog.Info($"IsRunning: false - {StartupElement.Program}");
                return false;
            }
            try
            {
                Process.GetProcessById(Process.Id);
            }
            catch (ArgumentException)
            {
                mLog.Info($"IsRunning: false - {StartupElement.Program} / {Process.Id}");
                return false;
            }
            return true;
        }
    }
}
