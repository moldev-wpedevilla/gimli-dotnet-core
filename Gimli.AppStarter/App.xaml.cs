﻿using Avalonia;
using Avalonia.Markup.Xaml;
using Gimli.AppStarter.Managed;
using Gimli.CoreService.OSController.GraphicsManager;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.AppStarter;
using System;
using System.IO;
using System.Reflection;
using System.Threading;

namespace Gimli.AppStarter
{
	public class App : Application
	{
		private Program mProgram;
		private Thread mWorkerThread;
		internal static string config;

		public override void Initialize()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
