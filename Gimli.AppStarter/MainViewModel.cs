﻿using System.ComponentModel;
using System.Windows;
using Gimli.AppStarter.Properties;

namespace Gimli.AppStarter
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private bool mCheckBoxShowHelper;
        private string mSplashscreenStatusText = Resources.ResourceManager.GetString("Splashscreen_Status_Text_Startup");
        private double mCurrentStartupProgress;
        private int mCurrentUpdateProgress;
        private bool mProgressBarStartupVisibility;
        private bool mProgressBarUpdateVisibility;
        private bool mButtonCloseVisibility;

        public bool CheckBoxShowHelper
        {
            get { return mCheckBoxShowHelper; }
            set
            {
                mCheckBoxShowHelper = value;
                OnPropertyChanged("CheckBoxShowHelper");
            }
        }

        public string LabelSplashscreenStatusText
        {
            get
            {
                return mSplashscreenStatusText;
            }
            set
            {
                mSplashscreenStatusText = value;
                OnPropertyChanged("LabelSplashscreenStatusText");
            }
        }

        public double CurrentStartupProgress
        {
            get
            {
                return mCurrentStartupProgress;
            }
            set
            {
                mCurrentStartupProgress = value;
                OnPropertyChanged("CurrentStartupProgress");
            }
        }

        public int CurrentUpdateProgress
        {
            get
            {
                return mCurrentUpdateProgress;
            }
            set
            {
                mCurrentUpdateProgress = value;
                OnPropertyChanged("CurrentUpdateProgress");
            }
        }

        public bool ButtonCloseVisibility
        {
            get
            {
                return mButtonCloseVisibility;
                
            }
            set
            {
                mButtonCloseVisibility = value;
                OnPropertyChanged("ButtonCloseVisibility");
            }
        }

        public bool ProgressBarStartupVisibility
        {
            get
            {
                return mProgressBarStartupVisibility;
            }
            set
            {
                mProgressBarStartupVisibility = value;
                OnPropertyChanged("ProgressBarStartupVisibility");
            }
        }

        public bool ProgressBarUpdateVisibility
        {
            get
            {
                return mProgressBarUpdateVisibility;
            }
            set
            {
                mProgressBarUpdateVisibility = value;
                OnPropertyChanged("ProgressBarUpdateVisibility");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        internal void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
