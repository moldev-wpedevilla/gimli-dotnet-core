﻿using System;
using Gimli.AppStarter.Managed;
using Gimli.AppStarter.Properties;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using Gimli.Shared.Actors.Messages.Outbound;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.AppStarter.Actors
{
    public class WatchdogUpdateActor : GimliActorBase
    {
        private readonly IAppManager mAppManager;
        private readonly IActorService mActorService;

        public static WatchdogUpdateActor Instance { get; private set; }
        private readonly UpdateWatcher mUpdateWatcher;

        private bool mShowUpdate = true;
        public bool? UpdateSuccess = null;
        public string UpdateVersion = null;

        public WatchdogUpdateActor(IActorService actorService, IActorsystemOverallState systemState, IAppManager appManager = null) : base(actorService, systemState)
        {
            if (actorService == null) throw new ArgumentNullException(nameof(actorService));

            mActorService = actorService;
            Instance = this;
            
            mAppManager = appManager;

            mUpdateWatcher = new UpdateWatcher(mActorService);
        
            Receive<RequestMessage<ConfirmSoftwareUpdateMessage>>(message =>
            {
                Log.Debug("WatchdogUpdateActor|<RequestMessage<ConfirmSoftwareUpdateMessage>>");
                //(mAppManager ?? AppManager.Instance).SetStatus(AppManager.Status.Updating);
                (mAppManager ?? AppManager.Instance).UpdateWatcher.ProcessUpdate();
            });

            Receive<RequestMessage<UsbDrivePluggedInMessage>>(message =>
            {
                var task = mActorService.Get<CoreServiceProxyActor>().Ask<GetPluggedInUsbDrivesResponseMessage>(MessageExtensions.CreateRequestMessage(new GetPluggedInUsbDrivesMessage()));
                task.Wait();
                
                string letters = "";

                var relevantDriveLetters = message.Model.InitialCheck
                    ? task.Result.DriveLetters
                    : task.Result.LastDriveLetters;
                
                foreach (var s in relevantDriveLetters)
                {
                    letters += s + "/";
                }
                Log.Debug($"WatchdogUpdateActor|<UsbDrivePluggedInMessage ({letters})");

                mUpdateWatcher.CheckDrives(task.Result.LastDriveLetters);
            });

            Receive<RequestMessage<UsbDrivePluggedOutMessage>>(message =>
            {
            });

            Receive<RequestMessage<SetFirmwareUpdateStatusMessage>>(message =>
            {
                Log.Debug($"WatchdogUpdateActor|<RequestMessage<SetFirmwareUpdateStatusMessage>> (mAppManager init: {mAppManager != null})");
                Log.Debug($"  => Update Status: {message.Model.Status} - {message.Model.UpdateStatusPercentage}% - {message.Model.UpdateStatusText}");
                
                (mAppManager ?? AppManager.Instance).SetUpdateStatus(message.Model.Status, message.Model.UpdateStatusPercentage, message.Model.UpdateStatusText);
            });

            Receive<RequestMessage<FrontendHubConnectionMessage>>(message =>
            {
                Log.Debug("WatchdogUpdateActor|<RequestMessage<FrontendHubConnectionMessage>>");
                (mAppManager ?? AppManager.Instance).SetStatus(AppManager.Status.Running);

                if (mShowUpdate && UpdateSuccess != null)
                {
                    if (UpdateSuccess.Value)
                    {
                        SendUpdateInfo(SoftwareUpdateInfo.Success, UpdateVersion);
                    }
                    else
                    {
                        SendUpdateInfo(SoftwareUpdateInfo.Generic, UpdateVersion);
                    }
                }
                mShowUpdate = false;
            });
        }

        public void ShowSpinner(string text = "")
        {
            Log.Debug($"WatchdogUpdateActor|ShowSpinner ({text})");
            
            mActorService?.Get<WebServiceProxyActor>()?.Tell(MessageExtensions.CreateRequestMessage(new SpinnerMessage
            {
                Active = true,
                Text = text
            }));
        }

        public void HideSpinner()
        {
            Log.Debug("WatchdogUpdateActor|HideSpinner");
            
            mActorService?.Get<WebServiceProxyActor>()?.Tell(MessageExtensions.CreateRequestMessage(new SpinnerMessage
            {
                Active = false
            }));
        }

        public void SendUpdateRequest(string stringVersion = "")
        {
            var outboundSessionMessage = mActorService.Get<DataServiceProxyActor>()
                .Ask<GetOutboundSessionResponseMessage>(
                    MessageExtensions.CreateRequestMessage(new GetOutboundSessionMessage()));

            if (outboundSessionMessage.Result.Session.GxpMode)
            {
                Log.Info($"WatchdogShutdownActor|SendUpdateRequest: Update_Not_Possible_Gxp ({stringVersion})");

                mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
                {
                    Parameters = new MessageParameters
                    {
                        Message = string.Format(Resources.Update_Not_Possible_Gxp, stringVersion),
                        MessageType = MessageType.Info,
                        DisplayType = MessageDisplayType.DialogOk,
                        MessageCode = MessageStatusCodes.SoftwareUpdateNotPossibleGxpMessage
                    }
                }));
            }
            else if (outboundSessionMessage.Result.Session.IsClientConnected)
            {
                Log.Info($"WatchdogShutdownActor|SendUpdateRequest: Update_Not_Possible_Smp ({stringVersion})");

                mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
                {
                    Parameters = new MessageParameters
                    {
                        Message = string.Format(Resources.Update_Not_Possible_Smp, stringVersion),
                        MessageType = MessageType.Info,
                        DisplayType = MessageDisplayType.DialogOk,
                        MessageCode = MessageStatusCodes.SoftwareUpdateNotPossibleGxpMessage
                    }
                }));
            }
            else
            {
                Log.Info($"WatchdogShutdownActor|SendUpdateRequest: Update_Confirmation_Request ({stringVersion})");

                mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
                {
                    Parameters = new MessageParameters
                    {
                        Message = string.Format(Resources.Update_Confirmation_Request, stringVersion),
                        MessageType = MessageType.Info,
                        DisplayType = MessageDisplayType.DialogDecision,
                        MessageCode = MessageStatusCodes.SoftwareUpdateMessage
                    }
                }));
            }
        }

        public void SendUpdateInfo(SoftwareUpdateInfo info, string stringVersion = "")
        {
            var message = string.Empty;
            var type = MessageType.Error;
            var displayType = MessageDisplayType.DialogHelp;
            var code = MessageStatusCodes.SoftwareUpdateFailed;

            switch (info)
            {
                case SoftwareUpdateInfo.Checksum:
                    message = string.Format(Resources.Update_Error_Checksum, stringVersion);
                    break;
                case SoftwareUpdateInfo.Corrupted:
                    message = Resources.Update_Error_Corrupted;
                    code = MessageStatusCodes.SoftwareUpdateDamagedMessage;
                    break;
                case SoftwareUpdateInfo.Generic:
                    message = string.Format(Resources.Update_Error_Generic, stringVersion);
                    break;
                case SoftwareUpdateInfo.Success:
                    message = string.Format(Resources.Update_Success, stringVersion);
                    type = MessageType.Info;
                    displayType = MessageDisplayType.DialogOk;
                    code = MessageStatusCodes.SoftwareUpdateSuccess;
                    break;
            }

            Log.Info($"WatchdogShutdownActor|SendUpdateInfo: Info ({stringVersion})");

            mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
            {
                Parameters = new MessageParameters
                {
                    Message = message,
                    MessageType = type,
                    DisplayType = displayType,
                    MessageCode = code,
                    GoToTopic = "Update"
                }
            }));
        }

        public void UpdateError(string text)
        {
            string ftext = "";
            if (text != "")
            {
                ftext = "(" + text + ")";
            }

            Log.Info($"WatchdogShutdownActor|UpdateError: Update_Error ({ftext})");

            mActorService.Get<WebServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
            {
                Parameters = new MessageParameters
                {
                    Message = string.Format(Resources.Update_Error + ftext),
                    MessageType = MessageType.Error,
                    DisplayType = MessageDisplayType.DialogOk,
                    MessageCode = MessageStatusCodes.SoftwareUpdateMessage
                }
            }));
        }
        
        public void SendFirmwareUpdateMessage(string path)
        {
            Log.Debug("WatchdogShutdownActor|SendFirmwareUpdateMessage");

            mActorService.Get<InstrumentServiceProxyActor>()
                .Tell(MessageExtensions.CreateRequestMessage(new UpdateFirmwareMessage
                {
                    PathToUpdateDirectory = path
                }));
        }

        public enum SoftwareUpdateInfo
        {
            Checksum,
            Corrupted,
            Generic,
            Success
        }
    }
}
