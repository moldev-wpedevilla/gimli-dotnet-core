﻿using System;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.AppStarter.Actors
{
    class AppManagerRoutingActor : ToplevelRoutingActorBase
    {
        private const string WatchdogShutdownName = nameof(WatchdogShutdownActor);
        private const string WatchdogUpdateName = nameof(WatchdogUpdateActor);
	    private const string SystemInitActorName = nameof(SystemInitActor);
        
		public AppManagerRoutingActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
        {
            if (actorService == null) throw new ArgumentNullException(nameof(actorService));

            Receive<RequestMessage<UsbDrivePluggedInMessage>>(message =>
            {
                var childActor = GetChild<WatchdogUpdateActor>(WatchdogUpdateName);
                childActor.Forward(message);
            });

            Receive<RequestMessage<UsbDrivePluggedOutMessage>>(message =>
            {
                var childActor = GetChild<WatchdogUpdateActor>(WatchdogUpdateName);
                childActor.Forward(message);
            });

            Receive<RequestMessage<ConfirmSoftwareUpdateMessage>>(message =>
            {
                var childActor = GetChild<WatchdogUpdateActor>(WatchdogUpdateName);
                childActor.Forward(message);
            });

            Receive<RequestMessage<SetFirmwareUpdateStatusMessage>>(message =>
            {
                var childActor = GetChild<WatchdogUpdateActor>(WatchdogUpdateName);
                childActor.Forward(message);
            });

            Receive<RequestMessage<FrontendHubConnectionMessage>>(message =>
            {
                var childActor = GetChild<WatchdogUpdateActor>(WatchdogUpdateName);
                childActor.Forward(message);
            });

            Receive<RequestMessage<RestartServicesMessage>>(message =>
            {
                var childActor = GetChild<WatchdogShutdownActor>(WatchdogShutdownName);
                childActor.Forward(message);
            });

            Receive<RequestMessage<ServiceInitFinishedMessage>>(message =>
            {
                var childActor = GetChild<WatchdogShutdownActor>(WatchdogShutdownName);
                childActor.Forward(message);
            });

            Receive<RequestMessage<ShutdownMessage>>(message =>
            {
                var childActor = GetChild<WatchdogShutdownActor>(WatchdogShutdownName);
                childActor.Forward(message);
            });

            Receive<RequestMessage<StartUsbWatcherMessage>>(message =>
            {
                var childActor = GetChild<WatchdogShutdownActor>(WatchdogShutdownName);
                childActor.Forward(message);
            });
        }

	    protected override IActor GetSysInitActor()
	    {
			return GetChild<SystemInitActor>(SystemInitActorName);
		}

	    protected override void PreStart()
	    {
		    GetSysInitActor();
            GetChild<WatchdogShutdownActor>(WatchdogShutdownName);
            GetChild<WatchdogUpdateActor>(WatchdogUpdateName);

            base.PreStart();
        }
    }
}
