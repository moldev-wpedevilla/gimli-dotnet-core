﻿using System;
using System.Collections.Generic;
using Gimli.AppStarter.Managed;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using NLog;

namespace Gimli.AppStarter.Actors
{
    public class WatchdogShutdownActor : GimliActorBase
    {
        public Logger Log { get; set; } = LogManager.GetCurrentClassLogger();

        private readonly IActorService mActorService;

        public static WatchdogShutdownActor Instance { get; private set; }

        public WatchdogShutdownActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
        {
            if (actorService == null) throw new ArgumentNullException(nameof(actorService));

            mActorService = actorService;
            Instance = this;

            Receive<RequestMessage<ShutdownMessage>>(message =>
            {
                AppManager.Instance.Shutdown();
            });

            Receive<RequestMessage<RestartServicesMessage>>(message =>
            {
                AppManager.Instance.RestartApplication();
            });

            Receive<RequestMessage<ServiceInitFinishedMessage>>(message =>
            {
                Log.Debug($"WatchdogShutdownActor|<ServiceInitFinishedMessage>: level ({message.Model.Level})|state ({message.Model.State})|sender ({message.Model.Sender})");
                AppManager.Instance.RaiseInitLevel(message.Model.Sender, message.Model.Level);
            });
            
            Receive<RequestMessage<StartUsbWatcherMessage>>(message =>
            {
                Log.Debug("WatchdogShutdownActor|<StartUsbWatcherMessage>");
                AppManager.Instance.UpdateWatcher = new UpdateWatcher(mActorService);
            });
        }
        
        public bool IsShutdownAllowed()
        {
            bool a = IsShutdownAllowed<InstrumentServiceProxyActor>(InitLevel.L4_Initialized, true);
            bool b = IsShutdownAllowed<DataServiceProxyActor>(InitLevel.L3_Operational, true); 
            bool c = IsShutdownAllowed<CoreServiceProxyActor>(InitLevel.L3_Operational, true);
            bool d = IsShutdownAllowed<WebServiceProxyActor>(InitLevel.L3_Operational, true);

            Log.Debug($"WatchdogShutdownActor|IsShutdownAllowed: Core ({a})|Data ({b})|Instrument ({c})|WebService ({d})");
            
            return a && b && c && d;
        }

        public bool IsShutdownAllowed<T>(InitLevel targetInitLevel, bool checkShutdownAllowed = false) where T : GimliActorBase
        {
            Log.Debug($"WatchdogShutdownActor|IsShutdownAllowed ({typeof(T)}): Start");
            
            var task = mActorService.Get<T>().Ask<ServiceInitFinishedMessage>(MessageExtensions.CreateRequestMessage(new IsServiceInitializedMessage()));

            try
            {
                task.Wait();
            }
            catch (Exception e)
            {
                Log.Debug($"WatchdogShutdownActor|IsShutdownAllowed ({typeof(T)}): ServiceInitFinishedMessage (false)|Exception ({e.Message})");
                return false;
            }

            if (!(task.Result.State == InitFinishedState.FinishedWithSuccess && task.Result.Level == targetInitLevel))
            {
                Log.Debug($"WatchdogShutdownActor|IsShutdownAllowed ({typeof(T)}): EvalState (false)|level ({task.Result.Level})|state ({task.Result.State})|sender ({task.Result.Sender})");
                return false;
            }
            
            if (checkShutdownAllowed)
            {
                var taskSa = mActorService.Get<T>().Ask<IsShutdownAllowedResponseMessage>(MessageExtensions.CreateRequestMessage(new IsShutdownAllowedMessage()));

                try
                {
                    taskSa.Wait();
                }
                catch (Exception e)
                {
                    Log.Debug($"WatchdogShutdownActor|IsShutdownAllowed ({typeof(T)}): GeneralException|Exception ({e.Message})");
                    return false;
                }

                Log.Debug($"WatchdogShutdownActor|IsShutdownAllowed ({typeof(T)}): EvalState ({taskSa.Result.IsShutdownAllowed})");
                return taskSa.Result.IsShutdownAllowed;
            }

            Log.Debug($"WatchdogShutdownActor|IsShutdownAllowed ({typeof(T)}): Stop");
            return true;
        }
        
        public void PublishInitLevel(List<string> proxies, InitLevel initLevel)
        {
	        if (initLevel == InitLevel.L0_Starting)
	        {
                Log.Debug($"WatchdogShutdownActor|PublishInitLevel: Not sending level ({initLevel})");
				return;
	        }

            foreach (var proxy in proxies)
            {
                IActor proxyActor = null;

                switch (proxy)
                {
                    case "InstrumentServerProxyActor":
                        proxyActor = mActorService.Get<InstrumentServiceProxyActor>();
                        break;
                    case "CoreServiceProxyActor":
                        proxyActor = mActorService.Get<CoreServiceProxyActor>();
                        break;
                    case "WebServiceProxyActor":
                        proxyActor = mActorService.Get<WebServiceProxyActor>();
                        break;
                    case "NetworkCommunicationServiceProxyActor":
                        proxyActor = mActorService.Get<NetworkCommunicationServiceProxyActor>();
                        break;
                    case "DataServiceProxyActor":
                        proxyActor = mActorService.Get<DataServiceProxyActor>();
                        break;
                }

                Log.Debug($"WatchdogShutdownActor|PublishInitLevel: Sending level ({initLevel}) to ({proxy})");

                proxyActor?.Tell(MessageExtensions.CreateRequestMessage(new InitSystemMessage
                {
                    TargetLevels = new [] { initLevel }
                }));
            }
        }

    }
}
