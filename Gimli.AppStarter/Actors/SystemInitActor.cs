﻿using Akka.Actor;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.AppStarter;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;

namespace Gimli.AppStarter.Actors
{
	public class SystemInitActor : SystemInitActorBase
	{

        public SystemInitActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
        }

		protected override bool L2_StandardImplementation(IActorsystemOverallState systemState)
		{
			var success = LetAssociateAndInitProxies(InitProxies, ActorService);
			Sender.Tell(ServiceInitFinishedMessage.Generate(InitLevel.L2_ProxysLive, InitFinishedState.FinishedWithSuccess,
				CentralActorConfiguration.Instance.ServiceConfigurations[SystemState.ServiceId].TopLevelRouter));
			return success;
		}

		protected override bool InitProxies(IActorService actorService)
		{
			var success = IsProxyConnected<WebServiceProxyActor>(actorService, GetGuaranteedAnswer);
			success = IsProxyConnected<DataServiceProxyActor>(actorService, GetGuaranteedAnswer) && success;
			success = InitInstrumentCoreNetworkProxies(actorService, success) && success;
			return success;
		}
	}
}
