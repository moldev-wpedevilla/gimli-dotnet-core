﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Threading;
using Gimli.AppStarter.Actors;
using Gimli.AppStarter.Managed;
using Gimli.AppStarter.Manifest;
using Gimli.JsonObjects;
using Gimli.JsonObjects.DummyData;
using NLog;

namespace Gimli.AppStarter.Update
{
    public class Update
    {
        private readonly FileInfo mFileInfoSource;
        private string mPathTemp;
        private bool mRestartRequired;

        private readonly IAppManager mAppManager;
        private readonly WatchdogUpdateActor mWatchdogUpdateActor;

        public UpdateManifest UpdateManfifest { get; set; }
        public List<int> Steps;

        public Logger Log { get; set; } = LogManager.GetCurrentClassLogger();

        public Update(FileInfo fileInfoSource, IAppManager appManager = null, WatchdogUpdateActor watchdogUpdateActor = null)
        {
            Log.Info("Update|Update");

            mAppManager = appManager ?? AppManager.Instance;
            mWatchdogUpdateActor = watchdogUpdateActor ?? WatchdogUpdateActor.Instance;

            mFileInfoSource = fileInfoSource;
            UpdateManfifest = new UpdateManifestReader(fileInfoSource.FullName).Get();
        }

        public void ProcessUpdate(Process process = null)
        {
            Log.Info("Update|ProcessUpdate");
            mAppManager.SetStatus(AppManager.Status.UpdatingFirmware);
            mPathTemp = mFileInfoSource.DirectoryName;

            UpdateGeneral();
        }
        
        public void UpdateGeneral(bool cont = false, bool success = false, string text = null)
        {
            Log.Info("Update|UpdateGeneral: " + cont + "|" + success + "|" + text);

            if (cont)
            {
                if (success)
                {
                    success = PrepareChromium();                        //Only required for dev updates or later updates of chromium
                    if (success) success &= UpdateDeletions();
                    if (success) success &= UpdateAdditionals();
                    if (success) success &= ClearCache();

                    if (success) success &= UpdateOnBoardSoftware();
                    WriteUpdateInfo(success);
                    ShutdownAppStarter(); 
                }
                else
                {
                    mWatchdogUpdateActor.UpdateError(text);
                }
            }
            else
            {
                UpdateFirmware();
            }
        }
        private void UpdateFirmware()
        {
            Log.Info("Update|UpdateFirmware: Start");

            DirectoryInfo source = new DirectoryInfo(Path.Combine(mPathTemp, @"Firmware\"));

            if (Directory.Exists(source.FullName))
            {
                DirectoryInfo target = new DirectoryInfo(UpdateChecker.DirectoryUpdateFirmware);

                PackageManager.PackageManager.PrepareDirectory(target.FullName);
                CopyFiles(source, target, true);

                mWatchdogUpdateActor.SendFirmwareUpdateMessage(target.FullName);
            }
            else
            {
                UpdateGeneral(true, true);
            }
        }
        private bool UpdateAdditionals()
        {
            Log.Info("Update|UpdateAdditionals: Start");

            DirectoryInfo source = new DirectoryInfo(Path.Combine(mPathTemp, @"Additionals\"));

            mAppManager.SetStatus(AppManager.Status.Updating);

            if (Directory.Exists(source.FullName))
            {
                string[] additionals = Directory.GetDirectories(source.FullName);

                foreach (string additional in additionals)
                {
                    DirectoryInfo additionalSource = new DirectoryInfo(additional);
                    DirectoryInfo additionalTarget =
                        new DirectoryInfo(Path.Combine(@"C:\", additional.Split('\\').ToList().Last()));
                    CopyFiles(additionalSource, additionalTarget, true);
                }
            }

            return true;
        }
        private bool UpdateDeletions()
        {
            Log.Info("Update|UpdateDeletions: Start");

            DirectoryInfo source = new DirectoryInfo(Path.Combine(mPathTemp, @"Deletions\"));

            if (Directory.Exists(source.FullName))
            {
                IEnumerable<string> deletions = Directory.EnumerateFiles(source.FullName, "*.*", SearchOption.AllDirectories);

                foreach (string deletion in deletions)
                {
                    FileInfo deletionSource = new FileInfo(Path.Combine(@"C:\", deletion.Replace(source.FullName, "")));
                    if (deletionSource.Exists)
                    {
                        deletionSource.Delete();
                    }
                }
            }

            return true;
        }

        private bool PrepareChromium()
        {
            Log.Info("Update|PrepareChromium: Start");
            
            mAppManager.StopFrontend();
            new ManualResetEvent(false).WaitOne(5000);

            if (Directory.Exists(@"C:\Tools\Chromium"))
            {
                DirectoryInfo directoryInfoChromium = new DirectoryInfo(@"C:\Tools\Chromium");

                foreach (FileInfo file in directoryInfoChromium.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in directoryInfoChromium.GetDirectories())
                {
                    dir.Delete(true);
                }
            }

            return true;
        }

        private void CopyFiles(DirectoryInfo source, DirectoryInfo target, bool deleteSource = false)
        {
            foreach (FileInfo fileInfoSource in source.GetFiles())
            {
                fileInfoSource.CopyTo(Path.Combine(target.FullName, fileInfoSource.Name), true);
            }

            foreach (DirectoryInfo directoryInfoSourceSub in source.GetDirectories())
            {
                DirectoryInfo nextTargetDirectoryInfoSub = target.CreateSubdirectory(directoryInfoSourceSub.Name);
                CopyFiles(directoryInfoSourceSub, nextTargetDirectoryInfoSub);
            }

            if (deleteSource)
            {
                source.Delete(true);
            }
        }

        private bool UpdateOnBoardSoftware()
        {
            Log.Info("Update|UpdateOnBoardSoftware: Start");

            var query = new SelectQuery("Win32_UserAccount");
            var searcher = new ManagementObjectSearcher(query);
            
            foreach (ManagementObject envVar in searcher.Get())
            {
                if (envVar["Name"].ToString() == "User1" || envVar["Name"].ToString() == "SpectraMax_iDx")
                {
                    GimliTrace.Start("SL");

                    mRestartRequired = true;

                    var argumentsPs = $" -File \"C:\\Tools\\Powershell\\UpdateShellLauncher.ps1\" {Path.Combine(mPathTemp, "Gimli.AppStarter.exe")} {envVar["Name"]}";
                    
                    Log.Info($"Update|StartOnBoardUpdater: Update AppStarter Destination ({argumentsPs})");

                    var processStartInfoPs = new ProcessStartInfo
                    {
                        FileName = "powershell.exe",
                        Arguments = argumentsPs,
                        WindowStyle = ProcessWindowStyle.Hidden,
                        CreateNoWindow = true,
                        UseShellExecute = false,
                    };

                    var resultProcessPs = Process.Start(processStartInfoPs);
                    resultProcessPs?.WaitForExit();

                    Log.Info($"Gimli|StartOnBoardUpdater|SetupShellLauncher ({envVar["Name"]}) ({GimliTrace.Stop("SL")}ms)");
                }
            }

            var progsForFirewall = AppManager.Instance.ManagedPrograms.Where(i => i.StartupElement.UpdateFirewall == true).ToArray();
            
            foreach (var program in progsForFirewall)
            {
                AddFirewallRule(program.StartupElement.Program);
                AddFirewallRule(program.StartupElement.Program, false);
            }
            
            Log.Info("Update|UpdateOnBoardSoftware: Stop");

            return true;
        }

        private void AddFirewallRule(string program, bool dir = true)
        {
            Log.Info("Update|AddFirewallRule: Start");

            var paramDir = dir ? "in" : "out";
            var argumentsNetSh = $"advfirewall firewall add rule name={program} dir={paramDir} action=allow program={Path.Combine(mPathTemp, program)} enable=yes";
            
            Log.Info($"Update|AddFirewallRule: Update Firewall ({argumentsNetSh})");

            var processStartInfoNetSh = new ProcessStartInfo
            {
                FileName = "netsh",
                Arguments = argumentsNetSh,
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                UseShellExecute = false
            };

            Process.Start(processStartInfoNetSh);
        }

        private bool ClearCache()
        {
            Log.Info("Update|ClearCache: Start");

            if (bool.Parse(AppConfigurationManager.GetSettingsValue("ManuallyClearCache")))
            {
                Array.ForEach(Directory.GetFiles(AppConfigurationManager.GetSettingsValue("FrontendCacheDirectory")), File.Delete);
            }

            return true;
        }

        private void WriteUpdateInfo(bool updateSuccess)
        {
            var updateSuccessText = updateSuccess ? "OK" : "FAIL";
            File.WriteAllText(AppConfigurationManager.GetSettingsValue("UpdateInfoFile"), $"UPDATE|{UpdateManfifest.Version}|{updateSuccessText}");
        }

        private void ShutdownAppStarter()
        {
            Log.Info("Update|ShutdownAppStarter: Start");

            if (mRestartRequired)
            {
                mAppManager.Restart();
            }
        }
    }
}