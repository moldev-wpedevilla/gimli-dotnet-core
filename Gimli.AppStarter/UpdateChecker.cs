﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using Gimli.AppStarter.Manifest;
using Gimli.JsonObjects;
using NLog;

namespace Gimli.AppStarter
{
    public class UpdateChecker : IDisposable
    {
        public static string DirectoryUpdateBase = AppConfigurationManager.GetSettingsValue("UpdateBase");
        public static string DirectoryUpdateTemp = Path.Combine(AppConfigurationManager.GetSettingsValue("UpdateBase"), AppConfigurationManager.GetSettingsValue("UpdateTemp"));
        public static string DirectoryUpdatePending = Path.Combine(AppConfigurationManager.GetSettingsValue("UpdateBase"), AppConfigurationManager.GetSettingsValue("UpdatePending"));
        public static string DirectoryUpdateFirmware = Path.Combine(AppConfigurationManager.GetSettingsValue("UpdateBase"), AppConfigurationManager.GetSettingsValue("UpdateFirmware"));
        public static string DirectoryUpdateBackup = Path.Combine(AppConfigurationManager.GetSettingsValue("UpdateBase"), AppConfigurationManager.GetSettingsValue("UpdateBackup"));

        public Logger Log { get; set; } = LogManager.GetCurrentClassLogger();

        private string mUpdateManifest;

        public UpdateChecker(bool initDirectories = true)
        { 
            InitAppSettings(initDirectories);
        }

        private void InitAppSettings(bool initDirectories)
        {
            if (initDirectories)
                PrepareUpdateDirectories();

            mUpdateManifest = AppConfigurationManager.GetSettingsValue("UpdateManifest");
        }

        public void PrepareUpdateDirectories()
        {
            Log.Debug("PackageManager|PrepareUpdateDirectories: Start");
            PackageManager.PackageManager.PrepareDirectory(DirectoryUpdateBase);
            PackageManager.PackageManager.PrepareDirectory(DirectoryUpdateTemp);
            PackageManager.PackageManager.PrepareDirectory(DirectoryUpdatePending);
            PackageManager.PackageManager.PrepareDirectory(DirectoryUpdateFirmware);
            PackageManager.PackageManager.PrepareDirectory(DirectoryUpdateBackup);
            Log.Debug("PackageManager|PrepareUpdateDirectories: Stop");
        }
        
        public static int GetVersionFromSystem()
        {
            var locUpdateManifest = AppConfigurationManager.GetSettingsValue("UpdateManifest");
            return new UpdateManifestReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, locUpdateManifest)).Get().VersionInt;
        }

        public FileInfo ExtractPending()
        {
            Log.Debug("UpdateChecker|ExtractPending: Start");

            FileInfo updateFile = new FileInfo((Directory.GetFiles(DirectoryUpdatePending, "*.mdup", SearchOption.TopDirectoryOnly)).First());

            var newLocation = AppConfigurationManager.GetSettingsValue("ProgramBaseDirectory") + GetVersionFromUpdate(new FileInfo(Path.Combine(DirectoryUpdatePending, updateFile.Name)));
            PackageManager.PackageManager.PrepareDirectory(newLocation);

            if (PackageManager.PackageManager.Extract(Path.Combine(DirectoryUpdatePending, updateFile.Name),
                newLocation, true))
            {
                Log.Debug($"UpdateChecker|ExtractPending ({Path.Combine(DirectoryUpdatePending, mUpdateManifest)}): Stop");
                return new FileInfo(Path.Combine(newLocation, mUpdateManifest));
            }

            Log.Debug("UpdateChecker|ExtractPending (Error): Stop");
            return null;
        }

        public int GetVersionFromUpdate(FileInfo fileInfo)
        {
            string version;
            return PackageManager.PackageManager.PeekManifest(fileInfo.FullName, DirectoryUpdateTemp, mUpdateManifest, out version).VersionInt;
        }
        
        public bool IsPendingChecksumValid(FileInfo fileInfoToCheck)
        {
            var checksumPending = PackageManager.PackageManager.CheckChecksum(fileInfoToCheck.DirectoryName);
            Log.Debug($"UpdateChecker|IsPendingChecksumValid|Result: {checksumPending}");
            return checksumPending;
        }
        
        public void Dispose()
        {
        }
    }
}
