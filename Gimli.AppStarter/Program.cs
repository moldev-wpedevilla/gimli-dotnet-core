﻿using System;
using System.IO;
using System.Reflection;
using Avalonia;
using Avalonia.Logging.Serilog;
using Gimli.AppStarter.Managed;
using Gimli.CoreService.OSController.GraphicsManager;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.AppStarter;

namespace Gimli.AppStarter
{
	class Program
	{
		// Initialization code. Don't use any Avalonia, third-party APIs or any
		// SynchronizationContext-reliant code before AppMain is called: things aren't initialized
		// yet and stuff might break.
		public static void Main(string[] args) => BuildAvaloniaApp().Start(AppMain, args);

		// Avalonia configuration, don't remove; also used by visual designer.
		public static AppBuilder BuildAvaloniaApp()
			=> AppBuilder.Configure<App>()
				.UsePlatformDetect()
				.LogToDebug();

		// Your application's entry point. Here you can initialize your MVVM framework, DI
		// container, etc.
		private static void AppMain(Application app, string[] args)
		{
			BrightnessController.SetBrightness();

			Environment.CurrentDirectory = new FileInfo(Assembly.GetEntryAssembly().Location).DirectoryName;

			var thisAssemblyName = Assembly.GetEntryAssembly().GetName().Name;
			var configFilePath = thisAssemblyName + ".dll.config";
			AppConfigurationManager.Build(configFilePath);

			var hidden = bool.Parse(AppConfigurationManager.GetSettingsValue("StartHidden"));
			var persistent = bool.Parse(AppConfigurationManager.GetSettingsValue("PersistentSplashscreen"));
			var showSplashscreenCloseButton = bool.Parse(AppConfigurationManager.GetSettingsValue("SplashscreenButtonClose"));
			var mMainWindow = new MainWindow
			{
				ViewModel = new MainViewModel
				{
					ButtonCloseVisibility = showSplashscreenCloseButton
				},
				ShowInTaskbar = !hidden,
				Topmost = persistent
			};
			AppManager.Instance.Init(StartupConfigurationHandler.GetHandler(configFilePath), mMainWindow.ViewModel);

			// TODO someone: do something to reenable this feature
			//mMainWindow.Closing += MainWindowClosing;
			//mMainWindow.Show();
			//// TODO someone: do something to reenable this feature
			////StarterThread();
			app.Run(mMainWindow); //App.mMainWindow);
		}

		//private void MainWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
		//{
		//	mMainWindow.Closing -= MainWindowClosing;
		//	AppManager.Instance.Shutdown();

		//	mWorkerThread.Join();
		//}

		// TODO someone: do something to reenable this feature
		//private void StartProgramm()
		//{
		//	mProgram = new Program();
		//	mProgram.Run(this, mMainWindow.ViewModel);
		//}

		//public void StarterThread()
		//{
		//	mWorkerThread = new Thread(StartProgramm);
		//	mWorkerThread.SetApartmentState(ApartmentState.MTA);
		//	mWorkerThread.Start();
		//}
	}
}
