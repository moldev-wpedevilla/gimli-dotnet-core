﻿using System.Collections.Generic;

namespace Gimli.AppStarter.Manifest
{
    public class UpdateObject
    {
        public string ExecutingProgram { get; set; }
        public string Source { get; set; }
        public string Target { get; set; }
        public bool DeleteSource { get; set; }
        public bool Verify { get; set; }
        public string FollowupProgram { get; set; }
        public string FollowupDelay { get; set; }
        public List<string> NoCopy { get; set; } = new List<string>();
    }
}
