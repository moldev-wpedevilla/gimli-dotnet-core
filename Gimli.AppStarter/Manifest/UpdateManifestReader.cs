﻿using Gimli.JsonObjects;
using System.Configuration;
using System.IO;
using System.Xml;

namespace Gimli.AppStarter.Manifest
{
    class UpdateManifestReader
    {
        private UpdateManifest mUpdateManifest;

        public UpdateManifestReader(string filePath)
        {
            string mode = "";

            mUpdateManifest = new UpdateManifest();

            InitManifest();

            if (!File.Exists(filePath))
            {
                return;
            }

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(filePath);
            
            mUpdateManifest.Checksum = xmlDocument.GetElementsByTagName("checksum")[0]?.Attributes?["value"].Value;
            mUpdateManifest.Version = xmlDocument.GetElementsByTagName("version")[0]?.Attributes?["value"].Value;

            var updateObjectOnBoard = new UpdateObject();
            var xmlNodeListOnBoard = xmlDocument.SelectNodes("/gimliupdate/onboard/parameter");

            foreach (XmlNode node in xmlNodeListOnBoard)
            {
                switch (node.Attributes["name"].Value)
                {
                    case "ExecutingProgram":
                        updateObjectOnBoard.ExecutingProgram = node.Attributes["value"].Value;
                        break;
                    case "FollowupProgram":
                        updateObjectOnBoard.FollowupProgram = node.Attributes["value"].Value; 
                        break;
                    case "FollowupDelay":
                        updateObjectOnBoard.FollowupDelay = node.Attributes["value"].Value;
                        break;
                    case "NoCopy":
                        updateObjectOnBoard.NoCopy.Add(node.Attributes["value"].Value);
                        break;
                }
            }
            mUpdateManifest.UpdateObjects.Add(updateObjectOnBoard);
        }

        public UpdateManifest Get()
        {
            return mUpdateManifest;
        }

        private void InitManifest()
        {
            mUpdateManifest.Version = AppConfigurationManager.GetSettingsValue("UpdateBaseVersion");
        }
    }
}
