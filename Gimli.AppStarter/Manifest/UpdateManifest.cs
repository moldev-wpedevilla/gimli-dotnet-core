﻿using System.Collections.Generic;

namespace Gimli.AppStarter.Manifest
{
    public class UpdateManifest
    {
        public string Version { get; internal set; }
        public string Checksum { get; internal set; }
        public List<UpdateObject> UpdateObjects { get; internal set; }

        public string UpdateProgram { get; internal set; }
        public List<string> UpdateParameters { get; internal set; }

        public int VersionInt
        {
            get
            {
                int versionInt;
                if (!int.TryParse(Version.Replace(".", ""), out versionInt))
                {
                    versionInt = 10000;
                }
                return versionInt;
            }
        }

        public UpdateManifest()
        {
            UpdateObjects = new List<UpdateObject>();
            UpdateParameters = new List<string>();
        }

        public bool IsNewerThan(UpdateManifest otherUpdateManifest)
        {
            return !(VersionInt <= otherUpdateManifest.VersionInt);
        }
    }
}
