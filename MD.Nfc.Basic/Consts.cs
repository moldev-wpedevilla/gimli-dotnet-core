﻿using System;

namespace MD.Nfc.Basic
{
    public static class Consts
    {
        public const int MaxFiltersPerSlider = 6;
        public const int MaxTagReadingTrials = 1;
    };

    public static class Auxiliary
    {
        public static string RemoveZeroChars(string text)
        {
            var trimmedStr = -1 != text.IndexOf("\0", StringComparison.Ordinal) ? text.Substring(0, text.IndexOf("\0", StringComparison.Ordinal)) : text;
            return trimmedStr;
        }

    }
}
