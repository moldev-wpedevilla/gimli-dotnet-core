﻿namespace MD.Nfc.Basic
{
    public enum WriteSliderDataError
    {
        NoError,
        WriteError,
        LockError
    }

    public enum CheckFilterDataResponse
    {
        Success,
        Failure,
        NonexistentId
    }

    public enum WriteAndLockFilterDataError
    { 
        NoError,
        WriteError,
        CheckError,
        LockError,
        LockedErrorOnWriting,
        LockedErrorOnLocking,
        UnknownFilterOnLocking
    }

    public enum FilterType
    {
        BandPass,
        LongPass,
        ShortPass,
        DualLabel
    }

    public enum MeasureType
    {
        Abs,
        Lum,
        Fl,
        Fpv,
        Fph
    }

    public enum SlideProtection
    {
        Na,
        Gxp
    };

    public enum SlideType
    {
        Ex,
        Em
    };

    public enum TagSlide
    {
        Ex,     // Excitation antenna
        Em      // Emition antenna
    }

    public enum SlideAntenna
    {
        Touch,      // Touch screen antenna
        Ex,         // Excitation antenna
        Em          // Emition antenna
    };
}
