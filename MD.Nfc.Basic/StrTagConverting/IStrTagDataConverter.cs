﻿using System;

namespace MD.Nfc.Basic.StrTagConverting
{
    public interface IStrTagDataConverter<T> : IDisposable
        where T : TagData
    {
        string ToEncodedString(T tag);
        string ToReadableString(T tag);
        T FromString(string strTag, string uid);
    }
}
