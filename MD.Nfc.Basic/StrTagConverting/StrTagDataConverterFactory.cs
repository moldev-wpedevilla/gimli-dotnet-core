﻿namespace MD.Nfc.Basic.StrTagConverting
{
    public class StrTagDataConverterFactory
    {
        private static IStrTagDataConverter<FilterTagData> GenerateFilterStrTagConverter()
        {
            return FilterStrTagConverterFactory.GenerateFilterStrTagConverter();
        }

        private static IStrTagDataConverter<SliderTagData> GenerateSlideStrTagConverter()
        {
            return SliderStrTagConverterFactory.GenerateSliderStrTagConverter();
        }

        private static IStrTagDataConverter<LinkTagData> GenerateLinkStrTagConverter()
        {
            return LinkStrTagConverterFactory.GenerateLinkStrTagConverter();
        }

        private static IStrTagDataConverter<ProtocolTagData> GenerateProtocolStrTagConverter()
        {
            return ProtocolStrTagConverterFactory.GenerateProtocolStrTagConverter();
        }

        private static IStrTagDataConverter<CardTagData> GenerateCardStrTagConverter()
        {
            return CardStrTagConverterFactory.GenerateCardStrTagConverter();
        }

        private static IStrTagDataConverter<PhoneTagData> GenerateNtagPhoneStrTagConverter()
        {
            return NtagPhoneStrTagConverterFactory.GeneratePhoneStrTagConverter();
        }

        public static TagData ConvertStrTagData(string strTag, string uid)
        {
            TagData tag;
            //if (string.IsNullOrWhiteSpace(strTag) || strTag.StartsWith("\0\0\0"))
            //{
            //    tag = new EmptyTagData();
            //    return tag;
            //}

            
            using (var filterStrTagConverter = GenerateFilterStrTagConverter())
            {
                tag = filterStrTagConverter.FromString(strTag, uid);
                if (null != tag && tag.IsValid)
                {
                    return tag;
                }
            }
            using (var slideStrTagConverter = GenerateSlideStrTagConverter())
            {
                tag = slideStrTagConverter.FromString(strTag, uid);
                if (null != tag && tag.IsValid)
                {
                    return tag;
                }
            }
            using (var ntagPhoneStrTagConverter = GenerateNtagPhoneStrTagConverter())
            {
                tag = ntagPhoneStrTagConverter.FromString(strTag, uid);
                if (null != tag && tag.IsValid)
                {
                    return tag;
                }
            }
            using (var cardStrTagConverter = GenerateCardStrTagConverter())
            {
                tag = cardStrTagConverter.FromString(strTag, uid);
                if (null != tag && tag.IsValid)
                {
                    return tag;
                }
            }
            using (var linkStrTagConverter = GenerateLinkStrTagConverter())
            {
                tag = linkStrTagConverter.FromString(strTag, uid);
                if (null != tag && tag.IsValid)
                {
                    return tag;
                }
            }
            using (var protocolStrTagConverter = GenerateProtocolStrTagConverter())
            {
                tag = protocolStrTagConverter.FromString(strTag, uid);
                if (null != tag && tag.IsValid)
                {
                    return tag;
                }
            }
            
            tag = new UnknownTagData() {Uid = uid, StrTag = strTag};
            return tag;
        }

        //public static TagData ConvertStrTag(string strTag)
        //{
        //    TagData tag;
        //    if (string.IsNullOrWhiteSpace(strTag) || strTag.StartsWith("\0\0\0"))
        //    {
        //        tag = new EmptyTagData();
        //        return tag;
        //    }
        //    using (var filterStrTagConverter = GenerateFilterStrTagConverter())
        //    {
        //        tag = filterStrTagConverter.FromString(strTag);
        //        if (null != tag && tag.IsValid)
        //        {
        //            return tag;
        //        }
        //    }
        //    using (var slideStrTagConverter = GenerateSlideStrTagConverter())
        //    {
        //        tag = slideStrTagConverter.FromString(strTag);
        //        if (null != tag && tag.IsValid)
        //        {
        //            return tag;
        //        }
        //    }
        //    tag = new EmptyTagData();
        //    return tag;

        //    return null;
        //}
    }
}
