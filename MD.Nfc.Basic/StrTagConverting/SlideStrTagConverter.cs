﻿using System;
using System.Diagnostics;
using System.Text;

namespace MD.Nfc.Basic.StrTagConverting
{
    public enum TagType
    {
        Unknown = 0,
        Slide = 1,
        Filter = 2,
        Link = 3,
        Phone = 4,
        Protocol = 5,
        Card = 6
    };

    public class SliderStrTagConverterFactory
    {
        public static IStrTagDataConverter<SliderTagData> GenerateSliderStrTagConverter()
        {
            return SliderStrTagConverter.Generate();
        }
    }

    [Serializable]
    public class SliderStrTagConverter : IStrTagDataConverter<SliderTagData>
    {
        public static SliderStrTagConverter Generate()
        {
            return new SliderStrTagConverter();
        }

        protected SliderStrTagConverter()
        {

        }

        public void Dispose()
        {

        }

        public string ToString_Old(SliderTagData tag)
        {
            if (null == tag || !tag.IsValid) return "";

            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append($"{Convert.ToByte(TagType.Slide)}#"); // 1#

            //strBuilder.Append($"{tag.No}:{tag.Name}:{(int)tag.Type}:{Convert.ToInt32(tag.IsStandard)}:{(int)tag.Protection}#");    // 1# ...
            strBuilder.Append($"{tag.No}:{tag.Name}#");    // 1# ...

            for (var filterIndex = 0; filterIndex < Consts.MaxFiltersPerSlider; filterIndex++)
            {
                FilterTagData filterTag = tag.Filter[filterIndex];              

                strBuilder.Append($"{(int)filterTag.FilterId}");
                var compositeVal = (byte)((filterTag.Type << 4) | (filterTag.Polarization));

                //strBuilder.Append($":{filterTag.Type}:{(int)filterTag.MeasureType}:{(int)filterTag.Polarization}");
                strBuilder.Append($":{compositeVal}");

                for (var bandIndex = 0; bandIndex < /*3*/1; bandIndex++)
                {
                    //strBuilder.Append($":{filterTag.CentralWavelengthOrTransition[index]}:{filterTag.Transition[index]}:{filterTag.Bandwidth[index]}");
                    strBuilder.Append($":{filterTag.Band[bandIndex].CentralWavelengthOrTransition}");
                }

                if (5 > filterIndex)
                {
                    strBuilder.Append(",");
                }
            }
            var strResult = strBuilder.ToString();
            return strResult;
        }

        public string ToEncodedString(SliderTagData tag)
        {
            try
            {
                var numIndex = tag.Name.IndexOfAny(new char[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'});
                
                // TODO Brane Using No part in tag for numeric part of the name!!!
                var tagNameStrAlfa = tag.Name.Substring(0, numIndex > 0 ? numIndex : tag.Name.Length);
                var tagNameStrNum = (-1 == numIndex) ?  "" : tag.Name.Substring(numIndex);

                if (null == tag || !tag.IsValid) return "";

                var tagByte = new byte[29];

                tagByte[0] = (byte)((((byte)TagType.Slide) << 4) | ((byte)(tag.No >> 8)));
                tagByte[1] = (byte)(tag.No & 0xFF);

                tag.Name = ConvertingHelper.NormalizeTagName(tag.Name);
                tagByte[2] = (byte)((ConvertingHelper.GetCharCode(tag.Name[0]) << 2) | (ConvertingHelper.GetCharCode(tag.Name[1]) >> 4) & 0x03);
                tagByte[3] = (byte)(((ConvertingHelper.GetCharCode(tag.Name[1]) & 0x0F) << 4) | ((ConvertingHelper.GetCharCode(tag.Name[2]) >> 2) & 0x0F));
                tagByte[4] = (byte)(((ConvertingHelper.GetCharCode(tag.Name[2]) & 0x03) << 6) | (ConvertingHelper.GetCharCode(tag.Name[3]) & 0x3F));

                for (var filterIndex = 0; filterIndex < Consts.MaxFiltersPerSlider; filterIndex++)
                {
                    FilterTagData filterTag = tag.Filter[filterIndex];

                    tagByte[(filterIndex * 4) + 5] = (byte)(filterTag.FilterId >> 4);
                    tagByte[(filterIndex * 4) + 6] = (byte)((byte)((filterTag.FilterId & 0x0F) << 4) | ((byte)(filterTag.Band[0].CentralWavelengthOrTransition >> 8)));
                    tagByte[(filterIndex * 4) + 7] = (byte)(filterTag.Band[0].CentralWavelengthOrTransition & 0xFF);
                    tagByte[(filterIndex * 4) + 8] = (byte)((filterTag.Type << 4) | (filterTag.Polarization));
                }
                var strResult = Convert.ToBase64String(tagByte);
                return strResult;
            }
            catch (Exception exc)
            {
                return "";
            }

        }

        public string ToReadableString(SliderTagData tag)
        {
            try
            {
                if (null == tag || !tag.IsValid) return "";

                StringBuilder strBuilder = new StringBuilder();
                strBuilder.Append($"Slider | Id:{tag.Id} | No:{tag.No} | {tag.Name}");

                for (var filterIndex = 0; filterIndex < Consts.MaxFiltersPerSlider; filterIndex++)
                {
                    FilterTagData filterTag = tag.Filter[filterIndex];
                    strBuilder.Append($" || 1 | ID:{filterTag.FilterId} | Type:{ConvertingHelper.GetTypeString(filterTag.Type)} | Meas.:{ConvertingHelper.GetMeasuringString(filterTag.MeasureType)} | Polar:{ConvertingHelper.GetPolarizationString(filterTag.Polarization)}");
                    for (var bandIndex = 0; bandIndex < /*3*/1; bandIndex++)
                    {
                        //strBuilder.Append($":{filterTag.CentralWavelengthOrTransition[index]}:{filterTag.Transition[index]}:{filterTag.Bandwidth[index]}");
                        strBuilder.Append($" || 1 | CWL/T:{filterTag.Band[bandIndex].CentralWavelengthOrTransition}");
                    }
                }
                var strResult = strBuilder.ToString();
                return strResult;
            }
            catch (Exception exc)
            {
                return "...";
            }
        }

        public SliderTagData FromString_Old(string strTag, string uid)
        {
            var tag = new SliderTagData(uid);
            var subStr = strTag.Split('#');
            if (Convert.ToString((int)TagType.Slide) != subStr[0])
            {
                //throw new Exception("String tag should begin with \"1#\" for slide tags.");
                tag.IsValid = false;
                return tag;
            }
            var slideAttrib = subStr[1].Split(':');
            var slideDataIndex = 0;
            tag.No = byte.Parse(slideAttrib[slideDataIndex++]);
            tag.Name = slideAttrib[slideDataIndex++];
            //tag.Type = byte.Parse(slideAttrib[slideDataIndex++]);
            //tag.IsStandard = Convert.ToBoolean(byte.Parse(slideAttrib[slideDataIndex++]));
            //tag.Protection = byte.Parse(slideAttrib[slideDataIndex]);

            string[] filter = subStr[2].Split(',');
            for (var filterIndex = 0; filterIndex < Consts.MaxFiltersPerSlider; filterIndex++)
            {
                string[] filterAttrib = filter[filterIndex].Split(':');
                var filterDataIndex = 0;
                var filterTag = tag.Filter[filterIndex];

                filterTag.FilterId = /*(FilterRegistry)*/int.Parse(filterAttrib[filterDataIndex++]);

                var compositeVal = byte.Parse(filterAttrib[filterDataIndex++]);
                filterTag.Type = (byte)(compositeVal >> 4);  //byte.Parse(filterAttrib[filterDataIndex++]);
                //filterTag.MeasureType = byte.Parse(filterAttrib[filterDataIndex++]);
                filterTag.Polarization = (byte) (compositeVal & 0x0F);  //byte.Parse(filterAttrib[filterDataIndex++]);
                for (var bandIndex = 0; bandIndex < /*3*/1; bandIndex++)
                {
                    //filterTag.CentralWavelengthOrTransition[bandIndex] = short.Parse(filterAttrib[filterDataIndex++]);
                    //filterTag.Transition[bandIndex] = byte.Parse(filterAttrib[filterDataIndex++]);
                    if (filterIndex < 5)
                    {
                        //filterTag.Bandwidth[bandIndex] = short.Parse(filterAttrib[filterDataIndex++]);
                        filterTag.Band[bandIndex].CentralWavelengthOrTransition = short.Parse(filterAttrib[filterDataIndex++]);
                    }
                    else
                    {
                        //filterTag.Bandwidth[bandIndex] = short.Parse(Auxiliary.RemoveZeroChars(filterAttrib[filterDataIndex]));
                        filterTag.Band[bandIndex].CentralWavelengthOrTransition = short.Parse((Auxiliary.RemoveZeroChars(filterAttrib[filterDataIndex])));
                    }
                }
            }
            tag.IsValid = true;
            return tag;
        }

        public SliderTagData FromString(string strTag, string uid)
        {
            try
            {
                var tag = new SliderTagData(uid);
                var tagCode = Convert.FromBase64String(strTag);
                var tagType = (tagCode[0] >> 4);
                if ((int) TagType.Slide != tagType)
                {
                    tag.IsValid = false;
                    return tag;
                }

                // TODO Brane to change No into int!?
                tag.Id = 1; // For the time being no ID is stored in the slide tag!
                tag.No = (byte) (((tagCode[0] & 0x0F) << 8) | tagCode[1]);

                // TODO Brane To remove duplicates (-> filter)!!
                var ch1 = ConvertingHelper.Decode(tagCode[2] >> 2);
                var ch2 = ConvertingHelper.Decode(((tagCode[2] & 0x03) << 4) | ((tagCode[3] >> 4) & 0x0F));
                var ch3 = ConvertingHelper.Decode(((tagCode[3] & 0x0F) << 2) | tagCode[4] >> 6);
                var ch4 = ConvertingHelper.Decode(tagCode[4] & 0x3F);
                tag.Name = new string(new char[] {ch1, ch2, ch3, ch4})/* + tag.No*/;
                tag.Name = tag.Name.Trim();
                
                for (var filterIndex = 0; filterIndex < Consts.MaxFiltersPerSlider; filterIndex++)
                {
                    var filterTag = tag.Filter[filterIndex];
                    filterTag.FilterId = (tagCode[(filterIndex*4) + 5] << 4) | (tagCode[(filterIndex*4) + 6] >> 4);
                    filterTag.Type = (byte) (tagCode[(filterIndex*4) + 8] >> 4);
                    filterTag.Polarization = (byte) (tagCode[(filterIndex*4) + 8] & 0x0F);
                    for (var bandIndex = 0; bandIndex < /*3*/ 1; bandIndex++)
                    {
                        filterTag.Band[bandIndex].CentralWavelengthOrTransition =
                            (byte)
                                ((tagCode[(bandIndex*2) + ((filterIndex*4) + 6)] << 8) |
                                 tagCode[(bandIndex*2) + ((filterIndex*4) + 7)]);
                    }
                }

                tag.StrTag = strTag;
                tag.IsValid = true;

                return tag;
            }
            catch (Exception exc)
            {
                Trace.WriteLine($"SliderStrTagConverter::FromString(): {exc.Message} - {exc.InnerException?.Message}");
                var tag = new SliderTagData();
                tag.IsValid = false;
                return tag;
            }
        }
    }
}
