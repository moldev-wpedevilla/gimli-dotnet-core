﻿using System;
using System.Text;

namespace MD.Nfc.Basic.StrTagConverting
{
    public class FilterStrTagConverterFactory
    {
        public static IStrTagDataConverter<FilterTagData> GenerateFilterStrTagConverter()
        {
            return FilterStrTagConverter.Generate();
        }
    }

    [Serializable]
    public class FilterStrTagConverter : IStrTagDataConverter<FilterTagData>
    {
        private const int GroupAmount = 3;

        public static FilterStrTagConverter Generate()
        {
            return new FilterStrTagConverter();
        }

        protected FilterStrTagConverter()
        {

        }

        public string ToString_Old(FilterTagData tag)
        {
            if (null == tag || !tag.IsValid) return "";

            StringBuilder strBuilder = new StringBuilder();

            strBuilder.Append($"{(int)TagType.Filter}#");
            strBuilder.Append($"{(int)tag.FilterId}:{tag.Name}:{(int)tag.Type}:{(int)tag.MeasureType}:{tag.Polarization}:{tag.Band[0].CentralWavelengthOrTransition}:{tag.Band[0].Transmission}:{tag.Band[0].Bandwidth}:{tag.Band[1].CentralWavelengthOrTransition}:{tag.Band[1].Transmission}:{tag.Band[1].Bandwidth}:{tag.Band[2].CentralWavelengthOrTransition}:{tag.Band[2].Transmission}:{tag.Band[2].Bandwidth}");

            return strBuilder.ToString();
        }

        public string ToEncodedString(FilterTagData tag)
        {
            try
            {
                if (null == tag || !tag.IsValid) return "";

                var tagByte = new byte[26];

                tagByte[0] = (byte) ((((byte) TagType.Filter) << 4) | ((byte) (tag.FilterId >> 8)));
                tagByte[1] = (byte) (tag.FilterId & 0xFF);
                
                tag.Name = ConvertingHelper.NormalizeTagName(tag.Name);
                for (var nameGroupIndex = 0; nameGroupIndex < GroupAmount; nameGroupIndex++)
                {
                    tagByte[(nameGroupIndex * 3) + 2] = (byte)(( ConvertingHelper.GetCharCode(tag.Name[(nameGroupIndex * 4) + 0]) << 2)         | (ConvertingHelper.GetCharCode (tag.Name[(nameGroupIndex * 4) + 1]) >> 4) & 0x03);
                    tagByte[(nameGroupIndex * 3) + 3] = (byte)(((ConvertingHelper.GetCharCode(tag.Name[(nameGroupIndex * 4) + 1]) & 0x0F) << 4) | ((ConvertingHelper.GetCharCode(tag.Name[(nameGroupIndex * 4) + 2]) >> 2) & 0x0F));
                    tagByte[(nameGroupIndex * 3) + 4] = (byte)(((ConvertingHelper.GetCharCode(tag.Name[(nameGroupIndex * 4) + 2]) & 0x03) << 6) | (ConvertingHelper.GetCharCode (tag.Name[(nameGroupIndex * 4) + 3]) & 0x3F));
                }

                //tagByte[2] = (byte)((ConvertingHelper.GetCharCode(tag.Name[0]) << 2) | (ConvertingHelper.GetCharCode(tag.Name[1]) >> 4) & 0x03);
                //tagByte[3] = (byte)(((ConvertingHelper.GetCharCode(tag.Name[1]) & 0x0F) << 4) | ((ConvertingHelper.GetCharCode(tag.Name[2]) >> 2) & 0x0F));
                //tagByte[4] = (byte)(((ConvertingHelper.GetCharCode(tag.Name[2]) & 0x03) << 6) | (ConvertingHelper.GetCharCode(tag.Name[3]) & 0x3F));

                var offset = 6;
                tagByte[offset + 5] = (byte) (((tag.Type << 4) & 0xF0) | (tag.Polarization  & 0x0F));

                tagByte[offset + 6] = tag.MeasureType;
                tagByte[offset + 7] = tag.ExtensionModuleOnly;

                tagByte[offset + 8] = tag.Band[0].Transmission;
                tagByte[offset + 9] = (byte) (tag.Band[0].CentralWavelengthOrTransition >> 4);
                tagByte[offset + 10] = (byte) (((tag.Band[0].CentralWavelengthOrTransition << 4) & 0xF0) | (tag.Band[0].Bandwidth >> 8));
                tagByte[offset + 11] = (byte) (tag.Band[0].Bandwidth & 0xFF);

                tagByte[offset + 12] = tag.Band[1].Transmission;
                tagByte[offset + 13] = (byte) (tag.Band[1].CentralWavelengthOrTransition >> 4);
                tagByte[offset + 14] = (byte) (((tag.Band[1].CentralWavelengthOrTransition << 4) & 0xF0) | (tag.Band[1].Bandwidth >> 8));
                tagByte[offset + 15] = (byte) (tag.Band[1].Bandwidth & 0xFF);

                tagByte[offset + 16] = tag.Band[2].Transmission;
                tagByte[offset + 17] = (byte) (tag.Band[2].CentralWavelengthOrTransition >> 4);
                tagByte[offset + 18] = (byte) (((tag.Band[2].CentralWavelengthOrTransition << 4) & 0xF0) | (tag.Band[2].Bandwidth >> 8));
                tagByte[offset + 19] = (byte) (tag.Band[2].Bandwidth & 0xFF);

                var tagStrEncoded = Convert.ToBase64String(tagByte);
                return tagStrEncoded;
            }
            catch (Exception exc)
            {
                return "...";
            }
        }

        public string ToReadableString(FilterTagData tag)
        {
            var strBuilder = new StringBuilder();
            try
            {
                if (null == tag || !tag.IsValid) return "";

                strBuilder.Append($"Filter | ID:{tag.FilterId} | {tag.Name} | ChipID: {tag.Uid}");
                strBuilder.Append($" | Type:{ConvertingHelper.GetTypeString(tag.Type)} | Meas.:{ConvertingHelper.GetMeasuringString(tag.MeasureType)} | Polar.:{ConvertingHelper.GetPolarizationString(tag.Polarization)} | Ext:{tag.ExtensionModuleOnly}");

                for (var bandIndex = 0; bandIndex < 3; bandIndex++)
                {
                    strBuilder.Append($" || CWL/T:{tag.Band[bandIndex].CentralWavelengthOrTransition} | BW:{tag.Band[bandIndex].Bandwidth} | Tr:{tag.Band[bandIndex].Transmission}");
                }

                return strBuilder.ToString();
            }
            catch (Exception exc)
            {
                return "...";
            }
        }

        public FilterTagData FromString_Old(string strTag, string uid)
        {
            FilterTagData tag = new FilterTagData(uid);
            string[] subStr = strTag.Split('#');
            if (Convert.ToString((int)TagType.Filter) != subStr[0])
            {
                //throw new Exception("String tag should begin with \"2#\" for filter tags.");
                tag.IsValid = false;
                return tag;
            }

            string[] filterAttrib = subStr[1].Split(':');
            var filterIndex = 0;
            tag.FilterId = /*(FilterRegistry)*/int.Parse(filterAttrib[filterIndex++]);
            tag.Name = filterAttrib[filterIndex++];
            tag.Type = byte.Parse(filterAttrib[filterIndex++]);
            tag.MeasureType = byte.Parse(filterAttrib[filterIndex++]);
            tag.Polarization = byte.Parse(filterAttrib[filterIndex++]);

            tag.Band[0].CentralWavelengthOrTransition = short.Parse(filterAttrib[filterIndex++]);
            tag.Band[0].Transmission = byte.Parse(filterAttrib[filterIndex++]);
            tag.Band[0].Bandwidth = short.Parse(filterAttrib[filterIndex++]);

            tag.Band[1].CentralWavelengthOrTransition = short.Parse(filterAttrib[filterIndex++]);
            tag.Band[1].Transmission = byte.Parse(filterAttrib[filterIndex++]);
            tag.Band[1].Bandwidth = short.Parse(filterAttrib[filterIndex++]);

            tag.Band[2].CentralWavelengthOrTransition = short.Parse(filterAttrib[filterIndex++]);
            tag.Band[2].Transmission = byte.Parse(filterAttrib[filterIndex++]);
            tag.Band[2].Bandwidth = short.Parse(Auxiliary.RemoveZeroChars(filterAttrib[filterIndex]));

            tag.IsValid = true;
            return tag;
        }

        public FilterTagData FromString(string strTag, string uid)
        {
            try
            {
                var tag = new FilterTagData(uid);
                var tagCode = Convert.FromBase64String(strTag);
                // TODO Check if tagCode is long enough (has appropriate data)!!!
                var tagType = (tagCode[0] >> 4);
                if ((int) TagType.Filter != tagType)
                {
                    tag.IsValid = false;
                    return tag;
                }

                tag.FilterId = ((tagCode[0] & 0x0F) << 8) | tagCode[1];

                var nameStrBuilder = new StringBuilder();
                for (var nameGroupIndex = 0; nameGroupIndex < 3; nameGroupIndex++)
                {
                    var ch1 = ConvertingHelper.Decode(tagCode[(nameGroupIndex * 3) + 2] >> 2);
                    var ch2 = ConvertingHelper.Decode(((tagCode[(nameGroupIndex * 3) + 2] & 0x03) << 4) | ((tagCode[(nameGroupIndex * 3) + 3] >> 4) & 0x0F));
                    var ch3 = ConvertingHelper.Decode(((tagCode[(nameGroupIndex * 3) + 3] & 0x0F) << 2) | tagCode[(nameGroupIndex * 3) + 4] >> 6);
                    var ch4 = ConvertingHelper.Decode(tagCode[(nameGroupIndex * 3) + 4] & 0x3F);
                    var name = new string(new char[] {ch1, ch2, ch3, ch4});
                    nameStrBuilder.Append(name);
                }

                tag.Name = nameStrBuilder.ToString();
                tag.Name = tag.Name.Trim();

                var offset = 6;
                tag.Type = (byte) ((tagCode[offset + 5] >> 4) & 0x0F);
                tag.Polarization = (byte) (tagCode[offset + 5] & 0x03);
                tag.MeasureType = tagCode[offset + 6];
                tag.ExtensionModuleOnly = (byte)(tagCode[offset + 7] & 0x01);

                tag.Band[0].Transmission = tagCode[offset + 8];
                tag.Band[0].CentralWavelengthOrTransition = (short) ((tagCode[offset + 9] << 4) | (tagCode[offset + 10] >> 4));
                tag.Band[0].Bandwidth = (short) (((tagCode[offset + 10] & 0x0F) << 8) | tagCode[offset + 11]);

                tag.Band[1].Transmission = tagCode[offset + 12];
                tag.Band[1].CentralWavelengthOrTransition = (short) ((tagCode[offset + 13] << 4) | (tagCode[offset + 14] >> 4));
                tag.Band[1].Bandwidth = (short) (((tagCode[offset + 14] & 0x0F) << 8) | tagCode[offset + 15]);

                tag.Band[2].Transmission = tagCode[offset + 16];
                tag.Band[2].CentralWavelengthOrTransition = (short) ((tagCode[offset + 17] << 4) | (tagCode[offset + 18] >> 4));
                tag.Band[2].Bandwidth = (short) (((tagCode[offset + 18] & 0x0F) << 8) | tagCode[offset + 19]);

                tag.StrTag = strTag;
                tag.IsValid = true;
                return tag;
            }
            catch (Exception exc)
            {
                var tag = new FilterTagData();
                tag.IsValid = false;
                return tag;
            }
        }

        public void Dispose()
        {
        }
    }
}
