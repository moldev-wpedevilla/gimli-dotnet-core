﻿using System;

namespace MD.Nfc.Basic.StrTagConverting
{
    public class LinkStrTagConverterFactory
    {
        public static IStrTagDataConverter<LinkTagData> GenerateLinkStrTagConverter()
        {
            return LinkStrTagConverter.Generate();
        }
    }

    [Serializable]
    public class LinkStrTagConverter : IStrTagDataConverter<LinkTagData>
    {
        public static LinkStrTagConverter Generate()
        {
            return new LinkStrTagConverter();
        }

        protected LinkStrTagConverter()
        {
        }

        public void Dispose()
        {
        }

        public string ToEncodedString(LinkTagData tag)
        {
            return "...";
        }

        public string ToReadableString(LinkTagData tag)
        {
            return $"3#{tag.Link}";
        }

        public LinkTagData FromString(string strTag, string uid)
        {
            if (string.IsNullOrWhiteSpace(strTag)) return null;
            if (!strTag.StartsWith("3#")) return null;

            var strTagPart = strTag.Split('#');
            var linkCand = -1 != strTagPart[1].IndexOf('\0') ? strTagPart[1].Substring(0, strTagPart[1].IndexOf('\0')) : strTagPart[1];

            var linkData = new LinkTagData
            {
                Link = linkCand,
                Uid = uid,
                IsValid = true
            };

            return linkData;
        }
    }
}
