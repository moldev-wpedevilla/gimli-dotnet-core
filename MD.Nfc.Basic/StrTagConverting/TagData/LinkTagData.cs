﻿namespace MD.Nfc.Basic.StrTagConverting
{
    public class LinkTagData : TagData
    {
        public string Link { get; set; }
    }
}
