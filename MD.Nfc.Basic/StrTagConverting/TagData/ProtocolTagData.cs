﻿using System;

namespace MD.Nfc.Basic.StrTagConverting
{
    public class ProtocolTagData : TagData
    {
        public Guid GuidTag { get; set; }
    }
}
