﻿using System.Xml.Serialization;

namespace MD.Nfc.Basic.StrTagConverting
{
    public class UnknownTagData : TagData
    {
        [XmlIgnore] public IStrTagDataConverter<UnknownTagData> UnknownStrTagConverter { get; set; }

        public string ToReadableString(UnknownTagData tag)
        {
            return tag.StrTag;   //UnknownStrTagConverter.ToReadableString(this);
        }
    }
}
