﻿using System;
using System.Xml.Serialization;

namespace MD.Nfc.Basic.StrTagConverting
{
    [Serializable]
    public abstract class TagData
    {
        protected TagData()
        {
            IsValid = false;
        }

        protected TagData(string uid)
        {
            Uid = uid;
        }

        public bool IsValid { get; set; }
        public string Uid { get; set; }
        public string StrTag { get; set; }
    }

    [Serializable]
    public class SliderTagData : TagData
    {
        [XmlIgnore] public IStrTagDataConverter<SliderTagData> SliderStrTagConverter { get; set; }
        public FilterTagData[] Filter { get; set; } = new FilterTagData[Consts.MaxFiltersPerSlider];
        public long Id { get; set; }
        public byte No { get; set; }
        public string Name { get; set; }
        public byte Type { get; set; }
        public bool IsStandard { get; set; }
        public byte Protection { get; set; }

        public SliderTagData()
        {
            Init();
        }

        public SliderTagData(string uid) : base(uid)
        {
            Init();
        }

        private void Init()
        {
            SliderStrTagConverter = SliderStrTagConverterFactory.GenerateSliderStrTagConverter();

            Id = -1;
            Name = Properties.Resources.DefaultSliderName;
            for (var filterIndex = 0; filterIndex < Consts.MaxFiltersPerSlider; filterIndex++)
            {
                Filter[filterIndex] = new FilterTagData();
            }
        }

        public string ToReadableString()
        {
            var resultStr = SliderStrTagConverter.ToReadableString(this);
            return resultStr;
        }

        public string ToEncodedString()
        {
            return SliderStrTagConverter.ToEncodedString(this);
        }
    }

    [Serializable]
    public class Band
    {
        public short CentralWavelengthOrTransition { set; get; }
        public byte Transmission { set; get; }
        public short Bandwidth { set; get; }
        public Band()
        {
            Init(0, 0, 0);
        }

        public Band(short wavelength, byte transmission, short bandwidth)
        {
            Init(wavelength, transmission, bandwidth);
        }

        private void Init(short wavelength, byte transmission, short bandwidth)
        {
            CentralWavelengthOrTransition = wavelength;
            Transmission = transmission;
            Bandwidth = bandwidth;
        }
    }

    [Serializable]
    public class FilterTagData : TagData
    {
        private const byte AmountOfBands = 3;

        [XmlIgnore]
        public IStrTagDataConverter<FilterTagData> FilterStrTagConverter;

        public int FilterId { get; set; }
        public string Name { get; set; }
        public byte Type { get; set; }                      // 1-BandPass, 2-LongPass, 3-ShortPass, 4-DualLabel, 5-UNI
        public byte MeasureType { get; set; }               // Combination possible. Bitwise: 0-ABS, 1-FL, 2-LUM, 3-FP, 4-TRF
        public Band[] Band { get; set; }
        public byte Polarization { get; set; }              // 0-NA, 1-Vertical, 2-Horizontal
        public byte ExtensionModuleOnly { get; set; }

        public FilterTagData()
        {
            Init();
        }

        public FilterTagData(string uid) : base(uid)
        {
            Init();
        }

        private void Init()
        {
            FilterStrTagConverter = FilterStrTagConverterFactory.GenerateFilterStrTagConverter();
            FilterId = 0;
            Band = new Band[AmountOfBands];
            for (var bandIndex = 0; bandIndex < AmountOfBands; bandIndex++)
            {
                Band[bandIndex] = new Band();
            }
            //CentralWavelengthOrTransition = new short[AmountOfBands];
            //Transmission = new byte[AmountOfBands];
            //Bandwidth = new short[AmountOfBands];
        }

        public string ToReadableString()
        {
            return FilterStrTagConverter.ToReadableString(this);
        }

        public string ToEncodedString()
        {
            return FilterStrTagConverter.ToEncodedString(this);
        }
    }
}
