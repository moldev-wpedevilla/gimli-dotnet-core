﻿using System.Xml.Serialization;

namespace MD.Nfc.Basic.StrTagConverting
{
    public class CardTagData : TagData
    {
        [XmlIgnore] public IStrTagDataConverter<CardTagData> CardStrTagConverter { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Www { get; set; }

        public string ToReadableString(CardTagData tag)
        {
            return CardStrTagConverter.ToReadableString(this);
        }
    }
}
