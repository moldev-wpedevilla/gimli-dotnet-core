﻿namespace MD.Nfc.Basic.StrTagConverting
{
    public class PhoneTagData : TagData
    {
        public string Phone { get; set; }
    }
}
