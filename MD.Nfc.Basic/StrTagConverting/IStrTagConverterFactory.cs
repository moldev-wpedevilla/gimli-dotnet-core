﻿using System;

namespace MD.Nfc.Basic.StrTagConverting
{
    public interface IStrTagDataConverterFactory : IDisposable
    {
        TagData ConvertStrTag(string strTag);
    }
}
