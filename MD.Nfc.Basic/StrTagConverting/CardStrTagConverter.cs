﻿using System;

namespace MD.Nfc.Basic.StrTagConverting
{
    public class CardStrTagConverterFactory
    {
        public static IStrTagDataConverter<CardTagData> GenerateCardStrTagConverter()
        {
            return CardStrTagConverter.Generate();
        }
    }

    [Serializable]
    public class CardStrTagConverter : IStrTagDataConverter<CardTagData>
    {
        public static CardStrTagConverter Generate()
        {
            return new CardStrTagConverter();
        }

        protected CardStrTagConverter()
        {
        }

        public void Dispose()
        {   
        }

        public string ToEncodedString(CardTagData tag)
        {
            return "...";       // TODO
        }

        public string ToReadableString(CardTagData tag)
        {
            return $"4#{tag.Title}:{tag.Name}:{tag.Surname}:{tag.Company}:{tag.Phone}:{tag.Email}:{tag.Www}";
        }

        public CardTagData FromString(string strTag, string uid)
        {
            if (string.IsNullOrWhiteSpace(strTag)) return null;
            if (!strTag.StartsWith("4#")) return null;

            var strTagPart = strTag.Split('#');
            var cardDataStr = strTagPart[1];
            var cardPart = cardDataStr.Split(':');
            var wwwPart = -1 != cardPart[6].IndexOf('\0') ? cardPart[6].Substring(0, cardPart[6].IndexOf('\0')) : cardPart[6];

            var cardData = new CardTagData
            {
                Title = cardPart[0],
                Name = cardPart[1],
                Surname = cardPart[2],
                Company = cardPart[3],
                Phone = cardPart[4],
                Email = cardPart[5],
                Www = wwwPart,
                
                Uid = uid,

                IsValid = true
            };
            return cardData;
        }
    }
}
