﻿using System;

namespace MD.Nfc.Basic.StrTagConverting
{
    public class ProtocolStrTagConverterFactory
    {
        public static IStrTagDataConverter<ProtocolTagData> GenerateProtocolStrTagConverter()
        {
            return ProtocolStrTagConverter.Generate();
        }
    }

    [Serializable]
    public class ProtocolStrTagConverter : IStrTagDataConverter<ProtocolTagData>
    {
        public static ProtocolStrTagConverter Generate()
        {
            return new ProtocolStrTagConverter();
        }

        public void Dispose()
        {
        }

        public ProtocolTagData FromString(string strTag, string uid)
        {
            if (string.IsNullOrWhiteSpace(strTag)) return null;
            if (!strTag.StartsWith("5#")) return null;

            var strTagPart = strTag.Split('#');
            var protocolCand = -1 != strTagPart[1].IndexOf('\0') ? strTagPart[1].Substring(0, strTagPart[1].IndexOf('\0')) : strTagPart[1];

            var protocolData = new ProtocolTagData
            {
                GuidTag = new Guid(protocolCand),
                Uid = uid,
                IsValid = true
            };

            return protocolData;
        }

        public string ToString_Old(ProtocolTagData tag)
        {
            return $"5#{tag.GuidTag}";
        }

        public string ToEncodedString(ProtocolTagData tag)
        {
            return "";
        }

        public string ToReadableString(ProtocolTagData tag)
        {
            return "";      //"$"5#{tag.GuidTag}";
        }
    }
}
