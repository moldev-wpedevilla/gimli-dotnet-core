﻿using System.Text;

namespace MD.Nfc.Basic.StrTagConverting
{
    public static class ConvertingHelper
    {
        private const int MaxNameLength = 12;
        static public string NormalizeTagName(string name)
        {
            string resultName;
            int nameLength = 0;
            if (null == name)
            {
                resultName = "";
                nameLength = 0;
            }
            else
            {
                resultName = name.ToUpper();
                nameLength = name.Length <= MaxNameLength ? name.Length : MaxNameLength;
            }
            for (var mancoIndex = 0; mancoIndex < MaxNameLength - nameLength; mancoIndex++)
            {
                resultName += " ";
            }
            return resultName;
        }

        static public byte GetCharCode(char ch)
        {
            byte code = 0;
            if (ch >= 'A' && ch <= 'Z')
            {
                code = (byte)(ch - 65);             // A - 0 ... Z - 25
            }
            else if (ch >= '0' && ch <= '9')
            {
                code = (byte)(ch - 22);             // 0 - 26 ... 9 - 35
            }
            else if (' ' == ch)
            {
                code = (byte)36;                    // SPACE - 36
            }
            else if ('/' == ch)                     
            {
                code = (byte) 37;                   // '/' - 37
            }
            else if ('.' == ch)             
            {
                code = (byte) 38;                   // '.' - 38
            }
            // TODO 25 chars codes free
            return code;
        }

        static public char Decode(int code)
        {
            var ch = '\0';
            if (code >= 0 && code <= 25)                // 0 - A, 1 - B, ... 25 - Z
            {
                ch = (char)((byte)('A') + code);
            }
            else if (code >= 26 && code <= 35)          // 26 - 0, 27 - 1, ... 35 - 9
            {
                ch = (char)((byte)('0') + code - 26);
            }
            else if (36 == code)                        // 36 - space
            {
                ch = ' ';
            }
            else if (37 == code)                        // 37 - '/'
            {
                ch = '/';
            }
            else if (38 == code)                        // 38 - '.'
            {
                ch = '.';
            }
            return ch;
        }

        static public string GetPolarizationString(byte code)
        {
            string text = "";
            switch (code)
            {
                case 0:
                    text = "/";
                    break;
                case 1:
                    text = "V";
                    break;
                case 2:
                    text = "H";
                    break;
            }
            return text;
        }

        static public string GetTypeString(byte code)
        {
            string text = "";
            switch (code)
            {
                case 1:
                    text = "BP";
                    break;
                case 2:
                    text = "LP";
                    break;
                case 3:
                    text = "SP";
                    break;
                case 4:
                    text = "DL";
                    break;
                case 5:
                    text = "UNI";
                    break;
            }
            return text;
        }

        static public string GetMeasuringString(byte code)
        {
            var text = new StringBuilder();
            if (0x01 == (code & 0x01))
            {
                text.Append(0 == text.Length ? "Abs" : "|Abs");
            }
            if (0x01 == ((code >> 1) & 0x01))
            {
                text.Append(0 == text.Length ? "Lum" : "|Lum");
            }
            if (0x01 == ((code >> 2) & 0x01))
            {
                text.Append(0 == text.Length ? "FL" : "|FL");
            }
            if (0x01 == ((code >> 3) & 0x01))
            {
                text.Append(0 == text.Length ? "FP" : "|FP");
            }
            if (0x01 == ((code >> 4) & 0x01))
            {
                text.Append(0 == text.Length ? "TRF" : "|TRF");
            }
            return text.ToString();
        }

    }
}
