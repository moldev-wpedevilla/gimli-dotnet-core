﻿using System;

namespace MD.Nfc.Basic.StrTagConverting
{
    public class NtagPhoneStrTagConverterFactory
    {
        public static IStrTagDataConverter<PhoneTagData> GeneratePhoneStrTagConverter()
        {
            return NtagPhoneStrTagConverter.Generate();
        }
    }

    [Serializable]
    public class NtagPhoneStrTagConverter : IStrTagDataConverter<PhoneTagData>
    {
        public static NtagPhoneStrTagConverter Generate()
        {
            return new NtagPhoneStrTagConverter();
        }

        protected NtagPhoneStrTagConverter()
        {
        }

        public void Dispose()
        {
        }

        public string ToEncodedString(PhoneTagData tag)
        {
            return "...";
        }

        public string ToReadableString(PhoneTagData tag)
        {
            const string prefix = "\u0012?\u0001\u000eU\u0005";
            const string suffix = "?\u0010\u0012";   //"?\u0010\u0012\0?\u0010\u0012";
            return $"{prefix}{tag.Phone}{suffix}";
        }

        public PhoneTagData FromString(string strTag, string uid)
        {
            if (string.IsNullOrWhiteSpace(strTag)) return null;
            if (!strTag.StartsWith("\u0012?\u0001\u000eU\u0005")) return null;

            var strTagPart = strTag.Substring(strTag.IndexOf("\u0012?\u0001\u000eU\u0005", StringComparison.Ordinal) + "\u0012?\u0001\u000eU\u0005".Length);
            //var phoneCand = strTagPart.Substring(0, strTagPart.IndexOf("?\u0010\u0012\0?\u0010\u0012", StringComparison.Ordinal));
            var phoneCand = strTagPart.Substring(0, strTagPart.IndexOf("?\u0010\u0012", StringComparison.Ordinal));
            var phoneData = new PhoneTagData
            {
                Phone = phoneCand,
                Uid = uid,
                IsValid = true
            };

            return phoneData;
        }
    }
}
