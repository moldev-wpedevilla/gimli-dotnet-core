﻿using System;
using Castle.Windsor;
using Topshelf;

namespace Gimli.Shared.Actors
{
	public class GimliActorHostServiceFactory
	{
		public void Run(
			ServiceConfiguration setup,
			string serviceName,
			string serviceDisplayName,
			string serviceDescription,
			string configFilePath,
			Action<IWindsorContainer> additionalInstall = null)
		{
			Host = new GimliActorHost(additionalInstall, configFilePath);
			Host.Start(setup);
			Console.ReadLine();

			//HostFactory.Run(x =>
			//{
			//	x.Service<GimliActorHost>(s =>
			//	{
			//		s.ConstructUsing(() =>
			//		{
			//			Host = new GimliActorHost(additionalInstall, configFilePath);
			//			return Host;
			//		});
			//		s.WhenStarted(tc => tc.Start(setup));
			//		s.WhenStopped(tc => tc.Stop());
			//	});

			//	x.RunAsLocalSystem();
			//    x.StartManually();
			//	x.SetDescription(serviceDescription);
			//	x.SetDisplayName(serviceDisplayName);
			//	x.SetServiceName(serviceName);
			//});
		}

		public GimliActorHost Host { get; private set; }
	}
}