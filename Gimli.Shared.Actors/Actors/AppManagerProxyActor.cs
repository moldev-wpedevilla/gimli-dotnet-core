﻿using MolDev.Common.Akka.Interfaces;

namespace Gimli.Shared.Actors.Actors
{
	public class AppManagerProxyActor : ProxyActorBase
	{
		public AppManagerProxyActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			ServerKey = ActorServiceKey.AppManager;
			InitProxy(ServerKey);
		}
	}
}
