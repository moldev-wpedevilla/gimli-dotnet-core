﻿using System;
using System.Collections.Generic;

namespace Gimli.Shared.Actors.Actors
{
	public enum InitFinishedState
	{
		None,
		Initializing,
		FinishedWithSuccess,
		FinishedWithError,
	}

	public enum InitLevel
	{
		L0_Starting,
		L1_SystemStarted,
		L2_ProxysLive,
		L3_Operational,
		L4_Initialized,
		L100_WaitASecond,
		L101_ReinitProxies,
	}

	public enum ShutdownForbiddenReason
	{
		None,
		InstrumentCommandsBeingExecuted,
		WritingToFilesystem,
	}

	public class ReasonIdentifier : IDisposable
	{
		private readonly List<ReasonIdentifier> mAllReasons;

		public ReasonIdentifier(List<ReasonIdentifier> allReasons, ShutdownForbiddenReason specificReason)
		{
			mAllReasons = allReasons;
			SpecificReason = specificReason;
		}

		public ShutdownForbiddenReason SpecificReason { get; set; }

		public void Dispose()
		{
			if (mAllReasons == null) return;
			if (!mAllReasons.Contains(this)) return;
			mAllReasons.Remove(this);
		}
	}

	public interface IActorsystemOverallState
	{
		bool IsShutdownAllowed { get; }
		List<ReasonIdentifier> ShutdownForbiddenReasons { get; }

		ReasonIdentifier DisableShutdown(ShutdownForbiddenReason reason);

		InitFinishedState InitState { get; set; }
		InitLevel InitLevel { get; set; }

		ActorServiceKey ServiceId { get; set; }
		bool WasRestarted { get; set; }
	}

	public class ActorsystemOverallState : IActorsystemOverallState
	{
		public bool IsShutdownAllowed => ShutdownForbiddenReasons.Count == 0;

		public List<ReasonIdentifier> ShutdownForbiddenReasons { get; } = new List<ReasonIdentifier>();

		public ReasonIdentifier DisableShutdown(ShutdownForbiddenReason reason)
		{
			var reasonIdentifier = new ReasonIdentifier(ShutdownForbiddenReasons, reason);
			ShutdownForbiddenReasons.Add(reasonIdentifier);
			return reasonIdentifier;
		}

		public InitFinishedState InitState { get; set; }
		public InitLevel InitLevel { get; set; }
		public ActorServiceKey ServiceId { get; set; }
		public bool WasRestarted { get; set; }
	}
}
