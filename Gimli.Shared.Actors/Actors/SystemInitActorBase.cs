﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.Shared.Actors.Actors
{
	public abstract class SystemInitActorBase : GimliActorBase
	{
		protected List<InitLevel> LevelList = new List<InitLevel> { InitLevel.L0_Starting, InitLevel.L1_SystemStarted, InitLevel.L2_ProxysLive, InitLevel.L3_Operational };
		protected Dictionary<InitLevel, Func<bool>> LevelActions = new Dictionary<InitLevel, Func<bool>>();

		protected SystemInitActorBase(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			LevelActions[InitLevel.L1_SystemStarted] = () => L1_StandardImplementation(systemState);

			LevelActions[InitLevel.L2_ProxysLive] = () => L2_StandardImplementation(systemState);

			LevelActions[InitLevel.L3_Operational] = () => L3_StandardImplementation(systemState);

			LevelActions[InitLevel.L100_WaitASecond] = () =>
			{
				Thread.Sleep(1000);
				return true;
			};

			LevelActions[InitLevel.L101_ReinitProxies] = () =>
			{
				systemState.InitState = InitFinishedState.Initializing;

				var success = InitProxies(actorService);

				var reinitSuccess = success ? InitFinishedState.FinishedWithSuccess : InitFinishedState.FinishedWithError;

				TellAppManagerInitStateLevel(InitLevel.L101_ReinitProxies, reinitSuccess);
				return true;
			};

			Receive<RequestMessage<InitSystemMessage>>(message =>
			{
				foreach (var initLevel in message.Model.TargetLevels)
				{
					LevelActions[initLevel]();
				}
			});
		}

		protected virtual bool L1_StandardImplementation(IActorsystemOverallState systemState)
		{
			systemState.InitState = InitFinishedState.FinishedWithSuccess;
			systemState.InitLevel = InitLevel.L1_SystemStarted;

			IsAppManagerRunningOrDie(ActorService, systemState, GetGuaranteedAnswer, s => Log.Error(s));

			TellAppManagerInitStateLevel(systemState.InitLevel, systemState.InitState);
			return true;
		}

		protected virtual bool L2_StandardImplementation(IActorsystemOverallState systemState)
		{
			systemState.InitState = InitFinishedState.Initializing;

			var success = LetAssociateAndInitProxies(InitProxies, ActorService);

			systemState.InitState = success ? InitFinishedState.FinishedWithSuccess : InitFinishedState.FinishedWithError;
			systemState.InitLevel = InitLevel.L2_ProxysLive;

			TellAppManagerInitStateLevel(systemState.InitLevel, systemState.InitState);
			return true;
		}

		protected bool InitInstrumentCoreNetworkProxies(IActorService actorService, bool success)
		{
			success = success && IsProxyConnected<InstrumentServiceProxyActor>(actorService, GetGuaranteedAnswer);
			success = success && IsProxyConnected<CoreServiceProxyActor>(actorService, GetGuaranteedAnswer);
			success = success && IsProxyConnected<NetworkCommunicationServiceProxyActor>(actorService, GetGuaranteedAnswer);
			return success;
		}

		protected virtual bool L3_StandardImplementation(IActorsystemOverallState systemState)
		{
			systemState.InitState = InitFinishedState.FinishedWithSuccess;
			systemState.InitLevel = InitLevel.L3_Operational;

			TellAppManagerInitStateLevel(systemState.InitLevel, systemState.InitState);
			return true;
		}

		public static void IsAppManagerRunningOrDie(IActorService actorService, IActorsystemOverallState systemState, Func<Func<InitSystemMessage, GimliResponse>, InitSystemMessage, GimliResponse> getGuaranteedAnswer, Action<string> errorReporter)
		{
			if (RemoteActorConfiguration.Instance.ServiceConfigurations[systemState.ServiceId].DoSelfInit) return;

			var appManagerInitSuccess = LetAssociateAndInitProxies(a => IsProxyConnected<AppManagerProxyActor>(a, getGuaranteedAnswer), actorService);

			if (appManagerInitSuccess) return;

			errorReporter("################ Unable to communicate with appManager: killing myself and let watchdog restart me. #################");

			// Give logger some time
			Thread.Sleep(1000);

			// Kill myself
			Process.GetCurrentProcess().Kill();
		}

		protected static bool LetAssociateAndInitProxies(Func<IActorService, bool> initFunc, IActorService actorService)
		{
			var firstRoundSucceeded = initFunc(actorService);

			if (firstRoundSucceeded) return true;

			// after an invalid association the address is gated for 5 sec
			Thread.Sleep(5100);

			return initFunc(actorService);
		}

		protected abstract bool InitProxies(IActorService actorService);

		protected static bool IsProxyConnected<T>(IActorService actorService, Func<Func<InitSystemMessage, GimliResponse>, InitSystemMessage, GimliResponse> getGuaranteedAnswer) 
			where T : ProxyActorBase
		{
			return getGuaranteedAnswer(m =>
			{
				var task = actorService.Get<T>()
					.Ask<GimliResponse>(MessageExtensions.CreateRequestMessage(new IsServiceRunningMessage()));
				task.Wait(1600);
				return task.Result;
			}, null).ResponseStatus == ResultStatusEnum.Success;
		}
	}
}
