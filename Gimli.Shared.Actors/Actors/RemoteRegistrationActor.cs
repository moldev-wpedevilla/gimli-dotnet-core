﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Castle.Core.Internal;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.Shared.Actors.Actors
{
	public class RemoteRegistrationActor : GimliActorBase
	{
		private readonly Dictionary<string, IActorRef> mConnectionStore = new Dictionary<string, IActorRef>();

		protected List<string> Subscribers = new List<string>();

		public RemoteRegistrationActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
		}

		protected virtual GimliResponse HandleRemoteRegistrationRequest(RequestMessage<RemoteRegistrationMessage> message)
		{
			if (message.Model.ConnectionString == HelperMethods.GetLocalHostName())
			{
				return new GimliResponse();
			}

			if (!Subscribers.Contains(message.Model.ConnectionString))
			{
				Subscribers.Add(message.Model.ConnectionString);
			}

			return new GimliResponse();
		}

		/// <summary>
		/// Submits a message to multiple remote machines and returns a success message.
		/// </summary>
		/// <param name="localActor"></param>
		/// <param name="message"></param>
		/// <param name="desiredSuccessLevel"></param>
		/// <param name="timeout"></param>
		/// <param name="routees"></param>
		/// <returns></returns>
		protected GimliResponse PublishToAll(ICanTell localActor, RequestMessageBase message, TellSuccessLevel desiredSuccessLevel, int timeout, List<string> routees = null)
		{
			var toBeRoutedTo = routees ?? Subscribers;
			GimliResponse errorResponse;
			if (CheckSubscribers(localActor, message, toBeRoutedTo, out errorResponse)) return errorResponse;

			foreach (var subscriber in toBeRoutedTo.ToList())
			{
				var actor = GetRemoteActorRef(subscriber, 2);
				if (actor == null)
				{
					Log.Info("Remove subscriber: " + subscriber);
					toBeRoutedTo.Remove(subscriber);
				}
			}

			if (CheckSubscribers(localActor, message, toBeRoutedTo, out errorResponse)) return errorResponse;

			var newSessionGuid = Guid.NewGuid();
			var answerAccumulator = Context.ActorOf(Props.Create(() => new AnswerAccumulatorActor(ActorService, SystemState)), newSessionGuid.ToString());
			var accumulationFinished = answerAccumulator.Ask<GimliResponse>(
				MessageExtensions.CreateRequestMessage(new DistributedRequestStartMessage
				{
					DesiredSuccessLevel = desiredSuccessLevel,
					TotalResponsesExpected = toBeRoutedTo.Count + (localActor == null ? 0 : 1),
					SelfDistuctionTimerInSeconds = timeout
				}));

			localActor?.Tell(message, answerAccumulator);
			foreach (var subscriber in toBeRoutedTo)
			{
				ForwardToCertain(message, subscriber, answerAccumulator);
			}

			accumulationFinished.Wait();
			return accumulationFinished.Result;
		}

		private bool CheckSubscribers(ICanTell localActor, RequestMessageBase message, List<string> toBeRoutedTo, out GimliResponse errorResponse)
		{
			if (toBeRoutedTo.Count < 1 && localActor == null)
			{
				{
					errorResponse =
						new GimliResponse(
							new ErrorInfo
							{
								Message = $"PublishToAll: No routees and no local actor for message: {message} in {GetType().Name}."
							},
							ResultStatusEnum.Error, Log);
					return true;
				}
			}

			errorResponse = null;
			return false;
		}

		protected GimliResponse AskServiceRunning(string hostname, int timeoutInMs, IActorRef resolvedActor = null)
		{
			return GetGuaranteedAnswer(m =>
			{
				var connectionString = hostname.IsNullOrEmpty() ? resolvedActor?.Path.ToString() : hostname;
				Func<Exception, GimliResponse> errorCreator = e => new GimliResponse(
					new ErrorInfo
					{
						Message = $"AskServiceRunning: No remote actor was returned as identity in {GetType().Name} and remote: {connectionString} in {timeoutInMs} ms. Exception: {e}"
					}, ResultStatusEnum.Error, Log);

				Task<GimliResponse> askTask;
				if (resolvedActor == null)
				{
					var selection = Context.ActorSelection(connectionString);
					askTask = selection.Ask<GimliResponse>(MessageExtensions.CreateRequestMessage(new IsServiceInitializedMessage()), TimeSpan.FromMilliseconds(timeoutInMs));
				}
				else
				{
					askTask = resolvedActor.Ask<GimliResponse>(MessageExtensions.CreateRequestMessage(new IsServiceInitializedMessage()), TimeSpan.FromMilliseconds(timeoutInMs));
				}

				try
				{
					askTask.Wait();
				}
				catch (Exception e)
				{
					return errorCreator(e);
				}

				return askTask.Result.ResponseStatus == ResultStatusEnum.Success ? new GimliResponse() : errorCreator(null);
			}, null as IsServiceRunningMessage);
		}

		protected bool ForwardToCertain(object message, string host, IActorRef sender)
		{
			try
			{
				var connectionString = host;
				var actor = GetRemoteActorRef(connectionString, 5);
				if (actor == null)
				{
					Log.Error($"No remote actor returned in {GetType().Name} for message {message.GetType()} and host {host}.");
					return false;
				}

				actor.Tell(message, sender);
				return true;
			}
			catch (Exception e)
			{
				Log.Error($"Remote forwarding failed in {GetType().Name} for message {message.GetType()} and host {host} {e}");
			}

			return false;
		}

		/// <summary>
		/// Made virtual for testing purpose only
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="numberOfRetries"></param>
		/// <returns></returns>
		protected virtual IActorRef GetRemoteActorRef(string connectionString, int numberOfRetries)
		{
			for (var i = 0; i < numberOfRetries; i++)
			{
				var actor = mConnectionStore.ContainsKey(connectionString)
					? mConnectionStore[connectionString]
					: ResolveRemoteActor(connectionString);

				if (actor == null)
				{
					RemoveActorFromDictionary(connectionString);
					continue;
				}

				var isRunning = AskServiceRunning(null, 1500, actor).ResponseStatus == ResultStatusEnum.Success;
				if (isRunning) return actor;

				RemoveActorFromDictionary(connectionString);
			}

			return null;
		}

		private void RemoveActorFromDictionary(string connectionString)
		{
			if (mConnectionStore.ContainsKey(connectionString))
			{
				mConnectionStore.Remove(connectionString);
			}
		}

		private IActorRef ResolveRemoteActor(string connectionString)
		{
			IActorRef actor = null;
			try
			{
				var selection = Context.ActorSelection(connectionString);
				var task = selection.ResolveOne(TimeSpan.FromMilliseconds(1500));
				// DO NOT REMOVE wait timeout !!!!!!!!
				task.Wait(1600);
				actor = task.Result;
				if (actor != null) mConnectionStore[connectionString] = actor;
			}
			catch (Exception e)
			{
				Log.Error($"Error resolving {connectionString}: " + e);
			}

			return actor;
		}

		protected GimliResponse AskCertainRemote<T>(string toConnect, RequestMessageBase message, int resolveTimeoutSec, int askTimeoutSec) where T: GimliResponse
		{
			return GetGuaranteedAnswer(m =>
			{
				var actorRef = GetRemoteActorRef(toConnect, resolveTimeoutSec);
				if (actorRef == null) return new GimliResponse(new ErrorInfo {Details = "Resolving failed. No actor"}, ResultStatusEnum.Error, Log);
				var responseTask = actorRef.Ask<T>(m, TimeSpan.FromSeconds(askTimeoutSec));
				responseTask.Wait();
				return responseTask.IsFaulted
					? new GimliResponse(new ErrorInfo {Details = $"Resolving failed with {responseTask.Exception?.ToString()}"},
						ResultStatusEnum.Error, Log)
					: responseTask.Result;
			}, message);
		}

		public static string GetRemoteServiceConnectionString(ServiceConfiguration config, string machine)
		{
			return $"akka.tcp://{config.ServiceName}@{machine}:{config.ServicePort}/user/{config.TopLevelRouter}";
		}
	}
}
