﻿using MolDev.Common.Akka.Interfaces;

namespace Gimli.Shared.Actors.Actors
{
	public class WebServiceProxyActor : ProxyActorBase
	{

		public WebServiceProxyActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			ServerKey = ActorServiceKey.Webservice;
			InitProxy(ServerKey);
		}
	}
}
