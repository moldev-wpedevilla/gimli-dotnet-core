﻿using MolDev.Common.Akka.Interfaces;

namespace Gimli.Shared.Actors.Actors
{
	public class CoreServiceProxyActor : ProxyActorBase
	{

		public CoreServiceProxyActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			ServerKey = ActorServiceKey.Core;
			InitProxy(ServerKey);
		}
	}
}
