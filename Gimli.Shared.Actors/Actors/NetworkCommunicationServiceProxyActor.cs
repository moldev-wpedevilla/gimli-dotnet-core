﻿using MolDev.Common.Akka.Interfaces;

namespace Gimli.Shared.Actors.Actors
{
	public class NetworkCommunicationServiceProxyActor : ProxyActorBase
	{
		public NetworkCommunicationServiceProxyActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			ServerKey = ActorServiceKey.Network;
			InitProxy(ServerKey);
		}
	}
}
