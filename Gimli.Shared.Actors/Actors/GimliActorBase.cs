﻿using System;
using System.Linq;
using Akka.Actor;
using Akka.Event;
using Gimli.Shared.Actors.AppStarter;
using Gimli.Shared.Actors.Messages;
using MolDev.ActorHost;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages.Errors;

namespace Gimli.Shared.Actors.Actors
{
	public class GimliActorBase : ActorReceiveBase
	{
		protected readonly IActorsystemOverallState SystemState;

		public GimliActorBase(IActorService actorService, IActorsystemOverallState systemState) : base(actorService)
		{
			SystemState = systemState;
		}

		/// <summary>
		/// To be able to make actors that create children unit-testable, this method can be overrided.
		/// </summary>
		/// <typeparam name="TActor"></typeparam>
		/// <param name="name"></param>
		/// <param name="createIfMissing"></param>
		/// <param name="createMessage"></param>
		/// <param name="forward"></param>
		/// <param name="useOnes"></param>
		/// <returns></returns>
		protected new virtual IActor GetChild<TActor>(string name, bool createIfMissing = true, object createMessage = null,
			bool forward = false, bool useOnes = false) where TActor : class
		{
			return base.GetChild<TActor>(name, createIfMissing, createMessage, forward, useOnes);
		}

		/// <summary>
		/// Do not let function create any async-await constructs or the try-catch block is left before it can catch the exception.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="function"></param>
		/// <param name="message"></param>
		/// <returns></returns>
		protected GimliResponse GetGuaranteedAnswer<T>(Func<T, GimliResponse> function, T message)
		{
			return GetGuaranteedAnswer(function, message, s => Log.Error(s), GetType().Name);
		}

		public static GimliResponse GetGuaranteedAnswer<T>(Func<T, GimliResponse> function, T message, Action<string> errorLogger,
			string name)
		{
			GimliResponse response;
			try
			{
				response = function(message);
			}
			catch (Exception e)
			{
				response = new GimliResponse(new ErrorInfo { Details = $"Exception thrown in {name} for message {message}: {e}" }, ResultStatusEnum.Error, logger2: errorLogger);
			}

			return response;
		}

		protected void TellAppManagerInitStateLevel(InitLevel level, InitFinishedState state)
		{
			Log.Info($"Reached init level: {level} and state {state}.");

			if (RemoteActorConfiguration.Instance.ServiceConfigurations[SystemState.ServiceId].DoSelfInit)
			{
				Log.Info("Selfint -> AppManager not informed");
				return;
			}

			ActorService.Get<AppManagerProxyActor>()
				.Tell(MessageExtensions.CreateRequestMessage(ServiceInitFinishedMessage.Generate(level, state, CentralActorConfiguration.Instance.ServiceConfigurations[SystemState.ServiceId].TopLevelRouter)));
		}

		protected void AnswerIsShutdownAllowed(IActorsystemOverallState systemState)
		{
			Sender.Tell(GetGuaranteedAnswer(m => 
			{
				var response = new IsShutdownAllowedResponseMessage();
				response.ShutdownForbiddenReasons.AddRange(systemState.ShutdownForbiddenReasons.Select(r => r.SpecificReason));
				Log.Info("Shutdown allowed: " + response.IsShutdownAllowed);
				return response;
			}, string.Empty));
		}
	}
}
