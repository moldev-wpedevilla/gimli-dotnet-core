﻿using System.Collections.Generic;
using Gimli.Shared.Actors.Messages;
using Akka.Actor;
using Gimli.Shared.Actors.AppStarter;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Actors
{
	public abstract class ToplevelRoutingActorBase : GimliActorBase
	{
		protected ToplevelRoutingActorBase(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			systemState.InitState = InitFinishedState.Initializing;
			Receive<RequestMessage<IsServiceInitializedMessage>>(message =>
			{
				Sender.Tell(ServiceInitFinishedMessage.Generate(systemState.InitLevel, systemState.InitState, CentralActorConfiguration.Instance.ServiceConfigurations[systemState.ServiceId].TopLevelRouter));
			});

			Receive<RequestMessage<IsShutdownAllowedMessage>>(message =>
			{
				AnswerIsShutdownAllowed(SystemState);
			});

			Receive<RequestMessage<InitSystemMessage>>(message => GetSysInitActor().Forward(message));

			Receive<SelfInitMessage>(s =>
			{
				var selfIinit = RemoteActorConfiguration.Instance.ServiceConfigurations[systemState.ServiceId].DoSelfInit;
				var selfIinitL2Delay = int.Parse(AppConfigurationManager.GetSettingsValue("SelfInitL2Delay"));
				var selfinitList = new List<InitLevel>
				{
					InitLevel.L1_SystemStarted,
					InitLevel.L2_ProxysLive,
					InitLevel.L3_Operational
				};

				for (var i = 0; i < selfIinitL2Delay; i++)
				{
					selfinitList.Insert(1, InitLevel.L100_WaitASecond);
				}

				GetSysInitActor().Tell(
					MessageExtensions.CreateRequestMessage(new InitSystemMessage
					{
						TargetLevels = selfIinit ? selfinitList.ToArray() : new[] { InitLevel.L1_SystemStarted }
					}));
			});

			Self.Tell(new SelfInitMessage());
		}

		protected abstract IActor GetSysInitActor();

		protected override void PreStart()
		{
			GetSysInitActor();

			base.PreStart();
		}
	}
}
