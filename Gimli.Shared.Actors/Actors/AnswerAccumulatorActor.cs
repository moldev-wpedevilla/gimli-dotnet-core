﻿using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.Shared.Actors.Actors
{
	public class AnswerAccumulatorActor : GimliActorBase
	{
		private readonly List<GimliResponse> mResponses = new List<GimliResponse>();

		private int mTotalResponsesExpected;
		private TellSuccessLevel mDesiredSuccessLevel;
		private IActorRef mSender;
		private bool mAnswerSent;
		private bool mInitialized;

		public AnswerAccumulatorActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			Func<GimliResponse> createErrorResponse = ()=> new GimliResponse(new ErrorInfo {Message = "PublishToAll failed for desired success level: " + mDesiredSuccessLevel}, ResultStatusEnum.Error, Log);

			Receive<RequestMessage<DistributedRequestStartMessage>>(message =>
			{
				mTotalResponsesExpected = message.Model.TotalResponsesExpected;
				mDesiredSuccessLevel = message.Model.DesiredSuccessLevel;
				mSender = Sender;

				SetReceiveTimeout(TimeSpan.FromSeconds(message.Model.SelfDistuctionTimerInSeconds));
				mInitialized = true;

				if (mAnswerSent) return;

				foreach (var gimliResponse in mResponses)
				{
					CheckForLocalSuccess(gimliResponse, createErrorResponse);
				}

				// in case Responses arrive before DistributedRequestStartMessage
				if (mResponses.Count < mTotalResponsesExpected)
				{
					return;
				}

				TellSuccess(createErrorResponse);
			});

			Receive<ReceiveTimeout>(message =>
			{
				if (!mAnswerSent)
				{
					mSender.Tell(createErrorResponse());
				}

				StopSelf();
			});

			Receive<GimliResponse>(message =>
			{
				if (mAnswerSent) return;

				mResponses.Add(message);

				if (!mInitialized || mResponses.Count < mTotalResponsesExpected)
				{
					return;
				}

				CheckForLocalSuccess(message, createErrorResponse);

				TellSuccess(createErrorResponse);
			});
		}

		private void CheckForLocalSuccess(GimliResponse message, Func<GimliResponse> createErrorResponse)
		{
			if (message.ResponseStatus != ResultStatusEnum.Success &&
				message.OriginHostname == HelperMethods.GetLocalHostName())
			{
				SendAnswer(createErrorResponse());
			}

			if (mDesiredSuccessLevel != TellSuccessLevel.AtLeastLocal ||
				message.ResponseStatus != ResultStatusEnum.Success ||
				message.OriginHostname != HelperMethods.GetLocalHostName())
			{
				return;
			}

			SendAnswer(message);
		}

		private void TellSuccess(Func<GimliResponse> createErrorResponse)
		{
			var responsesSuccess = mResponses.All(r => r.ResponseStatus == ResultStatusEnum.Success)
				? TellSuccessLevel.All
				: TellSuccessLevel.AtLeastLocal;

			var orderedLevelList = new List<TellSuccessLevel> {TellSuccessLevel.All, TellSuccessLevel.AtLeastLocal};

			var answer = orderedLevelList.IndexOf(responsesSuccess) > orderedLevelList.IndexOf(mDesiredSuccessLevel)
				? createErrorResponse()
				: new RemotingSuccessResponseMessage {NumberOfSuccesses = mResponses.Count(response => response.ResponseStatus == ResultStatusEnum.Success)};

			SendAnswer(answer);
		}

		private void SendAnswer(GimliResponse answer)
		{
			mAnswerSent = true;
			mSender.Tell(answer);
		}
	}
}
