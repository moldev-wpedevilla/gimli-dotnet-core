﻿using MolDev.Common.Akka.Interfaces;

namespace Gimli.Shared.Actors.Actors
{
	public class DataServiceProxyActor : ProxyActorBase
	{

		public DataServiceProxyActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			ServerKey = ActorServiceKey.Data;
			InitProxy(ServerKey);
		}
	}
}
