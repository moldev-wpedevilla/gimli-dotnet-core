﻿using MolDev.Common.Akka.Interfaces;

namespace Gimli.Shared.Actors.Actors
{
	public class InstrumentServiceProxyActor : ProxyActorBase
	{
		public InstrumentServiceProxyActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			ServerKey = ActorServiceKey.InstrumentServer;
			InitProxy(ServerKey);
		}
	}
}
