﻿using System.Linq;
using Akka.Actor;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.Shared.Actors.Actors
{
	public class ProxyActorBase : RemoteRegistrationActor
	{
		private ServiceConfiguration mConfig;

		protected ActorServiceKey ServerKey;

		public ProxyActorBase(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
		}

		protected void InitProxy(ActorServiceKey service)
		{
			mConfig = RemoteActorConfiguration.Instance.ServiceConfigurations[service];
			Subscribers.Insert(0, GetRemoteServiceConnectionString(mConfig,
				service == ActorServiceKey.Network
					? HelperMethods.GetIPAddressList().First()
					: HelperMethods.GetLocalHostName()));

			Receive<RequestMessage<IsServiceRunningMessage>>(message =>
			{
				var sender = Sender;

				UpdateSubscriber(service);

				// Do not HandleServiceNotResponding in case of != Success: this can be done by the sender!
				var answer = AskServiceRunning(Subscribers.FirstOrDefault(), 1500);
				sender.Tell(answer);
			});

			ReceiveAny(message =>
			{
				if (message is RequestMessage<IsServiceRunningMessage>) return;

				UpdateSubscriber(service);

				ForwardToMaster(message);
			});
		}

		private void UpdateSubscriber(ActorServiceKey service)
		{
			if (service == ActorServiceKey.Network)
			{
				Subscribers[0] = GetRemoteServiceConnectionString(mConfig, HelperMethods.GetIPAddressList().First());
			}
		}

		private void ForwardToMaster(object message)
		{
			var sender = Sender;
			var succeeded = ForwardToCertain(message, Subscribers.FirstOrDefault(), sender);
			if (succeeded) return;

			var errorMessage = $"!!!!!!!!!!!!!!!!! {GetType()} could not connect to server {Subscribers[0]} for message {message.GetType()}.";
			sender.Tell(new GimliResponse(new ErrorInfo {Message = errorMessage}, ResultStatusEnum.Error, Log));
		}
	}
}
