﻿using System;
using System.Collections.Generic;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class GetInputForReadResponseMessage : GimliResponse
	{
		public List<Document> DocumentsToStart { get; set; }

	}
}
