﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class TemperatureChangedMessage
	{
		public TemperatureNotification Settings { get; set; }
	}
}
