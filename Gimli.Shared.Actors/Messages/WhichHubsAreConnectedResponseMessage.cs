﻿using System;
using System.Collections.Generic;

namespace Gimli.Shared.Actors.Messages
{
	
	public class WhichHubsAreConnectedResponseMessage : GimliResponse
	{
		public List<string> Clients { get; set; }
		public List<string> Hubs { get; set; }
	}
}
