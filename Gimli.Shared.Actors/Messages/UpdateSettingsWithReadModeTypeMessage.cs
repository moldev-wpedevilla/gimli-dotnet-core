﻿using System;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Settings;

namespace Gimli.Shared.Actors.Messages
{
	
	public class UpdateSettingsWithReadModeTypeMessage
	{
		public MeasurementSettings SnapshotSettings { get; set; }
		public Mode DesiredReadMode { get; set; }
		public MeasurementType DesiredReadType { get; set; }
	}
}
