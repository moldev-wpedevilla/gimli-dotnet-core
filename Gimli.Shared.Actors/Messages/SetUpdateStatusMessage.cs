﻿using System;

namespace Gimli.Shared.Actors.Messages
{
    
    public class SetUpdateStatusMessage
    {
        public double updateStatusPercentage { get; set; }
        public string updateStatusText { get; set; }
    }
}
