﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
    
    public class ValidateUserFullMessage
    {
        public UserFull UserFull { get; set; }
    }
}
