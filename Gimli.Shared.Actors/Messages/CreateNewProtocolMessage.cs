﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class CreateNewProtocolMessage
	{
		public string Name { get; set; }
		public Mode ReadMode { get; set; }
	}
}
