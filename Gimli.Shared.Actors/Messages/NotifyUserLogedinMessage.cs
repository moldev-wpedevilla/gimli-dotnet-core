﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class NotifyUserLogedinMessage
	{
		public UserFull User { get; set; }
	}
}
