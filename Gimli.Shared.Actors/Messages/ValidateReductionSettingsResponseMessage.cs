﻿using System;
using Gimli.JsonObjects.Settings;

namespace Gimli.Shared.Actors.Messages
{
	
	public class ValidateReductionSettingsResponseMessage : GimliResponse
	{
		public DataReductionSettings Settings { get; set; }
	}
}
