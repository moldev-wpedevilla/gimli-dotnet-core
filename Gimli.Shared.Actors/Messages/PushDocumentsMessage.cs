﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class PushDocumentsMessage
	{
		public Guid[] Documents { get; set; }
	}
}
