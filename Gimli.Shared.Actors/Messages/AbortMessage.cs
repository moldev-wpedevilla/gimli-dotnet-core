﻿using System;
using Gimli.Shared.Actors.Messages.Instrument;

namespace Gimli.Shared.Actors.Messages
{
	
	public class AbortMessage : EventlisternBaseMessage
	{
		public bool DuringRead { get; set; }
		public string Identity { get; set; }
	}
}
