﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
    
	public class DocumentResponseMessage : GimliResponse
	{
		public Document Document { get; set; }
	}
}
