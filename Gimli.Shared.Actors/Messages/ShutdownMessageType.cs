﻿namespace Gimli.Shared.Actors.Messages
{
	public enum ShutdownMessageType
	{
		None = 0,
		Request,
		Notification,
		Confirmation,
		Denial
	}
}
