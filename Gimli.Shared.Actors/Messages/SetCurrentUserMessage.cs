﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
    
    public class SetCurrentUserMessage
    {
        public string Username { get; set; }
        public Guid Id { get; set; }
    }
}
