﻿using System;
using System.Collections.Generic;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors
{
    
    public class GetUsersResponseMessage : GimliResponse
    {
        public List<UserFull> Users { get; set; }
    }
}
