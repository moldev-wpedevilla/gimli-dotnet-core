﻿using System;

namespace Gimli.Shared.Actors.Messages.OperatingSystem
{
    
    public class PrepareDirectoryMessage
    {
        public bool DeleteIfExists { get; set; } = false;
        public bool DirectoryHidden { get; set; } = false;
        public string DirectoryName { get; set; }
    }
}
