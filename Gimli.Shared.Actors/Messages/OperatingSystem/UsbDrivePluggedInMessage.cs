﻿using System;

namespace Gimli.Shared.Actors.Messages.OperatingSystem
{
    
    public class UsbDrivePluggedInMessage
    {
        public bool InitialCheck { get; set; }
        public bool DrivesAdded { get; set; }
        public bool ValidDrive { get; set; }
    }
}
