﻿using System;

namespace Gimli.Shared.Actors.Messages.OperatingSystem
{   
    
    public class PrepareDirectoryResponseMessage : GimliResponse
    {
        public bool Success { get; set; }
    }
}
