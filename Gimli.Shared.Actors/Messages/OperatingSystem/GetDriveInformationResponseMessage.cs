﻿using System;

namespace Gimli.Shared.Actors.Messages.OperatingSystem
{
    
    public class DriveInformation
    {
        public ulong SpaceTotal { get; set; }
        public ulong SpaceFree { get; set; }
        public string FileSystem { get; set; }
        public bool FileSystemCompatible { get; set; }
        public bool Writeable { get; set; }
    }

    
    public class GetDriveInformationResponseMessage : GimliResponse
    {
        public DriveInformation DriveInformation { get; set; }
    }
}
