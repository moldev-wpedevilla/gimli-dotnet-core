﻿using System;
using System.Collections.Generic;

namespace Gimli.Shared.Actors.Messages.OperatingSystem
{
    
    public class GetPluggedInUsbDrivesResponseMessage : GimliResponse
    {
        public bool HasPluggedInDrives => CountDrives > 0;
        public int CountDrives => DriveLetters.Count; 
        public List<string> DriveLetters { get; set; }
        public List<string> LastDriveLetters { get; set; }
    }
}
