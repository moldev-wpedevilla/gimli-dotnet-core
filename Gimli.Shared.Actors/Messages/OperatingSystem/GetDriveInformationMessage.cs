﻿using System;

namespace Gimli.Shared.Actors.Messages.OperatingSystem
{
    
    public class GetDriveInformationMessage
    {
        public string DriveLetter { get; set; }
    }
}
