﻿using System;

namespace Gimli.Shared.Actors.Messages.OperatingSystem
{
    
    public class CopyFilesResponseMessage : GimliResponse
    {
        public bool Success { get; set; }
    }
}
