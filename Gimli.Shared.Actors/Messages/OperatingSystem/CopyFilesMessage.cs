﻿using System;
using System.IO;

namespace Gimli.Shared.Actors.Messages.OperatingSystem
{
    
    public class CopyFilesMessage
    {
        public bool FilesHidden { get; set; } = false;
        public DirectoryInfo Source { get; set; }
        public DirectoryInfo Target { get; set; }
        public string[] Filenames { get; set; }
    }
}
