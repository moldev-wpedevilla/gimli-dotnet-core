﻿using System;

namespace Gimli.Shared.Actors.Messages.OperatingSystem
{
    public class BatteryStatus
    {
        public bool DisplayScheduleNotification { get; set; }
        public bool BatteryDead { get; set; }
        public DateTime DateTimeChanged { get; set; }
        public DateTime DateTimeScheduled { get; set; }
    }

    
    public class CheckBatteryStatusResponseMessage : GimliResponse
    {
        public BatteryStatus BatteryStatus { get; set; }
    }
}
