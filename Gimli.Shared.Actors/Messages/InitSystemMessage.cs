﻿using System;
using Gimli.Shared.Actors.Actors;

namespace Gimli.Shared.Actors.Messages
{
	
	public class InitSystemMessage
	{
		public InitLevel[] TargetLevels { get; set; }
	}
}
