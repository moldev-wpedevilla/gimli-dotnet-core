﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class UpdateDocumentWithReductionSettingsMessage
	{
		public SnapshotReductionSettings Settings { get; set; }
	}
}
