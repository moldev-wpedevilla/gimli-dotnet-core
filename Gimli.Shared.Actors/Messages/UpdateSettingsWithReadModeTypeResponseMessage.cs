﻿using System;
using Gimli.JsonObjects.Settings;

namespace Gimli.Shared.Actors.Messages
{
	
	public class UpdateSettingsWithReadModeTypeResponseMessage: GimliResponse
	{
		public MeasurementSettings SnapshotSettings { get; set; }
	}
}
