﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
    
    public class SetShakeSettingsMessage
    {
	    public ShakeSettingsSetInfo Settings { get; set; }
	    public bool Unlock { get; set; }
		public bool GotoRead { get; set; }
    }
}