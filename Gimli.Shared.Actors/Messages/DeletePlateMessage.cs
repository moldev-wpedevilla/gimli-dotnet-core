﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class DeletePlateMessage
	{
		public Guid DocumentId { get; set; }
		public Guid PlateId { get; set; }
	}
}
