﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class UpdateDocumentWithInstrumentSettingsMessage
	{
		public Document Document { get; set; }
	}
}
