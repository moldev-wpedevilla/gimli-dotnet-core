﻿using System;
using System.Collections.Generic;

namespace Gimli.Shared.Actors.Messages
{
	
	public class DeleteDocumentsMessage
	{
		public List<Guid> Documents { get; set; }
		public bool OnUsb { get; set; }
	}
}
