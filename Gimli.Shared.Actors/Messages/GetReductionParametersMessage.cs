﻿using System;
using Gimli.JsonObjects.Settings;

namespace Gimli.Shared.Actors.Messages
{
	
	public class GetReductionParametersMessage
	{
		public DataReductionSettings Settings { get; set; }
	}
}
