﻿using System;
using Gimli.JsonObjects.FiltersMngmnt;

namespace Gimli.Shared.Actors.Messages.FilterMngmnt
{
    
    public class FiltersActionMessage
    {
        public FiltersMngrAction Action { get; set; }
    }
}
