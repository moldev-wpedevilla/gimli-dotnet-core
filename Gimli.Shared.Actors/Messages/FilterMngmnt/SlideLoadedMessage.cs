﻿using System;
using MD.Nfc.Basic.StrTagConverting;

namespace Gimli.Shared.Actors.Messages.FilterMngmnt
{
	
	public class SlideLoadedMessage
	{
	    public string SlideSel { get; set; }
        public SliderTagData Slide { get; set; }
	}
}
