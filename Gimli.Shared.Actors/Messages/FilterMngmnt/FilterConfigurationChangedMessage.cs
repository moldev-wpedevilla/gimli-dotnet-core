﻿using System;

namespace Gimli.Shared.Actors.Messages.FilterMngmnt
{
	
	public class FilterConfigurationChangedMessage
	{
		public bool IsFilterReady { get; set; }
	}
}
