﻿using System;
using MD.Nfc.Basic.StrTagConverting;

namespace Gimli.Shared.Actors.Messages.FilterMngmnt
{
	
	public class FrontNfcSlideMessage
	{
        public SliderTagData Slide { get; set; }
	}
}
