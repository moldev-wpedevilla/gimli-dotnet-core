﻿using System;
using MD.Nfc.Basic.StrTagConverting;

namespace Gimli.Shared.Actors.Messages.FilterMngmnt
{
	
	public class FrontNfcFilterCubeMessage
	{
        public FilterTagData Cube { get; set; }
	}
}
