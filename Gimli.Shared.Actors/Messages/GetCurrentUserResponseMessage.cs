﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
    
    public class GetCurrentUserResponseMessage : GimliResponse
    {
        public UserFull UserFull { get; set; }
    }
}
