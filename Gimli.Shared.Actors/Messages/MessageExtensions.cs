﻿using MolDev.Common.Messages;
using MolDev.Common.Messages.Context;

namespace Gimli.Shared.Actors.Messages
{
	public static class MessageExtensions
	{
		public static RequestMessage<T> CreateRequestMessage<T>(T model)
		{
			return new RequestMessage<T>(model, GetExecutionContext());
		}

		public static IExecutionContext GetExecutionContext()
		{
			return new ExecutionContext();
		}
	}
}
