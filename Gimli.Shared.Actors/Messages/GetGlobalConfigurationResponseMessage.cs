﻿using System;
using Gimli.JsonObjects;
using Gimli.JsonObjects.FiltersMngmnt;
using Gimli.JsonObjects.InjectorMaintenance;

namespace Gimli.Shared.Actors.Messages
{
	
	public class GetGlobalConfigurationResponseMessage : GimliResponse
	{
		public ReaderConfiguration ReaderConfiguration { get; set; }
		public SystemConfiguration SystemConfiguration { get; set; }
		public ReadPreferences Preferences { get; set; }
		public InjectorsSetup InjectorConfiguration { get; set; }
		public FilterModuleConfig FilterConfiguration { get; set; }
	}
}
