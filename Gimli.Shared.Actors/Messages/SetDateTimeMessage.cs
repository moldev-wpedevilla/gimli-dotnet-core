﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class SetDateTimeMessage
	{
		public double Value { get; set; }
		public bool IsDate { get; set; }
	}
}
