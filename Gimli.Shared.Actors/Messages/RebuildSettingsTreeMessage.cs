﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class RebuildSettingsTreeMessage
	{
		public AllSettings Settings { get; set; }
	}
}
