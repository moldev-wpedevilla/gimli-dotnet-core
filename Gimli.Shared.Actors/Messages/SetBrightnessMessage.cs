﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class SetBrightnessMessage
	{
		public double Value { get; set; }
	}
}
