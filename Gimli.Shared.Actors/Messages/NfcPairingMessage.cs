﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class NfcPairingMessage
	{
		public bool Abort { get; set; }
		public string UserName { get; set; }
	}
}
