﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class ActiveMessageBase
	{
		public bool Active { get; set; }
	}
}
