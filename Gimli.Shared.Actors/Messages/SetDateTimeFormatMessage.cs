﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class SetDateTimeFormatMessage
	{
		public bool IsDate { get; set; }
		public string Value { get; set; }
	}
}
