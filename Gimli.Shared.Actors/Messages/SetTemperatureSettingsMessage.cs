﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class SetTemperatureSettingsMessage
	{
		public TemperatureSettings Settings { get; set; }
	}
}
