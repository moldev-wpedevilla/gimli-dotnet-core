﻿using System;

namespace Gimli.Shared.Actors.Messages.ExportImport
{
	
	public class ImportFilesMessage
	{
		public Guid[] Files { get; set; }
	}
}
