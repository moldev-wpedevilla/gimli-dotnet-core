﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages.ExportImport
{
    
    public class ExportFilesMessage
    {
        public UserFull UserFull { get; set; }
        public string[] Filenames { get; set; }

        public bool AsExcel { get; set; }
		public bool AutoExported { get; set; }
        public bool Logs { get; set; }
		public bool SendProtocolsToUsb { get; set; }
	}
}
