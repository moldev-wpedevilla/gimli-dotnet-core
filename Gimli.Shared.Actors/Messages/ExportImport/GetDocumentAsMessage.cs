﻿using System;
using System.Collections.Generic;
using System.IO;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
    public enum SaveAsFiletype
    {
        Csv,
        Excel
    }

    
    public class GetDocumentAsMessage
    {
        public UserFull UserFull { get; set; }
        public List<Guid> documentIds { get; set; }
        public SaveAsFiletype SaveAsFiletype { get; set; }
        public Document Document { get; set; }
        public DirectoryInfo TargetDirectoryInfo { get; set; }
    }
}
