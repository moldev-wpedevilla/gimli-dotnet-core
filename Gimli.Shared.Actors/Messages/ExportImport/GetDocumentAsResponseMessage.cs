﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Gimli.Shared.Actors.Messages.ExportImport
{
    
    public class GetDocumentAsResponseMessage : GimliResponse
    {
        public DirectoryInfo TempDirectory { get; set; }
        public List<FileInfo> FileInfos { get; set; }
        public long Size { get; set; }
    }
}
