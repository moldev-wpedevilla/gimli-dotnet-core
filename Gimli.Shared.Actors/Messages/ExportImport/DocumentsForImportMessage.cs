﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages.ExportImport
{
	
	public class DocumentsForImportMessage: GimliResponse
	{
		public DocumentsForImport Documents { get; set; }
	}
}
