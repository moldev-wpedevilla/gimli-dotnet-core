﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class CopyPlateResponseMessage : GimliResponse
	{
		public Guid DocumentId { get; set; }
		public Guid PlateId { get; set; }
		public string Name { get; set; }
	}
}
