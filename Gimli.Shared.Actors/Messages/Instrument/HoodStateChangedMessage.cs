﻿
using System;

namespace Gimli.Shared.Actors.Messages.Instrument
{
	
	public class HoodStateChangedMessage
	{
		public bool IsOpen { get; set; }
	}
}
