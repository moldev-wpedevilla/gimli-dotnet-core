﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages.Instrument
{
	
	public class GetInstrumentStatusResponseMessage : GimliResponse
	{
		public InstrumentState InstrumentState { get; set; }

	}
}
