﻿using System;

namespace Gimli.Shared.Actors.Messages.Instrument
{
	
	public class DataeventMessage : EventlisternBaseMessage
	{
		public int RequestId { get; set; }
		public object Payload { get; set; }
		public bool IsFinalEvent { get; set; }
	}
}
