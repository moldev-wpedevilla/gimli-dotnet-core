﻿using System;

namespace Gimli.Shared.Actors.Messages.Instrument
{
	
	public class GetDeviceIdResponseMessage : GimliResponse
	{
		public string Identity { get; set; }
	}
}
