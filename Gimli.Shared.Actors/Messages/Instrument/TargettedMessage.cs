﻿using System;

namespace Gimli.Shared.Actors.Messages.Instrument
{
	
	public class TargettedMessage
	{
		public string MessageTarget { get; set; }
	}
}
