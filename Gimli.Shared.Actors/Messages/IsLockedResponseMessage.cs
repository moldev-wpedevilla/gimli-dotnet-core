﻿using System;

namespace Gimli.Shared.Actors.Messages
{
    
    public class IsLockedResponseMessage : GimliResponse
    {
        public bool Locked { get; set; }
    }
}