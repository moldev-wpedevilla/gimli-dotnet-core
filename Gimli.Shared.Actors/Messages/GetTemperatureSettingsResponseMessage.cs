﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class GetTemperatureSettingsResponseMessage : GimliResponse
	{
		public DoubleValueWithRange Target { get; set; }
		public bool ControllerRunning { get; set; }
	}
}
