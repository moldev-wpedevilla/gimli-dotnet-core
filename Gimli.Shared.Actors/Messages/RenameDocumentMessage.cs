﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class RenameDocumentMessage
	{
		public Guid DocumentId { get; set; }
		public string Name { get; set; }
	}
}
