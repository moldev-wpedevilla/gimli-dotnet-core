﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
    
    public class GetShakeSettingsResponseMessage : GimliResponse
    {
        public ShakeSettings Settings { get; set; }
    }
}