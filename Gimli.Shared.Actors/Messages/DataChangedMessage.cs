﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class DataChangedMessage
	{
		public DataChangedInfo Info { get; set; }
	}
}
