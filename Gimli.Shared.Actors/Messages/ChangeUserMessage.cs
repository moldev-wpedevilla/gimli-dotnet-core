﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
    
    public class ChangeUserMessage
    {
        public UserFull UserFullOld { get; set; }
        public UserFull UserFullNew { get; set; }
        public string PinNew { get; set; }
        public bool ResetAdminPin { get; set; }
    }
}
