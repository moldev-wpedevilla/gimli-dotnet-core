﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class CheckPinMessage
	{
		public Guid UserId { get; set; }
		public string Pin { get; set; }
	}
}
