﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class RemotingSuccessResponseMessage : GimliResponse
	{
		public int NumberOfSuccesses { get; set; }
	}
}
