﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class UpdateDocumentWithInstrumentSettingsResponseMessage : GimliResponse
	{
		public Document Document { get; set; }
	}
}
