﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class SpinnerMessage : ActiveMessageBase
	{
		public  string Text { get; set; }
	}
}
