﻿using System;
using Gimli.JsonObjects.InjectorMaintenance;

namespace Gimli.Shared.Actors.Messages.InjectorMaintenance
{

	
	public class InjectorStatusChangedMessage
	{
		public InjectorsUpdate Injectors { get; set; }
	}
}
