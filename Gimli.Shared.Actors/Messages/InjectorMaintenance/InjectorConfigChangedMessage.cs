﻿using System;
using Gimli.JsonObjects.InjectorMaintenance;

namespace Gimli.Shared.Actors.Messages.InjectorMaintenance
{
	
	public class InjectorConfigChangedMessage
	{
		public InjectorsSetup InjectorsSetup { get; set; }
	}
}
