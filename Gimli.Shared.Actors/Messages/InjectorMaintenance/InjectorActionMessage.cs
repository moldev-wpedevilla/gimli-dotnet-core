﻿using System;
using Gimli.JsonObjects.InjectorMaintenance;

namespace Gimli.Shared.Actors.Messages.InjectorMaintenance
{
    
    public class InjectorActionMessage
    {
        public InjectorAction Action { get; set; }
    }
}
