﻿using System;
using System.Collections.Generic;
using Gimli.JsonObjects.InjectorMaintenance;

namespace Gimli.Shared.Actors.Messages.InjectorMaintenance
{
	
	public class GetInjectorsCountersResponseMessage : GimliResponse
	{
		public List<InjectorCounters> InjectorsCounters { get; set; }
	}
}
