﻿using System;
using System.Collections.Generic;

namespace Gimli.Shared.Actors.Messages.InjectorMaintenance
{
    
    public class GetInjectorSettingsResponseMessage : GimliResponse
    {
        public List<int> Volume { get; set; }
    }
}
