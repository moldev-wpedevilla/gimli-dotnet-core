﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class StartReadResponseMessage : GimliResponse
	{
		public Guid DocumentId { get; set; }
		public Guid ExperimentId { get; set; }
		public Guid PlateId { get; set; }
	}
}
