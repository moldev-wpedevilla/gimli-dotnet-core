﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
    
    public class AddUserMessage
    {
        public UserFull UserFull { get; set; }
        public string Pin { get; set; }
    }
}
