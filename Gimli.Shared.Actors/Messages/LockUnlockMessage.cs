﻿using System;

namespace Gimli.Shared.Actors.Messages
{
    
    public class LockUnlockMessage
    {
        public bool Lock { get; set; }
    }
}
