﻿using System;
using System.Collections.Generic;

namespace Gimli.Shared.Actors.Messages
{
	
	public class GetInputForReadMessage
	{
		public Guid DocumentId { get; set; }
		public List<Guid> Plates { get; set; }
		public bool Overwrite{ get; set; }
		public string Name { get; set; }
	}
}
