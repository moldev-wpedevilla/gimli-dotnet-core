﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class SetQuickreadSlotMessage
	{
		public UserFull User { get; set; }
		public int SlotId { get; set; }
		public QuickReadSlot Slot { get; set; }
	}
}
