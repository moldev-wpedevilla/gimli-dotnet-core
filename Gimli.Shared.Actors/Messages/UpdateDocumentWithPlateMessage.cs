﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class UpdateDocumentWithPlateMessage
	{
		public Guid DocumentId { get; set; }
		public Plate Plate { get; set; }
		public DataChangedMessage FrontendMessage { get; set; }
		public long WellSeriailizationSize { get; set; }
		public byte[] WellSeriailization { get; set; }
		public Guid Id { get; set; }
	}
}
