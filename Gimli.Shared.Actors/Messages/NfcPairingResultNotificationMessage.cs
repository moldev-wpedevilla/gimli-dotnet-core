﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class NfcPairingResultNotificationMessage
	{
		public bool WasSuccessful { get; set; }
		public NfcPairingResult Result { get; set; }
	}
}
