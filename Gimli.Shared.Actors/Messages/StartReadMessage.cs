﻿using System;
using System.Collections.Generic;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class StartReadMessage
	{
		public List<Document> Documents { get; set; }
	}
}
