﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class ValidateSettingsResponseMessage : GimliResponse
	{
		public AllSettings Settings { get; set; }
	}
}
