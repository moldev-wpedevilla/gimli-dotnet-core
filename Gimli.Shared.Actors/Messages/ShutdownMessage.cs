﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class ShutdownMessage
	{
		public ShutdownMessageType Type { get; set; }
		public bool WantsAnswer { get; set; } = true;
	}
}
