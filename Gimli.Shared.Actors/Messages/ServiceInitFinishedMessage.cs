﻿using System;
using Gimli.Shared.Actors.Actors;

namespace Gimli.Shared.Actors.Messages
{

	
	public class ServiceInitFinishedMessage : GimliResponse
	{
		public InitLevel Level { get; set; }
		public InitFinishedState State { get; set; }
		public string Sender { get; set; }

		public static ServiceInitFinishedMessage Generate(InitLevel level, InitFinishedState state, string sender)
		{
			return new ServiceInitFinishedMessage {Level = level, State = state, Sender = sender};
		}
	}
}
