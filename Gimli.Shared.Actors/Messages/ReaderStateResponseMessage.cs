﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class ReaderStateResponseMessage : GimliResponse
	{
		public DataChangedMessage ReadState { get; set; }

		public string ReaderState { get; set; }

	}
}
