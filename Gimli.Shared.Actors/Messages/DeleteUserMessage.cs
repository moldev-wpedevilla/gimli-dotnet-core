﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
    
    public class DeleteUserMessage
    {
        public UserFull UserFull { get; set; }
    }
}
