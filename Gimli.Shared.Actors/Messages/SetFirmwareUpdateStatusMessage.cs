﻿using System;

namespace Gimli.Shared.Actors.Messages
{
    public class FirmwareUpdateStatus
    {
        public enum Status
        {
            Ok,
            Done,
            Error
        }
    }

    
    public class SetFirmwareUpdateStatusMessage
    {
        public int UpdateStatusPercentage { get; set; }
        public string UpdateStatusText { get; set; }
        public FirmwareUpdateStatus.Status Status { get; set; }
    }
}
