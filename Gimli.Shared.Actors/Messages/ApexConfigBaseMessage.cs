﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class ApexConfigBaseMessage : GimliResponse
	{
		public string ConfigKey { get; set; }
		public object Value { get; set; }
	}
}
