﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class InitializeInstrumentMessage
	{
		public bool FromActorsystemInit { get; set; }
	}
}
