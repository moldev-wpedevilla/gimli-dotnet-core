﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	public enum StorageStrategy
	{
		PublishToAll,
		Local,
		CertainRemote
	}


	
	public class StoreDocumentMessage
	{
		public StorageStrategy Strategy { get; set; }
		public string RemoteHostname { get; set; }
		public Document Document { get; set; }
		public string SerializedDocument { get; set; }
		public bool AutoExported { get; set; }
		public int NumberOfDocuments { get; set; }
		public Guid ExportCallIdentifier { get; set; }
	}
}
