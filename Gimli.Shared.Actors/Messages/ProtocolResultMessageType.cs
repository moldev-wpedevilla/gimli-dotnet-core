﻿namespace Gimli.Shared.Actors.Messages
{
	public enum ProtocolResultMessageType
	{
		None = 0,
		GetDocument,
		GetEmptyDocument,
		GetProtocol,
		GetProtocolList,
		GetResult,
		GetResultList,
        GetStandardProtocolList,
        GetLastUsedProtocolList,
        GetQuickProtocol,
		GetCategoriesListStandard,
		GetCategoriesListMy,
		GetResultsForImportList,
		GetProtocolsForImportList,
		ImportDocuments,
		GetCountsForImportDocuments,
	}
}
