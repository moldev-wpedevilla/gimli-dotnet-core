﻿using System;
using Gimli.JsonObjects.Settings;

namespace Gimli.Shared.Actors.Messages
{
	
	public class ValidateReductionSettingsMessage
	{
		public DataReductionSettings Settings { get; set; }
	}
}
