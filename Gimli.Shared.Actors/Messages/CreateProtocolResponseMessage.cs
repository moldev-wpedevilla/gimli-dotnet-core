﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class CreateProtocolResponseMessage : GimliResponse
	{
		public Guid DocumentId { get; set; }
	}
}
