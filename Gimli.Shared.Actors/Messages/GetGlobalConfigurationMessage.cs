﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	public enum ConfigurationPart
	{
		None,
		Injector,
		Read,
		Reader,
		Shake,
		System,
		Temperature,
		Filters
	}

	
	public class GetGlobalConfigurationMessage
	{
		public ConfigurationPart Configuration { get; set; }
	}
}
