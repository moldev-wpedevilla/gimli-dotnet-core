﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class UpdateDocumentWithSettingsMessage
	{
		public AllSettings Settings { get; set; }
	}
}
