﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class SetVolumeMessage
	{
		public double Value { get; set; }
	}
}
