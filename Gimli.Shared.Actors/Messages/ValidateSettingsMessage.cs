﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class ValidateSettingsMessage
	{
		public AllSettings Settings { get; set; }
	}
}
