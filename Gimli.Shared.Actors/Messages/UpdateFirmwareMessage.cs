﻿using System;

namespace Gimli.Shared.Actors.Messages
{
    
    public class UpdateFirmwareMessage
    {
        public string PathToUpdateDirectory { get; set; }
    }
}
