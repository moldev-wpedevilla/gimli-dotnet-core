﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class ValidateDocumentResponseMessage : GimliResponse
	{
		public string ModuleInfo{ get; set; }
	}
}
