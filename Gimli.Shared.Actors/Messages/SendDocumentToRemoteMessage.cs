﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class SendDocumentToRemoteMessage
	{
		public Guid DocumentId { get; set; }
		/// <summary>
		/// For datasync tool to decide if files where exported directly via export button or indirectly as the read finishes
		/// </summary>
		public bool AutoExported { get; set; }
		/// <summary>
		/// For datasync tool so it can decide if it already has received all messages
		/// </summary>
		public int NumberOfDocuments { get; set; }
		/// <summary>
		/// For datasync tool to decide to which export call (export button click) the message belongs to
		/// </summary>
		public Guid ExportCallIdentifier { get; set; }
	}
}
