﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class TogglePlateDrawerMessage
	{
        public PlateDrawerSettings Settings { get; set; }
    }
}
