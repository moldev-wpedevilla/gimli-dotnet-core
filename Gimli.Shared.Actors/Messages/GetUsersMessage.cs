﻿using System;

namespace Gimli.Shared.Actors
{
    
    public class GetUsersMessage
    {
        public string Username { get; set; }
        public Guid Id { get; set; }
    }
}
