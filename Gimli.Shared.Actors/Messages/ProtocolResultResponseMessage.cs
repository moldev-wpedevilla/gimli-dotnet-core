﻿using System;

namespace Gimli.Shared.Actors.Messages
{
    
	public class ProtocolResultResponseMessage : GimliResponse
	{
		public string JsonFormattedDocument { get; set; }
	}
}
