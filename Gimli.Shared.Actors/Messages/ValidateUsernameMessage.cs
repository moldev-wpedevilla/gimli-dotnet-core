﻿using System;

namespace Gimli.Shared.Actors.Messages
{
    
    public class ValidateUsernameMessage
    {
        public string Username { get; set; }
    }
}
