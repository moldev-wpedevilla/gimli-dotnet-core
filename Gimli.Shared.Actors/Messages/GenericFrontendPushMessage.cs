﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class GenericFrontendPushMessage
	{
		public MessageParameters Parameters { get; set; }
	}
}
