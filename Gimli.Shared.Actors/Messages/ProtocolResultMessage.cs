﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class ProtocolResultMessage
	{
		public ProtocolResultMessageType Type { get; set; }
		public Guid Id { get; set; }
		public PlateWellBase WellGroup { get; set; }
        public UserFull UserFull { get; set; }
		public int NumberOfItemsRequested { get; set; }
		public string Category { get; set; }
		public bool Serialize { get; set; } = true;
	}
}
