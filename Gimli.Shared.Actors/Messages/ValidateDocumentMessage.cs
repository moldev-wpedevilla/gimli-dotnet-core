﻿using System;
using Gimli.JsonObjects;

namespace Gimli.Shared.Actors.Messages
{
	
	public class ValidateDocumentMessage
	{
		public Document Document{ get; set; }
	}
}
