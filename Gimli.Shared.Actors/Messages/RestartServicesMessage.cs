﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class RestartServicesMessage
	{
		public ActorServiceKey[] ServiceIdentifier { get; set; }
	}
}
