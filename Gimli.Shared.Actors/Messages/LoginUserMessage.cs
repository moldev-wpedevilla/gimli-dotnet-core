﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	public enum LoginIdType
	{
		None,
		Nfc,
		User
	}

	
	public class LoginUserMessage
	{
		public LoginIdType Type { get; set; }
		public Guid Id { get; set; }
	}
}
