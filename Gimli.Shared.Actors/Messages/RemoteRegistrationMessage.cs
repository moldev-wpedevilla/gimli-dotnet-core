﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class RemoteRegistrationMessage : GimliResponse
	{
		public string ConnectionString { get; set; }
		public bool AsMaster { get; set; }
	}
}
