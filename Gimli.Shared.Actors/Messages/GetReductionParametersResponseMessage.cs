﻿using System;
using Gimli.JsonObjects.Settings;

namespace Gimli.Shared.Actors.Messages
{
	
	public class GetReductionParametersResponseMessage : GimliResponse
	{
		public DataReductionSettings Settings { get; set; }
	}
}
