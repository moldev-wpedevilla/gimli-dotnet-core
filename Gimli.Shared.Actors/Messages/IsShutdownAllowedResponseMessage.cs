﻿using System;
using System.Collections.Generic;
using Gimli.Shared.Actors.Actors;

namespace Gimli.Shared.Actors.Messages
{
	
	public class IsShutdownAllowedResponseMessage : GimliResponse
	{
		public bool IsShutdownAllowed => ShutdownForbiddenReasons.Count == 0;

		public List<ShutdownForbiddenReason> ShutdownForbiddenReasons { get; set; } = new List<ShutdownForbiddenReason>();
	}
}
