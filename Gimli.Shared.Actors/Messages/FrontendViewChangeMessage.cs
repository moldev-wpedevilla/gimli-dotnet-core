﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class FrontendViewChangeMessage
	{
		public string View { get; set; }
	}
}
