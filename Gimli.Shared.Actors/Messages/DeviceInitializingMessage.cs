﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class DeviceInitializingMessage
	{
		public bool IsInitializing { get; set; }
	}
}
