﻿using System;
using Gimli.JsonObjects.Outbound;

namespace Gimli.Shared.Actors.Messages.Outbound
{
	
	public class OutboundDocumentInfoMessage
	{
		public DocumentUpdateInfo DocumentUpdateInfo { get; set; }
	}
}
