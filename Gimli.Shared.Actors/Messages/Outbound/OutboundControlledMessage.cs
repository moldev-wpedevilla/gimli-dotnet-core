﻿using System;
using Gimli.Shared.Actors.Messages.Instrument;

namespace Gimli.Shared.Actors.Messages.Outbound
{
	
	public class OutboundControlledMessage : EventlisternBaseMessage
	{
		public bool IsOutboundControlled { get; set; }
		public string IpAddressOfClient { get; set; }
		public string ClientHostname { get; set; }
	}
}
