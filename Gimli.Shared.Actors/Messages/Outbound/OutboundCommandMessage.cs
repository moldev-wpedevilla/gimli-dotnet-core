﻿using System;
using Gimli.Shared.Actors.Messages.Instrument;

namespace Gimli.Shared.Actors.Messages.Outbound
{
	
	public class OutboundCommandMessage : EventlisternBaseMessage
	{
		public string Identity { get; set; }
		public string CommandType { get; set; }
		public string CommandParameters { get; set; }
	}
}
