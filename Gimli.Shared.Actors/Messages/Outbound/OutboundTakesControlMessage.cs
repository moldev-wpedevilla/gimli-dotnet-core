﻿using System;

namespace Gimli.Shared.Actors.Messages.Outbound
{
	
	public class OutboundTakesControlMessage : ActiveMessageBase
	{
		public string Identity { get; set; }
		public string EventlistenerId { get; set; }
	}
}
