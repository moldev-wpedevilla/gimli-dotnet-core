﻿using System;

namespace Gimli.Shared.Actors.Messages.Outbound
{
	
	public class OutboundControlledAndMoreInfoMessage: OutboundControlledMessage
	{
		public string Username { get; set; }
		public bool GxpMode { get; set; }
	}
}
