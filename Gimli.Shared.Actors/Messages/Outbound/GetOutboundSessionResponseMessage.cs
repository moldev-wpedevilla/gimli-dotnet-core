﻿using System;
using Gimli.JsonObjects.Outbound;

namespace Gimli.Shared.Actors.Messages.Outbound
{
	
	public class GetOutboundSessionResponseMessage : GimliResponse
	{
		public OutboundSession Session { get; set; }
	}
}
