﻿using System;
using Gimli.JsonObjects.Outbound;

namespace Gimli.Shared.Actors.Messages.Outbound
{
	
	public class OutboundDataMessage
	{
		public MeasurementDataInfo Data { get; set; }
	}
}
