﻿using System;
using Gimli.JsonObjects.Outbound;

namespace Gimli.Shared.Actors.Messages.Outbound
{
	
	public class GxpLoginMessage
	{
		public GxpUser GxpUser { get; set; }
	}
}
