﻿using System;
using Gimli.JsonObjects.Outbound;

namespace Gimli.Shared.Actors.Messages.Outbound
{
	public enum ReadControlCommand
	{
		None,
		Read,
		Stop,
		Pause
	}


	
	public class OutboundReadControlMessage
	{
		public ReadControlCommand Command { get; set; }

		public DocumentReadInfo ReadInfo { get; set; }
	}
}
