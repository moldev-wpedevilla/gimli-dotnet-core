﻿using System;

namespace Gimli.Shared.Actors.Messages
{
	
	public class DistributedRequestStartMessage
	{
		public TellSuccessLevel DesiredSuccessLevel { get; set; }
		public int TotalResponsesExpected { get; set; }
		public int SelfDistuctionTimerInSeconds { get; set; }
	}
}
