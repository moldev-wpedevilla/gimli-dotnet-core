﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;

namespace Gimli.Shared.Actors
{
	public class SpinnerController : IDisposable
	{
		private readonly string mSpinnerText;
		private readonly Func<IActor> mWebServiceActor;
		private readonly CancellationTokenSource mTokenSource = new CancellationTokenSource();
		private readonly ManualResetEventSlim mSpinnerOffTimer = new ManualResetEventSlim();
		private readonly int mTimeBeforSendingUserNotification = 1000;
		private bool mSpinnerHidden;
		private bool mSpinnerDisplayed;

		public SpinnerController(Func<IActor> webserviceActor, string spinnerText)
		{
			mWebServiceActor = webserviceActor;
			mSpinnerText = spinnerText;
			//const string key = "TimeBeforSendingUserNotification";
			//if (ConfigurationManager.AppSettings.AllKeys.Any(k => k == key))
			//{
			//	mTimeBeforSendingUserNotification = int.Parse(AppConfigurationManager.GetSettingsValue(key));
			//}
		}

		public void DisplaySpinner(int timoutMs = int.MaxValue)
		{
			Task.Run(() =>
			{
				var timeout = new ManualResetEventSlim();
				try
				{
					timeout.Wait(100, mTokenSource.Token);
				}
				catch (OperationCanceledException)
				{
					return;
				}

				if (mTokenSource.IsCancellationRequested)
				{
					return;
				}

				mSpinnerDisplayed = true;

				mWebServiceActor().Tell(MessageExtensions.CreateRequestMessage(new SpinnerMessage
				{
					Active = true,
					Text = mSpinnerText
				}));

				mSpinnerOffTimer.Wait(timoutMs);

				mWebServiceActor()?.Tell(MessageExtensions.CreateRequestMessage(new SpinnerMessage
				{
					Active = false
				}));

				mSpinnerHidden = true;
			});
		}

		public void HiddeSpinner()
		{
			mSpinnerOffTimer.Set();
			mTokenSource.Cancel();
		}

		public void HiddeSpinner<T>(T message) where T: class 
		{
			HiddeSpinner();
			Thread.Sleep(mTimeBeforSendingUserNotification);
			mWebServiceActor().Tell(MessageExtensions.CreateRequestMessage(message));
		}

		public void Dispose()
		{
			if (mSpinnerHidden || !mSpinnerDisplayed) return;

			if(mSpinnerOffTimer != null && !mSpinnerOffTimer.IsSet) mSpinnerOffTimer?.Set();
			else mWebServiceActor()?.Tell(MessageExtensions.CreateRequestMessage(new SpinnerMessage
			{
				Active = false
			}));
		}
	}
}
