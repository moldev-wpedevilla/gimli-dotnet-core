﻿using System;
using System.Configuration;
using System.Diagnostics;
using Castle.Core.Internal;

namespace Gimli.Shared.Actors.AppStarter
{
    public class StartupElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return base["name"] as string; }
            set { base["name"] = value; }
        }

        [ConfigurationProperty("program", IsRequired = false, IsKey = false)]
        public string Program
        {
            get { return base["program"] as string; }
            set { base["program"] = value; }
        }

        [ConfigurationProperty("actorName", IsRequired = false, IsKey = false)]
        public string ActorName
        {
            get { return base["actorName"] as string; }
            set { base["actorName"] = value; }
        }

        [ConfigurationProperty("actorPort", IsRequired = false, IsKey = false)]
        public int ActorPort
        {
            get { return Convert.ToInt32(base["actorPort"]); }
            set { base["actorPort"] = value; }
        }

        [ConfigurationProperty("actorClass", IsRequired = false, IsKey = false)]
        public string ActorClass
        {
            get { return base["actorClass"] as string; }
            set { base["actorClass"] = value; }
        }

        [ConfigurationProperty("actorProxy", IsRequired = false, IsKey = false)]
        public string ActorProxy
        {
            get { return base["actorProxy"] as string; }
            set { base["actorProxy"] = value; }
        }

        [ConfigurationProperty("watch", IsRequired = false, IsKey = false)]
        public bool Watch
        {
            get { return (bool)base["watch"]; }
            set { base["watch"] = value; }
        }

        [ConfigurationProperty("initAS", IsRequired = false, IsKey = false)]
        public bool InitAS
        {
            get { return (bool) base["initAS"]; }
            set { base["initAS"] = value; }
        }

        [ConfigurationProperty("watchActor", IsRequired = false, IsKey = false)]
        public bool WatchActor
        {
            get { return (bool)base["watchActor"]; }
            set { base["watchActor"] = value; }
        }

        [ConfigurationProperty("startupDelay", IsRequired = false, IsKey = false)]
        public int StartupDelay
        {
            get { return (int) base["startupDelay"];  }
            set { base["startupDelay"] = value;  }
        }

        [ConfigurationProperty("startupHidden", IsRequired = false, IsKey = false)]
        public bool StartupHidden
        {
            get { return (bool) base["startupHidden"];  }
            set { base["startupHidden"] = false;  }
        }

        [ConfigurationProperty("updateFirewall", IsRequired = false, IsKey = false)]
        public bool UpdateFirewall
        {
            get { return (bool) base["updateFirewall"];  }
            set { base["updateFirewall"] = false;  }
        }

        [ConfigurationProperty("frontendProgram", IsRequired = false, IsKey = false)]
        public string FrontendProgram
        {
            get { return (string) base["frontendProgram"];  }
            set { base["frontendProgram"] = value; }
        }

        [ConfigurationProperty("frontendCacheDisk", IsRequired = false, IsKey = false)]
        public string FrontendCacheDisk
        {
            get { return (string) base["frontendCacheDisk"]; }
            set { base["frontendCacheDisk"] = value;  }
        }

        [ConfigurationProperty("frontendCacheMedia", IsRequired = false, IsKey = false)]
        public string FrontendCacheMedia
        {
            get { return (string)base["frontendCacheMedia"]; }
            set { base["frontendCacheMedia"] = value; }
        }

        [ConfigurationProperty("frontendCheckUpdateInterval", IsRequired = false, IsKey = false)]
        public string FrontendCheckUpdateInterval
        {
            get { return (string) base["frontendCheckUpdateInterval"]; }
            set { base["frontendCheckUpdateInterval"] = value; }
        }
        
        public bool RequiresWatch => base["watch"] == null ? false : (bool)base["watch"];

        public bool RequiresActorWatch
        {
            get
            {
                if (WatchActor == null || String.IsNullOrEmpty(ActorProxy))
                {
                    return false;
                }
                return true;
            }
        }

        public bool StartWithActor => (!String.IsNullOrEmpty(ActorName) && !String.IsNullOrEmpty(ActorClass) && ActorPort != 0);

        public ProcessStartInfo GetStartInfo
        {
            get
            {
                if (Name == "Frontend")
                {
                    return new ProcessStartInfo
                    {
                        FileName = $"{FrontendProgram}",
                        Arguments = $" --new-window --disable-infobars --disable-session-crashed-bubble --noerrdialogs --no-default-browser-check --no-first-run --disable-component-update --incognito --disable-pinch --overscroll-history-navigation=0 --disk-cache-size={FrontendCacheDisk} --media-cache-size={FrontendCacheMedia} --check-for-update-interval={FrontendCheckUpdateInterval} --kiosk http://localhost:8002/"
                    };
                }

                if (((string)base["program"]).IsNullOrEmpty())
                {
                    return null;
                }

                if (StartupHidden)
                {
                    return new ProcessStartInfo
                    {
                        FileName        = (string) base["program"],
                        WindowStyle     = ProcessWindowStyle.Hidden,
                        CreateNoWindow  = true,
                        UseShellExecute = false
                    };
                }
                
                return new ProcessStartInfo
                {
                    FileName = (string)base["program"],
                };
            }
        }


    }
}
