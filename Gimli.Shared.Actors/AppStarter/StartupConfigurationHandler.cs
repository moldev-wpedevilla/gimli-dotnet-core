﻿using System;
using System.Configuration;
using System.IO;

namespace Gimli.Shared.Actors.AppStarter
{
    public class StartupConfigurationHandler : ConfigurationSection
    {
        [ConfigurationProperty("", IsDefaultCollection = true, IsKey = false, IsRequired = true)]
        public StartupCollection Startups
        {
            get
            {
                return base[""] as StartupCollection;
            }

            set
            {
                base[""] = value;
            }
        }
        
        public static StartupConfigurationHandler GetHandler(string configName)
        {
	        var libConfig = GetConfigByName(configName);
	        return libConfig.GetSection("startups") as StartupConfigurationHandler;
        }

	    public static Configuration GetConfigByName(string configName)
	    {
		    var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configName);
		    var fileMap = new ExeConfigurationFileMap {ExeConfigFilename = filePath};

		    return ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
	    }
    }
}
