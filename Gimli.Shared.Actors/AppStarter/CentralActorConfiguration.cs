﻿using System;
using System.Collections.Generic;
using Castle.Core.Internal;

namespace Gimli.Shared.Actors.AppStarter
{
    public class CentralActorConfiguration
    {
        private static CentralActorConfiguration InnerInstance;

		private CentralActorConfiguration()
		{
		    if (StartupConfigurationHandler.GetHandler("Gimli.AppStarter.dll.config") == null || StartupConfigurationHandler.GetHandler("Gimli.AppStarter.dll.config").Startups.IsNullOrEmpty())
		    {
		        return;
		    }

            foreach (StartupElement startup in StartupConfigurationHandler.GetHandler("Gimli.AppStarter.dll.config").Startups)
		    {
		        if (startup.StartWithActor)
		        {
		            ActorServiceKey key;
                    Enum.TryParse(startup.ActorName, true, out key);

                    ServiceConfigurations[key] = new ServiceConfiguration(startup.ActorName, (ushort)startup.ActorPort, startup.ActorClass, !startup.InitAS);
		        }
		    }
		}

		public Dictionary<ActorServiceKey, ServiceConfiguration> ServiceConfigurations { get; } = new Dictionary<ActorServiceKey, ServiceConfiguration>();

		public static CentralActorConfiguration Instance
		{
			get
			{
    			if (InnerInstance == null)
				{
					InnerInstance = new CentralActorConfiguration();
				}
	            return InnerInstance;
			}
		}
    }
}
