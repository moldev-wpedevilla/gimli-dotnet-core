﻿using System;
using System.Configuration;

namespace Gimli.Shared.Actors.AppStarter
{
    public class StartupCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new StartupElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((StartupElement)element).Name;
        }

        protected override string ElementName => "startup";

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.BasicMap;

        public StartupElement this[int index] => (StartupElement)BaseGet(index);
    }
}
