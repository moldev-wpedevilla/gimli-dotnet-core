﻿using System.Collections.Generic;
using Gimli.Shared.Actors.AppStarter;

namespace Gimli.Shared.Actors
{
    public class RemoteActorConfiguration
    {
        private static readonly object Locker = new object();
		private static RemoteActorConfiguration InnerInstance;

		public RemoteActorConfiguration()
		{
		    ServiceConfigurations = CentralActorConfiguration.Instance.ServiceConfigurations;
		}

		public Dictionary<ActorServiceKey, ServiceConfiguration> ServiceConfigurations { get; } = new Dictionary<ActorServiceKey, ServiceConfiguration>();

		public static RemoteActorConfiguration Instance
		{
			get
			{
				lock (Locker)
				{
					if (InnerInstance == null)
					{
						InnerInstance = new RemoteActorConfiguration();
					}
				}
				return InnerInstance;
			}
		}
    }
}
