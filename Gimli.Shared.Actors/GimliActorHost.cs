﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Castle.Core.Internal;
using Castle.Windsor;
using Gimli.JsonObjects;
using MolDev.Common.Akka.Interfaces;
using Gimli.JsonObjects.Utils;
using Gimli.Shared.Actors.Actors;

namespace Gimli.Shared.Actors
{
	public class GimliActorHost
	{
		private readonly Action<IWindsorContainer> mAdditionalInstall;
		private readonly string mConfigFilePath;

		public IWindsorContainer Container;
		private IActorService mActorService;
		private bool mWasRestarted;

		public string IpAddressUsedForConfiguration { get; private set; }

		public GimliActorHost(Action<IWindsorContainer> additionalInstall, string configFilePath)
		{
			mAdditionalInstall = additionalInstall;
			mConfigFilePath = configFilePath;
		}

		public void Start(ServiceConfiguration setup, bool registerWindsor = true)
		{
			var tmpFileName = ReworkConfig(setup);
			Container = new WindsorContainer().Install(new GimliWindsorInstaller(mAdditionalInstall, tmpFileName, registerWindsor));
			//var x = new MolDev.ActorHost.ActorService(Container, tmpFileName, "moldevhost:ActorSystemName");
			mActorService = Container.Resolve<IActorService>();
			var state = Container.Resolve<IActorsystemOverallState>();
			state.WasRestarted = mWasRestarted;
			
		    try
		    {
                File.Delete(tmpFileName);
            }
		    catch (Exception exc)
		    {
		        Trace.WriteLine("Error deleting File: " + exc.Message);
		    }
			
		}

		public void Restart(ServiceConfiguration setup)
		{
			Stop();
			mWasRestarted = true;
			Start(setup, false);
		}

		public string ReworkConfig(ServiceConfiguration setup)
		{

            var tmpConfigFilePath = DataSetHelper.RootPath + "temp\\";
            if (!Directory.Exists(tmpConfigFilePath))
                Directory.CreateDirectory(tmpConfigFilePath);
		    if (Directory.GetFiles(tmpConfigFilePath).Length > 20)
		        DeleteTempFiles(tmpConfigFilePath);

            var tmpFileName = tmpConfigFilePath + Guid.NewGuid().ToString() + ".tmp";

			var xx = Directory.GetCurrentDirectory();

			File.Copy(mConfigFilePath, tmpFileName);

            var config = File.ReadAllText(tmpFileName);

			var replaceString = "==replaceMeWithHostname==";
			config = config.Replace(replaceString, HelperMethods.GetLocalHostName());

			replaceString = "==replaceMeWithIP==";
			IpAddressUsedForConfiguration = HelperMethods.GetIPAddressList().First();
			config = config.Replace(replaceString, IpAddressUsedForConfiguration);

			replaceString = "==replaceMeWithPort==";
			config = config.Replace(replaceString, setup.ServicePort.ToString());

			File.WriteAllText(tmpFileName, config);
			return tmpFileName;
		}

        private void DeleteTempFiles(string tmpConfigFilePath)
        {
            try
            {
                var list = Directory.GetFiles(tmpConfigFilePath);
                foreach (var file in list)
                    File.Delete(file);
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Error deleting all files in folder: " + tmpConfigFilePath + "\r\n" + exc.Message);
            }
            
        }

        public void Stop()
		{
			mActorService.ShutDown();
		}
	}
}