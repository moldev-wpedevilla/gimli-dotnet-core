﻿using System;
using Akka.Event;
using Gimli.JsonObjects;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;
using Status = Akka.Actor.Status;

namespace Gimli.Shared.Actors
{
	
	public class GimliResponse : ResponseMessageBase
	{
		/// <summary>
		/// Constructor for the case an unexpected exception was thrown.
		/// </summary>
		/// <param name="failure"></param>
		/// <param name="logger"></param>
		public GimliResponse(Status.Failure failure, ILoggingAdapter logger)
		{
			OriginHostname = HelperMethods.GetLocalHostName();
			if (failure?.Cause == null) return;
			Error = new ErrorInfo {Details = failure.Cause.ToString()};
			logger.Error(Error.Details);
			//logger.Error(new Error(failure.Cause, "actor", typeof(GimliResponse), Error.Details));
			ResponseStatus = ResultStatusEnum.Error;
		}

		public GimliResponse(ErrorInfo info, ResultStatusEnum error, ILoggingAdapter logger = null, Action<string> logger2 = null)
		{
			OriginHostname = HelperMethods.GetLocalHostName();
			if (logger == null && logger2 == null) throw new ActorException("No logger for this error.");
			Error = info;
			if (error == ResultStatusEnum.Error)
			{
				var errorMessage = info.Details ?? info.Message;
				if (logger != null)
				{
					logger.Error(errorMessage);
				}
				else
				{
					logger2(errorMessage);
				}
			}

			ResponseStatus = error;
		}

		public GimliResponse()
		{
			OriginHostname = HelperMethods.GetLocalHostName();
			ResponseStatus = ResultStatusEnum.Success;
		}

		/// <summary>
		/// Additional status because if base.Status is set, a MoldevExcetion is thrown -> might not always be wanted
		/// </summary>
		public ResultStatusEnum ResponseStatus { get; }

		public string OriginHostname { get; }
	}
}
