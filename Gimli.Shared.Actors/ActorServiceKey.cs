﻿namespace Gimli.Shared.Actors
{
	public enum ActorServiceKey
	{
		None,
		Frontend,
		Webservice,
		Core,
		Data,
		Remote,
		RemoteInstrument,
		InstrumentServer,
		AppManager,
		Network,
	}
}
