﻿namespace Gimli.Shared.Actors
{
	public class ServiceConfiguration
	{
		public ServiceConfiguration(string name, ushort port, string topLevelRouter, bool doSelfInit)
		{
			ServiceName = name;
			ServicePort = port;
			TopLevelRouter = topLevelRouter;
			DoSelfInit = doSelfInit;
		}

		public string ServiceName { get; private set; }
		public ushort ServicePort { get; private set; }
		public string TopLevelRouter { get; set; }
		public bool DoSelfInit { get; set; }
	}
}
