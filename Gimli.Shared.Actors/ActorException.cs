﻿using System;

namespace Gimli.Shared.Actors
{
	
	public class ActorException : Exception
	{
		public ActorException()
		{
			
		}

		public ActorException(string msg) : base(msg)
		{
			
		}
	}
}
