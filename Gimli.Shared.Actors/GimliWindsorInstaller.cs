﻿using System;
using System.Configuration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Gimli.JsonObjects;
using MolDev.ActorHost;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;

namespace Gimli.Shared.Actors
{
	public class GimliWindsorInstaller : IWindsorInstaller
	{
		private readonly Action<IWindsorContainer> mAdditionalInstall;
		private readonly string mActorServiceConfigFileName;
		private readonly bool mRegisterWindsor;

		public GimliWindsorInstaller(Action<IWindsorContainer> additionalInstall, string akkaConfigFile, bool registerWindsor)
		{
			mAdditionalInstall = additionalInstall;
			mActorServiceConfigFileName = akkaConfigFile;
			mRegisterWindsor = registerWindsor;
		}

		public void Install(IWindsorContainer container, IConfigurationStore store)
		{
			if (mRegisterWindsor)
			{
				container.Register(Component.For<IWindsorContainer>().Instance(container));
			}

			container.Register(Component.For<ILogService>()
				.ImplementedBy<LogService>()
				.DependsOn(new
				{
					loggerName = Consts.MolDevWebLogger
				})
				.LifeStyle.Singleton);

			container.Register(Component.For<IActorService>()
				.ImplementedBy<ActorService>()
				.DependsOn(new
				{
					container,
					actorServiceConfigFileName = mActorServiceConfigFileName,
					serviceName = AppConfigurationManager.GetSettingsValue("moldevhost:ActorSystemName")
				})
				.LifeStyle.Singleton);

			mAdditionalInstall?.Invoke(container);
		}
	}
}
