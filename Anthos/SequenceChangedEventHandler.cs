using System.Collections.Generic;

namespace Apex.Extensibility
{
    public delegate void SequenceChangedEventHandler<T>(SequenceChangedEventArgs<T> args);

    public class SequenceChangedEventArgs<T>
    {
        private List<T> items = new List<T>();
        private int newIndex;
        private int oldIndex;
        private int count;
        private SequenceChangedReason reason;

        public SequenceChangedEventArgs(int changeCount)
        {
            Init(0, -1, changeCount, SequenceChangedReason.SequenceDeleted);
        }

        public SequenceChangedEventArgs(int itemIndex, int changeCount)
        {
            Init(itemIndex, -1, changeCount, SequenceChangedReason.SequenceDeleted);
        }

        public SequenceChangedEventArgs(T itemChanged, int itemIndex, SequenceChangedReason changeReason)
        {
            items.Add(itemChanged);
            Init(-1, itemIndex, 1, changeReason);
        }

        public SequenceChangedEventArgs(T itemChanged, int itemOldIndex, int itemNewIndex, SequenceChangedReason changeReason)
        {
            items.Add(itemChanged);
            Init(itemOldIndex, itemNewIndex, 1, changeReason);
        }

        public SequenceChangedEventArgs(IEnumerable<T> itemsChanged, int itemIndex, SequenceChangedReason changeReason)
        {
            items = new List<T>(itemsChanged);
            Init(-1, itemIndex, items.Count, changeReason);
        }

        public List<T> ChangedItems { get { return items; } }
        public int ItemOldIndex { get { return oldIndex; } }
        public int ItemNewIndex { get { return newIndex; } }
        public int ChangedItemsCount { get { return count; } }
        public SequenceChangedReason Reason { get { return reason; } }

        private void Init(int itemOldIndex, int itemNewIndex, int changedCount, SequenceChangedReason changeReason)
        {
            oldIndex = itemOldIndex;
            newIndex = itemNewIndex;
            reason = changeReason;
            count = changedCount;
        }
    }

    public interface INotifiesOnChanges<T>
    {
        event SequenceChangedEventHandler<T> Changed;
    }

    public enum SequenceChangedReason
    {
        ItemAdded,
        ItemDeleted,
        ItemMovedUp,
        ItemMovedDown,
        SequenceDeleted
    }
}
