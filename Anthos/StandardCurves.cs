using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Anthos.Analysis
{
	#region Enums
	public enum CurveTypes
	{
		PointToPoint,
		LinearRegression,
		CubicSpline,
		FourParameterFit,
		Polynomial
	}

	public enum AxisType
	{
		linear,
		logarithmic
	}

    [Serializable]
	public enum AnalysisStatus
	{
		OK,
		Overflow,
		Error,
		Underflow,
		Unused,
		Extrapolated,		
		NotEvaluated,
		Rejected
	}

	#endregion
	#region CurveBase
	public abstract class CurveBase
	{
        protected double[] _y;
        protected double[] _x;
        protected double[] _cy;
        protected double[] _cx;
		private double[] _bx;  //Backfit Values;
		private AnalysisStatus[] _bs;
		private double _extraYMin;
		private double _extraYMax;
		private double _extraXMin;
		private double _extraXMax;
		private AxisType _xAxis;
		private AxisType _yAxis;
		private double _extra = 0;
		private int _count;
		private bool _increasing = true;
		private bool _chkmonotony = true;
		private int _decPlaces = 3;
        protected bool _sortY = false;
		//private double _rsquared = 0;

		public CurveBase()
		{}
		public abstract void CalculateCurve(double[] Y, double[] X, int count, AxisType yAxis, AxisType xAxis, double extrapolation);
		protected abstract double CalcXFromY(double Y, out AnalysisStatus State);
		protected abstract double CalcYFromX(double X, out AnalysisStatus State);
		//public abstract double RSquared();

		public void SetCurveParameters(double[] Y, double[] X, int count, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
			SetParameters(Y, X, count, yAxis, xAxis, extrapolation);
		}

		public double GetXFromY(double Y, out AnalysisStatus State)
		{
			double dret;
			
			try
			{
				dret= CalcXFromY(Y, out State);
				if (double.IsNaN(dret))
					throw new Exception();
			}
			catch
			{
				State = AnalysisStatus.Error;
				return double.NaN;				
			}
			
			

			if (dret < ExtrapolationXMin)
			{
				State = AnalysisStatus.Underflow;
				return ExtrapolationXMin;
			}

			if (dret > ExtrapolationXMax)
			{
				State = AnalysisStatus.Overflow;
				return ExtrapolationXMax;
			}

			if (dret >= ExtrapolationXMin && dret < GetMinX)
				State = AnalysisStatus.Extrapolated;

			if (dret <= ExtrapolationXMax && dret > GetMaxX)
				State = AnalysisStatus.Extrapolated;

			return SetValue(dret);
		}

		public double GetYFromX(double X, out AnalysisStatus State)
		{
			double dret;
			
			try
			{
				dret = CalcYFromX(X, out State);
			}
			catch
			{
				State = AnalysisStatus.Error;
				return double.NaN;				
			}
			
			if (X < ExtrapolationXMin)
			{
				State = AnalysisStatus.Underflow;
				return dret;
			}

			if (X > ExtrapolationXMax)
			{
				State = AnalysisStatus.Overflow;
				return dret;
			}

			if (X > ExtrapolationXMin && X < GetMinX)
				State = AnalysisStatus.Extrapolated;

			if (X < ExtrapolationXMax && X > GetMaxX)
				State = AnalysisStatus.Extrapolated;
			
//			if (dret < ExtrapolationYMin)
//			{
//				State = AnalysisStatus.Underflow;
//				return ExtrapolationYMin;
//			}
//
//			if (dret > ExtrapolationYMax)
//			{
//				State = AnalysisStatus.Overflow;
//				return ExtrapolationYMax;
//			}

//			if (dret >= ExtrapolationYMin && dret < GetMinY)
//				State = AnalysisStatus.Extrapolated;
//
//			if (dret <= ExtrapolationYMax && dret > GetMaxY)
//				State = AnalysisStatus.Extrapolated;

			return SetValue(dret);
		}
		
		public double Extrapolation
		{
			get{return _extra;}
			set{_extra = SetValue(value);}
		}

		public double GetExtrapolationXMin
		{
			get{return ExtrapolationXMin;}
		}

		protected double ExtrapolationXMin
		{
			get{return _extraXMin;}
			set{_extraXMin = Math.Round(value, 4);}
		}

		public double GetExtrapolationYMin
		{
			get{return ExtrapolationYMin;}
		}

		protected double ExtrapolationYMin
		{
			get{return _extraYMin;}
			set{_extraYMin = Math.Round(value, 4);}
		}

		public double GetExtrapolationXMax
		{
			get{return ExtrapolationXMax;}
		}

		protected double ExtrapolationXMax
		{
			get{return _extraXMax;}
			set{_extraXMax = Math.Round(value, 4);}
		}

		public double GetExtrapolationYMax
		{
			get{return ExtrapolationYMax;}
		}

		protected double ExtrapolationYMax
		{
			get{return _extraYMax;}
			set{_extraYMax = Math.Round(value, 4);}
		}

//		public virtual double RSquare()
//		{
//			double sumXY = 0;
//			double sumX = 0;
//			double sumY = 0;
//			double sumX2 = 0;
//			double sumY2 = 0;
//			double rsquared = 0;
//			double yhat = 0;
//			double xhat = 0;
//			AnalysisStatus state;
//
//			for(int p = 0; p<Count; p++)
//			{
//				xhat = GetXFromY(YValue(p), out state);
//				yhat = ConvertLog_Y(GetYFromX(ConvertPow_X(XValue(p)), out state));
//				sumX += xhat;
//				sumY += YValue(p);
//				sumX2 += Math.Pow(xhat, 2);
//				sumY2 += Math.Pow(YValue(p), 2);
//				sumXY += xhat * YValue(p);
//			}
//
//			
//			rsquared = (Count*sumXY - sumX*sumY)/Math.Sqrt((Count*sumX2-Math.Pow(sumX,2))*(Count*sumY2-Math.Pow(sumY,2)));
//			Trace.WriteLine("RSquared: " + rsquared.ToString("0.0000"));
//			return rsquared;
//		}

		public virtual double RSquare()
		{
			double SSE = 0;
			double SST = 0;
			double yysum = 0;
			double ysum = 0;
			double rsquared = 0;
			double yhat = 0;
			AnalysisStatus state;

			try
			{
				for(var p = 0; p<Count; p++)
				{
					yhat = ConvertLog_Y(GetYFromX(ConvertPow_X(XValue(p)), out state));
					ysum += YValue(p);
					SSE += (YValue(p) - yhat) * (YValue(p) - yhat);
					yysum += YValue(p) * yhat;
				}

				SST = yysum - ysum * ysum / Count;
				rsquared = 1 - SSE / SST;
				Trace.WriteLine("RSquared: " + rsquared.ToString("0.0000"));
				return rsquared;
			}
			catch(Exception e)
			{
				Trace.WriteLine("RSquare: Error : " + e.Message);
			}
			return double.NaN;
		}

		protected void SetParameters(double[] Y, double[] X, int count, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
			_y = Y;
			_x = X;

			for(var i=0; i<Y.Length; i++)
			{
				_y[i] = SetValue(_y[i],5);
				_x[i] = SetValue(_x[i],5);
			}

			_xAxis = xAxis;
			_yAxis = yAxis;
			_count = count;
			Extrapolation = extrapolation;

			_cx = new double[_count];
			_cy = new double[_count];
			_bx = new double[_count];
			_bs = new AnalysisStatus[_count];

			SetCalcValues();
			SetExtrapolation();
		}

		private double GetMaxY
		{
			get
			{
				if (_y[0] > _y[Count-1])
					return _y[0];
				return _y[Count-1];
			}
		}

		private double GetMinY
		{
			get
			{
				if (_y[0] < _y[Count-1])
					return _y[0];
				return _y[Count-1];
			}
		}

		private double GetMaxX
		{
			get
			{
				if (_x[0] > _x[Count-1])
					return _x[0];
				return _x[Count-1];
			}
		}

		private double GetMinX
		{
			get
			{
				if (_x[0] < _x[Count-1])
					return _x[0];
				return _x[Count-1];
			}
		}

		protected void SetExtrapolation()
		{
			double xadd;
			xadd = (ConvertLog_X(Math.Abs(_x[0] - _x[Count-1]))) * _extra / 100;
			if (_x[0] < _x[Count-1])
			{
				ExtrapolationXMin = ConvertPow_X(ConvertLog_X(_x[0]) - xadd);
				ExtrapolationXMax = ConvertPow_X(ConvertLog_X(_x[Count-1]) + xadd);
			}
			else
			{
				ExtrapolationXMin = ConvertPow_X(ConvertLog_X(_x[Count-1]) - xadd);
				ExtrapolationXMax = ConvertPow_X(ConvertLog_X(_x[0]) + xadd);
			}
			
			xadd = (ConvertLog_Y(GetMaxY) - ConvertLog_Y(GetMinY)) * _extra / 100;
			if (_y[0] < _y[Count-1])
			{
				ExtrapolationYMin = GetMinY - ConvertPow_Y(xadd);
				ExtrapolationYMax = GetMaxY + ConvertPow_Y(xadd);
			}
			else
			{
				ExtrapolationYMin = GetMinY - ConvertPow_Y(xadd);
				ExtrapolationYMax = GetMaxY + ConvertPow_Y(xadd);
			}
			
			//Trace.WriteLine("Extrapolation MinX:{0} MaxX:{1} MinY:{2} MaxY:{3}", ExtrapolationXMin, ExtrapolationXMax, ExtrapolationYMin, ExtrapolationYMax);
		}

		protected double SetValue(double d)
		{
			return SetValue(d, _decPlaces);
		}

		protected double SetValue(double d, int decplaces)
		{
			if (double.IsNaN(d))
				return 0;
			return Math.Round(d, decplaces);
		}

		protected int Count
		{
			get{return _count;}
			set{_count = value;}
		}

		public virtual bool CheckMonotony
		{
			get{return _chkmonotony;}
			set{_chkmonotony = value;}
		}

			public AxisType XAxis
		{
			get{return _xAxis;}
			set{_xAxis = value;}
		}

		public AxisType YAxis
		{
			get{return _yAxis;}
			set{_yAxis = value;}
		}

		protected void SetCalcValues()
		{
			for(var i=0; i<_count; i++)
			{
				_cx[i] = _x[i];
				_cy[i] = _y[i];
				_bx[i] = 0;
				_bs[i] = AnalysisStatus.OK;

				_cx[i] = ConvertLog_X(_x[i]);
				_cy[i] = ConvertLog_Y(_y[i]);
			}
		}

		public bool IsIncreasing
		{
			get{return _increasing;}
			set{_increasing = value;}
		}

		private bool CheckInDecreasing(string valueType, double[] v)
		{
			double dTemp;
			var increasing = true;

			if (v[0] > v[_count - 1])
				increasing = false;

			dTemp = v[0];

			for(var i=1; i<_count; i++)
			{
				if (increasing)
				{
					if (dTemp >= v[i])	
						throw new Exception("StandardCurve" + valueType + "ValueNotMonotonIncreasing");
				}
				else
				{
					if (dTemp <= v[i])	
						throw new Exception("StandardCurve" + valueType + "ValueNotMonotonDecreasing");
				}
				dTemp = v[i];
			}
			return increasing;
		}

		protected void MonotonyCheck()
		{
			IsIncreasing = CheckInDecreasing("Y", _y);
            CheckInDecreasing("X", _x);
		}

        protected virtual void SortData(ref double[] yy, ref double[] xx)
        {
            double tempX = 0;
            double tempY = 0;

            for (var ix = 0; ix < xx.Length; ix++)
            {
                for (var i = 0; i < xx.Length - 1; i++)
                {
                    //if (xx[i] > xx[i + 1])
                    if ((_sortY && yy[i] > yy[i + 1]) || (!_sortY && xx[i] > xx[i + 1]))
                    {
                        tempX = xx[i];
                        tempY = yy[i];
                        xx[i] = xx[i + 1];
                        yy[i] = yy[i + 1];
                        xx[i + 1] = tempX;
                        yy[i + 1] = tempY;
                    }
                }
            }
        }

        protected double XValue(int index)
		{
			if (index >= 0 && index < _count)
				return _cx[index];
			return 0;
		}
		
		protected double YValue(int index)
		{
			if (index >= 0 && index < _count)
				return _cy[index];
			return 0;
		}

		protected double XValueOriginal(int index)
		{
			if (index >= 0 && index < _count)
				return _x[index];
			return 0;
		}
		
		protected double YValueOriginal(int index)
		{
			if (index >= 0 && index < _count)
				return _y[index];
			return 0;
		}

		public double GetBackFit_X(int index)
		{
			if (index >= 0 && index < _count)
				return _bx[index];
			return 0;
		}

		public AnalysisStatus GetBackFit_State(int index)
		{
			if (index >= 0 && index < _count)
				return _bs[index];
			return 0;
		}

		public void SetBackFit_X(int index, double backfit, AnalysisStatus state)
		{
			if (index >= 0 && index < _count)
			{
				_bx[index] = Math.Round(backfit, 3);
				_bs[index] = state;
			}
		}

		public double ConvertPow_Y(double Y)
		{
			if (YAxis == AxisType.linear)
				return Y;
			return Math.Pow(10, Y);
		}

		public double ConvertPow_X(double X)
		{
			if (XAxis == AxisType.linear)
				return X;
			return Math.Pow(10, X);
		}

		public double ConvertLog_Y(double Y)
		{
			if (YAxis == AxisType.linear)
				return Y;
			return Math.Log10(Y);
		}

		public double ConvertLog_X(double X)
		{
			if (XAxis == AxisType.linear)
				return X;
			return Math.Log10(X);
		}

	
		[DllImport("mathanal.Dll")]
		public static extern short CubicSpline(int count, double[] xValues, double[] yValues, double yValue, out double res);

		[DllImport("mathanal.Dll")]
		public static extern short FourParFitPar(int count, double[] xValues, double[] yValues, out double dA, out double dB, out double dC, out double dD);

		[DllImport("mathanal.Dll")]
		public static extern short FourParFitCalcX (double A, double B, double C, double D, double Y, out double X);

	}
	#endregion
	#region LineareRegressionClass
	public class LinearRegression : CurveBase
	{
		private double _yoffset;
		private double _slope;
		private double _correlation;
		private double _determination;
		private double _minCorrel;
		private bool _checkCorrel = false;

		public LinearRegression(){}
		
		public double YOffset
		{
            get { return Math.Round(_yoffset, 6); }
			set{_yoffset = value;}
		}

		public double Slope
		{
			get{return Math.Round(_slope, 6);}
			set{_slope = value;}
		}

		public double Correlation
		{
			get{return Math.Round(_correlation, 6);}
			set{_correlation = value;}
		}

		public double Determination
		{
			get{return Math.Round(_determination, 6);}
			set{_determination = value;}
		}

		public double MinimumCorrelation
		{
			get{return _minCorrel;}
			set{_minCorrel = SetValue(value);}
		}

		public bool CurveValid
		{
			get
			{
				if (!_checkCorrel)
					return true;

				if (Correlation >= MinimumCorrelation)
					return true;
				return false;
			}
		}

		public bool CheckCorrelation
		{
			get{return _checkCorrel;}
			set{_checkCorrel = value;}
		}

		public override void CalculateCurve(double[] Y, double[] X, int count, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
            _sortY = false;
            SortData(ref Y, ref X);

			SetParameters(Y, X, count, yAxis, xAxis, extrapolation);
			//base.MonotonyCheck();
			CalcCurve();

//			Trace.WriteLine("OffsetY:{0} Slope:{1} Deter:{2} Correl{3}", YOffset, Slope, Determination, Correlation);
		}

		protected override double CalcXFromY(double Y, out AnalysisStatus State)
		{
			State = AnalysisStatus.OK;
			return ConvertPow_X((ConvertLog_Y(Y) - YOffset)/_slope);
		}

		protected override double CalcYFromX(double X, out AnalysisStatus State)
		{
			State = AnalysisStatus.OK;
			return ConvertPow_Y((ConvertLog_X(X)*_slope)+YOffset);
		}

		private void CalcCurve()
		{
			double b = 0;
			double c = 0;
			double d = 0;
			double e = 0;
			double f = 0;
			double GG;
			double H;
			double j;
			double k;
			double m;
			double N;
			
			try
			{
				for(var x = 0; x < Count; x++)
				{
					b+= XValue(x);                              //Summe der Einheiten
					c+= XValue(x) * XValue(x);					//Quadrat der Einheiten
					d+= YValue(x);                              //Summe der ODWerte
					e+= YValue(x) * YValue(x);		            //Quadrat der ODWerte
					f+= YValue(x) * XValue(x);	                //ODWert*Units jeder Kontrolle
				}
			    
				GG = c - ((b * b) / Count);
				H = f - (b * d / Count);
				j = e - ((d * d) / Count);
				k = (H * H) / GG;
				m = b / Count;
				N = d / Count;
				Slope = H / GG;
				YOffset = N - m * _slope;
				Determination = k / j;
				Correlation = Math.Sqrt(Math.Abs(Determination)) * 100;
				Trace.WriteLine("LinReg Correlation: " + Correlation.ToString("0.000"));
			}
			catch(Exception exc)
			{
				Trace.WriteLine("LinearRegression: CalcCurve: " + exc.Message);
				throw new Exception("StandardCurveLinearRegressionCalcError");
			}
		}
	}
	#endregion
	#region PointToPointClass
	public class PointToPoint : CurveBase
	{
		public PointToPoint(){}
		public override void CalculateCurve(double[] Y, double[] X, int count, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
            _sortY = true;
            SortData(ref Y, ref X);
			SetParameters(Y, X, count, yAxis, xAxis, extrapolation);
			MonotonyCheck();
		}

		
		protected override double CalcXFromY(double Y, out AnalysisStatus State)
		{
			var x=0;
			var from = 0;
			var to = Count-1;
			State = AnalysisStatus.OK;

			if (Y == ExtrapolationYMin)
				from = 0;
			if (Y == ExtrapolationYMax)
				to = Count - 1;

			if (YValueOriginal(x)<YValueOriginal(x+1))
			{
				for(x = from; x<to; x++)
				{
				
					if ((YValueOriginal(x)<= Y && Y<=YValueOriginal(x+1)) ||
						(x==0 && Y<YValueOriginal(x)) ||
						(x==Count-2 && Y>YValueOriginal(x+1)))
						break;
				}
			}
			else
			{
				for(x = to-1; x>=from; x--)
				{

					if ((YValueOriginal(x+1)<=Y && Y<=YValueOriginal(x)) ||
						(x==to-1 && Y<YValueOriginal(x+1)) ||
						(x==from && Y>YValueOriginal(x)))
						break;
				}
			}
			return ConvertPow_X(XValue(x)+ ((XValue(x+1)-XValue(x))/(YValue(x+1)-YValue(x)))*(ConvertLog_Y(Y)-YValue(x)));

		}

		private double CalcY(double lowerY, double upperY, double lowerX, double upperX, double X)
		{
			return lowerY + ((upperY - lowerY) / (upperX - lowerX)) * (X - lowerX);
		}

		protected override double CalcYFromX(double X, out AnalysisStatus State)
		{
			State = AnalysisStatus.OK;

			if (X<ExtrapolationXMin)
			{
				State = AnalysisStatus.Underflow;
				return ExtrapolationXMin;
			}

			if (X>ExtrapolationXMax)
			{
				State = AnalysisStatus.Overflow;
				return ExtrapolationXMax;
			}

			if (XValueOriginal(0) < XValueOriginal(Count-1))
			{
				for(var i = 0; i<Count-1; i++)
				{
					if ((XValueOriginal(i)<=X && X<=XValueOriginal(i+1)) || 
						(i==0 && X<XValueOriginal(i)) ||
						(i==Count-2 && X>XValueOriginal(i))
						)
					{
						return ConvertPow_Y(CalcY(YValue(i), YValue(i+1), XValue(i), XValue(i+1), ConvertLog_X(X)));
					}
				}
			}

			for(var i = 0; i<Count-1; i++)
			{
				if ((XValueOriginal(i)>=X && X>=XValueOriginal(i+1)) ||
					(i==0 && X>XValueOriginal(i)) ||
					(i==Count-2 && X<XValueOriginal(i)))
				{
					return ConvertPow_Y(CalcY(YValue(i), YValue(i+1), XValue(i), XValue(i+1), ConvertLog_X(X)));
				}
			}

			throw new Exception("StandardCurveCannotCalculateConcentration");
		}
	}
	#endregion
	#region Cubic_SplineClass
	public class Cubic_Spline : CurveBase
	{
        //private double[] _y;
        //private double[] _x;

		double[] b;
		double[] d;
		double[] Q;
		double[] v;
		double[] AM;
		double[] AH;

		public Cubic_Spline(){}

		public override void CalculateCurve(double[] Y, double[] X, int count, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
            _sortY = false;
            SortData(ref Y, ref X);
			_y = Y;
			_x = X;
			SetParameters(Y, X, count, yAxis, xAxis, extrapolation);
			MonotonyCheck();
			SplineInit();
		}

		protected override double CalcXFromY(double Y, out AnalysisStatus State)
		{
			State = AnalysisStatus.OK;
			var xadd = (ExtrapolationXMax *1.01 - ExtrapolationXMin * 0.99)/2;
			var currX = ExtrapolationXMin + xadd;
			var Ymin = Y-0.00001;
			var Ymax = Y+0.00001;
			double currY = 0;

			for (var i=0; i<200; i++)
			{
				currY = CalcYFromX(currX, out State);
				if (Ymin<currY && currY<Ymax)
					return currX;

				xadd /=2;
				if (Y>currY)
				{
					if (YValueOriginal(0) < YValueOriginal(1))
						currX += xadd;
					else
						currX -= xadd;
				}
				else
				{
					if (YValueOriginal(0) < YValueOriginal(1))
						currX -= xadd;
					else
						currX += xadd;
				}
			}

			if (YValueOriginal(0) < YValueOriginal(1))
			{
				if (Y<YValueOriginal(0))
				{
					State = AnalysisStatus.Underflow;
					return ExtrapolationXMin - 0.001;
				}
				if (Y>YValueOriginal(Count-1))
				{
					State = AnalysisStatus.Overflow;
					return ExtrapolationXMax + 0.001;
				}
			}

			if (Y>YValueOriginal(0))
			{
				State = AnalysisStatus.Underflow;
				return ExtrapolationXMin - 0.001;
			}
			if (Y<YValueOriginal(Count-1))
			{
				State = AnalysisStatus.Overflow;
				return ExtrapolationXMax + 0.001;
			}

			throw new Exception("StandardCurveCannotCalculateConcentration");
		}

		private void SplineInit()
		{
			b = new double[Count + 2];
			d = new double[Count + 2];
			Q = new double[Count + 2];
			v = new double[Count + 2];
			AM = new double[Count + 2];
			AH = new double[Count + 2];
			
		
			AM[0] = 0;
			AM[Count-1] = 0;
			AH[1] = XValue(1) - XValue(0);

			for(var i=1; i<Count-1; i++)
			{
				AH[i+1] = XValue(i + 1) - XValue(i);
				b[i] = AH[i + 1] / (AH[i + 1] + AH[i]);
				v[i] = 1 - b[i];
				d[i] = 6 * ((YValue(i + 1) - YValue(i)) / AH[i + 1] - (YValue(i) - YValue(i - 1)) / AH[i]) / (AH[i + 1] + AH[i]);
			}

			Q[1] = 2;
			for(var i=1; i<Count-2;i++)
			{
				Q[i + 1] = 2 - b[i] * v[i + 1] / Q[i];
				d[i + 1] = d[i + 1] - d[i] * v[i + 1] / Q[i];
			}
		    
			AM[Count - 2] = d[Count - 2] / Q[Count - 2];

		    for(var i=Count-3; i>0; i--)
				AM[i] = (d[i] - b[i] * AM[i + 1]) / Q[i];
		}

		protected double Spline(double X)
		{
			double a;
			double b;
			double k;
			double t0;
			double T1;
			double T2;
			double T3;
			double t_Spline;
			var i=0;
			int s;
			int R;

		    s = 1;
		    R = Count;
		
			while (R-s>1)
			{
				i = (int)((R + s) / 2);
				if (XValue(i-1) > X)
					R=i;
				else
					s=i;
			}

		    k = AH[s];
		    a = (YValue(s) - YValue(s-1)) / k - (2 * AM[s-1] + AM[s]) * k / 6;
		    b = (AM[s] - AM[s-1]) / (6 * k);
		      
		    t0 = (X - XValue(s-1));
		    T1 = (YValue(s-1) + a * t0);
		    T2 = (AM[s-1] * t0 * t0 / 2);
		    T3 = (b * t0 * t0 * t0);
		    t_Spline = T1 + T2 + T3;

			return t_Spline;
		}

		protected override double CalcYFromX(double X, out AnalysisStatus State)
		{

			State = AnalysisStatus.OK;
			return ConvertPow_Y(Spline(ConvertLog_X(X)));
		}
	}
	#endregion
	#region FourParameterFitClass
	public class FourParameterFit : CurveBase
	{
		double _dA;
		double _dB;
		double _dC;
		double _dD;

		public FourParameterFit(){}

		public double Parameter_A
		{
			get{return _dA;}
			set{_dA = Math.Round(value,6);}
		}

		public double Parameter_B
		{
			get{return _dB;}
			set{_dB = Math.Round(value,6);}
		}

		public double Parameter_C
		{
			get{return _dC;}
			set{_dC = Math.Round(value,6);}
		}

		public double Parameter_D
		{
			get{return _dD;}
			set{_dD = Math.Round(value,6);}
		}

		public override void CalculateCurve(double[] Y, double[] X, int count, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
            _sortY = false;
            SortData(ref Y, ref X);
			SetParameters(Y, X, count, yAxis, xAxis, extrapolation);
			//base.MonotonyCheck();
			CalcCurve(Y, X);
		}

        protected override void SortData(ref double[] yy, ref double[] xx)
        {
            //double tempX = 0;
            //double tempY = 0;

            //for (int ix = 0; ix < xx.Length; ix++)
            //{
            //    for (int i = 0; i < xx.Length - 1; i++)
            //    {
            //        if (yy[i] > yy[i + 1])
            //        {
            //            tempX = xx[i];
            //            tempY = yy[i];
            //            xx[i] = xx[i + 1];
            //            yy[i] = yy[i + 1];
            //            xx[i + 1] = tempX;
            //            yy[i + 1] = tempY;
            //        }
            //    }
            //}


            double tempX = 0;
            double tempY = 0;

            for (var ix = 0; ix < xx.Length; ix++)
            {
                for (var i = 0; i < xx.Length - 1; i++)
                {
                    //if (xx[i] > xx[i + 1])
                    if ((_sortY && yy[i] > yy[i + 1]) || (!_sortY && xx[i] > xx[i + 1]))
                    {
                        tempX = xx[i];
                        tempY = yy[i];
                        xx[i] = xx[i + 1];
                        yy[i] = yy[i + 1];
                        xx[i + 1] = tempX;
                        yy[i + 1] = tempY;
                    }
                }
            }
        }
		protected override double CalcXFromY(double Y, out AnalysisStatus State)
		{
			State = AnalysisStatus.OK;
			double X = 0;
			var ret = FourParFitCalcX(Parameter_A, Parameter_B, Parameter_C, Parameter_D, Y, out X);

			if (ret == 1000)
			{
				if (YValueOriginal(0) < YValueOriginal(1))
				{
					State = AnalysisStatus.Overflow;
					return ExtrapolationXMax + 0.001;
				}
				else
				{
					State = AnalysisStatus.Underflow;
					return ExtrapolationXMin - 0.001;
				}
			}

			if (ret == 1001)
			{
				if (YValueOriginal(0) < YValueOriginal(1))
				{
					State = AnalysisStatus.Underflow;
					return ExtrapolationXMin - 0.001;
				}
				else
				{
					State = AnalysisStatus.Overflow;
					return ExtrapolationXMax + 0.001;
				}
			}

			if (ret != 0)
				throw new Exception("StandardCurveCannotCalculateConcentration");

			return X;
		}

		protected override double CalcYFromX(double X, out AnalysisStatus State)
		{
			State = AnalysisStatus.OK;
			return (Parameter_A-Parameter_D)/(Math.Pow(X/Parameter_C, Parameter_B)+1)+Parameter_D;
		}

        
		private void CalcCurve(double[] Y, double[] X)
		{
			try
			{
				var ret = FourParFitPar(Count, X, Y, out _dA, out _dB, out _dC, out _dD);
				if (ret != 0)
					throw new Exception("StandardCurveLinearRegressionCalcError");

				//Trace.WriteLine(String.Format("A:{0} B:{1} C:{2} D:{3}", Parameter_A, Parameter_B, Parameter_C, Parameter_D));
			}
			catch(Exception e)
			{
				Trace.WriteLine("FourParameterFit: CalcCurve: " + e.Message);
			}
		}
	}
	#endregion
	#region PolynomialClass
	public class Polynomial : CurveBase
	{
		double[] _Params;
		int _order = 3;

		public Polynomial()
		{
			_Params = new double[27];
		}

		public double Parameter(int index)
		{
			return _Params[index];
		}

		public void ParameterSet(double Value, int index)
		{
			_Params[index] = Math.Round(Value,6);
		}
	
		public int Order
		{
			get{//return _order;
                return _order-1;
            }
			set
			{
				_order = value+1;
				if (_order>26)
					_order = 27;
				if (_order<3)
					_order = 3;

				Trace.WriteLine("Setting Polynomial Order to " + _order.ToString());
			}
		}
		
		public override bool CheckMonotony
		{
			get
			{
				return base.CheckMonotony;
			}
			set
			{
				base.CheckMonotony = value;
			}
		}

		public override void CalculateCurve(double[] Y, double[] X, int count, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
            _sortY = false;
            SortData(ref Y, ref X);
			SetParameters(Y, X, count, yAxis, xAxis, extrapolation);
			if (base.CheckMonotony)
				MonotonyCheck();
			CalcCurve(_cy, X);
		}

		protected override double CalcXFromY(double Y, out AnalysisStatus State)
		{
			State = AnalysisStatus.OK;
			var xadd = (ExtrapolationXMax *1.01 - ExtrapolationXMin * 0.99)/2;
			var currX = ExtrapolationXMin + xadd;

            

			var Ymin = Y-0.00001;
			var Ymax = Y+0.00001;
			double currY = 0;

            //Y = ConvertLog_Y(Y);

			for (var i=0; i<200; i++)
			{
				currY = CalcYFromX(currX, out State);
				if (Ymin<currY && currY<Ymax)
					return currX;

				xadd /=2;
				if (Y>currY)
				{
					if (YValueOriginal(0) < YValueOriginal(1))
						currX += xadd;
					else
						currX -= xadd;
				}
				else
				{
					if (YValueOriginal(0) < YValueOriginal(1))
						currX -= xadd;
					else
						currX += xadd;
				}
			}

			if (YValueOriginal(0) < YValueOriginal(1))
			{
				if (Y<YValueOriginal(0))
				{
					State = AnalysisStatus.Underflow;
					return ExtrapolationXMin - 0.001;
				}
				if (Y>YValueOriginal(Count-1))
				{
					State = AnalysisStatus.Overflow;
					return ExtrapolationXMax + 0.001;
				}
			}

			if (Y>YValueOriginal(0))
			{
				State = AnalysisStatus.Underflow;
				return ExtrapolationXMin - 0.001;
			}
			if (Y<YValueOriginal(Count-1))
			{
				State = AnalysisStatus.Overflow;
				return ExtrapolationXMax + 0.001;
			}

			throw new Exception("StandardCurveCannotCalculateConcentration");
		}

		protected override double CalcYFromX(double X, out AnalysisStatus State)
		{
			State = AnalysisStatus.OK;
			double d = 0;
            double xv = 0;
			for(var x=_order; x>0; x--)
			{
                xv = X;
                if (XAxis == AxisType.logarithmic)
                    xv = Math.Log10(xv);
				d += _Params[x]*(Math.Pow(xv,x-1));
			}
			//d += _Params[_order];

			return ConvertPow_Y(d);
           // return d;
		}

		private void CalcCurve(double[] Y, double[] X)
		{
			try
			{
				var good = LLSqFit(Count, X, Y);
				if (!good)
					throw new Exception("StandardCurvePolynomialCalcError");
				_Params[0] = _Params[_order];
			}
			catch(Exception e)
			{
				Trace.WriteLine("Polynomial: CalcCurve: " + e.Message);
			}
		}

		private void GetLVals(double X, double[] PFuncVal, long[] func, bool[] inv, double[] n, double[] power)
		{
			//Get values for Linear Least Squares
			double v = 0;
            for (var i = 1; i <= _order; i++) 
			{
				switch(func[i])
				{
					case 0:
						v = 1;
						break;
					case 1:
						v = Math.Pow(X, power[i]);
						break;
				}
				if(inv[i])
				{
					if( v == 0)
						PFuncVal[i] = 0;
					else //'NOT V...
						PFuncVal[i] = 1 / v;
				}
				else //'INV(I) = FALSE
					PFuncVal[i] = v;
				
			}
		}
		
		private bool LLSqFit(int count, double[] X, double[] Y)
		{
			var Beta = new double[Count+1];
            var CoVar = new double[_order + 1, _order + 1];
            var PFuncVal = new double[_order + 1];
            var n = new double[_order + 1];
            var func = new long[_order + 1];
            var inv = new bool[_order + 1];
            var power = new double[_order + 1];
			_Params = new double[27];

            for (var i = 1; i <=_order; i++)
			{
				func[i] = 1;
				power[i] = i-1;
			}

			for(var i = 1; i<=count; i++)
			{
                var xv = X[i - 1];
                if (XAxis == AxisType.logarithmic)
                    xv = Math.Log10(xv);
                GetLVals(xv, PFuncVal, func, inv, n, power);
				//GetLVals(X[i-1], PFuncVal, func, inv, n, power);

                for (var L = 1; L <= _order; L++)
				{
					for(var m=1; m<=L; m++)
					{
						CoVar[L, m] = CoVar[L, m] + PFuncVal[L] * PFuncVal[m];
					}
					Beta[L] = Beta[L] + Y[i-1] * PFuncVal[L];
				}
			}
            for (var j = 2; j <= _order; j++)
			{
				for(var k=1; k<j; k++)
				{
					CoVar[k, j] = CoVar[j, k];
				}
			}
            if (GaussJordan(CoVar, _order+1, Beta))
			{
				//MsgBox "There was an error in the Linear Least Squares fit"
                for (var L = 1; L <= _order; L++)
				{
					_Params[L] = 0;
				}
				return false;
			}
			else //BAD = FALSE
			{
                for (var L = 1; L <= _order; L++)
				{
					_Params[L] = Beta[L];
				}
			}

			return true;
		}

		private bool GaussJordan(double[,] A, int n, double[] b)
		{
			//GaussJordan elimination for LLSq and LM solving
			var indxc = new long[n];
			var indxr = new long[n];
			var ipiv = new long[n];
			long icol = 0;
			long irow = 0;
			double Big = 0;
			double Dum = 0;
			double PivInv = 0;
		
			for(var i=1; i<n; i++)
			{
				Big = 0;
				for(var j=1; j<n; j++)
				{
					if(ipiv[j] != 1)
					{
						for(var k=1; k<n; k++)
						{
							if(ipiv[k] == 0)
							{
								if(Math.Abs(A[j, k]) >= Big)
								{
									Big = Math.Abs(A[j, k]);
									irow = j;
									icol = k;
								}
							}
						}
					}
				}
				ipiv[icol] = ipiv[icol] + 1;
				if(irow != icol)
				{
					for(var L=1; L<n; L++)
					{
						Dum = A[irow, L];
						A[irow, L] = A[icol, L];
						A[icol, L] = Dum;
					}
					Dum = b[irow];
					b[irow] = b[icol];
					b[icol] = Dum;
				}
				indxr[i] = irow;
				indxc[i] = icol;
				if(A[icol, icol] == 0)
				{
					//MsgBox "Sorry, the matrix was singular"
					return true;
				}
				PivInv = 1 / A[icol, icol];
				A[icol, icol] = 1;
				for(var L=1; L<n; L++)
				{
					A[icol, L] = A[icol, L] * PivInv;
				}
				b[icol] = b[icol] * PivInv;
				for(var ll=1; ll<n; ll++)
				{
					if(ll != icol)
					{
						Dum = A[ll, icol];
						A[ll, icol] = 0;
						for(var L=1; L<n; L++)
						{
							A[ll, L] = A[ll, L] - A[icol, L] * Dum;
						}
						b[ll] = b[ll] - b[icol] * Dum;
					}
				}
			}
			for(var L=n-1; L>0; L--)
			{
				if(indxr[L] != indxc[L])
				{
					for(var k=1; k<n; k++)
					{
						Dum = A[k, indxr[L]];
						A[k, indxr[L]] = A[k, indxc[L]];
						A[k, indxc[L]] = Dum;
					}
				}
			}
			return false;

		}
	}
	#endregion

	#region TestingClasses
	public class TestStandardCurve //: TestSet
	{
		#region PointToPoint
		//Y-Increasing, X-Increasing
        //[Test(1, 2, 3, 4, 5, 6, 100, 200, 300, 400, 500, 600, AxisType.linear, AxisType.linear, 5)]
        //[Test(100, 200, 300, 400, 500, 600, 1, 2, 3, 4, 5, 6, AxisType.linear, AxisType.linear, 5)]
        //[Test(0.001, 0.01, 0.1, 1, 10, 100, 1, 2, 3, 4, 5, 6, AxisType.logarithmic, AxisType.linear, 5)]
        //[Test(1, 2, 3, 4, 5, 6, 0.001, 0.01, 0.1, 1, 10, 100, AxisType.linear, AxisType.logarithmic, 5)]
        //[Test(0.1, 1, 10, 100, 1000, 10000, 0.001, 0.01, 0.1, 1, 10, 100, AxisType.logarithmic, AxisType.logarithmic, 5)]
		public void TestPointToPointIncreasing(double y1, double y2, double y3, double y4, double y5, double y6, double x1, double x2, double x3, double x4, double x5, double x6, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
			var Y = new double[6]{y1,y2,y3,y4,y5,y6};
			var X = new double[6]{x1,x2,x3,x4,x5,x6};
			var bs = AnalysisStatus.OK;

			var curve = new PointToPoint();
			curve.CalculateCurve(Y, X, Y.Length, yAxis, xAxis, extrapolation);
			double temp = -1;

			for(var i=0; i<Y.Length; i++)
			{
				temp = curve.GetXFromY(Y[i], out bs);
				curve.SetBackFit_X(i, temp, bs);
			}

            //for(int i=0; i<Y.Length; i++)
            //{
            //    if (curve.GetBackFit_State(i) == AnalysisStatus.OK)
            //        Assert(X[i] == curve.GetBackFit_X(i),"BackFit not equal Concentration");
            //    else
            //        Assert(X[i] != curve.GetBackFit_X(i), "State of BackFit value not OK");
            //}		

            //Assert(curve.Extrapolation == extrapolation, "Extrapolation protperty not set correctly");
            //Assert(curve.XAxis == xAxis, "XAxis protperty not set correctly");
            //Assert(curve.YAxis == yAxis, "YAxis protperty not set correctly");
            //Assert(curve.IsIncreasing, "IsIncreasing protperty not set correctly");

		}

		//Y-Decreasing, X-Increasing
        //[Test(1, 2, 3, 4, 5, 6, 100, 200, 300, 400, 500, 600, AxisType.linear, AxisType.linear, 5)]
        //[Test(100, 200, 300, 400, 500, 600, 1, 2, 3, 4, 5, 6, AxisType.linear, AxisType.linear, 5)]
        //[Test(0.001, 0.01, 0.1, 1, 10, 100, 1, 2, 3, 4, 5, 6, AxisType.logarithmic, AxisType.linear, 5)]
        //[Test(1, 2, 3, 4, 5, 6, 0.001, 0.01, 0.1, 1, 10, 100, AxisType.linear, AxisType.logarithmic, 5)]
        //[Test(0.1, 1, 10, 100, 1000, 10000, 0.001, 0.01, 0.1, 1, 10, 100, AxisType.logarithmic, AxisType.logarithmic, 5)]
		public void TestPointToPointDecreasing(double y1, double y2, double y3, double y4, double y5, double y6, double x1, double x2, double x3, double x4, double x5, double x6, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
			var Y = new double[6]{y6,y5,y4,y3,y2,y1};
			var X = new double[6]{x1,x2,x3,x4,x5,x6};
			var bs = AnalysisStatus.OK;

			var curve = new PointToPoint();
			curve.CalculateCurve(Y, X, Y.Length, yAxis, xAxis, extrapolation);
			double temp = -1;

			for(var i=0; i<Y.Length; i++)
			{
				temp = curve.GetXFromY(Y[i], out bs);
				curve.SetBackFit_X(i, temp, bs);
			}

            //for(int i=0; i<Y.Length; i++)
            //{
            //    if (curve.GetBackFit_State(i) == AnalysisStatus.OK)
            //        Assert(X[i] == curve.GetBackFit_X(i),"BackFit not equal Concentration");
            //    else
            //        Assert(X[i] != curve.GetBackFit_X(i), "State of BackFit value not OK");
            //}		

            //Assert(curve.Extrapolation == extrapolation, "Extrapolation protperty not set correctly");
            //Assert(curve.XAxis == xAxis, "XAxis protperty not set correctly");
            //Assert(curve.YAxis == yAxis, "YAxis protperty not set correctly");
            //Assert(!curve.IsIncreasing, "IsIncreasing protperty not set correctly");
		}
		#endregion
		#region LinearRegression
		//Y-Increasing, X-Increasing
        //[Test(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 100, 200, 300, 400, 500, 600, AxisType.linear, AxisType.linear, 5)]
        //[Test(100, 200, 300, 400, 500, 600, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, AxisType.linear, AxisType.linear, 5)]
        //[Test(1, 2, 3, 4, 5, 6, 100, 200, 300, 400, 500, 600, AxisType.linear, AxisType.linear, 5)]
        //[Test(100, 200, 300, 400, 500, 600, 1, 2, 3, 4, 5, 6, AxisType.linear, AxisType.linear, 5)]
        //[Test(0.001, 0.01, 0.1, 1, 10, 100, 1, 2, 3, 4, 5, 6, AxisType.logarithmic, AxisType.linear, 5)]
        //[Test(1, 2, 3, 4, 5, 6, 0.001, 0.01, 0.1, 1, 10, 100, AxisType.linear, AxisType.logarithmic, 5)]
        //[Test(0.1, 1, 10, 100, 1000, 10000, 0.001, 0.01, 0.1, 1, 10, 100, AxisType.logarithmic, AxisType.logarithmic, 5)]
		public void TestLinearRegressionIncreasing(double y1, double y2, double y3, double y4, double y5, double y6, double x1, double x2, double x3, double x4, double x5, double x6, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
			var Y = new double[6]{y1,y2,y3,y4,y5,y6};
			var X = new double[6]{x1,x2,x3,x4,x5,x6};
			var bs = AnalysisStatus.OK;

			var curve = new LinearRegression();
			curve.CalculateCurve(Y, X, Y.Length, yAxis, xAxis, extrapolation);
			double temp = -1;

			for(var i=0; i<Y.Length; i++)
			{
				temp = curve.GetXFromY(Y[i], out bs);
				curve.SetBackFit_X(i, temp, bs);
			}

            //for(int i=0; i<Y.Length; i++)
            //{
            //    if (curve.GetBackFit_State(i) == AnalysisStatus.OK)
            //        Assert(X[i] == curve.GetBackFit_X(i),"BackFit not equal Concentration");
            //    else
            //        Assert(curve.GetBackFit_State(i) == AnalysisStatus.Extrapolated, "State of BackFit value not OK");
            //}
		
            //Assert(curve.Correlation == 100, "Correlation not correct " + curve.Correlation.ToString());
            //Assert(curve.Extrapolation == extrapolation, "Extrapolation protperty not set correctly");
            //Assert(curve.XAxis == xAxis, "XAxis protperty not set correctly");
            //Assert(curve.YAxis == yAxis, "YAxis protperty not set correctly");
            //Assert(curve.IsIncreasing, "IsIncreasing protperty not set correctly");
		}

		//Y-Decreasing, X-Increasing
        //[Test(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 100, 200, 300, 400, 500, 600, AxisType.linear, AxisType.linear, 5)]
        //[Test(100, 200, 300, 400, 500, 600, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, AxisType.linear, AxisType.linear, 5)]
        //[Test(1, 2, 3, 4, 5, 6, 100, 200, 300, 400, 500, 600, AxisType.linear, AxisType.linear, 5)]
        //[Test(100, 200, 300, 400, 500, 600, 1, 2, 3, 4, 5, 6, AxisType.linear, AxisType.linear, 5)]
        //[Test(0.001, 0.01, 0.1, 1, 10, 100, 1, 2, 3, 4, 5, 6, AxisType.logarithmic, AxisType.linear, 5)]
        //[Test(1, 2, 3, 4, 5, 6, 0.001, 0.01, 0.1, 1, 10, 100, AxisType.linear, AxisType.logarithmic, 5)]
        //[Test(0.1, 1, 10, 100, 1000, 10000, 0.001, 0.01, 0.1, 1, 10, 100, AxisType.logarithmic, AxisType.logarithmic, 5)]
		public void TestLinearRegressionDecreasing(double y1, double y2, double y3, double y4, double y5, double y6, double x1, double x2, double x3, double x4, double x5, double x6, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
			var Y = new double[6]{y6,y5,y4,y3,y2,y1};
			var X = new double[6]{x1,x2,x3,x4,x5,x6};
			var bs = AnalysisStatus.OK;

			var curve = new LinearRegression();
			curve.CalculateCurve(Y, X, Y.Length, yAxis, xAxis, extrapolation);
			double temp = -1;

			for(var i=0; i<Y.Length; i++)
			{
				temp = curve.GetXFromY(Y[i], out bs);
				curve.SetBackFit_X(i, temp, bs);
			}

            //for(int i=0; i<Y.Length; i++)
            //{
            //    if (curve.GetBackFit_State(i) == AnalysisStatus.OK)
            //        Assert(X[i] == curve.GetBackFit_X(i),"BackFit not equal Concentration");
            //    else
            //        Assert(curve.GetBackFit_State(i) == AnalysisStatus.Extrapolated, "State of BackFit value not OK");
            //}
		
            //Assert(curve.Correlation == 100, "Correlation not correct " + curve.Correlation.ToString());
            //Assert(curve.Extrapolation == extrapolation, "Extrapolation protperty not set correctly");
            //Assert(curve.XAxis == xAxis, "XAxis protperty not set correctly");
            //Assert(curve.YAxis == yAxis, "YAxis protperty not set correctly");
            //Assert(!curve.IsIncreasing, "IsIncreasing protperty not set correctly");
		}

        //[Test(1, 2, 3, 4, 5, 6, 1, 2, 3, 4.9, 5, 6, AxisType.linear, AxisType.linear, 5)]
        //[Test(1, 2, 3, 4, 5, 6, 1, 2, 3, 4.9, 5, 6, AxisType.linear, AxisType.linear, 0)]
        //[Test(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.1, 0.2, 0.3, 0.49, 0.5, 0.6, AxisType.linear, AxisType.linear, 5)]
        //[Test(100, 200, 300, 400, 500, 600, 100, 200, 300, 490, 500, 600, AxisType.linear, AxisType.linear, 5)]
		public void TestLinearRegression2(double y1, double y2, double y3, double y4, double y5, double y6, double x1, double x2, double x3, double x4, double x5, double x6, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
			var Y = new double[6]{y1,y2,y3,y4,y5,y6};
			var X = new double[6]{x1,x2,x3,x4,x5,x6};

			var curve = new LinearRegression();
			curve.CalculateCurve(Y, X, Y.Length, yAxis, xAxis, extrapolation);
		
            //Assert(curve.Determination > 0.96, "Determination not correct");
            //Assert(curve.Determination < 0.97, "Determination not correct");
            //Assert(curve.Correlation > 98, "Correlation not correct");
            //Assert(curve.Correlation < 99, "Correlation not correct");
            //Assert(curve.Slope > 0.9, "Correlation not correct");
            //Assert(curve.Slope < 1.0, "Correlation not correct");
		}
		#endregion
		#region CubicSpline 
		//Y-Increasing, X-Increasing
        //[Test(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 100, 200, 300, 400, 500, 600, AxisType.linear, AxisType.linear, 5)]
        //[Test(100, 200, 300, 400, 500, 600, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, AxisType.linear, AxisType.linear, 5)]
        //[Test(1, 2, 3, 4, 5, 6, 100, 200, 300, 400, 500, 600, AxisType.linear, AxisType.linear, 5)]
        //[Test(100, 200, 300, 400, 500, 600, 1, 2, 3, 4, 5, 6, AxisType.linear, AxisType.linear, 5)]
        //[Test(0.001, 0.01, 0.1, 1, 10, 100, 1, 2, 3, 4, 5, 6, AxisType.logarithmic, AxisType.linear, 5)]
        //[Test(1, 2, 3, 4, 5, 6, 0.001, 0.01, 0.1, 1, 10, 100, AxisType.linear, AxisType.logarithmic, 5)]
        //[Test(0.1, 1, 10, 100, 1000, 10000, 0.001, 0.01, 0.1, 1, 10, 100, AxisType.logarithmic, AxisType.logarithmic, 5)]
		public void TestCubicSplineIncreasing(double y1, double y2, double y3, double y4, double y5, double y6, double x1, double x2, double x3, double x4, double x5, double x6, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
			var Y = new double[6]{y1,y2,y3,y4,y5,y6};
			var X = new double[6]{x1,x2,x3,x4,x5,x6};
			var bs = AnalysisStatus.OK;

			var curve = new Cubic_Spline();
			curve.CalculateCurve(Y, X, Y.Length, yAxis, xAxis, extrapolation);
			double temp = -1;

			for(var i=0; i<Y.Length; i++)
			{
				temp = curve.GetXFromY(Y[i], out bs);
				curve.SetBackFit_X(i, temp, bs);
			}

            //for(int i=0; i<Y.Length; i++)
            //{
            //    if (curve.GetBackFit_State(i) == AnalysisStatus.OK)
            //    {
            //        Assert(X[i] < curve.GetBackFit_X(i)+0.05,"BackFit not equal Concentration " + X[i].ToString() + " - " + curve.GetBackFit_X(i).ToString());
            //        Assert(X[i] > curve.GetBackFit_X(i)-0.05,"BackFit not equal Concentration " + X[i].ToString() + " - " + curve.GetBackFit_X(i).ToString());
            //    }
            //    else
            //        Assert(curve.GetBackFit_State(i)== AnalysisStatus.Extrapolated, "State of BackFit value not OK " + X[i].ToString() + " - " + curve.GetBackFit_X(i).ToString());
            //}

            //Assert(curve.Extrapolation == extrapolation, "Extrapolation protperty not set correctly");
            //Assert(curve.XAxis == xAxis, "XAxis protperty not set correctly");
            //Assert(curve.YAxis == yAxis, "YAxis protperty not set correctly");
            //Assert(curve.IsIncreasing, "IsIncreasing protperty not set correctly");
		}

		//Y-Decreasing, X-Increasing
        //[Test(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 100, 200, 300, 400, 500, 600, AxisType.linear, AxisType.linear, 5)]
        //[Test(100, 200, 300, 400, 500, 600, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, AxisType.linear, AxisType.linear, 5)]
        //[Test(1, 2, 3, 4, 5, 6, 100, 200, 300, 400, 500, 600, AxisType.linear, AxisType.linear, 5)]
        //[Test(100, 200, 300, 400, 500, 600, 1, 2, 3, 4, 5, 6, AxisType.linear, AxisType.linear, 5)]
        //[Test(0.001, 0.01, 0.1, 1, 10, 100, 1, 2, 3, 4, 5, 6, AxisType.logarithmic, AxisType.linear, 5)]
        //[Test(1, 2, 3, 4, 5, 6, 0.001, 0.01, 0.1, 1, 10, 100, AxisType.linear, AxisType.logarithmic, 5)]
        //[Test(0.1, 1, 10, 100, 1000, 10000, 0.001, 0.01, 0.1, 1, 10, 100, AxisType.logarithmic, AxisType.logarithmic, 5)]
		public void TestCubicSplineDecreasing(double y1, double y2, double y3, double y4, double y5, double y6, double x1, double x2, double x3, double x4, double x5, double x6, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
			var Y = new double[6]{y6,y5,y4,y3,y2,y1};
			var X = new double[6]{x1,x2,x3,x4,x5,x6};
			var bs = AnalysisStatus.OK;

			var curve = new Cubic_Spline();
			curve.CalculateCurve(Y, X, Y.Length, yAxis, xAxis, extrapolation);
			double temp = -1;

			for(var i=0; i<Y.Length; i++)
			{
				temp = curve.GetXFromY(Y[i], out bs);
				curve.SetBackFit_X(i, temp, bs);
			}

            //for(int i=0; i<Y.Length; i++)
            //{
            //    if (curve.GetBackFit_State(i) == AnalysisStatus.OK)
            //    {
            //        Assert(X[i] < curve.GetBackFit_X(i)+0.05,"BackFit not equal Concentration " + X[i].ToString() + " - " + curve.GetBackFit_X(i).ToString());
            //        Assert(X[i] > curve.GetBackFit_X(i)-0.05,"BackFit not equal Concentration " + X[i].ToString() + " - " + curve.GetBackFit_X(i).ToString());
            //    }
            //    else
            //        Assert(curve.GetBackFit_State(i)== AnalysisStatus.Extrapolated, "State of BackFit value not OK " + X[i].ToString() + " - " + curve.GetBackFit_X(i).ToString());
            //}

            //Assert(curve.Extrapolation == extrapolation, "Extrapolation protperty not set correctly");
            //Assert(curve.XAxis == xAxis, "XAxis protperty not set correctly");
            //Assert(curve.YAxis == yAxis, "YAxis protperty not set correctly");
            //Assert(!curve.IsIncreasing, "IsIncreasing protperty not set correctly");
		}
		#endregion
		#region FourParameterFit 
		//Y-Increasing, X-Increasing
		//[Test(2, 3, 4, 5, 6, 7, 0.00001, 0.01, 0.1, 1, 1000, 10000, AxisType.linear, AxisType.logarithmic, 10)]
		public void TestFourParameterFitIncreasing(double y1, double y2, double y3, double y4, double y5, double y6, double x1, double x2, double x3, double x4, double x5, double x6, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
			var Y = new double[5]{y1,y2,y3,y4,y5};
			var X = new double[5]{x1,x2,x3,x4,x5};
			var bs = AnalysisStatus.OK;

			var curve = new FourParameterFit();
			curve.CalculateCurve(Y, X, Y.Length, yAxis, xAxis, extrapolation);
			double temp = -1;

			for(var i=0; i<Y.Length; i++)
			{
				temp = curve.GetXFromY(Y[i], out bs);
				curve.SetBackFit_X(i, temp, bs);
			}

            //for(int i=1; i<Y.Length-1; i++)
            //{
            //    if (curve.GetBackFit_State(i) == AnalysisStatus.OK)
            //    {
            //        Assert(X[i] < curve.GetBackFit_X(i)+0.05,"BackFit not equal Concentration " + X[i].ToString() + " - " + curve.GetBackFit_X(i).ToString());
            //        Assert(X[i] > curve.GetBackFit_X(i)-0.05,"BackFit not equal Concentration " + X[i].ToString() + " - " + curve.GetBackFit_X(i).ToString());
            //    }
            //    else
            //        Assert(curve.GetBackFit_State(i)== AnalysisStatus.Extrapolated, "State of BackFit value not OK " + X[i].ToString() + " - " + curve.GetBackFit_X(i).ToString());
            //}

            //Assert(curve.Parameter_A < 1.95, "Parameter A not set correctly (high)" + curve.Parameter_A.ToString());
            //Assert(curve.Parameter_A > 1.92, "Parameter A not set correctly (low)");
            //Assert(curve.Parameter_B < 0.47, "Parameter B not set correctly (high)");
            //Assert(curve.Parameter_B > 0.45, "Parameter B not set correctly (low)");
            //Assert(curve.Parameter_C < 0.11, "Parameter C not set correctly (high)");
            //Assert(curve.Parameter_C > 0.09, "Parameter C not set correctly (low)");
            //Assert(curve.Parameter_D < 6.06, "Parameter D not set correctly (high)");
            //Assert(curve.Parameter_D > 6.04, "Parameter D not set correctly (low)");

            //Assert(curve.Extrapolation == extrapolation, "Extrapolation protperty not set correctly");
            //Assert(curve.XAxis == xAxis, "XAxis protperty not set correctly");
            //Assert(curve.YAxis == yAxis, "YAxis protperty not set correctly");
            //Assert(curve.IsIncreasing, "IsIncreasing protperty not set correctly");
		}

		//Y-Decreasing, X-Increasing
		//[Test(2, 3, 4, 5, 6, 7, 0.00001, 0.01, 0.1, 1, 1000, 10000, AxisType.linear, AxisType.logarithmic, 10)]
		public void TestFourParameterFitDecreasing(double y1, double y2, double y3, double y4, double y5, double y6, double x1, double x2, double x3, double x4, double x5, double x6, AxisType yAxis, AxisType xAxis, double extrapolation)
		{
			var Y = new double[5]{y5,y4,y3,y2,y1};
			var X = new double[5]{x1,x2,x3,x4,x5};
			var bs = AnalysisStatus.OK;

			var curve = new FourParameterFit();
			curve.CalculateCurve(Y, X, Y.Length, yAxis, xAxis, extrapolation);

			double temp = -1;

			for(var i=0; i<Y.Length; i++)
			{
				temp = curve.GetXFromY(Y[i], out bs);
				curve.SetBackFit_X(i, temp, bs);
			}

            //for(int i=1; i<Y.Length-1; i++)
            //{
            //    if (curve.GetBackFit_State(i) == AnalysisStatus.OK)
            //    {
            //        Assert(X[i] < curve.GetBackFit_X(i)+0.05,"BackFit not equal Concentration (high) " + X[i].ToString() + " - " + curve.GetBackFit_X(i).ToString());
            //        Assert(X[i] > curve.GetBackFit_X(i)-0.05,"BackFit not equal Concentration (low) " + X[i].ToString() + " - " + curve.GetBackFit_X(i).ToString());
            //    }
            //    else
            //        Assert(curve.GetBackFit_State(i)== AnalysisStatus.Extrapolated, "State of BackFit value not OK " + X[i].ToString() + " - " + curve.GetBackFit_X(i).ToString());
            //}

            //Assert(curve.Parameter_A < 1.95, "Parameter A not set correctly (high) " + curve.Parameter_A.ToString()+ "/" + curve.Parameter_B.ToString() + "/" + curve.Parameter_C.ToString() + "/" + curve.Parameter_D.ToString());
            //Assert(curve.Parameter_A > 1.93, "Parameter A not set correctly (low) " + curve.Parameter_A.ToString()+ "/" + curve.Parameter_B.ToString() + "/" + curve.Parameter_C.ToString() + "/" + curve.Parameter_D.ToString());
            //Assert(curve.Parameter_B < -0.45, "Parameter B not set correctly (high)");
            //Assert(curve.Parameter_B > -0.47, "Parameter B not set correctly (low)");
            //Assert(curve.Parameter_C < 0.11, "Parameter C not set correctly (high)");
            //Assert(curve.Parameter_C > 0.09, "Parameter C not set correctly (low)");
            //Assert(curve.Parameter_D < 6.06, "Parameter D not set correctly (high)");
            //Assert(curve.Parameter_D > 6.04, "Parameter D not set correctly (low)");

            //Assert(curve.Extrapolation == extrapolation, "Extrapolation protperty not set correctly");
            //Assert(curve.XAxis == xAxis, "XAxis protperty not set correctly");
            //Assert(curve.YAxis == yAxis, "YAxis protperty not set correctly");
            //Assert(!curve.IsIncreasing, "IsIncreasing protperty not set correctly");
		}
		#endregion
	}
	#endregion
}
