using System;

namespace Apex.Extensibility
{
	/// <summary>
	/// Summary description for OperationDescriptionAttribute.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method)]
	public class OperationDescriptionAttribute : Attribute
	{
		private string _name;
		private string _imageName;
		private string _toolTip;
		private int _order = 0;
		private string _grp;
		private string _property = string.Empty;
		private bool _propvalue;

		public OperationDescriptionAttribute(int order, string GlobalName, string ImageName, string ToolTip)
			:this( order, GlobalName, ImageName, ToolTip, "General"){}
		
		public OperationDescriptionAttribute(int order, string GlobalName, string ImageName, string ToolTip, string Group)
			:this(order, GlobalName, ImageName, ToolTip, Group, string.Empty, false){}

		public OperationDescriptionAttribute(int order, string GlobalName, string ImageName, string ToolTip, 
			string Group, string property, bool propertyValue)
		{
			_name = GlobalName;
			_imageName = ImageName;
			_toolTip = ToolTip;
			_order = order;
			_grp = Group;
			_property = property;
			_propvalue = propertyValue;
		}

		public string Name
		{
			get{ return _name;}
		}
		public string ImageName
		{
			get{ return _imageName;}
		}
		public string ToolTip
		{
			get{ return _toolTip;}
		}
		public int Order{get{return _order;}}
		public string Group{get{return _grp;}}
		public string Property{get{return _property;}}
		public bool PropertyValue{get{return _propvalue;}}
	}
}
