﻿///////////////////////////////////////////////////////////
//
//  Directorylookup.cs
//  Implementation of the Directory lookup class
//  Created on:      Jan 19, 2004
//  Original author: M Baumgartner
//  
///////////////////////////////////////////////////////////
//  Modification history:
//  
//
///////////////////////////////////////////////////////////
using Anthos.Utilities;
///
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;


namespace Anthos.ResourceReader
{
    /// <summary>
    /// Used to lookup the current application directory.
    /// </summary>
    public class DirectoryLookup
	{
		#region Members
		private static string _data;
		private static string _templates;
		private static string _temp;
		private static string _import;
		private static string _export;
		private static string _base;
		private static string _resources;
		private static string _help;
		private static string _filters;
		private static string _firmware;
		private static string _baseAllUsers;
		private static string _skins;

		#endregion
		#region Public Properties
		/// <summary>
		/// Returns the base application directory path (where the exe is).
		/// </summary>
		public static string GetBaseDir
		{
			get
			{	
				if (_base == null)
					_base = InitBaseDir;

				return _base;
			}
		    set { _base = value; }
		}

		/// <summary>
		/// Returns the base application directory path (where the exe is).
		/// </summary>
		public static string GetAllUsersBaseDir
		{
			get
			{	
				if (_baseAllUsers == null)
					_baseAllUsers = InitAllUsersBaseDir;

				return _baseAllUsers;
			}
		}
		/// <summary>
		/// Returns the templates directory - where labware and protocols live
		/// </summary>
		public static string GetTempDir
		{
			get 
			{	
				return CheckInitPath(ref _temp, "temp");
			}
			set 
			{
				_temp = CheckPath(value, _temp);
				SaveDirectoryInfo(_temp, "TEMPDirectory");
			}
		}

		/// <summary>
		/// Returns the templates directory - where labware and protocols live
		/// </summary>
		public static string GetTemplateDir
		{
			get 
			{	
				return CheckInitPath(ref _templates, "templates");
			}
			set 
			{
				_templates = CheckPath(value, _templates);
				SaveDirectoryInfo(_templates, "TEMPLATESDirectory");
			}
		}

		/// <summary>
		/// Returns the templates directory - where labware and protocols live
		/// </summary>
		public static string GetFiltersDir
		{
			get 
			{	
				return CheckInitPath(ref _filters, "filters");
			}
			set 
			{
				_filters = CheckPath(value, _filters);
				SaveDirectoryInfo(_filters, "FILTERSDirectory");
			}
		}

		/// <summary>
		/// Returns the skins directory - where skin files for readers lie
		/// </summary>
		public static string GetSkinsDir
		{
			get 
			{	
				return CheckInitPath(ref _skins, "skins");
			}
			set 
			{
				_skins = CheckPath(value, _skins);
				SaveDirectoryInfo(_skins, "SKINSDirectory");
			}
		}

		/// <summary>
		/// Returns the export directory 
		/// </summary>
		public static string GetFirmwareDir
		{
			get 
			{	
				return CheckInitPath(ref _firmware, "firmware");
			}
			set 
			{
				_firmware = CheckPath(value, _firmware);
				SaveDirectoryInfo(_firmware, "FIRMWAREDirectory");
			}
		}

		/// <summary>
		/// Returns the import directory 
		/// </summary>
		public static string GetImportDir
		{
			get 
			{	
				return CheckInitPath(ref _import, "import");
			}
			set 
			{
				_import = CheckPath(value, _import);
				SaveDirectoryInfo(_import, "IMPORTDirectory");
			}
		}

		/// <summary>
		/// Returns the export directory 
		/// </summary>
		public static string GetExportDir
		{
			get 
			{	
				return CheckInitPath(ref _export, "export");
			}
			set 
			{
				_export = CheckPath(value, _export);
				SaveDirectoryInfo(_export, "EXPORTDirectory");
			}
		}

		/// <summary>
		/// Sets the templates directory for running through automation - where labware and protocols live
		/// </summary>
		public static string AutomationTemplateDir
		{
			get 
			{	
				return CheckInitPath(ref _templates, "templates");
			}
			set 
			{
                Trace.WriteLine("Changing Template Path: " + value);
				_templates = CheckPath(value, _templates);
			}
		}
		private static string CheckInitPath(ref string path, string defaultpath)
		{
			if (path == null)
				path = ReadDirectoryInfo(defaultpath);

			if (!Directory.Exists(path))
				CreateDir(path);

			return path;
		}

		/// <summary>
		/// Returns the CommonApplication directory
		/// </summary>
		public static string GetCommonAppDir
		{
			get 
			{
				return Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
			}
		}
		/// <summary>
		/// Returns the data directory
		/// </summary>
		public static string GetDataDir
		{
			get 
			{
				return CheckInitPath(ref _data, "data");
			}
			set 
			{
				_data = CheckPath(value, _data);
				SaveDirectoryInfo(_data, "DATADirectory");
			}
		}
		/// <summary>
		/// Returns the help directory
		/// </summary>
		public static string GetHelpDir
		{
			get 
			{
				return CheckInitPath(ref _help, "help");
			}
			set 
			{
				_help = CheckPath(value, _help);
				SaveDirectoryInfo(_help, "HELPDirectory");
			}
		}

		/// <summary>
		/// Returns the resource directory
		/// </summary>
		public static string GetResourcesDir
		{
			get 
			{
				return CheckInitPath(ref _resources, "resources");
			}
			set 
			{
				_resources = CheckPath(value, _resources);
				SaveDirectoryInfo(_resources, "RESOURCESirectory");
			}
		}

        /// <summary>
        /// Returns log directory for Images for Plateheight detection in Gimli
        /// </summary>
	    public static string GetGimliPlateHeightDir
	    {
	        get
	        {

	            var directory = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
	                            @"\Molecular Devices\GimliData\logs\PlateHeight\";

                try
	            {
	                if (!Directory.Exists(directory))
	                    Directory.CreateDirectory(directory);
	            }
	            catch (Exception exc)
	            {
	                Trace.WriteLine($"Error creating Folder: {directory}\r\n{exc.Message}");
	            }
                
                return directory;
            }
	    }

		#endregion
		#region Public Methods
		/// <summary>
		/// creates a new directory.  If dir can't be created, returns false.
		/// </summary>
		public static bool CreateDir(string path)
		{
			try	
			{
				Directory.CreateDirectory(path);
			}
			catch (Exception e)	
			{
				Trace.WriteLine(String.Format("Cannot create directory ({0}) :{1}.", path, e.Message));
                //MessageBox.Show(string.Format("{0} ({1})", Controls.Properties.Resources.ErrorCannotCreateDirectory, path),
                //                              Controls.Properties.Resources.Error);
				return false;
			}
			return true;
		}
		public static string CheckIfSimFileExists(string fname)
		{

			if (fname == null || fname.Length < 1)
				return "";
			// make sure file has a path
			if (File.Exists(fname))
				if (fname.IndexOf(@"\") != -1)
					return fname;
			
			if (File.Exists(GetDataDir + fname))
				return GetDataDir + fname;

			if (File.Exists(GetTemplateDir + fname))
				return GetTemplateDir + fname;

			if (File.Exists(GetBaseDir + fname))
				return GetBaseDir + fname;

			return "NOT_FOUND";
		}

		public static string CheckSimulationFile(string fname, bool ShowMessage)
		{
			var filename = CheckIfSimFileExists(fname);

			if (filename == "NOT_FOUND")
			{
				filename = fname;
				//if (ShowMessage)
    //                MessageBox.Show(string.Format("{0} ({1})", Controls.Properties.Resources.ErrorSimFileNotFound, fname), Controls.Properties.Resources.Error);
			}

			return fname;
		}

		public static string GetSimulationFile(string keyname)
		{
			var fname = AppSetting.All.System[keyname].ToString();
			var filename = CheckIfSimFileExists(fname);

			if (filename == "NOT_FOUND")
				filename = fname;

			return filename;
		}

		public static string GetSimulationFile(string keyname, string defaultName)
		{
			var fname = AppSetting.All.System[keyname].ToString();

			if (fname == "")
				fname = defaultName;

			var filename = CheckIfSimFileExists(fname);

			if (filename == "NOT_FOUND")
				filename = fname;

			if (fname == defaultName)
				AppSetting.All.System[keyname] = filename;

			return filename;
		}
		#endregion
		#region Private Methods
		private static void SaveDirectoryInfo(string path, string sectionname)
		{
			if (path != null)
			{
				//AppSetting.All.System[sectionname] = path.ToLower();
			}
		}

		private static string ReadDirectoryInfo(string defaultpath)
		{
			try
			{
				var temppath = AppSetting.All.System[defaultpath.ToUpper() + "Directory"].ToString();

				if (temppath.Length > 2 && Directory.Exists(temppath))
					return temppath;
			}
			catch (Exception e)
			{
				Trace.WriteLine("Error directory lookup" + e.Message);
			}

			if (defaultpath == "import" || defaultpath == "export" || defaultpath == "firmware")
				return Environment.GetFolderPath(Environment.SpecialFolder.Personal);

			if (defaultpath == "resources")
				return GetBaseDir + defaultpath + @"\";

			if (defaultpath == "help" || defaultpath == "manuals")
			{
				if (Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) + @"\Molecular Devices\Manuals\"))
					return Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) + @"\Molecular Devices\Manuals\";
				return Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) + @"\Beckman Coulter\Manuals\";
			}

			return GetAllUsersBaseDir + defaultpath + @"\";
		}

		/// <summary>
		/// Makes sure that a backslash is at the end of the given string.
		/// </summary>
		private static string IncludeTrailingBackslash(string directoryName)
		{
			if (directoryName.EndsWith(@"\"))
				return directoryName;
			else
				return (directoryName + @"\");
		}

		private static string CheckPath(string path, string defaultpath)
		{
			if (path != null)
			{
				path = IncludeTrailingBackslash(path);
				if (Directory.Exists(path))
					return path;

				if (CreateDir(path))
					return path;
			}
			return defaultpath;
		}
		#endregion
		#region Private Properties
		private static string InitBaseDir
		{
			get
			{
				   string path;
				   try
				   {
					   path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToLower();
				   }
				   catch (Exception e)
				   {
					   Trace.WriteLine ("Path not found: " + e.Message);
					   path = Directory.GetCurrentDirectory();
					   Trace.WriteLine ("Setting path to: " + path);
				   }
				   // if bin\debug is found, we're on a development machine
                   if (path.IndexOf(@"\bin\debug") != -1 || path.IndexOf(@"\bin\x86") != -1 || path.IndexOf(@"\bin\release") != -1)
                   {
                       if (IsMultimode)
                           _base = @"c:\projects\multimode\apex\apex\bin\debug\";
                       else
                       {
                           _base = @"c:\projects\ThunderdomePlatform\thunderdome\source\output\bin\debug\readers\";
                           if (path.IndexOf(@"\bin\release") != -1)
                               _base = _base.Replace(@"\debug\", @"\release\");
                       }
                   }
                   else
                       _base = IncludeTrailingBackslash(path);
				
				   return _base;
			   }
		}
        private static string mVersionSubFolder;
	    public static string VersionSubFolder
	    {
            get
            {
                if (string.IsNullOrEmpty(mVersionSubFolder))
                    return mVersionSubFolder = GetVersionSubFolder();
                return mVersionSubFolder; 
            }
	        set { mVersionSubFolder = value; }
	    }
        private static string GetVersionSubFolder()
        {
            if (!string.IsNullOrEmpty(mVersionSubFolder))
                return mVersionSubFolder;

            return smpVersion;
        }

        private static string smpVersion = "SMP71";
        public static string SMPVersion { get { return smpVersion; } set { smpVersion = value; } }
	    private static string InitAllUsersBaseDir
		{
			get
			{
				string path;
				try
				{
                    var addPath = "Multimode\\Detection Software";
					path = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
                    if (IsMultimode)
					    path = Path.Combine(path, addPath);
                    else
                        path = Path.Combine(path, string.Format("Molecular Devices\\{0}\\{1}", VersionSubFolder, addPath));
                    Trace.WriteLine("DirectoryLookup returns path : " + path);
				}
				catch (Exception e)
				{
					Trace.WriteLine ("Path not found: " + e.Message);
					path = Directory.GetCurrentDirectory();
					Trace.WriteLine ("Setting path to: " + path);
				}
				_baseAllUsers = IncludeTrailingBackslash(path);
				
				return _baseAllUsers;
			}
		}

        private static bool mIsMultimode = false;
        public static bool IsMultimode
        {
            get { return mIsMultimode; }
            set { mIsMultimode = value; }
        }

        public static bool IsSMP
	    {
	        get
	        {
                if (!IsMultimode)
                    Trace.WriteLine("This is a SMP Version of Anthos.dll");
                return !IsMultimode;
            }
	    }

#endregion
    }
}
