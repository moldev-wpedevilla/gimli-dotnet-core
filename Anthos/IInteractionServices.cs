﻿
namespace Anthos.Controls
{
    public class IInteractionServices
    {
        public interface IInteractionService
        {
            void NotifyUser(string message, string caption);
            bool NotifyUserAndGetAcknowledgeYesNo(string message, string caption, bool defaultYes);
            bool NotifyUserAndGetAcknowledgeOkCancel(string message, string caption, bool defaultOk);
            bool NotifyUserAndGetAcknowledgeRetryCancel(string message, string caption, bool defaultRetry);
        }
    }
}
