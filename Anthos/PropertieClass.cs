﻿using System.Reflection;

namespace Anthos.Controls
{
    public class PropertieClass
    {

        public static string GetText(string key)
        {
            try
            {
                return typeof(Properties.Resources).GetProperty(key, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.IgnoreCase).GetValue(null, null).ToString();
            }
            catch {
                System.Diagnostics.Trace.WriteLine("Key not found: " + key);
            }

            key = key.Replace("______", " ");
            key = key.Replace("_____", "}{");
            key = key.Replace("____", "}");
            key = key.Replace("___", "{");
            key = key.Replace("__", ":");
            key = key.TrimStart(new char[]{'_'});
            key = key.Replace("_", ".");

            return key;
        }
    }
}
