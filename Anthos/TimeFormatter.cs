namespace Anthos.Utilities
{
	/// <summary>
	/// Summary description for TimeFormatter.
	/// </summary>
	public class TimeFormatter
	{
		private string dayname = Controls.Properties.Resources.Days;
		public TimeFormatter()
		{
		}

		public string GetKineticTimeFromSeconds(int secondTicks)
		{
			var seconds = secondTicks;
			
			var days = CheckMultiples(86400, ref seconds);
			var hours = CheckMultiples(3600, ref seconds);
			var minutes = CheckMultiples(60, ref seconds);

			
			if (days > 0)
				return string.Format("{3} {4}, {0}:{1}:{2}", hours.ToString("d02"), 
															 minutes.ToString("d02"), 
															 seconds.ToString("d02"), 
															 days, dayname);

			return string.Format("{0}:{1}:{2}", hours.ToString("d02"), 
												minutes.ToString("d02"), 
												seconds.ToString("d02"));
		}

		private int CheckMultiples(int ticks, ref int seconds)
		{
			var x = 0;
			while (seconds >= ticks)
			{
				seconds -= ticks;
				x++;
			}
			return x;
		}
	}
}
