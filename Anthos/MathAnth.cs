using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Anthos.Analysis
{
	/// <summary>
	/// Summary description for MathAnth.
	/// </summary>
	public class MathAnth 
	{
		static int _decPlaces = 3;
        static string _plateName = Controls.Properties.Resources.Plate;

		public MathAnth()
		{}

        public static string PlateName { get { return _plateName; } }
		public static string RDCName{get{return "RDC";}}
		public static string ResName{get{return "REDUCTION";}}
		public static double Mean(double[] arValues)
		{ 
			AnalysisStatus status; 
			return Round(CalcMean(arValues, true, out status));
		}

        public static double Decrease(List<double> yValues, List<double> xValues, double percent, out AnalysisStatus state)
        {
            state = AnalysisStatus.NotEvaluated;
            if (yValues == null || xValues == null)
                return 0;
            double dX = 0;

            try
            {
                if (yValues.Count == 0 || xValues.Count == 0)
                    return 0;

                var plateau = yValues[yValues.Count - 1];
                var span = yValues[0] - plateau;
                var dY = span * percent / 100 + plateau;

                state = AnalysisStatus.Overflow;
                if (dY > yValues[0])
                    return xValues[0];

                state = AnalysisStatus.Underflow;
                if (dY < yValues[yValues.Count - 1])
                    return xValues[xValues.Count - 1];

                state = AnalysisStatus.OK;
                for (var i = 0; i < yValues.Count - 1; i++)
                {
                    if (yValues[i] >= dY && dY >= yValues[i + 1])
                    {
                        return xValues[i] + ((xValues[i + 1] - xValues[i]) / (yValues[i + 1] - yValues[i])) * (dY - yValues[i]);
                    }
                }
            }
            catch { state = AnalysisStatus.Error; }
            return dX;
        }

        public static double Increase(List<double> yValues, List<double> xValues, double percent, out AnalysisStatus state)
        {
            state = AnalysisStatus.NotEvaluated;
            if (yValues == null || xValues == null)
                return 0;
            
            double dX = 0;
            try
            {
                if (yValues.Count == 0 || xValues.Count == 0)
                    return 0;

                var plateau = yValues[0];
                var span = yValues[yValues.Count - 1] - plateau;
                var dY = span * percent / 100 + plateau;

                state = AnalysisStatus.Underflow;
                if (dY < yValues[0])
                    return xValues[0];

                state = AnalysisStatus.Overflow;
                if (dY > yValues[yValues.Count - 1])
                    return xValues[xValues.Count - 1];

                state = AnalysisStatus.OK;
                for (var i = 0; i < yValues.Count - 1; i++)
                {
                    if (yValues[i] <= dY && dY <= yValues[i + 1])
                    {
                        return xValues[i] + ((xValues[i + 1] - xValues[i]) / (yValues[i + 1] - yValues[i])) * (dY - yValues[i]);
                    }
                }
            }
            catch { state = AnalysisStatus.Error; }
            return dX;
        }

        public static double Median(double[] arValues)
        {
            var dL = new List<double>(arValues);
            return Median(dL);
        }
        public static double Median(List<double> arValues)
        {
            if (arValues.Count == 0)
                return double.NaN;

            if (arValues.Count == 1)
                return arValues[0];

            arValues.Sort();
            double mean = 0;
            double arlC = (arValues.Count-1) / 2f;
            if (Math.IEEERemainder((double)(arValues.Count-1), (double)2f) != 0)
            {
                mean = (double)(((double)arValues[(int)Math.Floor(arlC)] + (double)arValues[(int)Math.Floor(arlC)+1]) / 2f);
            }
            else
                mean = (double)arValues[(int)arlC];

            return mean;
        }
        public static double Mean(double[] arValues, out AnalysisStatus status)
		{
			return Round(CalcMean(arValues, true, out status));
		}

        public static double Mean(double[] arValues, out AnalysisStatus status, out double cv)
		{
			double sd;
			return Mean(arValues, out status, out cv, out sd, true);
		}

        public static double Mean(double[] arValues, out AnalysisStatus status, out double cv, out double sd)
		{
			return Mean(arValues, out status, out cv, out sd, true);
		}
        public static double Mean(double[] arValues, out AnalysisStatus status, out double cv, out double sd, bool arithMean)
		{
			double   dMean = 0;

			cv = double.NaN;
			sd = double.NaN;
			status = AnalysisStatus.Error;
			
			dMean = CalcMean(arValues, arithMean, out status);
			
			cv = CV(arValues);
			sd = SD(arValues);

			return Round(dMean);
		}

		private static double CalcMean(double[] arValues, bool arithMean, out AnalysisStatus status)
		{
			double   dMean = 0;
			status = AnalysisStatus.Error;

			if (arValues == null || arValues.Length < 1)
				return double.NaN;

			try
			{
				if (arithMean)
				{
					for (var i = 0; i < arValues.Length; i++ )
						dMean += arValues[i];
					dMean /= arValues.Length;
				}
				else
				{
					for (var i = 0; i < arValues.Length; i++ )
						dMean *= arValues[i];
					Math.Pow(dMean, 1 / arValues.Length);
				}
			}
			catch(Exception e)
			{
				Trace.WriteLine("MathAnth: Mean: " + e.Message);
				status = AnalysisStatus.Error;
				return double.NaN;
			}

			status = AnalysisStatus.OK;
			return dMean;
		}

        public static double CV(double[] d)
		{
			double  rTemp;
			var  rStd 	   = 0.0;
			AnalysisStatus status;

			var mean = CalcMean(d, true, out status);

            if (status == AnalysisStatus.OK)
            {
                for (var i = 0; i < d.Length; i++)
                {
                    rTemp = d[i] - mean;
                    rTemp *= rTemp;
                    rStd += rTemp;
                }

                if (d.Length > 1)
                {
                    rStd /= d.Length - 1;
                    rStd = Math.Sqrt(rStd);
                    if (mean != 0.0)
                        return Math.Abs(Round(100 * rStd / mean, 3));
                }
            }

            return double.NaN; ;
		}

        public static double SD(double[] d)
		{
			double  rTemp;
			var  rStd 	   = 0.0;
			AnalysisStatus status;

			var mean = CalcMean(d, true, out status);
			
			if (status == AnalysisStatus.OK)
			{
				for (var i = 0; i < d.Length; i++ )
				{
					rTemp  = d[i] - mean;
					rTemp *= rTemp;
					rStd  += rTemp;
				}

				if ( d.Length > 1 )
				{
					rStd /= d.Length - 1;
                    return Round(Math.Sqrt(rStd), 3);
				}
			}
			return double.NaN;
		}

		public static double Round(double d)
		{
			return Round(d, _decPlaces);
		}

		public static double Round(double d, int decplaces)
		{
			if (double.IsNaN(d))
				return double.NaN;
			return Math.Round(d, decplaces);
		}

		public static double mP(double dPara, double dPerp, double G, out AnalysisStatus status)
		{
			double tmp;			
			try
			{
				tmp = (1-G*dPerp/dPara)/(1+G*dPerp/dPara)*1000;
			}
			catch(Exception e)
			{
				Trace.WriteLine("MathAnth: mP: " + e.Message);
				status = AnalysisStatus.Error;
				return double.NaN;
			}
			status = AnalysisStatus.OK;
			return Round(tmp, 0);
		}

        public static double mP(double dPara, double dPerp, double G, int decplaces, out AnalysisStatus status)
        {
            double tmp;
            try
            {
                tmp = (1 - G * (dPerp / dPara)) / (1 + G * (dPerp / dPara)) * 1000;
            }
            catch (Exception e)
            {
                Trace.WriteLine("MathAnth: mP: " + e.Message);
                status = AnalysisStatus.Error;
                return double.NaN;
            }
            status = AnalysisStatus.OK;
            return Round(tmp, decplaces);
        }

		public static double PolrizationAnisotropy(double dPara, double dPerp, double G, out AnalysisStatus status)
		{
			double res, mp;
			AnalysisStatus stat;
			
			//get mP first
			mp = mP(dPara, dPerp, G, out stat);
			if(stat == AnalysisStatus.OK)
			{
				try
				{
					res = (2*mp/1000)/(3-mp/1000);
				}
				catch(Exception e)
				{
					Trace.WriteLine("MathAnth: PolarizationAnisotropy: " + e.Message);
					status = AnalysisStatus.Error;
					return double.NaN;
				}
				status = AnalysisStatus.OK;
				return Round(res, 3);
			}			
			status = stat;
			return double.NaN;
		}
		
		public static double PolrizationTotalIntensity(double dPara, double dPerp, double G)
		{
			return Round(dPara + 2*G*dPerp, 0);
		}

		public static double Min(double[] d)
		{
			if(d.Length < 1)
				throw new ArgumentException();
			var  rTemp = d[0];			
			
			for(var i = 1;i < d.Length; i++)
			{
				if(d[i]<rTemp && !double.IsNaN(d[i]))
					rTemp = d[i];				
			}			

			return rTemp;
		}

        public static double Delta(double[] d)
        {
            if (d.Length < 1)
                throw new ArgumentException();

            return Max(d) - Min(d);
        }

		public static double Max(double[] d)
		{
			if(d.Length < 1)
				throw new ArgumentException();
			var  rTemp = d[0];			
			
			for(var i = 1;i < d.Length; i++)
			{
				if(d[i]>rTemp && !double.IsNaN(d[i]))
					rTemp = d[i];				
			}			

			return rTemp;
		}

		public static double Sum(double[] d)
		{
			double  rTemp = 0;			
			
			for(var i = 0;i < d.Length; i++)
			{
                if (!double.IsNaN(d[i]))
				    rTemp += d[i];				
			}			

			return rTemp;
		}

		public static bool HasLogicalOperators(string f)
		{
			if (f == null || f == string.Empty)
				return false;

            if (Utilities.StringFormatter.HasWord(f, "OR"))
                return true;
            if (Utilities.StringFormatter.HasWord(f, "XOR"))
				return true;
            if (Utilities.StringFormatter.HasWord(f, "AND"))
                return true;
            if (Utilities.StringFormatter.HasWord(f, "NOT"))
				return true;
			if (f.ToUpper().IndexOf("&")>-1)
				return true;
			if (f.ToUpper().IndexOf("|")>-1)
				return true;
			if (f.ToUpper().IndexOf("!")>-1)
				return true;
			if (f.ToUpper().IndexOf("~")>-1)
				return true;
            if (Utilities.StringFormatter.HasWord(f, "TRUE~"))
				return true;
            if (Utilities.StringFormatter.HasWord(f, "FALSE"))
				return true;

			return false;
		}

		public static bool HasRelationalOperators(string f)
		{
			if (f == null || f == string.Empty)
				return false;

			if (f.IndexOfAny(new char[]{'<', '>', '=', '!'})>-1)
				return true;

            if (Utilities.StringFormatter.HasWord(f, "EQUAL"))
				return true;
			return false;
		}
		
		public static double LinearYRegression(double[] arXValues, double[] arYValues, out double dIntercept, 
			out double dCC)
		{
			//Debug.Assert(arXValues.Length == arYValues.Length);
			//Debug.Assert(arXValues.Length > 1);

			int  i, n;
			double rXMean, rYMean, dSlope;
			double dSdxdy = 0.0, dSdx2  = 0.0, dSxy = 0.0, dSx = 0.0, dSy = 0.0, dSx2 = 0.0, dSy2 = 0.0, dTemp;

			rXMean = Mean(arXValues);
			rYMean = Mean(arYValues);

			n = arXValues.Length;
			for ( i = 0; i < n; i++ )
			{
				dSdxdy += (arXValues[i] - rXMean) * (arYValues[i] - rYMean);
				dSdx2  += (arXValues[i] - rXMean) * (arXValues[i] - rXMean);

				// for correlation-coefficient-calculation:
				dSxy += arXValues[i] * arYValues[i];
				dSx  += arXValues[i];
				dSy  += arYValues[i];
				dSx2 += arXValues[i] * arXValues[i];
				dSy2 += arYValues[i] * arYValues[i];
			}

//			if ( dSdx2 < ALREALTOL )
//				throw AlMathErr (AlMathErr::DIVBYZERO);

			dSlope     = dSdxdy / dSdx2;
			dIntercept = rYMean - dSlope * rXMean;

			dTemp = Math.Sqrt((n*dSx2 - dSx*dSx) * (n*dSy2 - dSy*dSy));
			dCC = ( n*dSxy - dSx*dSy ) / dTemp;
			
			return dSlope;
		}

		public static double AverageSlope(double[] arXValues, double[] arYValues, int smthPts, out AnalysisStatus state)
		{
			int		    uiCurSmoothPNum;
			int		    iLine;
			int			iLineNum;
			double		dSlope, d1, d2;
			var		dMeanSlope = 0.0;
			double[]	arLinRegX;
			double[]	arLinRegY;

			uiCurSmoothPNum = Math.Min(arXValues.Length, smthPts);

			iLineNum = arXValues.Length - uiCurSmoothPNum + 1;
			for ( iLine = 0; iLine < iLineNum; iLine++ )
			{
				arLinRegX = ExtractFromArray(arXValues, iLine, uiCurSmoothPNum);
				arLinRegY = ExtractFromArray(arYValues, iLine, uiCurSmoothPNum);
				dSlope = LinearYRegression(arLinRegX, arLinRegY, out d1, out d2);

				dMeanSlope += dSlope;
			}

			dMeanSlope /= iLineNum;

			if(Double.IsNaN(dMeanSlope))
				state = AnalysisStatus.Error;
			else
				state = AnalysisStatus.OK;

			return dMeanSlope;
		}

		private static double[] ExtractFromArray(double[] arr, int startPos, int length)
		{
			var tmp = new double[length];
			var i = 0;
			while(i<length)
				tmp[i++] = arr[startPos++];
			return tmp;
		}

		public static double Ratio(double dX1, double dX2, out AnalysisStatus status)
		{
			if(dX2 == 0)
			{
				status = AnalysisStatus.Error;
				return double.NaN;
			}
			status = AnalysisStatus.OK;
			return Round(dX1/dX2, 3);			
		}
	}
}
