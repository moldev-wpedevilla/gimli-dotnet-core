using System;

namespace Apex.Extensibility.Layouts
{
    [Serializable]
	public class CycleInfo
	{
		private int _start = 1;
		private int _end = 1;

		public CycleInfo()
		{
			_start = 1;
			_end = 1;
		}
		public CycleInfo(int start, int end)
		{
			_start = start;
			_end = end;
		}

		public int Start
		{
			get{return _start;}
			set{_start = value;}
		}

		public int End
		{
			get{return _end;}
			set{_end = value;}
		}
	}

	public class XYPosition
	{
		private int _startX = 1;
		private int _endX = 1;
		private int _startY = 1;
		private int _endY = 1;

		public XYPosition() : this(1, 1, 1, 1){}
		public XYPosition(int startX, int startY, int endX, int endY)
		{
			_startX = startX;
			_endX = endX;
			_startY = startY;
			_endY = endY;
		}

		public int Start_X
		{
			get{return _startX;}
			set{_startX = value;}
		}

		public int End_X
		{
			get{return _endX;}
			set{_endX = value;}
		}
		public int Start_Y
		{
			get{return _startY;}
			set{_startY = value;}
		}

		public int End_Y
		{
			get{return _endY;}
			set{_endY = value;}
		}
	}

	
}
