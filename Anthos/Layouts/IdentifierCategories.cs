namespace Apex.Extensibility.Layouts
{
    [System.Serializable]
	public enum IdentifierCategories
	{
		Standard,
		Control,
		Blank,
		Empty,
		Sample
	}
}
