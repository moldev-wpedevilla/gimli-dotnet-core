﻿using System;
using System.Collections.Generic;

namespace Anthos.Controls
{
    public class DependencyManager
    {
        private static readonly Dictionary<string, object> DependencyList = new Dictionary<string, object>();

        public static void SetDependency<T>(object obj)
        {
            if (DependencyList.ContainsKey(typeof(T).FullName))
                return;
            DependencyList.Add(typeof(T).FullName, obj);
        }

        public static void SetDependency<T>(object obj, bool clear)
        {
            if (DependencyList.ContainsKey(typeof (T).FullName))
            {
                if (clear)
                    DependencyList.Remove(typeof (T).FullName);
                else
                    return;
            }

            DependencyList.Add(typeof(T).FullName, obj);
        }

        public static T GetDependency<T>()
        {
            if (!DependencyList.ContainsKey(typeof(T).FullName))
                throw new InvalidOperationException(string.Format("Dependency of type {0} is not provided",
                    typeof(T).FullName));

            return (T)DependencyList[typeof(T).FullName];
        }
    }
}
