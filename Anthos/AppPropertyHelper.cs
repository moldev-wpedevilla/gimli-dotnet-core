using System;
using System.Data;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using Anthos.ResourceReader;


namespace Anthos.Utilities
{
	#region Class AppSetting
	public class AppSetting : SettingsBase
	{
		#region Members
		protected static SettingsBase _user;
		protected static SettingsBase _all;
		private static string _Name;
		#endregion
		#region Constructor
		AppSetting(){}
		#endregion
		#region Public Properties
		/// <summary>
		/// Gets or sets filename of configuration file. (Default is Apex2.Config)
		/// </summary>
		public static string FileName
		{
			get
			{
				return ApexConfig.ConfigName;
			}
			set
			{
				ApexConfig.ConfigName = value;
			}
		}
        public static bool UdateConfigFile(string updFileName, bool testIt)
        {
            return ApexConfig.UpdateConfigFile(updFileName, testIt);
        }
        public static DataSet GetDataSet
        {
            get
            {
                return ApexConfig.Dataset;
            }
        }

		public static string GetUserName
		{
			get
			{
				if(_user == null)
					return "";
				return _user.CurrUserName;
			}
		}

		public static SettingsBase User
		{
			get
			{
				if(_user == null)
					_user = new SettingsBase();

				_user.CurrUserName = _Name;
				return _user;
			}
		}
		public static SettingsBase All
		{
			get
			{
				if(_all == null)
					_all = new SettingsBase();

				_all.CurrUserName = "All";
				return _all;
			}
		}
		#endregion
		#region Public Methods
        public static void Save()
        {
            ApexConfig.WriteConfigFile();
        }

        public static void Save(string fileName)
        {
            ApexConfig.WriteConfigFile(fileName);
        }

		public static void SetUserName(string userName)
		{
			if(_user == null)
				_user = new SettingsBase();
			_Name = userName;
			_user.CurrUserName = userName;
		}
		#endregion
	}
	#endregion
	#region Class SettingsBase
	public class SettingsBase : SettingBase
	{
		#region Members
		protected static SettingBase _software;
		protected static SettingBase _system;
		protected static SettingBase _instrument;
		protected static SettingBase _printer;
		protected static SettingBase _license;
		protected static SettingBase _showAgainMsg;
        protected static SettingBase _autoInstall;
		private static string _uName;
		#endregion
		#region Constructor
		public SettingsBase()
		{}
		#endregion
		#region Public Properties
		public SettingBase ShowAgainMsg
		{
			get
			{
				if(_showAgainMsg == null)
					_showAgainMsg = new SettingBase();
				KeyType = "ShowAgainMsg";
				UserName = _uName;
				return _showAgainMsg;
			}
		}

		public SettingBase Software
		{
			get
			{
				if(_software == null)
					_software = new SettingBase();
				KeyType = "Software";
				UserName = _uName;
				return _software;
			}
		}

        public SettingBase AutoInstallation
        {
            get
            {
                if (_autoInstall == null)
                    _autoInstall = new SettingBase();
                KeyType = "AutoInstallation";
                UserName = _uName;
                return _autoInstall;
            }
        }

		public SettingBase License
		{
			get
			{
				if(_license == null)
					_license = new SettingBase();
				KeyType = "License";
				UserName = _uName;
				return _license;
			}
		}

		public SettingBase System
		{
			get
			{
				if(_system == null)
					_system = new SettingBase();
				KeyType = "System";
				UserName = _uName;
				return _system;
			}
		}

		public SettingBase Printer
		{
			get
			{
				if(_printer == null)
					_printer = new SettingBase();
				KeyType = "Printer";
				UserName = _uName;
				return _printer;
			}
		}

		public SettingBase Instrument
		{
			get
			{
				if(_instrument == null)
					_instrument = new SettingBase();
				KeyType = "Instrument";
				UserName = _uName;
				return _instrument;
			}
		}
		
		public string CurrUserName
		{
			get{return _uName;}
			set{_uName = value;}
		}
		#endregion
		
	}
	#endregion
	#region Class ApexConfig
	public class ApexConfig
	{
		#region Members
		private static DataSet _cfg = null;
		protected static string _configName = "Apex2.Config";
		#endregion
		#region Public Properties
		public static string ConfigName
		{
			get{return _configName;}
			set{_configName = value;}
		}
		#endregion
		#region Public Methods
        public static DataSet Dataset
        {
            get { return _cfg; }
        }

		public static object GetValue(string user, string section, string key, string description)
		{
			if (_cfg == null)
			{
				_cfg = new DataSet("Configuration");
                ReadConfigFile(_cfg, _configName);
			}

			if (_cfg.Tables[section] != null)
				return GetKeyValue(_cfg.Tables[section], user, key != string.Empty ? key.Replace(",","").Replace(".",""):key);
			return "";
		}

		public static bool SetValue(string user, string section, string key, object obj, string description)
		{
			if (_cfg == null)
			{
				_cfg = new DataSet("Configuration");
                ReadConfigFile(_cfg, _configName);
			}

			if (_cfg.Tables[section] == null)
				CheckTable(section, _cfg);

			if (!_cfg.Tables[section].Columns.Contains(user))
				CreateColumn(_cfg.Tables[section], user);

            if (!_cfg.Tables[section].Columns.Contains("Description"))
                CreateColumn(_cfg.Tables[section], "Description");

            if (SetKeyValue(_cfg.Tables[section], user, key != string.Empty ? key.Replace(",", "").Replace(".", "") : key, obj, description))
			{
				//WriteConfigFile();
				return true;
			}
			return false;
		}

		public static ArrayList GetAllKeys(string section)
		{
			var keyarl = new ArrayList();

			if (_cfg == null)
			{
				_cfg = new DataSet("Configuration");
				ReadConfigFile(_cfg, _configName);
			}

			if (_cfg.Tables[section] == null)
				CheckTable(section, _cfg);

			foreach(DataRow dr in _cfg.Tables[section].Rows)
			{
				keyarl.Add(dr[0].ToString());
			}
			
			return keyarl;
		}

		public static void RemoveUserEntry(string user, string section, string key)
		{
			if (_cfg.Tables[section] == null)
				return;

            if (RemoveEntry(_cfg.Tables[section], key != string.Empty ? key.Replace(",", "").Replace(".", "") : key))
			{
				_cfg.AcceptChanges();
				WriteConfigFile();
			}
		}

		private static bool RemoveEntry(DataTable dt, string key)
		{
			if (key == null)
			{
				dt.Rows.Clear();
				return true;
			}

			for(var i= dt.Rows.Count-1; i>=0; i--)
			{
				var dr = dt.Rows[i];
				if (dr["key"].ToString() == key)
				{
					dr.Delete();
					return true;
				}
			}
			return false;
		}

		public static void ResetUserEntrys(string user, string section, string key)
		{
			if (_cfg.Tables[section] == null)
				return;

            if (ResetEntrys(_cfg.Tables[section], user, string.IsNullOrEmpty(key) ? key : key.Replace(",", "").Replace(".", "")))
			{
				_cfg.AcceptChanges();
				WriteConfigFile();
			}
		}
		#endregion
		#region Private Methods
		private static bool ResetEntrys(DataTable dt, string colName, string key)
		{
			if (dt.Columns[colName] == null)
				return false;

			if (key == null)
			{
				dt.Columns.Remove(colName);
				return true;
			}

			foreach(DataRow dr in dt.Rows)
			{
				if (dr["key"].ToString() == key)
				{
					dr[colName] = null;
					return true;
				}
			}
			return false;
		}

        private static bool SetKeyValue(DataTable dt, string user, string key, object obj, string description)
		{
			foreach(DataRow dr in dt.Rows)
			{
				if (dr[0].ToString() == key)
				{
					dr[user] = obj;
                    if (!string.IsNullOrEmpty(description))
                        dr["Description"] = description;
					return true;
				}
			}

			try
			{
				var drn = dt.NewRow();
				drn[0] = key;
				drn[user] = obj;
                drn["Description"] = description;
				dt.Rows.Add(drn);
				dt.AcceptChanges();
				return true;
			}
			catch(Exception e)
			{
				Trace.WriteLine("AppPropertyHelper: " + e.Message);
			}
			return false;			
		}

		private static void CreateColumn(DataTable tbl, string colName)
		{
			var dc = tbl.Columns.Add(colName);
		}

		private static object GetKeyValue(DataTable tbl, string user, string key)
		{
			foreach(DataRow dr in tbl.Rows)
			{
				if (dr[0].ToString() == key)
				{
					for(var i=0; i<tbl.Columns.Count; i++)
						if (tbl.Columns[i].ToString() == user)
							return dr[i];
				}
			}
			return "";
		}

        private static void ReadConfigFile(string fileName)
        {
            _configName = fileName;
            ReadConfigFile(_cfg, _configName);
        }

        private static void ReadConfigFile(DataSet cfg, string configName)
		{
            var start = DateTime.Now;
			try
			{
                var fullName = configName;
                if (!System.IO.File.Exists(fullName))
                    fullName = DirectoryLookup.GetAllUsersBaseDir + configName;
				
                if (System.IO.File.Exists(fullName))
				{
					cfg.ReadXml(fullName);
					cfg = NaturalNumberFormatter.ReplaceDecimalSeperator(cfg);
                    NaturalNumberFormatter.ReplaceSpecialCharactersForUsing(ref cfg);
				}
			}
			catch(Exception e)
			{
				Trace.WriteLine("AppPropertyHelper: " + e.Message);
			}
            var tsoverall = DateTime.Now.Subtract(start);
            Trace.WriteLine("ApexConfig, Retrieving Data: Time: " + string.Format("{0} ms", tsoverall.TotalMilliseconds.ToString("0")));
		}

        public static bool UpdateConfigFile(string updFileName, bool testIt)
        {
            var diffFound = false;
            var dsUpd = new DataSet();
            ReadConfigFile(dsUpd, updFileName);

            try
            {
                if (dsUpd.Tables.Count == 0)
                    return false;
            }
            catch (Exception e)
            {
                Trace.WriteLine("AppPropertyHelper::UpdateConfigFile " + e.Message);
                return false;
            }

            foreach (DataTable table in dsUpd.Tables)
            {
                foreach (DataRow row in table.Rows)
                {
                    var key = row["key"].ToString();
                    foreach (DataColumn column in table.Columns)
                    {
                        if (column.ColumnName == "key")
                            continue;

                        if (AppSetting.GetUserName != column.ColumnName)
                            AppSetting.SetUserName(column.ColumnName);

                        switch (table.TableName)
                        {
                            case "Instrument":
                                if (testIt)
                                {
                                    if (AppSetting.User.Instrument[key, "", ""].ToString() != row[column].ToString())
                                        diffFound = true;
                                }
                                else
                                    AppSetting.User.Instrument[key] = row[column].ToString();
                                break;
                            case "System":
                                if (testIt)
                                {
                                    if (AppSetting.User.System[key, "", ""].ToString() != row[column].ToString())
                                        diffFound = true;
                                }
                                AppSetting.User.System[key] = row[column].ToString();
                                break;
                            case "License":
                                if (testIt)
                                {
                                    if (AppSetting.User.License[key, "", ""].ToString() != row[column].ToString())
                                        diffFound = true;
                                }
                                AppSetting.User.License[key] = row[column].ToString();
                                break;
                            case "Software":
                                if (testIt)
                                {
                                    if (AppSetting.User.Software[key, "", ""].ToString() != row[column].ToString())
                                        diffFound = true;
                                }
                                AppSetting.User.Software[key] = row[column].ToString();
                                break;
                        }
                        if (testIt && diffFound)
                            return true;
                    }
                }
            }
            
            if (!testIt)
                WriteConfigFile();

            return false;
        }

        public static void WriteConfigFile(string fileName)
        {
            _configName = fileName;
            WriteConfigFile();
        }

		public static void WriteConfigFile()
		{
            if (_cfg == null)
                return;

            var start = DateTime.Now;
            
			try
			{
                NaturalNumberFormatter.ReplaceSpecialCharactersForSaving(ref _cfg);
				_cfg.WriteXml(DirectoryLookup.GetAllUsersBaseDir + _configName);
			}
			catch(Exception e)
			{
				Trace.WriteLine("AppPropertyHelper: " + e.Message);
			}
            var tsoverall = DateTime.Now.Subtract(start);
            Trace.WriteLine("ApexConfig, Saving Data: Time: " + string.Format("{0} ms", tsoverall.TotalMilliseconds.ToString("0")));
		}

		/// <summary>
		/// For testing only
		/// </summary>
		/// <param name="ds"></param>
		private static void DisplayDS(DataSet ds)
		{
			if (ds == null)
				return;

			foreach(DataTable dst in  ds.Tables)
			{
				Trace.WriteLine("SQLDataStore: DataSet: " + ds.DataSetName + " Table: " + dst.TableName);
				foreach(DataRow dsr in dst.Rows)
					foreach(DataColumn dsc in dst.Columns)
						Trace.WriteLine(dsc.Caption + " - " + dsr.ItemArray[dsc.Ordinal].ToString());
			}
		}
		#endregion
		#region Protected Methods
		/// <summary>
		/// Checks if required table exists in Dataset
		/// </summary>
		/// <param name="TableName">Name of required table</param>
		/// <param name="dSet">reference to DataSet</param>
		/// <returns>true if table exists or table creation was successful</returns>
		protected static bool CheckTable(string TableName, DataSet dSet)
		{
			var tablesCol = dSet.Tables;
			var bOK = false;
		
			// Check if the named table exists.
			try
			{
				if (!tablesCol.Contains(TableName)) 
				{
					var newTable = new DataTable(TableName);
					newTable.Columns.Add("key");
					newTable.Columns.Add("All");
					dSet.Tables.Add(newTable);
					dSet.AcceptChanges();
					Trace.WriteLine ("Adding Table to DataSet: " + TableName);
				}
				bOK = true;
			}
			catch (Exception e)
			{
				Trace.WriteLine ("Error: " + e.Message );
			}
			return bOK;
		}
		#endregion
		

	}

	#endregion
	#region Class SettingBase
	public class SettingBase
	{
		#region Members
		protected static string _userName = "";
		protected static string _keyType = "";
		protected static string _value = "";
		protected static string _configName = "Apex2.Config";
		#endregion
		#region Constructor
		public SettingBase()
		{}
		#endregion
		#region Public Properties
		public object this[string keyname] 
		{
			get
			{
                return this[keyname != string.Empty ? keyname.Replace(",", "") : keyname, "", ""];
			}
			set
			{
                this[keyname != string.Empty ? keyname.Replace(",", "") : keyname, "", ""] = value;
			}
		}

        public object this[string keyname, object defaultValue, string description]
        {
            get
            {
                var actvalue = ApexConfig.GetValue(UserName, _keyType, keyname, description).ToString();

                if (actvalue == "")
                    actvalue = ApexConfig.GetValue("All", _keyType, keyname, description).ToString();

                if (actvalue == "")
                {
                    actvalue = defaultValue.ToString();
                    ApexConfig.SetValue("All", _keyType, keyname, actvalue, description);
                }

                return actvalue;
            }
            set
            {
                ApexConfig.SetValue(UserName, _keyType, keyname, value, description);
            }
        }

        public object this[string keyname, object defaultValue]
        {
            get
            {
                var actvalue = ApexConfig.GetValue(UserName, _keyType, keyname, "").ToString();

                if (actvalue == "")
                    actvalue = ApexConfig.GetValue("All", _keyType, keyname, "").ToString();

                if (actvalue == "")
                {
                    actvalue = defaultValue.ToString();
                    ApexConfig.SetValue("All", _keyType, keyname, actvalue, "");
                }

                return actvalue;
            }
            set
            {
                ApexConfig.SetValue(UserName, _keyType, keyname, value, "");
            }
        }

		

		public System.Drawing.Color this[string keyname, System.Drawing.Color defaultColor]
		{
			get
			{
				System.Drawing.Color newcol;
                var colName = ApexConfig.GetValue(UserName, _keyType, keyname, "").ToString(); 
				
				if (colName == "")
                    colName = ApexConfig.GetValue("All", _keyType, keyname, "").ToString(); 

				if (colName == "")
				{
					colName = defaultColor.Name;
                    ApexConfig.SetValue("All", _keyType, keyname, colName, "");
				}
				
				if (colName != "")
				{
					newcol = System.Drawing.Color.FromName(colName);
					if (!newcol.IsKnownColor)
						newcol = System.Drawing.Color.FromArgb(int.Parse(colName, NumberStyles.AllowHexSpecifier));
					return newcol;
				}
				else
					return defaultColor;
			}
		}

		public ArrayList AllKeys
		{
			get
			{
				return ApexConfig.GetAllKeys(_keyType);
			}
		}
		#endregion
		#region Public Methods
		/// <summary>
		/// Removes setting of key of specified User.
		/// </summary>
		/// <param name="key">Name of Key to be removed</param>
		public void Remove(string key)
		{
			if (_userName == "All")
				ApexConfig.RemoveUserEntry(_userName, _keyType, key);
			else
				ApexConfig.ResetUserEntrys(_userName, _keyType, key);
		}
		
		/// <summary>
		/// Removes all Settings of specified User.
		/// </summary>
		public void Remove()
		{
			if (_userName == "All")
				ApexConfig.RemoveUserEntry(_userName, _keyType, null);
			else
				ApexConfig.ResetUserEntrys(_userName, _keyType, null);
		}
		#endregion
		#region Protected Properties
		protected string ConfigName
		{
			get{return _configName;}
			set{_configName = value;}
		}

		protected string UserName
		{
			get
			{
				if (_userName == null)
					return "All";
				return _userName;
			}
			set
			{
				_userName = value;
				if (value == null || value == "")
					_userName = "All";
			}
		}
		
		protected string KeyType
		{
			get
			{	if (_keyType == null)
					return "System";
				return _keyType;
			}
			set{_keyType = value;}
		}
		#endregion
	}
	#endregion
}
