using System.Xml;

namespace Anthos.Utilities
{
    public class ActivatedClass
    {
        private XmlNode mNode;

        public ActivatedClass(XmlNode node)
        {
            mNode = node;
        }

        public string Type { get; set; }

        public void Load()
        {
            if (mNode == null)
                return;

            Type = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "type");
        }

        public void Save()
        {
            if (mNode == null)
                return;

            mNode.Attributes?.RemoveAll();

            ConfigurationHelperClass.SetAttribute(mNode, "type", Type);
        }
    }
}