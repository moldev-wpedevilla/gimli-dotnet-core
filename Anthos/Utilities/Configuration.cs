using System;
using System.Xml;
using System.Diagnostics;
using System.Collections;

namespace Anthos.Utilities
{
	/// <summary>
	/// Summary description for Configuration.
	/// </summary>
	public class Configuration 
	{
		private XmlDocument mXdoc;
		private ArrayList mNs;
		private string mRootnode = "configuration";
		private string mFilename;

	    public void Load(string filename)
		{
			mFilename = filename;
			LoadData();

            if (mNs != null)
                mNs.Clear();

			mNs = new ArrayList();
			var node = mXdoc.SelectSingleNode(mRootnode);
			if (node == null)
				return;

			foreach(XmlNode nsn in node.ChildNodes)
			{
				if (GetNameSpace(nsn.Name) == null)
					mNs.Add(new NameSpaceClass(nsn));
			}

			foreach(NameSpaceClass nsc in mNs)
			{
				nsc.Load();
			}
		}

		//		public NameSpaceClass AddNew(string name)
		//		{
		//			if (GetNameSpace(name) == null)
		//				_ns.Add(new NameSpaceClass(_rootnode, name, _xdoc));
		//			return GetNameSpace(name);
		//		}

		//		public void Remove(string name)
		//		{
		//			if (GetNameSpace(name) != null)
		//				_ns.Remove(GetNameSpace(name));
		//		}

		public NameSpaceClass[] Categories
		{
			get{return (NameSpaceClass[]) mNs.ToArray(typeof(NameSpaceClass));}
		}

		public NameSpaceClass Category(string name)
		{
			return GetNameSpace(name);
		}

		private NameSpaceClass GetNameSpace(string name)
		{
			foreach(NameSpaceClass ns in mNs)
			{
				if (ns.Name.ToLower() == name.ToLower())
					return ns;
			}
			return null;
		}

		private void LoadData()
		{
		    if (mXdoc != null)
		    {
                mXdoc.RemoveAll();
		        mXdoc = null;
		    }
			mXdoc = new XmlDocument();
			try
			{
				mXdoc.Load(mFilename);
			}
			catch(Exception e)
			{
				Trace.WriteLine("Configuration::Load " + e.Message);
			}
		}

		public void Save()
		{
			if (mXdoc == null)
				return;

			foreach(NameSpaceClass ns in mNs)
			{
				ns.Save();
			}

			try
			{
				mXdoc.Save(mFilename);
			}
			catch(Exception e)
			{
				Trace.WriteLine("Configuration::Save " + e.Message);
			}
		}
	}
}
