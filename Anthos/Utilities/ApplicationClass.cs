using System.Collections;
using System.Xml;

namespace Anthos.Utilities
{
    public class ApplicationClass
    {
        private XmlNode mNode;
        private ArrayList mCustProp;

        public ApplicationClass(XmlNode node)
        {
            mNode = node;
        }
        public CustomPropertyClass CustomProperty(string name)
        {
            foreach(CustomPropertyClass cs in mCustProp)
            {
                if (cs.Name == name)
                    return cs;
            }
            //create new CustomPropertyObject
            XmlNode node = mNode.OwnerDocument.CreateElement("CustomProperty");
            ConfigurationHelperClass.SetAttribute(node, "name", name);
            mNode.AppendChild(node);
            var cx = new CustomPropertyClass(node);
            cx.Load();
            mCustProp.Add(cx);
			
            return cx;
        }

        public ClientClass Client { get; set; }

        public ServiceClass Service { get; set; }

        public ChannelsClass Channels { get; private set; }

        public void Load()
        {
            Service = null;
            Client = null;
            Channels = null;

            if (mCustProp!=null)
                mCustProp.Clear();
            else
                mCustProp = new ArrayList();

            if (mNode == null)
                return;

            foreach(XmlNode cn in mNode.ChildNodes)
            {
                if (cn.Name.ToLower() == "service")
                    Service = new ServiceClass(cn);
                else if (cn.Name.ToLower() == "client")
                    Client = new ClientClass(cn);
                else if (cn.Name.ToLower() == "channels")
                    Channels = new ChannelsClass(cn);
                else if (cn.Name.ToLower() == "customproperty")
                    mCustProp.Add(new CustomPropertyClass(cn));
            }

            Channels?.Load();
            Service?.Load();
            Client?.Load();
            foreach(CustomPropertyClass cs in mCustProp)
            {
                cs.Load();
            }
        }
		
        public void Save()
        {
            if (mNode == null)
                return;

            if (mNode.Attributes != null) mNode.Attributes.RemoveAll();

            Channels?.Save();
            Service?.Save();
            Client?.Save();

            var arrDel = new ArrayList();
            foreach(CustomPropertyClass cs in mCustProp)
            {
                cs.Save();
                if (cs.Count == 0)
                    arrDel.Add(cs);
            }
            foreach(CustomPropertyClass cx in arrDel)
            {
                mCustProp.Remove(cx);
            }
            arrDel.Clear();
        }
    }
}