using System.Xml;

namespace Anthos.Utilities
{
    public class ConfigurationHelperClass
    {
	
        public static string GetAttribute(XmlAttributeCollection attrs, string name)
        {
            if (attrs == null || name == string.Empty)
                return string.Empty;

            foreach(XmlAttribute attr in attrs)
            {
                if (attr.Name.ToLower() == name.ToLower())
                    return attr.Value;
            }

            return string.Empty;
        }

        public static void SetAttribute(XmlNode node, string name, string text)
        {
            if (node == null || name == string.Empty || text == string.Empty)
                return;

            var attrNode = node.OwnerDocument.CreateNode(XmlNodeType.Attribute, name, null);
            attrNode.Value = text;
            node.Attributes.SetNamedItem(attrNode);
        }
    }
}