using System.Collections;
using System.Xml;

namespace Anthos.Utilities
{
    public class CustomPropertyClass
    {
        private XmlNode mNode;
        private Hashtable mProps = new Hashtable();

        public CustomPropertyClass(XmlNode node)
        {
            mNode = node;
        }

        public string Name
        {
            get
            {
                if(mProps.ContainsKey("name"))
                    return mProps["name"].ToString();				
                return string.Empty;
            }
            set
            {
                mProps["name"] = value;			
            }
        }

        public string this[string name]
        {
            get
            {
                if(mProps.ContainsKey(name))
                    return mProps[name].ToString();				
                return string.Empty;
            }
            set
            {
                if (!mProps.ContainsKey(name))
                    mProps.Add(name, value);
                else
                    mProps[name] = value;			
            }
        }
		
        public string Property(string name)
        {
            foreach(DictionaryEntry de in mProps)
            {
                if (de.Key.ToString().ToLower() == name.ToLower())
                    return de.Value.ToString();
            }
            return string.Empty;
        }
        public int Count => mProps.Count;

        public void Load()
        {
            if(mProps!=null)
                mProps.Clear();
            else
                mProps = new Hashtable();

            if (mNode == null)
                return;

            if (mNode.Attributes == null)
                return;

            foreach(XmlAttribute attr in mNode.Attributes)
            {
                if (mProps[attr.Name] == null)
                    mProps.Add(attr.Name, attr.Value);
            }
        }

        public void Save()
        {
            if (mNode == null)
                return;

            if (mNode.Attributes != null) 
                mNode.Attributes.RemoveAll();

            //delete hashtable if only name property exists.
            if (mProps.Count == 1 && mProps["name"] != null)
            {
                mNode.ParentNode.RemoveChild(mNode);
                mProps.Clear();
            }

            foreach(DictionaryEntry de in mProps)
            {
                ConfigurationHelperClass.SetAttribute(mNode, de.Key.ToString(), de.Value.ToString());
            }
        }
    }
}