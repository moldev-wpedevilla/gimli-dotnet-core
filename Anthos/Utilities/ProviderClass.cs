using System.Xml;

namespace Anthos.Utilities
{
    public class ProviderClass
    {
        private XmlNode mNode;

        public ProviderClass(XmlNode node)
        {
            mNode = node;
        }

        public string Ref { get; set; }

        public string Type { get; set; }

        public string CustomChannelSinkProperty { get; set; }

        public void Load()
        {
            if (mNode == null)
                return;

            Ref = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "ref");
            Type = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "type");
            CustomChannelSinkProperty = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "customChannelSinkProperty");
        }

        public void Save()
        {
            if (mNode == null)
                return;

            if (mNode.Attributes != null) mNode.Attributes.RemoveAll();

            ConfigurationHelperClass.SetAttribute(mNode, "ref", Ref);
            ConfigurationHelperClass.SetAttribute(mNode, "type", Type);
            ConfigurationHelperClass.SetAttribute(mNode, "customChannelSinkProperty", CustomChannelSinkProperty);
        }
    }
}