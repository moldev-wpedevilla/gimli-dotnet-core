using System.Xml;

namespace Anthos.Utilities
{
    public class FormatterClass
    {
        private XmlNode mNode;
        private string mInclVers;
        private string mStrict;

        public FormatterClass(XmlNode node)
        {
            mNode = node;
        }

        public string Ref { get; set; }

        public string TypeFilterLevel { get; set; }

        public string Type { get; set; }

        public string CustomFormatterProperty { get; set; }

        public bool StrictBinding
        {
            get
            {
                if (mStrict == bool.TrueString)
                    return true;
                return false;
            }
            set{mStrict = value.ToString();}
        }
        public bool IncludeVersions
        {
            get
            {
                if (mInclVers == bool.TrueString)
                    return true;
                return false;
            }
            set{mInclVers = value.ToString();}
        }

        public void Load()
        {
            if (mNode == null)
                return;
            
            Ref = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "ref");
            TypeFilterLevel = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "typeFilterLevel");
            Type = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "type");
            CustomFormatterProperty = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "customFormatterProperty");
            mStrict = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "strictBinding");
            mInclVers = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "includeVersions");
        }

        public void Save()
        {
            if (mNode == null)
                return;

            if (mNode.Attributes != null) mNode.Attributes.RemoveAll();

            ConfigurationHelperClass.SetAttribute(mNode, "ref", Ref);
            ConfigurationHelperClass.SetAttribute(mNode, "typeFilterLevel", TypeFilterLevel);
            ConfigurationHelperClass.SetAttribute(mNode, "type", Type);
            ConfigurationHelperClass.SetAttribute(mNode, "customFormatterProperty", CustomFormatterProperty);
            ConfigurationHelperClass.SetAttribute(mNode, "strictBinding", mStrict);
            ConfigurationHelperClass.SetAttribute(mNode, "includeVersions", mInclVers);
        }
    }
}