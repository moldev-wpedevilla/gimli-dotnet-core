using System.Collections;
using System.Xml;

namespace Anthos.Utilities
{
    public class ServerProviderClass
    {
        private XmlNode mNode;
        private ArrayList mProviders;
        private ArrayList mFormatter;
        public ServerProviderClass(XmlNode node)
        {
            mNode = node;
        }

        public string Name
        {
            get{return mNode.Name;}
        }
		
        public FormatterClass[] Formatters
        {
            get{return (FormatterClass[])mFormatter.ToArray(typeof(FormatterClass));}
        }
		
        public FormatterClass Formatter(string name)
        {
            foreach(FormatterClass fmt in mFormatter)
            {
                if (fmt.Ref.ToLower() == name.ToLower())
                    return fmt;
            }
            return null;
        }

        public ProviderClass[] Providers
        {
            get{return (ProviderClass[])mProviders.ToArray(typeof(ProviderClass));}
        }

        public ProviderClass Provider(string name)
        {
            foreach(ProviderClass pro in mProviders)
            {
                if (pro.Ref.ToLower() == name.ToLower())
                    return pro;
            }
            return null;
        }

        public void Load()
        {
            mProviders = new ArrayList();
            mFormatter = new ArrayList();

            if (mNode == null)
                return;

            foreach(XmlNode cn in mNode.ChildNodes)
            {
                if (cn.Name.ToLower() == "provider")
                    mProviders.Add(new ProviderClass(cn));
                else if (cn.Name.ToLower() == "formatter")
                    mFormatter.Add(new FormatterClass(cn));
            }

            foreach(ProviderClass prov in mProviders)
            {
                prov.Load();
            }
            foreach(FormatterClass formatter in mFormatter)
            {
                formatter.Load();
            }
        }

        public void Save()
        {
            if (mNode == null)
                return;

            if (mNode.Attributes != null) mNode.Attributes.RemoveAll();

            if (mProviders != null)
            {
                foreach(ProviderClass prov in mProviders)
                {
                    prov.Save();
                }
            }
            if (mFormatter != null)
            {
                foreach(FormatterClass formatter in mFormatter)
                {
                    formatter.Save();
                }
            }
        }
    }
}