using System.Xml;

namespace Anthos.Utilities
{
    public class ClientClass
    {
        private XmlNode mNode;
        private WellknownClientClass mWellknown;
        private ActivatedClass mActivated;

        public ClientClass(XmlNode node)
        {
            mNode = node;
        }

        public string DisplayName { get; set; }

        public string Url { get; set; }

        public WellknownClientClass Wellknown => mWellknown;

        public ActivatedClass Activated => mActivated;

        public void Load()
        {
            mWellknown = null;
            mActivated = null;

            if (mNode == null)
                return;

            foreach(XmlNode cn in mNode.ChildNodes)
            {
                if (cn.Name.ToLower() == "wellknown")
                    mWellknown = new WellknownClientClass(cn);
                else if (cn.Name.ToLower() == "activated")
                    mActivated = new ActivatedClass(cn);
            }

            DisplayName = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "displayName");
            Url = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "url");

            mWellknown?.Load();
            mActivated?.Load();
        }

        public void Save()
        {
            if (mNode == null)
                return;

            if (mNode.Attributes != null) mNode.Attributes.RemoveAll();

            ConfigurationHelperClass.SetAttribute(mNode, "displayName", DisplayName);
            ConfigurationHelperClass.SetAttribute(mNode, "url", Url);

            mWellknown?.Save();
            mActivated?.Save();
        }
    }
}