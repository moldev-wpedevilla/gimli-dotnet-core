using System.Xml;

namespace Anthos.Utilities
{
    public class WellknownServiceClass
    {
        private XmlNode mNode;

        public WellknownServiceClass(XmlNode node)
        {
            mNode = node;
        }

        public string DisplayName { get; set; }

        public string Type { get; set; }

        public string ObjectUri { get; set; }

        public string Mode { get; set; }

        public void Load()
        {
            if (mNode == null)
                return;

            Mode = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "mode");
            Type = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "type");
            ObjectUri = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "objectUri");
            DisplayName = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "displayName");
        }

        public void Save()
        {
            if (mNode == null)
                return;

            if (mNode.Attributes != null) mNode.Attributes.RemoveAll();

            ConfigurationHelperClass.SetAttribute(mNode, "mode", Mode);
            ConfigurationHelperClass.SetAttribute(mNode, "type", Type);
            ConfigurationHelperClass.SetAttribute(mNode, "objectUri", ObjectUri);
            ConfigurationHelperClass.SetAttribute(mNode, "displayName", DisplayName);
        }
    }
}