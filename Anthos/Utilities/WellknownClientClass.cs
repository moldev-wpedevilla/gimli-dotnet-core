using System.Xml;

namespace Anthos.Utilities
{
    public class WellknownClientClass
    {
        private XmlNode mNode;

        public WellknownClientClass(XmlNode node)
        {
            mNode = node;
        }

        public string DisplayName { get; set; }

        public string Type { get; set; }

        public string Url { get; set; }

        public void Load()
        {
            if (mNode == null)
                return;

            DisplayName = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "displayName");
            Type = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "type");
            Url = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "url");
        }

        public void Save()
        {
            if (mNode == null)
                return;

            if (mNode.Attributes != null) mNode.Attributes.RemoveAll();

            ConfigurationHelperClass.SetAttribute(mNode, "displayName", DisplayName);
            ConfigurationHelperClass.SetAttribute(mNode, "url", Url);
            ConfigurationHelperClass.SetAttribute(mNode, "type", Type);
        }
    }
}