using System.Xml;

namespace Anthos.Utilities
{
    public class ServiceClass
    {
        private XmlNode mNode;

        public ServiceClass(XmlNode node)
        {
            mNode = node;
        }

        public string DisplayName { get; set; }

        public string Url { get; set; }

        public WellknownServiceClass Wellknown { get; private set; }

        public ActivatedClass Activated { get; private set; }

        public void Load()
        {
            Activated = null;
            Wellknown = null;

            if (mNode == null)
                return;

            foreach(XmlNode cn in mNode.ChildNodes)
            {
                if (cn.Name.ToLower() == "wellknown")
                    Wellknown = new WellknownServiceClass(cn);
                if (cn.Name.ToLower() == "activated")
                    Activated = new ActivatedClass(cn);
            }

            DisplayName = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "displayName");
            Url = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "url");

            Wellknown?.Load();
            Activated?.Load();
        }

        public void Save()
        {
            if (mNode == null)
                return;

            if (mNode.Attributes != null) mNode.Attributes.RemoveAll();

            ConfigurationHelperClass.SetAttribute(mNode, "displayName", DisplayName);
            ConfigurationHelperClass.SetAttribute(mNode, "url", Url);

            Wellknown?.Save();
            Activated?.Save();
        }
    }
}