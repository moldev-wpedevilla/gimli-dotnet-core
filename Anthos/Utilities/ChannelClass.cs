using System.Collections;
using System.Xml;

namespace Anthos.Utilities
{
    public class ChannelClass
    {
        private XmlNode mNode;
        private string mPort;
        private string mExclude;
        private ArrayList mSerProv;

        public ChannelClass(XmlNode node)
        {
            mNode = node;
        }

        public string Ref { get; set; }

        public int Port
        {
            get
            {
                if (mPort == string.Empty)
                    return 0;
                try
                {
                    var port = int.Parse(mPort);
                    return port;
                }
                catch{}
                return 0;
            }
            set{mPort = value.ToString();}
        }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Domain { get; set; }

        public string MachineName { get; set; }

        public string DisplayName { get; set; }

        public string Type { get; set; }

        public string CustomProperty { get; set; }

        public bool Exclude
        {
            get{
                if (mExclude == bool.TrueString)
                    return true;
                return false;}
            set{
                mExclude = value.ToString();
            }
        }

        public ServerProviderClass[] ServerProviders
        {
            get{return (ServerProviderClass[])mSerProv.ToArray(typeof(ServerProviderClass));}
        }
        public ServerProviderClass ServerProvider(string name)
        {
            return GetProvider(name);
        }
        private ServerProviderClass GetProvider(string name)
        {
            foreach(ServerProviderClass sp in mSerProv)
            {
                if (sp.Name.ToLower() == name.ToLower())
                    return sp;
            }
            return null;
        }

        public void Load()
        {
            mSerProv = new ArrayList();
            if (mNode == null)
                return;

            foreach(XmlNode cn in mNode.ChildNodes)
            {
                if (cn.Name.ToLower() == "serverproviders")
                    mSerProv.Add(new ServerProviderClass(cn));
            }

            Ref = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "ref");
            mPort = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "port");
            MachineName = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "machineName");
            Type = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "type");
            DisplayName = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "displayName");
            CustomProperty = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "customProperty");
            mExclude = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "exclude");

            UserName = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "username");
            Password = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "password");
            Domain = ConfigurationHelperClass.GetAttribute(mNode.Attributes, "domain");

            foreach(ServerProviderClass ss in mSerProv)
            {
                ss.Load();
            }
        }

        public void Save()
        {
            if (mNode == null)
                return;

            if (mNode.Attributes != null) mNode.Attributes.RemoveAll();

            ConfigurationHelperClass.SetAttribute(mNode, "ref", Ref);
            ConfigurationHelperClass.SetAttribute(mNode, "port", mPort);
            if(!string.IsNullOrEmpty(MachineName))
                ConfigurationHelperClass.SetAttribute(mNode, "machineName", MachineName);
            ConfigurationHelperClass.SetAttribute(mNode, "type", Type);
            ConfigurationHelperClass.SetAttribute(mNode, "customProperty", CustomProperty);
            ConfigurationHelperClass.SetAttribute(mNode, "displayName", DisplayName);
            ConfigurationHelperClass.SetAttribute(mNode, "exclude", mExclude);

            ConfigurationHelperClass.SetAttribute(mNode, "username", UserName);
            ConfigurationHelperClass.SetAttribute(mNode, "password", Password);
            ConfigurationHelperClass.SetAttribute(mNode, "domain", Domain);

            if (mSerProv == null)
                return;

            foreach(ServerProviderClass ss in mSerProv)
            {
                ss.Save();
            }
        }
    }
}