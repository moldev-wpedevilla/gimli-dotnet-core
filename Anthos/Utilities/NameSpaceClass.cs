using System.Xml;

namespace Anthos.Utilities
{
    public class NameSpaceClass
    {
        private ApplicationClass _app;
        private XmlNode _node;

        public NameSpaceClass(XmlNode node)
        {
            _node = node;
        }
        public string Name
        {
            get{return _node.Name;}
        }
        public ApplicationClass Application
        {
            get{return _app;}
            set{_app = value;}
        }
		
        public void Load()
        {
            if (_node == null)
                return;

            foreach(XmlNode cn in _node.ChildNodes)
            {
                _app = new ApplicationClass(cn);
                _app.Load();
            }
        }

        public void Save()
        {
            if (_node == null)
                return;

            if (_node.Attributes != null) _node.Attributes.RemoveAll();
            if (_app != null)
                _app.Save();
        }
    }
}