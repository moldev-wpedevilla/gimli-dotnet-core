using System.Collections;
using System.Xml;

namespace Anthos.Utilities
{
    public class ChannelsClass
    {
        private XmlNode mNode;
        private ArrayList mChannels;

        public ChannelsClass(XmlNode node)
        {
            mNode = node;
        }

        public ChannelClass this[string name] => GetChannel(name);

        public ChannelClass Channel(string refName)
        {
            return GetChannel(refName);
        }
        private ChannelClass GetChannel(string name)
        {
            foreach(ChannelClass ch in mChannels)
            {
                if (ch.Ref.ToLower() == name.ToLower())
                    return ch;
            }
            return null;
        }

        public void Load()
        {
            mChannels = new ArrayList();

            if (mNode == null)
                return;

            foreach(XmlNode cn in mNode.ChildNodes)
            {
                if (cn.Name.ToLower() == "channel")
                    mChannels.Add(new ChannelClass(cn));
            }
            foreach(ChannelClass cc in mChannels)
            {
                cc.Load();
            }
        }
		
        public void Save()
        {
            if (mNode == null || mChannels == null)
                return;

            mNode.Attributes?.RemoveAll();

            foreach(ChannelClass ch in mChannels)
            {
                ch.Save();
            }
        }
    }
}