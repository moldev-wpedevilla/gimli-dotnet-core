using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Xml;
using System.IO;

namespace Anthos.Security.Cryptography.XML
{
	/// <summary>
	/// Summary description for CertifiedXML.
	/// </summary>
	public class CertifiedXML
	{
		public static bool CheckCerificate(string file)
		{
			var doc = new XmlDocument();
			doc.Load(file);
				
			//create a temp stream in memory for reading the certificate
			var tmpstr = new MemoryStream();
			doc.Save(tmpstr);				
			var c1 = ReadCertificate(tmpstr);		
		
			//remove certificate for the new calculation
			var node = doc.SelectSingleNode("//Certificate[1]");
			//if a certificate was created remove it
			if(node != null)
				node.ParentNode.RemoveChild(node);
				
			//save temp in memory stream for the new calculation
			tmpstr.SetLength(0);				
			doc.Save(tmpstr);
			var c2 = CalculateCertificate(tmpstr);

			return AreHashSame(c1, c2) ? true : false;			
		}

		public static void Certify(string file)
		{
			var doc = new XmlDocument();
			doc.Load(file);
			
			//create a temp stream in memory for reading the certificate
			var tmpstr = new MemoryStream();
			doc.Save(tmpstr);				
			ReadCertificate(tmpstr);		
	
			//remove certificate for the new calculation
			var node = doc.SelectSingleNode("//Certificate[1]");
			//if a certificate was created remove it
			if(node != null)
				node.ParentNode.RemoveChild(node);
			
			//save temp in memory stream for the new calculation
			tmpstr.SetLength(0);				
			doc.Save(tmpstr);
			var c2 = CalculateCertificate(tmpstr);

			//create a new cerificate element
			//node = doc.SelectSingleNode("//AssayApplication");
            node = doc.DocumentElement;
			var child = doc.CreateElement("Certificate");
			child.InnerText = ConvertHash2Hex(c2);
			node.AppendChild(child);			
			doc.Save(file);            
		}

		private static bool AreHashSame(IList<byte> h1, IList<byte> h2)
		{
			if(h1.Count != h2.Count)
				return false;
			for(var i=0;i<h1.Count;i++)
			{
				if(h1[i] != h2[i])
					return false;
			}
			return true;
		}

		private static byte[] ReadCertificate(Stream str)
		{
			str.Position = 0;
			var rdr = new XmlTextReader(str);
			var len = 0;		
			var hex = new byte[16];
			try
			{						
				while(rdr.Read())
					if(rdr.Name == "Certificate")
					{
						len = rdr.ReadBinHex(hex, 0, 16);
						break;
					}				
			}
			catch(Exception ex)
			{
				ex.ToString();	
			}		
			return hex;	
		}

		internal static string ConvertHash2Hex(byte[] hash)
		{
			var sb = new System.Text.StringBuilder();
			for(var i=0;i<hash.Length;i++)
			{
				sb.AppendFormat("{0:X2}", hash[i]);	
				//byte btmp = Byte.Parse(tmp, System.Globalization.NumberStyles.HexNumber);
			}
			return sb.ToString();
		}

		internal static byte[] CalculateCertificate(Stream str)
		{
			str.Position = 0;
			MD5 md5 = new MD5CryptoServiceProvider();            			
			var hash = md5.ComputeHash(str);
			return hash;
		}
	}

    public class FileHash
    {
        public static string CreateHash(string fileName)
        {
            using (var sr = File.OpenRead(fileName))
            {
                return CertifiedXML.ConvertHash2Hex(CertifiedXML.CalculateCertificate(sr));
            }
        }
    }
}
