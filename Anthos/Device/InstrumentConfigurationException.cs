using System;

namespace Apex.Extensibility.Device
{
    public class InstrumentConfigurationException : Exception
    {
        private ConfigurationProblems prbls;
        private string config;

        public InstrumentConfigurationException(ConfigurationProblems problems, string newConfiguration, string message)
            : base(message)
        {
            prbls = problems;
            config = newConfiguration;
        }

        public ConfigurationProblems Reason { get { return prbls; } }
        public string NewConfiguration { get { return config; } }
    }

    [Flags]
    public enum ConfigurationProblems
    { 
        WrongCarrierMounting    = 1,
        DifferentCarrierSN      = 2,
        DifferentBandwith       = 4
    }
}
