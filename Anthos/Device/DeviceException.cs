namespace Apex.Extensibility.Device
{
	/// <summary>
	/// Holds information about a specific device error. Errors are devided into categories like Transport, 
	/// Electronics ets. DeviceCode contains the original code of the underlying device.
	/// </summary>
	public class DeviceException : ADIException
	{
		private string _addInfo;
		private string _devCode;
        public DeviceException(string message, string addInfo, string devCode, int adiCode)
            : base(message, adiCode)
        {
            _addInfo = TranslateAdditionalInfoText(addInfo);
            _devCode = devCode;
            
            try
            {
                System.Diagnostics.Trace.WriteLine(string.Format("{0} - Code: {1}, Info: {2}", message, _devCode, _addInfo));
            }
            catch { }
        }
        private string TranslateAdditionalInfoText(string msg)
        {
            var addInfo = msg;
            if (addInfo.Contains("Hight"))
            {
                addInfo = msg.Replace("Meas PlateHeight", "\r\n- Detected microplate height");
                addInfo = addInfo.Replace("Set PlateHight", "\r\n- Defined microplate height");
            }

            return addInfo;
        }
		public string ExtInfo{get{return _addInfo;}}
		public string DeviceCode{get{return _devCode;}}
	}
}
