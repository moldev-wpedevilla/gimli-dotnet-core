namespace Apex.Extensibility.Device
{
	/// <summary>
	/// Summary description for CommunicationException.
	/// </summary>
	public class CommunicationException : ADIException
	{
        public CommunicationException(string message, int adiCode) : base(message, adiCode)
		{
			
		}
	}
}
