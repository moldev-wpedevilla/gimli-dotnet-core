using System;

namespace Apex.Extensibility.Device
{
	/// <summary>
	/// Base class for the device & communication exceptions
	/// </summary>
	public class ADIException : ApplicationException
	{
		private int _adicode;

		public ADIException(string message, int adiCode) : base(message)
		{
			_adicode = adiCode;
		}
		public int ADICode{get{return _adicode;}}
	}
}
