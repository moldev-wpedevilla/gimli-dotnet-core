using System;
using System.Data;
namespace Anthos.Utilities
{
	/// <summary>
	/// Summary description for StringFormatter.
	/// </summary>
	public class StringFormatter
	{
		/// <summary>
		/// Removes character which are not allowed in a filename.
		/// </summary>
		/// <param name="name">Filename</param>
		/// <returns>Filename without not allowed characters.</returns>
		static public string ToFileName(string name, int maxLength)
		{
			var newName = ToFileName(name);
			if (newName != null && newName.Length > maxLength)
				return newName.Substring(1, maxLength);
			return newName;
		}

		/// <summary>
		/// Removes character which are not allowed in a filename.
		/// </summary>
		/// <param name="name">Filename</param>
		/// <returns>Filename without not allowed characters.</returns>
		static public string ToFileName(string name)
		{
			if (name == null || name == string.Empty)
				return string.Empty;

			try
			{
				var f = new System.IO.FileInfo(name);
			}
			catch(ArgumentException ae)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("X {0} {1}", ae.Source, ae.Message));
				return string.Empty;
			}
			catch (Exception e)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("Y{0} {1}", e.Source, e.Message));
			}

			return name;
		}

        public static void SetPointSeperator(ref DataSet ds)
        {
            SetSeperator(ref ds, ",", ".");
        }

        public static void SetCommaSeperator(ref DataSet ds)
        {
            SetSeperator(ref ds, ".", ",");
        }

        public static void SetCurrentCultureSeperator(ref DataSet ds)
        {
            var oldSep = ".";
            var newSep = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            if (newSep == ".")
                oldSep = ",";
            SetSeperator(ref ds, oldSep, newSep);
        }

        public static string SetCurrentCultureSeperator(string str)
        {
            var oldSep = ".";
            var newSep = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            if (newSep == ".")
                oldSep = ",";
            return ParseCol(str, oldSep, newSep, 0);
        }

        private static string ParseCol(string formula, string oldSep, string newSep, int startindex)
        {
            var x = formula.IndexOf(oldSep, startindex);

            if (x == formula.Length - 1 || x == -1)
                return formula;
            if (formula[x + 1] > 47 && formula[x + 1] < 58)
            {
                formula = formula.Remove(x, 1);
                formula = formula.Insert(x, newSep);
            }
            formula = ParseCol(formula, oldSep, newSep, x + 1);
            return formula;
        }

        private static void SetSeperator(ref DataSet ds, string oldSep, string newSep)
        {
            foreach (DataTable table in ds.Tables)
            {
                foreach (DataColumn column in table.Columns)
                {
                    if (column.DataType != typeof(string))
                        continue;
                    if (column.ColumnName.ToLower().IndexOf("formula") == -1
                        && column.ColumnName.ToLower().IndexOf("value") == -1
                        && column.ColumnName.ToLower().IndexOf("parameter") == -1)
                        continue;

                    foreach (DataRow row in table.Rows)
                    {
                        row[column] = ParseCol(row[column].ToString(), oldSep, newSep, 0); //row[column].ToString().Replace(oldSep, newSep);
                    }
                }
            }
            ds.AcceptChanges();
        }

        /// <summary>
        /// Converts number to character 0..25 = A..Z, 26..51 = AA, AB, AC..AZ
        /// </summary>
        /// <param name="index">Number to be converted ( 0-Based)</param>
        /// <returns></returns>
        public static string ToCharacter(int index)
        {
            var count = 0;
            var item = index;

            while (item > 25)
            {
                count++;
                item -= 26;
            }

            if (count > 0)
                return string.Format("{0}{1}", (char)(count + 64), (char)(item + 65)); 
            return string.Format("{0}", (char)(item + 65)); 
        }

        /// <summary>
        /// Converts number to character 0..25 = A..Z, 26..51 = AA, BB, CC..ZZ
        /// </summary>
        /// <param name="index">Number to be converted ( 0-Based)</param>
        /// <returns></returns>
        public static string ToCharacter2(int index)
        {
            var count = 0;
            var item = index;

            while (item > 25)
            {
                count++;
                item -= 26;
            }

            if (count > 0)
            {
                var cc = count;
                var prefix = "";
                while (cc > 0)
                {
                    cc-=26;
                    prefix += (char)(item + count + 64);
                }
                return string.Format("{0}{1}", prefix, (char)(item + count + 64));
            }
                //return string.Format("{0}{1}", (char)(count + 64), (char)(item + 65));
            return string.Format("{0}", (char)(item + 65));
        }

        public static string GetWord(string text, int startIndex, bool allowPointInWord)
        {
            if (text == null || text == string.Empty)
                return string.Empty;

            var retName = string.Empty;
            while (startIndex < text.Length && (char.IsLetterOrDigit(text[startIndex]) || char.IsPunctuation(text[startIndex])))
                retName += text[startIndex++];

            return retName;
        }

        private static bool HasWord(string text, string word, ref int startIndex)
        {
            if (text == null || text == string.Empty)
                return false;

            startIndex = text.ToUpper().IndexOf(word.ToUpper(), startIndex);
            if (startIndex == -1)
                return false;

            if (startIndex == 0 || !char.IsLetter(text[startIndex - 1]))
            {
                if (startIndex + word.Length >= text.Length)
                    return true;

                if (startIndex + word.Length < text.Length && !char.IsLetter(text[startIndex + word.Length]))
                    return true;
            }
            startIndex += 1;
            return HasWord(text, word, ref startIndex);
        }

        public static bool HasWord(string text, string word)
        {
            var start = 0;
            return HasWord(text, word, ref start);
        }
	}
}
