using System;
using System.ComponentModel;

namespace Anthos.Utilities
{
	/// <summary>
	/// CustomInt32Converter is used in property grids to customize error messages during converting.
	/// </summary>
	public class CustomInt32Converter : Int32Converter
	{
		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if(value is string)
			{
				try
				{
					var tmp = int.Parse((string)value);
					return tmp;
				}
				catch
				{
					throw new ArgumentException(string.Format(Controls.Properties.Resources.ConvertorIllegalInt, value));
				}
			}
			return base.ConvertFrom (context, culture, value);
		}
	}

	/// <summary>
	/// CustomDoubleConverter is used in property grids to customize error messages during converting.
	/// </summary>
	public class CustomDoubleConverter : DoubleConverter
	{
		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if(value is string)
			{
				try
				{
					var tmp = double.Parse((string)value);
					return tmp;
				}
				catch
				{
                    throw new ArgumentException(string.Format(Controls.Properties.Resources.ConvertorIllegalDouble, value));
				}
			}
			return base.ConvertFrom (context, culture, value);
		}
	}

	/// <summary>
	/// CustomUInt16Converter is used in property grids to customize error messages during converting.
	/// </summary>
	public class CustomUInt16Converter : UInt16Converter
	{
		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if(value is string)
			{
				try
				{
					var tmp = ushort.Parse((string)value);
					return tmp;
				}
				catch
				{
                    throw new ArgumentException(string.Format(Controls.Properties.Resources.ConvertorIllegalInt, value));
				}
			}
			return base.ConvertFrom (context, culture, value);
		}
	}
}
