using System;
using System.Globalization;

namespace Anthos.Utilities
{
	/// <summary>
	/// Provides natural formatting for numbers without trailing zeros, points or exponential signs.
	/// </summary>
	public class NaturalNumberFormatter
	{
        private static char[] numbers = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
		private static string sep = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
		private static string sepchange = ",";
		private static string _nanText = "#Value";


		static public string ToViewString(double val, bool shorten)
        {
            if (double.IsNaN(val) || double.IsInfinity(val))
                return _nanText;

            if (Math.Abs(val) < 10)
                return val.ToString("0.000");
            else if (Math.Abs(val) < 100)
                return val.ToString("0.00");
            else if (Math.Abs(val) < 1000)
                return val.ToString("0.0");

            if (!shorten)
                return val.ToString("0");

            if (Math.Abs(val) < 100000)
                return (val / 1000).ToString("0.0k");
            else if (Math.Abs(val) < 1000000)
                return (val/1000).ToString("0k");
            else if (Math.Abs(val) < 100000000)
                return (val / 1000000).ToString("0.0M");
            
            return (val/1000000).ToString("0M");
        }

        static public string ToString(float val)
		{
			var sep = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
			var tmp = val.ToString("F10").TrimEnd('0');
			if(tmp.EndsWith(sep))
				return tmp.Substring(0, tmp.Length - sep.Length);
			return tmp;
		}

		static public string ToString(float val, CultureInfo ci)
		{
			
			var sep = ci.NumberFormat.NumberDecimalSeparator;
			var tmp = val.ToString("F10", ci).TrimEnd('0');
			if(tmp.EndsWith(sep))
				return tmp.Substring(0, tmp.Length - sep.Length);
			return tmp;
		}

		static public string ToString(double val)
		{
			var sep = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
			var tmp = val.ToString("F10").TrimEnd('0');
			if(tmp.EndsWith(sep))
				return tmp.Substring(0, tmp.Length - sep.Length);
			return tmp;
		}

		static public string ToString(double val, CultureInfo ci)
		{
			var sep = ci.NumberFormat.NumberDecimalSeparator;
			var tmp = val.ToString("F10", ci).TrimEnd('0');
			if(tmp.EndsWith(sep))
				return tmp.Substring(0, tmp.Length - sep.Length);
			return tmp;			
		}

		static public string ToString(int val)
		{
			return val.ToString();
		}

		static public string ToString(int val, CultureInfo ci)
		{
			return val.ToString(ci);
		}

        /// <summary>
        /// Returns name of value between 1 and 10
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        static public string ToName(int val)
        {
            switch (val)
            {
                case 1:
                    return Controls.Properties.Resources.First;
                case 2:
                    return Controls.Properties.Resources.Second;
                case 3:
                    return Controls.Properties.Resources.Third;
                case 4:
                    return Controls.Properties.Resources.Fourth;
                case 5:
                    return Controls.Properties.Resources.Fifth;
                case 6:
                    return Controls.Properties.Resources.Sixth;
                case 7:
                    return Controls.Properties.Resources.Seventh;
                case 8:
                    return Controls.Properties.Resources.Eighth;
                case 9:
                    return Controls.Properties.Resources.Ninth;
                case 10:
                    return Controls.Properties.Resources.Tenth;
                default:
                    return val.ToString();
            }
        }
        static public System.Data.DataSet ReplaceDecimalSeperator(System.Data.DataSet dataXML)
		{
            sepchange = ",";
            if (sep == ",")
                sepchange = ".";

			try
			{
				foreach(System.Data.DataTable table in dataXML.Tables)
				{
					foreach(System.Data.DataRow row in table.Rows)
					{
						for(var i=0; i<table.Columns.Count; i++)
						{
                            if (table.Columns[i].DataType != typeof(string))
                                continue;

							if (table.Columns[i].ColumnName.ToLower().IndexOf("date") > -1 
                                || table.Columns[i].ColumnName.ToLower().IndexOf("version") > -1 
                                || table.Columns[i].ColumnName.ToLower().IndexOf("name") > -1)
								continue;
							var newStr = ReplaceDecimalSeperator(row[i].ToString());
							if (newStr != row[i].ToString())
								row[i] = newStr;
						}
					}
				}
				dataXML.AcceptChanges();
			}
			catch(Exception e)
			{
				System.Diagnostics.Trace.WriteLine("NumberFormatter: ReplaceDecimalSeperator ds : " + e.Message);
			}

            return dataXML;
		}

		static public string ReplaceDecimalSeperator(string text)
		{
			if (text == null || text == string.Empty)
				return text;

			//Do not change Sperator on Version or Date strings
			if (text.ToLower().IndexOf("date") > -1 || 
                text.ToLower().IndexOf("version") > -1                 ||
                text.ToLower().IndexOf("name") > -1 ||
                text.ToLower().IndexOf("height") > -1)
				return text;

			var c = 0;
			

			try
			{
				var i = text.IndexOf(sepchange);

				while (i > -1 || c > 3000 )
				{
					//if seperator is on last position, leave it
					if (text.Length - 1 == i)
						return text;

					//if Seperator is within a number, change seperator
					if (text.IndexOfAny(numbers, i+1) == i+1)
					{
						text = text.Remove(i,1);
						text = text.Insert(i, sep);
					}
					c++;
					i = text.IndexOf(sepchange, i+1);
				}
			}
			catch(Exception e)
			{
				System.Diagnostics.Trace.WriteLine("Error NumberFormatter: ReplaceDecimalSeperator " + e.Message);
			}

			return text;
		}

        static public string ReplaceToPointSeperator(string text)
        {
            if (text == null || text == string.Empty)
                return text;

            //Do not change Sperator on Version or Date strings
            if (text.ToLower().IndexOf("date") > -1 ||
                text.ToLower().IndexOf("version") > -1 ||
                text.ToLower().IndexOf("name") > -1 ||
                text.ToLower().IndexOf("height") > -1)
                return text;

            var c = 0;


            try
            {
                var i = text.IndexOf(",");

                while (i > -1 || c > 3000)
                {
                    //if seperator is on last position, leave it
                    if (text.Length - 1 == i)
                        return text;

                    //if Seperator is within a number, change seperator
                    if (text.IndexOfAny(numbers, i + 1) == i + 1)
                    {
                        text = text.Remove(i, 1);
                        text = text.Insert(i, ".");
                    }
                    c++;
                    i = text.IndexOf(",", i + 1);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine("Error NumberFormatter: ReplaceToPointSeperator " + e.Message);
            }

            return text;
        }

        static public void ReplaceSpecialCharactersForSaving(ref System.Data.DataSet dataXML)
        {
            try
            {
                foreach (System.Data.DataTable table in dataXML.Tables)
                {
                    foreach (System.Data.DataRow row in table.Rows)
                    {
                        for (var i = 0; i < table.Columns.Count; i++)
                        {
                            if (row[i].ToString() == "\t")
                                row[i] = 9;
                        }
                    }
                }
                dataXML.AcceptChanges();
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine("NumberFormatter: ReplaceDecimalSeperator ds : " + e.Message);
            }
        }

        static public void ReplaceSpecialCharactersForUsing(ref System.Data.DataSet dataXML)
        {
            try
            {
                foreach (System.Data.DataTable table in dataXML.Tables)
                {
                    foreach (System.Data.DataRow row in table.Rows)
                    {
                        for (var i = 0; i < table.Columns.Count; i++)
                        {
                            if (row[i].ToString() == "9")
                                row[i] = "\t";
                        }
                    }
                }
                dataXML.AcceptChanges();
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine("NumberFormatter: ReplaceDecimalSeperator ds : " + e.Message);
            }
        }

        static public string ConvertToHHMMSS(double val)
        {
            var h = 0;
            var m = 0;
            var s = 0;

            while (val > 3600d)
            {
                h++;
                val -= 3600d;
            }

            while (val > 60d)
            {
                m++;
                val -= 60d;
            }
            s = (int)val;

            var ret = string.Format("{0}:{1}", m.ToString("00"), s.ToString("00"));
            if (h > 0)
                ret = string.Format("{0}:{1}", h.ToString("00"), ret);
            return ret;
        }
	}
}
