﻿using System;
using Castle.Core.Internal;
using Gimli.Instrument.Common;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.Outbound;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.NetworkCommunicationService.Actors
{
	internal class ActiveOutboundSession : OutboundSession
	{
		protected readonly string ConnectedComputer;
		protected readonly string ClientId;

		internal ActiveOutboundSession(string connectedComputer, string clientId, IActorService actorService, Func<RequestMessageBase, string, bool> connectionStrategy) : base(actorService, connectionStrategy)
		{
			ConnectedComputer = connectedComputer;
			ClientId = clientId;
		}

		internal override OutboundSession Activate(OutboundTakesControlMessage message, Action<string> logger, Action<GimliResponse> giveAnswer, Func<RequestMessageBase, string, bool> connectionStrategy)
		{
			giveAnswer(GetActivationDeclinedMessage(logger));
			return this;
		}

		internal override OutboundSession Deactivate(OutboundTakesControlMessage message, Action<string> logger, Action<GimliResponse> giveAnswer)
		{
			if (ConnectedComputer != message.Identity || (!ClientId.IsNullOrEmpty() && !message.EventlistenerId.IsNullOrEmpty() && ClientId != message.EventlistenerId))
			{
				if (message.Identity != SpecialMeaningsStrings.GimliMasterLockId)
				{
					giveAnswer(GetActivationDeclinedMessage(logger));
					return this;
				}
			}

			var response = NetworkHelperMethods.AskInstrumentAndAnswer(ActorService, MessageExtensions.CreateRequestMessage(message), giveAnswer);
			if (response.ResponseStatus != ResultStatusEnum.Success) return this;

			ActorService.Get<DataServiceProxyActor>()
				.Tell(MessageExtensions.CreateRequestMessage(new OutboundControlledMessage
				{
					IsOutboundControlled = false
				}));
			return new InactiveOutboundSession(ActorService);
		}

		private static GimliResponse GetActivationDeclinedMessage(Action<string> logger)
		{
			return new GimliResponse(new ErrorInfo { Details = "The reader is controlled by other user." },
				ResultStatusEnum.Error, logger2: logger);
		}

		internal override bool SendToOutboundUser(RequestMessageBase message, Action<string> executeBeforeSend)
		{
			executeBeforeSend(ClientId);
			return ConnectionStrategy(message, ConnectedComputer);
		}

		internal override void ExecuteChecked(string requesterId, string requesterListenerId, Action<string> sendError, Action action)
		{
			if (!IsUserAuthorized(requesterId, requesterListenerId, sendError)) return;

			action();
		}

		internal override string GetConnectedInformation()
		{
			return GetHostnameFromConnectionstring(ConnectedComputer) + " " + ClientId;
		}

		protected override bool IsUserAuthorized(string requesterId, string requesterListenerId, Action<string> sendError)
		{
			if (ConnectedComputer == requesterId && (ClientId == requesterListenerId || requesterListenerId.IsNullOrEmpty())) return true;

			sendError("The reader is not controlled by you.");
			return false;
		}
	}
}
