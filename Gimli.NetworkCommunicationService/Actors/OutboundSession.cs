﻿using System;
using Castle.Core.Internal;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Messages.Outbound;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.NetworkCommunicationService.Actors
{
	internal abstract class OutboundSession
	{
		protected readonly IActorService ActorService;
		protected readonly Func<RequestMessageBase, string, bool> ConnectionStrategy;

		protected OutboundSession(IActorService actorService, Func<RequestMessageBase, string, bool> connectionStrategy)
		{
			ActorService = actorService;
			ConnectionStrategy = connectionStrategy;
		}

		internal abstract OutboundSession Activate(OutboundTakesControlMessage message, Action<string> logger, Action<GimliResponse> giveAnswer, Func<RequestMessageBase, string, bool> connectionStrategy);
		internal abstract OutboundSession Deactivate(OutboundTakesControlMessage message, Action<string> logger, Action<GimliResponse> giveAnswer);
		internal abstract bool SendToOutboundUser(RequestMessageBase message, Action<string> executeBeforeSend);
		internal abstract void ExecuteChecked(string requesterId, string requesterListenerId, Action<string> sendError, Action action);
		internal abstract string GetConnectedInformation();

		protected abstract bool IsUserAuthorized(string requesterId, string requesterListenerId, Action<string> sendError);

		internal static string GetHostnameFromConnectionstring(string connectionstring)
		{
			try
			{
				if (connectionstring.IsNullOrEmpty()) return connectionstring;
				var atSplitParts = connectionstring.Split(new[] { "@" }, StringSplitOptions.None);
				if (atSplitParts.Length < 2) return connectionstring;
				var addressParts = atSplitParts[1].Split(new[] { "/" }, StringSplitOptions.None);
				if (addressParts.Length < 2) return connectionstring;
				var namePort = addressParts[0].Split(new[] { ":" }, StringSplitOptions.None);
				return namePort.Length < 2 ? connectionstring : namePort[0];
			}
			catch (Exception)
			{
				return connectionstring;
			}
		}
	}
}
