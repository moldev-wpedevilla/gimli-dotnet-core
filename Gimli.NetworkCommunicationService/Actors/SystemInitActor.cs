﻿using System;
using System.Linq;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Actors;
using MolDev.Common.Akka.Interfaces;
using System.ServiceModel;
//using System.ServiceModel.Discovery;
using Gimli.Instrument.Common;

namespace Gimli.NetworkCommunicationService.Actors
{
	public class SystemInitActor : SystemInitActorBase
	{
		//internal static ServiceHost DiscoverableServiceHost;
		public SystemInitActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
		}

		protected override bool L1_StandardImplementation(IActorsystemOverallState systemState)
		{
			ActorService.Get<NetworkCommunicationServiceRoutingActor>().Tell(new StartListeningMessage());

			// TODO someone: do something to reenable this feature
			//// starting WCF service for discoverability only
			//DiscoverableServiceHost = new ServiceHost(typeof (DiscoverableService),
			//	new Uri(string.Format(SpecialMeaningsStrings.DiscoverableServiceIdMask, HelperMethods.GetIPAddressList().First(),
			//		SpecialMeaningsStrings.DiscoverableServicePort, System.Net.Dns.GetHostName())));
			//var serviceDiscoveryBehavior = new ServiceDiscoveryBehavior();
			//serviceDiscoveryBehavior.AnnouncementEndpoints.Add(new UdpAnnouncementEndpoint());
			//DiscoverableServiceHost.Description.Behaviors.Add(serviceDiscoveryBehavior);
			//DiscoverableServiceHost.AddServiceEndpoint(new UdpDiscoveryEndpoint());
			//DiscoverableServiceHost.AddServiceEndpoint(typeof(IInstrumentService), new NetTcpBinding(), SpecialMeaningsStrings.DiscoverableServiceName);
			//DiscoverableServiceHost.Open();

			return base.L1_StandardImplementation(systemState);
		}

		protected override bool L3_StandardImplementation(IActorsystemOverallState systemState)
		{
			if (SystemState.WasRestarted) NetworkHelperMethods.RetakeConrtolFromSMP(ActorService, Log);
			return base.L3_StandardImplementation(systemState);
		}

		protected override bool InitProxies(IActorService actorService)
		{
			var succeeded = IsProxyConnected<DataServiceProxyActor>(actorService, GetGuaranteedAnswer);
			return IsProxyConnected<InstrumentServiceProxyActor>(actorService, GetGuaranteedAnswer) && succeeded;
		}
	}
}
