﻿using System;
using Akka.Actor;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.Instrument;
using Gimli.Shared.Actors.Messages.Outbound;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.NetworkCommunicationService.Actors
{
	public class InstrumentCommunicationActor : RemoteRegistrationActor
	{
        public InstrumentCommunicationActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
        {
	        OutboundSession outboundSession = new InactiveOutboundSession(actorService);

			Receive<RequestMessage<DataeventMessage>>(message =>
			{
				Log.Debug($"Sending dataevent to {outboundSession.GetConnectedInformation()}: {message.Model.Payload}");
				outboundSession.SendToOutboundUser(message, clientConnectedToTarget =>
				{
					message.Model.EventlistenerId = clientConnectedToTarget;
				});
			});

			Receive<RequestMessage<HeartbeatMessage>>(message =>
			{
				var success = outboundSession.SendToOutboundUser(message, clientConnectedToTarget =>
				{
					message.Model.EventlistenerId = clientConnectedToTarget;
				});

				if (success) return;

				NetworkHelperMethods.RetakeConrtolFromSMP(actorService, Log);
				outboundSession = new InactiveOutboundSession(actorService);
			});

			Receive<RequestMessage<NotifyRetakeControl>>(message =>
			{
				Log.Info($"Onboard informs {outboundSession.GetConnectedInformation()} that it retakes control.");
				outboundSession.SendToOutboundUser(message, clientConnectedToTarget =>
				{
					message.Model.EventlistenerId = clientConnectedToTarget;
				});

				outboundSession = new InactiveOutboundSession(actorService);
			});

			// ######## OUTBOUND REQUESTS ########

			Receive<RequestMessage<GetDeviceIdMessage>>(message =>
			{
				Log.Debug($"{OutboundSession.GetHostnameFromConnectionstring(message.Model.MessageTarget)} wants to get device id.");
				var response = NetworkHelperMethods.AskInstrumentAndAnswer(actorService, message, responseForOutbound =>  Sender.Tell(responseForOutbound)) as GetDeviceIdResponseMessage;
				var logpart = response == null ? string.Empty : response.Identity;
				Log.Debug("Returning response: " + logpart);
			});

			Receive<RequestMessage<GetInstrumentStatusMessage>>(message =>
			{
				Log.Debug($"{OutboundSession.GetHostnameFromConnectionstring(message.Model.MessageTarget)} wants to get instrument status.");
				var response = NetworkHelperMethods.AskInstrumentAndAnswer(actorService, message, responseForOutbound => Sender.Tell(responseForOutbound)) as GetInstrumentStatusResponseMessage;
				var logpart = response == null ? string.Empty : $"state: {response.InstrumentState.State}, outboundId: {response.InstrumentState.OutboundId}, locked: {response.InstrumentState.IsOutboundLocked}";
				Log.Debug("Returning response: " + logpart);
			});

			Receive<RequestMessage<OutboundTakesControlMessage>>(message =>
			{
				var logPart = message.Model.Active ? "take" : "release";
				Log.Info($"{OutboundSession.GetHostnameFromConnectionstring(message.Model.Identity)} {message.Model.EventlistenerId} wants to {logPart} control of the instrument.");

				Action<GimliResponse> responder = responseForOutbound => Sender.Tell(responseForOutbound);

				outboundSession = message.Model.Active
					? outboundSession.Activate(message.Model, errorText => Log.Error(errorText), responder, (msg, target) => ForwardToCertain(msg, target, null))
					: outboundSession.Deactivate(message.Model, errorText => Log.Error(errorText), responder);
			});

			Receive<RequestMessage<OutboundCommandMessage>>(message =>
			{
				Log.Info($"Outbound command received from {OutboundSession.GetHostnameFromConnectionstring(message.Model.Identity)} {message.Model.EventlistenerId}: type - {message.Model.CommandType}, params: {message.Model.CommandParameters}");

				outboundSession.ExecuteChecked(message.Model.Identity, message.Model.EventlistenerId,
					AnswerWithError,
					() => actorService.Get<InstrumentServiceProxyActor>().Forward(message));
			});

			Receive<RequestMessage<AbortMessage>>(message =>
			{
				Log.Info($"Abort command received from {OutboundSession.GetHostnameFromConnectionstring(message.Model.Identity)} {message.Model.EventlistenerId}");

				outboundSession.ExecuteChecked(message.Model.Identity, message.Model.EventlistenerId,
					AnswerWithError,
					() =>
					{
						var abortSuccess = NetworkHelperMethods.AskInstrumentAndAnswer(actorService, message, responseForOutbound => {});

						if (abortSuccess.ResponseStatus != ResultStatusEnum.Success)
						{
							Self.Tell(MessageExtensions.CreateRequestMessage(new DataeventMessage { Payload = "- No command was executed while abort." }));
						}
					});
			});
		}
		private void AnswerWithError(string errorText)
		{
			Sender.Tell(new GimliResponse(new ErrorInfo { Details = errorText }, ResultStatusEnum.Error,
				Log));
		}
	}
}
