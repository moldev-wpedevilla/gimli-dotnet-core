﻿using System;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.Outbound;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.NetworkCommunicationService.Actors
{
	internal class InactiveOutboundSession : OutboundSession
	{
		public InactiveOutboundSession(IActorService actorService) : base(actorService, (msg, targetId) => false)
		{
		}

		internal override OutboundSession Activate(OutboundTakesControlMessage message, Action<string> logger, Action<GimliResponse> giveAnswer, Func<RequestMessageBase, string, bool> connectionStrategy)
		{
			var response = NetworkHelperMethods.AskInstrumentAndAnswer(ActorService, MessageExtensions.CreateRequestMessage(message), giveAnswer);
			if (response.ResponseStatus != ResultStatusEnum.Success) return this;

			ActorService.Get<DataServiceProxyActor>()
				.Tell(MessageExtensions.CreateRequestMessage(new OutboundControlledMessage
				{
					IsOutboundControlled = true,
					IpAddressOfClient = string.Empty,
					EventlistenerId = string.Empty
				}));

			return new ActiveOutboundSession(message.Identity, message.EventlistenerId, ActorService, connectionStrategy);
		}

		internal override OutboundSession Deactivate(OutboundTakesControlMessage message, Action<string> sendError, Action<GimliResponse> giveAnswer)
		{
			giveAnswer(new GimliResponse());
			return this;
		}

		internal override bool SendToOutboundUser(RequestMessageBase message, Action<string> executeBeforeSend)
		{
			return false;
		}

		internal override void ExecuteChecked(string requesterId, string requesterListenerId, Action<string> sendError, Action action)
		{
			IsUserAuthorized(requesterId, requesterListenerId, sendError);
		}
		internal override string GetConnectedInformation()
		{
			return "<none>";
		}

		protected override bool IsUserAuthorized(string requesterId, string requesterListenerId, Action<string> sendError)
		{
			sendError("The reader is not controlled by you.");
			return false;
		}
	}
}
