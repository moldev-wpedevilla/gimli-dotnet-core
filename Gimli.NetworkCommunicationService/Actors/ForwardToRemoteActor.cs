﻿using Akka.Actor;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.NetworkCommunicationService.Actors
{
	public class ForwardToRemoteActor : RemoteRegistrationActor
	{
        public ForwardToRemoteActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			Receive<RequestMessage<RemoteRegistrationMessage>>(message =>
			{
				Log.Info($"Received registration message for {message.Model.ConnectionString}.");
				Sender.Tell(HandleRemoteRegistrationRequest(message));
			});

			Receive<RequestMessage<StoreDocumentMessage>>(message =>
			{
				if (Subscribers.Count < 1)
				{
					Sender.Tell(new GimliResponse(new ErrorInfo {Message = "No subscribers."}, ResultStatusEnum.Error, Log));
					return;
				}

				message.Model.RemoteHostname = HelperMethods.GetLocalHostName();

				Sender.Tell(GetGuaranteedAnswer(m => PublishToAll(null, message, TellSuccessLevel.All, 22), message));
			});
        }
	}
}
