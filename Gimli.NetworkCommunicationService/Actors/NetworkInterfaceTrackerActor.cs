﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using MolDev.Common.Akka.Interfaces;

namespace Gimli.NetworkCommunicationService.Actors
{
	public class NetworkInterfaceTrackerActor : GimliActorBase
	{
		public NetworkInterfaceTrackerActor(IActorService actorService, IActorsystemOverallState systemState, Func<GimliActorHost> host) : base(actorService, systemState)
		{
			Receive<StartListeningMessage>(message =>
			{
				Task.Run(() =>
				{
					try
					{
						while (host().IpAddressUsedForConfiguration == HelperMethods.GetIPAddressList().ToList().First() || host() == null)
						{
							Thread.Sleep(2000);
						}
					}
					catch (Exception e)
					{
						Log.Error("Exception thrown while determining IP address. " + e);
					}

					Log.Info("---------> Restarting service because assigned IP changed.");

					// give it some time to log
					Thread.Sleep(1000);

					// TODO someone: do something to reenable this feature
					//try
					//{
					//	SystemInitActor.DiscoverableServiceHost?.Close();
					//}
					//catch (Exception)
					//{
					//	Log.Error("Unable to close discoverable service.");
					//}

					host().Restart(RemoteActorConfiguration.Instance.ServiceConfigurations[ActorServiceKey.Network]);
				});
			});
		}
	}
}
