﻿using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.Instrument;
using Gimli.Shared.Actors.Messages.Outbound;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.NetworkCommunicationService.Actors
{
	public class NetworkCommunicationServiceRoutingActor : ToplevelRoutingActorBase
	{
		private readonly string NetworkActorName = nameof(NetworkInterfaceTrackerActor);
		private readonly string ForwardActorName = nameof(ForwardToRemoteActor);
		private readonly string InstrumentCommunicationActorName = nameof(InstrumentCommunicationActor);
		private readonly string InstrumentCommunicationActorStatusName = nameof(InstrumentCommunicationActor) + "GetStatusOrId";
		private const string SystemInitActorName = nameof(SystemInitActor);

		public NetworkCommunicationServiceRoutingActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			Receive<RequestMessage<RemoteRegistrationMessage>>(message =>
			{
				GetChild<ForwardToRemoteActor>(ForwardActorName).Forward(message);
			});

			Receive<RequestMessage<StoreDocumentMessage>>(message =>
			{
				GetChild<ForwardToRemoteActor>(ForwardActorName).Forward(message);
			});

			Receive<StartListeningMessage>(message =>
			{
				GetChild<NetworkInterfaceTrackerActor>(NetworkActorName).Forward(message);
			});

			Receive<RequestMessage<GetInstrumentStatusMessage>>(message =>
			{
				GetChild<InstrumentCommunicationActor>(InstrumentCommunicationActorStatusName).Forward(message);
			});

			Receive<RequestMessage<GetDeviceIdMessage>>(message =>
			{
				GetChild<InstrumentCommunicationActor>(InstrumentCommunicationActorStatusName).Forward(message);
			});

			Receive<RequestMessage<DataeventMessage>>(message =>
			{
				GetChild<InstrumentCommunicationActor>(InstrumentCommunicationActorName).Forward(message);
			});

			Receive<RequestMessage<OutboundTakesControlMessage>>(message =>
			{
				GetChild<InstrumentCommunicationActor>(InstrumentCommunicationActorName).Forward(message);
			});

			Receive<RequestMessage<OutboundCommandMessage>>(message =>
			{
				GetChild<InstrumentCommunicationActor>(InstrumentCommunicationActorName).Forward(message);
			});

			Receive<RequestMessage<HeartbeatMessage>>(message =>
			{
				GetChild<InstrumentCommunicationActor>(InstrumentCommunicationActorName).Forward(message);
			});

			Receive<RequestMessage<NotifyRetakeControl>>(message =>
			{
				GetChild<InstrumentCommunicationActor>(InstrumentCommunicationActorName).Forward(message);
			});

			Receive<RequestMessage<AbortMessage>>(message =>
			{
				GetChild<InstrumentCommunicationActor>(InstrumentCommunicationActorName).Forward(message);
			});
		}

		protected override IActor GetSysInitActor()
		{
			return GetChild<SystemInitActor>(SystemInitActorName);
		}

		protected override void PreStart()
		{
			GetChild<NetworkInterfaceTrackerActor>(NetworkActorName);
			GetChild<ForwardToRemoteActor>(ForwardActorName);
			GetChild<InstrumentCommunicationActor>(InstrumentCommunicationActorName);
			GetChild<InstrumentCommunicationActor>(InstrumentCommunicationActorStatusName);

			base.PreStart();
		}
	}
}
