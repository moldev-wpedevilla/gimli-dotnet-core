﻿using System;
using System.Reflection;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;

namespace Gimli.NetworkCommunicationService
{
	class Program
	{
		static void Main()
		{
			var thisAssemblyName = Assembly.GetEntryAssembly().GetName().Name;
			var configFilePath = thisAssemblyName + ".dll.config";
			AppConfigurationManager.Build(configFilePath);

			var factory = new GimliActorHostServiceFactory();
			factory.Run(
				RemoteActorConfiguration.Instance.ServiceConfigurations[ActorServiceKey.Network],
				"Gimli.NetworkCommunicationService",
				"Gimli NetworkCommunicationService",
				"Gimli NetworkCommunicationService with TopShelf",
				configFilePath,
				container =>
				{
					RegisterComponents(container, factory);
				});
		}

		internal static void RegisterComponents(IWindsorContainer container, GimliActorHostServiceFactory factory)
		{
			container.Register(Component
						.For<IActorsystemOverallState>()
						.Instance(new ActorsystemOverallState { ServiceId = ActorServiceKey.Network })
						.LifestyleSingleton());
			container.Register(Component.For<Func<GimliActorHost>>().Instance(() => factory.Host));
		}
	}
}
