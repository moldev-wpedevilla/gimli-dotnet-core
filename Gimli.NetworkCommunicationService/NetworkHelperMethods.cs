﻿using System;
using Akka.Actor;
using Akka.Event;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.Outbound;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.NetworkCommunicationService
{
	internal static class NetworkHelperMethods
	{
		internal static void RetakeConrtolFromSMP(IActorService actorService, ILoggingAdapter log)
		{
			try
			{
				var outboundSessionTask = actorService.Get<DataServiceProxyActor>()
					.Ask<GetOutboundSessionResponseMessage>(MessageExtensions.CreateRequestMessage(new GetOutboundSessionMessage()));
				outboundSessionTask.Wait();
				if (!outboundSessionTask.Result.Session.IsClientConnected) return;

				log.Info("Taking back control after SMP lost connection.");
				actorService.Get<DataServiceProxyActor>()
					.Tell(MessageExtensions.CreateRequestMessage(new OutboundControlledMessage
					{
						IsOutboundControlled = false,
					}));

				var task = actorService.Get<InstrumentServiceProxyActor>()
					.Ask<GimliResponse>(MessageExtensions.CreateRequestMessage(new AbortMessage()));
				task.Wait();

				AskInstrumentAndAnswer(actorService,
					MessageExtensions.CreateRequestMessage(new OutboundTakesControlMessage { Active = false }), r => {});
			}
			catch (Exception e)
			{
				log.Error("Exception catched during reinit after SMP lost connection." + e.Message);
			}

			log.Info("Finished taking back control.");
		}

		internal static GimliResponse AskInstrumentAndAnswer(IActorService actorService, RequestMessageBase message, Action<GimliResponse> giveAnswer)
		{
			// No forward possible here because only networkservice is able to communicate with remote because remote initiated communication here
			var task = actorService.Get<InstrumentServiceProxyActor>().Ask<GimliResponse>(message);
			task.Wait();
			giveAnswer(task.Result);
			return task.Result;
		}
	}
}
