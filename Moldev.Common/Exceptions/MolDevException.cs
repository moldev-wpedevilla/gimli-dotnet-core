﻿using MolDev.Common.Messages.Errors;
using System;
using System.Runtime.Serialization;

namespace MolDev.Common.Exceptions
{
  [Serializable]
  public class MolDevException : Exception
  {
    public MolDevException() { }
    public MolDevException(string message) : base(message) { }
    public MolDevException(string message, ResultStatusEnum status, ErrorInfo error) : base(message)
    {
      Status = status;
      Error = error;
    }
    protected MolDevException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    public MolDevException(string message, Exception innerException) : base(message, innerException) { }

    public ResultStatusEnum Status { get; set; }
    public ErrorInfo Error { get; set; }
  }
}
