﻿using MolDev.Common.Messages.Context;
using System.Globalization;

namespace MolDev.Common.Helpers
{
  public static class MessageHelper
  {
    public static CultureInfo GetCulture(this object message)
    {
      var msgBase = message as MessageBase;
      if (msgBase != null)
        return GetCulture(msgBase.ExecutionContext);
      return null;
    }
    public static CultureInfo GetCulture(IExecutionContext context)
    {
      string language = null;
      if (context != null && !string.IsNullOrEmpty(context.Language))
      {
        language = context.Language;
      }
      if (!string.IsNullOrEmpty(language))
      {
        return CultureInfo.GetCultureInfo(language);
      }
      return null;
    }

  }
}
