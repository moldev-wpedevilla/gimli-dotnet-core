﻿using System.IO;
using System.Linq;

namespace MolDev.Common.Helpers
{
  public static class StringHelper
  {
    public static string Humanize(this string key)
    {
      return key.Substring(0, 1).ToUpper() + key.Substring(1).ToLower();
    }

    public static string CamelCase(this string key)
    {
      return key.Substring(0, 1).ToLower() + key.Substring(1);
    }

    public static string GetFileName(this string fileName)
    {
      char[] invalidChars = Path.GetInvalidFileNameChars();
      return new string(fileName.Where(c => !invalidChars.Contains(c)).ToArray());
    }

    public static string RemoveLastBackSlash(this string src)
    {
      if (!src.EndsWith("\\")) return src;
      return src.TrimEnd(new char[] { '\\' });
    }

  }
}
