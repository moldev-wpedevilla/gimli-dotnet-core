﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Dynamic;
using System.Xml.Linq;
using System.Collections.Generic;

namespace MolDev.Common.Helpers
{
  public static class SettingsHelper
  {
	private static dynamic mConfig;
    public static string GetDataPath(string key, string configFileName)
    {
      var localPath = string.Empty;
      if (!string.IsNullOrEmpty(key))
        localPath = ReadSetting(key, configFileName);
#if DEBUG
      return localPath;
#else   
      var configDataPath = ReadSetting("moldevhost:DataPath");
      if (!string.IsNullOrEmpty(configDataPath))
      {
        configDataPath = Environment.ExpandEnvironmentVariables(configDataPath);
        return Path.Combine(configDataPath, localPath);
      }
      return localPath;
#endif
    }

    public static dynamic GetConfiguration(string configFileName)
    {
		if (mConfig != null) return mConfig;

		XDocument doc = XDocument.Load(configFileName);
		string jsonText = JsonConvert.SerializeXNode(doc);
		mConfig = JsonConvert.DeserializeObject<ExpandoObject>(jsonText);
		return mConfig;
	}

    public static string ReadSetting(string key, string configFileName)
    {
      string result = null;
      try
      {
		dynamic configuration = GetConfiguration(configFileName);
        var appSettings = configuration.configuration.appSettings.add;
		var settingsDictionary = new Dictionary<string, string>();
		foreach (var add in (List<dynamic>)appSettings)
		{
			var addMiniDictionary = (IDictionary<string, object>)add;
			settingsDictionary[addMiniDictionary["@key"].ToString()] = addMiniDictionary["@value"].ToString();
		}

		return settingsDictionary.ContainsKey(key) ? settingsDictionary[key] : result;
      }
      catch (Exception ex)
      {
        return result;
      }
    }

    public static object GetSection(string configFileName, string sectionName)
    {
      object result = null;
      try
      {
          dynamic configuration = GetConfiguration(configFileName);
          if (configuration != null)
          {
			result = configuration.GetType().GetProperty(sectionName).GetValue(configuration, null);
		  }
      }
      catch (Exception ex)
      {
        return result;
      }
      return result;
    }

    public static string ReadSettingString(string key, string defaultValue, string configFileName = null)
    {
      var result = ReadSetting(key, configFileName);
      return result == null ? defaultValue : result;
    }

    public static int ReadSettingInt(string key, int defaultValue, string configFileName = null)
    {
      int result = defaultValue;
      var str = ReadSetting(key, configFileName);
      if (!string.IsNullOrEmpty(str))
      {
        if (!int.TryParse(str, out result))
        {
          result = defaultValue;
        }
      }
      return result;
    }
    public static bool ReadSettingBool(string key, bool defaultValue, string configFileName = null)
    {
      bool result = defaultValue;
      var str = ReadSetting(key, configFileName);
      if (!string.IsNullOrEmpty(str))
      {
        if (!bool.TryParse(str, out result))
        {
          result = defaultValue;
        }
      }
      return result;
    }

  }
}
