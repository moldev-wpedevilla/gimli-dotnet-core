﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MolDev.Common.Helpers
{
  /// <summary>
  /// Helper class for automatic tests
  /// </summary>
  public static class CompareObjectsHelper
  {
    /// <summary>
    /// Search collection for the model  that satisfies the given predicate and compares it with the etalon model
    /// </summary>
    public static bool CollectionContainsProperModel<TModel>(IEnumerable<TModel> source, TModel etalonModel, Func<TModel, TModel, bool> searchPredicate)
    {
      var testModel = source.FirstOrDefault(t => searchPredicate(t, etalonModel));
      return testModel != null && CompareModels(testModel, etalonModel);
    }

    /// <summary>
    /// Compares tested model with etalon model
    /// </summary>
    /// <returns>true if both models are equal</returns>
    public static bool CompareModels<TModel>(TModel model1, TModel model2)
    {
      if (model1 == null) throw new ArgumentNullException("model1");
      if (model2 == null) throw new ArgumentNullException("model2");

      var modelType = model2.GetType();
      foreach (var property in modelType.GetProperties(BindingFlags.Instance | BindingFlags.Public))
      {
        var property1 = property.GetValue(model1);
        var property2 = property.GetValue(model2);

        if (!CompareProperties(property1, property2))
          return false;
      }

      return true;
    }

    private static bool CompareProperties<TProperty>(TProperty property1, TProperty property2)
    {
      if (property1 == null && property2 != null || property2 == null && property1 != null) return false;
      if (property1 == null) return true;

      var propertyType = property1.GetType();
      if (!propertyType.IsClass || propertyType == typeof(string)) return Equals(property1, property2);

      var collectionProperty = property1 as IEnumerable;
      return collectionProperty == null ? CompareModels(property1, property2) : CompareCollections(collectionProperty, (IEnumerable)property2);
    }

    private static bool CompareCollections(IEnumerable collection1, IEnumerable collection2)
    {
      var enumerator1 = collection1.GetEnumerator();
      var enumerator2 = collection2.GetEnumerator();
      while (enumerator1.MoveNext())
      {
        if (!enumerator2.MoveNext() || !CompareProperties(enumerator1.Current, enumerator2.Current)) return false;
      }

      return true;
    }

    public static string GetModelsDifferences<TModel>(TModel model1, TModel model2)
    {
      var result = string.Empty;
      if (model1 == null) throw new ArgumentNullException("model1");
      if (model2 == null) throw new ArgumentNullException("model2");

      var modelType = model2.GetType();
      foreach (var property in modelType.GetProperties(BindingFlags.Instance | BindingFlags.Public))
      {
        var property1 = property.GetValue(model1);
        var property2 = property.GetValue(model2);

        if (!CompareProperties(property1, property2))
        {
          result += string.Format("property = {0} value1 = {1}, value2 = {2} \n", property.Name, property1, property2);
        }
      }

      return result;
    }

  }
}
