﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace MolDev.Common.Helpers
{
  public static class JsonHelper
  {
    public static T LoadObjectFromFile<T>(string filePathName, JsonSerializerSettings jsonSettings)
    {
      var result = default(T);
      if (!File.Exists(filePathName)) throw new ArgumentException(string.Format("Couldn't find '{0}' ", filePathName));
      string jsonString = File.ReadAllText(filePathName);

      result = JsonConvert.DeserializeObject<T>(jsonString, jsonSettings);
      return result;
    }
    public static void WriteObjectToFile<T>(string filePathName, JsonSerializerSettings jsonSettings, T writeObject)
    {
      if (!File.Exists(filePathName)) throw new ArgumentException(string.Format("Couldn't find '{0}' ", filePathName));
      string jsonString = JsonConvert.SerializeObject(writeObject, jsonSettings);
      File.WriteAllText(filePathName, jsonString);
    }
  }
}
