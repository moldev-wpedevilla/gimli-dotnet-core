﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace MolDev.Common.Helpers
{
  public class SemicolonCSVData
  {
    private byte[] data;
    private char delim = ',';
    private char quoterSymb = '"';
    private string headerColumn;
    private IDictionary<string, string> replacements;
    private List<string> headers;
    private Dictionary<string, List<string>> values;
    private Dictionary<char, int> alphaBetic;

    public SemicolonCSVData(byte[] data, char delimiter, string headerColumn = null, IDictionary<string, string> replacements = null)
    {
      this.data = data;
      delim = delimiter;
      this.headerColumn = headerColumn;
      this.replacements = replacements;
    }

    private void ColumnInit()
    {
      string keyPattern = @"([a-z]\d)";
      Regex keyFilter = new Regex(keyPattern);
      var reader = new StreamReader(new MemoryStream(data));
      if (!reader.EndOfStream)
      {
        headers = new List<string>();
        values = new Dictionary<string, List<string>>();
        List<string> valArr = new List<string>();
        bool addData = true;
        var line = reader.ReadLine();
        bool hasWellName = false;
        if (DeconstructLine(line, valArr))
        {
          int headerColumnIndex = 0;
          for (int i = 0; i < valArr.Count(); i++)
          {
            string header = valArr[i].Trim();
            if (!hasWellName && header.ToLower() == "well name") hasWellName = true;
            if ((string.IsNullOrEmpty(headerColumn) && i == 0) || (!string.IsNullOrEmpty(headerColumn) && header.ToLower().StartsWith(headerColumn)))
            {
              headerColumnIndex = i;
            }
            else
            {
              if (replacements != null)
              {
                replacements.Keys.ToList().ForEach(o => { if (header.ToLower().StartsWith(o)) header = replacements[o]; });
              }
              addData = !headers.Any(h => h.ToLower() == header.ToLower());
              if (!addData)
              {
                header = string.Empty;
              }
              headers.Add(header);
            }
          }
          if (!hasWellName) throw new ValidationException("AnnotationFileFormatError");
          while (!reader.EndOfStream)
          {
            line = reader.ReadLine();
            valArr.Clear();
            if (DeconstructLine(line, valArr))
            {
              var strValues = new List<string>();
              string key = "";
              for (int i = 0; i < (headers.Count() + 1); i++)
              {
                string v = (i < valArr.Count()) ? valArr[i].Trim() : string.Empty;
                if (i == headerColumnIndex)
                {
                  key = v.ToLower();
                  if (!keyFilter.IsMatch(key)) throw new ValidationException("AnnotationFileFormatError");
                  values[key] = strValues;
                }
                else
                {
                  if (!string.IsNullOrEmpty(headers[i - 1]))
                  {
                    values[key].Add(v);
                  }
                }
              }
            }
          }
          headers = headers.Where(h => !string.IsNullOrEmpty(h)).ToList();
        }
      }
    }

    private void PlateInit()
    {
      alphaBetic = new Dictionary<char, int>();
      int ii = 1;
      for (char c = 'a'; c <= 'z'; c++, ii++)
      {
        alphaBetic.Add(c, ii);
      }
      var reader = new StreamReader(new MemoryStream(data));
      if (!reader.EndOfStream)
      {
        headers = new List<string>();
        values = new Dictionary<string, List<string>>();
        List<string> valArr = new List<string>();
        bool addData = true;
        var firstStringValues = new List<string>();
        while (!reader.EndOfStream)
        {
          var line = reader.ReadLine();
          valArr.Clear();
          if (DeconstructLine(line, valArr))
          {
            var strValues = new List<string>();
            string firstColValue;
            string key = "";
            var v = valArr[0].Trim();
            if (!string.IsNullOrEmpty(v) && addData)
            {
              firstColValue = v;
              if (v.Length == 1 && alphaBetic.ContainsKey(v.ToLower()[0]))
              {
                for (int i = 1; i < valArr.Count(); i++)
                {
                  key = firstColValue.ToLower() + firstStringValues[i];
                  v = valArr[i].Trim();
                  if (true/*!string.IsNullOrEmpty(v)*/)
                  {
                    if (!values.ContainsKey(key))
                    {
                      values[key] = new List<string>();
                    }
                    values[key].Add(v);
                  }
                }
              }
              else
              {
                firstStringValues.Clear();
                for (int l = 0; l < valArr.Count(); l++)
                {
                  var e = valArr[l];
                  decimal d = 0m;
                  if (l > 0 && !decimal.TryParse(e, out d))
                  {
                    throw new ValidationException("AnnotationFileFormatError");
                  }
                  if (l > 0 && Math.Truncate(d) != d)
                  {
                    throw new ValidationException("AnnotationFileFormatError");
                  }
                  firstStringValues.Add(e);
                }
                if (replacements != null)
                {
                  replacements.Keys.ToList().ForEach(o => { if (firstColValue.ToLower().StartsWith(o)) firstColValue = replacements[o]; });
                }
                addData = !headers.Any(h => h.ToLower() == firstColValue.ToLower());
                if (addData)
                {
                  headers.Add(firstColValue);
                }
              }
            }
          }
        }
      }
    }

    public SemicolonCSVData Init(bool isColumnFormat = true)
    {
      if (isColumnFormat)
      {
        ColumnInit();
      }
      else
      {
        PlateInit();
      }
      return this;
    }
    public List<string> Headers { get { return headers; } }
    public Dictionary<string, List<string>> Values { get { return values; } }

    public bool DeconstructLine(string lineText, List<string> row)
    {
      //      row.LineText = ReadLine();
      if (string.IsNullOrEmpty(lineText)) return false;

      int pos = 0;
      int rows = 0;

      while (pos < lineText.Length)
      {
        string value;

        // Special handling for quoted field
        if (lineText[pos] == quoterSymb)
        {
          // Skip initial quote
          pos++;

          // Parse quoted value
          int start = pos;
          while (pos < lineText.Length)
          {
            // Test for quote character
            if (lineText[pos] == quoterSymb)
            {
              // Found one
              pos++;

              // If two quotes together, keep one
              // Otherwise, indicates end of value
              if (pos >= lineText.Length || lineText[pos] != quoterSymb)
              {
                pos--;
                break;
              }
            }
            pos++;
          }
          value = lineText.Substring(start, pos - start);
          value = value.Replace("\"\"", "\"");
        }
        else
        {
          // Parse unquoted value
          int start = pos;
          while (pos < lineText.Length && lineText[pos] != delim) pos++;
          value = lineText.Substring(start, pos - start);
        }

        // Add field to list
        if (rows < row.Count) row[rows] = value;
        else row.Add(value);
        rows++;

        // Eat up to and including next comma
        while (pos < lineText.Length && lineText[pos] != delim) pos++;
        if (pos == lineText.Length - 1 && lineText[pos] == delim) row.Add(string.Empty);
        if (pos < lineText.Length) pos++;
      }
      // Delete any unused items
      //while (row.Count > rows)
      //  row.RemoveAt(rows);

      // Return true if any columns read
      return (row.Count > 0);
    }
  }
}
