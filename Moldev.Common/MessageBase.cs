﻿using MolDev.Common.Messages.Context;
using System;

namespace MolDev.Common
{
  [Serializable]
  public class MessageBase
  {
    public IExecutionContext ExecutionContext { get;  set; }
    public MessageBase() { }
    public MessageBase(IExecutionContext executionContext)
    {
      ExecutionContext = executionContext;
    }
  }
}
