﻿using MolDev.Common.Akka.Interfaces;
using System;

namespace MolDev.Common.Akka.Messages
{
  [Serializable]
  public class ActorTerminated
  {
    public IActor Actor { get; set; }
    public string ActorPath { get; set; }
  }
}
