﻿using MolDev.Common.Akka.Interfaces;
using System;

//namespace MolDev.Common.Actor.Messages
namespace MolDev.Common.Akka.Messages
{
  [Serializable]
  public class ActorWasFound
  {
    public IActor Actor { get; set; }
    public string ActorPath { get; set; }
  }
}
