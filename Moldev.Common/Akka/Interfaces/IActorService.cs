﻿namespace MolDev.Common.Akka.Interfaces
{
  public interface IActorService
  {
    IActor Get<TActor>(int? timeout = null) where TActor : class;
    IActor Get<TActor>(string path) where TActor : class;
    IActor Get(string name, int? timeout = null);
    IActor Create<TActor>(string name) where TActor : class;
    void Register<TActor>() where TActor : class;
    void ShutDown();
  }
}
