﻿using System.Threading.Tasks;
using MolDev.Common.Messages;

namespace MolDev.Common.Akka.Interfaces
{
  public interface IActor
  {
    Task<TResult> Ask<TResult>(object message, int? timeout = null) where TResult : ResponseMessageBase;

    void Tell(object message, IActor sender = null);

    void Forward(object message);

    string PathStr { get; }
  }
}
