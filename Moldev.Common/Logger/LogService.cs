﻿using NLog;
using System;

namespace MolDev.Common.Logger
{
  public class LogService : ILogService
  {
    private const string MSG_DETAILS_TEMPLATE = "{0}: {1}\nDetails: {2}";
    private const string MSG_CATEGORY_TEMPLATE = "{0}: {1}";
    private ILogger log;
    readonly object locker = new object();
    private readonly string loggerName;

    public LogService(string loggerName)
    {
      this.loggerName = loggerName;
    }

    private string LoggerName
    {
      get { return loggerName; }
    }

    private ILogger Log
    {
      get
      {
        if (log == null)
        {
          lock (locker)
          {
            if (log == null)
            {
#if DEBUG
              LogManager.ThrowExceptions = true;
#endif
              log = LogManager.GetLogger(LoggerName);
            }
          }
        }
        return log;
      }
    }

    public void Debug(LogCategoryEnum category, string message, params object[] formatArgs)
    {
      if (Log.IsDebugEnabled)
      {
        var msg = string.Format(message, formatArgs);
        Log.Debug(string.Format(MSG_CATEGORY_TEMPLATE, category.ToString(), msg));
      }
    }

    public void Debug(LogCategoryEnum category, string message, string details)
    {
      if (Log.IsDebugEnabled)
      {
        Log.Debug(String.Format(MSG_DETAILS_TEMPLATE, category.ToString(), message, details));
      }
    }

    public void Error(LogCategoryEnum category, string message, params object[] formatArgs)
    {
      if (Log.IsErrorEnabled)
      {
        var msg = string.Format(message, formatArgs);
        Log.Error(string.Format(MSG_CATEGORY_TEMPLATE, category.ToString(), msg));
      }
    }

    public void Error(LogCategoryEnum category, string message, Exception exception)
    {
      if (Log.IsErrorEnabled)
      {
        var msg = string.Format(MSG_CATEGORY_TEMPLATE, category.ToString(), message);
        Log.Error(exception, msg, null);
      }
    }

    public void Info(LogCategoryEnum category, string message, params object[] formatArgs)
    {
      if (Log.IsInfoEnabled)
      {
        var msg = string.Format(message, formatArgs);
        Log.Info(string.Format(MSG_CATEGORY_TEMPLATE, category.ToString(), msg));
      }
    }

    public void Warning(LogCategoryEnum category, string message, string details)
    {
      if (Log.IsWarnEnabled)
      {
        Log.Warn(String.Format(MSG_DETAILS_TEMPLATE, category.ToString(), message, details));
      }
    }

    public void Fatal(LogCategoryEnum category, string message, Exception exception)
    {
      if (Log.IsFatalEnabled)
      {
        var msg = string.Format(MSG_CATEGORY_TEMPLATE, category.ToString(), message);
        Log.Fatal(exception, msg);
      }
    }
  }
}
