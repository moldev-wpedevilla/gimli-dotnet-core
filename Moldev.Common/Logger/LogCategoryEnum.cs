﻿namespace MolDev.Common.Logger
{
  public enum LogCategoryEnum
  {
    Uncategorized = 0,
    Security = 10,
    WebApi = 20,
    Repository = 30,
    LogService = 40,
    Akka = 50,
    LabWare = 60,
    Logic = 70,
    Host = 80,
  }
}
