﻿using System;

namespace MolDev.Common.Logger
{
  public interface ILogService
  {
    void Debug(LogCategoryEnum category, string message, params object[] formatArgs);
    void Debug(LogCategoryEnum category, string message, string details);

    void Error(LogCategoryEnum category, string message, params object[] formatArgs);
    void Error(LogCategoryEnum category, string message, Exception exception);

    void Info(LogCategoryEnum category, string message, params object[] formatArgs);
    void Warning(LogCategoryEnum category, string message, string details);
    void Fatal(LogCategoryEnum category, string message, Exception exception);
  }
}
