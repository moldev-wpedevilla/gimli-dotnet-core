﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MolDev.Common.Messages.Errors
{
  [Serializable]
  public class ErrorInfo
  {
    public string Message { get; set; }
    public string Details { get; set; } // StackTrace, etc
    public string Code { get; set; }
    public IEnumerable<MoldevValidationResult> Validation { get; set; }
  }

  //
  // Summary:
  //     Represents a container for the results of a validation request.
  [Serializable]
  public class MoldevValidationResult
  {
    public static readonly MoldevValidationResult Success;

    public MoldevValidationResult(string errorMessage)
    {
      ErrorMessage = errorMessage;
    }
    public MoldevValidationResult(string errorMessage, IEnumerable<string> memberNames)
    {
      ErrorMessage = errorMessage;
      MemberNames = memberNames;
    }
    public MoldevValidationResult(ValidationResult validationResult)
    {
      ErrorMessage = validationResult.ErrorMessage;
      MemberNames = validationResult.MemberNames;
    }

    public string ErrorMessage { get; set; }
    public IEnumerable<string> MemberNames { get; }
  }

  [Serializable]
  public class ValidationMessage
  {
    public ValidationMessage(string message, string memberName)
    {
      Message = message;
      MemberName = memberName;
    }
    public string Message { get; private set; }
    public string MemberName { get; private set; }
  }

}