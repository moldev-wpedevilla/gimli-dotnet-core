﻿namespace MolDev.Common.Messages.Errors
{
  public enum ResultStatusEnum
  {
    Success,
    Error,
    ValidationError,
    InProgress
  }
}
