﻿using MolDev.Common.Messages.Context;
using MolDev.Common.Messages.Errors;
using System;

namespace MolDev.Common.Messages
{
  [Serializable]
  public class ResponseMessageBase : MessageBase
  {
    public ResultStatusEnum Status { get; set; }
    public ErrorInfo Error { get; set; }
    public ResponseMessageBase() : base() { }
    public ResponseMessageBase(IExecutionContext context) : base(context) { }

    public string Key
    {
      get
      {
        if (ExecutionContext != null)
        {
          return ExecutionContext.UserId;
        }
        return null;
      }
    }
  }

  [Serializable]
  public class ResponseMessage<T> : ResponseMessageBase
  {
    public ResponseMessage() { }
    public ResponseMessage(T model, IExecutionContext context = null) : base(context)
    {
      Model = model;
      Status = ResultStatusEnum.Success;
    }
    public ResponseMessage(ResultStatusEnum status, ErrorInfo error)
    {
      Status = status;
      Error = error;
    }
    public ResponseMessage(ResultStatusEnum status, ErrorInfo error, IExecutionContext context) : base(context)
    {
      Status = status;
      Error = error;
    }

    public T Model { get; set; }
  }

  [Serializable]
  public class ImageQualityModel
  {
    public string Format { get; set; }
    public long Quality { get; set; }
  }

  [Serializable]
  public class TypedImageModel
  {
    public ImageQualityModel ImageQuality { get; set; }
    public byte[] Image { get; set; }
  }

  [Serializable]
  public class GetImageResultBase
  {
    public int ImageSizeX { get; set; }
    public int ImageSizeY { get; set; }
    public byte[] Image { get; set; }
    public ImageQualityModel ImageQuality { get; set; }
  }
}
