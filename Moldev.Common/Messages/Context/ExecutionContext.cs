﻿using System;

namespace MolDev.Common.Messages.Context
{
  [Serializable]
  public class ExecutionContext : IExecutionContext
  {
    public ExecutionContext()
    {
      SessionId = Guid.Empty;
      UserName = "Undefined";
      UserId = "Undefined";
    }

    public ExecutionContext(string userName, string userId, Guid sessionId, string language)
    {
      UserName = userName;
      UserId = userId;
      SessionId = sessionId;
      Language = language;
    }

    public string UserName { get; private set; }
    public string UserId { get; private set; }
    public Guid SessionId { get; private set; }
    public string Language { get; private set; }
  }
}
