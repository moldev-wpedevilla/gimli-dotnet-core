﻿using System;

namespace MolDev.Common.Messages.Context
{
  public interface IExecutionContext
  {
    string UserName { get; }
    string UserId { get; }
    Guid SessionId { get; }
    string Language { get; }
  }
}

