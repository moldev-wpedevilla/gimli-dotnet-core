﻿using MolDev.Common.Messages.Context;
using System;

namespace MolDev.Common.Messages
{
  [Serializable]
  public class RequestMessageBase : MessageBase
  {
    public RequestMessageBase() : base() { }
    public RequestMessageBase(IExecutionContext context) : base(context) { }

    public string Key
    {
      get
      {
        if (ExecutionContext != null)
        {
          return ExecutionContext.UserId;
        }
        return null;
      }
    }
  }

  [Serializable]
  public class RequestMessage<T> : RequestMessageBase
  {
    public RequestMessage() : base() { }
    public RequestMessage(T model, IExecutionContext context) : base(context)
    {
      Model = model;
    }

    public T Model { get; set; }
  }
}
