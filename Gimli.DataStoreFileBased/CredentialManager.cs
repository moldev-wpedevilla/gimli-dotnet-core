﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Datasets;
using Gimli.JsonObjects.Utils;

namespace Gimli.DataStoreFileBased
{
    public class CredentialManager
    {
        private List<UserFullExtended> mUsers; 
        public static CredentialManager Generate()
        {
            return new CredentialManager();
        }

        private CredentialManager()
        {
            ReadUserCredentialsFile();
        }

        public void ClearCredentials()
        {
            mUsers = new List<UserFullExtended>();
            WriteUserCredentialsFile();
        }

        public List<UserFullExtended> GetAllUsers
        {
            get
            {
                if (mUsers == null)
                {
                    AddPublicUserToList();
                    AddAdminUserToList();
                    AddImportUserToList();
				}
                mUsers = mUsers.OrderBy(a => a, new CompareUsers()).ToList();

                foreach (var user in mUsers)
                {
                    Trace.WriteLine("User: " + user.UserName);
                }
                return mUsers.Where(u => u.Id != GlobalConstants.ImportGuid).ToList();
            }
        }

        public bool DeleteUser(UserFull user)
        {
            if (user.Id == GlobalConstants.PublicGuid || user.Id == GlobalConstants.AdminGuid) return false;

            var existingUser = GetUserWithId(user.Id);
            if (existingUser == null) return true;

            mUsers.Remove(existingUser);
            WriteUserCredentialsFile();

            return true;
        }

        private void AddPublicUserToList()
        {
            if (!UserExists(GlobalConstants.PublicGuid))
                AddOrUpdateUser(GetPublicUser());
        }

        private void AddAdminUserToList()
        {
            if (!UserExists(GlobalConstants.AdminGuid))
                AddOrUpdateUser(GetAdminUser());
		}

		private void AddImportUserToList()
		{
			if (!UserExists(GlobalConstants.ImportGuid))
				AddOrUpdateUser(GetImportUser());
		}

		private void CheckRenameAdminUser()
        {
            UserFullExtended user = GetUserWithName(GlobalConstants.AdminName);

            if (user != null && user.Id != GlobalConstants.AdminGuid)
            {
                user.UserName = GlobalConstants.AdminRename;
                AddOrUpdateUser(user);
            }
        }

		public static UserFullExtended GetPublicUser()
		{
			return new UserFullExtended
			{
			    UserName = GlobalConstants.PublicName,
                HasNfc = false,
                HasPin = GlobalConstants.PublicHasPin,
                Id = GlobalConstants.PublicGuid
			};
		}

        public static UserFullExtended GetAdminUser()
        {
            return new UserFullExtended
            {
                UserName = GlobalConstants.AdminName,
                HasNfc = false,
                HasPin = GlobalConstants.AdminHasPin,
                Id = GlobalConstants.AdminGuid,
                Pin = GlobalConstants.AdminPin,
                IsDefaultPin = true
            };
        }

		public static UserFullExtended GetImportUser()
		{
			return new UserFullExtended
			{
				UserName = "__ For Import Only __",
				Id = GlobalConstants.ImportGuid
			};
		}

		private void WriteUserCredentialsFile()
        {
            var userDs = new UserDs();
            try
            {
                userDs = EncryptUsers();
                userDs.WriteXml(GlobalConstants.CredentialsFile);
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Saving User Credentials: " + exc.Message);
            }
            finally
            {
                userDs?.Dispose();
            }
        }

        private void ReadUserCredentialsFile()
        {
            var userDs = new UserDs();
            mUsers = new List<UserFullExtended>();
            if (!DataSetHelper.CreateSubFolder(GlobalConstants.CredentialsFolder))
            {
                AddPublicUserToList();
                AddAdminUserToList();
            }
            try
            {
                if (File.Exists(GlobalConstants.CredentialsFile))
                    userDs.ReadXml(GlobalConstants.CredentialsFile);
                mUsers = DecryptUsers(userDs);
            }
            catch (Exception exc)
            {
                Trace.WriteLine(string.Format(CultureInfo.InvariantCulture, "Error addUserToFile: {0}\r\n{1}",
                    GlobalConstants.CredentialsFile, exc.Message));
            }
            finally
            {
                AddPublicUserToList();
                CheckRenameAdminUser();
                AddAdminUserToList();
                userDs.Dispose();
            }
        }

        private UserDs EncryptUsers()
        {
            var userDs = new UserDs();
            MemoryStream ms = null;
            try
            {
                foreach (var user in mUsers)
                {
                    ms = new MemoryStream();
                    var writer = new BinaryWriter(ms);
                    writer.Write(user.Id.ToString());
                    writer.Write(user.NfcId.ToString());
                    writer.Write(user.HasNfc);
                    writer.Write(user.HasPin);
                    writer.Write(user.HasPin ? user.Pin : GlobalConstants.DummyPin);
                    
                    userDs.UserTable.AddUserTableRow(user.UserName, ms.ToArray(), user.Id.ToString() + " / " + user.NfcId, "", DateTime.Now, DateTime.Now);
                }
                userDs.AcceptChanges();
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Error EncryptUsers. " + exc.Message);
            }
            finally
            {
                userDs.Dispose();
                ms?.Dispose();
            }
            return userDs;
        }

        private List<UserFullExtended> DecryptUsers(UserDs userDs)
        {
            return userDs.UserTable.Rows.Cast<UserDs.UserTableRow>().Select(DecryptUser).Where(user => user != null).ToList();
        }

        private UserFullExtended DecryptUser(UserDs.UserTableRow userDs)
        {
            var user = new UserFullExtended() {UserName = userDs.Name};

            try
            {
                var reader = new BinaryReader(new MemoryStream(userDs.Data));
                user.Id = Guid.Parse(reader.ReadString());
                user.NfcId = Guid.Parse(reader.ReadString());
                user.HasNfc = reader.ReadBoolean();
                
                if (!IsEof(reader))
                {
                    user.HasPin = reader.ReadBoolean();
                }
                else
                {
                    user.HasPin = false;
                }

                if (user.HasPin && !IsEof(reader))
                {
                    user.Pin = reader.ReadString();
                }

                if (user.Id == GlobalConstants.AdminGuid && user.Pin == GlobalConstants.AdminPin)
                {
                    user.IsDefaultPin = true;
                }

                Trace.WriteLine(string.Format("User: {0}, Id: {1}, HasNfc: {2} - Nfc: {3}, HasPin: {4}", user.UserName, user.Id, user.HasNfc, user.NfcId, user.HasPin));

                return user;
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Error Decrype User. " + exc.Message);
                return null;
            }
        }

        public bool UserExists(Guid guidId)
        {
            return GetUserWithId(guidId) != null;
        }

        public bool UserExists(string name)
        {
            return GetUserWithName(name) != null;
        }

        public UserFullExtended GetUserWithId(Guid guidId)
        {
            return mUsers.FirstOrDefault(user => user.Id == guidId);
        }

        public UserFullExtended GetUserWithName(string name)
        {
            return mUsers.FirstOrDefault(user => user.UserName == name);
        }

        public void AddOrUpdateUser(UserFullExtended user)
        {
            var found = false;
            foreach (var existingUser in mUsers)
            {
                if (existingUser.Id.Equals(user.Id))
                {
                    //USer found. He is in list. Update name, NFC and other properties, except GuidID
                    existingUser.UserName = user.UserName;
                    existingUser.HasNfc = user.HasNfc;
                    existingUser.NfcId = user.NfcId;
                    existingUser.HasPin = user.HasPin;
                    existingUser.IsDefaultPin = false;

                    if (user.HasPin && user.Pin != null && user.Pin != "undefined")
                    {
                        existingUser.Pin = user.Pin;
                    }

                    if (existingUser.Id == GlobalConstants.AdminGuid)
                    {
                        existingUser.IsDefaultPin = existingUser.Pin == GlobalConstants.AdminPin;
                    }
                    
                    found = true;
                }
            }

            //Add new user to list
            if (!found)
            {
                if (user.Id == Guid.Empty) user.Id = Guid.NewGuid();
                mUsers.Add(user);
            }
            WriteUserCredentialsFile();
        }

        private static bool IsEof(BinaryReader reader)
        {
            return reader.BaseStream.Position >= reader.BaseStream.Length;
        }

        public class CompareUsers : IComparer<UserFull>
        {
            // Because the class implements IComparer, it must define a 
            // Compare method. The method returns a signed integer that indicates 
            // whether s1 > s2 (return is greater than 0), s1 < s2 (return is negative),
            // or s1 equals s2 (return value is 0). This Compare method compares strings. 
            public int Compare(UserFull x, UserFull y)
            {
                if (x.UserName == y.UserName)
                    return 0;

                if (x.UserName == GlobalConstants.AdminName && y.UserName == GlobalConstants.PublicName)
                    return -1;
                if (x.UserName == GlobalConstants.PublicName && y.UserName == GlobalConstants.AdminName)
                    return 1;
                if (x.UserName == GlobalConstants.PublicName || x.UserName == GlobalConstants.AdminName)
                    return -1;
                if (y.UserName == GlobalConstants.PublicName || y.UserName == GlobalConstants.AdminName)
                    return 1;
                

                return string.Compare(x.UserName, y.UserName, true, CultureInfo.CurrentCulture);
            }
        }
    }
}
