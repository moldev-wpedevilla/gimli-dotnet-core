﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Datasets;
using Gimli.JsonObjects.Utils;

namespace Gimli.DataStoreFileBased
{
    public static class Utils
    {
        /*internal*/ public static bool SaveToXml(DocumentDs dataSet, out string fileName)
        {
	        if (dataSet == null)
	        {
				fileName = string.Empty;
				return false;
	        }
            var userName = GetUserId(dataSet);
	        if (!DataSetHelper.CreateSubFolder(userName))
	        {
		        fileName = string.Empty;
				return false;
	        }
            var guidName = DataSetHelper.GetGuidAsName(dataSet);

            fileName = $@"{DataSetHelper.RootPath}{userName}{Path.DirectorySeparatorChar}{guidName}{".xml"}";
            DataSetHelper.SetFileNameToDatSet(dataSet, fileName);

            return DataSetHelper.WriteXmlToDataSet(dataSet, fileName);
        }

        internal static bool DeleteFile(string userName, string fileName)
        {
            if (fileName.Length == 0) return false;
            if (!DataSetHelper.CreateSubFolder(userName)) return false;

            var pathAndFileName = File.Exists(fileName) ? fileName : $@"{DataSetHelper.RootPath}{userName}{Path.DirectorySeparatorChar}{fileName}";

            try
            {
                if (File.Exists(pathAndFileName))
                {
                    File.Delete(pathAndFileName);
                    return true;
                }
            }
            catch (Exception exc)
            {
                Trace.WriteLine($"Error deleting protocol: {pathAndFileName}\r\n{exc.Message}");
            }
            return false;
        }

        internal static bool SaveToFile(string userName, string protocolName, string data)
        {
            if (String.IsNullOrEmpty(data)) return false;
            if (!DataSetHelper.CreateSubFolder(userName)) return false;

            var start = DateTime.Now;
            var fileName = $@"{DataSetHelper.RootPath}{userName}{Path.DirectorySeparatorChar}{protocolName}";
            StreamWriter file = null;
            try
            {
                file = File.CreateText(fileName);
                file.Write(data);
                return true;
            }
            catch (Exception exc)
            {
                Trace.WriteLine($"Error saving protocol: {fileName}\r\n{exc.Message}");
            }
            finally
            {
                file?.Close();
                Trace.WriteLine($"SaveToFile done.        - Time: {DateTime.Now.Subtract(start).TotalMilliseconds.ToString("N0")} ms");
            }
            return false;
        }

        internal static DocumentDs LoadFromXml(string userName, string fileName)
        {
            if (fileName.Length == 0) return null;
            if (!DataSetHelper.CreateSubFolder(userName)) return null;

            var fullFileName = $@"{DataSetHelper.RootPath}{userName}{Path.DirectorySeparatorChar}{fileName}";
            if (fileName.ToLower().Contains(DataSetHelper.RootPath.ToLower()))
                fullFileName = fileName;
            try
            {
                var ds = new DocumentDs();
                ds.ReadXml(fullFileName);
                return ds;
            }
            catch (Exception exc)
            {
                Trace.WriteLine($"Error loading xml file: {fileName}\r\n{exc.Message}");
            }
            return null;
        }

        internal static string LoadFromFile(string userName, string fileName)
        {
            if (fileName.Length == 0) return String.Empty;
            if (!DataSetHelper.CreateSubFolder(userName)) return String.Empty;

            StreamReader file = null;
            var fullFileName = $@"{DataSetHelper.RootPath}{userName}{Path.DirectorySeparatorChar}{fileName}";
            if (fileName.ToLower().Contains(DataSetHelper.RootPath.ToLower()))
                fullFileName = fileName;
            try
            {
                file = File.OpenText(fullFileName);
                return file.ReadToEnd();
            }
            catch (Exception exc)
            {
                Trace.WriteLine($"Error loading protocol: {fileName}\r\n{exc.Message}");
            }
            finally
            {
                file?.Close();
            }
            return string.Empty;
        }

        internal static string GetTimeAsUtcString(this DateTime dateTime)
        {
            var dateOffset = new DateTimeOffset(dateTime, TimeZoneInfo.Local.GetUtcOffset(dateTime));
            return dateOffset.ToString("o", CultureInfo.InvariantCulture);
        }

        public static string GetUserId(DocumentDs dataSet)
        {
            foreach (DocumentDs.DocumentGeneralRow row in dataSet.DocumentGeneral.Rows)
            {
                return row.UserId;
            }
            return GlobalConstants.PublicGuidString;
        }
    }
}
