﻿using Gimli.JsonObjects.Interfaces;

namespace Gimli.DataStoreFileBased
{
	public class DataStoreFactory : IDataStoreFactory
	{
		public IDataStore Generate()
		{
			return DataStore.Generate();
		}
	}
}
