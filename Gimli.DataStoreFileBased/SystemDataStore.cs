﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using Gimli.JsonObjects.Interfaces;
using Gimli.JsonObjects.Utils;

namespace Gimli.DataStoreFileBased
{
    public class SystemDataStore : ISystemDataStore
    {
        private const string ConfigFolder = "_Config_";
        private const string FirmwareFolder = "_Firmware_";
        private const string SkinsFolder = "_Skins_";
        //private string mFiltersFolder = "_Filters_";

        public static ISystemDataStore Generate()
        {
            return new SystemDataStore();
        }

        #region ISystemDataStore
        public bool SaveFirmware(string typeOfFirmware, string data)
        {
            return Utils.SaveToFile(FirmwareFolder, typeOfFirmware, data);
        }

        public string LoadFirmware(string typeOfFirmware)
        {
            return Utils.LoadFromFile(FirmwareFolder, typeOfFirmware);
        }

        public ICollection<object> GetLogFiles()
        {
            throw new NotImplementedException();
        }

        public bool SetFilterInfo(string data)
        {
            return false;
        }

        public string GetSkinFile(int type, int version)
        {
            return GetSkinFromFile(type, version);
        }

        public bool SaveSkinFile(string data)
        {
            var skinName = GetNameTypeAndVersionFromData(data);
            return !string.IsNullOrEmpty(skinName) && Utils.SaveToFile(SkinsFolder, skinName, data);
        }

        public bool SaveConfigFile(string fileName, string data)
        {
            return !string.IsNullOrEmpty(fileName) && Utils.SaveToFile(ConfigFolder, fileName, data);
        }

        public string LoadConfigFile(string fileName)
        {
            return Utils.LoadFromFile(ConfigFolder, fileName);
        }

        public int GetFreeSpace()
        {
            return (from d in DriveInfo.GetDrives() where d.RootDirectory.Root.Name.Contains("C:") select (int)(d.AvailableFreeSpace / 1000000)).FirstOrDefault();
        }
        #endregion
        #region Implementation
        private string GetSkinFromFile(int type, int version)
        {
            var di = new DirectoryInfo(DataSetHelper.RootPath + SkinsFolder);
            var files = di.GetFiles("*.cartridge");
            var search = $"_{type}_{version}.";

            foreach (var fileInfo in files)
            {
                if (!fileInfo.Name.Contains(search)) continue;

                StreamReader file = null;
                try
                {
                    file = fileInfo.OpenText();
                    return file.ReadToEnd();
                }
                catch (Exception exc)
                {
                    Trace.WriteLine($"Error loading Skinfile: {fileInfo.FullName}\r\n{exc.Message}");
                }
                finally
                {
                    file?.Close();
                }
                return string.Empty;
            }
            return string.Empty;
        }

        private string GetNameTypeAndVersionFromXml(DataSet ds, out int type, out int version, out string name)
        {
            type = 0;
            version = 0;
            name = string.Empty;

            foreach (DataTable table in ds.Tables)
            {
                if (table.TableName != "Cartridge") continue;
                foreach (DataRow row in table.Rows)
                {
                    int.TryParse(row["Type"].ToString(), out type);
                    int.TryParse(row["Version"].ToString(), out version);
                    name = row["Name"].ToString();
                    return $"{row["Name"]}_{row["Type"]}_{row["Version"]}.cartridge";
                }
            }
            return string.Empty;
        }

        private string GetNameTypeAndVersionFromData(string data)
        {
            if (string.IsNullOrEmpty(data)) return string.Empty;
            var ds = new DataSet();
            ds.ReadXml(new XmlTextReader(new StringReader(data)));

            int type;
            int version;
            string name;
            return GetNameTypeAndVersionFromXml(ds, out type, out version, out name);
        }
        #endregion
    }
}
