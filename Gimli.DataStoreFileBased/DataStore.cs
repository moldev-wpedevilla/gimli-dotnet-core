﻿using Gimli.JsonObjects;
using Gimli.JsonObjects.DummyData;
using Gimli.JsonObjects.Enums;
using Gimli.JsonObjects.Interfaces;
using Gimli.JsonObjects.Load;
using Gimli.JsonObjects.Save;
using Gimli.JsonObjects.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using Gimli.JsonObjects.Datasets;

namespace Gimli.DataStoreFileBased
{
    public class DataStore : IDataStore
    {
		private string mListFileName = "ListOfItems.xml";
		private Document mCachedItem;
        private CredentialManager mCredential;
        private List<DocumentCategory> mStandardCategories;
        public static IDataStore Generate()
        {
            return new DataStore();
        }
        private DataStore()
        {
            mCredential = CredentialManager.Generate();
			var userDir = Path.Combine(DataSetHelper.RootPath, GlobalConstants.ImportGuid.ToString());
			if (Directory.Exists(userDir)) Directory.Delete(userDir, true);
			FillStandardCategories();
        }

        #region IDataStore

        public ICollection<ProtocolListItem> GetListOfItems(UserFull user, ItemType protocolType, bool includePublic)
        {
            return LoadProtocolsList(user, protocolType, includePublic);
        }

        public Document GetItem(ProtocolListItem item)
        {
            GimliTrace.Start("GetItemTotal");
            GimliTrace.Start("GetItem");
            if (item == null) return null;
            var fromDisk = false;
            if (mCachedItem == null || mCachedItem.Id != item.Id)
            {
                var documentStream = Utils.LoadFromXml(item.UserNameGuid, item.ProtocolName);
                GimliTrace.ElapsedRestart("GetItem", "Reading XML done");
                mCachedItem = LoadDocumentFromDataSet.Load(documentStream);
                GimliTrace.ElapsedRestart("GetItem", "Creating Document object done");
                fromDisk = true;
            }
            GimliTrace.Start("GetItemClone");
            var clone = mCachedItem.Clone();
            GimliTrace.ElapsedTime("GetItemClone", "GetItem Clone end");
            GimliTrace.Stop("GetItemTotal", $"GetItem {(fromDisk ? item.DisplayName : "")}");

            //if (item.ProtocolName.Contains("083d856d-dab1-42af-a3ff-0d43fed8e01e"))
            //{
            //    item.UserNameGuid = "083d856d-dab1-42af-a3ff-0d43fed8e01e";
            //    SaveItem(mChachedItem);
            //}
            return clone;
        }

        public bool SaveItem(Document document)
        {
            GimliTrace.Start("SaveItemTotal");
            GimliTrace.Start("SaveItem");
            if (string.IsNullOrEmpty(document.UserNameGuid)) document.UserNameGuid = GlobalConstants.PublicGuidString;
            
            mCachedItem = document;
            mCachedItem.LastUsedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);  //for lastused list update timestamp

            var item = SaveDocumentToDataSet.Save(mCachedItem);
            GimliTrace.ElapsedRestart("SaveItem", "Document to DataSet done");
	        string filename;
            var data = Utils.SaveToXml(item, out filename);
	        mCachedItem.ProtocolName = filename;
			GimliTrace.ElapsedRestart("SaveItem", "Save to XML done");

            SynchronizeList(item, mCachedItem.UserNameGuid, false);
            GimliTrace.ElapsedRestart("SaveItem", "Synchronize List done");
            GimliTrace.ElapsedRestart("SaveItemTotal", "SaveItem " + document.Name);
            return data;
        }

        internal void DeleteItemFromList(string protocolName, string userName)
        {
            var fileName = $@"{DataSetHelper.RootPath}{userName}{Path.DirectorySeparatorChar}{mListFileName}";
            var ds = ReadFileAndGetDataSet(new FileInfo(fileName));
            if (ds == null)
            {
                CreateListFile(userName, true);
                return;
            }

            try
            {
                foreach (DocumentDs.DocumentGeneralRow listItem in ds.DocumentGeneral.Rows)
                {
                    if (protocolName.Contains(listItem.Id.ToString()))
                    {
                        ds.DocumentGeneral.Rows.Remove(listItem);
                        ds.AcceptChanges();
                        ds.WriteXml(fileName);
                        return;
                    }
                }
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Error deleting item from List: " + exc.Message);
            }
            finally
            {
                ds.Dispose();
            }
        }

        private DocumentDs LoadListFile(string userName, bool suppressRewriteList)
        {
            var fileName = $@"{DataSetHelper.RootPath}{userName}{Path.DirectorySeparatorChar}{mListFileName}";
            var ds = ReadFileAndGetDataSet(new FileInfo(fileName));
            if (ds == null)
                return CreateListFile(userName, suppressRewriteList);

            RemoveNonExistingEntriesFromList(ds);
            return ds;
        }

        internal static void RemoveNonExistingEntriesFromList(DocumentDs ds)
        {
            try
            {
                for (var i = ds.DocumentGeneral.Rows.Count; i > 0; i--)
                {
                    if (!File.Exists(ds.DocumentGeneral.Rows[i - 1][1].ToString()))
                        ds.DocumentGeneral.Rows.RemoveAt(i - 1);
                }
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Error deleting FileList etries: " + exc.Message);
            }
        }

        private void SynchronizeList(DocumentDs item, string userName, bool suppressRewriteList)
        {
            var fileName = $@"{DataSetHelper.RootPath}{userName}{Path.DirectorySeparatorChar}{mListFileName}";
            var ds = LoadListFile(userName, suppressRewriteList);

            try
            {
                foreach (DocumentDs.DocumentGeneralRow row in item.DocumentGeneral.Rows)
                {
                    foreach (DocumentDs.DocumentGeneralRow listItem in ds.DocumentGeneral.Rows)
                    {
                        if (listItem.Id != row.Id) continue;
                        listItem.DisplayName = row.DisplayName;
                        listItem.HasData = row.HasData;
                        listItem.IsSynchronized = row.IsSynchronized;
                        listItem.Notes = row.Notes;
                        listItem.NumberOfPlates = row.NumberOfPlates;
                        listItem.ProtoclName = row.ProtoclName;
                        listItem.PercentMeasured = row.PercentMeasured;
                        listItem.Timestamp = row.Timestamp;
                        listItem.UserId = row.UserId;
                        listItem.IsProtected = row.IsProtected;
                        listItem.LastUsedDate = row.LastUsedDate;
                        listItem.IsPinned = !row.IsIsPinnedNull() && row.IsPinned;
                        listItem.PinSlot = row.IsPinSlotNull() ? -1 : row.PinSlot;
                        listItem.PinName = row.IsPinNameNull() ? null : row.PinName;
						listItem.PinDate = row.IsPinDateNull() ? 0 : row.PinDate;
                        listItem.Category = row.IsCategoryNull() ? "" : row.Category;
                        ds.AcceptChanges();
                        ds.WriteXml(fileName);
                        return;
                    }
                }

                AddItemToList(item, ds, fileName);
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Error reading FileList: " + exc.Message);
            }
            finally
            {
                ds.Dispose();
            }
        }

        private void AddItemToList(DocumentDs item, DocumentDs ds, string fileName)
        {
//not found, add it
            foreach (DocumentDs.DocumentGeneralRow row in item.DocumentGeneral.Rows)
            {
                AddItToList(row, ds);
            }

            ds.AcceptChanges();
            ds.WriteXml(fileName);
        }


        internal void AddItToList(ProtocolListItem item, DocumentDs list)
        {
            list.DocumentGeneral.AddDocumentGeneralRow(item.Id, item.ProtocolName, item.DisplayName, item.Timestamp,
                    item.IsSynchronized, item.HasData, item.NumberOfPlates, item.PercentMeasured, item.UserNameGuid,
                    item.Notes, "1.0", item.IsProtected, item.LastUsedDate.Ticks, item.IsPinned, item.Category, item.PinDate.Ticks, item.PinSlot, item.PinName, item.Categories);
        }

        private void AddItToList(DocumentDs.DocumentGeneralRow item, DocumentDs list)
        {
            list.DocumentGeneral.AddDocumentGeneralRow(item.Id, item.ProtoclName, item.DisplayName, item.Timestamp,
                    item.IsSynchronized, item.HasData, item.NumberOfPlates, item.PercentMeasured, item.UserId,
                    item.Notes, "1.0", item.IsProtected, item.LastUsedDate, item.IsPinned, item.Category, item.PinDate, item.PinSlot, item.PinName, item.Categories);
        }

        private const string MFileExtension = ".xml";
        public bool DeleteItem(string userName, string protocolName)
        {
            if (!protocolName.ToLower().EndsWith(MFileExtension))
            {
                var item = FindFileWithGuid(protocolName);
                if (item == null)
                    protocolName += MFileExtension;
                else
                {
                    protocolName = item.ProtocolName;
                    userName = item.UserNameGuid;
                    Trace.WriteLine("Delete Item with Guid: " + protocolName);
                }
            }
            var ret = Utils.DeleteFile(userName, protocolName);
            DeleteItemFromList(protocolName, userName);
            mCachedItem = null;

            return ret;
        }

        private ProtocolListItem FindFileWithGuid(string protocolName)
        {
            var di = new DirectoryInfo(DataSetHelper.RootPath);
            var files = di.GetFiles(mListFileName,SearchOption.AllDirectories);

            foreach (var file in files)
            {
                var ds = ReadFileAndGetDataSet(file);
                if (ds == null) continue;

                foreach (DocumentDs.DocumentGeneralRow row in ds.DocumentGeneral.Rows)
                {
                    if (row.Id.ToString() == protocolName)
                        return CreateItem(row);
                }
            }
            return null;
        }

        public int GetFreeSpace()
        {
            return FolderFileHelper.GetFreeSpace("C:");
        }

        public bool DeleteUser(UserFull user)
        {
            if (mCredential.DeleteUser(user))
                return DeleteAll(user.Id.ToString());
            return false;
        }

        public bool DeleteFolder(string folderName)
        {
            return DeleteAll(folderName);
        }

        public bool SaveUserCredentials(UserFull user, string pin = null)
        {
            var userFe = new UserFullExtended(user);
            userFe.Pin = pin;

            mCredential.AddOrUpdateUser(userFe);
            return true;
        }

        public List<UserFull> GetAllUserCredentials()
        {
            return mCredential.GetAllUsers.Cast<UserFull>().ToList();
        }

        public bool CheckPin(Guid userId, string pin)
        {
            var userFe = mCredential.GetUserWithId(userId);

            return !userFe.HasPin || (userFe.HasPin && userFe.Pin == pin);
        }

        public List<DocumentCategory> GetListOfCategories(Guid userId)
        {
            return mStandardCategories ?? new List<DocumentCategory>();
        }
        #endregion

        #region Implementation

        /// <summary>
        /// Get a list of items depending on user name and protocoltype
        /// </summary>
        /// <param name="user"></param>
        /// <param name="protocolType">0 = Standard Protocols, 1= Protocols, 2 = Result, 3 = List of LastUsedProtocols</param>
        /// <param name="includePublic"></param>
        /// <returns>List of ListItems</returns>
        private ICollection<ProtocolListItem> LoadProtocolsList(UserFull user, ItemType protocolType, bool includePublic)
        {
            var userNameGuid = GlobalConstants.PublicGuidString;
            if (user != null) userNameGuid = user.Id.ToString();
            if (!DataSetHelper.CreateSubFolder(userNameGuid)) return null;

            GimliTrace.Start("LoadProtocolList");

            var collection = new List<ProtocolListItem>();
            collection.AddRange(LoadItemsListFromLocation(userNameGuid, protocolType));

            if (userNameGuid != GlobalConstants.PublicGuidString && (includePublic))// || protocolType.HasFlag(ItemType.LastUpdated)))
                AddCollectionRange(collection, LoadItemsListFromLocation(GlobalConstants.PublicGuidString, protocolType));

            if (protocolType.HasFlag(ItemType.Standard))
                AddCollectionRange(collection, LoadItemsListFromLocation(GlobalConstants.StandardGuidString, protocolType));

            if (protocolType.HasFlag(ItemType.LastUpdated))
                collection = RemoveAllItemsWithoutTimeStamp(collection);

			if (protocolType == ItemType.Quick)
				collection = RemoveAllItemsNotPinned(collection);

            GimliTrace.Stop("LoadProtocolList", "LoadProtocolList done.FB");
            return collection;
        }

        internal void AddCollectionRange(ICollection<ProtocolListItem> collection, ICollection<ProtocolListItem> toAdd)
        {
            foreach (var item in toAdd.Where(item => collection.All(collItem => collItem.Id != item.Id)))
            {
                collection.Add(item);
            }
        }

        internal List<ProtocolListItem> RemoveAllItemsWithoutTimeStamp(List<ProtocolListItem> collection)
        {
			return collection.Where(item => item.LastUsedDate.Ticks > 0).OrderByDescending(d => d.LastUsedDate).ToList();
		}

        internal List<ProtocolListItem> RemoveAllItemsNotPinned(List<ProtocolListItem> collection)
        {
            return collection.Where(item => item.IsPinned).OrderByDescending(d => d.PinDate).ToList();
		}

		private ICollection<ProtocolListItem> LoadItemsListFromLocation(string userName, ItemType protocolType)
        {
            DataSetHelper.CreateSubFolder(userName);
            var collection = new List<ProtocolListItem>();
            var di = new DirectoryInfo(DataSetHelper.RootPath + (userName ?? ""));
            var files = di.GetFiles(mListFileName, SearchOption.AllDirectories);

            if (files.Length == 0)
            {
                CreateListFile(userName, true);
                files = di.GetFiles(mListFileName, SearchOption.AllDirectories);
            }

            foreach (var file in files)
            {
                if (file.Length < 500)
                    CreateListFile(userName, true);
                
                var item = GetList(file, protocolType, userName, false);
                if (item != null)
                    collection.AddRange(item);
            }

            return collection;
        }

        private DocumentDs CreateListFile(string userName, bool suppressRewriteList, bool dispose = false)
        {
            var list = LoadProtocolFromLocation(userName, suppressRewriteList);
            var ds = new DocumentDs();
            try
            {
                foreach (var item in list)
                {
                    AddItToList(item, ds);
                }
                ds.AcceptChanges();

                var fileName = $@"{DataSetHelper.RootPath}{userName}\{mListFileName}";
                ds.WriteXml(fileName);
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Exception CreateListFile: " + exc.Message);
            }
            finally
            {
                if (dispose)
                    ds.Dispose();
            }
            return ds;
        }

        private ICollection<ProtocolListItem> GetList(FileSystemInfo file, ItemType protocolType, string userName, bool suppressRewriteList)
        {
            var dataSet = new DocumentDs();
            var list = new List<ProtocolListItem>();

	        try
	        {
			        dataSet = ReadFileAndGetDataSet(file);

		        foreach (DocumentDs.DocumentGeneralRow row in dataSet.DocumentGeneral.Rows)
		        {
			        if ((row.Category.Contains(ItemType.Standard.ToString()) && protocolType.HasFlag(ItemType.Standard)) ||
			            (row.Category.Contains(ItemType.Protocol.ToString()) && protocolType.HasFlag(ItemType.Protocol)) ||
			            (row.Category.Contains(ItemType.Protocol.ToString()) && protocolType.HasFlag(ItemType.LastUpdated)) ||
			            (row.Category.Contains(ItemType.Result.ToString()) && protocolType.HasFlag(ItemType.Result)) ||
			            (row.Category.Contains(ItemType.Result.ToString()) && protocolType.HasFlag(ItemType.LastUpdated)) ||
			            (row.Category.Contains(ItemType.Protocol.ToString()) && protocolType.HasFlag(ItemType.Quick)))
				        list.Add(CreateItem(row));
		        }
		        return list;
	        }
	        catch (NullReferenceException nullexc)
	        {
				Trace.WriteLine($"Null reference exception reading list XML-file ({nullexc}).");
		        if (!suppressRewriteList)
		        {
			        Trace.WriteLine("Creating a new list file");
			        SynchronizeList(dataSet, userName, true);
			        return GetList(file, protocolType, userName, true);
		        }
	        }
			catch (Exception exc)
			{
				Trace.WriteLine("Error reading Xml-File: " + exc.Message);
			}
			finally
            {
                dataSet?.Dispose();
            }
            return null;
        }

		private static ProtocolListItem CreateItem(DocumentDs.DocumentGeneralRow row)
        {
            return new ProtocolListItem(
                row.Id,
                row.UserId,
                row.ProtoclName,
                row.DisplayName,
                row.Timestamp,
                row.HasData,
                row.IsSynchronized)
            {
                Notes = row.Notes,
                NumberOfPlates = row.NumberOfPlates,
                PercentMeasured = row.PercentMeasured,
                IsProtected = row.IsProtected,
                LastUsedDate = new DateTime(row.LastUsedDate),
                IsPinned = !row.IsIsPinnedNull() && row.IsPinned,
				PinSlot = row.IsPinSlotNull() ? -1 : row.PinSlot,
				PinName = row.IsPinNameNull() ? null : row.PinName,
				PinDate = new DateTime(row.IsPinDateNull() ? 0 : row.PinDate),
                Category = row.IsCategoryNull() ? "" : row.Category,
                Categories = row.IsCategoriesNull() ? "" : row.Categories
            };
        }

        private DocumentDs ReadFileAndGetDataSet(FileSystemInfo file)
        {
            var dataSet = new DocumentDs();
            try
            {
                dataSet.ReadXml(file.FullName);
                return dataSet;
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Error reading Xml-File: " + exc.Message);
            }
            finally
            {
                dataSet.Dispose();
            }
            return null;
        }

        private ICollection<ProtocolListItem> LoadProtocolFromLocation(string userName, bool suppressRewriteList)
        {
            DataSetHelper.CreateSubFolder(userName);
            var collection = new List<ProtocolListItem>();
            var di = new DirectoryInfo(DataSetHelper.RootPath + userName);
            var files = di.GetFiles("*.xml");

            foreach (var file in files)
            {
                if (file.Name.Contains(mListFileName)) continue;

                var items = GetList(file, (ItemType)65535, userName, suppressRewriteList);
                if (items != null)
                    collection.AddRange(items);
            }

            return collection;
        }

        internal bool DeleteAll(string userName)
        {
            var path = string.Format(CultureInfo.InvariantCulture, @"{0}{1}", DataSetHelper.RootPath, userName);
            if (!Directory.Exists(path))
                return true;
            try
            {
                Directory.Delete(path, true);
                return true;
            }
            catch (Exception exc)
            {
                Trace.WriteLine(string.Format(CultureInfo.InvariantCulture, "Error deleting folder: {0}\r\n{1}", path, exc.Message));
            }
            return false;
        }

        private DocumentCategory InitDocumentCategories()
        {
            mStandardCategories = new List<DocumentCategory>();
            
            var documentCategoryAll =  new DocumentCategory
            {
                Id = 0,
                Label = "All documents",
                SubLabel = string.Empty,
                Filter = GlobalConstants.CategoriesAllDocuments,
                GroupFlag = CategoryGroup.General,
                Count = 0
            };
            
            mStandardCategories.Add(documentCategoryAll);

            return documentCategoryAll;
        }

        private void FillStandardCategories()
        {
            var documentCategoryAll = InitDocumentCategories();
            var standardProtocols = LoadProtocolsList(null, ItemType.Standard, true);

            //Only for standard protocols, might be required to be extended for later use with more/all protocol types
            foreach (var standardProtocol in standardProtocols.Where(i => i.Categories != null))
            {
                var currentCategories = standardProtocol.Categories.Split('|');
                foreach (var cat in currentCategories.Where(i => i != string.Empty))
                {
                    var actCategory = mStandardCategories.Find(i => i.Label == cat);

                    if (actCategory != null)
                    {
                        actCategory.Count++;
                        documentCategoryAll.Count++;
                    }
                    else
                    {
                        mStandardCategories.Add(new DocumentCategory
                        {
                            Id = mStandardCategories.Count + 1,
                            Label = cat,
                            SubLabel = cat,
                            Filter = cat,
                            GroupFlag = CategoryGroup.Category,
                            Count = 1
                        });
                    }
                }
            }
        }
        #endregion
    }
}
