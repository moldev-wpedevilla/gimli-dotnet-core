﻿using System;
using Gimli.JsonObjects;

namespace Gimli.DataStoreFileBased
{
    [Serializable]
    public class UserFullExtended : UserFull
    {
        public string Pin { get; set; }

        public UserFullExtended()
        {
            
        }

        public UserFullExtended(UserFull userFull)
        {
            Id = userFull.Id;
            UserName = userFull.UserName;
            HasPin = userFull.HasPin;
            HasNfc = userFull.HasNfc;
            IsDefaultPin = userFull.IsDefaultPin;
            NfcId = userFull.NfcId;
        }
    }
}
