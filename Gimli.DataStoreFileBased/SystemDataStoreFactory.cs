﻿using Gimli.JsonObjects.Interfaces;

namespace Gimli.DataStoreFileBased
{
	public class SystemDataStoreFactory : ISystemDataStoreFactory
	{
		public ISystemDataStore Generate()
		{
			return SystemDataStore.Generate();
		}
	}
}
