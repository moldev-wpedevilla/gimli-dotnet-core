﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class DocumentsForImport
	{
		public int NumberOfResults { get; set; }
		public int NumberOfProtocols { get; set; }
	}
}
