﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class DataChangedInfo
	{
		public Guid DocumentId { get; set; }
		public Guid ExperimentId { get; set; }
		public Guid PlateId { get; set; }
		public PlateInRead[] PlatesInRead { get; set; }
		public Point[] Wells { get; set; }
		public bool IsPausable { get; set; }
		public int TotalPercentDone { get; set; }
		public int TimeLeft { get; set; }
		public MeasurementStatus Status { get; set; }
		public string StatusMessage { get; set; }
		[Serializable]
		public class PlateInRead
		{
			public Guid Id { get; set; }
			public int PercentDone { get; set; }
		}
	}
}