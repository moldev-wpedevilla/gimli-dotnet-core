﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class TemperatureNotification
	{
		public double Current { get; set; }
		public bool IsControllerRunning { get; set; }
	}
}
