﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Gimli.JsonObjects
{
    [Serializable]
    public abstract class ValidationBase
    {
        [NonSerialized]
        protected bool mIsValid = true;
		protected List<string> mErrorMessage = new List<string>();
		protected List<string> mWarningMessage = new List<string>();
        public virtual bool IsValid => mIsValid;
        public List<string> ErrorMessage => mErrorMessage;

	    public virtual bool IsInWarning
	    {
		    get
		    {
				if (mWarningMessage == null) mWarningMessage = new List<string>();
			    return mWarningMessage.Any();
		    }
	    }
		public List<string> WarningMessage => mWarningMessage;
		public bool IsModified { get; set; }
        public bool IsDisplayedInSummary { get; set; } = true;
        public string Name { get; set; }
        public string Description { get; set; }


    }
}
