﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class NfcPairingResult
	{
		public UserFull User { get; set; }
		public string ErrorMessage { get; set; }
	}
}
