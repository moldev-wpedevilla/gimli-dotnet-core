﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class SystemConfiguration
	{
		public ListValueWithLegalValues<string> DateFormat { get; set; }
		public ListValueWithLegalValues<string> TimeFormat { get; set; }
		public DoubleValueWithRange Brightness { get; set; }
		public DoubleValueWithRange Volume { get; set; }
	}
}
