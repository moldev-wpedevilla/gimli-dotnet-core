using Gimli.JsonObjects.Enumarations.Status;
using System;
using System.IO;
using System.Linq;
using Gimli.JsonObjects.Deserialize;
using Gimli.JsonObjects.Serialize;
using Gimli.JsonObjects.Settings;

namespace Gimli.JsonObjects
{
    [Serializable]
	public class Well
    {
        //for Serialization only
        public Well() { }

        public Well(Well originalWell)
        {
            if (originalWell == null) return;

            WellName = originalWell.WellName;
            XPos = originalWell.XPos;
            YPos = originalWell.YPos;
            mYAxisMinValue = originalWell.mYAxisMinValue;
	        ReductionValue = originalWell.ReductionValue;
	        ReductionState = originalWell.ReductionState;
            if (originalWell.Points != null)
                Points = (double[,,])originalWell.Points.Clone();
            if (originalWell.DataPointsState != null)
                DataPointsState = (DataPointStatus[,,])originalWell.DataPointsState.Clone();
            if (originalWell.CycleTime != null)
                CycleTime = (double[])originalWell.CycleTime.Clone();

            UpdateMinMeanMax();
            UpdateOverallStatus();
        }
        public Well(string wellName, int horizontalPosition, int verticalPosition, double[,,] dataPoints, DataPointStatus[,,] dataPointsState, bool setMinToZero = false)
		{
			WellName = wellName;
			XPos = horizontalPosition;
			YPos = verticalPosition;
			Points = dataPoints;
		    DataPointsState = dataPointsState;
            mYAxisMinValue = setMinToZero;

            UpdateMinMeanMax();
            UpdateOverallStatus();
		}

        private bool mYAxisMinValue = false;
        public void SetYAxisMinValue(bool useZero)
        {
            mYAxisMinValue = useZero;
        }
		public double[,] Mean { get; set; }
		public double[,] Min { get; set; }
		public double[,] Max { get; set; }
		public double MinValue
        {
            get
            {
                if (mYAxisMinValue)
                    return 0;

                var wlMin = MathUtil.CalculateMin(Min);
                var value = wlMin?.Min() ?? 0;
	            return MathUtil.RoundDownToDecimals(value, 3);
            }
        }
        public double MaxValue
        {
            get
            {
                var wlMax = MathUtil.CalculateMax(Max);
				var value = wlMax?.Max() ?? 0;
	            return MathUtil.RoundUpToDecimals(value, 3);
            }
		}

        internal Well Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as Well;
        }

        public double MeanValue
        {
            get
            {
                var wlMean = MathUtil.CalculateMean(Mean);
                return wlMean?.Average() ?? 0;
            }
        }
        public DataPointStatus OverallStatus { get; set; }
        public DataPointStatus[] ListOfFoundStatus { get; set; }
        public string WellName { get; set; }
		public int XPos { get; set; }
		public int YPos { get; set; }

        public void AddValue(int dimension1, int dimension2, int dimension3, double value, DataPointStatus status, double cycleTime = 0.0)
        {
            if (Points == null) Points = new double[dimension1, dimension2, dimension3];

            var recreate = dimension1 > Points.GetLength(0);
            if (dimension2 > Points.GetLength(1)) recreate = true;
            if (dimension3 > Points.GetLength(2)) recreate = true;
            if (CycleTime.GetLength(0) < dimension3) recreate = true;
            if (recreate)
            {
                var newPoints = new double[dimension1, dimension2, dimension3];
                var newStatus = new DataPointStatus[dimension1, dimension2, dimension3];
                var newCycleTime = new double[dimension3];
                Array.Copy(Points, newPoints, Points.GetLength(0) * Points.GetLength(1)* Points.GetLength(2));
                Array.Copy(DataPointsState, newStatus, DataPointsState.GetLength(0) * DataPointsState.GetLength(1) * DataPointsState.GetLength(2));
                Array.Copy(CycleTime, newCycleTime, CycleTime.GetLength(0));
                Points = newPoints;
                DataPointsState = newStatus;
                CycleTime = newCycleTime;

                if (WellDataDimensionChanged != null)
                    WellDataDimensionChanged(this, new EventArgs());
            }
            Points[dimension1, dimension2, dimension3] = value;
            DataPointsState[dimension1, dimension2, dimension3] = status;
            CycleTime[dimension3] = cycleTime;

            UpdateOverallStatus();
            UpdateMinMeanMax();

            if (WellDataPointChanged != null)
                WellDataPointChanged(this, new EventArgs());
        }

	    public double[] CycleTime { get; set; }
        /// <summary>
        /// Points[row, column]
        /// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Member")]
        public double[,,] Points { get; set; }
        public double ReductionValue { get; set; }
        public DataPointStatus ReductionState { get; set; }
		/// <summary>
		/// DataPointsState[row, column]
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Member")]
        public DataPointStatus[,,] DataPointsState { get; set; }

		/// <summary>
		/// Seperate method necessary because Points should be nullable while Mean still has to stay the same.
		/// </summary>
	    public void UpdateMinMeanMax()
	    {
            Min = MathUtil.CalculateMin(Points, DataPointsState);
            Max = MathUtil.CalculateMax(Points, DataPointsState);
            Mean = MathUtil.CalculateMean(Points, DataPointsState); 
        }

	    public void UpdateOverallStatus()
	    {
            ListOfFoundStatus = MathUtil.GetAllDifferentStatusOfThisWell(DataPointsState);
            OverallStatus = MathUtil.GetOverallState(DataPointsState);
		}
        public event EventHandler WellDataDimensionChanged;
        public event EventHandler WellDataPointChanged;

        public void Serialize(BinaryWriter writer)
        {
            writer.Write(WellName ?? "");
            writer.Write((int)OverallStatus);
            writer.Write(XPos);
            writer.Write(YPos);
		    writer.Write(ReductionValue);
		    writer.Write((int) ReductionState);

	        RawBinaryWriter.SerializeOneDimensionArray(writer, ListOfFoundStatus);
            RawBinaryWriter.SerializeTwoDimensionArray(writer, Min);
            RawBinaryWriter.SerializeTwoDimensionArray(writer, Mean);
            RawBinaryWriter.SerializeTwoDimensionArray(writer, Max);
            RawBinaryWriter.SerializeThreeDimensionArray(writer, Points);
            RawBinaryWriter.SerializeThreeDimensionArray(writer, DataPointsState);
            RawBinaryWriter.SerializeOneDimensionArray(writer, CycleTime);
        }

        public void Deserialize(BinaryReader reader, Version documentVersion = null)
        {
            WellName = reader.ReadString();
            OverallStatus = (DataPointStatus)reader.ReadInt32();
            XPos = reader.ReadInt32();
            YPos = reader.ReadInt32();

	        if (documentVersion == null || documentVersion.Major > 1)
	        {
		        ReductionValue = reader.ReadDouble();
		        ReductionState = (DataPointStatus) reader.ReadInt32();
	        }

	        ListOfFoundStatus = RawBinaryReader.DeserializeOneDimensionArrayStatus(reader);
            Min = RawBinaryReader.DeserializeTwoDimensionArray(reader);
            Mean = RawBinaryReader.DeserializeTwoDimensionArray(reader);
            Max = RawBinaryReader.DeserializeTwoDimensionArray(reader);
            Points = RawBinaryReader.DeserializeThreeDimensionArray(reader);
            DataPointsState = RawBinaryReader.DeserializeThreeDimensionArrayStatus(reader);
            CycleTime = RawBinaryReader.DeserializeOneDimensionArray(reader);
        }
    }
}