﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gimli.JsonObjects.Enumarations.Status;

namespace Gimli.JsonObjects
{
    internal static class MathUtil
    {
        internal static double[,] CalculateMean(List<Well> wells)
        {
            var dataWell = wells?.FirstOrDefault();
            if (dataWell?.Mean == null || dataWell.Mean.Length == 0) return null;

            var means = new double[dataWell.Mean.GetLength(0), dataWell.Mean.GetLength(1)];

            {
                for (var i = 0; i < means.GetLength(0); i++)
                {
                    for (var r = 0; r < means.GetLength(1); r++)
                    {
                        var count = 0;
                        var sum = 0.0;

                        foreach (var well in wells)
                        {
                            if (double.IsNaN(means[i, r])) continue;
                            count++;
                            sum += well.Mean[i, r];
                        }

                        if (count > 0 && sum > 0)
                            means[i, r] = Math.Round(sum/count, 3);
                    }
                }
            }
            return means;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "1#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#")]
        internal static double[] CalculateMean(double[,] values)
        {
            if (values == null || values.Length == 0) return null;
            var means = new double[values.GetLength(0)];

            for (var wl = 0; wl < values.GetLength(0); wl++)
            {
                var count = 0;
                var sum = 0.0;
                for (var r = 0; r < values.GetLength(1); r++)
                {
                    if (double.IsNaN(values[wl, r])) continue;

                    count++;
                    sum += values[wl, r];
                }
                if (count > 0 && sum > 0)
                {
                    means[wl] = Math.Round(sum / count, 3);
                }
                else
                {
                    means[wl] = 0.0;
                }
            }

            return means;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "1#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#")]
        internal static double[,] CalculateMean(double[,,] values, DataPointStatus[,,] status)
        {
	        if (values == null) return null;
            if (status == null) return null;
            var means = new double[values.GetLength(0), values.GetLength(1)];

            for (var wl = 0; wl < values.GetLength(0); wl++)
			{
                for (var r = 0; r < values.GetLength(1); r++)
                {
                    var count = 0;
                    var sum = 0.0;
                    for (var c = 0; c < values.GetLength(2); c++)
                    {
				        if (status[wl, r, c] == DataPointStatus.OK)
				        {
				            count++;
					        sum += values[wl, r, c];
                        }
			        }
                    if (count > 0 && sum > 0)
                    {
                        means[wl,r] = Math.Round(sum / count, 3);
                    }
                    else
                    {
                        means[wl, r] = 0.0;
                    }

                }
	        }

	        return means;
        }
        internal static double[,] CalculateMin(List<Well> wells)
        {
            var dataWell = wells?.FirstOrDefault();
            if (dataWell?.Min == null || dataWell.Min.Length == 0) return null;

            var mins = new double[dataWell.Min.GetLength(0), dataWell.Min.GetLength(1)];
            for (var i = 0; i < mins.GetLength(0); i++)
            {
                for (var r = 0; r < mins.GetLength(1); r++)
                {
                    mins[i, r] = double.MaxValue;
                }
            }

            foreach (var well in wells)
            {
                for (var i = 0; i < mins.GetLength(0); i++)
                {
                    for (var r = 0; r < mins.GetLength(1); r++)
                    {
                        if (mins[i, r] > well.Min[i, r])
                            mins[i, r] = RoundDownToDecimals(well.Min[i, r], 3);
                    }
                }
            }

            for (var i = 0; i < mins.GetLength(0); i++)
            {
                for (var r = 0; r < mins.GetLength(1); r++)
                {
                    if (Math.Abs(mins[i, r] - double.MaxValue) <= double.Epsilon)
                        mins[i, r] = 0;
                }
            }

            return mins;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "1#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#")]
        internal static double[] CalculateMin(double[,] values)
        {
            if (values == null) return null;
            var mins = new double[values.GetLength(0)];

            for (var wl = 0; wl < values.GetLength(0); wl++)
            {
                var tempMin = double.MaxValue;
                for (var r = 0; r < values.GetLength(1); r++)
                {
                    if (double.IsNaN(values[wl, r]) || tempMin < values[wl, r]) continue;
                    tempMin = values[wl, r];
                }
                if(tempMin<double.MaxValue)
                    mins[wl] = RoundDownToDecimals(tempMin, 3);
            }

            return mins;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "1#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#")]
        internal static double[,] CalculateMin(double[,,] values, DataPointStatus[,,] status)
        {
            if (values == null) return null;
            if (status == null) return null;
            var mins = new double[values.GetLength(0), values.GetLength(1)];

            for (var wl = 0; wl < values.GetLength(0); wl++)
            {
                var tempMin = double.MaxValue;
                for (var r = 0; r < values.GetLength(1); r++)
                {
                    for (var c = 0; c < values.GetLength(2); c++)
                        {
                        if (status[wl, r, c] == DataPointStatus.OK && values[wl, r, c] < tempMin)
                        {
                            tempMin = values[wl, r, c];
                        }
                    }

	                if (tempMin < double.MaxValue)
		                mins[wl, r] = RoundDownToDecimals(tempMin, 3);
                }
            }

            return mins;
        }

	    internal static double RoundDownToDecimals(double value, int decimals)
	    {
			return RoundToDecimals(Math.Floor, value, decimals);
		}
		internal static double RoundUpToDecimals(double value, int decimals)
		{
			return RoundToDecimals(Math.Ceiling, value, decimals);
		}

		private static double RoundToDecimals(Func<double, double> rounder, double value, int decimals)
		{
			var multiplier = Math.Pow(10, decimals);
			return rounder(value * multiplier) / multiplier;
		}

		internal static double[,] CalculateMax(List<Well> wells)
        {
            var dataWell = wells?.FirstOrDefault();
            if (dataWell?.Max == null || dataWell.Max.Length == 0) return null;

            var maxs = new double[dataWell.Max.GetLength(0), dataWell.Max.GetLength(1)];

            foreach (var well in wells)
            {
                for (var i = 0; i < maxs.GetLength(0); i++)
                {
                    for (var r = 0; r < maxs.GetLength(1); r++)
                    {
                        if (maxs[i, r] < well.Max[i, r])
                            maxs[i, r] = RoundUpToDecimals(well.Max[i, r], 3);
                    }
                }
            }

            return maxs;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "1#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#")]
        internal static double[] CalculateMax(double[,] values)
        {
            if (values == null) return null;
            var maxs = new double[values.GetLength(0)];

            for (var wl = 0; wl < values.GetLength(0); wl++)
            {
                var tempMax = 0d;
                for (var r = 0; r < values.GetLength(1); r++)
                {
                    if (double.IsNaN(values[wl, r]) || tempMax > values[wl, r]) continue;
                    tempMax = values[wl, r];
                }
                maxs[wl] = RoundUpToDecimals(tempMax, 3);
            }

            return maxs;
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "1#")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#")]
        internal static double[,] CalculateMax(double[,,] values, DataPointStatus[,,] status)
        {
            if (values == null) return null;
            if (status == null) return null;
            var maxs = new double[values.GetLength(0), values.GetLength(1)];

            for (var wl = 0; wl < values.GetLength(0); wl++)
            {
                var tempMax = 0d;
                for (var r = 0; r < values.GetLength(1); r++)
                {
                    for (var c = 0; c < values.GetLength(2); c++)
                    {
                        if (status[wl, r, c] == DataPointStatus.OK && values[wl, r, c] > tempMax)
                        {
                            tempMax = values[wl, r, c];
                        }
                    }
                    maxs[wl, r] = RoundUpToDecimals(tempMax, 3);
                }
            }

            return maxs;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#")]
        internal static DataPointStatus[] GetAllDifferentStatusOfThisWell(DataPointStatus[,,] status)
        {
            if (status == null) return null;

		    var statuses = new List<DataPointStatus>();
            for (var wl = 0; wl < status.GetLength(0); wl++)
		    {
			    for (var c = 0; c < status.GetLength(2); c++)
			    {
				    for (var r = 0; r < status.GetLength(1); r++)
				    {
					    if (status[wl, r, c] != DataPointStatus.OK && !statuses.Contains(status[wl, r, c]))
						    statuses.Add(status[wl, r, c]);
				    }
			    }
		    }

		    return statuses.ToArray();
	    }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "0#")]
        internal static DataPointStatus GetOverallState(DataPointStatus[,,] status)
        {
            if (status == null) return DataPointStatus.Unused;

            var statuses = GetAllDifferentStatusOfThisWell(status);

            if (statuses.Contains(DataPointStatus.Error)) return DataPointStatus.Error;
            if (statuses.Contains(DataPointStatus.Rejected)) return DataPointStatus.Rejected;
            if (statuses.Contains(DataPointStatus.Incorrect)) return DataPointStatus.Incorrect;
            if (statuses.Contains(DataPointStatus.Overflow)) return DataPointStatus.Overflow;
            if (statuses.Contains(DataPointStatus.Underflow)) return DataPointStatus.Underflow;
            if (statuses.Contains(DataPointStatus.Unused)) return DataPointStatus.Unused;
            return DataPointStatus.OK;
        }
    }
}
