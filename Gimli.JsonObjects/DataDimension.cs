﻿using System;
using System.Collections.Generic;

namespace Gimli.JsonObjects
{
    [Serializable]
    public class DataDimension
    {
        public DataDimension() { }

        public DataDimension(DataDimension originalDataDimension)
        {
            if (originalDataDimension == null) return;

            Name = originalDataDimension.Name;
            Dimension = originalDataDimension.Dimension;
            DimensionName = originalDataDimension.DimensionName;
            DimensionType = originalDataDimension.DimensionType;
            DataPointLabel = new List<double>();

            if (originalDataDimension.DataPointLabel == null) return;

            foreach (var item in originalDataDimension.DataPointLabel)
            {
                DataPointLabel.Add(item);
            }
        }
        public DataDimension(string dimensionName, int dimension)
        {
            Name = dimensionName;
            Dimension = dimension;
        }
        public string Name { get; private set; }
        public int Dimension { get; private set; }
        public string DimensionName { get; set; } = "nm";
        public Type DimensionType { get; set; } = typeof (double);
        public List<double> DataPointLabel { get; set; }
        public List<double> MinValues { get; set; }
        public List<double> MaxValues { get; set; }
        internal DataDimension Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as DataDimension;
        }
    }
}
