﻿using System.IO;
using System.Linq;

namespace Gimli.JsonObjects.Utils
{
    public class FolderFileHelper
    {
        public static int GetFreeSpace(string drive) { 
            return (from d in DriveInfo.GetDrives()
                where d.RootDirectory.Root.Name.Contains(drive)
                select (int) (d.AvailableFreeSpace/1048576)).FirstOrDefault();
        }
		public static int GetTotalSpace(string drive)
		{
			return (from d in DriveInfo.GetDrives()
					where d.RootDirectory.Root.Name.Contains(drive)
					select (int)(d.TotalSize / 1048576)).FirstOrDefault();
		}
	}
}
