﻿using System;
using System.Diagnostics;
using System.IO;
using Gimli.JsonObjects.Datasets;

namespace Gimli.JsonObjects.Utils
{
    public static class DataSetHelper
    {
        public static Guid GetGuidAsName(DocumentDs dataSet)
        {
            foreach (DocumentDs.DocumentGeneralRow row in dataSet.DocumentGeneral.Rows)
            {
                if (row.IsTimestampNull()) row.Timestamp = DateTime.Now;
                if (row.IsIdNull()) row.Id = Guid.NewGuid();
                return row.Id;
            }
            return Guid.NewGuid();
        }

        public static void SetFileNameToDatSet(DocumentDs dataSet, string fileName)
        {
            foreach (DocumentDs.DocumentGeneralRow row in dataSet.DocumentGeneral.Rows)
            {
                row.ProtoclName = fileName;
            }
            dataSet.AcceptChanges();
        }

        public static bool WriteXmlToDataSet(DocumentDs dataSet, string fileName)
        {
            var success = false;
            try
            {
                dataSet.WriteXml(fileName);
                success = true;
            }
            catch (Exception exc)
            {
                Trace.WriteLine($"Error saving protocol: {fileName}\r\n{exc.Message}");
                success = false;
            }
            finally
            {
                dataSet.Dispose();
            }
            return success;
        }
        
        public static string RootPath => Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                                           @"/Molecular Devices/GimliData/";
        public static bool CreateSubFolder(string name)
        {
            var directory = name.Contains(RootPath) ? name : $@"{RootPath}{name}";
            try
            {
                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);
            }
            catch (Exception exc)
            {
                Trace.WriteLine($"Error creating Folder: {directory}\r\n{exc.Message}");
                return false;
            }
            return true;
        }
    }
}
