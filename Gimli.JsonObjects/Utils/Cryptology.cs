﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using Gimli.JsonObjects.DummyData;

namespace Gimli.JsonObjects.Utils
{
    public static class Cryptology
    {
        public static byte[] CreateHashCode(MemoryStream serializedData)
        {
            return serializedData != null ? CreateHashCode(serializedData.ToArray()) : new byte[0];
        }

        public static byte[] CreateHashCode(byte[] serializedData)
        {
            GimliTrace.Start("Cryptology");

            SHA512 shaM = new SHA512Managed();
            var result = shaM.ComputeHash(serializedData);
                                
            GimliTrace.Stop("Cryptology", "CreateHashCode");

            return result;
        }

        internal static bool IsHashValid(byte[] data, byte[] hashToCompare)
        {
            GimliTrace.Start("Cryptology");

            if (data == null || data.Length == 0) return false;

            var newHash = CreateHashCode(data);
            if (!BitConverter.ToString(newHash).Equals(BitConverter.ToString(hashToCompare)))
            {
                Trace.WriteLine("Hash not valid!");
                return false;
            }
            GimliTrace.Stop("Cryptology", "IsHashValid");

            return true;
        }
    }
}
