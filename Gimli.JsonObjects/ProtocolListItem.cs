using System;
using System.Xml.Serialization;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class ProtocolListItem
	{
		//for Serialization only
		public ProtocolListItem()
		{
			
		}

		public ProtocolListItem(Guid id, string userNameId, string protocolName, string displayName, DateTime timestamp, bool hasData, bool isSynchronized)
		{
			Id = id;
			DisplayName = displayName;
			Timestamp = timestamp;
			UserNameGuid = userNameId;
			ProtocolName = protocolName;
			DisplayName = displayName;
			HasData = hasData;
			IsSynchronized = isSynchronized;
		    IsProtected = false;
            LastUsedDate = DateTime.MinValue;
		    IsPinned = false;
		    Category = string.Empty;
            PinDate = DateTime.MinValue;
			PinSlot = 0;
			PinName = string.Empty;
		    Categories = string.Empty;
		}

		public Guid Id { get; set; }

		// TODO: Tell Amplify to rename
		[XmlIgnore]
		public string Name => DisplayName;

	    public string ProtocolName { get; set; }
		public string DisplayName { get; set; }
		public DateTime Timestamp { get; set; }
		public bool HasData { get; set; }
		public int NumberOfPlates { get; set; } = 1;
		public int PercentMeasured { get; set; } = 0;
		public long Size { get; set; } = 0;
		public string UserNameGuid { get; set; }
		public string Notes { get; set; }
		public bool IsSynchronized { get; set; }
        public bool IsProtected { get; set; }
        public DateTime LastUsedDate { get; set; }
        public bool IsPinned { get; set; }
        public DateTime PinDate { get; set; }
		public int PinSlot { get; set; }
		public string PinName { get; set; }
		public string Category { get; set; }
        public string Categories { get; set; }
        public Version Version { get; set; }
	}
}