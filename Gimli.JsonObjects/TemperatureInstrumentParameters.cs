﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class TemperatureInstrumentParameters
	{
		public DoubleValueWithRange Target { get; set; }
		public bool ControllerRunning { get; set; }
	}
}
