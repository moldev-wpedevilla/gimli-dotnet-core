﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class UserNfcPair : UserNameOnly
	{
		public Guid NfcId { get; set; }
	}
}
