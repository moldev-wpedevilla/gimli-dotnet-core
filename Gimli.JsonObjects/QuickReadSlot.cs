﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class QuickReadSlot
	{
		public string Name { get; set; }
		public string ProtocolId { get; set; }

		public static QuickReadSlot CreateEmptySlot()
		{
			return new QuickReadSlot {Name = string.Empty, ProtocolId = null};
		}
	}
}
