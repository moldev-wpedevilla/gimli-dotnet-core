﻿namespace Gimli.JsonObjects
{
	public enum MeasurementStatus
	{
		None,
		Initializing,
		Optimizing,
		Preparing,
		Measuring,
		RunningChecks,
		Pausing,
		Paused,
		Stopping,
		Stopped,
		ErrorOccurred,
		Finished
	}
}
