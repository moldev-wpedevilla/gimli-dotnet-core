﻿using Gimli.JsonObjects.Deserialize;
using Gimli.JsonObjects.DummyData;
using Gimli.JsonObjects.Labware;
using Gimli.JsonObjects.Parameter;
using Gimli.JsonObjects.Settings;
using System;
using System.Collections.Generic;
using Gimli.JsonObjects.Datasets;
using DocumentDeserialization = Gimli.JsonObjects.Deserialize.DocumentDeserialization;

namespace Gimli.JsonObjects.Load
{
    public class LoadDocumentFromDataSet
    {
        public static Document Load(DocumentDs docDs)
        {
            var docObj = new LoadDocumentFromDataSet();
            return docObj.CreateAndAddDocument(docDs);
        }
        private Document CreateAndAddDocument(DocumentDs docDs)
        {
            if (docDs == null) return null;

            var document = new Document();
            foreach (DocumentDs.DocumentGeneralRow row in docDs.DocumentGeneral.Rows)
            {
                document.Id = row.Id;
                document.ProtocolName = row.ProtoclName;
                document.DisplayName = row.DisplayName;
                document.Timestamp = row.Timestamp;
                document.IsSynchronized = row.IsSynchronized;
                document.HasData = row.HasData;
                document.NumberOfPlates = row.NumberOfPlates;
                document.PercentMeasured = row.PercentMeasured;
                document.UserNameGuid = row.UserId;
                document.Notes = row.Notes;
                document.IsProtected = row.IsProtected;
                document.LastUsedDate = new DateTime(row.LastUsedDate);
                document.Experiments = new List<Experiment>();
                document.IsPinned = row.IsPinned;
	            document.PinSlot = row.IsPinSlotNull() ? -1 : row.PinSlot;
	            document.PinName = row.IsPinNameNull() ? null : row.PinName;
                document.Category = row.Category;
                document.PinDate = new DateTime(row.PinDate);
                document.Version = new Version(row.Version);
                CreateAndAddExperiment(row, document, document.Version);
            }
            return document;
        }

        private void CreateAndAddExperiment(DocumentDs.DocumentGeneralRow row, Document document, Version version)
        {
            foreach (var expRow in row.GetExperimentsRows())
            {
                var experiment = new Experiment
                {
                    Id = expRow.ExperiemtnGuid,
                    ExpName = expRow.ExperimentName,
                    Plates = new List<Plate>()
                };
                document.Experiments.Add(experiment);

                CreateAndAddPlate(expRow, experiment, version);
            }
        }

        private void CreateAndAddPlate(DocumentDs.ExperimentsRow expRow, Experiment experiment, Version version)
        {
            foreach (var pltRow in expRow.GetPlatesRows())
            {
                var plate = new Plate
                {
                    Id = pltRow.PlateGuid,
                    PlateName = pltRow.PlateName,
                    SnapShot = new List<Snapshot>()
                };
                plate.Settings = new PlateSettings { Plate = GetMicroplateInformation(pltRow) };
                plate.Settings.IsLidded = DeserializeParameter(pltRow.GetSettingsRows()[0], "IsLidded") as GimliParameterBool;
                plate.Settings.Orientation = DeserializeParameter(pltRow.GetSettingsRows()[0], "Orientation") as GimliParameterString;
                plate.Settings.MicroplateName = DeserializeString(pltRow.GetSettingsRows()[0], "MicroplateName");
                plate.Settings.WellCount = DeserializeString(pltRow.GetSettingsRows()[0], "WellCount");
                plate.Settings.Barcode = DeserializeString(pltRow.GetSettingsRows()[0], "Barcode");
                plate.Settings.UserName = DeserializeString(pltRow.GetSettingsRows()[0], "UserName");
                plate.Settings.InstrumentSerial = DeserializeString(pltRow.GetSettingsRows()[0], "InstrumentSerial");
                plate.Settings.InstrumentName = DeserializeString(pltRow.GetSettingsRows()[0], "InstrumentName");
                plate.Settings.MeasureDate = DeserializeDateTime(pltRow.GetSettingsRows()[0], "MeasureDate");

                experiment.Plates.Add(plate);

                CreateAndAddSnapShot(pltRow, plate, version);

                //if (plate.SnapShot[0].HasData && plate.SnapShot[0].Wells != null && plate.SnapShot[0].Wells.Count > 96)
                //{
                //    plate.Settings.WellCount = "384 Wells";
                //    plate.Settings.MicroplateName = "Standard clrbtm";
                //    plate.Settings.Plate = PlateLibrary.MicroplateLibrary.GetFirstPlateInfo(384, "MultimodeGroup");
                //}
            }
        }

        private string DeserializeString(DocumentDs.SettingsRow settingsRow, string name)
        {
            if (settingsRow == null) return null;
            foreach (var dataRow in settingsRow.GetSettingsParameterRows())
            {
                if (dataRow.Name == name && !dataRow.IsDataNull())
                    return DocumentDeserialization.DeserializeObject<string>(dataRow.Data);
            }
            return string.Empty;
        }
        private DateTime DeserializeDateTime(DocumentDs.SettingsRow settingsRow, string name)
        {
            if (settingsRow == null) return DateTime.MinValue;
            foreach (var dataRow in settingsRow.GetSettingsParameterRows())
            {
                if (dataRow.Name == name && !dataRow.IsDataNull())
                    return DocumentDeserialization.DeserializeObject<DateTime>(dataRow.Data);
            }
            return DateTime.MinValue;
        }
        private GimliParameter DeserializeParameter(DocumentDs.SettingsRow settingsRow, string name)
        {
            if (settingsRow == null) return null;
            foreach (var dataRow in settingsRow.GetSettingsParameterRows())
            {
                if (dataRow.Name == name && !dataRow.IsDataNull())
                    return DocumentDeserialization.DeserializeObject<GimliParameter>(dataRow.Data);
            }
            return null;
        }
        private Microplate GetMicroplateInformation(DocumentDs.PlatesRow pltRow)
        {
            var sRow = pltRow.GetSettingsRows();
            if (sRow.Length == 0) return PlateLibrary.MicroplateLibrary.GenerateDefaultPlate();
            var row = sRow[0].GetMicroplateRows();
            if (row == null) return PlateLibrary.MicroplateLibrary.GenerateDefaultPlate(); 
            return row[0].IsDataNull()
                ? null
                : DocumentDeserialization.DeserializeObject<Microplate>(row[0].Data);
        }
        private void CreateAndAddSnapShot(DocumentDs.PlatesRow pltRow, Plate plate, Version version)
        {
            foreach (var snapShotsRow in pltRow.GetSnapShotsRows())
            {
                GimliTrace.Start("Deserialize");
                var snapShot = new Snapshot
                {
                    Id = snapShotsRow.Id,
                    IncrementX = snapShotsRow.IncrementX,
                    IncrementY = snapShotsRow.IncrementY,
                    MaxXPos = snapShotsRow.MaxXPos,
                    MaxYPos = snapShotsRow.MaxYPos,
                    MinXPos = snapShotsRow.MinXPos,
                    MinYPos = snapShotsRow.MinYPos,
                    MaxValue = snapShotsRow.MaxValue,
                    MinValue = snapShotsRow.MinValue,
					ReducedMaxValue = snapShotsRow.ReducedMaxValue,
					ReducedMinValue = snapShotsRow.ReducedMinValue,
					SnapshotDataName = snapShotsRow.SnapShotDataName
                };
                GimliTrace.ElapsedRestart("Deserialize", "Creating SnapShot");
                if (snapShotsRow.WellBasedSerialization)
                {
                    CreateAndAddWells(snapShotsRow, snapShot);
                }
                else
                {
                    snapShot.Wells = RawBinaryReader.DeserializeWells(snapShotsRow.SerializedData, snapShotsRow.Size, version);
                }
                GimliTrace.ElapsedRestart("Deserialize", "Data deserialized.");
                var deSerializeObject = GetSettingsObject<MeasurementSettingsSerialize>(snapShotsRow, "Settings");
                if (deSerializeObject != null)
                {
                    snapShot.Settings = MeasurementSettings.Generate(deSerializeObject.MeasurementMode,
                        deSerializeObject.MeasurementType);
                    snapShot.Settings.Unit = deSerializeObject.Unit;
                    snapShot.Settings.IsModuleUsed = deSerializeObject.IsModuleUsed;
                    snapShot.Settings.ModuleInformation = deSerializeObject.ModuleInformation;
                }
	            snapShot.Settings.ReadArea = GetSettingsObject<ReadAreaSettings>(snapShotsRow, "ReadArea");
                snapShot.Settings.DetectionSetting = GetSettingsObject<DetectionSettings>(snapShotsRow, "DetectionSetting");
                snapShot.Settings.Shake = GetSettingsObject<ShakeSetting>(snapShotsRow, "Shake");
                snapShot.Settings.Kinetic = GetSettingsObject<KineticSettings>(snapShotsRow, "Kinetic");
                snapShot.Settings.Wavelengths = GetSettingsObject<WavelengthSettings>(snapShotsRow, "Wavelengths");
                var ws = GetSettingsObject<WellScanSettings>(snapShotsRow, "WellScan");
                if (ws != null)
                    snapShot.Settings.WellScan = ws;
	            snapShot.ReductionSettings = GetSettingsObject<DataReductionSettings>(snapShotsRow, "ReductionSettings");
                plate.Settings.SetReadModeForValidation(snapShot.Settings.MeasurementMode);
                GimliTrace.Stop("Deserialize", "Deserialization Settings");

                plate.SnapShot.Add(snapShot);
            }
        }

        private void CreateAndAddWells(DocumentDs.SnapShotsRow snapShotsRow, Snapshot snapShot)
        {
            snapShot.Wells = new List<Well>();
            foreach (var wellRow in snapShotsRow.GetWellSerializationRows())
            {
                snapShot.Wells.Add(DocumentDeserialization.UnzipAndDeserialize<Well>(wellRow.SerializedData, wellRow.Size));
            }
        }

        private T GetSettingsObject<T>(DocumentDs.SnapShotsRow snapRow, string name)
        {
            var sRow = snapRow.GetSnapshotSettingsRows();
            foreach (var settingsRow in sRow)
            {
                if (settingsRow == null) continue;
                if (!settingsRow.SettingsName.Equals(name)) continue;
                if (settingsRow.IsDataNull()) return default(T);

                return DocumentDeserialization.DeserializeObject<T>(settingsRow.Data);
            }
            return default(T);
        }
	}
}
