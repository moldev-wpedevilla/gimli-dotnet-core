﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class DoubleValueWithRange : DoubleValueWithMaximum
	{
		public double Minimum { get; set; }
		public double Step { get; set; }

		public bool IsValid()
		{
			if (Value > Maximum) return false;
			if (Value < Minimum) return false;

		    double stepFull = Step;
            double valueFull = Value;
		    double minFull = Minimum;

            if (Step - Math.Truncate(Step) > 0)
		    {
		        stepFull = Step*10000;
		        valueFull = Value*10000;
		        minFull = Minimum * 10000;
		    }
            return !(Math.Abs(Math.IEEERemainder(valueFull - minFull, stepFull)) > double.Epsilon);
		}
	}
}
