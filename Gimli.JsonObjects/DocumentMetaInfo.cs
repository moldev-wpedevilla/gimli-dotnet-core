﻿using System;
using System.Collections.Generic;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class DocumentMetaInfo
	{
		public List<ReadModeTypeCombination> ReadModeTypeCombinations { get; set; }
        public List<SupportedMicroplate> SupportedMicroplates { get; set; }
	}
}
