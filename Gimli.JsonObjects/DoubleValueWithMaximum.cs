﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class DoubleValueWithMaximum
	{
		public double Value { get; set; }
		public double Maximum { get; set; }
	}
}
