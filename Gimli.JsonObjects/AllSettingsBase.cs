﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class AllSettingsBase
	{
		public Guid DocumentId { get; set; }
		public Guid PlateId { get; set; }
	}
}
