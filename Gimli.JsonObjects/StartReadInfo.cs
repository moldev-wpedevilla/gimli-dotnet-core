﻿using System;
using System.Collections.Generic;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class StartReadInfo
	{
		public Guid DocumentID { get; set; }
		public List<Guid> Plates { get; set; }
		public bool Overwrite { get; set; }
		public string Name { get; set; }
	}
}
