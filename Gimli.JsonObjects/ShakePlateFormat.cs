﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public enum ShakePlateFormat
	{
		None,
		Wells6,
		Wells12,
		Wells24,
		Wells48,
		Wells96,
		Wells384,
	}
}