﻿using System;

namespace Gimli.JsonObjects.Enums
{
    [Flags]
    public enum ItemType
    {
        Protocol = 0x0001,
        Standard = 0x0002,
        Result = 0x0004,
        LastUpdated = 0x0008,
        Quick = 0x0010
    }
}
