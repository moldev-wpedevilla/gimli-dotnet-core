﻿namespace Gimli.JsonObjects.Enumarations.Status
{
    public enum DataPointStatus
    {
        Unused,
        OK,
        Overflow,
        Error,
        Incorrect,
        Rejected,
        Underflow,
		Extrapolated,
		NotEvaluated
	}
}
