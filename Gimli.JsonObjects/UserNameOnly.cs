﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class UserNameOnly
	{
		public string UserName { get; set; }
	}
}
