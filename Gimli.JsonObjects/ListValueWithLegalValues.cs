﻿using System;
using System.Collections.Generic;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class ListValueWithLegalValues<T>
	{
		public T Value { get; set; }

		public List<T> LegalValues { get; set; }

		public bool IsValid()
		{
			return LegalValues.Contains(Value);
		}
	}
}
