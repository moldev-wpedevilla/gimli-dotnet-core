﻿using Gimli.JsonObjects.Parameter;
using Gimli.JsonObjects.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;

namespace System.Windows.Media
{
	public class Color
	{
		public string Hex;
	}
	public class Colors
	{
		public static Color Gray = new Color { Hex = "a" };
		public static Color Transparent = new Color { Hex = "" };
	}
}

namespace Gimli.JsonObjects.ReadArea
{
    [Serializable]
    public class ReadAreaCollection:SettingsBase
    {
        protected WellItem[,] Items;

		//public static ReadAreaCollection CreateDefaultReadArea()
		//{
		//    return new ReadAreaCollection(8,12, true);
		//}

		/// <summary>
		/// Default constructor for serialization
		/// </summary>
		public ReadAreaCollection() { }

        public ReadAreaCollection(ReadAreaCollection originalSettings) : base(originalSettings)
        {
            if (originalSettings?.Items != null)
                Items = originalSettings.Items.Clone() as WellItem[,];
        }
        public ReadAreaCollection(int rows, int columns, bool selectAll = false)
        {
            Items = new WellItem[rows, columns];
            for(var r=0; r<rows;r++)
                for (var c = 0; c < columns; c++)
                {
                    Items[r, c] = new WellItem(selectAll);
                }
        }
        public ReadAreaCollection(WellItem[,] items)
        {
            Items = items;
        }
        #region Validation Properties
        //private bool mIsValid = true;
        //private List<string> mErrorMessage = new List<string>();
        //public virtual bool IsValid => mIsValid;
        //public List<string> ErrorMessage => mErrorMessage;
        //public bool IsModified { get; set; }
        //public bool IsDisplayedInSummary { get; set; }
        //public string Description { get; set; }
        #endregion

        
        public string GetSelectedRowColumnString()
        {
            if (Items == null || GetUsedWellCount() == 0) return "Selected|NoWell";
            //All Selected
            string s;
            if (IsAllCellsSelected(out s)) return s;

            //Rows selected
            if (AreRowsSelected(out s)) return s;

            //Columns selected
            if (AreColumnsSelected(out s)) return s;

            //Multiple cells or Single cell selected
            return GetSingleOrMultipleCellSelected();
        }

        private string GetSingleOrMultipleCellSelected()
        {
            var startRow = 999;
            var endRow = 0;
            var startColumn = 999;
            var endColumn = 0;

            for (var r = 0; r < Items.GetLength(0); r++)
            {
                for (var c = 0; c < Items.GetLength(1); c++)
                {
                    if (Items[r, c].Selected)
                    {
                        endColumn = endColumn < c ? c : endColumn;
                        endRow = endRow < r ? r : endRow;
                        startColumn = startColumn > c ? c : startColumn;
                        startRow = startRow > r ? r : startRow;
                    }
                }
            }

            if (startRow == endRow && startColumn == endColumn)
                return $"{Properties.Resources.ReadAreaSingleWellSelected}|{(char) (65 + endRow)}{endColumn + 1}";

            return
                $"{Properties.Resources.ReadAreaMultipleCells}|{(char) (65 + startRow)}{startColumn + 1}:{(char) (65 + endRow)}{endColumn + 1}";
        }

        private bool AreColumnsSelected(out string s)
        {
            s = "";
            var x = 0;
            var columnName = Properties.Resources.ReadAreaColumnSelected + "|";
            for (var c = 0; c < Items.GetLength(1); c++)
            {
                if (IsColumnSelected(c))
                {
                    columnName += (c + 1) + ",";
                    continue;
                }
                if (!IsColumnNotSelected(c))
                {
                    x = 999;
                    break;
                }
            }
            if (x == 0 && columnName.Contains(","))
            {
                s = columnName.TrimEnd(',');
                return true;
            }
            return false;
        }

        private bool AreRowsSelected(out string s)
        {
            s ="";
            var x = 0;
            var rowName = Properties.Resources.ReadAreaRowSelected + "|";
            for (var r = 0; r < Items.GetLength(0); r++)
            {
                if (IsRowSelected(r))
                {
                    rowName += (char) (65 + r) + ",";
                    continue;
                }
                if (!IsRowNotSelected(r))
                {
                    x = 999;
                    break;
                }
            }
            if (x == 0 && rowName.Contains(","))
            {
                s = rowName.TrimEnd(',');
                return true;
            }
            return false;
        }

        private bool IsAllCellsSelected(out string s)
        {
            s = "";
            var x = 0;
            for (var r = 0; r < Items.GetLength(0); r++)
            {
                if (IsRowSelected(r)) x++;
            }
            if (x == Items.GetLength(0))
            {
                s = Properties.Resources.ReadAreaSelected + "|" + Properties.Resources.ReadAreaAllSelected;
                return true;
            }
            return false;
        }

        public WellItem[,] Wells
	    {
		    get { return Items; }
			set { Items = value; }
	    }

        public WellItem GetWell(int row, int column)
        {
            if (row < Items?.GetLength(0) && column<Items.GetLength(1) && row >= 0 && column >= 0)
                return Items[row, column];
            return null;
        }

        public override List<SettingsSummary> GetSummary => new List<SettingsSummary> {new SettingsSummary(Name)};
		public override IEnumerable<GimliParameter> GetProperties()
	    {
		    throw new NotImplementedException();
	    }

	    public bool SetWell(int row, int column, string controlName = "")
        {
            if (!IsRowColumnValid(row, column)) return false;

            Items[row, column].Selected = true;
            if (!string.IsNullOrEmpty(controlName))
                Items[row, column].ControlName = controlName;
            return true;
        }

        public bool DeselectWell(int row, int column)
        {
            if (!IsRowColumnValid(row, column)) return false;

            Items[row, column].Selected = false;
            Items[row, column].ControlName = string.Empty;
            return true;
        }

        public void SetAllWells(string controlName = "")
        {
            SetWellProperties(true, controlName);
        }

        public void InvertSelection(string controlName = "")
        {
            SetWellProperties(true, controlName, true);
        }

        public void DeselectAllWells()
        {
            SetWellProperties(false, string.Empty);
        }

        private void SetWellProperties(bool measure, string controlName, bool invert = false)
        {
            for (var r = 0; r < Items.GetLength(0); r++)
            {
                for (var c = 0; c < Items.GetLength(1); c++)
                {
                    Items[r, c].Selected = invert ? !Items[r, c].Selected : measure;
                    Items[r, c].ControlName = invert && !string.IsNullOrEmpty(Items[r, c].ControlName) ? string.Empty : controlName;
                }
            }
        }

        public bool IsSelected(int row, int column)
        {
            return IsRowColumnValid(row, column) && Items[row, column].Selected;
        }

        public string GetControlName(int row, int column)
        {
            return !IsRowColumnValid(row, column) ? string.Empty : Items[row, column].ControlName;
        }

        public List<string> GetAllControlNames()
        {
            var list = new List<string>();
            foreach (var item in Items)
            {
                if (!list.Contains(item.ControlName))
                    list.Add(item.ControlName);
            }
            return list;
        }

        public int Rows { get { return Items.GetLength(0); } }
        public int Columns { get { return Items.GetLength(1); } }
        public int GetUsedWellCount()
        {
            if (Items == null || Items.GetLength(0) == 0) return 0;
            return Items.Cast<WellItem>().Count(item => item.Selected);
        }

        public void SelectRow(int row)
        {
            SelectRow(row, string.Empty);
        }
        public void SelectRow(int row, string controlName)
        {
            SelectRow(row, controlName, Colors.Gray);
        }
        public void SelectRow(int row, string controlName, Color color)
        {
            SelectRow(row, controlName, color, true);
        }
        public void DeselectRow(int row)
        {
            SelectRow(row, string.Empty, Colors.Transparent, false);
        }
        
        private void SelectRow(int row, string controlName, Color color, bool selected)
        {
            if (Items == null || row >= Items.GetLength(0) || row < 0) return;
            for (var c = 0; c < Items.GetLength(1); c++)
            {
                Items[row, c].Selected = selected;
                Items[row, c].ControlName = controlName;
                Items[row, c].ControlColor = color.ToString();
            }
        }
        public void SelectColumn(int column)
        {
            SelectColumn(column, string.Empty);
        }
        public void SelectColumn(int column, string controlName)
        {
            SelectColumn(column, controlName, Colors.Gray);
        }
        public void SelectColumn(int column, string controlName, Color color)
        {
            SelectColumn(column, controlName, color, true);
        }
        public void DeselectColumn(int column)
        {
            SelectColumn(column, string.Empty, Colors.Transparent, false);
        }
        private void SelectColumn(int column, string controlName, Color color, bool selected)
        {
            if (Items == null || column >= Items.GetLength(1) || column < 0) return;
            for (var r = 0; r < Items.GetLength(0); r++)
            {
                Items[r, column].Selected = selected;
                Items[r, column].ControlName = controlName;
                Items[r, column].ControlColor = color.ToString();
            }
        }

		public void CopyReadAreaSelection(ReadAreaCollection other)
		{
			if (other.Rows != Rows || other.Columns != Columns) return;

			for (var c = 0; c < other.Columns; c++)
			{
				for (var r = 0; r < other.Rows; r++)
				{
					Wells[r, c].Selected = other.Wells[r, c].Selected;
				}
			}
		}

	    public void SelectRange(int startRow, int endRow, int startColumn, int endColumn)
        {
            SelectRange(startRow, endRow, startColumn, endColumn, string.Empty);
        }
        public void SelectRange(int startRow, int endRow, int startColumn, int endColumn, string controlName)
        {
            SelectRange(startRow, endRow, startColumn, endColumn, controlName, Colors.Gray);
        }
        public void SelectRange(int startRow, int endRow, int startColumn, int endColumn, string controlName, Color color)
        {
            SelectRange(startRow, endRow, startColumn, endColumn, controlName, color, true);
        }
        public void DeselectRange(int startRow, int endRow, int startColumn, int endColumn)
        {
            SelectRange(startRow, endRow, startColumn, endColumn, string.Empty, Colors.Transparent, false);
        }
        private void SelectRange(int startRow, int endRow, int startColumn, int endColumn, string controlName, Color color, bool selected)
        {
            if (!IsRowColumnValid(startRow, startColumn) ) return;
            if (!IsRowColumnValid(endRow, endColumn)) return;

            var startCol = GetLowerValue(startColumn, endColumn);
            var endCol = GetHigherValue(startColumn, endColumn);
            var startR = GetLowerValue(startRow, endRow);
            var endR = GetHigherValue(endRow, endRow);

            for (var r = startR; r <= endR; r++)
            {
                for (var c = startCol; c <= endCol; c++)
                {
                    Items[r, c].Selected = selected;
                    Items[r, c].ControlName = controlName;
                    Items[r, c].ControlColor = color.ToString();
                }
            }
        }

        private int GetLowerValue(int firstArgument, int secondArgument)
        {
            return firstArgument < secondArgument ? firstArgument : secondArgument;
        }

        private int GetHigherValue(int firstArgument, int secondArgument)
        {
            return firstArgument > secondArgument ? firstArgument : secondArgument;
        }

        private bool IsRowSelected(int row)
        {
            if (!IsRowValid(row)) return false;
            for (var c = 0; c < Items.GetLength(1); c++)
            {
                if (!Items[row, c].Selected) return false;
            }
            return true;
        }

        private bool IsRowNotSelected(int row)
        {
            if (!IsRowValid(row)) return false;
            for (var c = 0; c < Items.GetLength(1); c++)
            {
                if (Items[row, c].Selected) return false;
            }
            return true;
        }

        private bool IsColumnSelected(int column)
        {
            if (!IsColumnValid(column)) return false;
            for (var r = 0; r < Items.GetLength(0); r++)
            {
                if (!Items[r, column].Selected) return false;
            }
            return true;
        }

        private bool IsColumnNotSelected(int column)
        {
            if (!IsColumnValid(column)) return false;
            for (var r = 0; r < Items.GetLength(0); r++)
            {
                if (Items[r, column].Selected) return false;
            }
            return true;
        }
        private bool IsRowColumnValid(int row, int column)
        {
            if (Items == null) return false;
            return IsRowValid(row) && IsColumnValid(column);
        }

        private bool IsRowValid(int row)
        {
            if (Items == null) return false;
            return row < Items.GetLength(0) && row >= 0;
        }

        private bool IsColumnValid(int column)
        {
            if (Items == null) return false;
            return column < Items.GetLength(1) && column >= 0;
        }
    }
}
