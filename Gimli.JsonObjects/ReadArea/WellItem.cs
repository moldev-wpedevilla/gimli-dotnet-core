﻿using System;
using System.ComponentModel;
using System.Windows.Media;

namespace Gimli.JsonObjects.ReadArea
{
    [Serializable]
    public class WellItem
    {
        private string mControlName = string.Empty;
        private string mControlColor = string.Empty;
        public WellItem() { }
        public WellItem(bool selected) :this(selected, string.Empty, Colors.Transparent) { }
        public WellItem(bool selected, string controlName):this(selected,controlName,Colors.Transparent) { }
        public WellItem(bool selected, string controlName, Color controlColor)
        {
            Selected = selected;
            ControlName = controlName;
            ControlColor = controlColor.ToString();
        }
        public bool Selected { get; set; }
        [DefaultValue("")]
        public string ControlName { get { return mControlName; } set { mControlName = value; } }
        public string ControlColor { get { return mControlColor; } set { mControlColor = value; } }
    }
}
