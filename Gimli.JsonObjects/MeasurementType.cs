namespace Gimli.JsonObjects
{
	public enum MeasurementType
	{
		None,
		Endpoint,
		Kinetic,
		SpectrumScan,
		AreaScan,
	}

    public static class Extensions
    {
        public static bool IsGraphView(this MeasurementType type)
        {
            return type == MeasurementType.SpectrumScan || type == MeasurementType.Kinetic;
        }
        public static bool IsAreaScan(this MeasurementType type)
        {
            return type == MeasurementType.AreaScan;
        }
        public static bool IsSpectralScan(this MeasurementType type)
        {
            return type == MeasurementType.SpectrumScan;
        }
        public static bool IsKinetic(this MeasurementType type)
        {
            return type == MeasurementType.Kinetic;
        }

        public static bool UseMaxPoints(this MeasurementType type)
        {
            return type != MeasurementType.SpectrumScan &&
                   type != MeasurementType.Kinetic &&
                   type != MeasurementType.AreaScan;

        }
    }
}