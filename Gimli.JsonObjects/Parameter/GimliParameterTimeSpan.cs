﻿using System;
using Gimli.JsonObjects.Settings;
using Newtonsoft.Json;

namespace Gimli.JsonObjects.Parameter
{
    [Serializable]
    public class GimliParameterTimeSpan : GimliParameter
    {
        /// <summary>
        /// Default constructor for serialization only
        /// </summary>
        [JsonConstructor]
        public GimliParameterTimeSpan() { }
        internal GimliParameterTimeSpan(string name, TimeSpan value) : base(name)
        {
            Value = value;
        }

        public GimliParameterTimeSpan(GimliParameterTimeSpan originalTimeSpan) : base(originalTimeSpan)
        { }
        public override Type GetValueType => typeof(TimeSpan);

        /// <summary>
        /// Minimum in Ticks
        /// </summary>
        public override double Minimum { get; set; }

        /// <summary>
        /// Maximum in Ticks
        /// </summary>
        public override double Maximum { get; set; }
        /// <summary>
        /// Step in Ticks
        /// </summary>
        public override double Step { get; set; }
        public override bool LowerMin
        {
            get
            {
                var retValue = Value != null && Minimum > TimeSpan.Parse(Value.ToString()).Ticks;
                if (retValue)
                    ErrorMessage.Add($"{Resources.GetResource.GetString(Name)}: {Resources.GetResource.GetString("LowerMin")} ({Value} < {Minimum})");
                return retValue;
            }
        }

        public override bool UpperMax
        {
            get
            {
                var retValue = Value != null && Maximum < TimeSpan.Parse(Value.ToString()).Ticks;
                if (retValue)
                    ErrorMessage.Add($"{Resources.GetResource.GetString(Name)}: {Resources.GetResource.GetString("UpperMax")} ({Value} < {Maximum})");
                return retValue;
            }
        }

        public GimliParameterTimeSpan Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as GimliParameterTimeSpan;
        }

        public override ItemClass GetSummary(object format = null)
        {
            if (!IsEnabled || !IsUsed || !IsDisplayedInSummary) return null;
            var val = Value.ToString();
            var s = format as string;
            if (s != null)
            {
                val = ToTimeSpan().ToString(s);
            }
            return new ItemClass(Resources.GetResource.GetString(Name), val + (string.IsNullOrEmpty(Units) ? "" : " " + Units));
        }
    }
}
