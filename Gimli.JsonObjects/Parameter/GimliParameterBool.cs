﻿using Newtonsoft.Json;
using System;
using System.Collections;
using Gimli.JsonObjects.Settings;

namespace Gimli.JsonObjects.Parameter
{
    [Serializable]
    public class GimliParameterBool : GimliParameter
    {
		/// <summary>
		/// Default constructor for serialization only
		/// </summary>
		[JsonConstructor]
		public GimliParameterBool() { }
        internal GimliParameterBool(string name, bool value) : base(name)
        {
            Value = value;
	        LegalValues = new ArrayList {true, false};
        }
		public GimliParameterBool(GimliParameterBool originalBool) : base(originalBool)
        { }
        public GimliParameterBool Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as GimliParameterBool;
        }
        public override Type GetValueType => typeof (bool);
        public override ItemClass GetSummary(object onOff = null)
        {
            if (!IsEnabled || !IsUsed  || !IsDisplayedInSummary) return null;
            var val = Value.ToString();
            if (onOff is bool && (bool)onOff)
            {
                val = ToBool() ? Resources.GetResource.GetString("On") : Resources.GetResource.GetString("Off");
            }
            var temp = onOff as ArrayList;
            if (temp != null && temp.Count > 1)
                val = ToBool() ? temp[0].ToString() : temp[1].ToString();
            return new ItemClass(Resources.GetResource.GetString(Name), val);
        }
    }
}
