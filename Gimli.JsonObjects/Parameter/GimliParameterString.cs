﻿using System;
using Gimli.JsonObjects.Settings;
using Newtonsoft.Json;

namespace Gimli.JsonObjects.Parameter
{
    [Serializable]
    public class GimliParameterString : GimliParameter
    {
        /// <summary>
        /// Default constructor for serialization only
        /// </summary>
        [JsonConstructor]
        public GimliParameterString() { }
        internal GimliParameterString(string name, string value) : base(name)
        {
            Value = value;
        }

	    public GimliParameterString(GimliParameterString originalString) : base(originalString)
	    {}

        public override Type GetValueType => typeof(string);
        public GimliParameterString Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as GimliParameterString;
        }

        public override ItemClass GetSummary(object upperCase = null)
        {
            if (!IsEnabled || !IsUsed || !IsDisplayedInSummary) return null;
	        var val = Resources.GetResource.GetString(Value.ToString());
            if (upperCase is bool)
            {
                val = (bool) upperCase ? val.ToUpperInvariant() : val;
            }
            return new ItemClass(Resources.GetResource.GetString(Name), val + (string.IsNullOrEmpty(Units) ? "" : " " + Units));
        }
    }
}
