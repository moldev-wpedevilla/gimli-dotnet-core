﻿using System;

namespace Gimli.JsonObjects.Parameter
{
    public class GimliValueChangedEvent :EventArgs
    {
        public GimliValueChangedEvent(object oldValue, object newValue)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }
        public object OldValue { get; private set; }
        public object NewValue { get; private set; }
    }
}
