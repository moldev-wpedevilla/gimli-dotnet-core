﻿using System;
using Gimli.JsonObjects.FiltersMngmnt;
using Gimli.JsonObjects.Settings;
using Newtonsoft.Json;

namespace Gimli.JsonObjects.Parameter
{
    [Serializable]
    public class GimliParameterWavelength : GimliParameterInt
    {
        [JsonConstructor]
        public GimliParameterWavelength()
        {
            mUnits = Properties.Resources.UnitNanometer;
        }

        internal GimliParameterWavelength(string name, int value) : base(name, value)
        {
            mUnits = Properties.Resources.UnitNanometer;
        }
        public GimliParameterWavelength(GimliParameterWavelength originalInt) : base(originalInt)
        {
            if (originalInt == null) return;

            UseMonocromator = originalInt.UseMonocromator;
        }
        private bool mUseMonochromator = true;
        public bool UseMonocromator { get {return mUseMonochromator;} set { mUseMonochromator = value; } }

		public new GimliParameterWavelength Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as GimliParameterWavelength;
        }
		protected override bool IsLegalValue
		{
			get
			{
				var isInLegals = base.IsLegalValue;
				if (!UseMonocromator && LegalValues?.Count == 0)
				{
					ErrorMessage.Add($"{Resources.GetResource.GetString(Name)}: {Resources.GetResource.GetString("NoSuitableFiltersType")}");
				}
				return isInLegals;
			}
		}
		public override ItemClass GetSummary(object inclNm = null)
        {
            if (!IsEnabled || !IsUsed || !IsDisplayedInSummary) return null;
            var val = Value.ToString();
            if (inclNm is bool && (bool)inclNm)
            {
				if (UseMonocromator) val = $"{ToInt()} {Units}";
				else if (val.EndsWith(TransformedFilterInfo.FilterSeperator)) val = $"{val}{Units}";
			}
            return new ItemClass(Resources.GetResource.GetString(Name), val.Trim());
        }

	    public override bool UpperMax => IsPossibleToConvert() && base.UpperMax;

		public override bool LowerMin => IsPossibleToConvert() && base.LowerMin;

		public bool IsPossibleToConvert()
		{
			var stringValue = Value?.ToString();
		    if (stringValue == null) return false;
		    return !stringValue.Contains(TransformedFilterInfo.FilterSeperator) || stringValue.EndsWith(TransformedFilterInfo.FilterSeperator);
	    }


	    public override bool IsValid
		{
			get
			{
				ErrorMessage.Clear();
				if (!IsUsed || !IsEnabled || Hidden) return true;

				var isValid = UseMonocromator || IsLegalValue;
				// do not "if (UseMonocromator)", otherwise exEmSpread won't work any more
				isValid = isValid && !LowerMin && !UpperMax;
				if (isValid)
					ErrorMessage.Clear();
				return isValid;
			}
		}
	}
}
