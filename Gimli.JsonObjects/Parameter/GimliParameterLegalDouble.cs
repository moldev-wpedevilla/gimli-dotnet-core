﻿using Gimli.JsonObjects.Settings;
using Newtonsoft.Json;
using System;

namespace Gimli.JsonObjects.Parameter
{
    [Serializable]
    public class GimliParameterLegalDouble: GimliParameter
    {
        private double mMinimum;
        private double mMaximum;
        private double mStep;

        /// <summary>
        /// Default constructor for serialization only
        /// </summary>
        [JsonConstructor]
        public GimliParameterLegalDouble() { }
        internal GimliParameterLegalDouble(string name, double value) : base(name)
        {
            Value = value;
        }
        public GimliParameterLegalDouble(GimliParameterLegalDouble originalDouble) : base(originalDouble)
        { }
        public override Type GetValueType => typeof(double);
	    public override ItemClass GetSummary(object digits = null)
	    {
			if (!IsEnabled || !IsUsed || !IsDisplayedInSummary) return null;
			var val = Value.ToString();
			if (digits is int)
			{
				val = $"{Math.Round(ToDouble(), (int)digits)}";
			}
			return new ItemClass(Resources.GetResource.GetString(Name), val + (string.IsNullOrEmpty(Units) ? "" : " " + Units));
		}

	    public override double Minimum { get { return mMinimum; } set { mMinimum = value; } }
        public override double Maximum { get { return mMaximum; } set { mMaximum = value; } }
        public override double Step { get { return mStep; } set { mStep = value; } }
        public override bool LowerMin => false;

	    public override bool UpperMax => false;

	    public GimliParameterLegalDouble Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as GimliParameterLegalDouble;
        }

		public override bool IsValid
		{
			get
			{
				ErrorMessage.Clear();
				if (!IsUsed || !IsEnabled || Hidden) return true;
				return IsLegalValue;
			}
		}

	    protected override object GetTypedValueOrObject()
	    {
		    return ToDouble();
	    }
    }
}
