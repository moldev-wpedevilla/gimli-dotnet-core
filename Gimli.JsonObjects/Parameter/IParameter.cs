﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gimli.JsonObjects.Settings;

namespace Gimli.JsonObjects.Parameter
{
    public interface IParameter : IDisposable
    {
        List<string> ErrorMessage { get; }
        string Name { get; set; }
        string Description { get; set; }
        double Minimum { get; set; }
        double Maximum { get; set; }
        double Step { get; set; }
        bool ProtocolEditable { get; set; }
        bool ReadOnly { get; set; }
        bool Hidden { get; set; }
        bool AffectOthers { get; set; }
        bool LowerMin { get; }
        bool UpperMax { get; }
        bool IsValid { get; }
        object Value { get; set; }
        ArrayList LegalValues { get; set; }
        Type GetValueType { get; }
        bool IsUsed { get; set; }
        ItemClass GetSummary(object format = null);
        string Units { get; set; }
        GimliParameter[] Children { get; set; }
    }
}
