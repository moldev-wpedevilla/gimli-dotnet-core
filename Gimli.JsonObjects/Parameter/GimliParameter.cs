﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.ComponentModel;
using Gimli.JsonObjects.Settings;

namespace Gimli.JsonObjects.Parameter
{
    [Serializable]
    public abstract class GimliParameter : ValidationBase, IParameter
    {
        public static IParameter Generate<T>(string name, object value)
        {
            if (value == null) return null;

            if (typeof (T) == typeof (GimliParameterBool))
                return new GimliParameterBool(name, (bool) value);

            if (typeof (T) == typeof (GimliParameterString))
                return new GimliParameterString(name, (string) value);
            if (typeof (T) == typeof (GimliParameterInt))
                return new GimliParameterInt(name, (int) value);
            if (typeof (T) == typeof (GimliParameterDouble))
                return new GimliParameterDouble(name, (double) value);
            if (typeof (T) == typeof (GimliParameterTimeSpan))
                return new GimliParameterTimeSpan(name, (TimeSpan) value);
            if (typeof (T) == typeof (GimliParameterWavelength))
                return new GimliParameterWavelength(name, (int) value);
            return null;
        }

        public static IParameter Generate(string name, object value)
        {
            if (value == null) return null;
            if (value is string)
                return new GimliParameterString(name, (string)value);
            if (value is int)
                return new GimliParameterInt(name, (int)value);
            if (value is double)
                return new GimliParameterDouble(name, (double)value);
            if (value is bool)
                return new GimliParameterBool(name, (bool)value);
            if (value is TimeSpan)
                return new GimliParameterTimeSpan(name, (TimeSpan)value);
            return null;
        }

        public static IParameter Generate(string name, object value, ArrayList legalValues)
        {
            if (value == null) return null;
            if (value is string)
                return new GimliParameterString(name, (string)value) {LegalValues = legalValues};
            if (value is int)
                return new GimliParameterInt(name, (int)value) { LegalValues = legalValues };
            if (value is double)
                return new GimliParameterDouble(name, (double)value) { LegalValues = legalValues };
            if (value is bool)
                return new GimliParameterBool(name, (bool)value) { LegalValues = legalValues };
            if (value is TimeSpan)
                return new GimliParameterTimeSpan(name, (TimeSpan)value) { LegalValues = legalValues };
            return null;
        }

        public static IParameter Generate(string name, object value, double min, double max)
        {
            if (value == null) return null;
            if (value is int)
                return new GimliParameterInt(name, (int) value) {Minimum = (int)min, Maximum = (int)max};
            if (value is double)
                return new GimliParameterDouble(name, (double)value) { Minimum = min, Maximum = max };
            if (value is bool)
                return new GimliParameterBool(name, (bool)value) { Minimum = min, Maximum = max };
            if (value is TimeSpan)
                return new GimliParameterTimeSpan(name, (TimeSpan)value) { Minimum = min, Maximum = max };
            return null;
        }

        public static IParameter Generate(string name, object value, double min, double max, double step)
        {
            if (value == null) return null;
            if (value is int)
                return new GimliParameterInt(name, (int)value) { Minimum = min, Maximum = max, Step = step };
            if (value is double)
                return new GimliParameterDouble(name, (double)value) { Minimum = min, Maximum = max, Step = step };
            if (value is bool)
                return new GimliParameterBool(name, (bool)value) { Minimum = min, Maximum = max, Step = step };
            if (value is TimeSpan)
                return new GimliParameterTimeSpan(name, (TimeSpan)value) { Minimum = min, Maximum = max, Step = step };
            return null;
        }

        /// <summary>
        /// Default constructor for serialization only
        /// </summary>
        [JsonConstructor]
        public GimliParameter()
        {
            LowerMin = true;
            UpperMax = true;
        }

        internal GimliParameter(string name)
        {
            LowerMin = true;
            UpperMax = true;
	        IsEnabled = true;
            Name = name;
        }

        public GimliParameter(GimliParameter originalParameter)
        {
            if (originalParameter == null) return;

            LowerMin = true;
            UpperMax = true;
            Units = originalParameter.Units;
            Name = originalParameter.Name;
            Value = originalParameter.Value;
            Minimum = originalParameter.Minimum;
            Maximum = originalParameter.Maximum;
            Step = originalParameter.Step;
            ProtocolEditable = originalParameter.ProtocolEditable;
            ReadOnly = originalParameter.ReadOnly;
            Hidden = originalParameter.Hidden;
            AffectOthers = originalParameter.AffectOthers;
            mIsValid = originalParameter.mIsValid;
            IsUsed = originalParameter.IsUsed;
            LegalValues = originalParameter.LegalValues;
            mErrorMessage = originalParameter.mErrorMessage;
            IsDisplayedInSummary = originalParameter.IsDisplayedInSummary;
            Description = originalParameter.Description;
            IsModified = originalParameter.IsModified;
            IsEnabled = originalParameter.IsEnabled;

            if (originalParameter.Children != null && originalParameter.Children.Length > 0)
            {
                Children = new GimliParameter[originalParameter.Children.Length];
                for (var i = 0; i < originalParameter.Children.Length; i++)
                {
                    if (originalParameter.Children[i] is GimliParameterBool)
                        Children[i] = new GimliParameterBool(originalParameter.Children[i] as GimliParameterBool);
                    if (originalParameter.Children[i] is GimliParameterInt)
                        Children[i] = new GimliParameterInt(originalParameter.Children[i] as GimliParameterInt);
                    if (originalParameter.Children[i] is GimliParameterString)
                        Children[i] = new GimliParameterString(originalParameter.Children[i] as GimliParameterString);
                    if (originalParameter.Children[i] is GimliParameterTimeSpan)
                        Children[i] = new GimliParameterTimeSpan(originalParameter.Children[i] as GimliParameterTimeSpan);
                    if (originalParameter.Children[i] is GimliParameterDouble)
                        Children[i] = new GimliParameterDouble(originalParameter.Children[i] as GimliParameterDouble);
                }
            }
        }
		public GimliParameter[] Children { get; set; }
        private object mValue;
        protected string mUnits = string.Empty;

        public object Value
        {
            get
            {
                return mValue;
            }
            set
            {
                GimliValueChangedEvent changed = null;
                if (mValue != value)
                    changed = new GimliValueChangedEvent(mValue, value);
                mValue = value;
                if (changed != null)
                    ValueChangedEventHandler?.Invoke(this, changed);
            }
        }

        public virtual double Minimum { get; set; }
        public virtual double Maximum { get; set; }
        public virtual double Step { get; set; } = 1d;
        public bool ProtocolEditable { get; set; } = true;
        public bool ReadOnly { get; set; }
        /// <summary>
        /// UI does not show this control is set to true.
        /// </summary>
        public bool Hidden { get; set; }
        /// <summary>
        /// Means, that changing the value of this parameter, other parameters in settings are affected
        /// </summary>
        public bool AffectOthers { get; set; }        
        public virtual bool LowerMin { get; }
        public virtual bool UpperMax { get; }
        /// <summary>
        /// Checks if value is within lower and upper limit
        /// </summary>
        public override bool IsValid
        {
            get
            {
                ErrorMessage.Clear();
                if (!IsUsed || !IsEnabled || Hidden) return true;
                var retValue = (!UpperMax && !LowerMin) || IsLegalValue;
                if (retValue)
                    ErrorMessage.Clear();
                return retValue;
            }
        }

        /// <summary>
        /// Checks if value is within LegalValue collection
        /// </summary>
        protected virtual bool IsLegalValue
        {
            get
            {
                var retValue = LegalValues?.Contains(GetTypedValueOrObject()) ?? false;
                if (!retValue && LegalValues?.Count > 0 && ErrorMessage.Count == 0)
                    ErrorMessage.Add($"{Resources.GetResource.GetString(Name)}: {Resources.GetResource.GetString("NotInLegalValues")}");
                return retValue;
            }
        }

	    protected virtual object GetTypedValueOrObject()
	    {
		    return Value;
	    }

        public ArrayList LegalValues { get; set; }
        public abstract Type GetValueType { get; }
        /// <summary>
        /// If true, this property is turned on by user and is used for measurement
        /// </summary>
        public bool IsUsed { get; set; } 
        /// <summary>
        /// If false, this property cannot be turned on or off by user and cannot be used for measurement
        /// </summary>
        public bool IsEnabled { get; set; }
        public string Units { get { return mUnits; } set { mUnits = value; } }
        public event EventHandler<GimliValueChangedEvent> ValueChangedEventHandler;

        public void Dispose()
        {
        }
        /// <summary>
        /// Converts object value to int value
        /// </summary>
        /// <returns></returns>
        public int ToInt()
        {
            var iValue = Convert<int>();
            if (Math.Abs(iValue) == 0)
                int.TryParse(Value.ToString(), out iValue);
            return iValue;
        }
        /// <summary>
        /// Converts object value to long value
        /// </summary>
        /// <returns></returns>
        public long ToLong()
        {
            var lValue = Convert<long>();
            if (Math.Abs(lValue) == 0)
                long.TryParse(Value.ToString(), out lValue);
            return lValue;
        }
        /// <summary>
        /// Converts object value to double value
        /// </summary>
        /// <returns></returns>
        public double ToDouble()
        {
            var dValue = Convert<double>();
            if (Math.Abs(dValue) <= double.Epsilon)
                double.TryParse(Value.ToString(), out dValue);
            return dValue;
        }
        /// <summary>
        /// Converts object value to TimeSpan value
        /// </summary>
        /// <returns></returns>
        public TimeSpan ToTimeSpan()
        {
            return Convert<TimeSpan>();
        }
        /// <summary>
        /// Converts object value to bool value
        /// </summary>
        /// <returns></returns>
        public bool ToBool()
        {
            return Convert<bool>();
        }

        public abstract ItemClass GetSummary(object format = null);
        private T Convert<T>()
        {
            if (typeof(T) == Value.GetType()) return (T)Value;

            var tc = TypeDescriptor.GetConverter(Value.GetType());
            if (tc.CanConvertTo(typeof (T)))
                return (T) tc.ConvertTo(Value, typeof (T));
            return default(T);
        }

	    public void SetToUsed(bool yesNo)
	    {
			if (yesNo)
			{
				IsUsed = true;
				IsEnabled = true;
				Hidden = false;
				IsDisplayedInSummary = true;
			}
			else
			{
				IsUsed = false;
				IsEnabled = false;
				Hidden = true;
				IsDisplayedInSummary = false;
			}
		}
    }
}
