﻿using System;
using System.Globalization;
using Gimli.JsonObjects.Settings;
using Newtonsoft.Json;

namespace Gimli.JsonObjects.Parameter
{
    [Serializable]
    public class GimliParameterInt :GimliParameter
    {
        private int mMinimum;
        private int mMaximum;
        private int mStep;

        /// <summary>
        /// Default constructor for serialization only
        /// </summary>
        [JsonConstructor]
        public GimliParameterInt() { }
        internal GimliParameterInt(string name, int value) : base(name)
        {
            Value = value;
        }
        public GimliParameterInt(GimliParameterInt originalInt) : base(originalInt)
        { }

        public override Type GetValueType => typeof(int);
        public override double Minimum { get { return mMinimum; } set { mMinimum = (int)Convert.ChangeType(value, typeof(int), CultureInfo.InvariantCulture); } }
        public override double Maximum { get { return mMaximum; } set { mMaximum = (int)Convert.ChangeType(value, typeof(int), CultureInfo.InvariantCulture); } }
        public override double Step { get { return mStep; } set { mStep =  (int) Convert.ChangeType(value, typeof (int), CultureInfo.InvariantCulture); } }

        public override bool LowerMin
        {
            get
            {
	            var stringValue = Value?.ToString();
				var isLowerMin = string.IsNullOrEmpty(stringValue) || mMinimum > Convert.ToInt32(Value);
                if (isLowerMin)
                    ErrorMessage.Add($"{Resources.GetResource.GetString(Name)}: {Resources.GetResource.GetString("LowerMin")} ({(string.IsNullOrEmpty(stringValue) ? "<no Value>" : stringValue)}{Units} < {Minimum}{Units})");
                return isLowerMin;
            }
        }

        public override bool UpperMax
        {
            get
            {
	            var stringValue = Value?.ToString();
                var isUpperMax = string.IsNullOrEmpty(stringValue) || mMaximum < Convert.ToInt32(Value);
                if (isUpperMax)
                    ErrorMessage.Add($"{Resources.GetResource.GetString(Name)}: {Resources.GetResource.GetString("UpperMax")} ({(string.IsNullOrEmpty(stringValue) ? "<no Value>" : stringValue)}{Units} < {Maximum}{Units})");
                return isUpperMax;
            }
        }

        public GimliParameterInt Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as GimliParameterInt;
        }

        public override ItemClass GetSummary(object decimalDigits = null)
        {
            if (!IsEnabled || !IsUsed || !IsDisplayedInSummary) return null;
            var val = Value.ToString();
            if (decimalDigits is int)
            {
                val = string.Format("{0}", Math.Round(ToDouble(), (int) decimalDigits));
            }
            return new ItemClass(Resources.GetResource.GetString(Name),
                val + (string.IsNullOrEmpty(Units) ? "" : " " + Units));
        }
    }
}
