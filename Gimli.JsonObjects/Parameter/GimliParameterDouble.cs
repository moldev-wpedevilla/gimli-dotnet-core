﻿using Gimli.JsonObjects.Settings;
using Newtonsoft.Json;
using System;

namespace Gimli.JsonObjects.Parameter
{
    [Serializable]
    public class GimliParameterDouble : GimliParameter
    {
        private double mMinimum;
        private double mMaximum;
        private double mStep;

        /// <summary>
        /// Default constructor for serialization only
        /// </summary>
        [JsonConstructor]
        public GimliParameterDouble() { }
        internal GimliParameterDouble(string name, double value) : base(name)
        {
            Value = value;
        }
        public GimliParameterDouble(GimliParameterDouble originalDouble) : base(originalDouble)
        { }
        public override Type GetValueType => typeof(double);

        public override double Minimum { get { return mMinimum; } set { mMinimum = value; } }
        public override double Maximum { get { return mMaximum; } set { mMaximum = value; } }
        public override double Step { get { return mStep; } set { mStep = value; } }
        public override bool LowerMin
        {
            get
            {
                var retValue = mMinimum > Convert.ToDouble(Value);
                if (retValue)
                    ErrorMessage.Add($"{Resources.GetResource.GetString(Name)}: {Resources.GetResource.GetString("LowerMin")} ({Value} < {Minimum})");
                return retValue;
            }
        }

        public override bool UpperMax
        {
            get
            {
                var retValue = mMaximum < Convert.ToDouble(Value);
                if (retValue)
                    ErrorMessage.Add($"{Resources.GetResource.GetString(Name)}: {Resources.GetResource.GetString("UpperMax")} ({Value} < {Maximum})");
                return retValue;
            }
        }
        public GimliParameterDouble Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as GimliParameterDouble;
        }

        public override ItemClass GetSummary(object decimalDigits = null)
        {
            if (!IsEnabled || !IsUsed || !IsDisplayedInSummary) return null;
            var val = Value.ToString();
            if (decimalDigits is int)
            {
                val = $"{Math.Round(ToDouble(), (int) decimalDigits)}";
            }
            return new ItemClass(Resources.GetResource.GetString(Name), val + (string.IsNullOrEmpty(Units)? "":" "+Units));
        }
    }
}
