﻿using System;
using Gimli.JsonObjects.Settings;
using System.Collections.Generic;

namespace Gimli.JsonObjects.Parameter
{
    internal static class GimliSummary
    {
        internal static void AddItToList(GimliParameter parameter, ICollection<ItemClass> list)
        {
            var item = parameter.GetSummary();
            if (item == null) return;
            list.Add(item);
        }

        internal static void AddItToList(ItemClass item, ICollection<ItemClass> list)
        {
            if (item == null) return;
            list.Add(item);
        }

        internal static void AddItToListAsTime(GimliParameter parameter, ICollection<ItemClass> list)
        {
            var item = parameter.GetSummary();
            if (item == null) return;

            var timeSpan = new TimeSpan((long)(parameter.ToDouble() * 10000000d));

            item.ItemValue = string.Format("{0,2:mm}:{0,2:ss}", timeSpan);
            if (timeSpan.Milliseconds > 0)
                item.ItemValue = $"{item.ItemValue}.{timeSpan,3:fff}";
            if (timeSpan.Hours > 0)
                item.ItemValue = $"{timeSpan,2:hh}:{item.ItemValue,2}";
            if (timeSpan.Days > 0)
                item.ItemValue = $"{timeSpan:dd} Day(s), {item.ItemValue,2}";
            list.Add(item);
        }
    }
}
