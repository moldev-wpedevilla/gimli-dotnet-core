using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class PlateWellRange : PlateWellBase
	{
		public Point Point1 { get; set; }
		public Point Point2 { get; set; }
	}
}