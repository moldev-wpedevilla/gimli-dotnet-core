﻿using System;
using System.Collections.Generic;

namespace Gimli.JsonObjects
{
    [Serializable]
    public class SupportedMicroplate
    {
        public List<string> AllPlates { get; set; }
        public List<string> Absorbance { get; set; }
        public string PlateType { get; set; }
    }
}
