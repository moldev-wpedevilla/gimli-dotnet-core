﻿using System;
using Gimli.JsonObjects.Parameter;
using System.Collections.Generic;
using Gimli.JsonObjects.FiltersMngmnt;

namespace Gimli.JsonObjects
{
    public class DataDimensionHelper
    {
        private Snapshot mSnapshot;
        private DataDimension[] mDimension;
	    private List<double> mGraphXValueList;

		public DataDimension[] CreateDataDimensionInfo(Snapshot snapshot, List<double> kineticXValueList)
        {
			if (snapshot.Settings == null) return null;
			mGraphXValueList = kineticXValueList;

            mSnapshot = snapshot;
            return SetDataDimensionInfo();
        }

        private DataDimension[] SetDataDimensionInfo()
        {
            mDimension = new DataDimension[3];
            mDimension[0] = new DataDimension(NameOfFirstDataDimension(), 1);
            mDimension[1] = new DataDimension(NameOfSecondDataDimension(), 2);
            mDimension[2] = new DataDimension(NameOfThirdDataDimension(), 3);

            if (mSnapshot.Settings == null || mSnapshot.Settings.Wavelengths?.NrOfWavelengths == null)
                return mDimension;

            if (mSnapshot.Settings.MeasurementType == MeasurementType.SpectrumScan)
            {
                FillSpectrumDimensionInfo();
            }
            else
            {
                if (mSnapshot.Settings.MeasurementMode == Mode.Abs)
                    return FillDataDimensionInfo(mSnapshot.Settings.Wavelengths.Excitation);
				FillDataDimensionInfo(mSnapshot.Settings.Wavelengths.Emission, mSnapshot.Settings.MeasurementMode == Mode.Fp);
			}

            //FillMultiDataDimensionInfo();
            return mDimension;
        }

        private void FillSpectrumDimensionInfo()
        {
            mDimension[0].DataPointLabel = new List<double> {0};
            mDimension[1].DataPointLabel = new List<double> ();
            if (mSnapshot.Settings.Wavelengths.ExcitationSweep.ToBool())
                mDimension[1].DataPointLabel.AddRange(GetSpectralExcitationWavelengths());
            else
                mDimension[1].DataPointLabel.Add(mSnapshot.Settings.Wavelengths.EmissionStart.ToInt());
            mDimension[2].DataPointLabel = new List<double> ();
            if (!mSnapshot.Settings.Wavelengths.ExcitationSweep.ToBool())
                mDimension[2].DataPointLabel.AddRange(GetSpectralEmissionWavelengths());
            else
                mDimension[2].DataPointLabel.Add(mSnapshot.Settings.Wavelengths.ExcitationStart.ToInt());
        }

        private DataDimension[] FillDataDimensionInfo(List<GimliParameterWavelength> filters, bool isFp = false)
        {
            mDimension[0].DataPointLabel = new List<double>();
            mDimension[1].DataPointLabel = new List<double>();
            mDimension[2].DataPointLabel = new List<double>();

            switch (mSnapshot.Settings.MeasurementType)
            {
                default:
					SetWavelengthDimension(filters, isFp);
					mDimension[2].DataPointLabel.Add(0);
					mDimension[2].DimensionName = "SinglePoint";
					break;
                case MeasurementType.Kinetic:
					SetWavelengthDimension(filters, isFp);
                    mDimension[2].DataPointLabel.AddRange(GetKineticData());
                    mDimension[2].DimensionName = "Seconds";
                    break;
                case MeasurementType.AreaScan:
                    mDimension[0].DataPointLabel.AddRange(AddFilterWavelength(filters));
                    mDimension[1].DataPointLabel.AddRange(GetScanPointsY());
                    mDimension[1].DimensionName = "ScanPoint";
                    mDimension[2].DataPointLabel.AddRange(GetScanPointsX());
                    mDimension[2].DimensionName = "ScanPoint";
                    break;
            }
            return mDimension;
        }

	    private void SetWavelengthDimension(List<GimliParameterWavelength> filters, bool isFp)
	    {
		    if (isFp)
		    {
				mDimension[0].DataPointLabel.AddRange(AddFilterWavelength(filters));
			    mDimension[1].DataPointLabel.Add(0);
			    mDimension[1].DataPointLabel.Add(0);
			    mDimension[1].DimensionName = "vert/horiz";
			}
		    else
		    {
			    mDimension[0].DataPointLabel.Add(0);
				mDimension[0].DimensionName = "";
				mDimension[1].DataPointLabel.AddRange(AddFilterWavelength(filters));
				mDimension[1].DimensionName = "Wavelengths";
			}
			
		}

	    private ICollection<double> AddFilterWavelength(List<GimliParameterWavelength> filters)
        {
            var wlList = new List<double>();
            foreach (var filter in filters)
            {
	            if (filter.IsUsed)
	            {
		            if (filter.UseMonocromator) wlList.Add(filter.ToDouble());
		            else
		            {
			            var filterString = filter.Value.ToString();
						if (filterString.Contains(TransformedFilterInfo.FilterSeperator))
						{
							var splitParts = filterString.Split(new[] {TransformedFilterInfo.FilterSeperator}, StringSplitOptions.None);
							double wavelength;
							double.TryParse(splitParts[0], out wavelength);
							wlList.Add(wavelength);
						}
						else
						{
							wlList.Add(filter.ToDouble());
						}
		            }
				}
			}
            return wlList;
        }

        private IEnumerable<double> GetScanPointsX()
        {
            if (mSnapshot.MaxXPos < 1)
                return new List<double>() { 1, 2, 3, 4, 5 };

            var list = new List<double>();
            for (var i = mSnapshot.MinXPos; i <= mSnapshot.MaxXPos; i += mSnapshot.IncrementX)
                list.Add(i);
            return list;
        }

        public IEnumerable<double> GetScanPointsY()
        {
            if (mSnapshot.MaxYPos < 1)
                return new List<double>() { 1, 2, 3, 4, 5 };
            var list = new List<double>();
            for (var i = mSnapshot.MinYPos; i <= mSnapshot.MaxYPos; i += mSnapshot.IncrementY)
                list.Add(i);
            return list;
        }
        private IEnumerable<double> GetKineticData()
        {
	        if (mGraphXValueList != null) return mGraphXValueList;
            var list = new List<double>();
            try
            {
                var kinData = mSnapshot.GetFirstUsedWell() != null ? mSnapshot.GetFirstUsedWell().CycleTime : null;
                if (kinData != null)
                {
                    for (var i = 0; i < kinData.Length; i++)
                    {
                        if (i>0 && kinData[i] < 0.01)
                            list.Add(list[list.Count-1] + mSnapshot.Settings.Kinetic.Intervall.ToDouble());
                        else
                            list.Add(kinData[i]);
                    }
                }
                else
                {
                    for (var i = 0; i < mSnapshot.Settings.Kinetic.Cycles.ToInt(); i++)
                    {
                        list.Add(i);
                    }
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Trace.WriteLine(exc.Message);
            }
            return list;
        }

        private List<double> GetSpectralEmissionWavelengths()
        {
	        if (mGraphXValueList != null) return mGraphXValueList;
            if (mSnapshot.Settings.MeasurementMode == Mode.Lum)
                return GetEmissionSpectralWavelengths();
            return GetFlSpectralWavelengths(false);
        }

        private List<double> GetSpectralExcitationWavelengths()
        {
	        if (mGraphXValueList != null) return mGraphXValueList;
            if (mSnapshot.Settings.MeasurementMode == Mode.Abs)
				return GetExcitationSpectralWavelengths();
            return GetFlSpectralWavelengths(true);
        }

        private List<double> GetFlSpectralWavelengths(bool excitationSweep)
        {
            if (excitationSweep)
                return GetExcitationSpectralWavelengths();
            return GetEmissionSpectralWavelengths();
        }

        private List<double> GetExcitationSpectralWavelengths()
        {
            var list = new List<double>();

            for (var i = mSnapshot.Settings.Wavelengths.ExcitationStart.ToInt();
                i <= mSnapshot.Settings.Wavelengths.ExcitationEnd.ToInt();
                i += mSnapshot.Settings.Wavelengths.ExcitationStep.ToInt())
            {
                list.Add(i);
            }
            return list;
        }

        private List<double> GetEmissionSpectralWavelengths()
        {
            var list = new List<double>();

            for (var i = mSnapshot.Settings.Wavelengths.EmissionStart.ToInt();
                i <= mSnapshot.Settings.Wavelengths.EmissionEnd.ToInt();
                i += mSnapshot.Settings.Wavelengths.EmissionStep.ToInt())
            {
                list.Add(i);
            }
            return list;
        }

        private string NameOfFirstDataDimension()
        {
            if (mSnapshot.Settings.MeasurementType == MeasurementType.AreaScan) return "Wavelength";
            return "Data";
        }
        private string NameOfSecondDataDimension()
        {
            if (mSnapshot.Settings.MeasurementType == MeasurementType.AreaScan) return "Scanpoints_Y";
            if (mSnapshot.Settings.MeasurementType == MeasurementType.Endpoint) return "Data";
            if (mSnapshot.Settings.MeasurementType == MeasurementType.SpectrumScan) return "Data";
            return "Wavelength";
        }

        private string NameOfThirdDataDimension()
        {
            if (mSnapshot.Settings.MeasurementType == MeasurementType.AreaScan) return "Scanpoints_X";
            if (mSnapshot.Settings.MeasurementType == MeasurementType.Kinetic) return "Cycles";
            if (mSnapshot.Settings.MeasurementType == MeasurementType.SpectrumScan) return "Wavelength";
            return "Data";
        }
    }
}
