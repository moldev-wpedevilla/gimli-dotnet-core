﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Gimli.JsonObjects.InjectorMaintenance;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Gimli.JsonObjects
{
	public static class SerializeObjectExtension
	{
		public static string XmlSerializeToString(this object objectInstance)
		{
			var serializer = new XmlSerializer(objectInstance.GetType());
			var sb = new StringBuilder();

			using (TextWriter writer = new StringWriter(sb, CultureInfo.InvariantCulture))
			{
				serializer.Serialize(writer, objectInstance);
			}

			return sb.ToString();
		}

		public static string JsonSerializeToCamelCase(this Document objectInstance, Action<string> errorReporter)
		{
			var serializationSucceeded = false;
			var maxNumberOfRetries = 10;
			var tryNumber = 0;
			var response = string.Empty;
			while (!serializationSucceeded && tryNumber < maxNumberOfRetries)
			{
				try
				{
					tryNumber++;
					response = objectInstance.JsonSerializeToCamelCase();
					serializationSucceeded = true;
				}
				catch (Exception)
				{
					errorReporter($"Try #{tryNumber}: Catched serialization exception for document: {objectInstance.Id} ({objectInstance.DocumentName}). Trying again.");
				}
			}

			return response;
		}

        public static string JsonSerializeToCamelCase(this InjectorAction objectInstance, Action<string> errorReporter)
        {
            var serializationSucceeded = false;
            var maxNumberOfRetries = 10;
            var tryNumber = 0;
            var response = string.Empty;
            while (!serializationSucceeded && tryNumber < maxNumberOfRetries)
            {
                try
                {
                    tryNumber++;
                    response = objectInstance.JsonSerializeToCamelCase();
                    serializationSucceeded = true;
                }
                catch (Exception)
                {
                    errorReporter($"Try #{tryNumber}: Catched serialization exception for injector action: {nameof(objectInstance.Mode)}. Trying again.");
                }
            }

            return response;
        }

        public static string JsonSerializeToCamelCase(this object objectInstance)
		{
			var jsonSerializer = new JsonSerializer
			{
				ContractResolver = new CamelCasePropertyNamesContractResolver()
			};
			jsonSerializer.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
			var sb = new StringBuilder();

			using (TextWriter writer = new StringWriter(sb, CultureInfo.InvariantCulture))
			{
				jsonSerializer.Serialize(writer, objectInstance);
			}

			return sb.ToString();
		}

		public static string JsonSerializeToClassRepresentation(this object objectInstance)
		{
			return JsonConvert.SerializeObject(objectInstance, Formatting.None, new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.Objects,
				TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
			});
		}

		public static T JsonDeserializeFromString<T>(this string objectData)
		{
			return JsonConvert.DeserializeObject<T>(objectData,
				new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Objects });
		}
	}
}
