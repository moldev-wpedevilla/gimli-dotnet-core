﻿using Gimli.JsonObjects.Enumarations.Status;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Gimli.JsonObjects.Serialize
{
    public class RawBinaryWriter
    {
        public static byte[] Serialize(List<Well> data, out long size)
        {
            size = 0;
            return data == null ? null : DoIt(data, out size);
        }
        private static byte[] DoIt(List<Well> data, out long size)
        {
            size = 0;
            var start = DateTime.Now;
            var ms = new MemoryStream();
            BinaryWriter writer=null;
            try
            {
                writer = new BinaryWriter(ms);
                writer.Write(data.Count);
                foreach (var well in data)
                {
                    well?.Serialize(writer);
                }
                size = ms.Length;
                Trace.WriteLine(string.Format("Trace: {0,-50}: {1,7} ms, {2} Bytes", "Serializing Document", DateTime.Now.Subtract(start).TotalMilliseconds.ToString("N0"), ms.Length.ToString("N0")));

                var zipped = DocumentSerialization.ZipStream(ms);
                return zipped?.ToArray();
                //return ms.ToArray();
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Document::RawSerialization: " + exc.Message);
                return null;
            }
            finally
            {
                ms.Close();
                writer?.Dispose();
				DocumentSerialization.TraceOutputDeltaTime(data, start, "Serialization raw");
            }
        }

        public static void SerializeOneDimensionArray(BinaryWriter writer, DataPointStatus[] data)
        {
			SerializeOneDimensionArray(writer, data, y => writer.Write((int)data[y]));
        }
        public static void SerializeOneDimensionArray(BinaryWriter writer, double[] data)
        {
			SerializeOneDimensionArray(writer, data, y =>writer.Write(data[y]));
        }

		public static void SerializeOneDimensionArray<T>(BinaryWriter writer, T[] data, Action<int> writerAction)
		{
			writer.Write(data?.GetLength(0) ?? 0);

			if (data == null) return;

			var yCount = data.GetLength(0);

			for (var y = 0; y < yCount; y++)
				writerAction(y);
		}

		public static void SerializeTwoDimensionArray(BinaryWriter writer, double[,] data)
        {
            writer.Write(data?.GetLength(0) ?? 0);
            writer.Write(data?.GetLength(1) ?? 0);

            if (data == null) return;
            for (var y = 0; y < data.GetLength(0); y++)
                for (var x = 0; x < data.GetLength(1); x++)
                    writer.Write(data[y, x]);
        }

        public static void SerializeThreeDimensionArray(BinaryWriter writer, double[,,] data)
        {
            writer.Write(data?.GetLength(0) ?? 0);
            writer.Write(data?.GetLength(1) ?? 0);
            writer.Write(data?.GetLength(2) ?? 0);

            if (data == null) return;

            var yCount = data.GetLength(0);
            var xCount = data.GetLength(1);
            var zCount = data.GetLength(2);

            GetValue(yCount, xCount, zCount, (x, y, z) => writer.Write(data[y, x, z]));
        }

	    private static void GetValue(int yCount, int xCount, int zCount, Action<int, int, int> writerAction)
	    {
		    for (var y = 0; y < yCount; y++)
			    for (var x = 0; x < xCount; x++)
				    for (var z = 0; z < zCount; z++)
					    writerAction(x, y, z);
	    }

	    public static void SerializeThreeDimensionArray(BinaryWriter writer, DataPointStatus[,,] data)
        {
            var yCount = data?.GetLength(0) ?? 0;
            var xCount = data?.GetLength(1) ?? 0;
            var zCount = data?.GetLength(2) ?? 0;

            writer.Write(yCount);
            writer.Write(xCount);
            writer.Write(zCount);

            if (data == null) return;

			GetValue(yCount, xCount, zCount, (x, y, z) => writer.Write((int)data[y, x, z]));
        }

        public static void SerializeString(BinaryWriter writer, string data)
        {
            writer.Write(data ?? string.Empty);
        }

        //public static void SerializeParameter(BinaryWriter writer, List<GimliParameterWavelength> list)
        //{
        //    writer.Write(list?.Count ?? 0);
        //    if (list == null) return;

        //    foreach (var parameter in list)
        //    {
        //        SerializeParameter(writer, parameter);
        //    }
        //}

        //public static void SerializeParameter(BinaryWriter writer, GimliParameter parameter)
        //{
        //    writer.Write(parameter != null);
        //    if (parameter == null) return;

        //    writer.Write(parameter.GetType().ToString());
        //    if (parameter is GimliParameterBool)
        //        SerializeParameterMain(writer, (GimliParameterBool) parameter);
        //    if (parameter is GimliParameterInt)
        //        SerializeParameterMain(writer, (GimliParameterInt) parameter);
        //    if (parameter is GimliParameterDouble)
        //        SerializeParameterMain(writer, (GimliParameterDouble)parameter);
        //    if (parameter is GimliParameterTimeSpan)
        //        SerializeParameterMain(writer, (GimliParameterTimeSpan)parameter);
        //    if (parameter is GimliParameterString)
        //        SerializeParameterMain(writer, (GimliParameterString)parameter);
        //    if (parameter is GimliParameterWavelength)
        //        SerializeParameterMain(writer, (GimliParameterWavelength)parameter);
        //}
        
        //private static void SerializeParameterMain(BinaryWriter writer, GimliParameterBool parameter)
        //{
        //    writer.Write(parameter != null);
        //    if (parameter == null) return;

        //    writer.Write(parameter.ToBool());
        //    SerializeParameterBase(writer, parameter);
        //}

        //private static void SerializeParameterMain(BinaryWriter writer, GimliParameterInt parameter)
        //{
        //    writer.Write(parameter != null);
        //    if (parameter == null) return;

        //    writer.Write(parameter.ToInt());
        //    SerializeParameterBase(writer, parameter);
        //}

        //private static void SerializeParameterMain(BinaryWriter writer, GimliParameterDouble parameter)
        //{
        //    writer.Write(parameter != null);
        //    if (parameter == null) return;

        //    writer.Write(parameter.ToDouble());
        //    SerializeParameterBase(writer, parameter);
        //}
        //private static void SerializeParameterMain(BinaryWriter writer, GimliParameterString parameter)
        //{
        //    writer.Write(parameter != null);
        //    if (parameter == null) return;

        //    writer.Write(parameter.ToString());
        //    SerializeParameterBase(writer, parameter);
        //}
        //private static void SerializeParameterMain(BinaryWriter writer, GimliParameterTimeSpan parameter)
        //{
        //    writer.Write(parameter != null);
        //    if (parameter == null) return;

        //    writer.Write(parameter.ToInt());
        //    SerializeParameterBase(writer, parameter);
        //}
        //private static void SerializeParameterMain(BinaryWriter writer, GimliParameterWavelength parameter)
        //{
        //    writer.Write(parameter != null);
        //    if (parameter == null) return;

        //    writer.Write(parameter.ToInt());
        //    writer.Write(parameter.UseMonocromator);
        //    SerializeParameterBase(writer, parameter);
        //}
        //public static void SerializeParameterBase(BinaryWriter writer, GimliParameter parameter)
        //{
        //    writer.Write(parameter.LegalValues != null);
        //    SerializeOneDimensionArray(writer, parameter.LegalValues);
        //    writer.Write(parameter.IsEnabled);
        //    writer.Write(parameter.IsUsed);
        //    writer.Write(parameter.Hidden);
        //    writer.Write(parameter.AffectOthers);
        //    writer.Write(parameter.Description);
        //    writer.Write(parameter.Name);
        //    writer.Write(parameter.IsDisplayedInSummary);
        //    writer.Write(parameter.IsModified);
        //    writer.Write(parameter.Units);
        //    writer.Write(parameter.Minimum);
        //    writer.Write(parameter.Maximum);
        //    writer.Write(parameter.Step);
        //    writer.Write(parameter.ProtocolEditable);
        //    writer.Write(parameter.ReadOnly);
        //    foreach (var child in parameter.Children)
        //    {
        //        if (child.GetType() == typeof(GimliParameterBool))
        //            SerializeParameterMain(writer, child as GimliParameterBool);
        //        if (child.GetType() == typeof(GimliParameterInt))
        //            SerializeParameterMain(writer, child as GimliParameterInt);
        //        if (child.GetType() == typeof(GimliParameterDouble))
        //            SerializeParameterMain(writer, child as GimliParameterDouble);
        //        if (child.GetType() == typeof(GimliParameterTimeSpan))
        //            SerializeParameterMain(writer, child as GimliParameterTimeSpan);
        //        if (child.GetType() == typeof(GimliParameterString))
        //            SerializeParameterMain(writer, child as GimliParameterString);
        //        if (child.GetType() == typeof(GimliParameterWavelength))
        //            SerializeParameterMain(writer, child as GimliParameterWavelength);
        //    }
        //}
    }
}
