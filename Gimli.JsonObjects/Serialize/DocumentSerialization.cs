﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace Gimli.JsonObjects.Serialize
{
    public class DocumentSerialization
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "1#")]
        public static byte[] SerializeAndZip(object data, out long size)
        {
            size = 0;
            return data == null ? null : DoIt(data, out size);
        }

        public static byte[] Serialize(object data)
        {
            long size;
            return data == null ? null : DoIt(data, out size, false);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "1#")]
        internal static byte[] DoIt(object data, out long size, bool zipIt=true)
        {
            size = 0;
            var start = DateTime.Now;
            var ms = new MemoryStream();
            try
            {
                new BinaryFormatter().Serialize(ms, data);
                size = ms.Length;

                //Trace.WriteLine($"Serializing Document    - Time: {DateTime.Now.Subtract(start).TotalMilliseconds.ToString("N0")} ms Size: {ms.Length.ToString("N0")} Bytes");

                //Microplate, ReadArea and Settings are not zipped on serialization
                if (!zipIt)
                    return ms.ToArray();

                var zipped = ZipStream(ms);
                return zipped?.ToArray();
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Document::Serialization: " + exc.Message);
                return null;
            }
            finally
            {
	            ms.Close();
	            TraceOutputDeltaTime(data, start, "Serialization pur");
            }
        }

	    internal static void TraceOutputDeltaTime(object data, DateTime start, string actionType)
	    {
		    var time = DateTime.Now.Subtract(start).TotalMilliseconds;
		    if (time > 20)
		    {
			    Trace.WriteLine(string.Format("Trace: {0,-50}: {1,7} ms, Type: {2} ", actionType,
				    time.ToString("N0"),
				    data));
		    }
	    }

	    public static byte[] ZipStream(MemoryStream unzippedStram)
        {
            //var start = DateTime.Now;
            var compms = new MemoryStream();

            try
            {
                var zip = new GZipStream(compms, CompressionMode.Compress, true);
                zip.Write(unzippedStram.ToArray(), 0, (int)unzippedStram.Length);
                zip.Close();

                //var tsread2 = DateTime.Now.Subtract(start);
                //Trace.WriteLine($"Compressing Document    - Time: {tsread2.TotalMilliseconds.ToString("N0")} ms Size: {unzippedStram.Length.ToString("N0")} -> {compms.Length.ToString("N0")} Bytes - Saved: {((unzippedStram.Length - compms.Length) / 1024).ToString("N1")} KB ({(1.00 - compms.Length / (double)unzippedStram.Length).ToString("0%")})");

                return compms.ToArray();
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Exception Zipping stream: " +exc.Message);
                return null;
            }
            finally
            {
                //Trace.WriteLine($"Compressing Completed   - Time: {DateTime.Now.Subtract(start).TotalMilliseconds.ToString("N0")} ms");
                compms.Close();
                compms.Dispose();
            }
        }
    }
}
