﻿using System;
using System.Collections.Generic;
using Gimli.JsonObjects.Deserialize;

namespace Gimli.JsonObjects.Serialize
{
	[Serializable]
	public class DocumentAndSerializedWells
	{
		public Document Document { get; set; }
		public Dictionary<string, long> WellSeriailizationSize { get; set; } = new Dictionary<string, long>();
		public Dictionary<string, byte[]> WellSeriailization { get; set; } = new Dictionary<string, byte[]>();

		public static DocumentAndSerializedWells SerializeDocument(Document document)
		{
			var returnObject = new DocumentAndSerializedWells();

			if (document != null)
			{
				foreach (var experiment in document.Experiments)
				{
					foreach (var plate in experiment.Plates)
					{
						foreach (var snapshot in plate.SnapShot)
						{
							long size;
							var wellData = snapshot.Wells;
							returnObject.WellSeriailization[Document.GetDictionaryKey(experiment.Id, plate.Id, snapshot.Id)] =
								RawBinaryWriter.Serialize(wellData, out size);
							returnObject.WellSeriailizationSize[Document.GetDictionaryKey(experiment.Id, plate.Id, snapshot.Id)] = size;
						}
					}
				}

				returnObject.Document = document.CloneToEmpty();
			}

			return returnObject;
		}

		// used in offboard solution
		public static Document DeserializeDocument(DocumentAndSerializedWells serializedDocument)
		{
			var returnObject = serializedDocument.Document.Clone();

			foreach (var experiment in returnObject.Experiments)
			{
				foreach (var plate in experiment.Plates)
				{
					foreach (var snapshot in plate.SnapShot)
					{
						var key = Document.GetDictionaryKey(experiment.Id, plate.Id, snapshot.Id);
						snapshot.Wells = RawBinaryReader.DeserializeWells(
							serializedDocument.WellSeriailization[key],
							serializedDocument.WellSeriailizationSize[key]);
					}
				}
			}

			return returnObject;
		}

		public string GetDictionaryKeyOfFirstSnapshot()
		{
			if (Document == null) return string.Empty;
			return Document.GetDictionaryKey(Document.Experiments[0].Id, Document.Experiments[0].Plates[0].Id,
				Document.Experiments[0].Plates[0].SnapShot[0].Id);
		}
	}
}
