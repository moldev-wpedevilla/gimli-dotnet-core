using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class ShakeSettingsSetInfo
	{
		public string Format { get; set; }
		public ShakeMode Mode { get; set; }
		public ShakeIntensity Intensity { get; set; }
		public bool Activate { get; set; }
	}
}