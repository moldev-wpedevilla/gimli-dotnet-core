﻿using System;
using System.Collections.Generic;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class ReadModeTypeCombination
	{
		public Mode Name { get; set; }
		public List<MeasurementType> Types { get; set; }
	}
}
