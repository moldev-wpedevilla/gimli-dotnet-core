using System.Linq;

namespace Gimli.JsonObjects
{
	public enum Mode
	{
		Abs,
        Fl,
        Lum,
        Trf,
		Fp,
		None,
	}

	public static class ReadModeExtensions
	{
		private static readonly Mode[] FluoroModeGroup = { Mode.Fp, Mode.Fl, Mode.Trf };

		public static bool IsModeInFluoroGroup(this Mode mode)
		{
			return FluoroModeGroup.Contains(mode);
		}
	}
}