﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class TemperatureSettings
	{
		public double Target { get; set; }
		public bool ControllerRunning { get; set; }
	}
}
