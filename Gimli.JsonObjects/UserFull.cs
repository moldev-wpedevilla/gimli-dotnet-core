﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class UserFull : UserNfcPair
	{
        public bool HasNfc { get; set; }
        public Guid Id { get; set; }
        public bool HasPin { get; set; }
	    public bool IsDefaultPin { get; set; }
	}
}
