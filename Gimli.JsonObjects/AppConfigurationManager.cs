﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Linq;

namespace Gimli.JsonObjects
{
	[XmlType(AnonymousType = true)]
	public class AppConfiguration
	{
		[XmlType(AnonymousType = true)]
		public class AddType
		{
			[XmlAttribute()]
			public string key { get; set; }
			[XmlAttribute()]
			public string value { get; set; }
		}

		[XmlArrayItem("add", typeof(AddType))]
		public List<AddType> appSettings { get; set; }
	}

	public static class AppConfigurationManager
	{
		// TODO someone: rewrite all of this: use appconfig.json project file instead of deprecated app.config xml file

		private static AppConfiguration mParseResult;
		public static void Build(string configFilePath)
		{
			XmlRootAttribute xRoot = new XmlRootAttribute();
			xRoot.IsNullable = true;
			xRoot.ElementName = "configuration";
			XmlSerializer xml = new XmlSerializer(typeof(AppConfiguration), xRoot);
			FileStream xmlStream = new FileStream(configFilePath, FileMode.Open, FileAccess.Read);
			mParseResult = ((AppConfiguration)xml.Deserialize(xmlStream));
		}

		public static string GetSettingsValue(string key)
		{
			return mParseResult.appSettings.FirstOrDefault(elem => elem.key == key).value;
		}
	}
}
