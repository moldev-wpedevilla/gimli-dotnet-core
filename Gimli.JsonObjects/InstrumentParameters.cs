﻿using System;
using Gimli.JsonObjects.FiltersMngmnt;
using Gimli.JsonObjects.InjectorMaintenance;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class InstrumentParameters
	{
		public TemperatureInstrumentParameters Temperature { get; set; }
		public ShakeSettings Shake { get; set; }
		public ReaderConfiguration ReaderConfiguration { get; set; }
		public ReadPreferences Preferences { get; set; }
		public InjectorsSetup InjectorConfiguration { get; set; }
        public FilterModuleConfig FilterConfiguration { get; set; }
	}
}
