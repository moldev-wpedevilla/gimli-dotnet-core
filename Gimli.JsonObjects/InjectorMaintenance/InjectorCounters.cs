﻿using System;

namespace Gimli.JsonObjects.InjectorMaintenance
{
	[Serializable]
	public class InjectorCounters
	{
		public InjectorSel Name { get; set; }
		public string Unit { get; set; }
		public DoubleValueWithMaximum TubingVolumeCounter { get; set; }
	}
}
