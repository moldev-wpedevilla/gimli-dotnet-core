﻿using System;
using Gimli.JsonObjects.Settings;

namespace Gimli.JsonObjects.InjectorMaintenance
{
	[Flags]
    public enum InjectorSel
    {
		None = 0,
        Injector1 = 1,
		Injector2 = 2
	}

	public enum InjectionMode
    {
		None,
        Prime,
		Rinse,
        RevPrime,
        Wash,

        LoadSettings,
        SaveSettings,

        LoadTimestamp,
        SaveTimestamp,

        ResetVolumeCounter,

        CalibWash,
        CalibDispensePlate,
        SetCalibFactor
    }

    public interface IInjectionAction
    {
         
    }


    [Serializable]
    public class InjectorAction
    {
        public InjectionMode Mode { get; set; }

        //public int Volume { get; set; }

        public IInjectionAction InjectionAction { get; set; }
    }

	public interface IInjectorSpecificAction
	{
        InjectorSel Injector { get; set; }
	}

	[Serializable]
    public class PrimeAction : IInjectionAction, IInjectorSpecificAction
	{
        public PrimeAction()
        {
            Injector = InjectorSel.Injector1;
        }

        public InjectorSel Injector { get; set; }
	}

    [Serializable]
    public class RinseAction : IInjectionAction, IInjectorSpecificAction
	{
        public InjectorSel Injector { get; set; }
	}

    [Serializable]
    public class RevPrimeAction : IInjectionAction, IInjectorSpecificAction
	{
        public InjectorSel Injector { get; set; }
	}

    [Serializable]
    public class WashAction : IInjectionAction, IInjectorSpecificAction
	{
        public InjectorSel Injector { get; set; }
		public int Volume;      // 1 ... 10.000 uL
        public int Step;        // ?!
	    public bool Reverse;
    }

    // ------------------------------------------------

    [Serializable]
    public class LoadSettings : IInjectionAction
    {
        
    }

    [Serializable]
    public class SaveSettings : IInjectionAction
    {
        public InjectorMaintenanceSettings Settings;
    }

    // ------------------------------------------------

    [Serializable]
    public class LoadTimestamp : IInjectionAction
    {
        
    }

    [Serializable]
    public class SaveTimestamp : IInjectionAction
    {

    }

    // ------------------------------------------------

    [Serializable]
    public class ResetVolumeCounterAction : IInjectionAction, IInjectorSpecificAction
	{
        public InjectorSel Injector { get; set; }
	}

    [Serializable]
    public class CalibWashAction : IInjectionAction, IInjectorSpecificAction
	{
        public InjectorSel Injector { get; set; }
    }

    [Serializable]
    public class DispensePlateAction : IInjectionAction, IInjectorSpecificAction
	{
		public InjectorSel Injector { get; set; }
	}

    [Serializable]
    public class SetCalibrationFactorAction : IInjectionAction, IInjectorSpecificAction
	{
		public InjectorSel Injector { get; set; }
		public double Weight1;
        public double Weight2;
    }
}
