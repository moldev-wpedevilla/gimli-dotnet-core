﻿using System;
using System.Collections.Generic;

namespace Gimli.JsonObjects.InjectorMaintenance
{
	[Serializable]
	public class InjectorsSetup
	{
		public List<InjectorConfiguration> Injectors { get; set; }
	}
}
