﻿using Gimli.JsonObjects.Parameter;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Gimli.JsonObjects.Deserialize;
using Gimli.JsonObjects.Serialize;

namespace Gimli.JsonObjects.Settings
{
    [Serializable]
    public class InjectorMaintenanceSettings : SettingsBase
    {
        private const int MinFluidVolume = 0; //1;
        private const int MaxFluidVolume = 10000;
        private const string Units = "µl";

        //private const int MinAbsorbance = 230;
        //private const int MaxAbsorbance = 1000;
        //private const int MinLuminescence = 300;
        //private const int MaxLuminescence = 850;
        //private const int MinEmissionFluorescence = 270;
        //private const int MaxEmissionFluorescence = 850;
        //private const int MinExcitationFluorescence = 250;
        //private const int MaxExcitationFluorescence = 830;
        //private const string Units = "nm";

        public static InjectorMaintenanceSettings Generate(/*Mode mode, MeasurementType type*/)
        {
            return new InjectorMaintenanceSettings(/*mode, type*/);
        }
        [JsonConstructor]
        //private InjectorMaintenanceSettings()
        //{
        //    Name = "InjectorMaintenanceSettings";
        //}
        private InjectorMaintenanceSettings(/*Mode mode, MeasurementType type*/)/*:this()*/
        {
            Name = "InjectorMaintenanceSettings";
            //Mode = mode;
            //MeasType = type;
            InitializeSettings(/*1*/);
        }
        public InjectorMaintenanceSettings(InjectorMaintenanceSettings originalSettings):base(originalSettings)
        {
            if (originalSettings == null) return;
            
            //IsUsed = originalSettings.IsUsed;
            //Mode = originalSettings.Mode;
            //MeasType = originalSettings.MeasType;

            //if (originalSettings.mAllWavelengths != null)
            //    mAllWavelengths = new GimliParameterBool(originalSettings.mAllWavelengths);

            //if (originalSettings.NrOfWavelengths != null)
            //    NrOfWavelengths = new GimliParameterInt(originalSettings.NrOfWavelengths);

            //Excitation = new List<GimliParameterWavelength>();
            //if (originalSettings.Excitation != null)
            //{
            //    foreach (var filter in originalSettings.Excitation)
            //    {
            //        Excitation.Add(new GimliParameterWavelength(filter));
            //    }
            //}

            //Emission = new List<GimliParameterWavelength>();
            //if (originalSettings.Emission != null)
            //{
            //    foreach (var filter in originalSettings.Emission)
            //    {
            //        Emission.Add(new GimliParameterWavelength(filter));
            //    }
            //}
            //if (originalSettings.Attenuation != null)
            //    Attenuation = new GimliParameterInt(originalSettings.Attenuation);
            //if (originalSettings.ExcitationStart != null)
            //    ExcitationStart = new GimliParameterWavelength(originalSettings.ExcitationStart);
            //if (originalSettings.ExcitationEnd != null)
            //    ExcitationEnd = new GimliParameterWavelength(originalSettings.ExcitationEnd);
            //if (originalSettings.EmissionStart != null)
            //    EmissionStart = new GimliParameterWavelength(originalSettings.EmissionStart);
            //if (originalSettings.EmissionEnd != null)
            //    EmissionEnd = new GimliParameterWavelength(originalSettings.EmissionEnd);
            //if (originalSettings.EmissionStep != null)
            //    EmissionStep = new GimliParameterInt(originalSettings.EmissionStep);
            //if (originalSettings.ExcitationStep != null)
            //    ExcitationStep = new GimliParameterInt(originalSettings.ExcitationStep);
            //if (originalSettings.PathCheck != null)
            //    PathCheck = new GimliParameterBool(originalSettings.PathCheck);
            //if (originalSettings.ExcitationSweep != null)
            //    ExcitationSweep = new GimliParameterBool(originalSettings.ExcitationSweep);
        }
        //public Mode Mode { get; set; }
        //public MeasurementType MeasType { get; set; }
        //private GimliParameterInt mNrOfWavelengths;

        //public GimliParameterInt NrOfWavelengths
        //{
        //    get { return mNrOfWavelengths; }
        //    set
        //    {
        //        if (mNrOfWavelengths != null)
        //            mNrOfWavelengths.ValueChangedEventHandler -= NrOfWavelengths_ValueChangedEventHandler;

        //        mNrOfWavelengths = value;
        //        if (mNrOfWavelengths != null)
        //            mNrOfWavelengths.ValueChangedEventHandler += NrOfWavelengths_ValueChangedEventHandler;
        //    }
        //}

        public List<GimliParameterInt> FluidVolume { get; set; }
        //public List<GimliParameterWavelength> Excitation { get; set; }
        //public List<GimliParameterWavelength> Emission { get; set; }
        //public GimliParameterInt Attenuation { get; set; }
        //public GimliParameterWavelength ExcitationStart { get; set; }
        //public GimliParameterWavelength ExcitationEnd { get; set; }
        //public GimliParameterWavelength EmissionStart { get; set; }
        //public GimliParameterWavelength EmissionEnd { get; set; }
        //public GimliParameterInt EmissionStep { get; set; }
        //public GimliParameterInt ExcitationStep { get; set; }
        //public GimliParameterBool ExcitationSweep { get; set; }
        //public GimliParameterBool PathCheck { get; set; }

        //private GimliParameterBool mAllWavelengths;
        //public GimliParameterBool AllWavelengths
        //{
        //    get { return mAllWavelengths; }
        //    set
        //    {
        //        mAllWavelengths = value;
                
        //        if (value?.Value != null)
        //        {
        //            NrOfWavelengths.IsUsed = !mAllWavelengths.ToBool();
        //        }
        //        else
        //        {
        //            NrOfWavelengths.IsUsed = true;
        //        }
        //    }
        //}

        public InjectorMaintenanceSettings Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as InjectorMaintenanceSettings;
        }

        public override List<SettingsSummary> GetSummary
        {
            get
            {
                var summary = new SettingsSummary(Resources.GetResource.GetString(Name)) {IsVisible = IsUsed };
                //if (Mode == Mode.Abs)
                //{
                //    if (MeasType == MeasurementType.SpectrumScan)
                //        summary.Items.AddRange(GetSpectrumExcitationSummary());
                //    else
                //    {
                //        for (var i = 0; i < Excitation.Count; i++)
                //            summary.Items.Add(Excitation[i].GetSummary(true));
                //    }
                //    if (PathCheck.Value != null && PathCheck.ToBool())
                //        summary.Items.Add(PathCheck.GetSummary(true));
                //}
                //if (Mode == Mode.Lum)
                //{
                //    if (MeasType == MeasurementType.SpectrumScan)
                //        summary.Items.AddRange(GetSpectrumEmissionSummary());
                //    else
                //    {
                //        if (AllWavelengths.IsUsed && AllWavelengths.ToBool())
                //            summary.Items.Add(AllWavelengths.GetSummary(true));
                //        else
                //        {
                //            for (var i = 0; i < Emission.Count; i++)
                //                summary.Items.Add(Emission[i].GetSummary(true));
                //        }
                //    }
                //}
                //if (Mode == Mode.Fl)
                //{
                //    if (MeasType == MeasurementType.SpectrumScan)
                //        summary.Items.AddRange(ExcitationSweep.ToBool()
                //            ? GetSpectrumExcitationSummary()
                //            : GetSpectrumEmissionSummary());
                //    else
                //    {
                //        for (var i = 0; i < Excitation.Count; i++)
                //        {
                //            var ex = Excitation[i].GetSummary();
                //            if (ex != null)
                //            {
                //                if (Emission.Count > i) ex.ItemValue += $" / {Emission[i].ToInt()} {Units}";
                //                summary.Items.Add(ex);
                //            }
                //        }
                //    }
                //}
                return new List<SettingsSummary> { summary };
            }
        }

        //private List<ItemClass> GetSpectrumEmissionSummary()
        //{
        //    var list = new List<ItemClass>();
        //    if (Mode == Mode.Fl)
        //    {
        //        list.Add(new ItemClass("Excitation", ExcitationStart.Value + " " + ExcitationStart.Units));
        //    }
        //    list.Add(EmissionStart.GetSummary(true));
        //    list.Add(EmissionEnd.GetSummary(true));
        //    list.Add(EmissionStep.GetSummary(true));
        //    return list;
        //}
        //private List<ItemClass> GetSpectrumExcitationSummary()
        //{
        //    var list = new List<ItemClass>();
        //    list.Add(ExcitationStart.GetSummary(true));
        //    list.Add(ExcitationEnd.GetSummary(true));
        //    list.Add(ExcitationStep.GetSummary(true));
        //    if (Mode == Mode.Fl)
        //    {
        //        list.Add(new ItemClass("Emission", EmissionStart.Value + " " + EmissionStart.Units));
        //    }
        //    return list;
        //}
        public override bool IsValid
        {
            get
            {
				var isvalid = DetermineIsvalidForAllParameters();

                foreach (var fluidVolume in FluidVolume)
                {
                    ValidateProperty(fluidVolume, mErrorMessage, ref isvalid);   
                }

				//foreach (var filter in Excitation)
    //            {
				//	ValidateProperty(filter, mErrorMessage, ref isvalid);
    //            }
    //            foreach (var filter in Emission)
    //            {
				//	ValidateProperty(filter, mErrorMessage, ref isvalid);
    //            }

                return isvalid;
            }
        }

		public override IEnumerable<GimliParameter> GetProperties()
	    {
			return new List<GimliParameter> { /*NrOfWavelengths,*/ /*Attenuation, ExcitationSweep, ExcitationStart, ExcitationEnd, ExcitationStep, EmissionStart, EmissionEnd, EmissionStep, PathCheck, AllWavelengths*/ };
		}
        
	    public MemoryStream Serialize()
        {
            var ms = new MemoryStream();
            BinaryWriter writer = null;
            try
            {
                writer = new BinaryWriter(ms);
                //RawBinaryWriter.SerializeParameter(writer, NrOfWavelengths);
                //RawBinaryWriter.SerializeParameter(writer, Attenuation);
                //RawBinaryWriter.SerializeParameter(writer, ExcitationStart);
                //RawBinaryWriter.SerializeParameter(writer, ExcitationEnd);
                //RawBinaryWriter.SerializeParameter(writer, EmissionStart);
                //RawBinaryWriter.SerializeParameter(writer, EmissionEnd);
                //RawBinaryWriter.SerializeParameter(writer, ExcitationStep);
                //RawBinaryWriter.SerializeParameter(writer, EmissionStep);
                //RawBinaryWriter.SerializeParameter(writer, ExcitationSweep);
                //RawBinaryWriter.SerializeParameter(writer, PathCheck);
                //RawBinaryWriter.SerializeParameter(writer, AllWavelengths);
                //RawBinaryWriter.SerializeParameter(writer, Excitation);
                //RawBinaryWriter.SerializeParameter(writer, Emission);


                //public List<GimliParameterWavelength> Excitation { get; set; }
                //public List<GimliParameterWavelength> Emission { get; set; }
                writer.Write(FluidVolume.JsonSerializeToCamelCase());     // TODO
                return ms;
            }
            catch (Exception e)
            {
                Trace.WriteLine("InjectorMaintenanceSettings: " + e.Message);
                return ms;
            }
            finally
            {
                writer?.Dispose();
            }
        }

        private void InitializeSettings(/*int nrOfWavelengths*/)
        {
            CreateDefaultEmptySettings();

            //if (Mode == Mode.Abs)
            //    CreateAbsorbanceWavelength(nrOfWavelengths);
            //else if (Mode == Mode.Lum)
            //    CreateLuminescenceWavelength(nrOfWavelengths);
            //else
            //    CreateFluorescenceWavelength(nrOfWavelengths);
        }

        //private void NrOfWavelengths_ValueChangedEventHandler(object sender, GimliValueChangedEvent e)
        //{
        //    InitializeSettings(mNrOfWavelengths.ToInt());
        //}

        private void CreateDefaultEmptySettings()
        {
            //if (NrOfWavelengths == null)
            //    NrOfWavelengths = new GimliParameterInt();

            FluidVolume = new List<GimliParameterInt>();

            for (int fluidIndex = 0; fluidIndex < 4; fluidIndex++)
            {
                FluidVolume.Add(new GimliParameterInt($"FluidVolume {fluidIndex + 1}", MinFluidVolume)
                {
                    Minimum = MinFluidVolume,
                    Maximum = MaxFluidVolume,
                    Step = 1,
                    IsUsed = true,
                    IsEnabled = true,
                    IsDisplayedInSummary = true,
                    Units = Units       //Properties.Resources.UnitNanometer
                });
            }
            //Excitation = new List<GimliParameterWavelength>();
            //Emission = new List<GimliParameterWavelength>();

            //Attenuation = new GimliParameterInt();
            //ExcitationStart = new GimliParameterWavelength();
            //ExcitationEnd = new GimliParameterWavelength();
            //EmissionStart = new GimliParameterWavelength();
            //EmissionEnd = new GimliParameterWavelength();
            //EmissionStep = new GimliParameterInt();
            //ExcitationStep = new GimliParameterInt();
            //ExcitationSweep = new GimliParameterBool();
            //PathCheck = new GimliParameterBool();
            //AllWavelengths = new GimliParameterBool("AllWavelengths", false);
        }

        //private void CreateFluorescenceWavelength(int nrOfWavelengths)
        //{
        //    if (MeasType == MeasurementType.SpectrumScan)
        //    {
        //        EmissionStart = new GimliParameterWavelength("EmissionStart", MinEmissionFluorescence)
        //        {
        //            Minimum = MinEmissionFluorescence,
        //            Maximum = MaxEmissionFluorescence,
        //            Step = 1,
        //            IsUsed = true,
        //            IsEnabled = true,
        //            IsDisplayedInSummary = true
        //        };
        //        EmissionEnd = new GimliParameterWavelength("EmissionEnd", MaxEmissionFluorescence)
        //        {
        //            Minimum = MinEmissionFluorescence,
        //            Maximum = MaxEmissionFluorescence,
        //            Step = 1,
        //            IsUsed = true,
        //            IsEnabled = true,
        //            IsDisplayedInSummary = true
        //        };
        //        EmissionStep = new GimliParameterInt("EmissionStep", 10)
        //        {
        //            Minimum = 1,
        //            Maximum = 50,
        //            Step = 1,
        //            IsUsed = true,
        //            IsEnabled = true,
        //            IsDisplayedInSummary = true,
        //            Units = Properties.Resources.UnitNanometer
        //        };
        //        ExcitationSweep = new GimliParameterBool("ExcitationSweep", true)
        //        {
        //            LegalValues = new ArrayList() { true, false },
        //            Hidden = true,
        //            IsUsed = true,
        //            IsEnabled = true,
        //            IsDisplayedInSummary = false
        //        };
        //        ExcitationStart = new GimliParameterWavelength("ExcitationStart", MinExcitationFluorescence)
        //        {
        //            Minimum = MinExcitationFluorescence,
        //            Maximum = MaxExcitationFluorescence,
        //            Step = 1,
        //            IsUsed = true,
        //            IsEnabled = true,
        //            IsDisplayedInSummary = true
        //        };
        //        ExcitationEnd = new GimliParameterWavelength("ExcitationEnd", MaxExcitationFluorescence)
        //        {
        //            Minimum = MinExcitationFluorescence,
        //            Maximum = MaxExcitationFluorescence,
        //            Step = 1,
        //            IsUsed = true,
        //            IsEnabled = true,
        //            IsDisplayedInSummary = true
        //        };
        //        ExcitationStep = new GimliParameterInt("ExcitationStep", 10)
        //        {
        //            Minimum = 1,
        //            Maximum = 50,
        //            Step = 1,
        //            IsUsed = true,
        //            IsEnabled = true,
        //            IsDisplayedInSummary = true,
        //            Units = Properties.Resources.UnitNanometer
        //        };
        //        Emission.Add(new GimliParameterWavelength("Emission", 485)
        //        {
        //            Minimum = MinEmissionFluorescence,
        //            Maximum = MaxEmissionFluorescence,
        //            Step = 1,
        //            LegalValues = new ArrayList(),
        //            IsUsed = true,
        //            IsEnabled = true,
        //            IsDisplayedInSummary = true
        //        });
        //        Excitation.Add(new GimliParameterWavelength("Excitation", 535)
        //        {
        //            Minimum = MinExcitationFluorescence,
        //            Maximum = MaxExcitationFluorescence,
        //            Step = 1,
        //            LegalValues = new ArrayList(),
        //            IsUsed = true,
        //            IsEnabled = true,
        //            IsDisplayedInSummary = true
        //        });
        //        return;
        //    }
        //    NrOfWavelengths = new GimliParameterInt("NrOfWavelengths", nrOfWavelengths) { Minimum = 1, Maximum = 6, Step = 1, IsUsed = true };
        //    for (var i = 0; i < nrOfWavelengths; i++)
        //    {
        //        Excitation.Add(new GimliParameterWavelength("Excitation" + (i + 1), 485)
        //        {
        //            Minimum = MinExcitationFluorescence,
        //            Maximum = MaxExcitationFluorescence,
        //            Step = 1,
        //            LegalValues = new ArrayList(),
        //            IsUsed = true,
        //            IsEnabled = true,
        //            IsDisplayedInSummary = true
        //        });
        //        Emission.Add(new GimliParameterWavelength("Emission" + (i + 1), 535)
        //        {
        //            Minimum = MinEmissionFluorescence,
        //            Maximum = MaxEmissionFluorescence,
        //            Step = 1,
        //            LegalValues = new ArrayList(),
        //            IsUsed = true,
        //            IsEnabled = true,
        //            IsDisplayedInSummary = true
        //        });
        //    }
        //}

        //private void CreateLuminescenceWavelength(int nrOfWavelengths)
        //{
        //    if (MeasType == MeasurementType.SpectrumScan)
        //    {
        //        EmissionStart = new GimliParameterWavelength("EmissionStart", MinLuminescence) { Minimum = MinLuminescence, Maximum = MaxLuminescence, Step = 1, IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true };
        //        EmissionEnd = new GimliParameterWavelength("EmissionEnd", MaxLuminescence) { Minimum = MinLuminescence, Maximum = MaxLuminescence, Step = 1, IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true };
        //        EmissionStep = new GimliParameterInt("EmissionStep", 1) { Minimum = 1, Maximum = 50, Step = 1, IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true, Units = Properties.Resources.UnitNanometer };
        //        ExcitationSweep = new GimliParameterBool("ExcitationSweep", false) { LegalValues = new ArrayList() { false }, Hidden = true, IsUsed = true, IsDisplayedInSummary = false };
        //        return;
        //    }
        //    NrOfWavelengths = new GimliParameterInt("NrOfWavelengths", nrOfWavelengths) { Minimum = 1, Maximum = 6, Step = 1, IsUsed = true };
        //    for (var i = 0; i < nrOfWavelengths; i++)
        //    {
        //        Emission.Add(new GimliParameterWavelength("Emission" + (i + 1), 400) { Minimum = MinEmissionFluorescence, Maximum = MaxEmissionFluorescence, Step = 1, LegalValues = new ArrayList(), IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true });
        //    }
        //    AllWavelengths = new GimliParameterBool("AllWavelengths", false) { LegalValues = new ArrayList() { false, true }, Hidden = false, IsEnabled = true, IsUsed = true, IsDisplayedInSummary = true };

        //    Attenuation = new GimliParameterInt("Attenuation", 0) { Minimum = 0, Maximum = 0, IsUsed = false, IsEnabled=true, IsDisplayedInSummary=true };
        //}

        //private void CreateAbsorbanceWavelength(int nrOfWavelengths)
        //{
        //    if (MeasType == MeasurementType.SpectrumScan)
        //    {
        //        ExcitationStart = new GimliParameterWavelength("ExcitationStart", MinAbsorbance) { Minimum = MinAbsorbance, Maximum = MaxAbsorbance, Step = 1, IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true};
        //        ExcitationEnd = new GimliParameterWavelength("ExcitationEnd", MaxAbsorbance) { Minimum = MinAbsorbance, Maximum = MaxAbsorbance, Step = 1, IsUsed = true, IsEnabled=true, IsDisplayedInSummary = true};
        //        ExcitationStep = new GimliParameterInt("ExcitationStep", 1) { Minimum = 1, Maximum = 50, Step = 1, IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true, Units = Properties.Resources.UnitNanometer };
        //        ExcitationSweep = new GimliParameterBool("ExcitationSweep", true) { LegalValues = new ArrayList() { true }, Hidden = true, IsUsed = false, IsDisplayedInSummary = false};
        //        return;
        //    }
        //    NrOfWavelengths = new GimliParameterInt("NrOfWavelengths", nrOfWavelengths) { Minimum = 1, Maximum = 6, Step = 1, IsUsed = true, IsEnabled = true};
        //    for (var i = 0; i < nrOfWavelengths; i++)
        //    {
        //        Excitation.Add(new GimliParameterWavelength("Excitation" + (i + 1), 405) { Minimum = MinAbsorbance, Maximum = MaxAbsorbance, Step = 1, LegalValues = new ArrayList(), IsUsed = true, IsEnabled=true, IsDisplayedInSummary=true });
        //    }

        //    var isused = MeasType == MeasurementType.Endpoint;
        //    PathCheck = new GimliParameterBool("PathCheck", false) { LegalValues = new ArrayList() { true, false }, Hidden = !isused, IsUsed = isused, IsEnabled= isused, IsDisplayedInSummary= isused };
        //}
    }
}
