﻿using System;

namespace Gimli.JsonObjects.InjectorMaintenance
{
	[Serializable]
	public class InjectorConfiguration
	{
		public InjectorSel Name { get; set; }
		public int VoidVolume { get; set; }
		public int WashVolume { get; set; }
		public int PrimeVolume { get; set; }
		public string Unit { get; set; }
		public DoubleValueWithRange ManualRange { get; set; }
		public DoubleValueWithRange CalibrationDispenseRange { get; set; }
		public DoubleValueWithRange CalibrationVerifyRange { get; set; }
		public string WeightUnit { get; set; }
	}
}
