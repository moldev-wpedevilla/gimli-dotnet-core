﻿using System;

namespace Gimli.JsonObjects.InjectorMaintenance
{
	public enum InjectorStatus
	{
		None,
		NotConnected,
		Idle,
		Running,
		Finished,
		FinishedWithError
	}

	[Serializable]
	public class InjectorsUpdate
	{
		public InjectorStatus Injector1 { get; set; }
		public InjectorStatus Injector2 { get; set; }
	}
}
