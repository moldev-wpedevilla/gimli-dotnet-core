using System;
using System.Collections.Generic;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class MeasurementMode
	{
		//for Serialization only
		public MeasurementMode()
		{
			
		}

		public MeasurementMode(Mode mode, string unit, MeasurementType measuremntType, double minValue, double maxValue, double minXpos, double maxXPos, double incrementX, double minYPos, double maxYPos, double incrementY, List<Loop> loops)
		{
			Mode = mode;
			Unit = unit;
			MeasuremntType = measuremntType;
			MinValue = minValue;
			MaxValue = maxValue;
			MinXPos = minXpos;
			MaxXPos = maxXPos;
			IncrementX = incrementX;
			MinYPos = minYPos;
			MaxYPos = maxYPos;
			IncrementY = incrementY;
			Loops = loops;
		}

		public Mode Mode { get; set; }
		public string Unit { get; set; }
		public MeasurementType MeasuremntType { get; set; }
		public double MinValue { get; set; }
		public double MaxValue { get; set; }
		public double MinXPos { get; set; }
		public double MaxXPos { get; set; }
		public double IncrementX { get; set; }
		public double MinYPos { get; set; }
		public double MaxYPos { get; set; }
		public double IncrementY { get; set; }
		public List<Loop> Loops { get; set; }
	}
}