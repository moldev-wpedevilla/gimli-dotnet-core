﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gimli.JsonObjects
{
    [Serializable]
    public class PlateDrawerSettings
    {
        public bool LockPlate { get; set; }
        public ToggleBehavior Behavior { get; set; }
    }

    [Serializable]
    public enum ToggleBehavior
    {
        Toggle,
        Open,
        Close
    }
}
