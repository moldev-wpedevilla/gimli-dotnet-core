﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Gimli.JsonObjects
{
	public static class HelperMethods
	{
		public static string GetLocalHostName()
		{
			return Dns.GetHostEntry("localhost").HostName;
		}

		public static string GetLocalIPAddress(List<string> ipAddresses)
		{
			var localIP = string.Empty;
			var cnt = 0;
			foreach (var addre in ipAddresses)
			{
				if (cnt == 1)
				{
					localIP += " (";
				}

				localIP += addre;

				if (cnt >= 1 && cnt < ipAddresses.Count - 1)
				{
					localIP += ", ";
				}

				cnt++;
			}

			if (cnt > 1)
			{
				localIP += ")";
			}

			return localIP == "127.0.0.1" ? "no network connection" : localIP;
		}

		public static IEnumerable<string> GetIPAddressList()
		{
			var host = Dns.GetHostEntry(Dns.GetHostName());
			return host.AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork).Select(addre => addre.ToString());
		}

		public static Task<T> RunTaskWithLogging<T>(Func<T> action, Action<string> logger, CancellationToken token)
		{
			var task = new Task<T>(action, token);
			return ConfigureTaskAndRun(logger, task);
		}

		public static Task RunTaskWithLogging(Action action, Action<string> logger, CancellationToken token)
		{
			var task = new Task(action, token);
			return ConfigureTaskAndRun(logger, task);
		}

		private static Task<T> ConfigureTaskAndRun<T>(Action<string> logger, Task<T> task)
		{
			task.ContinueWith(t => ExceptionHandler(t, logger), TaskContinuationOptions.OnlyOnFaulted);
			task.Start();
			return task;
		}

		private static Task ConfigureTaskAndRun(Action<string> logger, Task task)
		{
			task.ContinueWith(t => ExceptionHandler(t, logger), TaskContinuationOptions.OnlyOnFaulted);
			task.Start();
			return task;
		}

		private static void ExceptionHandler(Task task, Action<string> logger)
		{
			var exception = task.Exception;
			logger(exception?.ToString());
		}
	}
}
