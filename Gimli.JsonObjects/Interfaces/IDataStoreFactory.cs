﻿
namespace Gimli.JsonObjects.Interfaces
{
	public interface IDataStoreFactory
	{
		IDataStore Generate();
	}
}
