﻿
namespace Gimli.JsonObjects.Interfaces
{
	public interface ISystemDataStoreFactory
	{
		ISystemDataStore Generate();
	}
}
