﻿using System;
using System.Collections.Generic;
using Gimli.JsonObjects.Enums;

namespace Gimli.JsonObjects.Interfaces
{
    public interface IDataStore
    {
        ICollection<ProtocolListItem> GetListOfItems(UserFull user, ItemType protocolType, bool includePublic);
        Document GetItem(ProtocolListItem item);
        bool SaveItem(Document document);
        bool DeleteItem(string userName, string protocolName);
        /// <summary>
        /// Returns available free space on harddrive c:
        /// </summary>
        /// <returns>returns available free harddisk space in MB</returns>
        int GetFreeSpace();
        bool DeleteUser(UserFull user);
        bool DeleteFolder(string folderName);
        bool SaveUserCredentials(UserFull user, string pin = null);
        bool CheckPin(Guid userId, string pin);
        List<UserFull> GetAllUserCredentials();
        List<DocumentCategory> GetListOfCategories(Guid userId);
    }
}