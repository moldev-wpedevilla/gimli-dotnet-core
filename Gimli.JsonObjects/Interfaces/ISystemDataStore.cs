﻿using System.Collections.Generic;

namespace Gimli.JsonObjects.Interfaces
{
    public interface ISystemDataStore
    {
        bool SaveFirmware(string typeOfFirmware, string data);
        string LoadFirmware(string typeOfFirmware);
        ICollection<object> GetLogFiles();
        bool SetFilterInfo(string data);
        string GetSkinFile(int type, int version);
        /// <summary>
        /// Save cartridge information into a cartridge file on Gimli
        /// </summary>
        /// <param name="data">content of cartridge file</param>
        /// <returns>true if file is saved on internal data store, false if file cannot be stored</returns>
        bool SaveSkinFile(string data);

        bool SaveConfigFile(string fileName, string data);
        string LoadConfigFile(string fileName);

        int GetFreeSpace();
    }
}
