﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class MessageParameters
	{
		public MessageType MessageType { get; set; }
		public MessageDisplayType DisplayType { get; set; }
		public int MessageCode { get; set; }
		public string Message { get; set; }
		public string YesButtonText { get; set; }
		public string NoButtonText { get; set; }
        public string GoToTopic { get; set; }
	}

	public enum MessageType
	{
		None,
		Error,
		Info,
		Warning
	}

	public enum MessageDisplayType
	{
		None,
		Toast,
		DialogOk,
		DialogDecision,
        DialogHelp
	}
}
