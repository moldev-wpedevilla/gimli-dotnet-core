﻿using System;
using System.Collections.Generic;
using Gimli.JsonObjects.Settings;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class SnapshotReductionSettings : AllSettingsBase
	{
		public Dictionary<Guid,DataReductionSettings> ReductionSettings { get; set; }
	}
}
