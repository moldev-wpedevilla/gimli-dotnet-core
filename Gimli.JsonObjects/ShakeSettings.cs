﻿using System;

namespace Gimli.JsonObjects
{
    [Serializable]
    public class ShakeSettings
    {
		public ListValueWithLegalValues<string> Format { get; set; }
		public ListValueWithLegalValues<ShakeMode> Mode { get; set; }
		public ListValueWithLegalValues<ShakeIntensity> Intensity { get; set; }
        public int AutoPauseTimeoutSeconds { get; set; }
	}
}