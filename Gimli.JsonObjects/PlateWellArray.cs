using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class PlateWellArray : PlateWellBase
	{
		public Point[] Wells { get; set; }
	}
}