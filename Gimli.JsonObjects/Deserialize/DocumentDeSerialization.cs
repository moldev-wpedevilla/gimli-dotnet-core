﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;

namespace Gimli.JsonObjects.Deserialize
{
    public static class DocumentDeserialization
    {
        public static T UnzipAndDeserialize<T>(byte[] serialized, long unCompressedSize)
        {
            return Deserialize<T>(serialized, true, unCompressedSize);
        }
        public static T DeserializeObject<T>(byte[] serialized)
        {
            return Deserialize<T>(serialized, false);
        }
        private static T Deserialize<T>(byte[] serialized, bool unzipIt, long unCompressedSize = 0)
        {
            if (serialized == null || Convert.IsDBNull(serialized) || serialized.Length == 0) return default(T);

            var uncomp = unzipIt ? UnzipStream(serialized, unCompressedSize) : serialized;
            var start2 = DateTime.Now;
            var memStream = new MemoryStream(uncomp.Length < 2 ? serialized : uncomp);
            try
            {
                return (T) new BinaryFormatter().Deserialize(memStream);
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Exception Deserialize: " + typeof (T) + " - " + exc.Message);
                return default(T);
            }
            finally
            {
                memStream.Dispose();
                var time = DateTime.Now.Subtract(start2).TotalMilliseconds;
                if (time > 0)
                {
                    Trace.WriteLine(string.Format("Trace: {0,-50}: {1,7} ms, Type: {2} ", "Deserialization pur",
                        time.ToString("N0"),
                        typeof (T)));
                }
            }
        }
        public static byte[] UnzipStream(byte[] stream, long uncompressed)
        {
            var start = DateTime.Now;
            var uncomp = new byte[uncompressed];
            MemoryStream memStream = null;

            try
            {
                memStream = new MemoryStream(stream);
                var zip = new GZipStream(memStream, CompressionMode.Decompress, false);
                zip.Read(uncomp, 0, uncomp.Length);
                zip.Close();
                zip.Dispose();
                return uncomp;
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Document::UnzipStream: " + exc.Message);
                return null;
            }
            finally
            {
                Trace.WriteLine(string.Format("Trace: {0,-50}: {1,7} ms, Size: {2} -> {3} Bytes - Saved: {4} KB {5}", "UnZipping Documedata" , 
                    DateTime.Now.Subtract(start).TotalMilliseconds.ToString("N0"), 
                    stream.Length.ToString("N0"), 
                    uncomp.Length.ToString("N0"), 
                    ((uncomp.Length - stream.Length) / 1024).ToString("N1"), 
                    (1.0 - stream.Length / (double)uncomp.Length).ToString("0%")));
                memStream?.Dispose();
            }
        }
    }
}
