﻿using Gimli.JsonObjects.Enumarations.Status;
using Gimli.JsonObjects.Parameter;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Gimli.JsonObjects.Serialize;

namespace Gimli.JsonObjects.Deserialize
{
    public static class RawBinaryReader
    {
        public static List<Well> DeserializeWells(byte[] serialized, long unCompressedSize, Version version = null)
        {
            return Deserialize(serialized, unCompressedSize, version);
        }
        private static List<Well> Deserialize(byte[] serialized, long unCompressedSize, Version version = null)
        {
            var list = new List<Well>();
            if (serialized == null || Convert.IsDBNull(serialized) || serialized.Length == 0) return list;

            var start2 = DateTime.Now;
            BinaryReader reader = null;

            try
            {
                var uncomp = DocumentDeserialization.UnzipStream(serialized, unCompressedSize);
                reader = new BinaryReader(new MemoryStream(uncomp));
                var count = reader.ReadInt32();
                for (var c = 0; c < count; c++)
                {
                    var well = new Well();
                    well.Deserialize(reader, version);
                    list.Add(well);
                }
                return list;
            }
            catch (Exception exc)
            {
                Trace.WriteLine("Exception Deserialize: Well - " + exc.Message);
                return list;
            }
            finally
            {
                reader?.Dispose();
				DocumentSerialization.TraceOutputDeltaTime(typeof(Well), start2, "Deserialization raw");
            }
        }

        public static DataPointStatus[] DeserializeOneDimensionArrayStatus(BinaryReader reader)
        {
            var firstDim = reader.ReadInt32();

            if (firstDim == 0) return null;

            var obj = new DataPointStatus[firstDim];

            for (var y = 0; y < firstDim; y++)
                obj[y] = (DataPointStatus)reader.ReadInt32();
            return obj;
        }

        public static double[] DeserializeOneDimensionArray(BinaryReader reader)
        {
            var firstDim = reader.ReadInt32();

            if (firstDim == 0) return null;

            var obj = new double[firstDim];

            for (var y = 0; y < firstDim; y++)
                obj[y] = reader.ReadDouble();
            return obj;
        }

        public static double[,] DeserializeTwoDimensionArray(BinaryReader reader)
        {
            var firstDim = reader.ReadInt32();
            var secondDim = reader.ReadInt32();

            if (firstDim == 0 || secondDim == 0) return null;

            var obj = new double[firstDim, secondDim];

            for (var y = 0; y < firstDim; y++)
                for (var x = 0; x < secondDim; x++)
                    obj[y, x] = reader.ReadDouble();
            return obj;
        }

        public static double[,,] DeserializeThreeDimensionArray(BinaryReader reader)
        {
			var dimensions = ReadDimensions(reader);
			if (!dimensions.AllValid) return null;

			var obj = new double[dimensions.First, dimensions.Second, dimensions.Third];

            for (var y = 0; y < dimensions.First; y++)
                for (var x = 0; x < dimensions.Second; x++)
                    for (var z = 0; z < dimensions.Third; z++)
                        obj[y, x, z] = reader.ReadDouble();
            return obj;
        }

        public static DataPointStatus[,,] DeserializeThreeDimensionArrayStatus(BinaryReader reader)
        {
	        var dimensions = ReadDimensions(reader);
			if (!dimensions.AllValid) return null;

	        var obj = new DataPointStatus[dimensions.First, dimensions.Second, dimensions.Third];

            for (var y = 0; y < dimensions.First; y++)
                for (var x = 0; x < dimensions.Second; x++)
                    for (var z = 0; z < dimensions.Third; z++)
                        obj[y, x, z] = (DataPointStatus)reader.ReadInt32();
            return obj;
        }

	    private class Dimensions
	    {
			internal bool AllValid => First != 0 && Second != 0 && Third != 0;
		    internal int First { get; set; }
		    internal int Second { get; set; }
		    internal int Third { get; set; }
	    }

	    private static Dimensions ReadDimensions(BinaryReader reader)
	    {
		    return new Dimensions {First = reader.ReadInt32(), Second = reader.ReadInt32(), Third = reader.ReadInt32()};
	    }

	    public static string DeserialzeString(BinaryReader reader)
        {
            return reader.ReadString();
        }

        //public static GimliParameter DeserialzeParameter<T>(BinaryReader reader)
        //{
        //    var isNull = reader.ReadBoolean();
        //    if (isNull) return null;

        //    GimliParameter parameter = null;
        //    if (default (T) is GimliParameterInt)
        //    {
        //        parameter = new GimliParameterInt {Value = reader.ReadInt32()};
        //    }
        //    else if (default(T) is GimliParameterBool)
        //    {
        //        parameter = new GimliParameterBool {Value = reader.ReadBoolean()};
        //    }
        //    else if (default(T) is GimliParameterDouble)
        //    {
        //        parameter = new GimliParameterDouble {Value = reader.ReadDouble()};
        //    }
        //    else if (default(T) is GimliParameterString)
        //    {
        //        parameter = new GimliParameterString { Value = reader.ReadString() };
        //    }
        //    else if (default(T) is GimliParameterTimeSpan)
        //    {
        //        parameter = new GimliParameterTimeSpan { Value = reader.ReadInt32() };
        //    }
        //    else if (default(T) is GimliParameterWavelength)
        //    {
        //        parameter = new GimliParameterWavelength { Value = reader.ReadInt32() };
        //        ((GimliParameterWavelength) parameter).UseMonocromator = reader.ReadBoolean();
        //    }

        //    DeserialzeParameterBase(reader, parameter);
        //    return parameter;
        //}

        public static GimliParameter DeserialzeParameterBase(BinaryReader reader, GimliParameter parameter)
        {
            if (reader.ReadBoolean())
            {
                //Deserialize()
            }
            return null;
            //    SerializeOneDimensionArray(writer, parameter.LegalValues);
            //    writer.Write(parameter.IsEnabled);
            //    writer.Write(parameter.IsUsed);
            //    writer.Write(parameter.Hidden);
            //    writer.Write(parameter.AffectOthers);
            //    writer.Write(parameter.Description);
            //    writer.Write(parameter.Name);
            //    writer.Write(parameter.IsDisplayedInSummary);
            //    writer.Write(parameter.IsModified);
            //    writer.Write(parameter.Units);
            //    writer.Write(parameter.Minimum);
            //    writer.Write(parameter.Maximum);
            //    writer.Write(parameter.Step);
            //    writer.Write(parameter.ProtocolEditable);
            //    writer.Write(parameter.ReadOnly);
        }
    }
}
