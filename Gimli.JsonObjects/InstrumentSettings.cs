﻿using System;
using System.Collections.Generic;
using Gimli.JsonObjects.InjectorMaintenance;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class InstrumentSettings
	{
		public List<InjectorCounters> InjectorsCounters { get; set; }
	}
}
