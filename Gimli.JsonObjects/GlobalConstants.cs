﻿using System;
using System.Globalization;
using Gimli.JsonObjects.Utils;

namespace Gimli.JsonObjects
{
    public class GlobalConstants
    {
        public static readonly Guid PublicGuid = Guid.Parse("00000000-0000-0000-0000-000000000001");
        public static readonly Guid StandardGuid = Guid.Parse("00000000-0000-0000-0000-000000000002");
        public static readonly Guid AdminGuid = Guid.Parse("00000000-0000-0000-0000-000000000003");
        public static readonly Guid ImportGuid = Guid.Parse("00000000-0000-0000-0000-000000000004");
		public static readonly string PublicGuidString = PublicGuid.ToString();
        public static readonly string StandardGuidString = StandardGuid.ToString();
        public static readonly string AdminGuidString = AdminGuid.ToString();
        public static readonly string PublicName = "Public";
        public static readonly string StandardName = "_Standard_";
        public static readonly string AdminName = "Admin";
        public static readonly string CredentialsFolder = string.Format(CultureInfo.InvariantCulture, @"{0}{1}", DataSetHelper.RootPath, "UserCredentials");
        public static readonly string CredentialsFile = CredentialsFolder + @"\UserList.xml";
        public static readonly bool PublicHasPin = false;
        public static readonly bool AdminHasPin = true;
        public static readonly string AdminPin = "0000";
        public static readonly string DummyPin = "0000";
        public static readonly string AdminRename = "AdminOld";

	    public const string UsbMdDocumentStore = "MD.DocumentStore_0936A205-2C42-4CD7-86E3-9C0C650CB39E";
		public const string CategoriesAllDocuments = "ACB5146E-0D4B-431A-B5BA-B94AF51CD36E";

		public const string DeviceId3 = "SpectraMax iD3";
		public const string DeviceId5 = "SpectraMax iD5";
    }
}
