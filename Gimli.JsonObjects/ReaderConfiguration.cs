﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class ReaderConfiguration
	{
		public string PICVersion { get; set; }
		public string PICEEPROM { get; set; }
		public string FirmwareVersion { get; set; }
		public string FirmwareEEPROM { get; set; }
		public string SerialNumber { get; set; }
		public string DeviceNumber { get; set; }
		public string DeviceType { get; set; }
		public string AssignedIp { get; set; }
		public string Hostname { get; set; }
        public bool ReadBarcodeOnLoad { get; set; }
        public string ModuleName { get; set; }
        public string ModuleSerialNumber { get; set; }
	}
}
