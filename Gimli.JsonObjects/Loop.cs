using System;
using System.Collections.Generic;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class Loop
	{
		//for Serialization only
		public Loop()
		{
			
		}

		public Loop(int cycleNumber, DateTime timeStamp, double temperature, List<Well> wells)
		{
			CycleNumber = cycleNumber;
			TimeStamp = timeStamp;
			Temperature = temperature;
			Wells = wells;
		}

		public int CycleNumber { get; set; }
		public DateTime TimeStamp { get; set; }
		public double Temperature { get; set; }
		public List<Well> Wells { get; set; }
	}
}