﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public enum ShakeIntensity
	{
		None,
		Low,
		Medium,
		High
	}
}