﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public enum ShakeMode
	{
		None,
		Orbital,
		Linear,
        DoubleOrbital
	}

    public static class ConvertShake
    {
        public static ShakeMode ConvertFromString(this string modeString)
        {
            if (modeString.Equals(Properties.Resources.ShakeModeOrbital))
                return ShakeMode.Orbital;
            if (modeString.Equals(Properties.Resources.ShakeModeLinear))
                return ShakeMode.Linear;
            if (modeString.Equals(Properties.Resources.ShakeModeDoubleOrbital))
                return ShakeMode.DoubleOrbital;
            return ShakeMode.Orbital;
        }
    }
}
