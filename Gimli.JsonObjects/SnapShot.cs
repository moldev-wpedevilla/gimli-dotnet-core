using Gimli.JsonObjects.Enumarations.Status;
using Gimli.JsonObjects.Settings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Gimli.JsonObjects
{
    [Serializable]
    public class Snapshot
    {
        private double mMinValue;
        private double mMaxValue;

		private double mReducedMinValue;
		private double mReducedMaxValue;
		//for Serialization only
		[JsonConstructor]
        public Snapshot()
        {
        }
       
        public Snapshot(Snapshot originalSnapshot)
        {
            if (originalSnapshot == null) return;

	        Id = originalSnapshot.Id;
            MinXPos = originalSnapshot.MinXPos;
            MaxXPos = originalSnapshot.MaxXPos;
            IncrementX = originalSnapshot.IncrementX;
            MinYPos = originalSnapshot.MinYPos;
            MaxYPos = originalSnapshot.MaxYPos;
            mMaxValue = originalSnapshot.mMaxValue;
            mMinValue = originalSnapshot.mMinValue;
	        mReducedMaxValue = originalSnapshot.mReducedMaxValue;
	        mReducedMinValue = originalSnapshot.mReducedMinValue;
            IncrementY = originalSnapshot.IncrementY;
            SnapshotDataName = originalSnapshot.SnapshotDataName;

            if (originalSnapshot.Settings != null)
            {
                Settings = originalSnapshot.Settings.Clone();
                originalSnapshot.Settings.SummaryRequest -= Settings_SummaryRequest;
            }
            Settings.SummaryRequest += Settings_SummaryRequest;

	        if (originalSnapshot.ReductionSettings != null)
	        {
		        ReductionSettings = originalSnapshot.ReductionSettings.Clone();
	        }

            if (originalSnapshot.Wells == null) return;

            Wells = new List<Well>();
            foreach (var well in originalSnapshot.Wells)
            {
                var newWell = well.Clone();
                Wells.Add(newWell);
                newWell.WellDataDimensionChanged += WellDataDimensionChanged;
                newWell.WellDataPointChanged += WellWellDataPointChanged;
            }

	        GraphXValueList = originalSnapshot.GraphXValueList;
        }

        private void Settings_SummaryRequest(GimliSummaryRequestEvent e)
        {
            OnSummaryRequest(e);
        }

        public Snapshot(Guid id, Mode mode, string unit, MeasurementType measurementType,
            double minXpos, double maxXPos, double incrementX, double minYPos, double maxYPos, double incrementY,
            List<Well> wells)
        {
	        Id = id;
            MinXPos = minXpos;
            MaxXPos = maxXPos;
            IncrementX = incrementX;
            MinYPos = minYPos;
            MaxYPos = maxYPos;
            IncrementY = incrementY;
            Wells = wells;
            SnapshotDataName = "DataName";
            Settings = MeasurementSettings.Generate(mode, measurementType);
            Settings.Unit = unit;
			ReductionSettings = DataReductionSettings.Generate(DataReductionSettings.MehtodPolarization);
            UpdateMinMax();
        }

        public delegate void SummaryRequestEventHandler(GimliSummaryRequestEvent args);
        public event SummaryRequestEventHandler SummaryRequest;

        public void OnSummaryRequest(GimliSummaryRequestEvent args)
        {
            SummaryRequest?.Invoke(args);
        }
        public void InitializeWells(int firstDim, int dataRows, int dataCols, bool setPoints = true)
        {
            if (Settings.ReadArea == null) return;
            var setMinToZero = Settings.MeasurementMode == Mode.Abs;

            if (Wells == null)
            {
                Wells = new List<Well>();
                for (var r = 0; r < Settings.ReadArea.Rows; r++)
                {
                    for (var c = 0; c < Settings.ReadArea.Columns; c++)
                    {
                        Wells.Add(new Well($"{(char)(r + 65)}{c + 1}", c, r, null, null, setMinToZero));
                    }
                }
            }

	        if (setPoints)
	        {
		        foreach (var well in Wells)
		        {
		            well.CycleTime = new double[dataCols];
			        well.Points = new double[firstDim, dataRows, dataCols];
			        well.DataPointsState = new DataPointStatus[firstDim, dataRows, dataCols];
			        well.WellDataDimensionChanged += WellDataDimensionChanged;
			        well.WellDataPointChanged += WellWellDataPointChanged;
		        }
	        }

	        UpdateMinMax();
        }
        private void WellWellDataPointChanged(object sender, EventArgs e)
        {
            //Trace.WriteLine("WellDataPointChanged!");
            UpdateMinMax();
        }

        private void WellDataDimensionChanged(object sender, EventArgs e)
        {
            Trace.WriteLine("WellDataDimensionChanged!");
        }
        public Guid Id { get; set; }
        public double MinValue
        {
            get{ return mMinValue; }
            set { mMinValue = value; }
        }
        public double MaxValue {
            get
            {
                return mMaxValue;
            }
            set { mMaxValue = value; }
        }
		public double ReducedMinValue
		{
			get { return mReducedMinValue; }
			set { mReducedMinValue = value; }
		}
		public double ReducedMaxValue
		{
			get
			{
				return mReducedMaxValue;
			}
			set { mReducedMaxValue = value; }
		}
		public double MinXPos { get; set; }
        public double MaxXPos { get; set; }
        public double IncrementX { get; set; }
        public double MinYPos { get; set; }
        public double MaxYPos { get; set; }
        public double IncrementY { get; set; }
        public bool HasData
        {
            get
            {
                return Wells != null && Wells.Any(well => Math.Abs(well.MinValue + well.MaxValue) >= double.Epsilon);
            }
        }
        public MeasurementSettings Settings { get; set; }
        public DataReductionSettings ReductionSettings { get; set; }
		public List<Well> Wells { get; set; }
        public string SnapshotDataName { get; set; }
        public DataDimension[] DataDimensionInfo
        {
            get
            {
                return CreateDefaultDimensionInfo(GraphXValueList);
            }
        }

		public List<double> GraphXValueList { private get; set; }

        public Well GetFirstUsedWell()
        {
            return Wells?.FirstOrDefault(well => well.Points != null);
        }
        public DataDimension[] CreateDefaultDimensionInfo(List<double> kineticXValueList)
        {
            var dimInfo = new DataDimensionHelper();
            return dimInfo.CreateDataDimensionInfo(this, kineticXValueList);
        }
        public bool WellBasedSerialization => GetDataSizePerSnapShot > 1000000000;
        public long GetDataSizePerSnapShot
        {
            get
            {
                long maxSize = 1;

                try
                {
                    foreach (var info in DataDimensionInfo)
                    {
                        if (info.DataPointLabel == null) continue;
                        if (info != null) maxSize = maxSize * info.DataPointLabel.Count;
                    }
                }
                catch (Exception exc)
                {
                    Trace.WriteLine("Error GetDataSizePerSnapShot: " + exc.Message);
                }
                
                return maxSize*Wells.Count*64;
            }
        }
        public void UpdateMinMax()
        {
            UpdateMax();
            UpdateMin();
        }
        private void UpdateMax()
        {
            mMaxValue = 0;
            if (Wells == null) return;

            foreach (var tempMax in Wells.Select(well => well?.MaxValue ?? 0).Where(tempMax => mMaxValue < tempMax))
            {
                mMaxValue = tempMax;
            }
            mMaxValue = Math.Round(mMaxValue, 3);

			var okWells = Wells.Where(w => w.ReductionState == DataPointStatus.OK).ToList();
			if (!okWells.Any()) return;
			mReducedMaxValue = okWells.Max(w => w.ReductionValue);
        }
        private void UpdateMin()
        {
            mMinValue = double.MaxValue;
            if (Wells == null) return;

            foreach (var well in Wells)
            {
                var tempMin = well?.MinValue ?? 0;
                if (mMinValue > tempMin)
                {
                    mMinValue = tempMin;
                }
            }

            if (Math.Abs(mMinValue - double.MaxValue) < double.Epsilon) mMinValue = 0;

            mMinValue = Math.Round(mMinValue, 3);

	        var okWells = Wells.Where(w => w.ReductionState == DataPointStatus.OK).ToList();
			if (!okWells.Any()) return;
			mReducedMinValue = okWells.Min(w => w.ReductionValue);
		}

		public Snapshot Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as Snapshot;
        }
    }


    public class GimliSummaryRequestEvent : EventArgs
    {
        public GimliSummaryRequestEvent(List<SettingsSummary> summary)
        {
            Summary = summary;
        }
        public List<SettingsSummary> Summary { get; set; }
    }
}