using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace Gimli.JsonObjects
{
    [Serializable]
	public class Document : ProtocolListItem
	{
        private bool mIsValid = true;
        //for Serialization only
        public Document(){}

	    public Document(Document originalDocument) : this(originalDocument as ProtocolListItem)
	    {
	        if (originalDocument != null)
	        {
	            if (originalDocument.Experiments == null) return;
                Experiments = new List<Experiment>();
	            foreach (var experiment in originalDocument.Experiments)
	            {
	                Experiments.Add(experiment.Clone());
	            }
	        }
		}

		public Document(ProtocolListItem item)
			: base(
				item.Id, item.UserNameGuid, item.ProtocolName, item.DisplayName, item.Timestamp, item.HasData,
				item.IsSynchronized)
		{
			IsProtected = item.IsProtected;
			LastUsedDate = item.LastUsedDate;
			NumberOfPlates = item.NumberOfPlates;
			PercentMeasured = item.PercentMeasured;
			Size = item.Size;
			Notes = item.Notes;
			IsPinned = item.IsPinned;
			PinDate = item.PinDate;
			PinSlot = item.PinSlot;
			PinName = item.PinName;
			Category = item.Category;
		    Version = item.Version;
			MetaInfo = new DocumentMetaInfo();
		}

	    public Document(Guid id, string userName, string protocolName, string displayName, DateTime timestamp, bool hasData, bool isSynchronized, List<Experiment> experiments) : base(id, userName, protocolName, displayName, timestamp, hasData, isSynchronized)
		{
			Experiments = experiments;
		}

		// TODO: Tell Amplify to rename if this is not different from respective result file name or protocol file name
		public string DocumentName => Name;

        // TODO: Tell Amplify to rename
        public DateTime TimeStamp => Timestamp;

	    public List<Experiment> Experiments { get; set; }
        public bool IsValid
        {
            get
            {
                foreach (var experiment in Experiments)
                {
                    if (experiment.IsValid) return false;
                }
                return mIsValid;
            }
        }

		public DocumentMetaInfo MetaInfo { get; set; }

		public static Version GetVersion()
		{
			var assembly = Assembly.GetExecutingAssembly();
			return new Version(FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion);
		}

		public Document Clone()
		{
			var newObject = Activator.CreateInstance(GetType(), this);
			return newObject as Document;
		}

		/// <summary>
		/// Creates a new document without welldata from the current document.
		/// </summary>
		/// <param name="saveClone">If this is true clone operation is done before removing welldata. This gives you a performance penanlty but reduces the risk that parts of the document are changed while still cloning to empty.</param>
		/// <returns>A clone of the document without any welldata.</returns>
		public Document CloneToEmpty(bool saveClone = false)
		{
			var returnObject = saveClone ? Clone() : null;
			var exp = returnObject != null ? returnObject.Experiments : Experiments;

			// for performance reasons not using clone first. Instead remember list of wells and temporarily replace with empty list before cloning.
			var listOfWellLists = new Dictionary<string, List<Well>>();
			foreach (var experiment in exp)
			{
				foreach (var plate in experiment.Plates)
				{
					foreach (var snapshot in plate.SnapShot)
					{
						if (!saveClone) listOfWellLists[GetDictionaryKey(experiment.Id, plate.Id, snapshot.Id)] = snapshot.Wells;
						snapshot.Wells = new List<Well>();
					}
				}
			}

			if (saveClone)
			{
				return returnObject;
			}

			returnObject = Clone();

			foreach (var experiment in Experiments)
			{
				foreach (var plate in experiment.Plates)
				{
					foreach (var snapshot in plate.SnapShot)
					{
						snapshot.Wells = listOfWellLists[GetDictionaryKey(experiment.Id, plate.Id, snapshot.Id)];
					}
				}
			}

			return returnObject;
		}

		public static string GetDictionaryKey(Guid experimentId, Guid plateId, Guid snapshotId)
		{
			return $"{experimentId}_{plateId}_{snapshotId}";
		}
	}

    [Serializable]
    public class RuntimeData
    {
        public Document Document { get; set; }
        public MeasurementStatus Status { get; set; }
        public double RemainingTime { get; set; }
        public int PercentDone { get; set; }
        public Point[] MeasuredWells { get; set; }
		public string ErrorMessage { get; set; }
	    public long WellSeriailizationSize { get; set; }
	    public byte[] WellSeriailization { get; set; }
	}
}