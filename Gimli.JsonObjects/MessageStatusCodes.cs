﻿namespace Gimli.JsonObjects
{
	public static class MessageStatusCodes
	{
		public const int SoftwareUpdateMessage = 100;
		public const int InstrumentErrorMessage = 101;
		public const int TogglePlateInfoMessage = 102;
		public const int ChangedValueInfoMessage = 103;
		public const int OutboundConnectionAttempt = 104;
	    public const int ExportDataMessage = 105;
	    public const int ExportLogMessage = 106;
	    public const int UsbDrivePluginValid = 107;
	    public const int UsbDrivePluginInvalid = 108;
	    public const int UserLoggedIn = 109;
	    public const int DeviceLocked = 110;
	    public const int NfcTagNotAssigned = 111;
	    public const int ShutdownAfterLockMessage = 112;
		public const int ReaderNotIdle = 113;
	    public const int FirmwareUpdate = 114;
	    public const int ExportedSomeResults = 115;
	    public const int TimeUpdated = 116;
		public const int ReadFinished = 117;
		public const int GxpLockChanged = 118;
        public const int DismissBatteryScheduleMessage = 119;
        public const int DismissBatteryDeadMessage = 120;
	    public const int SoftwareUpdateNotPossibleGxpMessage = 121;
	    public const int InjectionError = 122;
	    public const int RemovePlateMessage = 123;
        public const int FiltersMngmntError = 124;
	    public const int SoftwareUpdateDamagedMessage = 125;
	    public const int DiskSpaceWarningMessage = 126;
	    public const int ImportFinishedMessage = 127;
	    public const int RemoveModuleSlidesMessage = 128;
	    public const int RemoveBottlesMessage = 129;
	    public const int PlateHeightError = 130;
		public const int ShutdownAfterInitFailureMessage = 131;

		public const int SoftwareUpdateFailed = 140;
	    public const int SoftwareUpdateSuccess = 141;

		public const int DummyMessage = 1234;
	}
}
