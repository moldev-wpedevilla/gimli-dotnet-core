using Gimli.JsonObjects.Settings;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gimli.JsonObjects
{
    [Serializable]
    public class Plate
	{
        private double mMinValue;
        private double mMaxValue;

        //for Serialization only
        public Plate() { }

	    public Plate(Plate originalPlate)
	    {
	        if (originalPlate == null) return;

	        Id = originalPlate.Id;
	        PlateName = originalPlate.PlateName;
	        NumberFormat = originalPlate.NumberFormat;
	        Settings = originalPlate.Settings?.Clone();

	        if (originalPlate.SnapShot == null) return;

            SnapShot = new List<Snapshot>();
	        foreach (var snapshot in originalPlate.SnapShot)
	        {
	            var clone = snapshot.Clone();
                clone.SummaryRequest += Clone_SummaryRequest;
                SnapShot.Add(clone);
	        }
	    }

        private void Clone_SummaryRequest(GimliSummaryRequestEvent e)
        {
            e.Summary = Settings.GetSummary;
        }
        
        public Plate(Guid id, string plateName, List<Snapshot> snapShot)
		{
			Id = id;
			PlateName = plateName;
			SnapShot = snapShot;

		}
        public PlateSettings Settings { get; set; }
        public Guid Id { get; set; }
        public string PlateName { get; set; }

        public bool IsValid
        {
            get
            {
                //Settings?.SetReadModeForValidation(Mode.Fl);
                if (SnapShot == null) return true;

                //foreach (var snap in SnapShot)
                //{
                //    if (snap.Settings.MeasurementMode == Mode.Abs)
                //    {
                //        Settings.SetReadModeForValidation(snap.Settings.MeasurementMode);
                //        if (Settings.Plate != null && !Settings.Plate.PlateSpecification.IsClearBottom)
                //            return false;
                //    }
                //}

                return SnapShot.All(s => s.Settings.IsValid) && Settings.IsValid;
            } set { ; }
        } 
	    public bool HasData
	    {
	        get
	        {
	            return SnapShot != null && SnapShot.Any(snapshot => snapshot.HasData);
	        }
	    }
	    public Plate Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as Plate;
        }

	    public int Columns
	    {
		    get { return Settings.Plate.PlateSpecification.NumberOfColumns; }
	    }

	    public int Rows
	    {
		    get { return Settings.Plate.PlateSpecification.NumberOfRows; }
	    }

	    public double MinValue
	    {
            get
            {
                mMinValue = double.MaxValue;
                if (SnapShot == null) return mMinValue;

                foreach (var tempMin in SnapShot.Select(snapshot => snapshot?.MinValue ?? 0).Where(tempMin => mMinValue > tempMin))
                {
                    mMinValue = tempMin;
                }
                if (Math.Abs(mMinValue - double.MaxValue) < double.Epsilon) mMinValue = 0;

                return mMinValue;
            }
	    }

        public double MaxValue
        {
            get
            {
                mMaxValue = 0;
                if (SnapShot == null) return mMaxValue;

                foreach (var tempMax in SnapShot.Select(snapshot => snapshot?.MaxValue ?? 0).Where(tempMax => mMaxValue < tempMax))
                {
                    mMaxValue = tempMax;
                }
                return mMaxValue;
            }
        }

        public List<Snapshot> SnapShot
        {
            get; set;
        }

        public List<SettingsSummary> GetSummary => Settings != null ? new List<SettingsSummary>(Settings.GetSummary) : new List<SettingsSummary>();

        //Exponential:                      e2          1.05e 04
        //decimal digits, group separators: N to N3     1,234 -> 1,234.567
        //Short form                        short       4k7   3M2
        //Number of significant digits      G1..Gn      4..4700.1 
        public string NumberFormat { get; set; } = "N3";
	}
}