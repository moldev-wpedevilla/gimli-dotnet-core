﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using Gimli.JsonObjects.Utils;

namespace Gimli.JsonObjects.Labware
{
    public class MicroplateLibrary
    {
        private Collection<Microplate> mMicroPlates = new Collection<Microplate>();
        private MicroplateLibrary mInternalPlateLibrary;
        #region ctor
        public Microplate GenerateDefaultPlate()
        {
            return GetPlates(96).FirstOrDefault();
        }
        public MicroplateLibrary()
        {}
        public MicroplateLibrary(string fileName)
        {
            var serializer = new PlateLibrarySerializer();
            var library = serializer.LoadPlateLibraryFromResources();
            if (library != null && library.Plates.Count > 0)
                mMicroPlates = library.Plates;

            if (string.IsNullOrEmpty(fileName)) return;
            if (File.Exists(DataSetHelper.RootPath + fileName))
                mInternalPlateLibrary = serializer.LoadXmlPlateLibrary(DataSetHelper.RootPath + fileName);
        }
        #endregion ctor

        public MicroplateLibrary InternaLibrary => mInternalPlateLibrary;
        public static double MaxHeightAllowed { get; set; }
        #region properties		

        public Collection<Microplate> Plates
        {
            get { return mMicroPlates; }
            set { mMicroPlates = value; }
        }

        #endregion properties

        #region methods

        /// <summary>
        /// SetInternalPlateLibrary - replaces the internal library
        /// </summary>
        public void SetInternalPlateLibrary(MicroplateLibrary library)
        {
            mInternalPlateLibrary = library;
        }

       
        #region get microplates

        #region get specific plate
        
        private Microplate GetMicroplate(Func<Microplate, bool> whereClause)
        {
            var microPlates = mMicroPlates.Where(whereClause);
            return microPlates.Any() ? microPlates.ToList()[0] : null;
        }

        #endregion get specific plate

        #region get list of plates...

        
        /// <summary>
        /// GetPlates -- returns a list of microplates with the specified number of wells
        /// </summary>
        public Collection<Microplate> GetAllPlates(int wellNumber)
        {
            // try to get list from embedded library first (if any)
            var microPlatesCollection = (mInternalPlateLibrary == null)
                ? new Collection<Microplate>()
                : mInternalPlateLibrary.GetAllPlates(wellNumber);

            var microplates = from microPlate in mMicroPlates
                where microPlate.PlateSpecification.NumberOfWells == wellNumber
                select microPlate;
            foreach (var microplate in microplates)
            {
                if (!microPlatesCollection.Contains(microplate))
                    microPlatesCollection.Add(microplate);
            }
            return microPlatesCollection;
        }

        /// <summary>
        /// GetPlates -- returns a list of microplates with the specified number of wells
        /// </summary>
        public Collection<Microplate> GetPlates(int wellNumber)
        {
            return GetAllPlates(wellNumber);
        }

        /// <summary>
        /// GetGroups
        /// </summary>
        public Collection<string> GetMicroplateGroups()
        {
            // try to get list from embedded library first (if any)
            var allGroups = (mInternalPlateLibrary == null)
                ? new Collection<string>()
                : mInternalPlateLibrary.GetMicroplateGroups();

            var comparer = new IgnoreCaseStringComparer();
            foreach (var microplate in mMicroPlates)
            {
                if (!allGroups.Contains(microplate.MicroplateGroup, comparer))
                    allGroups.Add(microplate.MicroplateGroup);
            }

            return allGroups;
        }

        #endregion get list of plates...

        #endregion get microplates

        #region deprecated, can be reomved once Paradigm starts using the new methods above
        public Microplate GetMicroplateByNameAndSize(int size, string plateName)
        {
            Func<Microplate, bool> whereClause = m => m.MicroplateName.Equals(MapToNewMicroplateName(plateName), StringComparison.OrdinalIgnoreCase)
                                                      && m.PlateSpecification.NumberOfWells == size;
            return GetMicroplate(whereClause);
        }
        // note: currently only used in unit and fit tests,
        // note: can remove later with the others below, not much value in keeping this around
        /// <summary>
        /// GetPlatesFilteredByManufacturer
        /// </summary>
        public Collection<Microplate> GetPlatesFilteredByManufacturer(string manufacturerName)
        {
            // try to get list from embedded library first (if any)
            var filteredPlates = (mInternalPlateLibrary == null)
                ? new Collection<Microplate>()
                : mInternalPlateLibrary.GetPlatesFilteredByManufacturer(manufacturerName);

            var microplates = from microPlate in mMicroPlates
                where (microPlate.Manufacturer.Equals(manufacturerName, StringComparison.OrdinalIgnoreCase))
                select microPlate;

            foreach (var microplate in microplates)
            {
                if (!filteredPlates.Contains(microplate))
                    filteredPlates.Add(microplate);
            }

            return filteredPlates;
        }

        // note: this can be removed later once Paradigm is in sync
        /// <summary>
        /// GetAllWellType -- returns a list of "number of wells" for all plates
        /// </summary>
        public Collection<int> GetAllWellType()
        {
            // try to get list from embedded library first (if any)
            var wellNumberCollection = (mInternalPlateLibrary == null)
                ? new Collection<int>()
                : mInternalPlateLibrary.GetAllWellType();

            var wellNumbers = (from microplate in mMicroPlates
                orderby microplate.PlateSpecification.NumberOfWells
                select microplate.PlateSpecification.NumberOfWells).Distinct();
            foreach (var wellNumber in wellNumbers)
            {
                if (!wellNumberCollection.Contains(wellNumber))
                    wellNumberCollection.Add(wellNumber);
            }
            return wellNumberCollection;
        }

        #endregion deprecated...

        #endregion methods

        #region add,remove plate(s)
        /// <summary>
        /// AddMicroplate(Microplate) - add a microplate to the plate library
        /// * We do not add/remove any item(s) from an internal library
        /// </summary>
        public bool SaveMicroplate(Microplate inPlate)
        {
            if (inPlate == null)
                return false;

            // temporary block until SMP6 can handle larger plate controls
            if (inPlate.PlateSpecification.NumberOfWells > 1536)
            {
                //MessageBox.Show(Properties.Resources.PlateLibrary_MessageBox);
                return false;
            }

            var plateToSave = new Microplate();
            plateToSave.CopyFrom(inPlate);

            var plate = FindMicroplate(inPlate);
            if (plate != null)
            {
                // just replace it...
                var index = mMicroPlates.IndexOf(plate);
                mMicroPlates[index] = plateToSave;
            }
            else
            {
                mMicroPlates.Add(plateToSave);
            }
            return true;
        }

        /// <summary>
        /// RemoveMicroplate(Microplate) - Remove specified plate
        /// * We do not add/remove any item(s) from an internal library
        /// </summary>
        public bool RemoveMicroplate(Microplate inPlate)
        {
            if (inPlate == null)
                return false;

            var plate = FindMicroplate(inPlate);
            if (plate != null)
            {
                mMicroPlates.Remove(plate);
                return true;
            }
            return false;
        }

        private Microplate FindMicroplate(Microplate inPlate)
        {
            var microPlates = from microPlate in mMicroPlates
                              where microPlate.MicroplateName.Equals(inPlate.MicroplateName, StringComparison.OrdinalIgnoreCase)
                                    && microPlate.BelongsToMicroplateGroup(inPlate.MicroplateGroup)
                              select microPlate;

            return microPlates.Any() ? microPlates.ToList()[0] : null;
        }

        #endregion add,remove plate(s)

        #region get microplates
        #region get specific plate

        /// <summary>
        /// GetDefaultPlateInfo -- returns the default microplate with the specified plate name
        /// </summary>
        public bool IsPlateWithNameAlreadyInLibrary(string plateName)
        {
            return (mInternalPlateLibrary.GetMicroplateByName(plateName) != null);
        }

        /// <summary>
        /// GetFirstPlateInfo -- returns the microplate with the specified plate name
        /// </summary>
        public Microplate GetFirstPlateInfo(string plateName, string microplateGroup)
        {
            // 5.x - if a custom plate uses the same name as an internal plate,
            //		 the information of that internal plate will be retrieved.
            // we should try looking at customized platelibrary first
            var plate = GetMicroplateByNameAndGroup(plateName, microplateGroup);
            if ((plate == null) && (mInternalPlateLibrary != null))
                plate = mInternalPlateLibrary.GetMicroplateByNameAndGroup(plateName, microplateGroup);
            return plate;
        }

        /// <summary>
        /// GetFirstPlateInfo -- returns the microplate with the specified plate name
        ///	* This is used for 5.x import where we use the name to get the plate *
        /// </summary>
        private Microplate GetFirstPlateInfo(string plateName)
        {
            // we should try looking at customized platelibrary first
            var plate = GetMicroplateByName(plateName);
            if ((plate == null) && (mInternalPlateLibrary != null))
                plate = mInternalPlateLibrary.GetMicroplateByName(plateName);
            return plate;
        }

        /// <summary>
        /// GetBestMatchedMicroplateForGroup -- returns the microplate that best matches the
        ///			input parameters. If the number of wells is not supported, then get the
        ///			first microplate found for the specified group. We should allow the app
        ///			to crash if there are no microplate defined for the given group/reader.
        /// 
        ///			5.x -- when changing connected reader, uses WellsToIDFor[instrument.name](int)
        ///			--> it just gets the first supported plate with the specified number of wells.
        /// </summary>
        public Microplate GetBestMatchedMicroplateForGroup(int wellNumber, string microplateGroup)
        {
            // try if we get a plate the matches both number of wells and group
            var plate = GetFirstPlateInfo(wellNumber, microplateGroup) ?? GetFirstPlateInfoForGroup(microplateGroup);
            return plate;
        }

        public Microplate GetOptimizedOrFirstPlate(string plateName, string groupName)
        {
            var microPlate = (GetOptimizedMicroplate(plateName, groupName) ??
                              GetFirstPlateInfo(plateName, groupName)) ?? GetFirstPlateInfo(plateName);
            return microPlate;
        }

        /// <summary>
        /// GetOptimizedMicroplate -- returns the microplate with the specified plate name
        /// </summary>
        public Microplate GetOptimizedMicroplate(string plateName, string microplateGroup)
        {
            return GetMicroplateByNameAndGroup(plateName, microplateGroup);
        }

        /// <summary>
        /// GetOptimizedMicroplate -- returns the microplate with the number of wells
        /// </summary>
        public Microplate GetOptimizedMicroplate(int wellNumber, string microplateGroup)
        {
            return GetMicroplateByWellsAndGroup(wellNumber, microplateGroup);
        }


        /// <summary>
        /// GetFirstPlateInfo -- returns the first microplate with the specified number of wells.
        ///			5.x -- when changing connected reader, uses WellsToIDFor[instrument.name](int)
        ///			--> it just gets the first supported plate with the specified number of wells.
        /// </summary>
        public Microplate GetFirstPlateInfo(int wellNumber, string microplateGroup)
        {
            // we should try looking at customized platelibrary first
            var plate = GetMicroplateByWellsAndGroup(wellNumber, microplateGroup);
            if ((plate == null) && (mInternalPlateLibrary != null))
                plate = mInternalPlateLibrary.GetMicroplateByWellsAndGroup(wellNumber, microplateGroup);
            return plate;
        }

        /// <summary>
        /// GetFirstPlateInfo -- returns the first microplate with the specified number of wells.
        ///			5.x -- when changing connected reader, uses WellsToIDFor[instrument.name](int)
        ///			--> it just gets the first supported plate with the specified number of wells.
        /// </summary>
        private Microplate GetFirstPlateInfoForGroup(string microplateGroup)
        {
            // we should try looking at customized platelibrary first
            var plate = GetMicroplateByGroup(microplateGroup);
            if ((plate == null) && (mInternalPlateLibrary != null))
                plate = mInternalPlateLibrary.GetMicroplateByGroup(microplateGroup);
            return plate;
        }

        // basic methods to find a microplate that matches the input parameters
        private Microplate GetMicroplateByNameAndGroup(string plateName, string microplateGroup)
        {
            //var microPlates = from microPlate in mMicroPlates
            //                where (microPlate.MicroplateName.Equals(plateName, StringComparison.OrdinalIgnoreCase)
            //                       && microPlate.BelongsToMicroplateGroup(microplateGroup))
            //                select microPlate;
            Func<Microplate, bool> whereClause = m => m.MicroplateName.Equals(MapToNewMicroplateName(plateName), StringComparison.OrdinalIgnoreCase)
                                                      && m.BelongsToMicroplateGroup(microplateGroup);
            return GetMicroplate(whereClause);
        }
        private Microplate GetMicroplateByWellsAndGroup(int wellNumber, string microplateGroup)
        {
            //var microPlates = from microPlate in mMicroPlates
            //                  where microPlate.PlateSpecification.NumberOfWells == wellNumber
            //                        && microPlate.BelongsToMicroplateGroup(microplateGroup)
            //                  select microPlate;
            Func<Microplate, bool> whereClause = m => m.PlateSpecification.NumberOfWells == wellNumber
                                                      && m.BelongsToMicroplateGroup(microplateGroup);
            return GetMicroplate(whereClause);
        }
        private Microplate GetMicroplateByGroup(string microplateGroup)
        {
            //var microPlates = from microPlate in mMicroPlates
            //                  where microPlate.BelongsToMicroplateGroup(microplateGroup)
            //                  select microPlate;
            Func<Microplate, bool> whereClause = m => m.BelongsToMicroplateGroup(microplateGroup);
            return GetMicroplate(whereClause);
        }
        private Microplate GetMicroplateByName(string microplateName)
        {
            //var microPlates = from microPlate in mMicroPlates
            //                  where microPlate.MicroplateName.Equals(microplateName, StringComparison.OrdinalIgnoreCase)
            //                  select microPlate;
            Func<Microplate, bool> whereClause = m => m.MicroplateName.Equals(MapToNewMicroplateName(microplateName), StringComparison.OrdinalIgnoreCase);
            return GetMicroplate(whereClause);
        }

        public Microplate GetUpdatedPlateIfChanged(Microplate plate, string microplateGroup)
        {
            if (plate == null)
                return null;

            //reload plate to get new dimensions if plate was modified after saving protocol
            var libraryPlate = GetFirstPlateInfo(plate.MicroplateName, microplateGroup);

            if ((libraryPlate != null) && !plate.Equals(libraryPlate))
            {
                return libraryPlate;
            }

            return plate;
        }
        public static string MapToNewMicroplateName(string microplateName)
        {
            var newName = microplateName.Replace("Blak", "Blok");
            newName = newName.Replace("Well MDC HE", "Well MD HE");
            newName = newName.Replace("µMax 24 Well Low Volume", "SpectraDrop 24 Well Low Volume");
            newName = newName.Replace("µMax 64 Well Low Volume", "SpectraDrop 64 Well Low Volume");
            newName = newName.Replace("96 Well Corning Half Area flat btm", "96 Well Corning Half Area opaque");

            return newName;
        }

        #endregion get specific plate

        #region get list of plates...
        /// <summary>
        /// GetAllWellType -- returns a list of "number of wells" for all plates
        /// </summary>
        public Collection<int> GetAllWellType(string microplateGroup)
        {
            // try to get list from embedded library first (if any)
            var wellNumberCollection = (mInternalPlateLibrary == null) ?
                new Collection<int>() : mInternalPlateLibrary.GetAllWellType(microplateGroup);

            var wellNumbers = (from microplate in mMicroPlates
                               orderby microplate.PlateSpecification.NumberOfWells
                               where microplate.BelongsToMicroplateGroup(microplateGroup)
                               select microplate.PlateSpecification.NumberOfWells).Distinct();
            foreach (var wellNumber in wellNumbers)
            {
                if (!wellNumberCollection.Contains(wellNumber))
                    wellNumberCollection.Add(wellNumber);
            }
            return wellNumberCollection;
        }

        /// <summary>
        /// GetPlateNames -- returns a list of plate names with the specified number of wells
        /// </summary>
        public Collection<string> GetPlateNames(int wellNumber, string microplateGroup)
        {
            // try to get list from embedded library first (if any)
            var wellNamesCollection = (mInternalPlateLibrary == null) ?
                new Collection<string>() : mInternalPlateLibrary.GetPlateNames(wellNumber, microplateGroup);

            var wellNames = from microPlate in mMicroPlates
                            where microPlate.PlateSpecification.NumberOfWells == wellNumber
                                  && microPlate.BelongsToMicroplateGroup(microplateGroup)
                            select microPlate.MicroplateName;

            foreach (var wellName in wellNames)
            {
                if (!wellNamesCollection.Contains(wellName, new IgnoreCaseStringComparer()))
                    wellNamesCollection.Add(wellName);
            }

            var sortedArray = wellNamesCollection.OrderBy(a => a, new CompareMicroplateName());
            return new Collection<string>(sortedArray.ToList());
        }

        public IEnumerable<Microplate> GetPlate(int wellNumber, string microplateGroup, string name)
        {
            return GetPlates(wellNumber, microplateGroup).Where(p => p.MicroplateName.Contains(name));
        }

        /// <summary>
        /// GetPlates -- returns a list of microplates with the specified number of wells
        /// </summary>
        public Collection<Microplate> GetPlates(int wellNumber, string microplateGroup)
        {
            if (MaxHeightAllowed < 1.0) MaxHeightAllowed = 22.0;
            // try to get list from embedded library first (if any)
            var microPlatesCollection = (mInternalPlateLibrary == null) ?
                new Collection<Microplate>() : mInternalPlateLibrary.GetPlates(wellNumber, microplateGroup);

            var microplates = from microPlate in mMicroPlates
                              where microPlate.PlateSpecification.NumberOfWells == wellNumber
                                    && microPlate.BelongsToMicroplateGroup(microplateGroup)
                                    && microPlate.PlateSpecification.PlateHeight <= MaxHeightAllowed * 1000
                              select microPlate;
            foreach (var microplate in microplates)
            {
                if (microPlatesCollection.Contains(microplate))
                    continue;

                var exists = false;
                foreach (var existingPlate in microPlatesCollection)
                {
                    if (existingPlate.MicroplateName.ToLowerInvariant() == microplate.MicroplateName.ToLowerInvariant())
                    {
                        exists = true;
                        break;
                    }
                }
                if (!exists)
                    microPlatesCollection.Add(microplate);
            }
            return microPlatesCollection;
        }

        #endregion get list of plates...

        #endregion get microplates
    }

    public class IgnoreCaseStringComparer : IEqualityComparer<string>
    {
        public bool Equals(string x, string y)
        {
            return (x.Equals(y, StringComparison.OrdinalIgnoreCase));
        }

        public int GetHashCode(string obj)
        {
            return obj.GetHashCode();
        }
    }

    public class CompareMicroplateName : IComparer<string>
    {
        // Because the class implements IComparer, it must define a 
        // Compare method. The method returns a signed integer that indicates 
        // whether s1 > s2 (return is greater than 0), s1 < s2 (return is negative),
        // or s1 equals s2 (return value is 0). This Compare method compares strings. 
        public int Compare(string x, string y)
        {
            if (x == y)
                return 0;

            if (x.ToUpperInvariant().Contains("STANDARD CLR"))
                return -1;

            if (y.ToUpperInvariant().Contains("STANDARD CLR"))
                return 1;

            if (x.ToUpperInvariant().Contains("STANDARD"))
                return -1;

            if (y.ToUpperInvariant().Contains("STANDARD"))
                return 1;

            return string.Compare(x, y, true, CultureInfo.CurrentCulture);
        }
    }
}