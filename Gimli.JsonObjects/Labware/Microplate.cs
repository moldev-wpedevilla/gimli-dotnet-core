﻿using Gimli.JsonObjects.Settings;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Gimli.JsonObjects.Parameter;

namespace Gimli.JsonObjects.Labware
{
    [Serializable]
    public class Microplate : SettingsBase
    {
        public Microplate()
        {
            UserInformation = string.Empty;
            Name = "Microplate";
        }

        #region properties
        #region Validation Properties
        //private bool mIsValid;
        //private List<string> mErrorMessage = new List<string>();
        //public virtual bool IsValid => mIsValid;
        //public List<string> ErrorMessage => mErrorMessage;
        //public bool IsModified { get; set; }
        //public bool IsDisplayedInSummary { get; set; }
        //public string Description { get; set; }
        #endregion

        public string MicroplateID { get; set; }

        public string MicroplateName{get;set ; }

        public string MicroplateGroup { get; set; }

        public string Manufacturer { get; set; }

        public MicroplateSpecification PlateSpecification { get; set; }

        public string UserInformation { get; set; }
        #endregion properties

        public void CopyFrom(Microplate inPlate)
        {
            MicroplateName = inPlate.MicroplateName;
            MicroplateGroup = inPlate.MicroplateGroup;
            MicroplateID = inPlate.MicroplateID;
            Manufacturer = inPlate.Manufacturer;
            // don't copy OtherSupportedMicroplateGroups
            OtherSupportedMicroplateGroups = inPlate.OtherSupportedMicroplateGroups;
            PlateSpecification = new MicroplateSpecification();
            PlateSpecification.CopyFrom(inPlate.PlateSpecification);
        }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")] //Xml serialization
        public Collection<string> OtherSupportedMicroplateGroups { get; set; }
        internal bool BelongsToMicroplateGroup(string microplateGroup)
        {
            // check if this microplate is defined for the specified group
            bool canUseForGroup = MicroplateGroup.Equals(microplateGroup, StringComparison.OrdinalIgnoreCase);

            // if not, check if the specified group is in other supported groups
            if (!canUseForGroup && (OtherSupportedMicroplateGroups != null) && (OtherSupportedMicroplateGroups.Count > 0))
            {
                canUseForGroup = OtherSupportedMicroplateGroups.Contains(microplateGroup, new IgnoreCaseStringComparer());
            }
            return canUseForGroup;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = 1;
                hashCode = PlateSpecification.GetHashCode();
                if (MicroplateID != null)
                    hashCode ^= MicroplateID.GetHashCode();
                if (MicroplateName != null)
                    hashCode ^= MicroplateName.GetHashCode();
                if ((OtherSupportedMicroplateGroups != null) && (OtherSupportedMicroplateGroups.Count > 0))
                {
                    int groupHash = 1;
                    for (int index = 0; index < OtherSupportedMicroplateGroups.Count; index++)
                    {
                        groupHash += OtherSupportedMicroplateGroups[index].GetHashCode();
                    }
                    hashCode ^= groupHash;
                }
                if (Manufacturer != null)
                    hashCode ^= Manufacturer.GetHashCode();
                return hashCode;
            }
        }

        // OtherSupportedMicroplateGroups should not be part of equality check
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (GetType().UnderlyingSystemType != obj.GetType().UnderlyingSystemType)
            {
                return false;
            }
            Microplate compareObj = obj as Microplate;
            if (!PlateSpecification.Equals(compareObj.PlateSpecification))
            {
                return false;
            }
            if (MicroplateID != compareObj.MicroplateID ||
                MicroplateName != compareObj.MicroplateName ||
                (compareObj.Manufacturer != "Unknown" && Manufacturer != compareObj.Manufacturer))
            {
                return false;
            }
            // just checking (MicroplateGroup != compareObj.MicroplateGroup) is not enough
            if (string.IsNullOrEmpty(MicroplateGroup) || string.IsNullOrEmpty(compareObj.MicroplateGroup) ||
                (!compareObj.BelongsToMicroplateGroup(MicroplateGroup) && !BelongsToMicroplateGroup(compareObj.MicroplateGroup)))
            {
                return false;
            }
            return true;
        }

        public string GetChanges(Microplate compObj)
        {
            if (compObj == null)
                return string.Empty;
            return PlateSpecification.GetChanges(compObj.PlateSpecification);
        }

        public override List<SettingsSummary> GetSummary
        {
            get
            {
                var plateinfo = new SettingsSummary(Name);
                plateinfo.Items.Add(new ItemClass("Name", MicroplateName ?? "Microplate"));
                plateinfo.Items.Add(new ItemClass("Format", PlateSpecification?.NumberOfWells + " Wells"));
                return new List<SettingsSummary> { plateinfo }; }
        }

		public override IEnumerable<GimliParameter> GetProperties()
	    {
		    return new List<GimliParameter>();
	    }
    }
}
