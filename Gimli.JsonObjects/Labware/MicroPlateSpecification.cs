﻿using System;

namespace Gimli.JsonObjects.Labware
{
    [Serializable]
    public class MicroplateSpecification
    {
        private static readonly double DefaulteLidHeight = 3000d;
        //private static readonly double MaximalPlateHeight = 22000d;

        public MicroplateSpecification()
        {
            DateOptimized = DateTime.Now;
        }

        #region properties

        public int NumberOfWells { get; set; }

        public int NumberOfColumns { get; set; }

        public int NumberOfRows { get; set; }

        public double CenterOfFirstWellX { get; set; }
        //XPOS - horizontal offset of (center of) well A1 from left edge of the plate (in mm)

        public double CenterOfFirstWellY { get; set; }
        //YPOS - vertical offset of (center of) well A1 from top edge of the plate (in mm)

        public double DistanceBetweenWellsX { get; set; } //Horizontal distance (in x-axis) between wells

        public double DistanceBetweenWellsY { get; set; } //Vertical distance (in y-axis) between wells

        public double WellDiameter { get; set; }

        public double WellDepth { get; set; }

        public double PlateHeight { get; set; }

        public double WellVolume { get; set; } //Well Volume in microliter

        public double WellBottomThickness { get; set; } //Well bottom thickness in um

        public bool IsClearBottom { get; set; }
        public double PlateLength_X { get; set; }
        public double PlateLength_Y { get; set; }
        public double ReadingHeight { get; set; }

        private double mPlateHeightWithLid;
        public double PlateHeightWithLid
        {
            get {
                var height = mPlateHeightWithLid > 0 ? mPlateHeightWithLid : PlateHeight + DefaulteLidHeight;
                //height = height > MaximalPlateHeight ? MaximalPlateHeight : height;
                return height;
            }
            set { mPlateHeightWithLid=value; }
        }

        public string WellShape { get; set; }
        public string WellBottomShape { get; set; }
        public string Label { get; set; }
        public double WellLength { get; set; }
        public double WellWidth { get; set; }
        public int OptimizationTechnique { get; set; } //MeasureTech enum
        public int PlateOrientation { get; set; } //PlateOrientation enum
        public DateTime DateOptimized { get; set; }
        private bool mMaskPlateHeight = false;

        public bool MaskPlateHeight
        {
            get { return mMaskPlateHeight; }
            set { mMaskPlateHeight = value; }
        }

        #endregion properties


        #region methods

        public void CopyFrom(MicroplateSpecification specifications)
        {
            NumberOfWells = specifications.NumberOfWells;
            NumberOfColumns = specifications.NumberOfColumns;
            NumberOfRows = specifications.NumberOfRows;
            CenterOfFirstWellX = specifications.CenterOfFirstWellX;
            CenterOfFirstWellY = specifications.CenterOfFirstWellY;
            DistanceBetweenWellsX = specifications.DistanceBetweenWellsX;
            DistanceBetweenWellsY = specifications.DistanceBetweenWellsY;
            WellDiameter = specifications.WellDiameter;
            WellDepth = specifications.WellDepth;
            PlateHeight = specifications.PlateHeight;
            WellVolume = specifications.WellVolume;
            WellBottomThickness = specifications.WellBottomThickness;
            IsClearBottom = specifications.IsClearBottom;

            // we will need to copy the extended properties
            PlateLength_X = specifications.PlateLength_X;
            PlateLength_Y = specifications.PlateLength_Y;
            ReadingHeight = specifications.ReadingHeight;
            PlateHeightWithLid = specifications.PlateHeightWithLid;
            WellShape = specifications.WellShape;
            WellBottomShape = specifications.WellBottomShape;
            Label = specifications.Label;
            WellLength = specifications.WellLength;
            WellWidth = specifications.WellWidth;
            OptimizationTechnique = specifications.OptimizationTechnique;
            PlateOrientation = specifications.PlateOrientation;
	        DateOptimized = specifications.DateOptimized;
        }

        public override int GetHashCode()
        {
            int hashCode = 1;
            hashCode ^= NumberOfWells;
            hashCode ^= NumberOfColumns;
            hashCode ^= NumberOfRows;
            hashCode ^= CenterOfFirstWellX.GetHashCode();
            hashCode ^= CenterOfFirstWellY.GetHashCode();
            hashCode ^= DistanceBetweenWellsX.GetHashCode();
            hashCode ^= DistanceBetweenWellsY.GetHashCode();
            hashCode ^= WellDiameter.GetHashCode();
            hashCode ^= WellDepth.GetHashCode();
            hashCode ^= PlateHeight.GetHashCode();
            hashCode ^= WellVolume.GetHashCode();
            hashCode ^= WellBottomThickness.GetHashCode();
            hashCode ^= IsClearBottom.GetHashCode();

            hashCode ^= PlateLength_X.GetHashCode();
            hashCode ^= PlateLength_Y.GetHashCode();
            hashCode ^= ReadingHeight.GetHashCode();
            hashCode ^= PlateHeightWithLid.GetHashCode();
            if (WellShape != null)
                hashCode ^= WellShape.GetHashCode();
            if (WellBottomShape != null)
                hashCode ^= WellBottomShape.GetHashCode();
            if (Label != null)
                hashCode ^= Label.GetHashCode();
            hashCode ^= WellLength.GetHashCode();
            hashCode ^= WellWidth.GetHashCode();
            hashCode ^= OptimizationTechnique.GetHashCode();
            hashCode ^= PlateOrientation.GetHashCode();

            return hashCode;
        }

        public override bool Equals(object obj)
        {
            var compareObj = obj as MicroplateSpecification;
            if (compareObj == null)
            {
                return false;
            }

            if ((NumberOfWells != compareObj.NumberOfWells) ||
                (NumberOfColumns != compareObj.NumberOfColumns) ||
                (NumberOfRows != compareObj.NumberOfRows) ||
                Math.Abs(CenterOfFirstWellX - compareObj.CenterOfFirstWellX) > Double.Epsilon ||
                Math.Abs(CenterOfFirstWellY - compareObj.CenterOfFirstWellY) > Double.Epsilon ||
                Math.Abs(DistanceBetweenWellsX - compareObj.DistanceBetweenWellsX) > Double.Epsilon ||
                Math.Abs(DistanceBetweenWellsY - compareObj.DistanceBetweenWellsY) > Double.Epsilon ||
                Math.Abs(WellDiameter - compareObj.WellDiameter) > Double.Epsilon ||
                compareObj.WellDepth > 0 && Math.Abs(WellDepth - compareObj.WellDepth) > Double.Epsilon ||
                !compareObj.mMaskPlateHeight && Math.Abs(PlateHeight - compareObj.PlateHeight) > Double.Epsilon ||
                Math.Abs(WellBottomThickness - compareObj.WellBottomThickness) > Double.Epsilon ||
                IsClearBottom != compareObj.IsClearBottom)
            {
                return false;
            }
            return true;
        }
        private static string GetChangeMessage(string name, object a, object b)
        {
            if (a == null || b == null)
                return string.Empty;

            if (a.Equals(b))
                return string.Empty;

            return string.Format(System.Globalization.CultureInfo.CurrentCulture, "{0} changed from {1} to {2}/r/n", name, a, b);
        }
        public string GetChanges(MicroplateSpecification compObj)
        {
            var msg = GetChangeMessage("NumberOfWells", NumberOfWells, compObj.NumberOfWells);
            msg += GetChangeMessage("NumberOfColumns", NumberOfColumns, compObj.NumberOfColumns);
            msg += GetChangeMessage("NumberOfRows", NumberOfRows, compObj.NumberOfRows);
            msg += GetChangeMessage("CenterOfFirstWellX", CenterOfFirstWellX, compObj.CenterOfFirstWellX);
            msg += GetChangeMessage("CenterOfFirstWellY", CenterOfFirstWellY, compObj.CenterOfFirstWellY);
            msg += GetChangeMessage("DistanceBetweenWellsX", DistanceBetweenWellsX, compObj.DistanceBetweenWellsX);
            msg += GetChangeMessage("DistanceBetweenWellsY", DistanceBetweenWellsY, compObj.DistanceBetweenWellsY);
            msg += GetChangeMessage("WellDiameter", WellDiameter, compObj.WellDiameter);
            msg += GetChangeMessage("WellDepth", WellDepth, compObj.WellDepth);
            msg += GetChangeMessage("PlateHeight", PlateHeight, compObj.PlateHeight);
            msg += GetChangeMessage("WellVolume", WellVolume, compObj.WellVolume);
            msg += GetChangeMessage("WellBottomThickness", WellBottomThickness, compObj.WellBottomThickness);
            msg += GetChangeMessage("PlateLength_X", PlateLength_X, compObj.PlateLength_X);
            msg += GetChangeMessage("PlateLength_Y", PlateLength_Y, compObj.PlateLength_Y);
            msg += GetChangeMessage("ReadingHeight", ReadingHeight, compObj.ReadingHeight);
            msg += GetChangeMessage("PlateHeightWithLid", PlateHeightWithLid, compObj.PlateHeightWithLid);
            msg += GetChangeMessage("WellShape", WellShape, compObj.WellShape);
            msg += GetChangeMessage("WellBottomShape", WellBottomShape, compObj.WellBottomShape);
            msg += GetChangeMessage("Label", Label, compObj.Label);
            msg += GetChangeMessage("WellLength", WellLength, compObj.WellLength);
            msg += GetChangeMessage("WellWidth", WellWidth, compObj.WellWidth);
            msg += GetChangeMessage("OptimizationTechnique", OptimizationTechnique, compObj.OptimizationTechnique);
            msg += GetChangeMessage("PlateOrientation", PlateOrientation, compObj.PlateOrientation);
            msg += GetChangeMessage("NumberOfWells", NumberOfWells, compObj.NumberOfWells);
            msg += GetChangeMessage("NumberOfWells", NumberOfWells, compObj.NumberOfWells);

            return msg;
        }

        #endregion methods
    }
}


