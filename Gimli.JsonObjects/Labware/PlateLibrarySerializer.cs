﻿using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Gimli.JsonObjects.DummyData;

namespace Gimli.JsonObjects.Labware
{
    internal class PlateLibrarySerializer
    {
        private object mLocker = new object();
        public MicroplateLibrary LoadPlateLibraryFromResources()
        {
            lock (mLocker)
            {
                // load internal plate library from Resources...
                Assembly thisAssembly = Assembly.GetExecutingAssembly();
                Stream plateLibraryStream = thisAssembly.GetManifestResourceStream(
                    "Gimli.JsonObjects.Resources.MaxlineMicroplateLibrary.xml");
                return LoadXmlPlateLibrary(plateLibraryStream);
            }
        }

        public MicroplateLibrary LoadXmlPlateLibrary(Stream stream)
        {
            lock (mLocker)
            {
                if (stream == null)
                    return null;
                TextReader reader = new StreamReader(stream);
                var xmlSerializer = new XmlSerializer(typeof(MicroplateLibrary));
                var library = (MicroplateLibrary)xmlSerializer.Deserialize(reader);
                reader.Close();

                RemoveWellAmountAndWellText(library);

                return library;
            }
        }

        private void RemoveWellAmountAndWellText(MicroplateLibrary library)
        {
            GimliTrace.Start("Library");
            foreach (var plate in library.Plates)
            {
                plate.MicroplateName = plate.MicroplateName.Replace($"{plate.PlateSpecification.NumberOfWells} Well ", "");
            }
            GimliTrace.Stop("Library");
        }

        /// <summary>
		/// LoadPlateLibrary - load the internal Xml format plate library
		/// </summary>
		public MicroplateLibrary LoadXmlPlateLibrary(string fileName)
        {
            lock (mLocker)
            {
                if (string.IsNullOrEmpty(fileName) || !File.Exists(fileName))
                    return null;

                var reader = new StreamReader(fileName);
                return LoadXmlPlateLibrary(reader.BaseStream);
            }
        }

        /// <summary>
        /// SaveXmlPlateLibrary -- save MicroplateLibrary to Xml file.
        /// </summary>
        public bool SaveXmlPlateLibrary(string fileName, MicroplateLibrary library)
        {
            lock (mLocker)
            {
                if (string.IsNullOrEmpty(fileName) || !File.Exists(fileName))
                    return false;

                Stream stream = new FileStream(fileName, FileMode.Create);
                return SaveXmlPlateLibrary(stream, library);
            }
        }

        /// <summary>
        /// SaveXmlPlateLibrary -- save MicroplateLibrary to Xml file.
        /// </summary>
        private bool SaveXmlPlateLibrary(Stream stream, MicroplateLibrary library)
        {
            lock (mLocker)
            {
                if (stream == null)
                    return false;

                var writer = new XmlTextWriter(stream, Encoding.Unicode) { Formatting = Formatting.Indented };
                var xmlSerializer = new XmlSerializer(typeof(MicroplateLibrary));
                xmlSerializer.Serialize(writer, library);
                writer.Close();
                return true;
            }
        }
    }
}
