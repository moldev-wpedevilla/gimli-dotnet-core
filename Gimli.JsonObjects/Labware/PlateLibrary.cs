﻿using Gimli.JsonObjects.DummyData;

namespace Gimli.JsonObjects.Labware
{
    public static class PlateLibrary
    {
        private static MicroplateLibrary mPlateLibrary;

        public static MicroplateLibrary MicroplateLibrary
        {
            get
            {
                GimliTrace.Start("MicroplateLibrary");
                if (mPlateLibrary == null) mPlateLibrary = new MicroplateLibrary(string.Empty);
                GimliTrace.Stop("MicroplateLibrary");
                return mPlateLibrary;
            }
        }
    }
}
