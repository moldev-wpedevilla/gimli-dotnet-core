﻿using System;
using Gimli.JsonObjects.DummyData;
using Gimli.JsonObjects.Enums;
using Gimli.JsonObjects.Serialize;
using Gimli.JsonObjects.Settings;
using Gimli.JsonObjects.Utils;
using Gimli.JsonObjects.Datasets;

namespace Gimli.JsonObjects.Save
{
    public class SaveDocumentToDataSet
    {
        public static DocumentDs Save(Document document)
        {
            var docObj = new SaveDocumentToDataSet();
            return docObj.SaveDocument(document);
        }

        public DocumentDs SaveDocument(Document document)
        {
            var ds = new DocumentDs();
	        var version = Document.GetVersion().ToString();
            document.Category = document.HasData ? ItemType.Result.ToString() : document.Category;

            var docRow = ds.DocumentGeneral.NewDocumentGeneralRow();
            docRow.Id = document.Id;
            docRow.ProtoclName = document.ProtocolName;
            docRow.DisplayName = document.DisplayName == string.Empty ? "NoName" : document.DisplayName;
            docRow.Timestamp = document.TimeStamp;
            docRow.IsSynchronized = document.IsSynchronized;
            docRow.HasData = document.HasData;
            docRow.NumberOfPlates = document.NumberOfPlates;
            docRow.PercentMeasured = document.PercentMeasured;
            docRow.UserId = document.UserNameGuid;
            docRow.Notes = document.Notes;
            docRow.Version = version;
            docRow.IsProtected = document.IsProtected;
            docRow.LastUsedDate = document.LastUsedDate.Ticks;
            docRow.IsPinned = document.IsPinned;
	        docRow.PinSlot = document.PinSlot;
	        docRow.PinName = document.PinName;
            docRow.Category = document.Category;
            docRow.PinDate = document.PinDate.Ticks;
            ds.DocumentGeneral.AddDocumentGeneralRow(docRow);
			
            foreach (var experiment in document.Experiments)
            {
                var expRow = ds.Experiments.AddExperimentsRow(docRow, experiment.Id, experiment.ExpName);
                foreach (var plate in experiment.Plates)
                {
                    var pltRow = ds.Plates.AddPlatesRow(expRow, plate.Id, plate.PlateName);
                    AddMicroplateInformation(pltRow, plate.Settings, ds);

                    var snapShotNumber = 0;
                    foreach (var snapShot in plate.SnapShot)
                    {
                        long dimSize;

                        var dimSerData = DocumentSerialization.SerializeAndZip(snapShot.DataDimensionInfo, out dimSize);
                        var snapShotRow = ds.SnapShots.AddSnapShotsRow(pltRow, 
                            snapShot.ReducedMinValue, snapShot.MinValue, snapShot.ReducedMaxValue, snapShot.MaxValue, snapShot.MinXPos,
                            snapShot.MaxXPos, snapShot.IncrementX, snapShot.MinYPos, snapShot.MaxYPos,
                            snapShot.IncrementY, dimSerData, (int)dimSize, snapShotNumber++, null, 0,
                            new byte[0], snapShot.WellBasedSerialization, snapShot.SnapshotDataName, snapShot.Id);
                        GimliTrace.Start("Serialization");
                        if (snapShot.WellBasedSerialization)
                        {
                            AddAllWellsToDataSet(snapShot, snapShotRow, ds);
                        }
                        else
                        {
                            long size;
                            snapShotRow.SerializedData = RawBinaryWriter.Serialize(snapShot.Wells, out size);
                            snapShotRow.Size = size;
                            snapShotRow.MDHash = Cryptology.CreateHashCode(snapShotRow.SerializedData);
                        }
                        GimliTrace.ElapsedRestart("Serialization", "Serialization Data");
						AddSettingsInformation(snapShotRow, snapShot.Settings, ds);
                        AddWavelengthsInformation(snapShotRow, snapShot.Settings, ds);
                        AddDetectionSettingInformation(snapShotRow, snapShot.Settings, ds);
                        AddKineticInformation(snapShotRow, snapShot.Settings, ds);
                        AddShakeInformation(snapShotRow, snapShot.Settings, ds);
                        AddReadAreaInformation(snapShotRow, snapShot.Settings, ds);
                        AddWellScanInformation(snapShotRow, snapShot.Settings, ds);
                        AddReductionSettingsInformation(snapShotRow, snapShot.ReductionSettings, ds);
                        GimliTrace.Stop("Serialization", "Serialization Settings");
					}
                }
            }
            ds.AcceptChanges();

            return ds;
        }

        private void AddReadAreaInformation(DocumentDs.SnapShotsRow snapRow, MeasurementSettings settings, DocumentDs ds)
        {
            if (settings?.ReadArea == null) return;
            ds.SnapshotSettings.AddSnapshotSettingsRow(snapRow, "ReadArea", DocumentSerialization.Serialize(settings.ReadArea));
        }

       private void AddMicroplateInformation(DocumentDs.PlatesRow pltRow, PlateSettings settings, DocumentDs ds)
        {
            if (settings == null) return;

            var sRow = ds.Settings.AddSettingsRow(pltRow);
            ds.SettingsParameter.AddSettingsParameterRow(sRow, "IsLidded", SerializeParameter(settings.IsLidded));
            ds.SettingsParameter.AddSettingsParameterRow(sRow, "Orientation", SerializeParameter(settings.Orientation));
            ds.SettingsParameter.AddSettingsParameterRow(sRow, "MicroplateName", SerializeParameter(settings.MicroplateName));
            ds.SettingsParameter.AddSettingsParameterRow(sRow, "WellCount", SerializeParameter(settings.WellCount));
            ds.SettingsParameter.AddSettingsParameterRow(sRow, "Barcode", SerializeParameter(settings.Barcode));
            ds.SettingsParameter.AddSettingsParameterRow(sRow, "UserName", SerializeParameter(settings.UserName));
            ds.SettingsParameter.AddSettingsParameterRow(sRow, "InstrumentSerial", SerializeParameter(settings.InstrumentSerial));
            ds.SettingsParameter.AddSettingsParameterRow(sRow, "InstrumentName", SerializeParameter(settings.InstrumentName));
            ds.SettingsParameter.AddSettingsParameterRow(sRow, "MeasureDate", SerializeParameter(settings.MeasureDate));

            if (settings.Plate == null) return;
            ds.Microplate.AddMicroplateRow(sRow, DocumentSerialization.Serialize(settings.Plate));
        }

        private void AddAllWellsToDataSet(Snapshot snapShot, DocumentDs.SnapShotsRow snapShotRow, DocumentDs dataSet)
        {
            foreach (var well in snapShot.Wells)
            {
                long size;
                var serData = DocumentSerialization.SerializeAndZip(well, out size);
                dataSet.WellSerialization.AddWellSerializationRow(snapShotRow, serData,
                    Cryptology.CreateHashCode(serData), size, well.YPos, well.XPos);
            }
        }

        private void AddSettingsInformation(DocumentDs.SnapShotsRow snapRow, MeasurementSettings settings, DocumentDs ds)
        {
            if (settings == null) return;
            var serializeObject = new MeasurementSettingsSerialize(settings.MeasurementMode, settings.MeasurementType,
                settings.Unit) {IsModuleUsed = settings.IsModuleUsed, ModuleInformation = settings.ModuleInformation};
            ds.SnapshotSettings.AddSnapshotSettingsRow(snapRow, "Settings", DocumentSerialization.Serialize(serializeObject));
        }
		private void AddReductionSettingsInformation(DocumentDs.SnapShotsRow snapRow, DataReductionSettings settings, DocumentDs ds)
		{
			if (settings == null) return;
			ds.SnapshotSettings.AddSnapshotSettingsRow(snapRow, "ReductionSettings", DocumentSerialization.Serialize(settings));
		}

		private void AddWavelengthsInformation(DocumentDs.SnapShotsRow snapRow, MeasurementSettings settings, DocumentDs ds)
        {
            //if (settings?.Wavelengths == null) return;
            //var stream = settings.Wavelengths.Serialize();
            //ds.SnapshotSettings.AddSnapshotSettingsRow(snapRow, "Wavelengths", stream.ToArray());
            if (settings?.Wavelengths == null) return;
            ds.SnapshotSettings.AddSnapshotSettingsRow(snapRow, "Wavelengths", DocumentSerialization.Serialize(settings.Wavelengths));
        }

        private void AddWellScanInformation(DocumentDs.SnapShotsRow snapRow, MeasurementSettings settings, DocumentDs ds)
        {
            if (settings?.Wavelengths == null) return;
            ds.SnapshotSettings.AddSnapshotSettingsRow(snapRow, "WellScan", DocumentSerialization.Serialize(settings.WellScan));
        }

        private void AddDetectionSettingInformation(DocumentDs.SnapShotsRow snapRow, MeasurementSettings settings, DocumentDs ds)
        {
            if (settings?.DetectionSetting == null) return;
            ds.SnapshotSettings.AddSnapshotSettingsRow(snapRow, "DetectionSetting", DocumentSerialization.Serialize(settings.DetectionSetting));
        }
        private void AddKineticInformation(DocumentDs.SnapShotsRow snapRow, MeasurementSettings settings, DocumentDs ds)
        {
            if (settings?.Kinetic == null) return;
            ds.SnapshotSettings.AddSnapshotSettingsRow(snapRow, "Kinetic", DocumentSerialization.Serialize(settings.Kinetic));
        }
        private void AddShakeInformation(DocumentDs.SnapShotsRow snapRow, MeasurementSettings settings, DocumentDs ds)
        {
            if (settings?.Shake == null) return;
            ds.SnapshotSettings.AddSnapshotSettingsRow(snapRow, "Shake", DocumentSerialization.Serialize(settings.Shake));
        }

        private byte[] SerializeParameter(Parameter.GimliParameter parameter)
        {
            if (parameter == null) return null;
            return DocumentSerialization.Serialize(parameter);
        }
        private byte[] SerializeParameter(string parameter)
        {
            if (parameter == null) return null;
            return DocumentSerialization.Serialize(parameter);
        }
        private byte[] SerializeParameter(DateTime parameter)
        {
            return DocumentSerialization.Serialize(parameter);
        }
    }
}
