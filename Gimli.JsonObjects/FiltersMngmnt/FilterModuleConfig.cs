﻿using System;

namespace Gimli.JsonObjects.FiltersMngmnt
{
	[Serializable]
	public class FilterModuleConfig
	{
		public bool IsFilterReady { get; set; }
		public FilterSlidesCubeMemory StoredFilterInfo { get; set; }
	}
}
