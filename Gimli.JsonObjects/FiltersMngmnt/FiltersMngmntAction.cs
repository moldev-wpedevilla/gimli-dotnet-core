﻿using System;
using MD.Nfc.Basic.StrTagConverting;

namespace Gimli.JsonObjects.FiltersMngmnt
{
    public enum SlideSel
    {
        None = 0,
        SlideA = 1,
        SlideB = 2
    }

    public enum FiltersMngmntMode
    {
        LoadSlide,
        EjectSlide,
		DummyInsertManually,
        ToggleSlide
    }

    public interface IFiltersMngmntAction
    {

    }

    [Serializable]
    public class FiltersMngrAction
    {
        public FiltersMngmntMode Mode { get; set; }
        public IFiltersMngmntAction FiltersMngmntAction { get; set; }

		public static SliderTagData SimulatorSlide { get; set; }

		public static string MapFilterSlotId(int number)
		{
			switch (number)
			{
				case 1:
					return "A";
				case 2:
					return "B";
				default:
					throw new NotImplementedException("This is not a valid slide number " + number);
			}
		}

		public static int MapFilterSlotId(string name)
		{
			switch (name)
			{
				case "A":
					return 1;
				case "B":
					return 2;
				default:
					throw new NotImplementedException("This is not a valid slide name " + name);
			}
		}
	}

    public interface IFiltersSpecificAction
    {
        SlideSel Slide { get; set; }
    }

    [Serializable]
    public class EjectSlideAction : IFiltersMngmntAction, IFiltersSpecificAction
    {
        public EjectSlideAction()
        {
            Slide = SlideSel.SlideA;
        }

        public SlideSel Slide { get; set; }
    }

    [Serializable]
    public class ToggleSlideAction : IFiltersMngmntAction, IFiltersSpecificAction
    {
        public ToggleSlideAction()
        {
            Slide = SlideSel.SlideA;
        }

        public SlideSel Slide { get; set; }
    }

    [Serializable]
    public class LoadSlideAction : IFiltersMngmntAction, IFiltersSpecificAction
    {
        public LoadSlideAction()
        {
            Slide = SlideSel.SlideA;
        }

        public SlideSel Slide { get; set; }
    }

	[Serializable]
	public class DummyInsertManuallyAction : IFiltersMngmntAction, IFiltersSpecificAction
	{
		public DummyInsertManuallyAction()
		{
			Slide = SlideSel.SlideA;
		}

		public SlideSel Slide { get; set; }
		public SliderTagData Info { get; set; }
	}
}
