﻿using System;
using MD.Nfc.Basic.StrTagConverting;

namespace Gimli.JsonObjects.FiltersMngmnt
{
	[Serializable]
	public class CurrentFilterSlides
	{
		public SliderTagData SlideEx{ get; set; }
		public SliderTagData SlideEm { get; set; }
	}
}