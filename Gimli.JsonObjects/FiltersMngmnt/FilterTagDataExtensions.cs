﻿using System;
using System.Collections.Generic;
using System.Linq;
using MD.Nfc.Basic.StrTagConverting;

namespace Gimli.JsonObjects.FiltersMngmnt
{
	[Serializable]
	public enum FilterSlotType
	{
		None,
		Ex,
		Em
	}

	[Serializable]
	// this extends definition in Md.Nfc.Lib
	public enum FilterType : byte
	{
		Singlepass = 1,
		Longpass = 2,
		Shortpass = 3,
		Multipass = 4,
		None = 5,
	}

	[Serializable]
	// this exactly matches definition in Md.Nfc.Lib
	public enum Polarization : byte
	{
		None = 0,
		Vertical = 1,
		Horizontal = 2
	}

	public class TransformedFilterInfo
	{
		public const string FilterSeperator = " ";
		public const string FilterShortpass = "SP";
		public const string FilterLongpass = "LP";
		public const string FilterMultipass = "MP";

		public short FirstWavelength { get; set; }
		public short FirstBandwidth { get; set; }
		public List<Mode> Modes { get; set; }
		public FilterType Type { get; set; }
		public Polarization Polarization { get; set; }

		public string GetMiniFilterString()
		{
			if (Type == FilterType.None) return null;

			var returnString = FirstWavelength + FilterSeperator;

			switch (Type)
			{
				case FilterType.Singlepass:
					break;
				case FilterType.Longpass:
					returnString += FilterLongpass;
					break;
				case FilterType.Shortpass:
					returnString += FilterShortpass;
					break;
				case FilterType.Multipass:
					// assuming not more than one MP filter exists with the same first wavelength,
					// otherwise we would have to add filter information about the other bands as well
					returnString += FilterMultipass;
					break;
			}

			// assuming that the user won't place two filters with the same CWL and different BW on the same slide
			// otherwise we would have to add BW information to this string here

			return returnString;
		}
	}

	public static class FilterTagDataExtensions
	{
		[Flags]
		// this exactly matches definition in Md.Nfc.Lib
		public enum InternalReadModes : byte
		{
			None,
			Abs = 1,
			Fl = 1 << 1,
			Lum = 1 << 2,
			Fp = 1 << 3,
			Trf = 1 << 4
		}

		public static List<Mode> IntrpretMeasurementType(this FilterTagData filter)
		{
			var allSupportedModes = new List<Mode>();
			var modes = (InternalReadModes)filter.MeasureType;

			if (modes.HasFlag(InternalReadModes.Abs)) allSupportedModes.Add(Mode.Abs);
			if (modes.HasFlag(InternalReadModes.Fl)) allSupportedModes.Add(Mode.Fl);
			if (modes.HasFlag(InternalReadModes.Lum)) allSupportedModes.Add(Mode.Lum);
			if (modes.HasFlag(InternalReadModes.Fp)) allSupportedModes.Add(Mode.Fp);
			if (modes.HasFlag(InternalReadModes.Trf)) allSupportedModes.Add(Mode.Trf);

			if (!allSupportedModes.Any()) allSupportedModes.Add(Mode.None);

			return allSupportedModes;
		}

		public static FilterType IntrpretFilterType(this FilterTagData filter)
		{
			var filterType = (FilterType)filter.Type;
			if (filter.Band.Length == 0 || filter.Band.All(f => f.CentralWavelengthOrTransition == 0) || string.IsNullOrEmpty(filter.Name))
			{
				filterType = FilterType.None;
			}

			return filterType;
		}

		public static List<TransformedFilterInfo> GetTransformedFilterInfos(this SliderTagData slide, Mode forMode, bool isEmSide)
		{
			var transformedFilters = new List<TransformedFilterInfo>();
			if (slide == null) return transformedFilters;

			foreach (var filter in slide.Filter)
			{
				var modes = filter.IntrpretMeasurementType();
				var type = filter.IntrpretFilterType();
				if (!modes.Contains(forMode) || type == FilterType.None) continue;

				transformedFilters.Add(new TransformedFilterInfo
				{
					FirstWavelength = filter.Band[0].CentralWavelengthOrTransition,
					FirstBandwidth = filter.Band[0].Bandwidth,
					Modes = modes,
					Polarization = (Polarization)filter.Polarization,
					Type = type
				});
			}

			// FP H/V pairs checking on Em side
			if (isEmSide && forMode == Mode.Fp)
			{
				// assuming not more than one MP filter exists with the same first wavelength,
				// otherwise we would have to check for equlaity not only for the first wavelength

				Func<TransformedFilterInfo, string> selector = f => $"{f.FirstWavelength}/{f.FirstBandwidth}{f.Type}";
				var polarizerGroups = transformedFilters
					.Where(f => f.Modes.Contains(Mode.Fp) && f.Polarization != Polarization.None)
					.OrderBy(selector)
					.GroupBy(selector)
					.ToList();

				transformedFilters.Clear();
				foreach (var polarizerGroup in polarizerGroups)
				{
					var hvPair = new List<TransformedFilterInfo>();
					foreach (var pol in new[] { Polarization.Horizontal, Polarization.Vertical })
					{
						var filterList = polarizerGroup.ToList();
						if (hvPair.All(f => f.Polarization != pol) && filterList.Any(f => f.Polarization == pol))
						{
							hvPair.Add(filterList.First(f => f.Polarization == pol));
						}
					}

					if (hvPair.Count == 2)
					{
						// add only one filter of the pair because user is not supposed to select 
						// a H or V filter but a pair thereof -> the added filter represents tht pair
						transformedFilters.Add(hvPair[0]);
					}
				}
			}

			// TODO: maybe we have to restrict filters on Ex side to V or H -> info from sys-eng or fw needed
			return transformedFilters;
		}
	}
}
