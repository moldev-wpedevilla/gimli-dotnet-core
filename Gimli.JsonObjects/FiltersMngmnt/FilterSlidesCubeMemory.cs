using System;
using System.Collections.Generic;
using MD.Nfc.Basic.StrTagConverting;

namespace Gimli.JsonObjects.FiltersMngmnt
{
	[Serializable]
	public class FilterSlidesCubeMemory
	{
		public CurrentFilterSlides CurrentSlides { get; set; }
		public List<FilterTagData> CubeMemory { get; set; }
	}
}