﻿using Gimli.JsonObjects.Parameter;
using Gimli.JsonObjects.ReadArea;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Gimli.JsonObjects.Settings
{

    [Serializable]
    public class ReadAreaSettings : ReadAreaCollection
    {
	    public static string ReadOrderRow = "Row";
	    public static string ReadOrderColumn = "Column";
	    public static string ReadOrderWell = "Well";


		public static ReadAreaSettings CreateDefaultReadAreaSettings()
        {
            return new ReadAreaSettings(8,12,true);
        }

        public static ReadAreaSettings CreateReadAreaSettings(int rows, int cols, bool selectAll)
        {
            return new ReadAreaSettings(rows, cols, selectAll);
        }

        /// <summary>
        /// Default constructor for serialization only
        /// </summary>
        [JsonConstructor]
        public ReadAreaSettings()
	    {
            Name = "ReadArea";
        }

        private ReadAreaSettings(int rows, int cols, bool selectAll = false) : base(rows, cols, selectAll) 
        {
            InitializeSettings();
        }
        public ReadAreaSettings(ReadAreaSettings originalSettings) : base(originalSettings)
        {
            if (originalSettings == null) return;

            IsUsed = originalSettings.IsUsed;

            if (originalSettings.InterlacedReading != null)
                InterlacedReading = new GimliParameterBool(originalSettings.InterlacedReading);
            if (originalSettings.ReadOrder != null)
                ReadOrder = new GimliParameterString(originalSettings.ReadOrder);
            
        }
        public GimliParameterBool InterlacedReading { get; set; }
        public GimliParameterString ReadOrder { get; set; }

        public override List<SettingsSummary> GetSummary
        {
            get
            {
                var textSplit = GetSelectedRowColumnString().Split('|');
                var list = new List<SettingsSummary>
                {
                    new SettingsSummary(Resources.GetResource.GetString(Name))
                    {
                        IsVisible = IsUsed,
                        Items =
                            new List<ItemClass>()
                            {
                                new ItemClass(textSplit[0], textSplit[1])
                            }
                    }
                };
                list[0].Items.AddRange(GetAllParameterSummary());
                return list;
            }
        }

        protected override List<ItemClass> GetAllParameterSummary()
        {
            var list = new List<ItemClass>();
            GimliSummary.AddItToList(ReadOrder.GetSummary(), list);
            GimliSummary.AddItToList(InterlacedReading.GetSummary(true), list);
            return list;
        }

        public ReadAreaSettings Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as ReadAreaSettings;
        }

		public override IEnumerable<GimliParameter> GetProperties()
	    {
		    return new List<GimliParameter> {InterlacedReading, ReadOrder};
	    }

	    public override bool IsValid
        {
            get
            {
                mErrorMessage.Clear();
                if (GetUsedWellCount() < 1)
                    mErrorMessage.Add(Properties.Resources.NoWellSelected);
                if (!InterlacedReading.IsValid)
                    mErrorMessage.Add(Properties.Resources.InterlacedParameterInvalid);
                if (!ReadOrder.IsValid)
                    mErrorMessage.Add(Properties.Resources.ReadOrderParameterInvalid);
                
                return mErrorMessage.Count == 0;
            }
        }

        private void InitializeSettings()
        {
            Name = "ReadArea";
            InterlacedReading = new GimliParameterBool("InterlacedReading", false) {LegalValues = new ArrayList {true, false}, Hidden=true, IsEnabled=false};
            ReadOrder = new GimliParameterString("ReadOrder", ReadOrderRow) { LegalValues = new ArrayList { ReadOrderRow, ReadOrderColumn, ReadOrderWell }, IsUsed=true, IsEnabled=true, IsDisplayedInSummary=true, Hidden=false };
            
        }
    }
}
