﻿using System;

namespace Gimli.JsonObjects.Settings
{
    [Serializable]
    public class MeasurementSettingsSerialize
    {
        public MeasurementSettingsSerialize() { }

        public MeasurementSettingsSerialize(Mode mode, MeasurementType type, string unit)
        {
            MeasurementMode = mode;
            MeasurementType = type;
            Unit = unit;
        }
        public string Unit { get; set; }
        public Mode MeasurementMode { get; set; }
        public MeasurementType MeasurementType { get; set; }
        public bool IsModuleUsed { get; set; }
        public string ModuleInformation { get; set; }
    }
}
