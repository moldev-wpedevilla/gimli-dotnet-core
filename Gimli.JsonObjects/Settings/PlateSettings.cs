﻿using Gimli.JsonObjects.Labware;
using Gimli.JsonObjects.Parameter;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Gimli.JsonObjects.Settings
{
    [Serializable]
    public class PlateSettings :SettingsBase
	{
        private const string GroupName = "GimliGroup";
        private const string DefaultPlateName = "Standard clrbtm";

        public static PlateSettings GenerateDefaultSettings()
	    {
	        return new PlateSettings(8, 12); 
	    }
        [JsonConstructor]
        public PlateSettings() { }
        public PlateSettings(int rows, int columns):this()
        {
            Plate = PlateLibrary.MicroplateLibrary.GetPlate(rows*columns, GroupName, DefaultPlateName).FirstOrDefault();
            if (Plate != null) MicroplateName = Plate.MicroplateName;
            InitializeSettings();
        }
        public PlateSettings(PlateSettings originalSettings) : base(originalSettings)
        {
            if (originalSettings == null) return;

	        WellCount = originalSettings.WellCount;
            mErrorMessage = originalSettings.mErrorMessage;
            mReadMode = originalSettings.mReadMode;
            Barcode = originalSettings.Barcode;
            UserName = originalSettings.UserName;
            InstrumentSerial = originalSettings.InstrumentSerial;
            InstrumentName = originalSettings.InstrumentName;
            MeasureDate = originalSettings.MeasureDate;

            if (originalSettings.Plate != null)
            {
                var plate = new Microplate();
                plate.CopyFrom(originalSettings.Plate);
                Plate = plate;
                //Plate = new Microplate();
                //Plate.CopyFrom(originalSettings.Plate);
            }

            MicroplateName = originalSettings.MicroplateName;
            IsUsed = originalSettings.IsUsed;

            if (originalSettings.Orientation != null)
                Orientation = new GimliParameterString(originalSettings.Orientation);
            if (originalSettings.IsLidded != null)
                IsLidded = new GimliParameterBool(originalSettings.IsLidded);
        }

        private Microplate mPlate;

        public Microplate Plate
        {
            get { return mPlate; }
            set
            {
                mPlate = value;
            }
        }

        public string WellCount { get; set; } = string.Format(Resources.GetResource.GetString("Plate_Format_Mask"), 96);
        public string Barcode { get; set; }
        public string InstrumentSerial { get; set; }
        public string InstrumentName { get; set; }
        public string UserName { get; set; }
        public DateTime MeasureDate { get; set; }
        public void SetMicroplateByName(string name)
        {
            if (string.IsNullOrEmpty(name)) return;
            Plate = PlateLibrary.MicroplateLibrary.GetMicroplateByNameAndSize(GetWellcount(WellCount), name);
        }
		public string MicroplateName { get; set; }

		public GimliParameterString Orientation { get; set; }
        public GimliParameterBool IsLidded { get; set; }
        public override List<SettingsSummary> GetSummary
        {
            get
            {
                if (Plate == null || Plate.PlateSpecification == null) return new List<SettingsSummary>();

                var setting = new SettingsSummary(string.Format(Resources.GetResource.GetString("Plate_Format_Mask"), mPlate.PlateSpecification.NumberOfWells)) {IsVisible = IsUsed };
                setting.Items.Add(new ItemClass("", Plate?.MicroplateName));
                var height = Plate == null ? string.Empty : Math.Round((IsLidded.ToBool() ? Plate.PlateSpecification.PlateHeightWithLid : Plate.PlateSpecification.PlateHeight)/1000.0, 1).ToString(CultureInfo.InvariantCulture);
				setting.Items.Add(new ItemClass(Resources.GetResource.GetString("PlateHeightHeader"), $"{height} {Resources.GetResource.GetString("UnitMillimeter")}"));
				//GimliSummary.AddItToList(Orientation, setting.Items);
				if (IsLidded.IsUsed && IsLidded.ToBool())
                    GimliSummary.AddItToList(IsLidded.GetSummary(true), setting.Items);
                return new List<SettingsSummary> { setting};
            }
        }

		public override IEnumerable<GimliParameter> GetProperties()
	    {
		    return new List<GimliParameter> {IsLidded, Orientation};
	    }

        private bool IsMicroplateValid(Microplate plate, Mode readMode)
        {
            mErrorMessage.Clear();
            if (Plate == null)
            {
                mErrorMessage.Add("Plate not set.");
                return false;
            }
            if ((int) readMode != 0) return true;

            if (!Plate.PlateSpecification.IsClearBottom)
                mErrorMessage.Add("This is not a clear bottom plate!");
            return Plate.PlateSpecification.IsClearBottom;
        }

        public override bool IsValid
        {
            get
            {
                mErrorMessage.Clear();
                if (Orientation == null || !Orientation.IsValid)
                    mErrorMessage.Add(Properties.Resources.OrientationParameterInvalid);
                if (IsLidded == null || !IsLidded.IsValid)
                    mErrorMessage.Add(Properties.Resources.IsLiddedParameterInvalid);
                if (Plate == null)
                    mErrorMessage.Add(Properties.Resources.NoPlateSelected);
                //IsMicroplateValid(Plate, mReadMode);
                return mErrorMessage.Count == 0;
            }
        }

        private Mode mReadMode = Mode.Fl;
        public void SetReadModeForValidation(Mode readMode)
        {
            mReadMode = readMode;
        }
        public void SetErrorMessage(string message, bool clearMessage = false)
        {
            if (clearMessage) mErrorMessage.Clear();
            mErrorMessage.Add(message);
        }
	    private int GetWellcount(string wellsString)
	    {
		    var parts = wellsString.Split(' ');
			int format;
			return !int.TryParse(parts[0], out format) ? 0 : format;
	    }

        public List<int> GetAllPlateTypes()
        {
            return PlateLibrary.MicroplateLibrary.GetAllWellType(GroupName).ToList();
        }
        public List<string> GetAllPlatesForAbs(int wellCount)
        {
            var plates = PlateLibrary.MicroplateLibrary.GetPlates(wellCount, GroupName).Where(p => p.PlateSpecification.IsClearBottom);
            return plates.Select(plate => plate.MicroplateName).ToList();
        }
        public List<string> GetAllPlatesForFiFl(int wellCount)
        {
            return PlateLibrary.MicroplateLibrary.GetPlates(wellCount, GroupName).Select(plate => plate.MicroplateName).ToList();
        }

        
        private void InitializeSettings()
        {
            Name = "PlateSettings";
            Orientation = new GimliParameterString("Orientation", "Landscape") { LegalValues = new ArrayList() { "Landscape", "OppositeLandscape" },
                IsUsed = true,
                IsEnabled = true,
                IsDisplayedInSummary = true,
                Hidden = false
            };
            IsLidded = new GimliParameterBool("IsLidded", false) { LegalValues = new ArrayList() { true, false },
                IsUsed = true,
                IsEnabled = true,
                IsDisplayedInSummary = true,
                Hidden = false
            };
        }
        public PlateSettings Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as PlateSettings;
        }
    }
}
