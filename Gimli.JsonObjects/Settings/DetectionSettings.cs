﻿using Gimli.JsonObjects.Parameter;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Gimli.JsonObjects.Settings
{
    [Serializable]
    public class DetectionSettings : SettingsBase
    {


		private readonly double mValueReadHeight = 1;
		private readonly int mValNumberOfPulses = 20;
		private readonly double mValMeasurementDelay = 0.03;
		private readonly double mValExcitationTime = 0.05;
		private readonly double mValIntegrationTime = 140;

		public const string ExcitationTimeName = "ExcitationTime";
	    public const string MeasurementDelayName = "MeasurementDelay";
	    public const string NumberOfPulsesName = "NumberOfPulses";
		public const string PmtSensitivityName = "PmtSensitivity";
	    public const string IntegrationTimeName = "IntegrationTime";
	    public const string AttenuationName = "Attenuation";
	    public const string AttenuationValueName = "AttenuationValue";
		public const string NormalizationName = "Normalization";
	    public const string SpeedReadName = "SpeedRead";
	    public const string ReadFromBottomName = "ReadFromBottom";
	    public const string ReadHeightName = "ReadHeight";
        public const string DetectionSettingsName = "DetectionSettings";

        public static DetectionSettings Generate(Mode readMode)
        {
            return new DetectionSettings(readMode);
        }

        private DetectionSettings(Mode readMode):this()
        {
            Mode = readMode;
            CreateDefaultEmptySettings();
            SetDefaultParameters();
        }

        [JsonConstructor]
        public DetectionSettings()
        {
            Name = DetectionSettingsName;
        }

        public DetectionSettings(object originalObject) : base(originalObject)
        {
            var originalSettings = originalObject as DetectionSettings;
            if (originalSettings == null) return;

            Mode = originalSettings.Mode;

            if (originalSettings.IntegrationTime != null)
                IntegrationTime = new GimliParameterDouble(originalSettings.IntegrationTime);
            //if (originalSettings.IntegrationTime2 != null)
            //    IntegrationTime2 = new GimliParameterDouble(originalSettings.IntegrationTime2);
            if (originalSettings.PmtSensitivity != null)
                PmtSensitivity = new GimliParameterString(originalSettings.PmtSensitivity);
            if (originalSettings.Attenuation != null)
                Attenuation = new GimliParameterBool(originalSettings.Attenuation);
            if (originalSettings.SpeedRead != null)
                SpeedRead = new GimliParameterBool(originalSettings.SpeedRead);
            if (originalSettings.Normalization != null)
                Normalization = new GimliParameterBool(originalSettings.Normalization);
            if (originalSettings.ReadFromBottom != null)
                ReadFromBottom = new GimliParameterBool(originalSettings.ReadFromBottom);
            if (originalSettings.ExcitationTime != null)
                ExcitationTime = new GimliParameterDouble(originalSettings.ExcitationTime);
            if (originalSettings.MeasurementDelay != null)
                MeasurementDelay = new GimliParameterDouble(originalSettings.MeasurementDelay);
			if (originalSettings.NumberOfPulses != null)
				NumberOfPulses = new GimliParameterInt(originalSettings.NumberOfPulses);
			if (originalSettings.ReadHeight != null)
				ReadHeight = new GimliParameterDouble(originalSettings.ReadHeight);

			AttenuationValue = new GimliParameterLegalDouble(AttenuationValueName, 0) {Units = Properties.Resources.UnitOpticalDensity };
			if (originalSettings.AttenuationValue != null)
				AttenuationValue = new GimliParameterLegalDouble(originalSettings.AttenuationValue);
		}

        public Mode Mode { get; set; }
        public DetectionSettings Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as DetectionSettings;
        }

        public GimliParameterDouble IntegrationTime { get; set; }
        //public GimliParameterDouble IntegrationTime2 { get; set; }
        public GimliParameterString PmtSensitivity { get; set; }
        public GimliParameterBool Attenuation { get; set; }
		public GimliParameterLegalDouble AttenuationValue { get; set; }
        public GimliParameterBool SpeedRead { get; set; }
		public GimliParameterBool Normalization { get; set; }
        public GimliParameterBool ReadFromBottom { get; set; }
        public GimliParameterDouble ExcitationTime { get; set; }
        public GimliParameterDouble MeasurementDelay { get; set; }
		public GimliParameterInt NumberOfPulses { get; set; }
		public GimliParameterDouble ReadHeight { get; set; }

		public override List<SettingsSummary> GetSummary => GetListOfSettingsSummary();

	    protected override List<ItemClass> GetAllParameterSummary()
        {
            var list = new List<ItemClass>();
            GimliSummary.AddItToList(ExcitationTime, list);
            GimliSummary.AddItToList(IntegrationTime, list);
            //GimliSummary.AddItToList(IntegrationTime2, list);
            GimliSummary.AddItToList(MeasurementDelay, list);
            if(NumberOfPulses!= null) GimliSummary.AddItToList(NumberOfPulses, list);
			GimliSummary.AddItToList(PmtSensitivity, list);
			if (AttenuationValue != null && AttenuationValue.IsUsed) GimliSummary.AddItToList(AttenuationValue, list);
            if(Attenuation.ToBool())
                GimliSummary.AddItToList(Attenuation.GetSummary(true), list);
            if(Normalization.ToBool())
                GimliSummary.AddItToList(Normalization.GetSummary(true), list);
            if (SpeedRead.ToBool() || list.Count==0)
                GimliSummary.AddItToList(SpeedRead.GetSummary(new ArrayList {Properties.Resources.MethodNameFast, Properties.Resources.MethodNamePrecise }), list);
            if (ReadFromBottom.ToBool())
                GimliSummary.AddItToList(ReadFromBottom.GetSummary(true), list);
            GimliSummary.AddItToList(ReadHeight, list);
			return list;
        }

	    public override IEnumerable<GimliParameter> GetProperties()
	    {
			var list = new List<GimliParameter> { Attenuation, AttenuationValue, ExcitationTime, IntegrationTime, MeasurementDelay, Normalization, PmtSensitivity, ReadFromBottom, ReadHeight, SpeedRead };
			if (NumberOfPulses != null) list.Add(NumberOfPulses);
		    return list;
	    }

		public override bool IsValid => DetermineIsvalidForAllParameters() && (AttenuationValue?.LegalValues == null || AttenuationValue.LegalValues.Contains(AttenuationValue.ToDouble()));

	    private void CreateDefaultEmptySettings()
        {
            ExcitationTime = new GimliParameterDouble(ExcitationTimeName, mValExcitationTime);
            MeasurementDelay = new GimliParameterDouble(MeasurementDelayName, mValMeasurementDelay);
			NumberOfPulses = new GimliParameterInt(NumberOfPulsesName, mValNumberOfPulses);
			PmtSensitivity = new GimliParameterString(PmtSensitivityName, Resources.GetResource.GetString("PmtSensitivityAuto"));
            IntegrationTime = new GimliParameterDouble(IntegrationTimeName, mValIntegrationTime);
            //IntegrationTime2 = new GimliParameterDouble("IntegrationTime2", mValIntegrationTime);
            Attenuation = new GimliParameterBool(AttenuationName, false);
            Normalization = new GimliParameterBool(NormalizationName, false);
            SpeedRead = new GimliParameterBool(SpeedReadName, false);
            ReadFromBottom = new GimliParameterBool(ReadFromBottomName, false);
            ReadHeight = new GimliParameterDouble(ReadHeightName, mValueReadHeight);
			AttenuationValue = new GimliParameterLegalDouble(AttenuationValueName, 0);
        }

        public void SetDefaultParameters()
        {
            ExcitationTime.Units = Properties.Resources.UnitMicroseconds;
            MeasurementDelay.Units = Properties.Resources.UnitMicroseconds;
	        if (NumberOfPulses != null) NumberOfPulses.Units = null;

            IntegrationTime.Units = Properties.Resources.UnitMilliseconds;
			ReadHeight.Units = Properties.Resources.UnitMillimeter;
			if (AttenuationValue != null) AttenuationValue.Units = Properties.Resources.UnitOpticalDensity;
        }
    }
}
