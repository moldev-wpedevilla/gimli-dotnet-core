﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gimli.JsonObjects.Parameter;
using Newtonsoft.Json;

namespace Gimli.JsonObjects.Settings
{
	[Serializable]
	public class DataReductionSettings : SettingsBase
	{
        public const string DataReductionSettingsName = "DataReductionSettings";
        public const string MethodName = "Method";
		public const string MehtodPolarization = "Polarization";
		public const string MehtodAnisotropy = "Anisotropy";
		public const string ParameterGfactor = "Gfactor";

		public static DataReductionSettings Generate(string method)
		{
			var settings = new DataReductionSettings
			{
				Method =
					new GimliParameterString(MethodName, Resources.GetResource.GetString(method))
					{
						LegalValues =
							new ArrayList
							{
								Resources.GetResource.GetString(MehtodPolarization),
								Resources.GetResource.GetString(MehtodAnisotropy)
							}
					}
				
			};
			settings.Method.SetToUsed(true);
			switch (method)
			{
				case MehtodAnisotropy:
				case MehtodPolarization:
					settings.ParametersDouble =
						new List<GimliParameterDouble>
						{
							new GimliParameterDouble(Resources.GetResource.GetString(ParameterGfactor), 0.5)
							{
								Minimum = 0.001,
								Maximum = 100.0,
								Step = 0.001
							}
						};
					break;
				default:
					throw new NotImplementedException();
			}

			foreach (var gimliParameterDouble in settings.ParametersDouble)
			{
				gimliParameterDouble.SetToUsed(true);
			}

			settings.IsUsed = true;
			return settings;
		}


		[JsonConstructor]
		public DataReductionSettings()
		{
			Name = DataReductionSettingsName;
		}

		public DataReductionSettings(object originalObject) : base(originalObject)
		{
			var originalSettings = originalObject as DataReductionSettings;
			if (originalSettings == null) return;

			if (originalSettings.ParametersDouble != null)
			{
				ParametersDouble = originalSettings.ParametersDouble;
			}
			if (originalSettings.Method != null)
			{
				Method = originalSettings.Method;
			}
		}

		public List<GimliParameterDouble> ParametersDouble { get; set; }
		public GimliParameterString Method { get; set; }

		public override bool IsValid
		{
			get
			{
				var errors = new List<string>();
				var isValid = true;
				foreach (var gimliParameter in ParametersDouble)
				{
					ValidateProperty(gimliParameter, errors, ref isValid);
				}

				ValidateProperty(Method, errors, ref isValid);

				return isValid;
			}
		}

		public DataReductionSettings Clone()
		{
			var newObject = Activator.CreateInstance(GetType(), this);
			return newObject as DataReductionSettings;
		}

		public override List<SettingsSummary> GetSummary => GetListOfSettingsSummary();

		public override IEnumerable<GimliParameter> GetProperties()
		{
			var list = new List<GimliParameter> { Method };
			return list;
		}

		protected override List<ItemClass> GetAllParameterSummary()
		{
			return new List<ItemClass>();
		}


	}
}
