﻿using Newtonsoft.Json;
using System;

namespace Gimli.JsonObjects.Settings
{
    [Serializable]
    public class WellScanSpacingAndPoints
    {
        [JsonConstructor]
        public WellScanSpacingAndPoints()
        {}

        public WellScanSpacingAndPoints(WellScanSpacingAndPoints originlaObject)
        {
            Spacing = originlaObject.Spacing;
            PointMax = originlaObject.PointMax;
            PointMin = originlaObject.PointMin;
        }

        public double Spacing { get; set; }
        public int PointMax { get; set; }
        public int PointMin { get; set; }

        public WellScanSpacingAndPoints Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as WellScanSpacingAndPoints;
        }
    }
}
