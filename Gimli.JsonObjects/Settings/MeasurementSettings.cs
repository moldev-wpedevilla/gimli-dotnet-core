﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Gimli.JsonObjects.Parameter;

namespace Gimli.JsonObjects.Settings
{
    [Serializable]
    public class MeasurementSettings:SettingsBase
    {

        /// <summary>
        /// Default constructor for serialization only
        /// </summary>
        [JsonConstructor]
        public MeasurementSettings()
        {
            Name = "MeasurementSettings";
        }

        public static MeasurementSettings Generate(Mode mode, MeasurementType type)
        {
			var settings = new MeasurementSettings(mode, type);
	        switch (mode)
	        {
		        case Mode.Abs:
			        settings.Unit = "OD";
			        break;
				case Mode.Fl:
			        settings.Unit = "RFU";
			        break;
				case Mode.Lum:
			        settings.Unit = "RLU";
			        break;
				case Mode.Trf:
			        settings.Unit = "RFU";
			        break;
				case Mode.Fp:
					settings.Unit = "RFU";
					break;
				default:
			        throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
	        }

	        return settings;
        }

	    private MeasurementSettings(Mode mode, MeasurementType type) : this()
	    {
		    MeasurementMode = mode;
		    MeasurementType = type;
		    IsModeTypeValid = true;

		    InitializeSettings();
	    }

	    public MeasurementSettings(MeasurementSettings originalSettings) : base(originalSettings)
	    {
		    if (originalSettings == null) return;

		    IsUsed = originalSettings.IsUsed;
		    MeasurementType = originalSettings.MeasurementType;
		    MeasurementMode = originalSettings.MeasurementMode;
		    Unit = originalSettings.Unit;

		    if (originalSettings.Wavelengths != null)
			    Wavelengths = originalSettings.Wavelengths.Clone();
		    if (originalSettings.DetectionSetting != null)
			    DetectionSetting = originalSettings.DetectionSetting.Clone();
		    if (originalSettings.Kinetic != null)
			    Kinetic = originalSettings.Kinetic.Clone();
		    if (originalSettings.Shake != null)
			    Shake = originalSettings.Shake.Clone();
		    if (originalSettings.ReadArea != null)
			    ReadArea = originalSettings.ReadArea.Clone();
		    if (originalSettings.WellScan != null)
			    WellScan = originalSettings.WellScan.Clone();
		    EstimatedTime = originalSettings.EstimatedTime;
	        IsModuleUsed = originalSettings.IsModuleUsed;
	        ModuleInformation = originalSettings.ModuleInformation;
		    IsModeTypeValid = originalSettings.IsModeTypeValid;
	    }

	    public string Unit { get; set; }
	    public Mode MeasurementMode { get; set; }
	    public MeasurementType MeasurementType { get; set; }
		public bool IsModeTypeValid { get; set; }
		public WavelengthSettings Wavelengths { get; set; }
	    public DetectionSettings DetectionSetting { get; set; }
	    public KineticSettings Kinetic { get; set; }
	    public ShakeSetting Shake { get; set; }
	    public ReadAreaSettings ReadArea { get; set; }
	    public WellScanSettings WellScan { get; set; }

        public bool IsModuleUsed { get; set; }
        public string ModuleInformation { get; set; }

		[JsonIgnore]
		public Func<List<SettingsSummary>> ExternalPlateSummaryAccessor { get; set; }

		public event Snapshot.SummaryRequestEventHandler SummaryRequest;



	    private List<SettingsSummary> OnSummaryRequest()
	    {
		    var summary = new GimliSummaryRequestEvent(new List<SettingsSummary>());
		    SummaryRequest?.Invoke(summary);
		    return summary.Summary;
	    }

	    public override List<SettingsSummary> GetSummary
	    {
		    get
		    {
			    var list = new List<SettingsSummary>();
			    var modeType = new SettingsSummary(Resources.GetResource.GetString(Name));
			    modeType.Items.Add(new ItemClass(Resources.GetResource.GetString("ReadMode"), Resources.GetResource.GetString($"ReadMode_{MeasurementMode}")));
			    modeType.Items.Add(new ItemClass(Resources.GetResource.GetString("ReadType"), Resources.GetResource.GetString($"ReadType_{MeasurementType}")));
                if(IsModuleUsed && MeasurementMode == Mode.Trf)
                    modeType.Items.Add(new ItemClass(Resources.GetResource.GetString("UsedModule"), ModuleInformation));
                
                list.Add(modeType);

			    var plate = OnSummaryRequest();

			    if (ExternalPlateSummaryAccessor != null && (plate == null || plate.Count == 0))
					plate = ExternalPlateSummaryAccessor();

			    if (plate != null && plate.Count > 0)
				    list.AddRange(plate);
			    if (ReadArea.IsUsed)
				    list.AddRange(ReadArea.GetSummary);
			    if (Wavelengths.IsUsed)
				    list.AddRange(Wavelengths.GetSummary);
			    if (DetectionSetting.IsUsed)
				    list.AddRange(DetectionSetting.GetSummary);
			    if (Kinetic.IsUsed)
				    list.AddRange(Kinetic.GetSummary);
			    if (Shake.IsUsed)
				    list.AddRange(Shake.GetSummary);
			    if (WellScan.IsUsed)
				    list.AddRange(WellScan.GetSummary);
			    return list;
		    }
	    }

	    public override IEnumerable<GimliParameter> GetProperties()
	    {
		    return new List<GimliParameter>();
	    }

	    public override bool IsValid
	    {
		    get
		    {
			    if ((Wavelengths.IsUsed && !Wavelengths.IsValid) || 
					(DetectionSetting.IsUsed && !DetectionSetting.IsValid) ||
			        (Kinetic.IsUsed && !Kinetic.IsValid) || (Shake.IsUsed && !Shake.IsValid) ||
			        (ReadArea.IsUsed && !ReadArea.IsValid) ||
					!IsModeTypeValid)
				    return false;
			    return true;
		    }
	    }

	    public TimeSpan EstimatedTime { get; set; }

	    public MeasurementSettings Clone()
	    {
		    var newObject = Activator.CreateInstance(GetType(), this);
		    return newObject as MeasurementSettings;
	    }

	    private void InitializeSettings()
	    {
		    ReadArea = ReadAreaSettings.CreateDefaultReadAreaSettings();
		    ReadArea.UiTabOrder = 0;
		    ReadArea.IsUsed = true;

		    Wavelengths = WavelengthSettings.Generate(MeasurementMode, MeasurementType);
		    Wavelengths.UiTabOrder = 1;
		    Wavelengths.IsUsed = true;

		    DetectionSetting = DetectionSettings.Generate(MeasurementMode);
		    DetectionSetting.UiTabOrder = 2;
		    DetectionSetting.IsUsed = true;

		    Kinetic = KineticSettings.Generate();
		    Kinetic.UiTabOrder = 3;

		    Shake = ShakeSetting.Generate();
		    Shake.UiTabOrder = 4;
		    Shake.IsUsed = true;

		    WellScan = WellScanSettings.Generate();
		    WellScan.UiTabOrder = 5;

		    SetSettingsVisibility();
	    }

	    public void SetSettingsVisibility()
	    {
		    Kinetic.IsUsed = MeasurementType == MeasurementType.Kinetic;
		    WellScan.IsUsed = MeasurementType == MeasurementType.AreaScan;
	    }
    }
}
