﻿using Gimli.JsonObjects.Parameter;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Gimli.JsonObjects.Settings
{
    [Serializable]
    public class WellScanSettings : SettingsBase
    {
	    public static string ScanModeArea = "Area";
	    public static string ScanModeHorizontal= "Horizontal";

		public static WellScanSettings Generate()
        {
			return new WellScanSettings(5,5);
        }
        [JsonConstructor]
        public WellScanSettings()
        {
        }

        internal WellScanSettings(int vertical, int horizontal) : this ()
        {
			InitializeSettings();
            Y_Points.Value = vertical;
			X_Points.Value = horizontal;
        }
        public WellScanSettings(WellScanSettings originalSettings) : base(originalSettings)
        {
            if (originalSettings == null) return;

            IsUsed = originalSettings.IsUsed;

            if (originalSettings.X_Spacing != null)
                X_Spacing = originalSettings.X_Spacing.Clone();
            if (originalSettings.Y_Spacing != null)
                Y_Spacing = originalSettings.Y_Spacing.Clone();
            if (originalSettings.X_Points != null)
                X_Points = originalSettings.X_Points.Clone();
            if (originalSettings.Y_Points != null)
                Y_Points = originalSettings.Y_Points.Clone();
            if (originalSettings.X_Center != null)
                X_Center = originalSettings.X_Center.Clone();
            if (originalSettings.Y_Center != null)
                Y_Center = originalSettings.Y_Center.Clone();
            if (originalSettings.ScanMode != null)
                ScanMode = originalSettings.ScanMode.Clone();
        }
        public GimliParameterDouble X_Spacing { get; set; }
        public GimliParameterDouble Y_Spacing { get; set; }
        public GimliParameterDouble X_Points { get; set; }
        public GimliParameterDouble Y_Points { get; set; }
        public GimliParameterDouble X_Center { get; set; }
        public GimliParameterDouble Y_Center { get; set; }
        public GimliParameterString ScanMode { get; set; }

        public List<WellScanSpacingAndPoints> AllowedSpacingAndDensity { get; set; }
        public WellScanSettings Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as WellScanSettings;
        }
        public override bool IsUsed { get { return mIsUsed; }
            set
            {
                if (mIsUsed == value) return;
                mIsUsed = value;
                InitializeSettings();
            } }

	    public override List<SettingsSummary> GetSummary => GetListOfSettingsSummary();

        public void Serialize()
        {
            
        }
        protected override List<ItemClass> GetAllParameterSummary()
        {
            var list = new List<ItemClass>();
            GimliSummary.AddItToList(ScanMode, list);
            GimliSummary.AddItToList(X_Points, list);
			GimliSummary.AddItToList(X_Spacing.GetSummary(2), list);
            GimliSummary.AddItToList(X_Center.GetSummary(3), list);
            GimliSummary.AddItToList(Y_Points, list);
            GimliSummary.AddItToList(Y_Spacing.GetSummary(3), list);
            GimliSummary.AddItToList(Y_Center.GetSummary(3), list);
            return list;
        }

		public override IEnumerable<GimliParameter> GetProperties()
	    {
		    return new List<GimliParameter> {ScanMode, X_Spacing, X_Center, X_Points, Y_Center, Y_Points, Y_Spacing};
	    }
       
        public override bool IsValid
	    {
		    get { return DetermineIsvalidForAllParameters(); }
	    }
	    private void InitializeSettings()
	    {
	        var isActive = mIsUsed;
            Name = "WellScanSettings";
            X_Spacing = new GimliParameterDouble("X_Spacing", 0.225) { Minimum = 0.05625, Maximum = 2.25, Step = 0.05625,
                IsUsed = isActive,
                IsEnabled = isActive,
                IsDisplayedInSummary = isActive,
                Hidden = !isActive,
				Units = Resources.GetResource.GetString("UnitMillimeter")
            };
			Y_Spacing = new GimliParameterDouble("Y_Spacing", 0.225) { Minimum = 0.05625, Maximum = 2.25, Step = 0.05625,
                IsUsed = isActive,
                IsEnabled = isActive,
                IsDisplayedInSummary = false,
                Hidden = !isActive,
            };
            X_Points = new GimliParameterDouble("X_Points", 2) { Minimum = 2, Maximum = 30, Step = 1,
                IsUsed = isActive,
                IsEnabled = isActive,
                IsDisplayedInSummary = isActive,
                Hidden = !isActive
            };
            Y_Points = new GimliParameterDouble("Y_Points", 2) { Minimum = 2, Maximum = 30, Step = 1,
                IsUsed = isActive,
                IsEnabled = isActive,
                IsDisplayedInSummary = false,
                Hidden = !isActive
            }; 
            X_Center = new GimliParameterDouble("X_Center", 4.5) { Minimum = 0.05625, Maximum = 9, Step = 0.05625,
                IsUsed = isActive,
                IsEnabled = isActive,
                IsDisplayedInSummary = false,
                Hidden = !isActive
            };
            Y_Center = new GimliParameterDouble("Y_Center", 4.5) { Minimum = 0.05625, Maximum = 9, Step = 0.05625,
                IsUsed = isActive,
                IsEnabled = isActive,
                IsDisplayedInSummary = false,
                Hidden = !isActive
            };
            ScanMode = new GimliParameterString("ScanMode", ScanModeArea) { LegalValues = new ArrayList { ScanModeArea, ScanModeHorizontal},
                IsUsed = isActive,
                IsEnabled = isActive,
                IsDisplayedInSummary = isActive,
                Hidden = !isActive,
            };
        }
    }
}
