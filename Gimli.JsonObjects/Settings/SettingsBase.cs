﻿using System;
using System.Collections.Generic;
using Gimli.JsonObjects.Parameter;

namespace Gimli.JsonObjects.Settings
{
    [Serializable]
    public abstract class SettingsBase : ValidationBase
    {
        public SettingsBase() { }

        public SettingsBase(object originalSettings)
        {
            var settingsBase = originalSettings as SettingsBase;
            if (settingsBase == null) return;

            Name = settingsBase.Name;
            mIsUsed = settingsBase.mIsUsed;
            UiTabOrder = settingsBase.UiTabOrder;
            mErrorMessage = settingsBase.mErrorMessage;
            mIsValid = settingsBase.mIsValid;
            IsModified = settingsBase.IsModified;
            IsDisplayedInSummary = settingsBase.IsDisplayedInSummary;
            Description = settingsBase.Description;
        }
        protected bool mIsUsed = true;
        public virtual bool IsUsed { get {return mIsUsed;} set { mIsUsed = value; } }

        /// <summary>
        /// ReadMode & Type as string eg: Abs, AbsSpectralScan, Lum, LumSpectralScan, Fi, Trf, FiSpectralScan, Htrf...
        /// </summary>
        public abstract List<SettingsSummary> GetSummary { get; }

        //public abstract SettingsSummary Summary();
        /// <summary>
        /// zero-based order of tabs in UI
        /// </summary>
        public int UiTabOrder { get; set; }

		protected bool DetermineIsvalidForAllParameters(bool clearErrors = true, bool isValid = true)
		{
			if (clearErrors) mErrorMessage.Clear();
			mWarningMessage.Clear();

			foreach (var gimliParameter in GetProperties())
			{
				ValidateProperty(gimliParameter, mErrorMessage, ref isValid);
			}
			return isValid;
		}

	    public abstract IEnumerable<GimliParameter> GetProperties();

	    protected void ValidateProperty(GimliParameter parameter, List<string> errors, ref bool isvalid)
	    {
			if (parameter == null || !parameter.IsUsed || !parameter.IsEnabled || parameter.Hidden) return;

			mWarningMessage.AddRange(parameter.WarningMessage);

		    if (parameter.IsValid) return;

		    isvalid = false;
		    mErrorMessage.AddRange(parameter.ErrorMessage);
	    }

		protected List<SettingsSummary> GetListOfSettingsSummary()
		{
			return new List<SettingsSummary>
			{
				new SettingsSummary(Resources.GetResource.GetString(Name))
				{
					IsVisible = IsUsed,
					Items = GetAllParameterSummary()
				}
			};
		}

	    protected virtual List<ItemClass> GetAllParameterSummary()
	    {
		    throw new NotImplementedException();
	    }
    }
}
