﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Gimli.JsonObjects.Settings
{
    [Serializable]
    public class SettingsSummary
    {
        private List<ItemClass> mItems = new List<ItemClass>();

        [JsonConstructor]
        public SettingsSummary()
        { }

        public SettingsSummary(string header)
        {
            Header = header;
        }
        public string Header { get; set; }
        public List<ItemClass> Items { get { return mItems; } set { mItems = value; } }
        public bool IsVisible { get; set; } = false;
    }

    [Serializable]
    public class ItemClass
    {
        [JsonConstructor]
        public ItemClass() { }

        public ItemClass(string title, string itemValue)
        {
            Title = title;
            ItemValue = itemValue;
        }
        public string Title { get; set; }
        public string ItemValue { get; set; }
    }
}
