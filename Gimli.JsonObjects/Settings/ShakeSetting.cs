﻿using Gimli.JsonObjects.Parameter;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Gimli.JsonObjects.Settings
{
    [Serializable]
    public class ShakeSetting : SettingsBase
    {
	    private const string DurationName = "Duration";
	    private const string ModeName = "Mode";
	    private const string IntensityName = "Intensity";
	    private const string DoShakeName = "DoShake";

		public static ShakeSetting Generate()
        {
			var settings = new ShakeSetting();
			settings.InitializeSettings();
			return settings;
        }

        [JsonConstructor]
        public ShakeSetting()
        {
        }

        public ShakeSetting(ShakeSetting originalSettings) : base(originalSettings)
        {
            if (originalSettings == null) return;

            IsUsed = originalSettings.IsUsed;

            if (originalSettings.Duration != null)
                Duration = new GimliParameterInt(originalSettings.Duration);
            if (originalSettings.Mode != null)
                Mode = new GimliParameterString(originalSettings.Mode);
            if (originalSettings.Intensity != null)
                Intensity = new GimliParameterString(originalSettings.Intensity);
			if (originalSettings.DoShake != null)
				DoShake = new GimliParameterBool(originalSettings.DoShake);
        }

        public GimliParameterBool DoShake { get; set; }
        public GimliParameterInt Duration { get; set; }
		public GimliParameterString Mode { get; set; }
        public GimliParameterString Intensity { get; set; }

        public ShakeSetting Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as ShakeSetting;
        }

	    public override List<SettingsSummary> GetSummary => GetListOfSettingsSummary();

        protected override List<ItemClass> GetAllParameterSummary()
        {
            var list = new List<ItemClass>();
	        if (DoShake == null) return list;

	        if (DoShake.ToBool())
	        {
                var tempList = new List<ItemClass>();
                GimliSummary.AddItToListAsTime(Duration, tempList);
                var duration = tempList.FirstOrDefault();
	            var mode = Mode.GetSummary();
	            if (duration != null && mode != null)
	            {
	                duration.Title = mode?.ItemValue;
	                GimliSummary.AddItToList(duration, list);
	                GimliSummary.AddItToList(Intensity, list);
	            }
	        }
	        else
	        {
		       list.Add(DoShake.GetSummary(true));
			}
	        return list;
        }

		public override IEnumerable<GimliParameter> GetProperties()
		{
			return new List<GimliParameter> { Duration, Mode, Intensity, DoShake };
		}

		public override bool IsValid => DetermineIsvalidForAllParameters();

	    public void SetVisibility()
	    {
			foreach (var property in GetProperties())
			{
				if (property.Name != DoShakeName)
				{
					property.SetToUsed(DoShake.ToBool());
				}
				else
				{
					DoShake.IsDisplayedInSummary = !DoShake.ToBool();
				}
			}
		}

		private void InitializeSettings()
        {
            Name = "ShakeSettings";
		    Duration = new GimliParameterInt(DurationName, 5)
		    {
		        Minimum = 1,
		        Maximum = 2 * 60 * 60,
		        IsUsed = true,
		        IsEnabled = true,
		        IsDisplayedInSummary = true,
		        Hidden = false,
                Units = Properties.Resources.UnitSeconds
		    };
			Mode = GetEnabledStringParameter(ModeName, "ShakeModeOrbital",
				new[] {"ShakeModeOrbital", "ShakeModeLinear", "ShakeModeDoubleOrbital"});
			Intensity = GetEnabledStringParameter(IntensityName, "ShakeIntensityMedium",
				new[] {"ShakeIntensityLow", "ShakeIntensityMedium", "ShakeIntensityHigh"});
			DoShake = new GimliParameterBool(DoShakeName, false)
			{
				LegalValues = new ArrayList {true, false},
				IsEnabled = true,
				IsUsed = true,
				Hidden = false,
				IsDisplayedInSummary = true
			};

			SetVisibility();
        }

	    private GimliParameterString GetEnabledStringParameter(string name, string valueKey, IEnumerable<string> legalValueKeys)
	    {
		    var param = new GimliParameterString(name, Resources.GetResource.GetString(valueKey))
		    {
				LegalValues = new ArrayList(),
				IsUsed = true,
				IsEnabled = true,
				IsDisplayedInSummary = true,
				Hidden = false
			};

		    foreach (var key in legalValueKeys)
		    {
			    param.LegalValues.Add(Resources.GetResource.GetString(key));
		    }

		    return param;
	    }
    }
}
