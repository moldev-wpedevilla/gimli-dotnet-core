﻿using Gimli.JsonObjects.Parameter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Gimli.JsonObjects.Settings
{
    [Serializable]
    public class KineticSettings :SettingsBase
    {
	    private const string TotalRunTimeName = "TotalRunTime";
	    private const string CyclesName = "NumberOfReads";
	    private const string IntervallName = "Intervall";

	    public static KineticSettings Generate()
        {
			var settings = new KineticSettings();
			settings.InitializeSettings();
		    return settings;
        }
        [JsonConstructor]
        public KineticSettings()
        {
        }
        public KineticSettings(KineticSettings originalSettings) : base(originalSettings)
        {
            if (originalSettings == null) return;

            IsUsed = originalSettings.IsUsed;

            if (originalSettings.TotalRunTime!= null)
                TotalRunTime = new GimliParameterInt(originalSettings.TotalRunTime);
            if (originalSettings.Cycles != null)
                Cycles = new GimliParameterInt(originalSettings.Cycles);
            if (originalSettings.Intervall != null)
                Intervall = new GimliParameterDouble(originalSettings.Intervall);
            if (originalSettings.IntervallShake != null)
                IntervallShake = originalSettings.IntervallShake.Clone();

            IntervalInMilliseconds = originalSettings.IntervalInMilliseconds;
        }
        public KineticSettings Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as KineticSettings;
        }
        public GimliParameterInt TotalRunTime { get; set; }
        public GimliParameterInt Cycles { get; set; }
        public GimliParameterDouble Intervall { get; set; }
        public ShakeSetting IntervallShake { get; set; }

        public bool IntervalInMilliseconds { get; set; }
        public override bool IsUsed
        {
            get { return mIsUsed; }
            set
            {
                if (mIsUsed == value) return;
                mIsUsed = value;
            }
        }
        public override List<SettingsSummary> GetSummary
        {
            get
            {
	            var list = GetListOfSettingsSummary();
                if (IntervallShake != null && IntervallShake.DoShake.ToBool())
                {
                    var shake = IntervallShake.GetSummary[0];
                    foreach (var item in shake.Items)
                    {
                        GimliSummary.AddItToList(item, list[0].Items);
                    }
                }
                return list;
            }
        }

        protected override List<ItemClass> GetAllParameterSummary()
        {
            var list = new List<ItemClass>();
            GimliSummary.AddItToListAsTime(Intervall, list);
            GimliSummary.AddItToList(Cycles, list);
            GimliSummary.AddItToListAsTime(TotalRunTime, list);
            
            return list;
        }

		public override IEnumerable<GimliParameter> GetProperties()
		{
			return new List<GimliParameter> { Intervall, Cycles, TotalRunTime};
		}

		public override bool IsValid
		{
			get
			{
				if (!IsUsed) return true;
				var isvalid = DetermineIsvalidForAllParameters();

				if (IntervallShake != null && IntervallShake.IsUsed && !IntervallShake.IsValid)
				{
					isvalid = false;
					mErrorMessage.AddRange(IntervallShake.ErrorMessage);
				}

				return isvalid;
			}
		}

		private void InitializeSettings()
		{
            Name = "KineticSettings";
			TotalRunTime = new GimliParameterInt(TotalRunTimeName, (int) TimeSpan.FromMinutes(10).TotalSeconds);
			Cycles = new GimliParameterInt(CyclesName, 10);
			Intervall = new GimliParameterDouble(IntervallName, TimeSpan.FromSeconds(59).TotalSeconds);
            IntervallShake = ShakeSetting.Generate();
		    IntervalInMilliseconds = false;
		}
    }
}
