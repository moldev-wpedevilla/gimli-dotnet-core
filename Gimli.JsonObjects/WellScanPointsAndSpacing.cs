﻿using Newtonsoft.Json;
using System;

namespace Gimli.JsonObjects
{
    [Serializable]
    public class WellScanPointsAndSpacing
    {
        [JsonConstructor]
        public WellScanPointsAndSpacing()
        {
            //Spacings = new List<double>();
        }

        public WellScanPointsAndSpacing(WellScanPointsAndSpacing originlaObject)
        {
            Points = originlaObject.Points;
        }
        public int Points { get; set; }
        public double SpacingMax { get; set; }
        public double SpacingMin { get; set; }
        public double SpacingStep { get; set; }

        public WellScanPointsAndSpacing Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as WellScanPointsAndSpacing;
        }
    }
}
