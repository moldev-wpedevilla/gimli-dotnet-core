namespace Gimli.JsonObjects
{
	public enum Status
	{
		None,
		Ok,
        Error,
        Underflow,
        Overflow
	}
}