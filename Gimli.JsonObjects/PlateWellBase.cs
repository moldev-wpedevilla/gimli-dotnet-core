using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class PlateWellBase
	{
		public Guid DocumentID { get; set; }
		public Guid PlateID { get; set; }
		public Point MaxPoints { get; set; }
	}
}