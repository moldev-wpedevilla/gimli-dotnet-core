﻿using System;
using System.Collections.Generic;
using Gimli.JsonObjects.Settings;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class AllSettings : AllSettingsBase
	{
		public PlateSettings PlateSettings { get; set; }
		public Dictionary<Guid,MeasurementSettings> SnapshotSettings { get; set; }
	}
}
