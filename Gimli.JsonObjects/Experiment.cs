using System;
using System.Collections.Generic;
using System.Linq;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class Experiment
	{
	    private bool mIsValid = true;
        //for Serialization only
        public Experiment()
		{
			
		}

	    public Experiment(Experiment originalExperiment) 
	    {
	        if (originalExperiment != null)
	        {
	            Id = originalExperiment.Id;
	            ExpName = originalExperiment.ExpName;

	            if (originalExperiment.Plates == null) return;

                Plates = new List<Plate>();
	            foreach (var plate in originalExperiment.Plates)
	            {
	                Plates.Add(plate.Clone());
	            }
	        }
	    }
		public Experiment(Guid id, string expName, List<Plate> plates)
		{
			Id = id;
			ExpName = expName;
			Plates = plates;
		}

		public Guid Id { get; set; }
		public string ExpName { get; set; }
		public List<Plate> Plates { get; set; }

	    public bool IsValid
	    {
	        get
	        {
	            foreach (var plate in Plates)
	            {
	                if (plate.IsValid) return false;
	            }
	            return mIsValid;
	        }
	    }

	    public bool HasData
        {
            get
            {
                return Plates != null && Plates.Any(plate => plate.HasData);
            }
        }
        internal Experiment Clone()
        {
            var newObject = Activator.CreateInstance(GetType(), this);
            return newObject as Experiment;
        }
    }
}