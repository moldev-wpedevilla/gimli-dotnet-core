﻿using System;

namespace Gimli.JsonObjects.Outbound
{
	[Serializable]
	public class DocumentReadInfo
	{
		// + maybe neccessary 
		//public Guid DocumentId { get; set; }
		//public Guid ExperimentId { get; set; }
		//public Guid PlateIed { get; set; }
		// - maybe neccessary 


		public string User { get; set; }
		public string Document { get; set; }
		public string Plate { get; set; }
	}
}
