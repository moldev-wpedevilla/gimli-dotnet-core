﻿using System;

namespace Gimli.JsonObjects.Outbound
{
    [Serializable]
    public class GxPUnlockInstrumentInfo
    {
        public bool Unlock { get; set; }
    }
}
