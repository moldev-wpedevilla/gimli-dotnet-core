﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

namespace Gimli.JsonObjects.Outbound
{
	[Serializable]
	public class DocumentUpdateInfo
	{
        [JsonConstructor]
	    public DocumentUpdateInfo()
	    {
            Experiments = new List<ExperimentUpdateInfo>();
        }
	    public DocumentUpdateInfo(string name) :this()
	    {
	        DocumentName = name;
	    }
	    public string DocumentName { get; set; }
        public Guid DocumentId { get; set; }

	    public List<string> ExperimentsAndPlatesString()
	    {
            var list = new List<string>();
	        foreach (var experiment in Experiments)
	        {
	            list.AddRange(experiment.GetExperimentAndPlatesString(false));
	        }
	        return list;
	    }

	    public List<ExperimentUpdateInfo> Experiments { get; set; }
        public ExperimentUpdateInfo AddOrGetExperiment(string name)
        {
            foreach (var exp in Experiments)
            {
                if (exp.ExperimentName.ToLowerInvariant().Equals(name.ToLowerInvariant()))
                    return exp;
            }
            var newExp = new ExperimentUpdateInfo(name);
            Experiments.Add(newExp);
            return newExp;
        }
        public GxpUser UserInfo { get; set; }
    }
}
