﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Gimli.JsonObjects.Outbound
{
	[Serializable]
	public class ExperimentUpdateInfo
	{
	    private const string ExperimentPrefix = "Experiment_";

	    [JsonConstructor]
	    public ExperimentUpdateInfo()
	    {
            Plates = new List<PlateUpdateInfo>();
        }
        public ExperimentUpdateInfo(string name):this()
	    {
	        ExperimentName = name;
	    }
        public string ExperimentName { get; set; }
		public string ExperimentId { get; set; }
        public Guid ExperimentGuid
        {
            get
            {
                if (string.IsNullOrEmpty(ExperimentId)) return Guid.Empty;
                var guidString = ExperimentId.Replace(ExperimentPrefix, "");
                Guid guid;
                return Guid.TryParse(guidString, out guid) ? guid : Guid.Empty;
            }
            set { ExperimentId = ExperimentPrefix + value.ToString(); }
        }
        public List<PlateUpdateInfo> Plates { get; set; }
        public PlateUpdateInfo AddOrGetPlate(string name)
        {
            foreach (var plate in Plates)
            {
                if (plate.ElementName.ToLowerInvariant().Equals(name.ToLowerInvariant()))
                    return plate;
            }
            var newPlate = new PlateUpdateInfo(name);
            Plates.Add(newPlate);
            return Plates.LastOrDefault();
        }
	    public List<string> GetExperimentAndPlatesString(bool withGuid)
	    {
	        var list = new List<string>();
	        foreach (var plate in Plates)
	        {
	            list.Add(withGuid
	                ? $"{ExperimentName}|{ExperimentGuid}|{plate.GetPlateString(withGuid)}"
	                : $"{ExperimentName}|{plate.GetPlateString(withGuid)}");
	        }
	        return list;
	    } 
    }
}
