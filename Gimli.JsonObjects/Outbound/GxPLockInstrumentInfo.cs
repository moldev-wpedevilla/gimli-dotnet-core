﻿using System;

namespace Gimli.JsonObjects.Outbound
{
    [Serializable]
    public class GxPLockInstrumentInfo
    {
        public bool Lock { get; set; }
    }
}
