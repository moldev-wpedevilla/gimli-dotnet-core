﻿using System;

namespace Gimli.JsonObjects.Outbound
{
    [Serializable]
    public class MeasurementDataInfo : DataChangedInfo
    {
        public GxpUser User { get; set; }
        
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string DocumentName { get; set; }
        public string ExperimentName { get; set; }
        public string PlateName { get; set; }
    }
}
