﻿using System;
using Newtonsoft.Json;

namespace Gimli.JsonObjects.Outbound
{
	[Serializable]
	public class PlateUpdateInfo
	{
	    private const string PlatePrefix = "Plate_";
        [JsonConstructor]
        public PlateUpdateInfo() { }
        public PlateUpdateInfo(string plateName)
        {
            ElementName = plateName;
        }
        public string ElementName { get; set; }

	    public Guid ElementGuid
	    {
            get
            {
                if (string.IsNullOrEmpty(ElementId)) return Guid.Empty;
                var guidString = ElementId.Replace(PlatePrefix, "");
                Guid guid;
                return Guid.TryParse(guidString, out guid) ? guid : Guid.Empty;
            }
            set { ElementId = PlatePrefix + value.ToString(); }
        }

	    public string ElementId { get; set; }
        public string ElementLegend { get; set; }
        public bool IsSelected { get; set; }

	    public string GetPlateString(bool withGuid)
	    {

	        return withGuid
	            ? $"{ElementGuid}|{ElementName}|{IsSelected}|{ElementLegend}"
	            : $"{ElementName}";
	    }
	}
}
