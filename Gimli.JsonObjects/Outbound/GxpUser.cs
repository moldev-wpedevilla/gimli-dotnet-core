﻿using System;

namespace Gimli.JsonObjects.Outbound
{
	[Serializable]
	public class GxpUser
	{
		public string User { get; set; }
        public string ID { get; set; }
        public bool ReadEnabled { get; set; }
        public bool StopEnabled { get; set; }
        public ConnectionInfo Connection { get; set; }
    }
}
