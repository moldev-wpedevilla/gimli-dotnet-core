﻿using System;

namespace Gimli.JsonObjects.Outbound
{
	[Serializable]
	public class OutboundSession
	{
		/// <summary>
		/// The document the user currently views in SMP.
		/// </summary>
		public DocumentUpdateInfo Document { get; set; }
		public bool GxpMode { get; set; }
		public GxpUser User { get; set; }
		public string SmpHostname { get; set; }
		public string IpAddressOfClient { get; set; }
		public bool IsClientConnected { get; set; }
		/// <summary>
		/// In the connection phase the execution of onboard command could lead to connection failures.
		/// This indicates that the connnection phase is over.
		/// </summary>
		public bool IsOutboundConnectionStable => IsClientConnected && SmpHostname != null;

		public DataChangedInfo MeasurementProgress { get; set; }

		/// <summary>
		/// The document started and running.
		/// </summary>
		public DocumentReadInfo DocumentReadInfo { get; set; }
	}
}
