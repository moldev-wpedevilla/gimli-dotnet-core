﻿using System;

namespace Gimli.JsonObjects.Outbound
{
    [Serializable]
    public class ConnectionInfo 
    {
        public string HostName { get; set; }
        public string IP { get; set; }
    }
}
