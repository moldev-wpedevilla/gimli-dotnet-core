﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Gimli.JsonObjects
{
    public static class WellExtension
    {
        public static bool IfNotInRangeDeleteData(this Well well, PlateWellBase range)
        {
            if (range != null) return false;
            DeleteAllWellData(well);
	        return true;
        }

	    public static void DeleteAllWellData(this Well well)
	    {
		    well.CycleTime = null;
		    well.Points = null;
		    well.DataPointsState = null;
	    }

	    public static bool IsNotInRange(this Well well, PlateWellRange range)
        {
            return well.XPos < range.Point1.X
				|| well.XPos > range.Point2.X
				|| well.YPos < range.Point1.Y
				|| well.YPos > range.Point2.Y;
        }

		public static bool IsNotInArray(this Well well, PlateWellArray array)
		{
			return !array.Wells.Any(w => w.X == well.XPos && w.Y == well.YPos);
		}

		public static void RoundValues(this Well well, int decimalPlaces)
        {
			if (well.Points == null) return;
            for (var wl = 0; wl < well.Points.GetLength(0); wl++)
            {
                for (var r = 0; r < well.Points.GetLength(1); r++)
                {
                    for (var c = 0; c < well.Points.GetLength(2); c++)
                    {
                        well.Points[wl, r, c] = Math.Round(well.Points[wl, r, c], decimalPlaces);
                    }
                }
            }
        }

        public static Well GetWell(this List<Well> wells, int row, int col)
        {
            return wells?.FirstOrDefault(well => well.XPos == col && well.YPos == row);
        }

        
    }
}
