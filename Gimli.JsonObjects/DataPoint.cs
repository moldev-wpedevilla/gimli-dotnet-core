using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class DataPoint
	{
		//for Serialization only
		public DataPoint()
		{
			
		}

		public DataPoint(int xPos, int yPos, double value, Status status)
		{
			XPos = xPos;
			YPos = yPos;
			Value = value;
			Status = status;
		}

		public int XPos { get; set; }
		public int YPos { get; set; }
		public double Value { get; set; }
		public Status Status { get; set; }
	}
}