﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class DocumentCategory
	{
		public int Id { get; set; }
		public string Label { get; set; }
		public string SubLabel { get; set; }
		public string Filter { get; set; }
		public CategoryGroup GroupFlag { get; set; }
		public int Count { get; set; }
	}

	public enum CategoryGroup
	{
		None,
		Category,
		General
	}
}
