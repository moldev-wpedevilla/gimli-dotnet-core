﻿namespace Gimli.JsonObjects.Resources
{
    public static class GetResource
    {
        public static string GetString(string name)
        {
            if (string.IsNullOrEmpty(name)) return name;
            var text = JsonObjects.Properties.Resources.ResourceManager.GetString(name);
            if (string.IsNullOrEmpty(text)) return name;
            return text;
        }
    }
}
