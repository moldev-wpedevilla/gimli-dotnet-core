﻿using System;
using System.Collections;
using System.Diagnostics;

namespace Gimli.JsonObjects.DummyData
{
    public class GimliTrace
    {
        static readonly Hashtable mTable = new Hashtable();
        public static void Start(string name)
        {
#if DEBUG
            if (!mTable.ContainsKey(name))
                mTable.Add(name, null);
            mTable[name] = DateTime.Now;
#endif
        }

        public static int Stop(string name, string text = "")
        {
#if DEBUG
            if (!mTable.ContainsKey(name)) return 0;
            return GetMilliseconds(name, text, true);
#else
            return 0;
#endif
        }
        public static int ElapsedTime(string name, string text = "")
        {
            return GetMilliseconds(name, text);
        }

        public static int ElapsedRestart(string name, string text = "")
        {
            var time = GetMilliseconds(name, text);
            Start(name);
            return time;
        }

        private static int GetMilliseconds(string name, string text, bool removeItem = false)
        {
#if DEBUG
            if (!mTable.ContainsKey(name)) return 0;

            var startTime = (DateTime)mTable[name];
            var timeInt = (int)DateTime.Now.Subtract(startTime).TotalMilliseconds;

            if (timeInt > 0)
                Trace.WriteLine(string.Format("{0,-50}: {1,7} ms", string.IsNullOrEmpty(text) ? name : text, timeInt.ToString("N0")), "Trace");

            if (removeItem)
                mTable.Remove(name);

            return timeInt;
#else
            return 0;
#endif
        }
    }
}
