﻿//TempSolution
using System;
using System.Diagnostics.CodeAnalysis;
using Gimli.JsonObjects.Enumarations.Status;

namespace Gimli.JsonObjects.DummyData
{
    [ExcludeFromCodeCoverage]
    internal class DataForRoundAreaScan
    {
        private static double min;
        internal static void CreateRoundData(int points, double[,,] data, DataPointStatus[,,] state, int wl, ref double dataBase, double increment, int wellRow, int wellCol)
        {
            var percent = new[]
            {
                points/2d/100d*60,
                points/2d/100d*70,
                points/2d/100d*75,
                points/2d/100d*80,
                points/2d/100d*85
            };

            var halfPoints = points / 2d;

                min = dataBase;
                for (var y = 0; y < halfPoints; y++)
                {
                    for (var x = 0; x < halfPoints; x++)
                    {
                        var r = (x + y > 0 ? Math.Sqrt(y * y + x * x) : 0);
                        foreach (var d in percent)
                        {
                            if (r < d)
                            {
                                SetDataPointToContainer(y, x, (int)halfPoints, GetRandomValue(d), data, state, wl);
                                break;
                            }
                            //SetDataPointToContainer(y, x, (int)halfPoints, percent[4], data, state, wl);
                        }
                    }
                }
                dataBase += wellCol * wellRow * increment;
        }

        private static void SetDataPointToContainer(int y, int x, int halfPoint, double data, double[,,] wellData, DataPointStatus[,,] state, int wl)
        {
            var roundedData = Math.Round(data, 3);
            wellData[wl, y + halfPoint, x + halfPoint]= roundedData;
            wellData[wl, halfPoint - y, x + halfPoint]= roundedData;
            wellData[wl, y + halfPoint, halfPoint - x]= roundedData;
            wellData[wl, halfPoint - y, halfPoint - x]= roundedData;

            state[wl, y + halfPoint, x + halfPoint] = DataPointStatus.OK;
            state[wl, halfPoint - y, x + halfPoint] = DataPointStatus.OK;
            state[wl, y + halfPoint, halfPoint - x] = DataPointStatus.OK;
            state[wl, halfPoint - y, halfPoint - x] = DataPointStatus.OK;
        }

        private static readonly Random Rd = new Random();
        private static double GetRandomValue(double minimum)
        {
            return (Rd.NextDouble() + 0.001) / 4 + minimum + min;
        }
    }
}
