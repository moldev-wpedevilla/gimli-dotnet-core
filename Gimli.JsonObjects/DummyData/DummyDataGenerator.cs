﻿//TempSolution
using Gimli.JsonObjects.Enumarations.Status;
using Gimli.JsonObjects.Parameter;
using Gimli.JsonObjects.Settings;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;

namespace Gimli.JsonObjects.DummyData
{
    [ExcludeFromCodeCoverage]
    public static class DummyDataGenerator
    {
        static Random mRnd = new Random(DateTime.Now.Millisecond);
        //private static int Counter = 0;
        public static int[] CreateReadAreaMinMaxRowCol(int maxRows, int maxCols)
        {
            //minRow, minCol, maxRow, maxCol
            var array = new int[4];
            array[0] = 0;
            array[1] = 0;
            array[2] = maxRows;
            array[3] = maxCols;
            return array;

            //if (Math.IEEERemainder(Counter, 10) < double.Epsilon)
            //{
            //    array[2] = maxRows;
            //    array[3] = maxCols;
            //}
            //else if (Math.IEEERemainder(Counter, 4) < double.Epsilon)
            //{
            //    array[0] = 1;
            //    array[1] = mRnd.Next(1, maxCols - 1);
            //    array[2] = maxRows;
            //    array[3] = mRnd.Next(array[1], maxCols);
            //}
            //else if (Math.IEEERemainder(Counter, 6) < double.Epsilon)
            //{
            //    array[0] = mRnd.Next(1, maxRows - 1);
            //    array[1] = 1;
            //    array[2] = mRnd.Next(array[0], maxRows);
            //    array[3] = maxCols;
            //}
            //else
            //{
            //    array[0] = mRnd.Next(1, maxRows - 1);
            //    array[1] = mRnd.Next(1, maxCols - 1);
            //    array[2] = mRnd.Next(array[0], maxRows);
            //    array[3] = mRnd.Next(array[1], maxCols);
            //}
            //Counter++;
            //return array;
        }
        public static Document CreateProtocol(ProtocolListItem listItem, int experimentNumbers, int plateNumbers,
            int snapshotNumbers, int rows, int columns, int dataPointRows, int dataPointColumns,
            int pointIncrementY, int pointIncrementX, double maxValue, bool roundWell)
        {
            return CreateResult(listItem, experimentNumbers, plateNumbers, snapshotNumbers, rows, columns,
                dataPointRows, dataPointColumns, pointIncrementY, pointIncrementX, maxValue,
                false, 1, Mode.Abs, MeasurementType.AreaScan, "OD", 0, 0, roundWell);
        }

        public static Document CreateProtocol(ProtocolListItem listItem, int rows, int columns, int dataPointRows, int dataPointColumns, 
            double maxValue, bool roundWell, Mode mode, MeasurementType type)
        {
            return CreateResult(listItem, 1, 1, 1, rows, columns, dataPointRows, dataPointColumns, 1, 1, maxValue,
                false, 1, mode, type, "OD", 0, 0, roundWell);
        }

        public static Document CreateResult(ProtocolListItem listItem, int experimentNumbers, int plateNumbers,
            int snapshotNumbers, int rows, int columns, int dataPointRows,
            int dataPointColumns, int pointIncrementY, int pointIncrementX, double maxValue,
            int numberOfWavelengths, Mode mode, MeasurementType type, string unit, double pointXMin, double pointYMin, bool roundWell)
        {
            return CreateResult(listItem, experimentNumbers, plateNumbers, snapshotNumbers, rows, columns,
                dataPointRows, dataPointColumns, pointIncrementY, pointIncrementX, maxValue,
                true, numberOfWavelengths, mode, type, unit, pointXMin, pointYMin, roundWell);
        }

        private static Document CreateResult(ProtocolListItem listItem, int experimentNumbers, int plateNumbers,
            int snapshotNumbers, int rows, int columns, int dataPointRows, int dataPointColumns,
            int pointIncrementY, int pointIncrementX, double maxValue, bool withData,
            int numberOfWavelengths, Mode mode, MeasurementType type, string unit, double pointXMin, double pointYMin, bool roundWell)
        {
            var experiments = new List<Experiment>();

            for (var j = 0; j < experimentNumbers; j++)
            {
                experiments.Add(new Experiment(Guid.NewGuid(), "Experiment " + (j+1),
                    CreatePlates(plateNumbers, snapshotNumbers, rows, columns, dataPointRows, dataPointColumns,
                        pointIncrementY, pointIncrementX, maxValue, withData, numberOfWavelengths, mode, type, unit,
                        pointXMin, pointYMin, roundWell)));
            }

            return new Document(listItem.Id, listItem.UserNameGuid, listItem.ProtocolName, listItem.DisplayName,
                listItem.Timestamp, listItem.HasData, listItem.IsSynchronized, experiments) {IsProtected = listItem.IsProtected, LastUsedDate = listItem.LastUsedDate, IsPinned = listItem.IsPinned, Category = listItem.Category, PinDate = listItem.PinDate};
        }

        private static List<Plate> CreatePlates(int plateNumbers, int snapshotNumbers, int rows,
            int columns, int dataPointRows, int dataPointColumns, int pointIncrementY, int pointIncrementX,
            double maxValue,
            bool withData, int numberOfWavelengths, Mode mode, MeasurementType type, string unit, double pointXMin,
            double pointYMin, bool roundWell)
        {
            var plates = new List<Plate>();

            for (var k = 0; k < plateNumbers; k++)
            {
                var setting = new PlateSettings(rows, columns);
                plates.Add(new Plate(Guid.NewGuid(), "Plate " + (k+1),
                    CreateSnapShot(snapshotNumbers, rows, columns, dataPointRows, dataPointColumns,
                        pointIncrementY,
                        pointIncrementX, maxValue, withData, numberOfWavelengths, mode, type, unit, pointXMin, pointYMin, roundWell)){Settings=setting});
            }
            return plates;
        }

        private static List<Snapshot> CreateSnapShot(int snapshotNumbers, int rows,
            int columns,
            int dataPointRows, int dataPointColumns, int pointIncrementY, int pointIncrementX, double maxValue,
            bool withData,
            int numberOfWavelengths, Mode mode, MeasurementType type, string unit, double pointXMin, double pointYMin, bool roundWell)
        {
            var snapShot = new List<Snapshot>();

            for (var s = 0; s < snapshotNumbers; s++)
            {
                var measSettings = MeasurementSettings.Generate(mode, type);
                measSettings.ReadArea = ReadAreaSettings.CreateDefaultReadAreaSettings();

                var snap = new Snapshot(Guid.NewGuid(), mode, unit, type,
                    pointXMin, pointXMin + dataPointColumns*pointIncrementX - pointIncrementX, pointIncrementX,
                    pointYMin, pointYMin + dataPointRows*pointIncrementY - pointIncrementY, pointIncrementY,
                    CreateWells(rows, columns, dataPointRows, dataPointColumns, maxValue, withData, numberOfWavelengths, type, roundWell));

                snap.Settings = MeasurementSettings.Generate(mode, type);
                snap.Settings.ReadArea = ReadAreaSettings.CreateReadAreaSettings(rows, columns, false);

                var startstop = CreateReadAreaMinMaxRowCol(rows, columns);
                for (var r = startstop[0]; r < startstop[2]; r++)
                {
                    for (var c = startstop[1]; c < startstop[3]; c++)
                    {
                        snap.Settings.ReadArea.SetWell(r, c);
                    }
                }
                snapShot.Add(snap);

                if (type == MeasurementType.Kinetic)
                {
                    snap.Settings.Kinetic.IsUsed = true;
                    while (snap.Settings.Kinetic.Cycles.ToInt() < dataPointColumns)
                    {
                        snap.Settings.Kinetic.Cycles.Value = snap.Settings.Kinetic.Cycles.ToInt() + 1;
                    }
                }
                
                AddWavelengthInfoToSnapShot(snap, numberOfWavelengths, mode, type, pointIncrementX);
            }
            return snapShot;
        }

        public static void AddWavelengthInfoToSnapShot(Snapshot snap, int numberOfWavelengths, Mode mode, MeasurementType type, int pointIncrementX, bool areWavelengthsSet = false, int waveStart = 0, int waveStop = 0, int waveStep = 0)
        {
            if (snap.DataDimensionInfo == null) return;

            if (type == MeasurementType.SpectrumScan)
            {
                AddSpectrumWavelengthInfoToSnapShot(snap, pointIncrementX, mode, waveStart, waveStop, waveStep);
                return;
            }
            if (mode == Mode.Abs) AddAbsWavelengthInfoToSnapShot(snap, numberOfWavelengths, type, areWavelengthsSet);
            if (mode.IsModeInFluoroGroup()) AddFiWavelengthInfoToSnapShot(snap, numberOfWavelengths, type, areWavelengthsSet);
            if (mode == Mode.Lum) AddLumWavelengthInfoToSnapShot(snap, numberOfWavelengths, type);
        }

        private static void AddFiWavelengthInfoToSnapShot(Snapshot snap, int numberOfWavelengths, MeasurementType type, bool wavelengthsSet = false)
        {
            List<double> list;
            if (wavelengthsSet)
            {
                list = new List<double>();
                for (int wavelenIndex = 0; wavelenIndex < numberOfWavelengths; wavelenIndex++)
                {
                    list.Add(Convert.ToDouble(snap.Settings.Wavelengths.Excitation[wavelenIndex].Value) + Convert.ToDouble(snap.Settings.Wavelengths.Emission[wavelenIndex].Value)/1000.0);
                }
            }
            else
            {
                list = new List<double> { 435.485, 435.535, 485.600, 485.615, 360.465, 485.625 };
            }
            
            var wlDimensionInfo = snap.DataDimensionInfo[type == MeasurementType.AreaScan ? 0 : 1].DataPointLabel;
            snap.DataDimensionInfo[type == MeasurementType.AreaScan ? 0 : 1].DimensionName = "Ex.Em";
            snap.DataDimensionInfo[type == MeasurementType.AreaScan ? 0 : 1].DimensionType = typeof(string);
            wlDimensionInfo.Clear();

            snap.Settings.Wavelengths.NrOfWavelengths.Value = list.Count;
            snap.Settings.Wavelengths.Excitation = new List<GimliParameterWavelength>();
            snap.Settings.Wavelengths.Emission = new List<GimliParameterWavelength>();

            for (var i = 0; i < numberOfWavelengths; i++)
            {
                wlDimensionInfo.Add(list[i]);
                var em = Convert.ToDouble(list[i]);
                var ex = Convert.ToDouble(list[i]);
                ex = Math.Truncate(ex);
                em = (em - ex)*1000;
                snap.Settings.Wavelengths.Excitation.Add(new GimliParameterWavelength("Excitation" + (i + 1), Convert.ToInt32(ex)) { IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true, Units = "nm", Minimum = 230, Maximum = 1000, Step = 1 });
                snap.Settings.Wavelengths.Emission.Add(new GimliParameterWavelength("Emission" + (i + 1), Convert.ToInt32(em)) { IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true, Units = "nm", Minimum = 230, Maximum = 1000, Step = 1 });
            }
        }

        private static void AddWavelengthParam(string paramName, WavelengthSettings settings, int i, List<double> list)
        {
            List<GimliParameterWavelength> paramList = null; 
            if ("Excitation" == paramName) paramList = settings.Excitation;
            else if ("Emission" == paramName) paramList = settings.Emission;
            paramList.Add(new GimliParameterWavelength("Excitation" + (i + 1), Convert.ToInt32(list[i])) { IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true, Units = "nm", Minimum = 230, Maximum = 1000, Step = 1 });
        }

        private static void AddAbsWavelengthInfoToSnapShot(Snapshot snap, int numberOfWavelengths, MeasurementType type, bool wavelengthsSet = false)
        {
            List<double> list;
            if (wavelengthsSet)
            {
                list = new List<double>();
                for (int wavelenIndex = 0; wavelenIndex < numberOfWavelengths; wavelenIndex++)
                {
                    list.Add(Convert.ToDouble(snap.Settings.Wavelengths.Excitation[wavelenIndex].Value));
                }
            }
            else
            {
                list = new List<double> { 405, 450, 550, 572, 620, 690 };
            }
            
            var wlDimensionInfo = snap.DataDimensionInfo[type == MeasurementType.AreaScan ? 0:1].DataPointLabel;
            //snap.DataDimensionInfo[type == MeasurementType.AreaScan ? 0 : 1].DimensionName = "nm";
            //snap.DataDimensionInfo[type == MeasurementType.AreaScan ? 0 : 1].DimensionType = typeof (int);
            wlDimensionInfo.Clear();

            snap.Settings.Wavelengths.NrOfWavelengths.Value = numberOfWavelengths;
            snap.Settings.Wavelengths.Excitation = new List<GimliParameterWavelength>();

            AddWavelengthParams(snap, numberOfWavelengths, wlDimensionInfo, list, "Excitation");
        }

	    private static void AddWavelengthParams(Snapshot snap, int numberOfWavelengths, List<double> wlDimensionInfo, List<double> list, string name)
	    {
		    for (var i = 0; i < numberOfWavelengths; i++)
		    {
			    wlDimensionInfo.Add(list[i]);

			    AddWavelengthParam(name, snap.Settings.Wavelengths, i, list);
		    }
	    }

	    private static void AddLumWavelengthInfoToSnapShot(Snapshot snap, int numberOfWavelengths, MeasurementType type)
        {
            var list = new List<double> { 405, 450, 550, 572, 620, 690 };
            var wlDimensionInfo = snap.DataDimensionInfo[type == MeasurementType.AreaScan ? 0:1].DataPointLabel;
            //snap.DataDimensionInfo[type == MeasurementType.AreaScan ? 0 : 1].DimensionName = "nm";
            //snap.DataDimensionInfo[type == MeasurementType.AreaScan ? 0 : 1].DimensionType = typeof (int);
            wlDimensionInfo.Clear();

            snap.Settings.Wavelengths.NrOfWavelengths.Value = list.Count;
            snap.Settings.Wavelengths.Emission = new List<GimliParameterWavelength>();

			AddWavelengthParams(snap, numberOfWavelengths, wlDimensionInfo, list, "Emission");
		}

		private static void AddSpectrumWavelengthInfoToSnapShot(Snapshot snap, int pointIncrementX, Mode mode, int waveStart, int waveStop, int waveStep)
        {
            bool isSpecDef = !(waveStart == 0 && waveStop == 0 && waveStep == 0);

            var wlDimensionInfo = snap.DataDimensionInfo[2].DataPointLabel;
            var count = snap.Wells.FirstOrDefault().Points != null ? snap.Wells.FirstOrDefault().Points.GetLength(1) : 1;
            var start = 1000 - count * pointIncrementX;
            if (start > 600) start = 400;
            var prefix = mode.IsModeInFluoroGroup() ? 435d : 0d;

            wlDimensionInfo.Clear();

            snap.DataDimensionInfo[2].DimensionName = (Math.Abs(prefix) < double.Epsilon) ? ".Em" : "Ex.Em";
            snap.DataDimensionInfo[2].DimensionType = typeof(string);

            if (mode == Mode.Abs || mode.IsModeInFluoroGroup())
            {
                snap.Settings.Wavelengths.NrOfWavelengths.Value = 1;
                snap.Settings.Wavelengths.ExcitationStart = new GimliParameterWavelength("ExcitationStart", isSpecDef ? waveStart : start) { IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true, Units = "nm", Minimum = 230, Maximum = 1000, Step = 1 };
                snap.Settings.Wavelengths.ExcitationEnd = new GimliParameterWavelength("ExcitationEnd", isSpecDef ? waveStop : start + count*pointIncrementX) { IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true, Units = "nm", Minimum = 230, Maximum = 1000, Step = 1 };
                snap.Settings.Wavelengths.ExcitationStep = new GimliParameterInt("ExcitationStep", isSpecDef ? waveStep : pointIncrementX) { IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true, Units = "nm", Minimum = 1, Maximum = 50, Step = 1 };
                if (mode.IsModeInFluoroGroup())
				{
					snap.Settings.Wavelengths.Emission = GetDefaultWavelength("Emission", prefix);
				}
            }
            else
            {
                snap.Settings.Wavelengths.NrOfWavelengths.Value = 1;
                snap.Settings.Wavelengths.EmissionStart = new GimliParameterWavelength("EmissionStart", start) { IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true, Units = "nm", Minimum = 230, Maximum = 1000, Step = 1 };
                snap.Settings.Wavelengths.EmissionEnd = new GimliParameterWavelength("EmissionEnd", start + count * pointIncrementX) { IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true, Units = "nm", Minimum = 230, Maximum = 1000, Step = 1 };
                snap.Settings.Wavelengths.EmissionStep = new GimliParameterInt("EmissionStep", pointIncrementX) { IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true, Units = "nm", Minimum = 1, Maximum = 50, Step = 1 };
                snap.Settings.Wavelengths.ExcitationSweep = new GimliParameterBool("ExcitationSweep", true) { IsUsed = true, IsEnabled = true, IsDisplayedInSummary = true, Units = "nm", LegalValues = new ArrayList() {true, false} };
                if (mode.IsModeInFluoroGroup())
                {
	                snap.Settings.Wavelengths.Excitation = GetDefaultWavelength("Excitation", prefix);
                }
            }

            for (var i = 0; i < count; i++)
            {
                wlDimensionInfo.Add(prefix + (double)(start + i * pointIncrementX)/1000);
            }
        }

	    private static List<GimliParameterWavelength> GetDefaultWavelength(string name, double prefix)
	    {
		    return new List<GimliParameterWavelength>
		    {
			    new GimliParameterWavelength(name, Convert.ToInt32(prefix))
			    {
				    IsUsed = true,
				    IsEnabled = true,
				    IsDisplayedInSummary = true,
				    Units = "nm",
				    Minimum = 230,
				    Maximum = 1000,
				    Step = 1
			    }
		    };
	    }

		private static List<Well> CreateWells(int rows, int columns, int dataPointRows, int dataPointColumns,
            double maxValue,
            bool withData, int numberOfWavelengths, MeasurementType type, bool roundWell)
        {
            var wells = new List<Well>();

            for (var n = 0; n < rows; n++)
            {
                for (var o = 0; o < columns; o++)
                {
                    DataPointStatus[,,] dataStates;
                    var wellData = CreateWellDataAndFillWithData(dataPointRows, dataPointColumns, maxValue, withData,
                        out dataStates, numberOfWavelengths, type, n, o, roundWell);
                    wells.Add(new Well($"{(char) (n + 65)}{o + 1}", o, n, wellData, dataStates, false));
                }
            }
            return wells;
        }

        private static readonly Random ValueGenerator = new Random();

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Body")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1814:PreferJaggedArraysOverMultidimensional", MessageId = "Return")]
        public static double[,,] CreateWellDataAndFillWithData(int dataPointRows, int dataPointColumns, double maxValue,
            bool withData, out DataPointStatus[,,] dataStates, int numberOfWavelengths, MeasurementType type, int wellRow, int wellCol, bool roundWell)
        {
            dataStates = null;
            if (!withData) return null;

	        var z = numberOfWavelengths;
	        var y = dataPointRows;
	        var x = dataPointColumns;

	        if (type == MeasurementType.Endpoint || type == MeasurementType.Kinetic)
	        {
		        z = 1;
		        y = numberOfWavelengths;
		        x = dataPointColumns;
	        }

            var dataPoints = new double[z, y, x];
            dataStates = new DataPointStatus[z, y, x];

            var maxSimulatedValue = ValueGenerator.NextDouble()*6;
            var minSimulatedValue = ValueGenerator.NextDouble() + 1;
            var increment = (maxSimulatedValue - minSimulatedValue)/x;
            var actValue = minSimulatedValue;

            for (var wl = 0; wl < z; wl++)
            {
                if (y == x && x > 1) //AreaScanData -> Round Well
                {
                    if(roundWell)
                        DataForRoundAreaScan.CreateRoundData(y, dataPoints, dataStates, wl, ref actValue, increment, wellRow, wellCol);
                    else
                        DataForSquaredAreaScan.CreateRoundData(y, dataPoints, dataStates, wl, ref actValue, increment, wellRow, wellCol);
                    continue;
                }
                for (var p = 0; p < x; p++)
                {
                    for (var q = 0; q < y; q++)
                    {
                        actValue += increment;
                        dataPoints[wl, q, p] = (dataPointRows == 1 && dataPointColumns != 1) ? GenerateKineticValue(actValue, q) : ValueGenerator.NextDouble()*maxValue;
                        dataStates[wl, q, p] = DataPointStatus.OK;
                    }
                }
            }
            return dataPoints;
        }
        
        static double GenerateKineticValue(double randomValue, int wavelength)
        {
            return Math.Round(Math.Sin(randomValue) + 2 + 2 * wavelength, 3);
        }
    }
}
