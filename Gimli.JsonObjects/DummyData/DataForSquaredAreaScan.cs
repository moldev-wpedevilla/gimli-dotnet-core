﻿//TempSolution

using System.Diagnostics.CodeAnalysis;
using Gimli.JsonObjects.Enumarations.Status;

namespace Gimli.JsonObjects.DummyData
{
    [ExcludeFromCodeCoverage]
    internal class DataForSquaredAreaScan
    {
        internal static void CreateRoundData(int points, double[,,] data, DataPointStatus[,,] state, int wl,
            ref double dataBase, double increment, int wellRow, int wellCol)
        {
            var percent = new[]
            {
                dataBase/100d*20,
                dataBase/100d*55
            };

            for (var y = 0; y < points; y++)
            {
                for (var x = 0; x < points; x++)
                {
                    state[wl, y, x] = DataPointStatus.OK;
                    data[wl, y, x] = percent[0];
                    if (y==0 || y==points-1 || x==0 || x==points-1)
                        data[wl, y, x] = dataBase;
                    if ((y == 1 || y == points - 2) && (x != 0 && x != points - 1))
                        data[wl, y, x] = percent[1];
                    if ((y != 0 && y != points - 1) && (x == 1 || x == points - 2))
                        data[wl, y, x] = percent[1];
                }
            }
            dataBase += increment;
        }
    }
}

