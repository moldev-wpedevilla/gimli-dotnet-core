﻿using System;
using System.Collections.Generic;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class LastUsedDocuments
	{
		public List<ProtocolListItem> Results { get; set; }
		public List<ProtocolListItem> Protocols { get; set; }
		public List<ProtocolListItem> MixedDocuments { get; set; }
	}
}
