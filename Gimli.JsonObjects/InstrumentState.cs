﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public enum DeviceState
	{
		None,
		Idle,
		Busy
	}

	[Serializable]
	public class InstrumentState
	{
		public DeviceState State { get; set; }
		public bool IsOutboundLocked { get; set; }
		public string OutboundId { get; set; }
	}
}
