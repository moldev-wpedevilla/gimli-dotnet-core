﻿using System;

namespace Gimli.JsonObjects
{
	[Serializable]
	public class ReadPreferences
	{
		public bool AutoEjectPlate { get; set; }
		public bool ExportExcel { get; set; }
		public bool ReadPlateheightOnLoad { get; set; }
	}
}
