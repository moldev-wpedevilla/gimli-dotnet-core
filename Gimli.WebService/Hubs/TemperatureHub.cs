﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MolDev.Common.Logger;

namespace Gimli.WebService.Hubs
{
	public class TemperatureHub : Hub
	{
		private readonly IClientHubDictionary mClientHubs;
		private readonly ILogService mLogger;

		public TemperatureHub(IClientHubDictionary clientHubs, ILogService logger)
		{
			mClientHubs = clientHubs;
			mLogger = logger;
		}

		public override Task OnConnectedAsync()
		{
			mLogger.Info(LogCategoryEnum.WebApi, "TemperatureHub connected to " + Context.ConnectionId);
			mClientHubs.TemperaturehubDictionary[Context.ConnectionId] = this;
		    Task.Run(() =>
		    {
                Thread.Sleep(500);
                if(!string.IsNullOrEmpty(LastMessage))
		            Clients.Client(Context.ConnectionId).SendAsync("temperatureChanged", LastMessage);
		    });
            return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			mLogger.Info(LogCategoryEnum.WebApi, "TemperatureHub disconnected from " + Context.ConnectionId);
			mClientHubs.TemperaturehubDictionary.Remove(Context.ConnectionId);
			return base.OnDisconnectedAsync(exception);
		}

        internal static string LastMessage { get; set; }
	}
}
