﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.WebService.Actors;
using Gimli.WebService.Messages;
using Gimli.WebService.Properties;
using Microsoft.AspNetCore.SignalR;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;

namespace Gimli.WebService.Hubs
{
	public class MessageHub : Hub
	{
		private static bool DiskWarningSent = false;

		private readonly IActorService mActorService;
		private readonly IClientHubDictionary mClientHubs;
		private readonly ILogService mLogger;
		private readonly IMessageConfirmationContext mContext;

		public MessageHub(IActorService actorService, IClientHubDictionary clientHubs, ILogService logger, IMessageConfirmationContext context)
		{
			mActorService = actorService;
			mClientHubs = clientHubs;
			mLogger = logger;
			mContext = context;
		}

		public override Task OnConnectedAsync()
		{
			mLogger.Info(LogCategoryEnum.WebApi, "MessageHub connected to " + Context.ConnectionId);
			mClientHubs.MessagehubDictionary[Context.ConnectionId] = this;
			RepeatLastMessage();

			NotifyCoreService();
			NotifyAppManager();
			SendDiskUsageWarning();

			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			mLogger.Info(LogCategoryEnum.WebApi, "MessageHub disconnected from " + Context.ConnectionId);
			mClientHubs.MessagehubDictionary.Remove(Context.ConnectionId);

			NotifyAppManager(false);

			return base.OnDisconnectedAsync(exception);
		}

		private void RepeatLastMessage()
		{
			mActorService.Get<WebServiceRoutingActor>().Tell(new RepeatLastConfirmationMessage());
		}

		private void NotifyCoreService()
		{
			mActorService.Get<CoreServiceProxyActor>().Tell(MessageExtensions.CreateRequestMessage(new RunAutoStartMessage()));
		}

		private void NotifyAppManager(bool active = true)
		{
			mActorService.Get<AppManagerProxyActor>()
				.Tell(MessageExtensions.CreateRequestMessage(new FrontendHubConnectionMessage {Active = active}));
		}

		private void SendDiskUsageWarning()
		{
			if (DiskWarningSent) return;

			Task.Run(() =>
			{
				DiskUsageCalculator diskusageCalculator = null;
				try
				{
					diskusageCalculator = new DiskUsageCalculator();
				}
				catch (Exception e)
				{
					mLogger.Error(LogCategoryEnum.WebApi, "Error calculating disk usage: " + e);
				}

				const int cutoff = 90;
				if (diskusageCalculator == null	|| diskusageCalculator.OccupiedSpace <= cutoff) return;

				mActorService.Get<WebServiceRoutingActor>().Tell(MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
				{
					Parameters = new MessageParameters
					{
						Message = string.Format(CultureInfo.InvariantCulture, Resources.Too_Little_Disk_Space_Left, 100 - cutoff),
						MessageCode = MessageStatusCodes.DiskSpaceWarningMessage,
						DisplayType = MessageDisplayType.DialogOk,
						MessageType = MessageType.Warning
					}
				}));
			});

			DiskWarningSent = true;
		}
	}
}
