﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MolDev.Common.Logger;

namespace Gimli.WebService.Hubs
{
	public class NFCHub : Hub
	{
		private readonly IClientHubDictionary mClientHubs;
		private readonly ILogService mLogger;

		public NFCHub(IClientHubDictionary clientHubs, ILogService logger)
		{
			mClientHubs = clientHubs;
			mLogger = logger;
		}

		public override Task OnConnectedAsync()
		{
			mLogger.Info(LogCategoryEnum.WebApi, "NFCHub connected to " + Context.ConnectionId);
			mClientHubs.NFChubDictionary[Context.ConnectionId] = this;
			Task.Run(() =>
			{
				Thread.Sleep(500);
				Clients.Client(Context.ConnectionId).SendAsync("configurationChanged", LastConfigMessage);
			});
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			mLogger.Info(LogCategoryEnum.WebApi, "NFCHub disconnected from " + Context.ConnectionId);
			mClientHubs.NFChubDictionary.Remove(Context.ConnectionId);
			return base.OnDisconnectedAsync(exception);
		}

		internal static bool LastConfigMessage { get; set; }
	}
}
