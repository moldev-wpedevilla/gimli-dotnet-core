﻿using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR;

namespace Gimli.WebService.Hubs
{
	public class ClientHubDictionary:  IClientHubDictionary
	{
		public ClientHubDictionary()
		{
			BusyhubDictionary = new Dictionary<string, Hub>();
			TemperaturehubDictionary = new Dictionary<string, Hub>();
			DatahubDictionary = new Dictionary<string, Hub>();
			InjectorhubDictionary = new Dictionary<string, Hub>();
			TimehubDictionary = new Dictionary<string, Hub>();
			DocumenthubDictionary = new Dictionary<string, Hub>();
			InstrumentHubDictionary = new Dictionary<string, Hub>();
			MessagehubDictionary = new Dictionary<string, Hub>();
			NFChubDictionary = new Dictionary<string, Hub>();
			NetworkHubDictionary = new Dictionary<string, Hub>();

			Dictionaries = new List<Dictionary<string, Hub>>
			{
				BusyhubDictionary,
				TemperaturehubDictionary,
				DatahubDictionary,
				InjectorhubDictionary,
				TimehubDictionary,
				DocumenthubDictionary,
				InstrumentHubDictionary,
				MessagehubDictionary,
				NFChubDictionary,
				NetworkHubDictionary
			};
		}

		public List<Dictionary<string, Hub>> Dictionaries { get; }
		public Dictionary<string, Hub> BusyhubDictionary { get; }
		public Dictionary<string, Hub> NetworkHubDictionary { get; }
		public Dictionary<string, Hub> DatahubDictionary { get; }
		public Dictionary<string, Hub> InjectorhubDictionary { get; }
		public Dictionary<string, Hub> TemperaturehubDictionary { get; }
		public Dictionary<string, Hub> DocumenthubDictionary { get; }
		public Dictionary<string, Hub> InstrumentHubDictionary { get; }
		public Dictionary<string, Hub> MessagehubDictionary { get; }
		public Dictionary<string, Hub> NFChubDictionary { get; }
		public Dictionary<string, Hub> TimehubDictionary { get; }
	}
}
