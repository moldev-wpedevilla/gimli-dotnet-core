﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MolDev.Common.Logger;

namespace Gimli.WebService.Hubs
{
	public class NetworkHub : Hub
	{
		private readonly IClientHubDictionary mClientHubs;
		private readonly ILogService mLogger;

		public NetworkHub(IClientHubDictionary clientHubs, ILogService logger)
		{
			mClientHubs = clientHubs;
			mLogger = logger;
		}

		public override Task OnConnectedAsync()
		{
			mLogger.Info(LogCategoryEnum.WebApi, "NetworkHub connected to " + Context.ConnectionId);
			mClientHubs.NetworkHubDictionary[Context.ConnectionId] = this;
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			mLogger.Info(LogCategoryEnum.WebApi, "NetworkHub disconnected from " + Context.ConnectionId);
			mClientHubs.NetworkHubDictionary.Remove(Context.ConnectionId);
			return base.OnDisconnectedAsync(exception);
		}
	}
}
