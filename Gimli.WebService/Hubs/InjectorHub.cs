﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MolDev.Common.Logger;

namespace Gimli.WebService.Hubs
{
	public class InjectorHub : Hub
	{
		private readonly IClientHubDictionary mClientHubs;
		private readonly ILogService mLogger;

		public InjectorHub(IClientHubDictionary clientHubs, ILogService logger)
		{
			mClientHubs = clientHubs;
			mLogger = logger;
		}

		public override Task OnConnectedAsync()
		{
			mLogger.Info(LogCategoryEnum.WebApi, "InjectorHub connected to " + Context.ConnectionId);
			mClientHubs.InjectorhubDictionary[Context.ConnectionId] = this;
			Task.Run(() =>
			{
				Thread.Sleep(500);
				if (!string.IsNullOrEmpty(LastConfigMessage))
					Clients.Client(Context.ConnectionId).SendAsync("configurationChanged", LastConfigMessage);
			});
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			mLogger.Info(LogCategoryEnum.WebApi, "InjectorHub disconnected from " + Context.ConnectionId);
			mClientHubs.InjectorhubDictionary.Remove(Context.ConnectionId);
			return base.OnDisconnectedAsync(exception);
		}

		internal static string LastConfigMessage { get; set; }
	}
}
