﻿using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR;

namespace Gimli.WebService.Hubs
{
	public interface IClientHubDictionary
	{
		List<Dictionary<string, Hub>> Dictionaries { get; }
		Dictionary<string, Hub> BusyhubDictionary { get; }
		Dictionary<string, Hub> NetworkHubDictionary { get; }
		Dictionary<string, Hub> DatahubDictionary { get; }
		Dictionary<string, Hub> InjectorhubDictionary { get; }
		Dictionary<string, Hub> TemperaturehubDictionary { get; }
		Dictionary<string, Hub> DocumenthubDictionary { get; }
		Dictionary<string, Hub> InstrumentHubDictionary { get; }
		Dictionary<string, Hub> MessagehubDictionary { get; }
		Dictionary<string, Hub> NFChubDictionary { get; }
		Dictionary<string, Hub> TimehubDictionary { get; }
	}
}
