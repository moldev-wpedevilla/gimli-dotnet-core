﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Gimli.WebService.Hubs
{
	public class TimeHub : Hub
	{
		private readonly IClientHubDictionary  mClientHubs;
		private bool mTimeSending;

		public TimeHub(IClientHubDictionary clientHubs)
		{
			mClientHubs = clientHubs;
		}

		public async void Send()
		{
			//await StartTimePushing();
		}

		public void StopSend()
		{
			var other = GetTimeHub();
			if (other == null) return;

			other.mTimeSending = false;
		}

		private TimeHub GetTimeHub()
		{
			if (!mClientHubs.TimehubDictionary.ContainsKey(Context.ConnectionId))
			{
				return null;
			}

			var other = mClientHubs.TimehubDictionary[Context.ConnectionId] as TimeHub;
			if (other == null) throw new NotSupportedException("Wrong type of hub.");
			return other;
		}

		//private async Task StartTimePushing()
		//{
		//	var other = GetTimeHub();
		//	if (other != null) return;

		//	mClientHubs.TimehubDictionary[Context.ConnectionId] = this;
		//	mTimeSending = true;
		//	Clients.All.SendAsync("addMessage", "SignalR transport technology: " + Context.QueryString["transport"]);
		//	while (mTimeSending)
		//	{
		//		Clients.Caller.SendAsync("addMessage", "Notification from server: " + DateTime.Now.ToString(CultureInfo.InvariantCulture));

		//		await Task.Delay(500);
		//	}

		//	mClientHubs.TimehubDictionary.Remove(Context.ConnectionId);
		//}
	}
}
