﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MolDev.Common.Logger;

namespace Gimli.WebService.Hubs
{
	public class DocumentHub : Hub
	{
		private readonly IClientHubDictionary mClientHubs;
		private readonly ILogService mLogger;

		public DocumentHub(IClientHubDictionary clientHubs, ILogService logger)
		{
			mClientHubs = clientHubs;
			mLogger = logger;
		}

		public override Task OnConnectedAsync()
		{
			mLogger.Info(LogCategoryEnum.WebApi, "DocumentHub connected to " + Context.ConnectionId);
			mClientHubs.DocumenthubDictionary[Context.ConnectionId] = this;
			Task.Run(() =>
			{
				Thread.Sleep(500);
				if (!string.IsNullOrEmpty(LastImportMessage))
					Clients.Client(Context.ConnectionId).SendAsync("documentsForImport", LastImportMessage);
			});
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			mLogger.Info(LogCategoryEnum.WebApi, "DocumentHub disconnected from " + Context.ConnectionId);
			mClientHubs.DocumenthubDictionary.Remove(Context.ConnectionId);
			return base.OnDisconnectedAsync(exception);
		}

        internal static string LastImportMessage { get; set; }
	}
}
