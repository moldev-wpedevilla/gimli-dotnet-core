﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MolDev.Common.Logger;

namespace Gimli.WebService.Hubs
{
	public class DataHub : Hub
	{
		private readonly IClientHubDictionary mClientHubs;
		private readonly ILogService mLogger;

		public DataHub(IClientHubDictionary clientHubs, ILogService logger)
		{
			mClientHubs = clientHubs;
			mLogger = logger;
		}

		public override Task OnConnectedAsync()
		{
			mLogger.Info(LogCategoryEnum.WebApi, "DataHub connected to " + Context.ConnectionId);
			mClientHubs.DatahubDictionary[Context.ConnectionId] = this;
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			mLogger.Info(LogCategoryEnum.WebApi, "DataHub disconnected from " + Context.ConnectionId);
			mClientHubs.DatahubDictionary.Remove(Context.ConnectionId);
			return base.OnDisconnectedAsync(exception);
		}
	}
}
