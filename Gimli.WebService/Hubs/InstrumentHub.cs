﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MolDev.Common.Logger;

namespace Gimli.WebService.Hubs
{
	public class InstrumentHub : Hub
	{
		private readonly IClientHubDictionary mClientHubs;
		private readonly ILogService mLogger;

		public InstrumentHub(IClientHubDictionary clientHubs, ILogService logger)
		{
			mClientHubs = clientHubs;
			mLogger = logger;
		}

		public override Task OnConnectedAsync()
		{
			mLogger.Info(LogCategoryEnum.WebApi, "InstrumentHub connected to " + Context.ConnectionId);
			mClientHubs.InstrumentHubDictionary[Context.ConnectionId] = this;
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			mLogger.Info(LogCategoryEnum.WebApi, "InstrumentHub disconnected from " + Context.ConnectionId);
			mClientHubs.InstrumentHubDictionary.Remove(Context.ConnectionId);
			return base.OnDisconnectedAsync(exception);
		}
	}
}
