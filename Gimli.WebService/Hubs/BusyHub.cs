﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MolDev.Common.Logger;

namespace Gimli.WebService.Hubs
{
	public class BusyHub : Hub
	{
		private readonly IClientHubDictionary mClientHubs;
		private readonly ILogService mLogger;

		public BusyHub(IClientHubDictionary clientHubs, ILogService logger)
		{
			mClientHubs = clientHubs;
			mLogger = logger;
		}

		public override Task OnConnectedAsync()
		{
			mLogger.Info(LogCategoryEnum.WebApi, "BusyHub connected to " + Context.ConnectionId);
			mClientHubs.BusyhubDictionary[Context.ConnectionId] = this;
            Task.Run(() =>
            {
                Thread.Sleep(500);
                Clients.Client(Context.ConnectionId).SendAsync("deviceInit", LastIsInitializingMessage);
            });

            return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			mLogger.Info(LogCategoryEnum.WebApi, "BusyHub disconnected from " + Context.ConnectionId);
			mClientHubs.BusyhubDictionary.Remove(Context.ConnectionId);
			return base.OnDisconnectedAsync(exception);
		}

        internal static bool LastIsInitializingMessage { get; set; }
    }
}
