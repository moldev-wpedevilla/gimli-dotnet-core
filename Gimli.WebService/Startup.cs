using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.AppStarter;
using Gimli.WebService.Actors;
using Gimli.WebService.Hubs;
using Microsoft.AspNetCore.SignalR;
using Nancy.Owin;
using MolDev.Common.Logger;
using MolDev.Common.Akka.Interfaces;
using System.Reflection;
using Gimli.JsonObjects;
using Microsoft.AspNetCore.Http.Features;

namespace Gimli.WebService
{
	public class Startup
	{
		private static GimliActorHost GimliActorHost;

		private readonly IConfiguration config;

		//public Startup(IWebHostEnvironment env)
		//{
		//	var builder = new ConfigurationBuilder();
		//		//.AddJsonFile("appsettings.json")
		//		//.SetBasePath(env.ContentRootPath);

		//	config = builder.Build();
		//}

		//public void Configure(IApplicationBuilder app)
		//{
		//	app.UseCors("AllowAll");
		//	app.UseSignalR(hubBuilder =>
		//	{
		//		hubBuilder.MapHub<BusyHub>("/signalr");
		//		hubBuilder.MapHub<DataHub>("/signalr");
		//		hubBuilder.MapHub<DocumentHub>("/signalr");
		//		hubBuilder.MapHub<InjectorHub>("/signalr");
		//		hubBuilder.MapHub<InstrumentHub>("/signalr");
		//		hubBuilder.MapHub<MessageHub>("/signalr");
		//		hubBuilder.MapHub<NetworkHub>("/signalr");
		//		hubBuilder.MapHub<NFCHub>("/signalr");
		//		hubBuilder.MapHub<TemperatureHub>("/signalr");
		//		hubBuilder.MapHub<TimeHub>("/signalr");
		//	}).UseOwin(x => x.UseNancy(cfg =>
		//	{
		//		cfg.Bootstrapper = new NancyBootstrapper(GimliActorHost.Container, "Client");
		//	}));
		//}



		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			var thisAssemblyName = Assembly.GetEntryAssembly().GetName().Name;
			var configFilePath = thisAssemblyName + ".dll.config";
			AppConfigurationManager.Build(configFilePath);

			GimliActorHost = new GimliActorHost(RegisterComponents, configFilePath);
			GimliActorHost.Start(RemoteActorConfiguration.Instance.ServiceConfigurations[ActorServiceKey.Webservice], false);
			services.AddHttpContextAccessor();
			services.AddTransient(serviceProvider => GimliActorHost.Container.Resolve<IClientHubDictionary>());
			services.AddTransient(serviceProvider => GimliActorHost.Container.Resolve<ILogService>());
			services.AddTransient(serviceProvider => GimliActorHost.Container.Resolve<IActorService>());
			services.AddTransient(serviceProvider => GimliActorHost.Container.Resolve<IMessageConfirmationContext>());
			services.AddSignalR();
			//services.AddMvc()
			//	.AddNewtonsoftJson();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			//app.UseCors("AllowAll");
			app.UseSignalR(hubBuilder =>
			{
				// TODO someone: test this (maybe each hub needs own route path maybe also requires changes in frontend)
				hubBuilder.MapHub<BusyHub>("/signalr");
				hubBuilder.MapHub<DataHub>("/signalr");
				hubBuilder.MapHub<DocumentHub>("/signalr");
				hubBuilder.MapHub<InjectorHub>("/signalr");
				hubBuilder.MapHub<InstrumentHub>("/signalr");
				hubBuilder.MapHub<MessageHub>("/signalr");
				hubBuilder.MapHub<NetworkHub>("/signalr");
				hubBuilder.MapHub<NFCHub>("/signalr");
				hubBuilder.MapHub<TemperatureHub>("/signalr");
				hubBuilder.MapHub<TimeHub>("/signalr");
			}).UseOwin(x => x.UseNancy(cfg =>
			{
				cfg.Bootstrapper = new NancyBootstrapper(GimliActorHost.Container, "Client");
			}));
		}


		internal static void RegisterComponents(IWindsorContainer container)
		{
			var instrumentServerConfig = StartupConfigurationHandler.GetConfigByName("Gimli.InstrumentServer.dll.config");
			bool connectToSimulator;
			const string simulatorKey = "ConnectToSimulator";
			if (instrumentServerConfig.AppSettings.Settings.AllKeys.Contains(simulatorKey))
			{
				bool.TryParse(instrumentServerConfig.AppSettings.Settings[simulatorKey].Value, out connectToSimulator);
			}
			else
			{
				connectToSimulator = false;
			}

			container.Register(Component
				.For<IClientHubDictionary>()
				.ImplementedBy<ClientHubDictionary>()
				.LifeStyle.Singleton);
			container.Register(Classes
				.FromAssembly(typeof(DataHub).Assembly)
				.BasedOn<Hub>()
				.Configure(x => x.Named(x.Implementation.FullName.ToLower()))
				.LifestyleTransient());
			container.Register(Component
				.For<IMessageConfirmationContext>()
				.ImplementedBy<MessageConfirmationContext>()
				.LifestyleSingleton());
			container.Register(Component
				.For<IActorsystemOverallState>()
				.Instance(new ActorsystemOverallState { ServiceId = ActorServiceKey.Webservice })
				.LifestyleSingleton());
			container.Register(Component.For<Func<GimliActorHost>>().Instance(() => GimliActorHost));
			container.Register(Component.For<Func<bool>>().Instance(() => connectToSimulator));
		}

		internal static void Run(string serviceName,
		   string serviceDisplayName,
		   string serviceDescription,
		   Action<IWindsorContainer> additionalInstall = null)
		{
			//HostFactory.Run(x =>
			//{
			//	x.Service<WebserviceHost>(s =>
			//	{
			//		s.ConstructUsing(() =>
			//		{
						//Host = new WebserviceHost(additionalInstall);
			//		});
			//		s.WhenStarted(tc => tc.Start());
			//		s.WhenStopped(tc => tc.Stop());
			//	});

			//	x.RunAsLocalSystem();
			//	x.SetDescription(serviceDescription);
			//	x.SetDisplayName(serviceDisplayName);
			//	x.SetServiceName(serviceName);
			//});
		}
	}
}
