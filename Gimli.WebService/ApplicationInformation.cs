﻿namespace Gimli.WebService
{
    class ApplicationInformation
    {
        //### Don't change this name - is set by 'UpdateBuildNumber' script.
        public const string BuildNumber = "000000";

        private ApplicationInformation()
        {
        }
    }
}
