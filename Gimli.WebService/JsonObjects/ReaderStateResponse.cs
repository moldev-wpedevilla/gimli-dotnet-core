﻿using System;
using Gimli.Shared.Actors.Messages;

namespace Gimli.WebService.JsonObjects
{
	
	public class ReaderStateResponse
	{
		public DataChangedMessage ReadState { get; set; }
		public string ReaderState { get; set; }
	}
}
