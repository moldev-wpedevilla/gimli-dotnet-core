﻿using System;

namespace Gimli.WebService.JsonObjects
{
	
	internal class StartReadResponse
	{
		public Guid DocumentId { get; set; }
		public Guid ExperimentId { get; set; }
		public Guid PlateId { get; set; }
		
	}
}
