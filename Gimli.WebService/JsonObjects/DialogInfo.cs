﻿namespace Gimli.WebService.JsonObjects
{
	public class DialogInfo
	{
		public string Title { get; set; }
		public string Description { get; set; }
		public string Button { get; set; }
	}
}
