﻿using System;

namespace Gimli.WebService.JsonObjects
{
	
	public class SaveAsInfo
	{
		public string Name { get; set; }
		public Guid DocumentId { get; set; }
	}
}
