﻿namespace Gimli.WebService.JsonObjects
{
	public class OutboundConnectionUpdate
	{
		public string Computer { get; set; }
		public string User { get; set; }
		public string Type { get; set; }
	}
}
