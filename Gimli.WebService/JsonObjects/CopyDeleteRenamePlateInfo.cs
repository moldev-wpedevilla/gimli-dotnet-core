﻿using System;

namespace Gimli.WebService.JsonObjects
{
	
	public class CopyDeleteRenamePlateInfo
	{
		public Guid DocumentId { get; set; }
		public Guid PlateId { get; set; }
		public string Name { get; set; }
	}
}
