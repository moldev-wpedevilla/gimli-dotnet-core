﻿using Gimli.JsonObjects;

namespace Gimli.WebService.JsonObjects
{
	public class ExtendedReaderConfig : ReaderConfiguration
	{
		public ExtendedReaderConfig(ReaderConfiguration baseConfig)
		{
			PICVersion = baseConfig.PICVersion;
			PICEEPROM = baseConfig.PICEEPROM;
			FirmwareVersion = baseConfig.FirmwareVersion;
			FirmwareEEPROM = baseConfig.FirmwareEEPROM;
			SerialNumber = baseConfig.SerialNumber;
			DeviceNumber = baseConfig.DeviceNumber;
			DeviceType = baseConfig.DeviceType;
			AssignedIp = baseConfig.AssignedIp;
			Hostname = baseConfig.Hostname;
			ReadBarcodeOnLoad = baseConfig.ReadBarcodeOnLoad;
		    ModuleName = baseConfig.ModuleName;
		    ModuleSerialNumber = baseConfig.ModuleSerialNumber;
		}

		public double OccupiedDiskSpace { get; set; }
		public double FreeDiskSpace { get; set; }
		public string SpaceUnit { get; set; }
		public string MacAddress { get; set; }
	}
}
