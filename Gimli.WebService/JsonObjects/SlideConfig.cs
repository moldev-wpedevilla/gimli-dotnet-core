﻿using System;
using Gimli.JsonObjects;
using Gimli.JsonObjects.FiltersMngmnt;

namespace Gimli.WebService.JsonObjects
{
	public class FilterslideConfiguration
	{
		public SlideConfig SlideEm { get; set; }
		public SlideConfig SlideEx { get; set; }
	}

	
	public class SlideConfig
	{
		public string Error { get; set; }
		public string Name { get; set; }
		public FilterSlotType Type { get; set; }
		public FilterCubeData[] FilterCubes { get; set; }
	}

	
	public class FilterCubeData
	{
		public string Name { get; set; }
		public Mode[] ReadModes { get; set; }
		public FilterType Type { get; set; }
		public Band[] Bands { get; set; }
		public Polarization Polarize { get; set; }
		public GraphPoint[] Graph { get; set; }
		public string Error { get; set; }
	}

	
	public class Band
	{
		public int Cwl { get; set; }
        public int Tr { get; set; }
		public int Bw { get; set; }
	}

	
	public class GraphPoint
	{
		public int X { get; set; }
		public int Y { get; set; }
	}
}
