﻿using System;
using System.Collections.Generic;
using Gimli.JsonObjects.InjectorMaintenance;

namespace Gimli.WebService.JsonObjects
{
	public enum UserInjectionMode
	{
		None,
		Prime,
		Wash,
		Manual
	}

	public enum InjectionDirection
	{
		None,
		Normal,
		Reverse
	}


	
	public class InjectionReqest
	{
		public UserInjectionMode Mode { get; set; }
		public List<InjectorSel> Injectors { get; set; }
		public InjectionDirection Direction { get; set; }
		public int Amount{ get; set; }
	}
}
