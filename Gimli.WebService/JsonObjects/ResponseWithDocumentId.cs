﻿using System;

namespace Gimli.WebService.JsonObjects
{
	
	internal class ResponseWithDocumentId
	{
		public Guid DocumentId { get; set; }
	}
}
