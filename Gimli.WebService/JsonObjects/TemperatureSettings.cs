﻿using Gimli.JsonObjects;

namespace Gimli.WebService.JsonObjects
{
	internal class TemperatureSettings
	{
		public DoubleValueWithRange Target { get; set; }
		public bool IsControllerRunning { get; set; }
	}
}
