﻿using System.Collections.Generic;
using Gimli.JsonObjects.InjectorMaintenance;

namespace Gimli.WebService.JsonObjects
{
	public class InjectorsCounter
	{
		public List<InjectorCounters> Injectors { get; set; }
	}
}
