﻿using Gimli.JsonObjects;

namespace Gimli.WebService.JsonObjects
{
	public class OutboundDataUpdate
	{
		public string Name { get; set; }
		public string Experiment { get; set; }
		public string Plate { get; set; }
		public MeasurementStatus Running { get; set; }
		public string User { get; set; }
	}
}
