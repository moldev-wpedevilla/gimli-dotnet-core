﻿using System;

namespace Gimli.WebService.JsonObjects
{
	
	public class RenameDocumentInfo
	{
		public Guid DocumentId { get; set; }
		public string Name { get; set; }
	}
}
