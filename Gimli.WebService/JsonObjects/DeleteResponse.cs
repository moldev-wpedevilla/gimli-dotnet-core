﻿using System;

namespace Gimli.WebService.JsonObjects
{
	
	public class DeleteResponse
	{
		public Guid PlateId { get; set; }
	}
}
