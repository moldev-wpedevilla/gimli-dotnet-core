﻿using Gimli.JsonObjects;

namespace Gimli.WebService.JsonObjects
{
    internal class ShakeSettings : Gimli.JsonObjects.ShakeSettings
	{
		public ListValueWithLegalValues<bool> Activate { get; set; }
    }
}
