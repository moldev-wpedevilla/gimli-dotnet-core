﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Gimli.JsonObjects;
using Gimli.JsonObjects.FiltersMngmnt;
using Gimli.WebService.Properties;
using MD.Nfc.Basic.StrTagConverting;

namespace Gimli.WebService.JsonObjects
{
	internal static class FilterTagDataExtensions
	{
		internal static SlideConfig TransformToSlideDataMessage(this SliderTagData slideInfo, string slot, Action<string> errorLogger)
		{
			if (slideInfo == null) return null;
			if (slideInfo.Filter.Length != 6) errorLogger($"Only {slideInfo.Filter.Length} in filter slide. 6 expected.");

			var cubes = new List<FilterCubeData>();
			var slideConfig = new SlideConfig
			{
				Name = slideInfo.Name,
				Type = slot.IsNullOrEmpty() ? FilterSlotType.None : "A" == slot ? FilterSlotType.Ex : FilterSlotType.Em,
			};

			// have to reverse filter list because the items in the list are orderd as they are read from the instrument beginning with the cube on the far right side in the filter slide dialog
			foreach (var filter in slideInfo.Filter.Reverse())
			{
				var cube = filter.TransformFilterCubeData(errorLogger);

				cubes.Add(cube);

				if (!cube.Error.IsNullOrEmpty()) slideConfig.Error = cube.Error;
			}

			slideConfig.FilterCubes = cubes.ToArray();

			return slideConfig;
		}

		public static FilterCubeData TransformFilterCubeData(this FilterTagData filter, Action<string> errorLogger)
		{
			var polarization = (Polarization) filter.Polarization;
			var modes = filter.IntrpretMeasurementType();
			var filterType = filter.IntrpretFilterType();
			if (polarization != Polarization.None && (modes.Count != 1 || modes[0] != Mode.Fp))
			{
				errorLogger($"Polarization only allowed with Fp (actually is [{string.Join(", ", modes)}]).");
			}

			var bands = new List<Band>();
			for (var i = 0; i < filter.Band.Length; i++)
			{
                if (filter.Band[i].CentralWavelengthOrTransition == 0 && filter.Band[i].Transmission == 0 && filter.Band[i].Bandwidth == 0) continue;
                bands.Add(new Band {Cwl = filter.Band[i].CentralWavelengthOrTransition, Tr = filter.Band[i].Transmission, Bw = filter.Band[i].Bandwidth });
			}

			var cube = new FilterCubeData
			{
				Name = filter.Name ?? "None",
				ReadModes = modes.ToArray(),
				Type = filterType,
				Bands = bands.ToArray(),
				Polarize = polarization,
				Error = filter.IsValid ? string.Empty : Resources.ResourceManager.GetString("Filtertag_Read_Error")
			};
			return cube;
		}
	}
}
