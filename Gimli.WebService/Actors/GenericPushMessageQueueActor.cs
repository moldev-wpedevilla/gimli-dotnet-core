﻿using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.WebService.Actors
{
	internal class GenericPushMessageQueueActor : GimliActorBase
	{
		private readonly string WebsocketCommunicationActorName = nameof(WebsocketCommunicationActor);
		public GenericPushMessageQueueActor(IActorService actorService, IMessageConfirmationContext context, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			Receive<RequestMessage<GenericFrontendPushMessage>>(message =>
			{
				context.RepeatMessage = message.Model;
				var websocketActor = GetChild<WebsocketCommunicationActor>(WebsocketCommunicationActorName);
				websocketActor.Forward(message);

				context.WaitForNext();
			});
		}

		protected override void PreStart()
		{
			GetChild<WebsocketCommunicationActor>(WebsocketCommunicationActorName);

			base.PreStart();
		}
	}
}
