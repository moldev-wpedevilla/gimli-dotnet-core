﻿using System.Threading;
using Akka.Actor;
using Gimli.JsonObjects;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.ExportImport;
using Gimli.Shared.Actors.Messages.FilterMngmnt;
using Gimli.Shared.Actors.Messages.InjectorMaintenance;
using Gimli.Shared.Actors.Messages.Instrument;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using Gimli.Shared.Actors.Messages.Outbound;
using Gimli.WebService.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.WebService.Actors
{
	public class WebsocketCommunicationRoutingActor : RemoteRegistrationActor
	{
		protected readonly string WebsocketCommunicationActorName = nameof(WebsocketCommunicationActor);
		protected readonly string GenericPushMessageQueueActorName = nameof(GenericPushMessageQueueActor);

		public WebsocketCommunicationRoutingActor(IActorService actorService, IMessageConfirmationContext context, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			Receive<RequestMessage<RemoteRegistrationMessage>>(message =>
			{
				Sender.Tell(HandleRemoteRegistrationRequest(message));
			});

			Receive<RequestMessage<HoodStateChangedMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});
			Receive<RequestMessage<ShutdownMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});
			Receive<RequestMessage<DataChangedMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});
			Receive<RequestMessage<DeleteDocumentsMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});
			Receive<RequestMessage<TemperatureChangedMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});
			Receive<RequestMessage<GenericFrontendPushMessage>>(message =>
			{
				if (message.Model.Parameters.DisplayType == MessageDisplayType.Toast)
				{
					ForwardToWebsocketActor(message);
				}
				else
				{
					context.LastMessageConfirmed.Add(new ManualResetEventSlim());

					var queue = GetChild<GenericPushMessageQueueActor>(GenericPushMessageQueueActorName);
					queue.Forward(message);
				}
			});
			Receive<RequestMessage<ConfirmGenericPushMessage>>(message =>
			{
				context.LastMessageConfirmed[0].Set();
			});


			Receive<RequestMessage<NfcPairingMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<NotifyUserLogedinMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<SpinnerMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<NfcPairingResultNotificationMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<OutboundControlledAndMoreInfoMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<DeviceInitializingMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<GxpModeMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<GxpLoginMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<GxpLogoutMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<OutboundDocumentInfoMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<OutboundDataMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<InjectorStatusChangedMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<WhichHubsAreConnectedMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<InjectorConfigChangedMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RepeatLastConfirmationMessage>(message =>
			{
				ForwardToWebsocketActor(message);
			});

		    Receive<RequestMessage<SlideLoadedMessage>>(message =>
		    {
		        ForwardToWebsocketActor(message);
		    });

			Receive<RequestMessage<FrontNfcSlideMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<FrontNfcFilterCubeMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<FilterConfigurationChangedMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<UsbDrivePluggedOutMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});

			Receive<RequestMessage<DocumentsForImportMessage>>(message =>
			{
				ForwardToWebsocketActor(message);
			});
		}

		private void ForwardToWebsocketActor<T>(T message)
		{
			var websocketActor = GetChild<WebsocketCommunicationActor>(WebsocketCommunicationActorName);
			websocketActor.Forward(message);
		}

		protected override void PreStart()
		{
			GetChild<WebsocketCommunicationActor>(WebsocketCommunicationActorName);
			GetChild<GenericPushMessageQueueActor>(GenericPushMessageQueueActorName);

			base.PreStart();
		}
	}
}
