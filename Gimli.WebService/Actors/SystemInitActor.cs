﻿using Gimli.Shared.Actors.Actors;
using MolDev.Common.Akka.Interfaces;

namespace Gimli.WebService.Actors
{
	public class SystemInitActor : SystemInitActorBase
	{
		public SystemInitActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
		}

		protected override bool InitProxies(IActorService actorService)
		{
			var success = IsProxyConnected<DataServiceProxyActor>(actorService, GetGuaranteedAnswer);

			success = InitInstrumentCoreNetworkProxies(actorService, success) && success;

			return success;
		}
	}
}
