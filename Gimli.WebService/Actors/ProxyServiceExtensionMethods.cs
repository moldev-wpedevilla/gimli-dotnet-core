﻿using System;
using System.Threading.Tasks;
using Gimli.Shared.Actors;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;

namespace Gimli.WebService.Actors
{
	internal static class ProxyServiceExtensionMethods
	{
		internal static async Task<GimliResponse> AskCheckRunning(this IActor proxy, RequestMessageBase message, ILogService logService, int? timeout, Func<GimliActorHost> host, IActorService actorService)
		{
			try
			{
				return await proxy.Ask<GimliResponse>(message, timeout);
			}
			catch (TaskCanceledException e)
			{
				return new GimliResponse(new ErrorInfo {Details = e.ToString()}, ResultStatusEnum.Error, logger2: s => logService.Error(LogCategoryEnum.WebApi, s, string.Empty));
			}
		}
	}
}
