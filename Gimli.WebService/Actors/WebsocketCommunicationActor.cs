﻿using System;
using System.Collections.Generic;
using Akka.Actor;
using Akka.Util.Internal;
using Castle.Core.Internal;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.ExportImport;
using Gimli.Shared.Actors.Messages.FilterMngmnt;
using Gimli.Shared.Actors.Messages.InjectorMaintenance;
using Gimli.Shared.Actors.Messages.Instrument;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using Gimli.Shared.Actors.Messages.Outbound;
using Gimli.WebService.Hubs;
using Gimli.WebService.JsonObjects;
using Gimli.WebService.Messages;
using Gimli.WebService.Properties;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.WebService.Actors
{
	public class WebsocketCommunicationActor : GimliActorBase
	{
		public WebsocketCommunicationActor(IActorService actorService, IClientHubDictionary clientHubDictionary,
			[FromServices] IHubContext<InstrumentHub> instrumentHub,
			[FromServices] IHubContext<DataHub> dataHub,
			[FromServices] IHubContext<DocumentHub> documentHub,
			[FromServices] IHubContext<MessageHub> messageHub,
			[FromServices] IHubContext<NetworkHub> networkHub,
			[FromServices] IHubContext<NFCHub> nfcHub,
			[FromServices] IHubContext<BusyHub> busyHub,
			[FromServices] IHubContext<TemperatureHub> temperatureHub,
			[FromServices] IHubContext<InjectorHub> injectorHub, IActorsystemOverallState systemState,
			IMessageConfirmationContext confirmationContext) : base(actorService, systemState)
		{
			Receive<RequestMessage<ShutdownMessage>>(message =>
			{
				Log.Info("Shutdown message");
				var sender = Sender;
				switch (message.Model.Type)
				{
					case ShutdownMessageType.Denial:
					case ShutdownMessageType.Notification:
						instrumentHub.Clients.All.SendAsync("askShutdown");
						break;
				}

				if (message.Model.WantsAnswer) sender?.Tell(new GimliResponse());
			});

			Receive<RequestMessage<HoodStateChangedMessage>>(message =>
			{
				Log.Info($"Hood state changed message: hood {(message.Model.IsOpen ? "open" : "closed" )}");
				if (message.Model.IsOpen)
				{
					instrumentHub.Clients.All.SendAsync("hoodOpened");
				}
				else
				{ 
					instrumentHub.Clients.All.SendAsync("hoodClosed");
				}
			});

			Receive<RequestMessage<DataChangedMessage>>(message =>
			{
				Log.Debug("Data messge");
				dataHub.Clients.All.SendAsync("dataChanged", message.Model.Info.JsonSerializeToCamelCase());
			});

			Receive<RequestMessage<DeleteDocumentsMessage>>(message =>
			{
				Log.Info("Delete documents: " + string.Join(", ", message.Model.Documents));
				var sender = Sender;
				documentHub.Clients.All.SendAsync("documentsDeleted", message.Model.Documents.JsonSerializeToCamelCase());

				sender?.Tell(new GimliResponse());
			});

			Receive<RequestMessage<DocumentsForImportMessage>>(message =>
			{
				var serialized = message.Model.Documents.JsonSerializeToCamelCase();
				Log.Info("Documents for import: " + serialized);
				DocumentHub.LastImportMessage = serialized;
				documentHub.Clients.All.SendAsync("documentsForImport", DocumentHub.LastImportMessage);
			});

			Receive<RequestMessage<UsbDrivePluggedOutMessage>>(message =>
			{
				Log.Info("USB stick removed." );
				DocumentHub.LastImportMessage = null;
				documentHub.Clients.All.SendAsync("usbStickRemoved");
			});

			Receive<RequestMessage<TemperatureChangedMessage>>(message =>
			{
				Log.Debug(
					$"Temperature message: {message.Model.Settings.Current}, heating on: {message.Model.Settings.IsControllerRunning}");
				TemperatureHub.LastMessage = message.Model.Settings.JsonSerializeToCamelCase();
				temperatureHub.Clients.All.SendAsync("temperatureChanged", TemperatureHub.LastMessage);
			});

			Receive<RequestMessage<GenericFrontendPushMessage>>(message =>
			{
				Log.Info("Generic message to frontend: " + message.Model.Parameters.Message);
				messageHub.Clients.All.SendAsync("displayMessage", message.Model.Parameters.JsonSerializeToCamelCase());
			});

			Receive<RepeatLastConfirmationMessage>(message =>
			{
				if (confirmationContext.RepeatMessage == null) return;
				var serializedMessage = confirmationContext.RepeatMessage.Parameters.JsonSerializeToCamelCase();
				Log.Info("Repeat confirmation message: " + serializedMessage);

				messageHub.Clients.All.SendAsync("displayMessage", serializedMessage);
			});

			Receive<RequestMessage<NfcPairingResultNotificationMessage>>(message =>
			{
				Log.Info("NFC pairing " +
				         (message.Model.WasSuccessful
					         ? "success " + message.Model.Result.User.UserName
					         : "fail " + message.Model.Result.ErrorMessage));
				if (message.Model.WasSuccessful)
				{
					nfcHub.Clients.All.SendAsync("pairingSuccessful", message.Model.Result.User.JsonSerializeToCamelCase());
				}
				else
				{
					nfcHub.Clients.All.SendAsync("pairingUnsuccessful", message.Model.Result.JsonSerializeToCamelCase());
				}
			});

			Receive<RequestMessage<NotifyUserLogedinMessage>>(message =>
			{
				Log.Info($"User login: {message.Model.User.Id} {message.Model.User.UserName}");
				nfcHub.Clients.All.SendAsync("userChanged", message.Model.User.JsonSerializeToCamelCase());
				messageHub.Clients.All.SendAsync("displayMessage",
					new MessageParameters
					{
						Message = string.Format(Resources.ResourceManager.GetString("User_Logged_In"), message.Model.User.UserName),
						DisplayType = MessageDisplayType.Toast,
						MessageType = MessageType.Info,
						MessageCode = MessageStatusCodes.UserLoggedIn
					}.JsonSerializeToCamelCase());
			});

			Receive<RequestMessage<SpinnerMessage>>(message =>
			{
				Log.Info("Spinner " + (message.Model.Active ? "on " + message.Model.Text : "off"));
				if (message.Model.Active)
				{
					busyHub.Clients.All.SendAsync("showLoadingIndicator", message.Model.Text);
				}
				else
				{
					busyHub.Clients.All.SendAsync("hideLoadingIndicator");
				}
			});

			Receive<RequestMessage<DeviceInitializingMessage>>(message =>
			{
				Log.Info("Device initiliazing " + (message.Model.IsInitializing ? "on" : "off"));
				BusyHub.LastIsInitializingMessage = message.Model.IsInitializing;
				busyHub.Clients.All.SendAsync("deviceInit", BusyHub.LastIsInitializingMessage);
			});

			Receive<RequestMessage<GxpLoginMessage>>(message =>
			{
				Log.Info($"GxP login: user - {message.Model.GxpUser.User} readEnabled - {message.Model.GxpUser.ReadEnabled}.");
				networkHub.Clients.All.SendAsync("gxpLogin", message.Model.GxpUser.JsonSerializeToCamelCase());
			});

			Receive<RequestMessage<GxpLogoutMessage>>(message =>
			{
				Log.Info("GxP logout.");
				networkHub.Clients.All.SendAsync("gxpLogout");
			});

			Receive<RequestMessage<OutboundDocumentInfoMessage>>(message =>
			{
				Log.Info(
					$"Outbound document update: name - {message.Model.DocumentUpdateInfo.DocumentName}, plates - {message.Model.DocumentUpdateInfo.ExperimentsAndPlatesString().Join(", ")}.");
				networkHub.Clients.All.SendAsync("documentUpdate",
					new
					{
						Name = message.Model.DocumentUpdateInfo.DocumentName,
						Plates = message.Model.DocumentUpdateInfo.ExperimentsAndPlatesString()
					}.JsonSerializeToCamelCase());
			});

			Receive<RequestMessage<GxpModeMessage>>(message =>
			{
				Log.Info($"GxP mode active: {message.Model.Active}.");

				if (message.Model.Active)
				{
					networkHub.Clients.All.SendAsync("gxpScreen");
				}
				else
				{
					networkHub.Clients.All.SendAsync("standardScreen");
				}
			});

			Receive<RequestMessage<OutboundControlledAndMoreInfoMessage>>(message =>
			{
				Log.Info("Reader outbound connection changed: " + message.Model.JsonSerializeToCamelCase());
				if (message.Model.IsOutboundControlled)
				{
					networkHub.Clients.All.SendAsync("outboundConnect", GetOutboundUpdate(message));
				}
				else
				{
					networkHub.Clients.All.SendAsync("outboundReleased", GetOutboundUpdate(message));
				}
			});

			Receive<RequestMessage<OutboundDataMessage>>(message =>
			{
				Log.Info("Outbound data messge: " + message.Model.Data.JsonSerializeToCamelCase());
				networkHub.Clients.All.SendAsync("dataUpdate",
					new OutboundDataUpdate
					{
						Name = message.Model.Data.DocumentName,
						Experiment = message.Model.Data.ExperimentName,
						Plate = message.Model.Data.PlateName,
						Running = message.Model.Data.Status,
						User = message.Model.Data.User?.User
					}.JsonSerializeToCamelCase());
				//hub.Clients.All.dataUpdate(message.Model.Data.JsonSerializeToCamelCase());
			});

			Receive<RequestMessage<InjectorStatusChangedMessage>>(message =>
			{
				Log.Info("Injector status changed messge: " + message.Model.Injectors.JsonSerializeToCamelCase());
				injectorHub.Clients.All.SendAsync("statusChanged", message.Model.Injectors.JsonSerializeToCamelCase());
			});

			Receive<RequestMessage<InjectorConfigChangedMessage>>(message =>
			{
				InjectorHub.LastConfigMessage = message.Model.InjectorsSetup.JsonSerializeToCamelCase();
				Log.Info("Injector configuration changed messge: " + InjectorHub.LastConfigMessage);
				injectorHub.Clients.All.SendAsync("configurationChanged", InjectorHub.LastConfigMessage);
			});

			Receive<RequestMessage<FilterConfigurationChangedMessage>>(message =>
			{
				NFCHub.LastConfigMessage = message.Model.IsFilterReady;
				Log.Info("Filter configuration changed messge: " + NFCHub.LastConfigMessage);
				nfcHub.Clients.All.SendAsync("configurationChanged", NFCHub.LastConfigMessage);
			});

			Receive<RequestMessage<WhichHubsAreConnectedMessage>>(message =>
			{
				var sender = Sender;
				var clients = new List<string>();
				var hubs = new List<string>();
				foreach (var dictionary in clientHubDictionary.Dictionaries)
				{
					foreach (var key in dictionary.Keys)
					{
						if (!clients.Contains(key))
						{
							clients.Add(key);
						}
						var hub = dictionary[key].GetType().Name;
						if (!hubs.Contains(hub))
						{
							hubs.Add(hub);
						}
					}
				}

				Log.Info("There are {0} clients connected: {1} and {2} hubs connected: {3}.", clients.Count,
					string.Join(", ", clients.ToArray()), hubs.Count, string.Join(", ", hubs.ToArray()));

				sender.Tell(new WhichHubsAreConnectedResponseMessage {Clients = clients, Hubs = hubs});
			});

			Receive<RequestMessage<SlideLoadedMessage>>(message =>
			{
				if (message.Model.Slide == null)
				{
					Log.Info("No slide.");
					return;
				}

				var transformed = message.Model.Slide.TransformToSlideDataMessage(message.Model.SlideSel, s => Log.Error(s));
				var frontendMessage = transformed.JsonSerializeToCamelCase();
				Log.Info($"Slide loaded: {frontendMessage}");
				nfcHub.Clients.All.SendAsync("slotInsertRecognized", frontendMessage);

				if (transformed.Error.IsNullOrEmpty()) return;

				actorService.Get<WebServiceRoutingActor>().Tell(
					MessageExtensions.CreateRequestMessage(new GenericFrontendPushMessage
					{
						Parameters = new MessageParameters
						{
							Message = transformed.Error,
							DisplayType = MessageDisplayType.DialogOk,
							MessageCode = MessageStatusCodes.FiltersMngmntError,
							MessageType = MessageType.Error
						}
					}));
			});

			Receive<RequestMessage<FrontNfcSlideMessage>>(message =>
			{
				var transformed = message.Model.Slide.TransformToSlideDataMessage(null, s => Log.Error(s)).JsonSerializeToCamelCase();
				Log.Info($"Slide held to front nfc: {transformed}");
				nfcHub.Clients.All.SendAsync("filterSlideRecognized", transformed);
			});

			Receive<RequestMessage<FrontNfcFilterCubeMessage>>(message =>
			{
				var singleCube = message.Model.Cube.TransformFilterCubeData(s => Log.Error(s)).JsonSerializeToCamelCase();
				Log.Info($"Filter cube held to front nfc: {singleCube}");
				nfcHub.Clients.All.SendAsync("filterCubeRecognized", singleCube);
			});
		}

		private static string GetOutboundUpdate(RequestMessage<OutboundControlledAndMoreInfoMessage> message)
		{
			return new OutboundConnectionUpdate
			{
				Computer = message.Model.ClientHostname,
				User = message.Model.Username,
				Type = message.Model.GxpMode ? "gxp" : "standard"
			}.JsonSerializeToCamelCase();
		}
	}
}
