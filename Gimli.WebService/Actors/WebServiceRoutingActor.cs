﻿using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.ExportImport;
using Gimli.Shared.Actors.Messages.FilterMngmnt;
using Gimli.Shared.Actors.Messages.InjectorMaintenance;
using Gimli.Shared.Actors.Messages.Instrument;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using Gimli.Shared.Actors.Messages.Outbound;
using Gimli.WebService.Messages;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Messages;

namespace Gimli.WebService.Actors
{
	public class WebServiceRoutingActor : ToplevelRoutingActorBase
	{
		private const string WebsocketActorName = nameof(WebsocketCommunicationRoutingActor);
		private const string SystemInitActorName = nameof(SystemInitActor);

		public WebServiceRoutingActor(IActorService actorService, IActorsystemOverallState systemState) : base(actorService, systemState)
		{
			Receive<RequestMessage<HoodStateChangedMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});
			Receive<RequestMessage<ShutdownMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});
			Receive<RequestMessage<DataChangedMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});
			Receive<RequestMessage<DeleteDocumentsMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<TemperatureChangedMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<RemoteRegistrationMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<GenericFrontendPushMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<ConfirmGenericPushMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<NfcPairingMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<NotifyUserLogedinMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<SpinnerMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<NfcPairingResultNotificationMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<OutboundControlledAndMoreInfoMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<DeviceInitializingMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<GxpModeMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<GxpLoginMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<GxpLogoutMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<OutboundDocumentInfoMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<OutboundDataMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<InjectorStatusChangedMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<WhichHubsAreConnectedMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<InjectorConfigChangedMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RepeatLastConfirmationMessage>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

		    Receive<RequestMessage<SlideLoadedMessage>>(message =>
		    {
                ForwardToWebsocketRoutingActor(message);
		    });

			Receive<RequestMessage<FrontNfcSlideMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<FrontNfcFilterCubeMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<FilterConfigurationChangedMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<UsbDrivePluggedOutMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});

			Receive<RequestMessage<DocumentsForImportMessage>>(message =>
			{
				ForwardToWebsocketRoutingActor(message);
			});
		}

        private void ForwardToWebsocketRoutingActor<T>(T message)
		{
			var websocketActor = GetChild<WebsocketCommunicationRoutingActor>(WebsocketActorName);
			websocketActor.Forward(message);
		}

		protected override IActor GetSysInitActor()
		{
			return GetChild<SystemInitActor>(SystemInitActorName);
		}

		protected override void PreStart()
		{
			// init
			GetChild<WebsocketCommunicationRoutingActor>(WebsocketActorName);

			base.PreStart();
		}
	}
}
