﻿using System.Collections.Generic;
using System.Threading;
using Gimli.Shared.Actors.Messages;

namespace Gimli.WebService.Actors
{
	public interface IMessageConfirmationContext
	{
		List<ManualResetEventSlim> LastMessageConfirmed { get; set; }
		GenericFrontendPushMessage RepeatMessage { get; set; }

		void WaitForNext();
		void EmptyQueue();
	}
}
