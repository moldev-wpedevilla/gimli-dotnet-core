﻿using System.Collections.Generic;
using System.Threading;
using Gimli.Shared.Actors.Messages;

namespace Gimli.WebService.Actors
{
	public class MessageConfirmationContext : IMessageConfirmationContext
	{
		private CancellationTokenSource mTokenSource;

		public List<ManualResetEventSlim> LastMessageConfirmed { get; set; } = new List<ManualResetEventSlim>();
		public GenericFrontendPushMessage RepeatMessage { get; set; }

		public void WaitForNext()
		{
			if (LastMessageConfirmed.Count < 1) return;
			mTokenSource = new CancellationTokenSource();
			LastMessageConfirmed[0].Wait(mTokenSource.Token);
			if (LastMessageConfirmed.Count > 0)
			{
				RepeatMessage = null;
				LastMessageConfirmed.RemoveAt(0);
			}
		}

		public void EmptyQueue()
		{
			LastMessageConfirmed.Clear();
			mTokenSource?.Cancel();
			RepeatMessage = null;
		}
	}
}
