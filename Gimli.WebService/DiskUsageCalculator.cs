﻿using System;
using Gimli.JsonObjects.Utils;

namespace Gimli.WebService
{
	internal class DiskUsageCalculator
	{
		public DiskUsageCalculator()
		{
			const string drive = "C:";
			SpaceFree = (ulong)FolderFileHelper.GetFreeSpace(drive);
			SpaceTotal = (ulong)FolderFileHelper.GetTotalSpace(drive);
			OccupiedSpace = Math.Round(((SpaceTotal - SpaceFree) / (double)SpaceTotal) * 100, 1);
		}

		internal ulong SpaceFree { get; }
		internal ulong SpaceTotal { get; }
		internal double OccupiedSpace { get; }
	}
}
