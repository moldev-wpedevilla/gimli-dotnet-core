﻿using Gimli.JsonObjects;
using Gimli.WebService.Hubs;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Hosting;
using System.IO;

namespace Gimli.WebService
{
	class Program
	{
		public static void Main(string[] args)
		{
			var host = CreateHostBuilder(args).Build();
			host.Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseStartup<Startup>();
					webBuilder.UseUrls("http://localhost:8002");
					webBuilder.ConfigureKestrel((context, options) =>
					{
						// TODO someone: check this
						options.AllowSynchronousIO = true;
					});
				});
	}
}
