﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Gimli.JsonObjects;
using Nancy;
using Nancy.Responses;

namespace Gimli.WebService
{
	public static class FileAccessHelpers
	{
		public static Task<Response> GetFile(string filePath, string name, bool streamVideo, IResponseFormatter rf, Request req)
		{
			return Task.Run(() =>
			{
				var fileName = Directory.GetFiles(filePath).FirstOrDefault(f => f.Contains(filePath + name));
				if (fileName == null || !Path.GetFullPath(fileName).Contains(filePath))
				{
					return HttpStatusCode.NotFound;
				}

				var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
				var contentType = MimeTypes.GetMimeType(fileName);
				var response = !streamVideo
					? new StreamResponse(() => stream, contentType)
					: rf.FromPartialStream(req, stream, contentType);
				
				return response.AsAttachment(name.Replace(' ', '_') + Path.GetExtension(fileName), contentType);
			});
		}

		public static Response FromPartialStream(this IResponseFormatter f, Request req, Stream stream, string contentType)
		{
			// Store the len
			var len = stream.Length;
			// Create the response now
			var res = f.FromStream(stream, contentType).
				//WithHeader("connection", "keep-alive").
				WithHeader("accept-ranges", "bytes");
			// Use the partial status code
			res.StatusCode = HttpStatusCode.PartialContent;
			long startI = 0;
			foreach (var s in req.Headers["Range"])
			{
				var start = s.Split('=')[1];
				var m = Regex.Match(start, @"(\d+)-(\d+)?");
				start = m.Groups[1].Value;
				var end = len - 1;
				if (!string.IsNullOrWhiteSpace(m.Groups[2].Value))
				{
					end = Convert.ToInt64(m.Groups[2].Value);
				}

				startI = Convert.ToInt64(start);
				var length = len - startI;
				res.WithHeader("content-range", "bytes " + start + "-" + end + "/" + len);
				res.WithHeader("content-length", length.ToString(CultureInfo.InvariantCulture));
			}
			stream.Seek(startI, SeekOrigin.Begin);
			return res;
		}

		public static Task<string> GetFileList(string filePath)
		{
			return Task.Run(() =>
			{
				if (!Directory.Exists(filePath))
				{
					return new string[0].JsonSerializeToCamelCase();
				}

				var files = Directory.GetFiles(filePath).Select(Path.GetFileNameWithoutExtension);
				return files.JsonSerializeToCamelCase();
			});
		}
	}
}
