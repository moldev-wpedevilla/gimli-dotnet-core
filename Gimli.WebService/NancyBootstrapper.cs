﻿using System;
using System.IO;
using Castle.Windsor;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Conventions;
using Nancy.TinyIoc;
using Gimli.Shared.Actors;

namespace Gimli.WebService
{
	public class NancyBootstrapper : DefaultNancyBootstrapper
	{
		private readonly TinyIoCContainer mContainer;
		private readonly string mResourcesDir;

		public NancyBootstrapper(IWindsorContainer container, string resourcesDir)
		{
			mResourcesDir = resourcesDir;

			mContainer = new TinyIoCContainer();
			mContainer.Register<Func<IActorService>>(() => container.Resolve<IActorService>());
			mContainer.Register<Func<ILogService>>(() => container.Resolve<ILogService>());
			mContainer.Register<Func<Func<GimliActorHost>>>(() => container.Resolve<Func<GimliActorHost>>());
			mContainer.Register<Func<Func<bool>>>(() => container.Resolve<Func<bool>>());
		}

		protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
		{
			var additionalSearchPath = $"{mResourcesDir}/";
			Conventions.ViewLocationConventions.Add((viewName, model, context) => string.Concat(additionalSearchPath, viewName));
			Conventions.StaticContentsConventions.AddDirectory(additionalSearchPath);
			//Conventions.CultureConventions.Add( => string.Concat(additionalSearchPath, rootPath));
			var logger = container.Resolve<Func<ILogService>>()();

			pipelines.BeforeRequest += (ctx) =>
			{
				var urlString = ctx.Request.Url.ToString();
				if (urlString.Contains("range") || urlString.Contains("validate"))
				{
					logger.Debug(LogCategoryEnum.WebApi, urlString);
				}
				else
				{
					logger.Info(LogCategoryEnum.WebApi, urlString);
				}

				return null;
			};

			pipelines.AfterRequest += (ctx) =>
			{
				if (ctx.Response.StatusCode != HttpStatusCode.OK)
				{
					logger.Error(LogCategoryEnum.WebApi, ctx.Trace.TraceLog.ToString());
				}


				if (!(ctx.Response is Nancy.Responses.StreamResponse))
				{
					try
					{
						using (var stream = new MemoryStream())
						{
							ctx.Response.Contents.Invoke(stream);

							stream.Position = 0;
							using (var reader = new StreamReader(stream))
							{
								var content = reader.ReadToEnd();

								logger.Debug(LogCategoryEnum.WebApi,
									$"Response to {ctx.Request.Url}: {ctx.Response.StatusCode} {content}",
									string.Empty);
							}
						}
					}
					catch (Exception)
					{

					}

				}
			};

			pipelines.OnError += (ctx, ex) =>
			{
				logger.Error(LogCategoryEnum.WebApi, ex.ToString());
				return null;
			};

			base.ApplicationStartup(container, pipelines);
		}

		protected override void ConfigureConventions(NancyConventions nancyConventions)
		{
			nancyConventions.ViewLocationConventions.Add((viewName, model, context) => string.Concat($"{mResourcesDir}/", viewName));
			nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("", mResourcesDir));
			base.ConfigureConventions(nancyConventions);
		}

		protected override TinyIoCContainer GetApplicationContainer()
		{
			return mContainer ?? base.GetApplicationContainer();
		}
	}
}
