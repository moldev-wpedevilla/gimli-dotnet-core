﻿using System;
using Gimli.Shared.Actors;
using MolDev.Common.Messages;
using MolDev.Common.Messages.Errors;
using Nancy;
using Nancy.Responses;

namespace Gimli.WebService
{
	internal class ActorMessageHelpers
	{
		internal static Response CheckResponseAndConvert<T>(ResponseMessageBase response, Func<T, IResponseFormatter, Response> answerforFrontendCreater, IResponseFormatter responseFormatter) where T : GimliResponse
		{
			var typed = CheckResponseAndConvert<T>(response);
			return typed != null 
				? answerforFrontendCreater(typed, responseFormatter) 
				: GetErrorResponse(response);
		}

		internal static T CheckResponseAndConvert<T>(ResponseMessageBase response) where T : GimliResponse
		{
			var typed = response as T;
			if (typed != null)
			{
				return typed;
			}

			if (response.Error == null || (response.Error.Details == null && response.Error.Message == null))
			{
				throw new ActorException($"Unexpected response: {response}.");
			}

			return null;
		}

		internal static Response GetOkOrErrorResponse(GimliResponse response)
		{
			return response.ResponseStatus == ResultStatusEnum.Success 
				? HttpStatusCode.OK 
				: GetErrorResponse(response);
		}

		internal static Response GetErrorResponse(ResponseMessageBase response, HttpStatusCode? errorCode = null)
		{
			var errorCodeInternal = GetErrorCode(response, errorCode);
			var errorMessage = response.Error?.Details ?? response.Error?.Message;
			return new TextResponse(errorCodeInternal, errorMessage);
		}

		private static HttpStatusCode GetErrorCode(ResponseMessageBase response, HttpStatusCode? errorCode)
		{
			if (errorCode.HasValue)
			{
				return errorCode.Value;
			}

			return response.Error?.Details != null
				? HttpStatusCode.InternalServerError
				: HttpStatusCode.Conflict;
		}
	}
}
