﻿using System;
using System.Threading.Tasks;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.WebService.Actors;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;
using Nancy;

namespace Gimli.WebService
{
	internal class DocumentActorMessageHelpers : ActorMessageHelpers
	{
		internal static async Task<Response> GetSingleDocument(IActorService actorService, Guid id, ProtocolResultMessageType type, IResponseFormatter responseFormatter, ILogService logService, Func<GimliActorHost> host,
			PlateWellBase wellGroup = null)
		{
			return await GetDocuments<ProtocolResultResponseMessage>(actorService, id, type, 1, (r, rf) => rf.AsText(r.JsonFormattedDocument), responseFormatter, logService, host, null, wellGroup);
		}

		internal static async Task<Response> GetDocumentList(IActorService actorService, ProtocolResultMessageType type, IResponseFormatter responseFormatter, ILogService logService, Func<GimliActorHost> host, string category, int numberOfItemsRequested = 0)
		{
			return await GetDocuments<ProtocolResultResponseMessage>(actorService, null, type, numberOfItemsRequested, (r, rf) => rf.AsText(r.JsonFormattedDocument), responseFormatter, logService, host, category);
		}


		/// <summary>
		/// Uses actor to request a protocol or result or a list thereof.
		/// </summary>
		/// <param name="actorService"></param>
		/// <param name="id"></param>
		/// <param name="type"></param>
		/// <param name="responseFormatter"></param>
		/// <param name="logService"></param>
		/// <param name="numberOfItemsRequested"></param>
		/// <param name="answerforFrontendCreater"></param>
		/// <param name="wellGroup"></param>
		/// <returns></returns>
		private static async Task<Response> GetDocuments<T>(
			IActorService actorService, 
			Guid? id, 
			ProtocolResultMessageType type,
			int numberOfItemsRequested,
			Func<T, IResponseFormatter, Response> answerforFrontendCreater, 
			IResponseFormatter responseFormatter, 
			ILogService logService, 
			Func<GimliActorHost> host,
			string category,
			PlateWellBase wellGroup = null) where T : GimliResponse
		    {
		    var result = await actorService.Get<DataServiceProxyActor>().AskCheckRunning(MessageExtensions.CreateRequestMessage(new GetCurrentUserMessage()), logService, 5000, host, actorService);

			var message = new ProtocolResultMessage
			{
                UserFull = (result as GetCurrentUserResponseMessage)?.UserFull,
				Type = type,
				WellGroup = wellGroup,
			    NumberOfItemsRequested = numberOfItemsRequested,
				Category = category
			};

			if (id != null)
			{
			    message.Id = id.Value;
			}

			var documentMessage = MessageExtensions.CreateRequestMessage(message);
			var response = await actorService.Get<DataServiceProxyActor>().AskCheckRunning(documentMessage, logService, 25000, host, actorService);

            return CheckResponseAndConvert(response, answerforFrontendCreater, responseFormatter);
		}
	}
}
