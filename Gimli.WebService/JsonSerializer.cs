﻿using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Nancy;
using Nancy.Responses.Negotiation;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Gimli.WebService
{
	public class JsonNetSerializer : ISerializer
	{
		private readonly JsonSerializer mSerializer;

		public JsonNetSerializer()
		{
			var settings = new JsonSerializerSettings
			{
				ContractResolver = new CamelCasePropertyNamesContractResolver()
			};

			mSerializer = JsonSerializer.Create(settings);
		}

		public bool CanSerialize(MediaRange mediaRange)
		{
			return mediaRange == "application/json";
		}

		public void Serialize<TModel>(MediaRange mediaRange, TModel model, Stream outputStream)
		{
			using (var writer = new JsonTextWriter(new StreamWriter(outputStream)))
			{
				mSerializer.Serialize(writer, model);
				writer.Flush();
			}
		}

		public IEnumerable<string> Extensions { get; }
	}
}
