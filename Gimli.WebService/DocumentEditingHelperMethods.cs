﻿using System;
using System.Threading.Tasks;
using Gimli.JsonObjects;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.WebService.Actors;
using Gimli.WebService.JsonObjects;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;
using MolDev.Common.Messages.Errors;
using Nancy;

namespace Gimli.WebService
{
	internal class DocumentEditingHelperMethods : ActorMessageHelpers
	{

		internal static async Task<Response> RenamePlate(IActorService actorService, CopyDeleteRenamePlateInfo renamePlateInfo,
			IResponseFormatter responseFormatter, ILogService logService, Func<GimliActorHost> host)
		{
			var request = MessageExtensions.CreateRequestMessage(
				new RenamePlateMessage
				{
					DocumentId = renamePlateInfo.DocumentId,
					PlateId = renamePlateInfo.PlateId,
					Name = renamePlateInfo.Name
				});

			var response = await actorService.Get<DataServiceProxyActor>().AskCheckRunning(request, logService, 50000, host, actorService);

			return response.ResponseStatus == ResultStatusEnum.Success
				? renamePlateInfo.JsonSerializeToCamelCase()
				: GetErrorResponse(response);
		}

		internal static async Task<Response> DeletePlate(IActorService actorService, CopyDeleteRenamePlateInfo deletePlateInfo,
			IResponseFormatter responseFormatter, ILogService logService, Func<GimliActorHost> host)
		{
			var request = MessageExtensions.CreateRequestMessage(
				new DeletePlateMessage
				{
					DocumentId = deletePlateInfo.DocumentId,
					PlateId = deletePlateInfo.PlateId
				});

			var response = await actorService.Get<DataServiceProxyActor>().AskCheckRunning(request, logService, 50000, host, actorService);

			return response.ResponseStatus == ResultStatusEnum.Success 
				? new DeleteResponse {PlateId = deletePlateInfo.PlateId}.JsonSerializeToCamelCase() 
				: GetErrorResponse(response, HttpStatusCode.Conflict);
		}

		internal static async Task<Response> CopyPlate(IActorService actorService, CopyDeleteRenamePlateInfo copyPlateInfo,
			IResponseFormatter responseFormatter, ILogService logService, Func<GimliActorHost> host)
		{
			var request = MessageExtensions.CreateRequestMessage(
				new CopyPlateMessage
				{
					DocumentId = copyPlateInfo.DocumentId,
					PlateId = copyPlateInfo.PlateId,
					Name = copyPlateInfo.Name
				});

			var response = await actorService.Get<DataServiceProxyActor>().AskCheckRunning(request, logService, 50000, host, actorService);

			return CheckResponseAndConvert<CopyPlateResponseMessage>(response,
				(message, formatter) =>
				{
					var messageForUi = new CopyDeleteRenamePlateInfo {DocumentId = message.DocumentId, PlateId = message.PlateId, Name = message.Name};
					return messageForUi.JsonSerializeToCamelCase();
				}, responseFormatter);
		}

		internal static async Task<Response> CreateProtocol(IActorService actorService, SaveAsInfo saveAsInfo, IResponseFormatter responseFormatter, ILogService logService, Func<GimliActorHost> host)
		{
			var createProtocolResponse = await actorService.Get<DataServiceProxyActor>()
				.AskCheckRunning(MessageExtensions.CreateRequestMessage(new CreateProtocolFromDocumentMessage
				{
					DocumentId = saveAsInfo.DocumentId,
					Name = saveAsInfo.Name
				}), logService, 30000, host, actorService);

			return CheckResponseAndConvert<CreateProtocolResponseMessage>(createProtocolResponse,
				(message, formatter) => new ResponseWithDocumentId {DocumentId =  message.DocumentId}.JsonSerializeToCamelCase(), responseFormatter);
		}

		internal static async Task<Response> ValidateAndSave(IActorService actorService, IResponseFormatter responseFormatter,
			ILogService logService, AllSettings settings, Func<GimliActorHost> host)
		{
			var validationResult = await Validate(actorService, settings, responseFormatter);
			if (validationResult.StatusCode != HttpStatusCode.OK)
			{
				return validationResult;
			}

			var response = await Save(actorService, logService, settings, host);
			return GetOkOrErrorResponse(response);
		}

		internal static async Task<GimliResponse> Save(IActorService actorService,
			ILogService logService, AllSettings settings, Func<GimliActorHost> host)
		{
			var request = MessageExtensions.CreateRequestMessage(new UpdateDocumentWithSettingsMessage {Settings = settings });
			return await actorService.Get<DataServiceProxyActor>().AskCheckRunning(request, logService, 35000, host, actorService);
		}

		internal static async Task<Response> Validate(IActorService actorService, AllSettings message, IResponseFormatter responseFormatter)
		{
			var documentMessage = MessageExtensions.CreateRequestMessage(new ValidateSettingsMessage { Settings = message });
			var response =
				await actorService.Get<InstrumentServiceProxyActor>().Ask<GimliResponse>(documentMessage);

			return CheckResponseAndConvert<ValidateSettingsResponseMessage>(response, (r, rf) =>
			{
				foreach (var snapshot in r.Settings.SnapshotSettings)
				{
					snapshot.Value.ExternalPlateSummaryAccessor = () => r.Settings.PlateSettings.GetSummary;
				}
				
				return rf.AsText(r.Settings.JsonSerializeToCamelCase());
			}, responseFormatter);
		}
	}
}
