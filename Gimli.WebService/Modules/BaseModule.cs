﻿using System.Threading.Tasks;
using Nancy;

namespace Gimli.WebService.Modules
{
	public class BaseModule :NancyModule
	{
		public BaseModule(): base("/")
		{
			Get(string.Empty, async (ctx, ct) => await Task.Run(()=> View["Index.html"]));
		}
	}
}
