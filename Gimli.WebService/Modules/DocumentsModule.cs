﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Gimli.JsonObjects;
using Gimli.JsonObjects.Settings;
using Gimli.Shared.Actors;
using Gimli.Shared.Actors.Actors;
using Gimli.Shared.Actors.Messages;
using Gimli.Shared.Actors.Messages.ExportImport;
using Gimli.Shared.Actors.Messages.OperatingSystem;
using Gimli.WebService.Actors;
using Gimli.WebService.JsonObjects;
using MolDev.Common.Akka.Interfaces;
using MolDev.Common.Logger;
using MolDev.Common.Messages.Errors;
using Nancy;
using Newtonsoft.Json;

namespace Gimli.WebService.Modules
{
	// TODO someone: also migrate rest of the routes

	public class DocumentsModule : NancyModule
	{
		private readonly ILogService mLogService;
		private readonly Func<GimliActorHost> mHost;

		public DocumentsModule(Func<IActorService> actorService, Func<ILogService> logService, Func<Func<GimliActorHost>> host, Func<Func<bool>> connectToSimulator) : base("/documents")
		{
			mLogService = logService();
			mHost = host();
			var resolvedActorservice = actorService();
			var resolvedConnectToSimulator = connectToSimulator();

			Post("/delete/", (ctx, ct) => ExecuteDocumentListAction(resolvedActorservice, mLogService, DeleteFilesAsync));
			Post("/deleteOnUsb/", (ctx, ct) => ExecuteDocumentListAction(resolvedActorservice, mLogService, DeleteUsbFilesAsync));

			Post("/synchronize/", (ctx, ct) => ExecuteDocumentListAction(resolvedActorservice, mLogService, (actorSvc, logSvc, guids) => ExportFilesAsync(actorSvc, logSvc, guids, false)));
			Post("/exportProtocolsToUsb/", (ctx, ct) => ExecuteDocumentListAction(resolvedActorservice, mLogService, (actorSvc, logSvc, guids) => ExportFilesAsync(actorSvc, logSvc, guids, true)));
			Post("/import/", (ctx, ct) => ExecuteDocumentListAction(resolvedActorservice, mLogService, ImportFilesAsync));

			Get(string.Empty, (ctx, ct) => GetDocuments(resolvedActorservice, mLogService, mHost));

			Get("/categories", (ctx, ct) => GetCategories(resolvedActorservice, mLogService, mHost));

			Put("quickread/{slot}", (ctx, ct) => SetQuickreadSlot(resolvedActorservice, mLogService, ctx));

			Delete("quickread/{slot}", (ctx, ct) => UpdateQuickreadSlot(resolvedActorservice, mLogService, null, ctx.slot));

			Post(string.Empty, (ctx, ct) => WriteDocument(resolvedActorservice, mLogService, mHost));

			Post("/{documentId}", (ctx, ct) => RenameDocument(resolvedActorservice, mLogService, mHost, ctx));

			Post("/{documentId}/plates/{plateId}/settings/read", (ctx, ct) => UpdateReadModeType(resolvedActorservice, mLogService, mHost));

			Post("/{documentId}/read", (ctx, ct) =>
			{
				var stringBody = Request.Body.ReadAsString(b => mLogService.Debug(LogCategoryEnum.WebApi, b, string.Empty));
				var startReadInfo = JsonConvert.DeserializeObject<StartReadInfo>(stringBody);
				startReadInfo.DocumentID = Guid.Parse(ctx.documentId);
				return StartRead(resolvedActorservice, startReadInfo, Response, mLogService);
			});

			Put("/{documentId}/plates/{plateId}/settings", (ctx, ct) =>
			{
				var settings = GetAllSettingsFromBody<AllSettings>(ctx);
				return DocumentEditingHelperMethods.ValidateAndSave(resolvedActorservice, Response, mLogService, settings, mHost);
			});

			Put("/{documentId}/plates/{plateId}/reductionSettings", (ctx, ct) => SetReductionSettings(resolvedActorservice, mLogService, mHost, ctx));

			Post("/{documentId}/plates/{plateId}/validateReduction", (ctx, ct) =>
			{
				var settings = GetDataReductionSettingsFromBody();
				return ValidateReductionSettings(resolvedActorservice, settings);
			});

			Post("/updateReductionParameters", (ctx, ct) => UpdateReductionParams(resolvedActorservice));

			Post("/{documentId}/plates/{plateId}/settings/validate", (ctx, ct) =>
			{
				var settings = GetAllSettingsFromBody<AllSettings>(ctx);
				return DocumentEditingHelperMethods.Validate(resolvedActorservice, settings, Response);
			});

			Post("/{documentId}/plates/{plateId}/settings/plate", (ctx, ct) => SetPlate(resolvedActorservice, ctx));

			RegisterDummyPaths(resolvedActorservice, resolvedConnectToSimulator);
		}

		private async Task<object> UpdateReadModeType(IActorService actorService, ILogService logService, Func<GimliActorHost> host)
		{
			var settingsString = Request.Body.ReadAsString(b => logService.Debug(LogCategoryEnum.WebApi, b, string.Empty));
			var settings = JsonConvert.DeserializeObject<MeasurementSettings>(settingsString);

			string mode = Request.Query.mode;
			string type = Request.Query.type;
			Mode castedMode;
			var parseSuccess = Enum.TryParse(UppercaseFirst(mode), out castedMode);
			CheckParseSuccess(parseSuccess, mode);
			MeasurementType castedType;
			parseSuccess = Enum.TryParse(UppercaseFirst(type), out castedType);
			CheckParseSuccess(parseSuccess, type);
			var response = await
				actorService.Get<InstrumentServiceProxyActor>()
					.AskCheckRunning(
						MessageExtensions.CreateRequestMessage(new UpdateSettingsWithReadModeTypeMessage
						{
							SnapshotSettings = settings,
							DesiredReadType = castedType,
							DesiredReadMode = castedMode
						}), logService, null, host, actorService);

			return ActorMessageHelpers.CheckResponseAndConvert<UpdateSettingsWithReadModeTypeResponseMessage>(response,
				(message, formatter) => message.SnapshotSettings.JsonSerializeToCamelCase(), Response);
		}

		private async Task<object> SetPlate(IActorService actorService, object ctx)
		{
			var settings = GetAllSettingsFromBody<AllSettings>(ctx);
			var settingsMessage =
				MessageExtensions.CreateRequestMessage(new ValidatePlateSettingsMessage
				{
					Settings = settings
				});
			var response =
				await actorService.Get<InstrumentServiceProxyActor>().Ask<GimliResponse>(settingsMessage);

			return ActorMessageHelpers.CheckResponseAndConvert<ValidateSettingsResponseMessage>(response, (r, rf) => rf.AsText(r.Settings.JsonSerializeToCamelCase()), Response);
		}

		private async Task<object> UpdateReductionParams(IActorService actorService)
		{
			var settings = GetDataReductionSettingsFromBody();
			var validationMessage =
				MessageExtensions.CreateRequestMessage(new GetReductionParametersMessage { Settings = settings });
			var response = await actorService.Get<InstrumentServiceProxyActor>().Ask<GimliResponse>(validationMessage);

			return ActorMessageHelpers.CheckResponseAndConvert<GetReductionParametersResponseMessage>(response,
				(r, rf) => rf.AsText(r.Settings.JsonSerializeToCamelCase()), Response);
		}

		private async Task<Response> SetReductionSettings(IActorService actorService, ILogService logService, Func<GimliActorHost> host, object ctx)
		{
			var settings = GetAllSettingsFromBody<SnapshotReductionSettings>(ctx);
			foreach (var snapshotId in settings.ReductionSettings.Keys)
			{
				var validationResult = await ValidateReductionSettings(actorService, settings.ReductionSettings[snapshotId]);
				if (validationResult.StatusCode != HttpStatusCode.OK)
				{
					return validationResult;
				}
			}
			var request = MessageExtensions.CreateRequestMessage(new UpdateDocumentWithReductionSettingsMessage { Settings = settings });
			var result = await actorService.Get<DataServiceProxyActor>().AskCheckRunning(request, logService, 35000, host, actorService);
			return ActorMessageHelpers.GetOkOrErrorResponse(result);
		}

		private async Task<object> RenameDocument(IActorService actorService, ILogService logService, Func<GimliActorHost> host, dynamic ctx)
		{
			Guid documentId = Guid.Parse(ctx.documentId);
			string name = Request.Query.name;

			var response = await actorService.Get<DataServiceProxyActor>()
				.AskCheckRunning(MessageExtensions.CreateRequestMessage(new RenameDocumentMessage
				{
					Name = name,
					DocumentId = documentId
				}), logService, null, host, actorService);

			return response.ResponseStatus != ResultStatusEnum.Success
				? ActorMessageHelpers.GetErrorResponse(response)
				: new RenameDocumentInfo { DocumentId = documentId, Name = name }.JsonSerializeToCamelCase();
		}

		private async Task<object> WriteDocument(IActorService actorService, ILogService logService, Func<GimliActorHost> host)
		{
			//string documentType = Request.Query.type;
			string name = Request.Query.name;
			var mode = name == null ? Enum.Parse(typeof(Mode), Request.Query.readMode.ToString()) : Mode.Abs;

			var response = await actorService.Get<DataServiceProxyActor>()
				.AskCheckRunning(MessageExtensions.CreateRequestMessage(new CreateNewProtocolMessage
				{
					Name = name,
					ReadMode = mode
				}), logService, 15000, host, actorService);

			return ActorMessageHelpers.CheckResponseAndConvert<CreateProtocolResponseMessage>(response,
				(message, formatter) => new ResponseWithDocumentId { DocumentId = message.DocumentId }.JsonSerializeToCamelCase(), Response);
		}

		private async Task<object> SetQuickreadSlot(IActorService actorService, ILogService logService, dynamic ctx)
		{
			var slotInfo = JsonConvert.DeserializeObject<QuickReadSlot>(Request.Body.ReadAsString(b => logService.Debug(LogCategoryEnum.WebApi, b, string.Empty)));
			return await UpdateQuickreadSlot(actorService, logService, slotInfo, ctx.slot);
		}

		private async Task<object> GetCategories(IActorService actorService, ILogService logService, Func<GimliActorHost> host)
		{
			string listType = Request.Query.type;

			var innerListType = ProtocolResultMessageType.None;
			switch (listType)
			{
				case "my_protocol":
					innerListType = ProtocolResultMessageType.GetCategoriesListMy;
					break;
				case "std_protocol":
					innerListType = ProtocolResultMessageType.GetCategoriesListStandard;
					break;
			}

			return await DocumentActorMessageHelpers.GetDocumentList(actorService, innerListType, Response, logService, host, null);
		}

		private async Task<object> GetDocuments(IActorService actorService, ILogService logService, Func<GimliActorHost> host)
		{
			string listType = Request.Query.type;

			var innerListType = ProtocolResultMessageType.None;
			switch (listType)
			{
				case "":
					// TODO
					break;
				case "result":
					innerListType = ProtocolResultMessageType.GetResultList;
					break;
				case "my_protocol":
					innerListType = ProtocolResultMessageType.GetProtocolList;
					break;
				case "recent_document":
					innerListType = ProtocolResultMessageType.GetLastUsedProtocolList;
					break;
				case "std_protocol":
					innerListType = ProtocolResultMessageType.GetStandardProtocolList;
					break;
				case "quickread":
					innerListType = ProtocolResultMessageType.GetQuickProtocol;
					break;
				case "usb_protocol":
					innerListType = ProtocolResultMessageType.GetProtocolsForImportList;
					break;
				case "usb_result":
					innerListType = ProtocolResultMessageType.GetResultsForImportList;
					break;
			}

			// As frontend can only display 7 of the last updated documents this is hardcoded here, but actually this info should come from frontend
			return await DocumentActorMessageHelpers.GetDocumentList(actorService, innerListType, Response, logService, host, Request.Query.filter, 7);
		}

		/// <summary>
		/// The paths in here are only available if connected to simulator.
		/// </summary>
		/// <param name="actorService"></param>
		/// <param name="connectToSimulator"></param>
		private void RegisterDummyPaths(IActorService actorService, Func<bool> connectToSimulator)
		{
			if (!connectToSimulator()) return;

			Get("/{documentId}/plates/{plateId}/wells", (ctx, ct) =>
			{
				var documentId = ctx.documentId;
				var plateId = ctx.plateId;

				var maxPoints = Request.Query.maxPoints;

				XXX(documentId, plateId, maxPoints);

				return null;
			});

			Delete("", (ctx, ct) =>
			{
				var documents = (DynamicDictionaryValue)Request.Query.documents;
				var documentsString = documents.TryParse<string>();
				var list = documentsString.Split(',').Select(int.Parse).ToList();

				return (dynamic)list.ToString();
			});

			Get("/wells", (ctx, ct) =>
			{
				var wells = (DynamicDictionaryValue)Request.Query.wells;
				var pointList = GetWellsList(wells);
				var maxPoints = Request.Query.maxPoints;
				YYY(pointList, maxPoints);

				return null;
			});

			Get("dummy/documentsOnStick", (ctx, ct) =>
			{
				var nbrOfResults = int.Parse(Request.Query.results);
				var nbrOfProtocols = int.Parse(Request.Query.protocols);
				actorService.Get<WebServiceRoutingActor>().Tell(
					MessageExtensions.CreateRequestMessage(new DocumentsForImportMessage
					{
						Documents = new DocumentsForImport { NumberOfProtocols = nbrOfProtocols, NumberOfResults = nbrOfResults }
					}));
				return Task.Run(() => HttpStatusCode.OK);
			});

			Get("/dummy/unplugStick", (ctx, ct) =>
			{
				actorService.Get<WebServiceRoutingActor>()
					.Tell(MessageExtensions.CreateRequestMessage(new UsbDrivePluggedOutMessage()));
				return Task.Run(() => HttpStatusCode.OK);
			});
		}

		private DataReductionSettings GetDataReductionSettingsFromBody()
		{
			var bodyString = Request.Body.ReadAsString(b => mLogService.Debug(LogCategoryEnum.WebApi, b, string.Empty));
			var settings = JsonConvert.DeserializeObject<DataReductionSettings>(bodyString);
			return settings;
		}

		private async Task<dynamic> ValidateReductionSettings(IActorService actorService, DataReductionSettings settings)
		{
			var validationMessage =
				MessageExtensions.CreateRequestMessage(new ValidateReductionSettingsMessage { Settings = settings });
			var response = await actorService.Get<InstrumentServiceProxyActor>().Ask<GimliResponse>(validationMessage);
			DataReductionSettings newSettings = null;
			var answer = ActorMessageHelpers.CheckResponseAndConvert<ValidateReductionSettingsResponseMessage>(response,
				(r, rf) =>
				{
					newSettings = r.Settings;
					return rf.AsText(settings.JsonSerializeToCamelCase());
				}, Response);
			if (!newSettings.IsValid) answer.StatusCode = HttpStatusCode.Conflict;
			return answer;
		}

		private async Task<dynamic> ExecuteDocumentListAction(IActorService actorService, ILogService logService, Func<IActorService, ILogService, List<Guid>, Task<GimliResponse>> documentListAction)
		{
			var stringBody = Request.Body.ReadAsString(b => logService.Debug(LogCategoryEnum.WebApi, b, string.Empty));
			var documents = JsonConvert.DeserializeObject<List<Guid>>(stringBody);
			var success = await documentListAction(actorService, logService, documents);
			return ActorMessageHelpers.GetOkOrErrorResponse(success);
		}

		private async Task<dynamic> UpdateQuickreadSlot(IActorService actorService, ILogService logService, QuickReadSlot slotInfo, int slotId)
		{
			var result = await actorService.Get<DataServiceProxyActor>()
				.AskCheckRunning(MessageExtensions.CreateRequestMessage(new GetCurrentUserMessage()), logService, 5000, mHost, actorService);
			var userResponse = result as GetCurrentUserResponseMessage;
			if (userResponse == null)
			{
				return ActorMessageHelpers.GetErrorResponse(result);
			}

			var setResult = await actorService.Get<DataServiceProxyActor>()
				.AskCheckRunning(MessageExtensions.CreateRequestMessage(new SetQuickreadSlotMessage
				{
					Slot = slotInfo,
					SlotId = slotId,
					User = userResponse.UserFull
				}), logService, 5000, mHost, actorService);
			return ActorMessageHelpers.GetOkOrErrorResponse(setResult);
		}

		private T GetAllSettingsFromBody<T>(dynamic ctx) where T : AllSettingsBase
		{
			var bodyString = Request.Body.ReadAsString(b => mLogService.Debug(LogCategoryEnum.WebApi, b, string.Empty));
			var settings = JsonConvert.DeserializeObject<T>(bodyString);
			settings.DocumentId = ctx["documentId"];
			settings.PlateId = ctx["plateId"];
			return settings;
		}

		public async Task<Response> StartRead(IActorService actorService, StartReadInfo info, IResponseFormatter responseFormatter, ILogService logService)
		{
			var readMessage = MessageExtensions.CreateRequestMessage(new StartReadMessage());
			var documentMessage = MessageExtensions.CreateRequestMessage(new GetInputForReadMessage
			{
				DocumentId = info.DocumentID,
				Plates = info.Plates,
				Overwrite = info.Overwrite,
				Name = info.Name
			});
			var response =
				await actorService.Get<DataServiceProxyActor>().AskCheckRunning(documentMessage, logService, 25000, mHost, actorService);

			var typed = ActorMessageHelpers.CheckResponseAndConvert<GetInputForReadResponseMessage>(response);

			if (typed == null)
			{
				return ActorMessageHelpers.GetErrorResponse(response);
			}

			readMessage.Model.Documents = typed.DocumentsToStart;

			var readResponse =
				await actorService.Get<InstrumentServiceProxyActor>().AskCheckRunning(readMessage, logService, null, mHost, actorService);

			return ActorMessageHelpers.CheckResponseAndConvert<StartReadResponseMessage>(readResponse, (r, rf) =>
			{
				var answer = new StartReadResponse { DocumentId = r.DocumentId, PlateId = r.PlateId, ExperimentId = r.ExperimentId };
				return rf.AsText(answer.JsonSerializeToCamelCase());
			}, responseFormatter);
		}

		private void CheckParseSuccess(bool parseSuccess, string mode)
		{
			if (!parseSuccess)
			{
				throw new ArgumentException($"Argument {mode} could not be parsed.");
			}
		}

		private static string UppercaseFirst(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				return string.Empty;
			}

			return char.ToUpper(s[0]) + s.Substring(1);
		}

		private Task<GimliResponse> DeleteFilesAsync(IActorService actorService, ILogService logService, List<Guid> deleteDocuments)
		{
			return actorService.Get<DataServiceProxyActor>()
				.AskCheckRunning(MessageExtensions.CreateRequestMessage(
					new DeleteDocumentsMessage { Documents = deleteDocuments }), logService, null, mHost, actorService);
		}
		private Task<GimliResponse> DeleteUsbFilesAsync(IActorService actorService, ILogService logService, List<Guid> deleteUsbDocuments)
		{
			return actorService.Get<DataServiceProxyActor>()
				.AskCheckRunning(MessageExtensions.CreateRequestMessage(
					new DeleteDocumentsMessage { Documents = deleteUsbDocuments, OnUsb = true }), logService, null, mHost, actorService);
		}

		private Task<GimliResponse> ExportFilesAsync(IActorService actorService, ILogService logService, List<Guid> exportDocuments, bool protocolsToUsb)
		{
			var exportFiles = new List<string>();

			foreach (var guid in exportDocuments)
			{
				exportFiles.Add(guid + ".xml");
			}

			var task = actorService.Get<DataServiceProxyActor>()
				.Ask<GetCurrentUserResponseMessage>(MessageExtensions.CreateRequestMessage(
					new GetCurrentUserMessage()));
			task.Wait();

			return actorService.Get<DataServiceProxyActor>()
				.AskCheckRunning(MessageExtensions.CreateRequestMessage(
					new ExportFilesMessage { Filenames = exportFiles.ToArray(), UserFull = task.Result.UserFull, SendProtocolsToUsb = protocolsToUsb }), logService, 25000, mHost, actorService);
		}

		private Task<GimliResponse> ImportFilesAsync(IActorService actorService, ILogService logService, List<Guid> exportDocuments)
		{
			return actorService.Get<DataServiceProxyActor>()
				.AskCheckRunning(MessageExtensions.CreateRequestMessage(
					new ImportFilesMessage { Files = exportDocuments.ToArray() }), logService, 25000, mHost, actorService);
		}

		private static List<Point> GetWellsList(DynamicDictionaryValue wells)
		{
			var points = new List<Point>();

			var match = Regex.Match(wells.ToString(), @"\(([0-9\-]+),([0-9\-]+)\)");

			while (match.Success)
			{
				points.Add(new Point { X = int.Parse(match.Groups[1].ToString()), Y = int.Parse(match.Groups[2].ToString()) });

				match = match.NextMatch();
			}

			return points;
		}

		#region DummyCode for testing
		private static void YYY(List<Point> wells, int maxPoints)
		{

		}

		private static void XXX(int id1, int id2, int maxPoints)
		{

		}
		#endregion DummyCode for testing
	}
}
