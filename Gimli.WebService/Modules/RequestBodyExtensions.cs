﻿using System;
using System.IO;

namespace Gimli.WebService.Modules
{
	public static class RequestBodyExtensions
	{
		public static string ReadAsString(this Stream requestStream, Action<string> logBody)
		{
			using (var reader = new StreamReader(requestStream))
			{
				var body = reader.ReadToEnd();
				logBody(body);
				return body;
			}
		}
	}
}
